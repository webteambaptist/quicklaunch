﻿namespace QuickLaunch.Shared
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public enum ImprivataEvent
    {
        UserLogon,

        WorkstationLock,

        WorkstationUnlock,

        NonOneSignLogon,

        Install,

        InstallComplete,

        Reset
    }
}