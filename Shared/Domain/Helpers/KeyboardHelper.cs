﻿namespace QuickLaunch.Shared
{
    using System;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    public static class KeyboardHelper
    {
        private const int INPUT_KEYBOARD = 1;

        /// <summary>
        /// Sends the CTRL+Q keystroke.
        /// </summary>
        public static void Ctrl_Q()
        {
            InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.LCONTROL, VirtualKeyCode.VK_Q);
        }

        /// <summary>
        /// Sends the Windows+D keystroke.
        /// </summary>
        public static void Windows_D()
        {
            InputSimulator.SimulateModifiedKeyStroke(VirtualKeyCode.LWIN, VirtualKeyCode.VK_D);
        }
        
    }
}