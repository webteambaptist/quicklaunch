﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//using ActiveDs;

namespace QuickLaunch.Shared
{
    public class ADHelper
    {        
        public ADHelper()
        {
        }

        /// <summary>
        /// Determines whether [is in group] [the specified user].
        /// </summary>
        /// <param name="groups">Available AD groups.</param>
        /// <param name="group">The group.</param>
        /// <returns>
        ///   <c>true</c> if [is in group] [the specified user]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInGroup(List<string> groups, string group)
        {
            return groups.Contains(group);
        }
        public static bool IsInGroup(List<string> groups, string[] group)
        {
            foreach (string s in group)
                if (groups.Contains(s))
                    return true;
            return false;
        }

        /// <summary>
        /// Gets the groups.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public static bool GetGroups(ref Shared.UserInfo user)
        {              
            bool result = true;
            string userPath = Utility.getPath() + "\\" + "QL_" + user.UserName + "_AD.xml";

            if (Utility.checkCache(userPath, "QL_USER_AD"))
            {

                result = FindAD(user, result);
            }
            else
            {
                try
                {
                    UserInfo ui = deserializeAD(userPath);
                    user.ADgroups = ui.ADgroups;
                    user.UserAdTitle = ui.UserAdTitle;
                    user.UserAdDept = ui.UserAdDept;
                }
                catch(Exception ee)//something is broken load from AD
                {
                    result = FindAD(user, result);
                }
            }

            return result;
        }

        private static bool FindAD(Shared.UserInfo user, bool result)
        {
            DirectorySearcher search = new DirectorySearcher(new DirectoryEntry("LDAP://BH.LOCAL"));  //DirectoryEntry de = new DirectoryEntry(string.Format("WinNT://{0}/{1},user", user.Domain, user.UserName)); 
            search.Filter = string.Format("(SAMAccountName={0})", user.UserName);
            search.PropertiesToLoad.Add("department");
            search.PropertiesToLoad.Add("title");
            SearchResult r = search.FindOne();
            try
            {
                var gs = (IEnumerable)r.GetDirectoryEntry().Invoke("Groups");

                foreach (var g in gs)
                {
                    var gn = new DirectoryEntry(g);
                    user.ADgroups.Add(gn.Name.Replace("CN=", string.Empty));
                }

                try
                {
                    user.UserAdTitle = r.Properties["title"][0].ToString();
                    user.UserAdDept = r.Properties["department"][0].ToString();
                }
                catch { user.UserAdTitle = string.Empty; user.UserAdDept = string.Empty; } //not all users have these attributes in AD, and properties.contains is not doing a proper check
                serializeUserAD(user);
            }
            catch (Exception ex)
            {
                result = false;
                //Program.LogException(ex, false);
            }
            search.Dispose();
            return result;
        }

        private static UserInfo deserializeAD(string userFile)
        {
            UserInfo uInfo = new UserInfo();
            try
            {
                Stream stream = File.Open(userFile, FileMode.Open);
                BinaryFormatter binFormatter = new BinaryFormatter();
                uInfo = (UserInfo)binFormatter.Deserialize(stream);
                stream.Close();
            }
            catch(Exception ex)
            {

            }
            return uInfo;
        }

        private static void serializeUserAD(UserInfo user)
        {
            string userFile = Utility.getPath() + "QL_" + user.UserName + "_AD.xml";
            
            if(File.Exists(userFile))
            {
                File.Delete(userFile);
            }

            Stream stream = File.Open(userFile, FileMode.Create);
            BinaryFormatter binFormatter = new BinaryFormatter();
            binFormatter.Serialize(stream, user);
            stream.Close();
        }

        /// <summary>
        /// Gets the groups.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public static List<Models.Machine> GetComputers()
        {
            List<Models.Machine> machines = new List<Models.Machine>();
            DirectoryEntry de = new DirectoryEntry(string.Format(@"LDAP://{0}", "bh"));
            DirectorySearcher ds = new DirectorySearcher(de, "(&(objectCategory=computer))", new string[] { "name", "description", "location" }, SearchScope.Subtree);            
            SearchResultCollection src = ds.FindAll();
            try
            {                
                foreach (SearchResult g in src)
                {
                    DirectoryEntry subDE = g.GetDirectoryEntry();
                    machines.Add(new Models.Machine {
                        PCName = (subDE.Properties["name"].Value ?? string.Empty).ToString(),
                        Description = (subDE.Properties["description"].Value ?? string.Empty).ToString(),
                        LastLogin = new DateTime(1900, 1, 1)
                    });
                }
            }
            catch (Exception ex)
            {                
                //Program.LogException(ex, false);
            }
            return machines;
        }
    
    }
}
