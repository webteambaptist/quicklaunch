﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using QuickLaunch.Shared;
using System.Configuration;
namespace QuickLaunch.Models
{
    [Serializable]
    public class Deployment : Abstract.IDataInfo  //for deployment, not in WCF
    {        
        #region Properties
        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the Install Message.
        /// </summary>
        public string InstallMessage { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build.
        /// </summary>
        public byte[] MSIBuild { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build file content type.
        /// </summary>
        public string MSIBuildContentType { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build file size.
        /// </summary>
        public string MSIBuildFileSize { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build version.
        /// </summary>
        public string MSIBuildVersion { get; set; }

        /// <summary>
        /// Gets/Sets the Locations
        /// </summary>
        public List<string> Locations { get; set; }
        
        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /// <summary>
        /// Gets/Sets the datetime the deployment was added
        /// </summary>
        public DateTime add_dt { get; set; }

        /// <summary>
        /// Gets/Sets the datetime the isPublished flag was set
        /// </summary>
        public DateTime publish_dt { get; set; }

        /// <summary>
        /// Gets/Sets the IsEverywhere indicator
        /// </summary>
        public bool IsEverywhere { get; set; }

        /// <summary>
        /// Gets/Sets the IsPublished indicator
        /// </summary>
        public bool IsPublished { get; set; }

        /// <summary>
        /// Gets/Sets the IsArchived indicator
        /// </summary>
        public bool IsArchived { get; set; }

 
        /* Display Fields */

        public int MachineCt { get; set; }
        /* End Display Fields */


        #endregion

        public Deployment() { }        

        #region "SELECTS"

        public static Deployment selDeploymentById(int id)
        {
            Deployment deploy = new Deployment();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selDeploymentById", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        try
                        {

                            deploy.id = Utility.IntParse(sqlDataReader["id"].ToString());
                            deploy.Title = sqlDataReader["title"].ToString();
                            deploy.Description = sqlDataReader["description"].ToString();
                            deploy.InstallMessage = sqlDataReader["installMessage"].ToString();
                            deploy.MSIBuild = sqlDataReader["MSIBuild"] as byte[];
                            deploy.MSIBuildContentType = sqlDataReader["MSIBuildContentType"].ToString();
                            deploy.MSIBuildFileSize = sqlDataReader["MSIBuildFileSize"].ToString();
                            deploy.MSIBuildVersion = sqlDataReader["MSIBuildVersion"].ToString();
                            deploy.lup_user = sqlDataReader["lup_user"].ToString();
                            deploy.lup_dt = Utility.DTParse(sqlDataReader["lup_dt"].ToString());
                            deploy.add_dt = Utility.DTParse(sqlDataReader["lup_dt"].ToString());
                            deploy.publish_dt = Utility.DTParse(sqlDataReader["lup_dt"].ToString());
                            deploy.IsEverywhere = Utility.BoolParse(sqlDataReader["IsEverywhere"].ToString());
                            deploy.IsPublished = Utility.BoolParse(sqlDataReader["IsPublished"].ToString());
                            deploy.IsArchived = Utility.BoolParse(sqlDataReader["IsArchived"].ToString());
                        }
                        catch (Exception ee)
                        { }
                    }
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch
            }//end sql

            return deploy;
        }

        public static Deployment selDeploymentById(string id)
        {
            Deployment deploy = new Deployment();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selDeploymentById", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        try
                        {

                            deploy.id = Utility.IntParse(sqlDataReader["id"].ToString());
                            deploy.Title = sqlDataReader["title"].ToString();
                            deploy.Description = sqlDataReader["description"].ToString();
                            deploy.InstallMessage = sqlDataReader["installMessage"].ToString();
                            deploy.MSIBuild = sqlDataReader["MSIBuild"] as byte[];
                            deploy.MSIBuildContentType = sqlDataReader["MSIBuildContentType"].ToString();
                            deploy.MSIBuildFileSize = sqlDataReader["MSIBuildFileSize"].ToString();
                            deploy.MSIBuildVersion = sqlDataReader["MSIBuildVersion"].ToString();
                            deploy.lup_user = sqlDataReader["lup_user"].ToString();
                            deploy.lup_dt = Utility.DTParse(sqlDataReader["lup_dt"].ToString());
                            deploy.add_dt = Utility.DTParse(sqlDataReader["lup_dt"].ToString());
                            deploy.publish_dt = Utility.DTParse(sqlDataReader["lup_dt"].ToString());
                            deploy.IsEverywhere = Utility.BoolParse(sqlDataReader["IsEverywhere"].ToString());
                            deploy.IsPublished = Utility.BoolParse(sqlDataReader["IsPublished"].ToString());
                            deploy.IsArchived = Utility.BoolParse(sqlDataReader["IsArchived"].ToString());
                        }
                        catch (Exception ee)
                        { }
                    }
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch
            }//end sql

            return deploy;
        }

        public static Deployment selPublishedDeployment(string Machine)
        { //NOTE IF THERE ARE NO LOCATION ASSOCIATED, IT IS ASSUMMED TO BE ALL LOCATIONS

            Deployment deployment = new Deployment();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selPublishedDeployment", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("machine", Machine);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            deployment.id = Utility.IntParse(sqlDataReader["id"].ToString());
                            deployment.Title = sqlDataReader["Title"].ToString();
                            deployment.Description = sqlDataReader["Description"].ToString();
                            deployment.MSIBuildVersion = sqlDataReader["MSIBuildVersion"].ToString();
                            deployment.publish_dt = Utility.DTParse(sqlDataReader["publish_dt"].ToString());
                            deployment.IsPublished = Utility.BoolParse(sqlDataReader["IsPublished"].ToString());
                            deployment.IsEverywhere = Utility.BoolParse(sqlDataReader["IsEverywhere"].ToString());
                            deployment.IsArchived = Utility.BoolParse(sqlDataReader["IsArchived"].ToString());                                
                        }
                        catch (Exception ee)
                        { }
                    }


                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql 


            return deployment;



        }

        #endregion



        public static List<Deployment> selActiveDeployments()
        {
            List<Deployment> deployments = new List<Deployment>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selActiveDeployment", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                   // sqlcmd.Parameters.AddWithValue("machine", Machine);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        Deployment deployment = new Deployment();

                        try
                        {
                            deployment.id = Utility.IntParse(sqlDataReader["id"].ToString());
                            deployment.Title = sqlDataReader["Title"].ToString();
                            deployment.Description = sqlDataReader["Description"].ToString();
                            deployment.MSIBuildVersion = sqlDataReader["MSIBuildVersion"].ToString();
                            deployment.publish_dt = Utility.DTParse(sqlDataReader["publish_dt"].ToString());
                            deployment.IsPublished = Utility.BoolParse(sqlDataReader["IsPublished"].ToString());
                            deployment.IsEverywhere = Utility.BoolParse(sqlDataReader["IsEverywhere"].ToString());
                            deployment.IsArchived = Utility.BoolParse(sqlDataReader["IsArchived"].ToString());
                        }
                        catch (Exception ee)
                        { }

                        deployments.Add(deployment);
                    }


                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql 


            return deployments;
        }
    }
}
