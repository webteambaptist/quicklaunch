﻿using QuickLaunch.Shared;
using QuickLaunch.Shared.Domain.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;

namespace QuickLaunch.Models
{
    /// <summary>
    /// DAO model to handle KillList DAL, it's just a string
    /// </summary>
    public class KillList
    {        
        /// <summary>
        /// gets new data from db and updates cache
        /// </summary>
        /// <returns></returns>
        public static void refreshKillList()
        {

            List<string> machineList = new List<string>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selKillList", sqlConn)  //get list of BPC machines
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        string m = sdr["name"].ToString();

                        machineList.Add(m);
                    }

                    sdr.Close();

                }
                catch (SqlException se)
                {
                    Truncus.LogErrorEvents(se.Message, false, Assembly.GetCallingAssembly().FullName);
                }
                catch (Exception ee)
                { 
                    Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
                }

            }//end sql


            bool isBPC = checkBPC(System.Environment.MachineName, machineList);

            deleteCache();

            saveCache(isBPC);
                        
        }

        private static void deleteCache()
        {
            try
            {
                string xmlFileName = Utility.getPath() + "QL_KillList.xml";

                if (File.Exists(xmlFileName))
                {
                    File.Delete(xmlFileName);
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// save kill list cache
        /// </summary>
        /// <param name="isBPC"></param>
        private static void saveCache(bool isBPC)
        {
            List<QLKillList> temp = QLKillList.selKillList(); 
            List<string> kList =  (from t in temp select t.ProcessName).ToList();

            if(kList.Count < 1)
            {
                kList = new List<string>() { "iexplore", "firefox", "chrome", "notepad", "cmd" };
            }

            if (!isBPC)
            {
                kList.Add("natspeak");
            }
            
            try
            {
                string xmlFileName = Utility.getPath() + "QL_KillList.xml";

                foreach (string s in kList)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("KillItems").Add(
                            new XElement("KillItem",
                                new XElement("Name", s)));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("Kill List for QL"),
                            new XElement("KillItems",
                                new XElement("KillItem",
                                    new XElement("Name", s))
                                    )
                                );
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

        }

        /// <summary>
        /// retrieve kill list from cache
        /// </summary>
        /// <returns></returns>
        public static List<string> getCache()
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            List<String> kList = new List<string>();

            bool isCache = false;

            string xmlFileName = Utility.getPath() + "QL_KillList.xml";

            if (!File.Exists(xmlFileName))
            {
                isCache = true;
                refreshKillList();
            }

            try
            {
                XDocument xmlDoc = XDocument.Load(xmlFileName);
                var kllList = from s in xmlDoc.Descendants("KillItem")
                              select new
                              {
                                  name = s.Element("Name").Value,
                              };

                foreach (var s in kllList)
                {
                    kList.Add(s.name.ToString());
                }
            }
            catch (Exception ee)
            {
                deleteCache(); //if cache is broken, just delete it and use defaults
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            sw.Stop();

            try
            {
                XMLEntry xmlEntry = new XMLEntry();
                xmlEntry.timeStamp = DateTime.Now;
                xmlEntry.eventType = "KILL LIST CACHE";
                xmlEntry.ntID = "";
                xmlEntry.machine = System.Environment.MachineName;
                xmlEntry.app = "";
                xmlEntry.timeElapsed = sw.ElapsedMilliseconds.ToString();
                Truncus.writeXMLEntry(xmlEntry);
            }
            catch (Exception ee)
            { }

            return kList;
        }

        /// <summary>
        /// check to see if machine is a bpc machine
        /// </summary>
        /// <param name="machine"></param>
        /// <param name="machineList"></param>
        /// <returns></returns>
        public static bool checkBPC(string machine, List<string> machineList)
        {
            bool isbpc = false;

            isbpc = (from m in machineList where String.Compare(machine, m, true) == 0 select m).Any();

            return isbpc;
        }

        [Obsolete]
        private List<string> getKillListfromDB()
        {
            string machine = System.Environment.MachineName;
            bool isbpc = false;
            List<string> machineList = GetBPCMachines();
            isbpc = (from m in machineList where String.Compare(machine, m, true) == 0 select m).Any();

            List<string> klist = new List<string>() { "iexplore", "firefox", "chrome", "notepad", "cmd" };
            if (isbpc)
            {
                klist.Add("natspeak");
            }

            return klist;
        }

        [Obsolete]
        private List<string> GetBPCMachines()
        {
            List<string> machlist = new List<string>();
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["ALTPROD"]);
            using (SqlConnection sqlConnection = new SqlConnection(config))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.usp_getBPCmachines", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        string s = sqlDataReader[0].ToString();
                        machlist.Add(s);
                    }
                    sqlDataReader.Close();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
            return machlist;
        }


    }
}
