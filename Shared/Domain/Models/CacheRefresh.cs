﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;

namespace QuickLaunch.Shared.Domain.Models
{
    public class CacheRefresh : Abstract.IDataInfo
    {
        public string ID { get; set; }
        public string CreateDT { get; set; }
        public string CacheName { get; set; }
        public string RefreshInHours { get; set; }

        public static List<CacheRefresh> selCacheRefresh()
        {
            string build = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            List<CacheRefresh> cList = new List<CacheRefresh>();

            //check for freshness
            bool stale = Utility.checkCache(Utility.getPath() + "QL_Refresh.xml");

            if (stale)
            {

                string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
                using (SqlConnection sqlConn = new SqlConnection(config))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.selQLCacheSetting", sqlConn)  //to do change to use SQL call and cache
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                        while (sqlDataReader.Read())
                        {
                            try
                            {
                                CacheRefresh c = new CacheRefresh()
                                {
                                    ID = sqlDataReader["ID"].ToString(),
                                    CreateDT = sqlDataReader["CreateDT"].ToString(),
                                    CacheName = sqlDataReader["CacheName"].ToString(),
                                    RefreshInHours = sqlDataReader["RefreshInHours"].ToString(),
                                };

                                cList.Add(c);
                            }
                            catch (Exception ee)
                            { }
                        }
                    }//end try

                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch
                }//end sql

                Utility.Serialize<List<CacheRefresh>>(cList, Utility.getPath() + "QL_Refresh.xml");
            }
            else
            {
                cList = Utility.Deserialize<List<CacheRefresh>>(Utility.getPath() + "QL_Refresh.xml");
            }
            return cList;
        }

    }
}
