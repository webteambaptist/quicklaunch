﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using QuickLaunch.Shared;
using System.Configuration;
using System.IO;
using System.Xml.Linq;
using System.Linq;

namespace QuickLaunch.Models
{
    public class Version_QL : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets/Sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets/Sets the latest version
        /// </summary>
        public string LatestVersion { get; set; }

        /// <summary>
        /// Gets/Sets the acceptable version 1
        /// </summary>
        public string AcceptableVersion1 { get; set; }

        /// <summary>
        /// Gets/Sets the acceptable version 2
        /// </summary>
        public string AcceptableVersion2 { get; set; }

        /// <summary>
        /// Gets/Sets the acceptable version 3
        /// </summary>
        public string AcceptableVersion3 { get; set; }

        /// <summary>
        /// Gets/Sets the Check Frequency
        /// </summary>
        public int CheckFrequency { get; set; }

        /// <summary>
        /// Gets/Sets the lup user
        /// </summary>
        public string lup_user { get; set; }

         /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /* Display Fields */
        public int MachineCt { get; set; }
        /* End Display Fields */

        #endregion

        public Version_QL() { }
        
        #region "SELECTS"

        public static List<Version_QL> selVersions()
        {

            List<Version_QL> vList = new List<Version_QL>();
            string xmlFileName = Utility.getPath() + "QL_VCache.xml";

            if (Utility.checkCache(xmlFileName, "QL_VCACHE")) //if true, load or reset cache
            {  
                string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
                using (SqlConnection sqlConn = new SqlConnection(config))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.selVersions", sqlConn)  //to do change to use SQL call and cache
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                        while(sqlDataReader.Read())
                        {
                            try
                            {
                                Version_QL v = new Version_QL()
                                {
                                    id = int.Parse(sqlDataReader["id"].ToString()),
                                    Code = sqlDataReader["Code"].ToString(),
                                    LatestVersion = sqlDataReader["LatestVersion"].ToString(),
                                    AcceptableVersion1 = sqlDataReader["AcceptableVersion1"].ToString(),
                                    AcceptableVersion2 = sqlDataReader["AcceptableVersion2"].ToString(),
                                    AcceptableVersion3 = sqlDataReader["AcceptableVersion3"].ToString(),
                                    Message = sqlDataReader["Message"].ToString(),
                                    CheckFrequency = int.Parse(sqlDataReader["CheckFrequency"].ToString()),
                                    lup_user = sqlDataReader["lup_user"].ToString(),
                                    lup_dt = DateTime.Now, //doesn't matter for this use here
                                    MachineCt = int.Parse(sqlDataReader["MachineCt"].ToString()),
                                };

                                vList.Add(v);
                            }
                            catch(Exception ee)
                            { }
                        }
                    }//end try

                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch
                }//end sql

                saveVCache(vList);
            }
            else
            {
                vList = getVCache();
            }
          
            return vList;
        }

        private static void saveVCache(List<Version_QL> vList)
        {
            try
            {
                string xmlFileName = Utility.getPath() + "QL_VCache.xml";
                
                foreach(Version_QL v in vList)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("VItems").Add(
                                new XElement("VItem",
                                    new XElement("id", v.id.ToString()),
                                    new XElement("Code", v.Code),
                                    new XElement("LatestVersion", v.LatestVersion),
                                    new XElement("AcceptableVersion1", v.AcceptableVersion1),
                                    new XElement("AcceptableVersion2", v.AcceptableVersion2),
                                    new XElement("AcceptableVersion3", v.AcceptableVersion3),
                                    new XElement("Message", v.Message),
                                    new XElement("CheckFrequency", v.CheckFrequency.ToString()),
                                    new XElement("lup_user", v.lup_user),
                                    new XElement("lup_dt", v.lup_dt.ToString()),
                                    new XElement("MachineCt", v.MachineCt)));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("Version cache for QL"),
                            new XElement("VItems",
                                new XElement("VItem",
                                    new XElement("id", v.id.ToString()),
                                    new XElement("Code", v.Code),
                                    new XElement("LatestVersion", v.LatestVersion),
                                    new XElement("AcceptableVersion1", v.AcceptableVersion1),
                                    new XElement("AcceptableVersion2", v.AcceptableVersion2),
                                    new XElement("AcceptableVersion3", v.AcceptableVersion3),
                                    new XElement("Message", v.Message),
                                    new XElement("CheckFrequency", v.CheckFrequency.ToString()),
                                    new XElement("lup_user", v.lup_user),
                                    new XElement("lup_dt", v.lup_dt.ToString()),
                                    new XElement("MachineCt", v.MachineCt))));
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch(Exception ee)
            { }
        }

        private static List<Version_QL> getVCache()
        {
            List<Version_QL> vList = new List<Version_QL>();

            try
            {
                string xmlFileName = Utility.getPath() + "QL_VCache.xml";

                if (File.Exists(xmlFileName))
                {
                    XDocument xmlDoc = XDocument.Load(xmlFileName);
                    var appData = from app in xmlDoc.Descendants("VItem")
                                  select new Version_QL()
                                  {
                                      id = int.Parse(app.Element("id").Value.ToString()),
                                      Code = app.Element("Code").Value.ToString(),
                                      LatestVersion = app.Element("LatestVersion").Value.ToString(),
                                      AcceptableVersion1 = app.Element("AcceptableVersion1").Value.ToString(),
                                      AcceptableVersion2 = app.Element("AcceptableVersion2").Value.ToString(),
                                      AcceptableVersion3 = app.Element("AcceptableVersion3").Value.ToString(),
                                      Message = app.Element("Message").Value.ToString(),
                                      CheckFrequency = int.Parse(app.Element("CheckFrequency").Value.ToString()),
                                      lup_user = app.Element("lup_user").Value.ToString(),
                                      lup_dt = DateTime.Parse(app.Element("lup_dt").Value.ToString()),
                                      MachineCt = int.Parse(app.Element("MachineCt").Value.ToString())
                                  };

                    foreach (Version_QL a in appData)
                    {
                        vList.Add(a);
                    }
                }
            }
            catch (Exception ee)
            { }

            return vList;
        }

        #endregion


   }
}
