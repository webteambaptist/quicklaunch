﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using QuickLaunch.Shared;
using System.Configuration;
using System.Xml.Linq;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;

namespace QuickLaunch.Models
{    
    public class Bookmark : Abstract.IDataInfo
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the network id.
        /// </summary>
        public string NtId { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the url.
        /// </summary>
        public string Url { get; set; }
       
        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /// <summary>
        /// Gets/Sets the add datetime
        /// </summary>
        public DateTime add_dt { get; set; }

        public bool IsArchived { get; set; }

        #endregion

        public Bookmark() { }
                        

        #region "SELECTS"

        public static List<Bookmark> selBookmarkByUserId(string userId)
        {
            string xmlFileName = Utility.getPath() + "QL_" + userId + "BkmkCache.xml";
            List<Bookmark> bookmarks = new List<Bookmark>();
            
            if (Utility.checkCache(xmlFileName, "QL_USER_BKMKCACHE"))
            {
                bookmarks = getFromDB(userId);
                saveBCache(bookmarks, xmlFileName);
            }
            else
            {
                bookmarks = getBCache(xmlFileName, userId);
            }

            return bookmarks;
        }

        private static List<Bookmark> getFromDB(string userId)
        {
            List<Bookmark> bookmarks = new List<Bookmark>();
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selBookmarkByUserId", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlcmd.Parameters.AddWithValue("userId", userId);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            Bookmark b = new Bookmark()
                            {
                                id = int.Parse(sqlDataReader["id"].ToString()),
                                NtId = sqlDataReader["NtId"].ToString(),
                                Title = sqlDataReader["Title"].ToString(),
                                Url = sqlDataReader["Url"].ToString(),
                                OrderId = int.Parse(sqlDataReader["OrderId"].ToString()),
                                lup_dt = DateTime.Now, //placeholder data
                                add_dt = DateTime.Now,
                                IsArchived = false
                            };

                            bookmarks.Add(b);
                        }
                        catch (Exception ee)
                        { }
                    }
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch
            }//end sql

            return bookmarks;
        }

        private static List<Bookmark> getBCache(string xmlFileName, string userId)
        {
            List<Bookmark> bList = new List<Bookmark>();

            try
            {
                if (File.Exists(xmlFileName))
                {
                    XDocument xmlDoc = XDocument.Load(xmlFileName);
                    var appData = from app in xmlDoc.Descendants("BItem")
                                  select new Bookmark()
                                  {
                                      id = int.Parse(app.Element("id").Value.ToString()),
                                      NtId = app.Element("NtId").Value.ToString(),
                                      Title = app.Element("Title").Value.ToString(),
                                      Url = app.Element("Url").Value.ToString(),
                                      OrderId = int.Parse(app.Element("OrderId").Value.ToString()),
                                      lup_dt = DateTime.Now, //placeholder data
                                      add_dt = DateTime.Now,
                                      IsArchived = bool.Parse(app.Element("IsArchived").Value.ToString())       
                                  };

                    foreach (Bookmark a in appData)
                    {
                        bList.Add(a);
                    }
                }
            }
            catch (Exception ee)
            {
                bList = getFromDB(userId);
            }

            return bList;
        }

        private static void saveBCache(List<Bookmark> bookmarks, string xmlFileName)
        {
            try
            {
                foreach (Bookmark b in bookmarks)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("BItems").Add(
                                new XElement("BItem",
                                    new XElement("id", b.id.ToString()),
                                    new XElement("NtId", b.NtId),
                                    new XElement("Title", b.Title),
                                    new XElement("Url", b.Url),
                                    new XElement("OrderId", b.OrderId),
                                    new XElement("lup_dt", b.lup_dt.ToString()),
                                    new XElement("add_dt", b.add_dt.ToString()),
                                    new XElement("IsArchived", b.IsArchived.ToString())));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("Bookmark cache for QL"),
                            new XElement("BItems",
                                new XElement("BItem",
                                    new XElement("id", b.id.ToString()),
                                    new XElement("NtId", b.NtId),
                                    new XElement("Title", b.Title),
                                    new XElement("Url", b.Url),
                                    new XElement("OrderId", b.OrderId),
                                    new XElement("lup_dt", b.lup_dt.ToString()),
                                    new XElement("add_dt", b.add_dt.ToString()),
                                    new XElement("IsArchived", b.IsArchived.ToString()))));
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch (Exception ee)
            { }
        }

        /// serialize data in local cache
        /// </summary>
        /// <param name="listData"></param>
        /// <param name="xmlFileName"></param>
        private static void serializeCache(List<Bookmark> listData, string xmlFileName)
        {
            try
            {
                if (File.Exists(xmlFileName))
                {
                    File.Delete(xmlFileName);
                }

                Stream stream = File.Open(xmlFileName, FileMode.Create);
                BinaryFormatter binFormatter = new BinaryFormatter();
                binFormatter.Serialize(stream, listData);
                stream.Close();
            }
            catch (Exception ee)
            {

            }

        }

        #endregion

        #region "INSERTS"

        public static string insBookmark(Bookmark b)
        {
            string id = "";

            //string sql = @"INSERT INTO SSO_Bookmark ([NtId],[Title],[Url],[OrderId]) VALUES (@NtId,@Title,@Url,@OrderId); SELECT @@IDENTITY as id";
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

                using (SqlConnection sqlConn = new SqlConnection(config))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.insBookmark", sqlConn)  //to do change to use SQL call and cache
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        sqlcmd.Parameters.AddWithValue("NtId", b.NtId);
                        sqlcmd.Parameters.AddWithValue("Title", Shared.SysHelper.CleanSql(b.Title));
                        sqlcmd.Parameters.AddWithValue("Url", Shared.SysHelper.CleanSql(b.Url));
                        sqlcmd.Parameters.AddWithValue("OrderID", b.OrderId);
                        var retVal = sqlcmd.ExecuteScalar();
                        id = retVal.ToString();
                    }//end try
                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch

                }//end sql    

                return id;

        }

        public static void insBookmark(List<Bookmark> fav)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            foreach (Bookmark b in fav)
            {
                using (SqlConnection sqlConn = new SqlConnection(config))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.insBookmark", sqlConn)  //to do change to use SQL call and cache
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        sqlcmd.Parameters.AddWithValue("NtId", b.NtId);
                        sqlcmd.Parameters.AddWithValue("Title", Shared.SysHelper.CleanSql(b.Title));
                        sqlcmd.Parameters.AddWithValue("Url", Shared.SysHelper.CleanSql(b.Url));
                        sqlcmd.Parameters.AddWithValue("OrderID", b.OrderId);
                        sqlcmd.ExecuteNonQuery();

                    }//end try
                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch

                }//end sql
            }

            //foreach(Bookmark b in fav)
            //    sql += string.Format(@"INSERT INTO SSO_Bookmark ([NtId],[Title],[Url],[OrderId]) VALUES ('{0}','{1}','{2}',{3}); ", b.NtId, Shared.SysHelper.CleanSql(b.Title), Shared.SysHelper.CleanSql(b.Url), b.OrderId);  
        }

        #endregion

        #region "UPDATES"

        public static void updBookmark(List<Bookmark> fav)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            foreach (Bookmark b in fav)
            {
                using (SqlConnection sqlConn = new SqlConnection(config))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.updBookmark", sqlConn)  //to do change to use SQL call and cache
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        sqlcmd.Parameters.AddWithValue("Title", Shared.SysHelper.CleanSql(b.Title));
                        sqlcmd.Parameters.AddWithValue("Url", Shared.SysHelper.CleanSql(b.Url));
                        sqlcmd.Parameters.AddWithValue("OrderID", b.OrderId);
                        sqlcmd.Parameters.AddWithValue("id", b.id);
                        sqlcmd.ExecuteNonQuery();

                    }//end try
                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch

                }//end sql
            }

            //foreach (Bookmark b in fav)
            //    sql += string.Format(@"UPDATE SSO_Bookmark SET [Title] = '{1}',[Url] = '{2}',[OrderId] = {3}, lup_dt = getdate() WHERE [id] = {0}; ", b.id, Shared.SysHelper.CleanSql(b.Title), Shared.SysHelper.CleanSql(b.Url), b.OrderId);
        }

        public static void delBookmark(List<Bookmark> fav)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            foreach (Bookmark b in fav)
            {

                using (SqlConnection sqlConn = new SqlConnection(config))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.delBookmark", sqlConn)  //to do change to use SQL call and cache
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        sqlcmd.Parameters.AddWithValue("id", b.id);
                        sqlcmd.ExecuteNonQuery();

                    }//end try
                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch

                }//end sql
            }
        }
        #endregion
    }
}
