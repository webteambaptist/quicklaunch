﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLaunch.Models
{
    public class DeploymentLocation : Abstract.IDataInfo
    {
        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime? lup_dt { get; set; }



        public static List<DeploymentLocation> selDeployLocations(int deployID)
        {
            List<DeploymentLocation> dLocs = new List<DeploymentLocation>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selDeployLocations", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlcmd.Parameters.AddWithValue("DeployId", deployID);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        DeploymentLocation mac = new DeploymentLocation();

                        mac.id = int.Parse(sdr["id"].ToString());
                        mac.Title = sdr["Title"].ToString();
                        mac.Description = sdr["Description"].ToString();
                        mac.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());

                        dLocs.Add(mac);
                    }//end while

                    sdr.Close();


                }//end try
                catch (SqlException se)
                {
                    // EventLog.WriteEntry("QL", se.Message);
                }//end catch
                catch (Exception ee)
                {
                    // EventLog.WriteEntry("QL", ee.Message);
                }//end catch

            }//end sql

            return dLocs;
        }
    }
}
