﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using QuickLaunch.Shared;
using System.Configuration;
using System.Xml.Linq;
using System.Linq;

namespace QuickLaunch.Models
{
    [Serializable]
    public class MenuApplication : App, Abstract.IDataInfo //moved to WCF
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the menu link ID.
        /// </summary>
        public int MenuLink_Id { get; set; }

        /// <summary>
        /// Gets/Sets the application ID.
        /// </summary>
        public int Application_Id { get; set; }

        /// <summary>
        /// Gets/Sets the Order ID.
        /// </summary>
        public int Order_Id { get; set; }

 
        /* Display Fields */

        
        /* End Display Fields */

        /// <summary>
        /// Gets/Sets relationship between Citrix Applications and menu.
        /// Default list for offline mode : gets overriden on QL menu load
        /// </summary>        
        public static List<MenuApplication> MenuApplications = new List<MenuApplication> {  
                    new MenuApplication { MenuLink_Id = 100, Application_Id = 100, Order_Id = 1},
                    new MenuApplication { MenuLink_Id = 100, Application_Id = 101, Order_Id = 2},
                    new MenuApplication { MenuLink_Id = 100, Application_Id = 102, Order_Id = 3},
                    new MenuApplication { MenuLink_Id = 101, Application_Id = 103, Order_Id = 1},
                    new MenuApplication { MenuLink_Id = 101, Application_Id = 104, Order_Id = 2},
                    new MenuApplication { MenuLink_Id = 102, Application_Id = 105, Order_Id = 1},
                    new MenuApplication { MenuLink_Id = 102, Application_Id = 106, Order_Id = 2}
        };
        public static DateTime LastMenuApplicationsUpdate { get; set; }
        #endregion

        public MenuApplication() { }

        public static void LoadMenuApplications()
        {
            if (LastMenuApplicationsUpdate < DateTime.Now.AddHours(-1)) //only check once an hour
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();

                List<MenuApplication> DbApps = selMenuApplications();
                if (DbApps.Count > 0)
                {
                    MenuApplications.Clear();
                    MenuApplications.AddRange(DbApps);
                    LastMenuApplicationsUpdate = DateTime.Now;
                }

                watch.Stop();

                try
                {
                    string logLevel = Utility.getSettingFromCache("LogLevel");

                    if (string.IsNullOrEmpty(logLevel))
                    {
                        logLevel = "1";
                    }

                    if ((logLevel == "2") || (logLevel) == "3") //only log if more thorough diagnostics turned on
                    {
                        XMLEntry xmlEntry = new XMLEntry();
                        xmlEntry.timeStamp = DateTime.Now;
                        xmlEntry.eventType = "Load Menu Applications";
                        xmlEntry.ntID = "";
                        xmlEntry.machine = System.Environment.MachineName;
                        xmlEntry.app = "";
                        xmlEntry.timeElapsed = watch.ElapsedMilliseconds.ToString();
                        Truncus.writeXMLEntry(xmlEntry);
                    }
                }
                catch (Exception ee) { }
            }
        }        

        #region "SELECTS"

        public static List<MenuApplication> selMenuApplications()
        {

            string xmlFileName = Utility.getPath() + "QL_MenuAppCache.xml";
            List<MenuApplication> menuApps = new List<MenuApplication>();

            if (Utility.checkCache(xmlFileName, "QL_MENUAPPCACHE"))
            {
                menuApps = getFromDB();

                if (menuApps.Count > 0)
                {
                    saveMACache(menuApps, xmlFileName);
                }
            }
            else
            {
                menuApps = getMACache(xmlFileName);
            }

            return menuApps;
        }

        private static List<MenuApplication> getFromDB()
        {
            List<MenuApplication> menuApps = new List<MenuApplication>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selMenuApplications", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            MenuApplication a = new MenuApplication()
                            {
                                id = int.Parse(sqlDataReader["id"].ToString()),
                                MenuLink_Id = int.Parse(sqlDataReader["MenuLink_Id"].ToString()),
                                Application_Id = int.Parse(sqlDataReader["Application_Id"].ToString()),
                                Order_Id = int.Parse(sqlDataReader["Order_Id"].ToString()),
                                lup_dt = DateTime.Now,
                                Title = sqlDataReader["Title"].ToString(),
                                Value = sqlDataReader["Value"].ToString(),
                                WindowTitle = sqlDataReader["WindowTitle"].ToString()
                            };

                            menuApps.Add(a);
                        }
                        catch (Exception ee)
                        { }
                    }
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch
            }//end sql

            return menuApps;
        }

        private static List<MenuApplication> getMACache(string xmlFileName)
        {
            List<MenuApplication> maList = new List<MenuApplication>();

            try
            {
                if (File.Exists(xmlFileName))
                {
                    XDocument xmlDoc = XDocument.Load(xmlFileName);
                    var appData = from app in xmlDoc.Descendants("MAItem")
                                  select new MenuApplication()
                                  {
                                      id = int.Parse(app.Element("id").Value.ToString()),
                                      MenuLink_Id = int.Parse(app.Element("MenuLink_Id").Value.ToString()),
                                      Application_Id = int.Parse(app.Element("Application_Id").Value.ToString()),
                                      Order_Id = int.Parse(app.Element("Order_Id").Value.ToString()),
                                      lup_dt = DateTime.Now,
                                      Title = app.Element("Title").Value,
                                      Value = app.Element("Value").Value,
                                      WindowTitle = app.Element("WindowTitle").Value
                                  };

                    foreach (MenuApplication a in appData)
                    {
                        maList.Add(a);
                    }
                }
            }
            catch (Exception ee)
            {
                maList = getFromDB();
            }

            return maList;
        }

        private static void saveMACache(List<MenuApplication> menuApps, string xmlFileName)
        {
            try
            {
                foreach (MenuApplication a in menuApps)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("MAItems").Add(
                                new XElement("MAItem",
                                    new XElement("id", a.id.ToString()),
                                    new XElement("MenuLink_Id", a.MenuLink_Id.ToString()),
                                    new XElement("Application_Id", a.Application_Id.ToString()),
                                    new XElement("Order_Id", a.Order_Id.ToString()),
                                    new XElement("lup_dt", ""),
                                    new XElement("Title", a.Title), 
                                    new XElement("Value", a.Value),
                                    new XElement("WindowTitle", a.WindowTitle)
                                    ));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("Menu App cache for QL"),
                            new XElement("MAItems",
                                new XElement("MAItem",
                                    new XElement("id", a.id.ToString()),
                                    new XElement("MenuLink_Id", a.MenuLink_Id.ToString()),
                                    new XElement("Application_Id", a.Application_Id.ToString()),
                                    new XElement("Order_Id", a.Order_Id.ToString()),
                                    new XElement("lup_dt", ""),
                                    new XElement("Title", a.Title),
                                    new XElement("Value", a.Value),
                                    new XElement("WindowTitle", a.WindowTitle)                                    
                                    )));
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch (Exception ee)
            { }
        }

        #endregion
    }
}
