﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLaunch.Shared.Domain.Models
{
    public class QLKillList
    {
        #region Properties
        public int id { get; set; }
        public string ProcessName { get; set; }
        public Boolean Active { get; set; }
        public DateTime CreateDt { get; set; }

        public string UpdateUser { get; set; }
        public DateTime LastUpdate { get; set; }
        #endregion

        public QLKillList() { }

        #region "SELECTS"
        public static QLKillList selKillListById(string id)
        {
            QLKillList kl = new QLKillList();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selKillListById", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            kl.id = int.Parse(sqlDataReader["id"].ToString());
                            kl.ProcessName = sqlDataReader["ProcessName"].ToString();
                            kl.Active = Boolean.Parse(sqlDataReader["Active"].ToString());
                            kl.CreateDt = DateTime.Parse(sqlDataReader["CreateDt"].ToString());
                            kl.LastUpdate = DateTime.Parse(sqlDataReader["LastUpdate"].ToString());
                            kl.UpdateUser = sqlDataReader["UpdateUser"].ToString();
                        }
                        catch (Exception ee)
                        { }
                    }

                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return kl;
        }

        public static List<QLKillList> selKillList()
        {
            List<QLKillList> qlkl = new List<QLKillList>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selKillList_Admin", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            QLKillList kl = new QLKillList();
                            kl.id = int.Parse(sqlDataReader["id"].ToString());
                            kl.ProcessName = sqlDataReader["ProcessName"].ToString();
                            kl.Active = Boolean.Parse(sqlDataReader["Active"].ToString());
                            kl.CreateDt = DateTime.Parse(sqlDataReader["CreateDt"].ToString());
                            kl.LastUpdate = DateTime.Parse(sqlDataReader["LastUpdate"].ToString());
                            kl.UpdateUser = sqlDataReader["UpdateUser"].ToString();

                            qlkl.Add(kl);
                        }
                        catch (Exception ee)
                        { }
                    }

                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return qlkl;
        }

        #endregion

        #region "INSERTS"
        public static void insKillList(QLKillList KL)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insKillList", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ProcessName", KL.ProcessName);
                    sqlcmd.Parameters.AddWithValue("Active", KL.Active);
                    sqlcmd.Parameters.AddWithValue("LastUpdate", KL.LastUpdate);
                    sqlcmd.Parameters.AddWithValue("UpdateUser", KL.UpdateUser);
                    sqlcmd.Parameters.AddWithValue("CreateDt", KL.CreateDt);

                    sqlcmd.ExecuteNonQuery();

                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }
        #endregion

        #region "UPDATES"
        public static void updKillList(QLKillList KL)
        {            
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.updKillList", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", KL.id);
                    sqlcmd.Parameters.AddWithValue("ProcessName", KL.ProcessName);
                    sqlcmd.Parameters.AddWithValue("Active", KL.Active);
                    sqlcmd.Parameters.AddWithValue("LastUpdate", KL.LastUpdate);
                    sqlcmd.Parameters.AddWithValue("UpdateUser", KL.UpdateUser);

                    sqlcmd.ExecuteNonQuery();

                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }
        #endregion
    }
}
