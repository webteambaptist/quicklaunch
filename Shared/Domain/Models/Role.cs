﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class Role : Abstract.IDataInfo //moved to WCF
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the role title.
        /// </summary>
        public string role { get; set; }

        /// <summary>
        /// Gets/Sets the lup user
        /// </summary>
        public string lup_user { get; set; }

         /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion

        public Role() { }       

    }
}
