﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLaunch.Models
{
    public class DeploymentResponse : Abstract.IDataInfo
    {

        public static void Upsert(string PCName, string QLVersion)
        {

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.upsDepResponse", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("PCName", PCName);
                    sqlcmd.Parameters.AddWithValue("QLVersion", QLVersion);
                    sqlcmd.ExecuteNonQuery();

                }//end try
                catch (SqlException se)
                {
                    // EventLog.WriteEntry("QL", se.Message);
                }//end catch
                catch (Exception ee)
                {
                    // EventLog.WriteEntry("QL", ee.Message);
                }//end catch

            }//end sql
        }
    }
}
