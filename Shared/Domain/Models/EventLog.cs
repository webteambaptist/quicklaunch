﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using QuickLaunch.Shared;
using System.Configuration;
namespace QuickLaunch.Models
{
    public class EventLog_QL : BaseModel, Abstract.IDataInfo //moved to WCF
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the EventCode.
        /// </summary>
        public string EventCode { get; set; }

        /// <summary>
        /// Gets/Sets the NtId
        /// </summary>
        public string NtId { get; set; }

        /// <summary>
        /// Gets/Sets the Machine
        /// </summary>
        public string Machine { get; set; }

        /// <summary>
        /// Gets/Sets the App
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// Gets/Sets the Menu Item Id.
        /// </summary>
        public int MenuItemId { get; set; }

        /// <summary>
        /// Gets/Sets the time length in event.
        /// </summary>
        public double LengthOfTime { get; set; }

        /* Display Fields */
        public int GroupCount { get; set; }
        public long JSTimeStamp { get; set; }
        /* End Display Fields */

        #endregion

        public EventLog_QL() { }   

        #region "INSERTS"
        public static string insEventLog(EventLog_QL EventLog)
        {
//            string sql = @"INSERT INTO SSO_EventLog ([EventCode],[NtId],[Machine],[Application],[MenuItemId],[LengthOfTime]) 
//                                             VALUES (@EventCode,@NtId,@Machine,@Application,@MenuItemId,@LengthOfTime); SELECT @@IDENTITY as id";
//            SqlParameter[] @params = new SqlParameter[10];

//            @params[0] = new SqlParameter("@EventCode", EventLog.EventCode);
//            @params[1] = new SqlParameter("@NtId", EventLog.NtId);
//            @params[2] = new SqlParameter("@Machine", EventLog.Machine);
//            @params[3] = new SqlParameter("@Application", EventLog.Application);
//            @params[4] = new SqlParameter("@MenuItemId", EventLog.MenuItemId);
//            @params[5] = new SqlParameter("@LengthOfTime", EventLog.LengthOfTime);
            
//            return BOHelper.FillObject<EventLog_QL>(DBManager.ExecCommandDR(sql, @params)).id.ToString();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insEventLog", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("EventCode", EventLog.EventCode);
                    sqlcmd.Parameters.AddWithValue("NtId", EventLog.NtId);
                    sqlcmd.Parameters.AddWithValue("Machine", EventLog.Machine);
                    sqlcmd.Parameters.AddWithValue("Application", EventLog.Application);
                    sqlcmd.Parameters.AddWithValue("MenuItemId", EventLog.MenuItemId);
                    sqlcmd.Parameters.AddWithValue("LengthOfTime", EventLog.LengthOfTime);

                }
                catch (Exception ee)
                {

                }
            }

            return string.Empty;
        }

        public static string insEventLog(string eventCode, string NTID, string machine, string application, int mID, double length)
        {
            string id = "";
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insEventLog";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("EventCode", eventCode);
                    sqlcmd.Parameters.AddWithValue("NtId", NTID);
                    sqlcmd.Parameters.AddWithValue("Machine", machine);
                    sqlcmd.Parameters.AddWithValue("Application", application);
                    sqlcmd.Parameters.AddWithValue("MenuItemId", mID);
                    sqlcmd.Parameters.AddWithValue("LengthOfTime", length);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {

                }
            }//end sql

            return id;
        }

        #endregion

        public static void LogEvent(string Code, string UserId, string App, int MenuItemId = 0, double LengthOfTime = 0)
        {
            try
            {
                insEventLog(new EventLog_QL
                {
                     EventCode = Code
                    ,NtId = UserId.ToUpper()
                    ,Machine = System.Environment.MachineName
                    ,LengthOfTime = LengthOfTime
                    ,MenuItemId = MenuItemId
                    ,Application = App
                });
            } catch {} //do nothing if network is unavailable
        }

    }
}
