﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{    
    public class Event : Abstract.IDataInfo //moved to WCF
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the code.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }


 
        #endregion

        public Event() { }
                        

        #region "SELECTS"

        public static Event selEventById(int id)
        {
            string sql = "";
            sql = @"SELECT * FROM SSO_Event e                        
                    WHERE e.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<Event>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Event> selEvents()
        {
            string sql = @"SELECT e.* FROM SSO_Event e                                 
                           order by title";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Event>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"
        public static string insEvent(Event Event)
        {
            string sql = @"INSERT INTO SSO_Event ([Title],[Code]) VALUES (@Title,@Code); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@Title", Event.Title);
            @params[1] = new SqlParameter("@Code", Event.Code);                        

            return BOHelper.FillObject<Event>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updEvent(Event Event)
        {
            string sql = @"UPDATE SSO_MachineEvent SET [Title] = @Title,[Code] = @Code,lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@Title", Event.Title);
            @params[1] = new SqlParameter("@Code", Event.Code);                        
            @params[2] = new SqlParameter("@lup_dt", Event.lup_dt);            
            @params[3] = new SqlParameter("@id", Event.id);

            DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void delEvent(Event Event)
        {
            throw new NotImplementedException();
            string sql = @"DELETE FROM SSO_MachineEvent WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", Event.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }

        #endregion

    }
}
