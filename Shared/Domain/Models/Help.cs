﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace QuickLaunch.Models
{
    public class Help : Abstract.IDataInfo //moved to WCF
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the parent node ID.
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// Gets/Sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets/Sets the Open Onload indicator
        /// </summary>
        public bool IsOpenOnload { get; set; }

        /// <summary>
        /// Gets/Sets the node selectable indicator
        /// </summary>
        public bool IsSelectable { get; set; }

        /// <summary>
        /// Gets/Sets the order ID.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets/Sets the lup user
        /// </summary>
        public string lup_user { get; set; }

         /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion

        public Help() { }
        
        #region "SELECTS"

        public static Help selHelpById(int id)
        {
            string sql = "";
            Help help = new Help();
            sql = "SELECT * FROM SSO_Help WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);
            return help;          
           // return BOHelper.FillObject<Help>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<Help> selHelps()
        {
            List<Help> helpList = new List<Help>();
            string sql = "SELECT * FROM SSO_Help order by ParentId, OrderId";
            SqlParameter[] @params = new SqlParameter[1];

            return helpList; //BOHelper.FillCollection<Help>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<Help> selHelpByParentId(int ParentId)
        {
            List<Help> helpList = new List<Help>();
            string sql = "SELECT * FROM SSO_Help WHERE [ParentId] = @ParentId order by ParentId, OrderId";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@ParentId", ParentId);
            return helpList; //BOHelper.FillCollection<Help>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

    }
}
