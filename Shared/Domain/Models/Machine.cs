﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace QuickLaunch.Models
{
    [Serializable]
    public class Machine : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the pc.
        /// </summary>
        public string PCName { get; set; }

        /// <summary>
        /// Gets/Sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the LSID.
        /// </summary>
        public string LSID { get; set; }

        /// <summary>
        /// Gets/Sets the location id.
        /// </summary>
        public int? LocationId { get; set; }

        /// <summary>
        /// Gets/Sets the QLVersion.
        /// </summary>
        public string QLVersion { get; set; }

        /// <summary>
        /// Gets/Sets the ICAVersion.
        /// </summary>
        public string ICAVersion { get; set; }

        /// <summary>
        /// Gets/Sets the IEVersion.
        /// </summary>
        public string IEVersion { get; set; }

        /// <summary>
        /// Gets/Sets the AutoInstall Version_QL on the computer.
        /// </summary>
        public string AutoInstallVersion { get; set; }

        /// <summary>
        /// Gets/Sets the Computer Info.
        /// </summary>
        public string ComputerInfo { get; set; }

        /// <summary>
        /// Gets/Sets the Operating System Info.
        /// </summary>
        public string OSInfo { get; set; }

        /// <summary>
        /// Gets/Sets the Processes running on the computer.
        /// </summary>
        public string Processes { get; set; }

        /// <summary>
        /// Gets/Sets the Memory on the computer.
        /// </summary>
        public string Memory { get; set; }

        /// <summary>
        /// Gets/Sets the IpAddress of the computer.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets/Sets the Last login.
        /// </summary>
        public DateTime? LastLogin { get; set; } //nullable

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

 


        /* Display Fields */

        /// <summary>
        /// Gets/Sets the Location Title.
        /// </summary>
        public string LocationTitle { get; set; }

        /// <summary>
        /// Gets/Sets the WTS Printer Location.
        /// </summary>
        public string WTSLocation { get; set; }

        public double AvgLoginLength { get; set; }
        public double AvgEMRLaunchLength { get; set; }
        public double AvgRoamLength { get; set; }
        /* End Display Fields */
        #endregion

        public Machine() { }
        
        #region "INSERTS"
        public static void insMachine(Machine m)
        {
            try
            {
                int location = 0;

                if (m.PCName.Trim().ToLower().StartsWith("vclin")) //VDI
                {
                    string clientName = SysHelper.GetClientName(m.PCName);

                    m.ComputerInfo = "HostName: " + clientName + ", " + m.ComputerInfo;

                    string clientAddress = SysHelper.GetClientIP(m.PCName);

                    m.IpAddress = clientAddress;

                    location = FindLocation(clientAddress);
                }
                else
                {
                    location = FindLocation(m.IpAddress);
                }

                string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

                using (SqlConnection sqlConn = new SqlConnection(config))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.insMachinesWithLoc", sqlConn)  //to do change to use SQL call and cache
                        {
                            CommandType = CommandType.StoredProcedure
                        };

                        sqlcmd.Parameters.AddWithValue("PCName", m.PCName);
                        sqlcmd.Parameters.AddWithValue("LSID", m.LSID);
                        sqlcmd.Parameters.AddWithValue("QLVersion", m.QLVersion);
                        sqlcmd.Parameters.AddWithValue("ICAVersion", m.ICAVersion);
                        sqlcmd.Parameters.AddWithValue("IEVersion", m.IEVersion);
                        sqlcmd.Parameters.AddWithValue("ComputerInfo", m.ComputerInfo);
                        sqlcmd.Parameters.AddWithValue("OSInfo", m.OSInfo);
                        sqlcmd.Parameters.AddWithValue("Processes", m.Processes);
                        sqlcmd.Parameters.AddWithValue("Memory", m.Memory);
                        sqlcmd.Parameters.AddWithValue("IpAddress", m.IpAddress);
                        sqlcmd.Parameters.AddWithValue("AutoInstallVersion", m.AutoInstallVersion);
                        sqlcmd.Parameters.AddWithValue("LocationId", location.ToString());

                        sqlcmd.ExecuteNonQuery();

                    }//end try
                    catch (SqlException se)
                    {
                        // EventLog.WriteEntry("QL", se.Message);
                    }//end catch
                    catch (Exception ee)
                    {
                        // EventLog.WriteEntry("QL", ee.Message);
                    }//end catch

                }//end sql
            }
            catch(Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, "insMachine");
            }
        }

        [Obsolete]
        public static IEnumerable<Machine> GetMachinesByLocationID(int j)
        {
            List<Machine> machines = new List<Machine>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selActiveMachinesByLocationOnly", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("locid", j);

                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Machine mac = new Machine();

                        mac.AutoInstallVersion = "";
                        mac.ComputerInfo = "";
                        mac.Description = sdr["Description"].ToString();
                        mac.ICAVersion = "";
                        mac.id = int.Parse(sdr["id"].ToString());
                        mac.IEVersion = "";
                        mac.IpAddress = sdr["IpAddress"].ToString();
                        mac.LastLogin = null;
                        mac.LocationId = null;
                        mac.LocationTitle = "";
                        mac.LSID = sdr["LSID"].ToString();
                        mac.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        mac.Memory = "";
                        mac.OSInfo = "";
                        mac.PCName = sdr["PCName"].ToString();
                        mac.Processes = "";
                        mac.QLVersion = sdr["QLVersion"].ToString();
                        mac.WTSLocation = "";

                        machines.Add(mac);
                    }//end while

                    sdr.Close();


                }//end try
                catch (SqlException se)
                {
                    // EventLog.WriteEntry("QL", se.Message);
                }//end catch
                catch (Exception ee)
                {
                    // EventLog.WriteEntry("QL", ee.Message);
                }//end catch

            }//end sql

            return machines;
        }

        public static int FindLocation(string IP)
        {
            int locationID = 0;

            try
            {
                // IPAddress ipAddress = IPAddress.Parse(IP);
                char[] splitter = new char[] { '.' };
                string[] splits = IP.Split(splitter);
                string ipthree = splits[0] + "." + splits[1] + "." + splits[3] + ".%"; //check for three octet matches

                locationID = FindLocationID(locationID, ipthree);

                if (locationID == 0) //now try two
                {
                    string iptwo = splits[0] + "." + splits[1] + ".%"; //check for two octet matches
                    locationID = FindLocationID(locationID, iptwo);
                }
            }
            catch (Exception ee)
            {
               // EventLog.WriteEntry("QL", ee.Message);
            }//end catch

            return locationID;
        }

        private static int FindLocationID(int locationID, string ip)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConnection = new SqlConnection(config))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.selLocationIDByIP", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.AddWithValue("IP3", ip);
                    object obj = sqlCommand.ExecuteScalar();
                    locationID = int.Parse(obj.ToString());

                }
                catch (SqlException se)
                {
                    //EventLog.WriteEntry("QL", se.Message);
                }//end catch
                catch (Exception ee)
                {
                   // EventLog.WriteEntry("QL", ee.Message);
                }//end catch
            }
            return locationID;
        }

        public static string FindLocation(string IP, string ex)
        {
            string locationName = "";
            int locationID = 0;

            try
            {
                // IPAddress ipAddress = IPAddress.Parse(IP);
                char[] splitter = new char[] { '.' };
                string[] splits = IP.Split(splitter);
                string ipthree = splits[0] + "." + splits[1] + "." + splits[3] + ".%"; //check for three octet matches

                locationID = FindLocationID(locationID, ipthree);

                if (locationID == 0) //now try two
                {
                    string iptwo = splits[0] + "." + splits[1] + ".%"; //check for two octet matches
                    locationID = FindLocationID(locationID, iptwo);
                }

                if(locationID != 0) //get loc name
                {
                    locationName = FindLocationName(locationID);
                }
            }
            catch (Exception ee)
            {
                // EventLog.WriteEntry("QL", ee.Message);
            }//end catch

            return locationName;
        }

        private static string FindLocationName(int locationID)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            string locName = "";

            using (SqlConnection sqlConnection = new SqlConnection(config))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.selLocationName", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.AddWithValue("ID", locationID);
                    object obj = sqlCommand.ExecuteScalar();

                    if (obj != null)
                    {
                        locName = obj.ToString();
                    }

                }
                catch (SqlException se)
                {
                    //EventLog.WriteEntry("QL", se.Message);
                }//end catch
                catch (Exception ee)
                {
                    // EventLog.WriteEntry("QL", ee.Message);
                }//end catch
            }

            return locName.Trim();
        }

        #endregion

        #region SystemInfo
        public static string GetIcaVersion()
        {
            string result = "";
            string x32Bit = "";
            string x64Bit = "";
            bool isInstalled = false;

            try
            {
                string citrixFileName = @"\Citrix\ICA Client\wfica32.exe";
                x32Bit = string.Format(@"{0}{1}", @"C:\Program Files (x86)", citrixFileName);
                x64Bit = string.Format(@"{0}{1}", @"C:\Program Files", citrixFileName);

                if (File.Exists(x32Bit))
                {
                    citrixFileName = x32Bit;
                    isInstalled = true;
                }

                if (File.Exists(x64Bit))
                {
                    citrixFileName = x64Bit;
                    isInstalled = true;
                }

                if(!isInstalled) //assume Citrix web client
                {
                    string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    if (userName.ToLower().Contains("bh"))
                    {
                        userName = userName.Substring(3);
                    }

                    if (Directory.Exists(@"C:\users\")) //win7 or later, \AppData\Local\Citrix\Citrix online plug-in - web\TrolleyExpress.exe
                    {
                        citrixFileName = "C:\\Users\\" + userName + "\\AppData\\Local\\Citrix\\Citrix online plug-in - web\\TrolleyExpress.exe";
                    }
                    else //assume winxp, \Local Settings\Application Data\Citrix\Citrix online plug-in - web\TrolleyExpress.exe
                    {
                        citrixFileName = "C:\\Documents and Settings\\" + userName + "\\Local Settings\\Application Data\\Citrix\\Citrix online plug -in -web\\TrolleyExpress.exe";
                    }
                }



                result = System.Diagnostics.FileVersionInfo.GetVersionInfo(citrixFileName).ProductVersion;
            }
            catch (Exception e)
            {
                    XMLEntry xmlEntry = new XMLEntry();
                    xmlEntry.timeStamp = DateTime.Now;
                    xmlEntry.eventType = "Get ICA Version - " + e.Message;
                    xmlEntry.ntID = "";
                    xmlEntry.machine = System.Environment.MachineName;
                    xmlEntry.app = "";
                    //xmlEntry.timeElapsed = sw.ElapsedMilliseconds.ToString();
                    Truncus.writeXMLEntry(xmlEntry);

                    result = "N/A - Error";
                }

                return result;
        }

        public static string GetIEVersion()
        {
            try
            {
                return Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Internet Explorer").GetValue("Version").ToString();
            }
            catch { return "N/A"; }
        }

        public static string GetAutoInstallVersion()
        {
            try
            {
                string FileName = @"\Baptist Health\QL Auto Install\QuickLaunch.Install.exe";
                string x32Bit = string.Format(@"{0}{1}", @"C:\Program Files (x86)", FileName);
                string x64Bit = string.Format(@"{0}{1}", @"C:\Program Files", FileName);
                if (System.IO.File.Exists(x32Bit))
                    FileName = x32Bit;
                else if (System.IO.File.Exists(x64Bit))
                    FileName = x64Bit;

                return System.Diagnostics.FileVersionInfo.GetVersionInfo(FileName).ProductVersion;
            }
            catch { return "N/A"; }
        }

        #endregion

    }
}
