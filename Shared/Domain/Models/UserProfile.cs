﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using QuickLaunch.Shared;
using System.Configuration;

namespace QuickLaunch.Models
{
    [Serializable]
    public class UserProfile : Abstract.IDataInfo //moved to WCF
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the ntID.
        /// </summary>
        public string ntID { get; set; }

        /// <summary>
        /// Gets/Sets the user type. ie. Physician, Clinician
        /// </summary>
        public string userType { get; set; }

        /// <summary>
        /// Gets/Sets the user title
        /// </summary>
        public string userTitle { get; set; }

        /// <summary>
        /// Gets/Sets the user dept
        /// </summary>
        public string userDept { get; set; }

        /// <summary>
        /// Gets/Sets the Last login.
        /// </summary>
        public DateTime lastLogin { get; set; }

        /// <summary>
        /// Gets/Sets the last time the portal was launched.
        /// </summary>
        public DateTime lastPortalLaunch { get; set; }

        /// <summary>
        /// Gets/Sets the first time the portal was launched.
        /// </summary>
        public DateTime firstLogin { get; set; }
                
        /// <summary>
        /// Gets/Sets the auto launch app for the EMR.
        /// </summary>
        public string CitrixAutoLaunchApp { get; set; }

        /// <summary>
        /// Gets/Sets the forms Horizontal position Startup location.
        /// </summary>
        public string formHorizontalStartup { get; set; }

        /// <summary>
        /// Gets/Sets the forms Vertical position Startup location.
        /// </summary>
        public string formVerticalStartup { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lastUpdate { get; set; }

        /// <summary>
        /// Gets/Sets the users last login time length.
        /// </summary>
        public double lastLoginLength { get; set; }

        /// <summary>
        /// Gets/Sets the users last roam time length.
        /// </summary>
        public double lastRoamLength { get; set; }

        /// <summary>
        /// Gets/Sets the users last EMR launch time length (not including roams).
        /// </summary>
        public double lastEMRLaunchLength { get; set; }

        /// <summary>
        /// Gets/Sets the users last login machine name
        /// </summary>
        public string lastLoginMachine { get; set; }     

        /// <summary>
        /// Gets/Sets the user roaming count.
        /// </summary>
        public int roamingCt { get; set; }

        /// <summary>
        /// Gets/Sets the PowerChart Launch Ct.
        /// </summary>
        public int pcLaunchCt { get; set; }

        /// <summary>
        /// Gets/Sets the FirstNet Launch Ct.
        /// </summary>
        public int fnLaunchCt { get; set; }

        /// <summary>
        /// Gets/Sets the SurgiNet Launch Ct.
        /// </summary>
        public int snLaunchCt { get; set; }

        /// <summary>
        /// Gets/Sets the last Metric Count Clear
        /// </summary>
        public DateTime lastMetricCtClear { get; set; }


        /* Display Fields */
        /// <summary>
        /// Gets/Sets the Role count
        /// </summary>
        public int RoleCt { get; set; }
                
        /// <summary>
        /// Gets/Sets the Roles
        /// </summary>
        public List<Models.Role> Roles { get; set; }

        /// <summary>
        /// Gets/Sets the Role count
        /// </summary>
        public double Launch_Roam_Avg { get; set; }

        /// <summary>
        /// Gets/Sets the WTS Printer Location.
        /// </summary>
        public string WTSLocation { get; set; }

        public class UserMetrics : Abstract.IDataInfo
        {
            public double AvgLoginLength { get; set; }
            public double SumLoginLength { get; set; }
            public double AvgEMRLaunchLength { get; set; }
            public double SumEMRLaunchLength { get; set; }
            public double AvgRoamLength { get; set; }
            public double SumRoamLength { get; set; }            
            public double AvgRoamLaunchSavings { get; set; }
            public double SumRoamLaunchSavings { get; set; }
            public int SumRoamCt { get; set; }
            public int SumPCLaunchCt { get; set; }
            public int SumFNLaunchCt { get; set; }
            public int SumSNLaunchCt { get; set; }
            public int SumAutoLaunchCt { get; set; }
            public int SumQLUserCt { get; set; }
            public int SumNewUsersCt { get; set; }
            public int SumMachineCt { get; set; }
            public DateTime LastMetricClear { get; set; }     
        }
        /* End Display Fields */
        #endregion

        public UserProfile() { }
        
        #region "SELECTS"

        public static UserProfile selUserProfileById(string ntID)
        {

            UserProfile userProfile = new UserProfile();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selUserProfileByUserName", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("UserName", ntID);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            userProfile.id = Utility.IntParse(sqlDataReader["id"].ToString());
                            userProfile.ntID = sqlDataReader["ntID"].ToString();
                            userProfile.userType = sqlDataReader["userType"].ToString();
                            userProfile.userTitle = sqlDataReader["userTitle"].ToString();
                            userProfile.userTitle = sqlDataReader["userDept"].ToString();
                            userProfile.lastLogin = Utility.DTParse(sqlDataReader["lastLogin"].ToString());
                            userProfile.lastPortalLaunch = Utility.DTParse(sqlDataReader["lastPortalLaunch"].ToString());
                            userProfile.firstLogin = Utility.DTParse(sqlDataReader["firstLogin"].ToString());
                            userProfile.CitrixAutoLaunchApp = sqlDataReader["CitrixAutoLaunchApp"].ToString();
                            userProfile.formHorizontalStartup = sqlDataReader["formHorizontalStartup"].ToString();
                            userProfile.formVerticalStartup = sqlDataReader["formVerticalStartup"].ToString();
                            userProfile.lastLoginMachine = sqlDataReader["lastLoginMachine"].ToString();
                            userProfile.lastLoginLength = Utility.DBParse(sqlDataReader["lastLoginLength"].ToString());
                            userProfile.lastEMRLaunchLength = Utility.DBParse(sqlDataReader["lastEMRLaunchLength"].ToString());
                            userProfile.lastRoamLength = Utility.DBParse(sqlDataReader["lastRoamLength"].ToString());
                            userProfile.roamingCt = Utility.IntParse(sqlDataReader["roamingCt"].ToString());
                            userProfile.pcLaunchCt = Utility.IntParse(sqlDataReader["pcLaunchCt"].ToString());
                            userProfile.fnLaunchCt = Utility.IntParse(sqlDataReader["fnLaunchCt"].ToString());
                            userProfile.snLaunchCt = Utility.IntParse(sqlDataReader["snLaunchCt"].ToString());
                            userProfile.lastMetricCtClear = Utility.DTParse(sqlDataReader["lastMetricCtClear"].ToString());
                            userProfile.lastUpdate = Utility.DTParse(sqlDataReader["lastUpdate"].ToString());
                          //  userProfile.isSizedPortalLaunch

                        }
                        catch (Exception ee)
                        { }
                    }

                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return userProfile;
        }

        public static List<Role> selUserProfileRoles(int ProfileId)
        {

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            List<Role> roles = new List<Role>();

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selUserProfileRoles", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", ProfileId);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            Role role = new Role();
                            role.id = Utility.IntParse(sqlDataReader["id"].ToString());
                            roles.Add(role);

                        }
                        catch (Exception ee)
                        { }
                    }

                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return roles;
        }

        #endregion

        #region "INSERTS"
        public static void insUserProfile(UserProfile up)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insUserProfile", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("NtID", up.ntID);
                    sqlcmd.Parameters.AddWithValue("UserType", up.userType);
                    sqlcmd.Parameters.AddWithValue("UserTitle", up.userTitle);
                    sqlcmd.Parameters.AddWithValue("UserDept", up.userDept);
                    sqlcmd.Parameters.AddWithValue("LastLoginLength", up.lastLoginLength);
                    sqlcmd.Parameters.AddWithValue("LastLogin", up.lastLogin);
                    sqlcmd.Parameters.AddWithValue("LastLoginMachine", up.lastLoginMachine);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    sqlcmd.ExecuteNonQuery();

                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql 


        }
        
        #endregion

        #region "UPDATES"
        public static void updUserProfile(UserProfile up)
        {

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.updUserProfile", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("NtID", up.ntID);
                    sqlcmd.Parameters.AddWithValue("UserType", up.userType);
                    sqlcmd.Parameters.AddWithValue("UserTitle", up.userTitle);
                    sqlcmd.Parameters.AddWithValue("UserDept", up.userDept);
                    sqlcmd.Parameters.AddWithValue("LastLogin", up.lastLogin);
                    sqlcmd.Parameters.AddWithValue("LastLoginLength", up.lastLoginLength);
                    sqlcmd.Parameters.AddWithValue("LastLoginMachine", up.lastLoginMachine);
                 //   sqlcmd.Parameters.AddWithValue("LastUpdate", Date)
                   // SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    sqlcmd.ExecuteNonQuery();

                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }

        public static void updUserProfileLastPortalLaunch(UserProfile up)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.updUserProfileLastPortalLaunch", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("NtID", up.ntID);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    sqlcmd.ExecuteNonQuery();

                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }

        public static void updUserProfileSettings(UserProfile up)
        {

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.updUserProfileSettings", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("NtID", up.ntID);
                    sqlcmd.Parameters.AddWithValue("CitrixAutoLaunchApp", up.CitrixAutoLaunchApp);
                    sqlcmd.Parameters.AddWithValue("formHorizontalStartup", up.formHorizontalStartup);
                    sqlcmd.Parameters.AddWithValue("formVerticalStartup", up.formVerticalStartup);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    sqlcmd.ExecuteNonQuery();

                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }

        public static void updUserProfileRoamingMetrics(string UserId, bool Roaming = false, bool PowerChart = false, bool FirstNet = false, bool SurgiNet = false, bool AllScripts = false, double LastRoamLength = 0, double LastEMRLaunchLength = 0)
        {
            try
            {
                if (!Roaming && !PowerChart && !FirstNet && !SurgiNet && LastRoamLength == 0 && LastEMRLaunchLength == 0)
                {
                    //nothing to do
                }
                else
                {
                    UserProfile up = selUserProfileById(UserId); //get current data

                    if(up !=null)
                    {
                        string sqlFull = "";
                        string sqlStart = @"UPDATE SSO_UserProfile SET ";
                        string sqlMid = "";
                        string sqlEnd = " WHERE [NTID] = '" + up.ntID + "'";

                   
                    
                        if (Roaming && LastRoamLength > 0)
                        {
                            sqlMid = @"[roamingCt] = [roamingCt] + 1, [lastRoamLength] = " + LastRoamLength;
                        }

                        if (PowerChart && LastEMRLaunchLength > 0)
                        {
                            sqlMid = @"[pcLaunchCt] = [pcLaunchCt] + 1, [lastEMRLaunchLength] = " + LastEMRLaunchLength;
                        }

                        if (FirstNet && LastEMRLaunchLength > 0)
                        {
                            sqlMid = @"[fnLaunchCt] = [fnLaunchCt] + 1, [lastEMRLaunchLength] = " + LastEMRLaunchLength;
                        }

                        if (SurgiNet && LastEMRLaunchLength > 0)
                        {
                            sqlMid = @"[snLaunchCt] = [snLaunchCt] + 1, [lastEMRLaunchLength] = " + LastEMRLaunchLength;
                        }

                        //if (AllScripts && LastEMRLaunchLength > 0)
                        //{
                        //    //to do
                        //}

                        if (!string.IsNullOrEmpty(sqlMid))
                        {
                            sqlFull = sqlStart + sqlMid + sqlEnd;

                            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

                            using (SqlConnection sqlConn = new SqlConnection(config))
                            {
                                try
                                {
                                    sqlConn.Open();
                                    SqlCommand sqlcmd = new SqlCommand(sqlFull, sqlConn);  //to do change to use SQL call and cache
                                    sqlcmd.CommandType = CommandType.Text;
                                    sqlcmd.ExecuteNonQuery();
                                }//end try
                                catch (SqlException se)
                                { }//end catch
                                catch (Exception ee)
                                { }//end catch

                            }//end sql
                        }
                    }

                }
                //this is old code, trash it
                //if (!Roaming && !PowerChart && !FirstNet && !SurgiNet && LastRoamLength == 0 && LastEMRLaunchLength == 0)
                //{
                //    // throw new Exception("No Profile Metrics passed [UserProfile.updUserProfileRoamingMetrics]"); //never throw exceptions in production code!
                //}

                //string sql = @"UPDATE SSO_UserProfile SET "
                //               + (Roaming ? " [roamingCt] = [roamingCt] + 1 " : string.Empty)
                //               + (PowerChart ? " [pcLaunchCt] = [pcLaunchCt] + 1 " : string.Empty)
                //               + (FirstNet ? " [fnLaunchCt] = [fnLaunchCt] + 1 " : string.Empty)
                //               + (SurgiNet ? " [snLaunchCt] = [snLaunchCt] + 1 " : string.Empty)
                //               + (LastRoamLength > 0 ? " [lastRoamLength] = @LastRoamLength " : string.Empty)
                //               + (LastEMRLaunchLength > 0 ? " [lastEMRLaunchLength] = @LastEMRLaunchLength " : string.Empty)
                //               + " WHERE [NTID] LIKE @NtId";
                //SqlParameter[] @params = new SqlParameter[5];
                //@params[0] = new SqlParameter("@NtId", UserId);
                //@params[1] = new SqlParameter("@LastRoamLength", LastRoamLength);
                //@params[2] = new SqlParameter("@LastEMRLaunchLength", LastEMRLaunchLength);
                //@params[3] = new SqlParameter("@lastUpdate", DateTime.Now);

                //DBManager.ExecCommandDR(sql, @params, true);
            }
            catch(Exception ee)
            {}
        }

       #endregion


    }
}
