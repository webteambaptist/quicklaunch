﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLaunch.Models
{
    public class MenuImage : Abstract.IDataInfo
    {
        public byte[] ImgFile { get; set; }
        public string ImgName { get; set; }
        public string UpdateDT { get; set; }
        public string UpdateUser { get; set; }

        public static MenuImage getFile(string imgName)
        {
            MenuImage mImage = new MenuImage();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConnection = new SqlConnection(config))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.selMenuImage", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("ImgName", imgName);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        mImage.ImgFile = ((byte[])(sqlDataReader["ImgFile"]));
                        mImage.ImgName = sqlDataReader["ImgName"].ToString();
                        mImage.UpdateDT = sqlDataReader["UpdateDT"].ToString();
                        mImage.UpdateUser = sqlDataReader["UpdateUser"].ToString();
                    }
                    sqlDataReader.Close();

                }
                catch (SqlException sqlException)
                {
                    //success = false;
                }
                catch (Exception exception)
                {
                    //success = false;
                }

            }

            return mImage;
        }

        public static bool saveFile(byte[] imageArray, string fileName)
        {
            bool success = true;

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConnection = new SqlConnection(config))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.insMenuImage", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("ImgFile", imageArray);
                    sqlCommand.Parameters.AddWithValue("ImgName", fileName);
                    sqlCommand.Parameters.AddWithValue("UpdateDT", DateTime.Now.ToString());
                    sqlCommand.Parameters.AddWithValue("UpdateUser", "Admin");
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

                    sqlDataReader.Close();

                }
                catch (SqlException sqlException)
                {
                    success = false;
                }
                catch (Exception exception)
                {
                    success = false;
                }

            }

            return success;
        }
    }
}
