﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;
using System.Linq;
using QuickLaunch.Shared;
using System.Configuration;
using System.Runtime.Serialization.Formatters.Binary;
using QuickLaunch.Shared.Domain.Models;

namespace QuickLaunch.Models
{
    [Serializable]
    public class App : Abstract.IDataInfo  //properties copied to WCF, renamed to App to avoid confusion with .Net object
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        public int LaunchCt { get; set; }

        public DateTime LastLaunch { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the Value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets/Sets the Window Title.
        /// </summary>
        public string WindowTitle { get; set; }

        /// <summary>
        /// Gets/Sets the Allowing Roaming
        /// </summary>
        public bool AllowRoaming { get; set; }

        /// <summary>
        /// Gets/Sets the default img
        /// </summary>
        public string DefaultImg { get; set; }
        
        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

 
        /* Display Fields */

        /// <summary>
        /// Gets/Sets the display value for the title/value
        /// </summary>
        public string TitleValueDisplay { get; set; }

        /* End Display Fields */

        /// <summary>
        /// Gets/Sets Citrix Applications.
        /// Default list for offline mode : gets overriden on QL initialize
        /// </summary>        
        public static List<App> CitrixApplications = new List<App> {  
                    new App { id = 100, Title = "Power Chart", Value = "EMR 1024x768 - SSOI", WindowTitle = "PowerChart", AllowRoaming = true, DefaultImg = "powerchart"},
                    new App { id = 101, Title = "Power Chart", Value = "EMR 1024x768 - SP", WindowTitle = "PowerChart", AllowRoaming = true, DefaultImg = "powerchart"},
                    new App { id = 102, Title = "Power Chart", Value = "EMR Imprivata 4-6 SSO Test", WindowTitle = "PowerChart", AllowRoaming = true, DefaultImg = "powerchart"},
                    new App { id = 103, Title = "FirstNet", Value = "Firstnet - SSOI", WindowTitle = "FirstNet", AllowRoaming = true, DefaultImg = "firstnet"},
                    new App { id = 104, Title = "FirstNet", Value = "Firstnet - SP", WindowTitle = "FirstNet", AllowRoaming = true, DefaultImg = "firstnet"},
                    new App { id = 105, Title = "SurgiNet", Value = "Surginet - SSOI", WindowTitle = "SNSurgiNet", AllowRoaming = true, DefaultImg = "surginet"},
                    new App { id = 106, Title = "SurgiNet", Value = "Surginet - SP", WindowTitle = "SNSurgiNet", AllowRoaming = true, DefaultImg = "surginet"}
        };

        public static DateTime LastCitrixApplicationsUpdate { get; set; }
        #endregion

        public App() { }

        public static void LoadCitrixApplications()
        {
            if (LastCitrixApplicationsUpdate < DateTime.Now.AddHours(-1)) //only check once an hour
            {
                Stopwatch watch = new Stopwatch();
                watch.Start();
                string eventType = "Load Citrix Applications";

                List<App> DbApps = new List<App>();
                string xmlFileName = Utility.getPath() + "QL_Apps.xml";

                if (Utility.checkCache(xmlFileName, "QL_APPS"))
                {
                    DbApps= selApplications();
                    saveApplicationsCache(DbApps);
                }
                else
                {
                    DbApps = selApplicationsCache();
                    eventType = "Load Citrix Applications Cache";
                }


                if (DbApps.Count > 0)
                {
                    CitrixApplications.Clear();
                    CitrixApplications.AddRange(DbApps);
                    LastCitrixApplicationsUpdate = DateTime.Now;
                }
                
                watch.Stop();

                //now save updated cache              


                try
                {
                    string logLevel = Utility.getSettingFromCache("LogLevel");

                    if (string.IsNullOrEmpty(logLevel))
                    {
                        logLevel = "1";
                    }

                    if ((logLevel == "2") || (logLevel) == "3") //only log if more thorough diagnostics turned on
                    {
                        XMLEntry xmlEntry = new XMLEntry();
                        xmlEntry.timeStamp = DateTime.Now;
                        xmlEntry.eventType = eventType;
                        xmlEntry.ntID = "";
                        xmlEntry.machine = System.Environment.MachineName;
                        xmlEntry.app = "";
                        xmlEntry.timeElapsed = watch.ElapsedMilliseconds.ToString();
                        Truncus.writeXMLEntry(xmlEntry);
                    }
                }
                catch (Exception ee) { }

            }
        }

        /// <summary>
        /// saves application list to cache
        /// </summary>
        /// <param name="DbApps"></param>
        private static void saveApplicationsCache(List<App> DbApps)
        {
            try
            {
                string xmlFileName = Utility.getPath() + "QL_Apps.xml";

                foreach (App a in DbApps)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("Applications").Add(
                           new XElement("Application",
                                    new XElement("id", a.id),
                                    new XElement("launchCt", a.LaunchCt),
                                    new XElement("lastLaunch", a.LastLaunch),
                                    new XElement("title", a.Title),
                                    new XElement("value", a.Value),
                                    new XElement("WindowTitle", a.WindowTitle),
                                    new XElement("AllowRoaming", a.AllowRoaming),
                                    new XElement("DefaultImg", a.DefaultImg),
                                    new XElement("lup_user", a.lup_user),
                                    new XElement("lup_dt", a.lup_dt),
                                    new XElement("TitleValueDisplay", a.TitleValueDisplay)
                                    ));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("Application List for QL"),
                            new XElement("Applications",
                                new XElement("Application",
                                    new XElement("id", a.id),
                                    new XElement("launchCt", a.LaunchCt),
                                    new XElement("lastLaunch", a.LastLaunch),
                                    new XElement("title", a.Title),
                                    new XElement("value", a.Value),
                                    new XElement("WindowTitle", a.WindowTitle),
                                    new XElement("AllowRoaming", a.AllowRoaming),
                                    new XElement("DefaultImg", a.DefaultImg),
                                    new XElement("lup_user", a.lup_user),
                                    new XElement("lup_dt", a.lup_dt),
                                    new XElement("TitleValueDisplay", a.TitleValueDisplay)))
                            );
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch (Exception ee)
            {

            }
        }

        /// <summary>
        /// returns cached applications list
        /// </summary>
        /// <returns></returns>
        private static List<App> selApplicationsCache()
        {
            List<App> apps = new List<App>();
            try
            {
                string xmlFileName = Utility.getPath() + "QL_Apps.xml";

                if (File.Exists(xmlFileName))
                {
                    XDocument xmlDoc = XDocument.Load(xmlFileName);
                    var appData = from app in xmlDoc.Descendants("Application")
                                  select new App()
                                  {
                                      id = int.Parse(app.Element("id").Value.ToString()),
                                      LaunchCt = int.Parse(app.Element("launchCt").Value.ToString()),
                                      LastLaunch = DateTime.Parse(app.Element("lastLaunch").Value.ToString()),
                                      Title = app.Element("title").Value.ToString(),
                                      Value = app.Element("value").Value.ToString(),
                                      WindowTitle = app.Element("WindowTitle").Value.ToString(),
                                      AllowRoaming = bool.Parse(app.Element("AllowRoaming").Value.ToString()),
                                      DefaultImg = app.Element("DefaultImg").Value.ToString(),
                                      lup_user = app.Element("lup_user").Value.ToString(),
                                      lup_dt = DateTime.Parse(app.Element("lup_dt").Value.ToString()),
                                      TitleValueDisplay = app.Element("TitleValueDisplay").Value.ToString()
                                  };

                    foreach(App a in appData)
                    {
                        apps.Add(a);
                    }
                }

                return apps;
            }
            catch(Exception ee)
            {
                //onerror, load from DB
                apps = selApplications();

                return apps;
            }

        }

        /// <summary>
        /// check for QL_Apps.xml and delete if stale
        /// </summary>
        /// <returns></returns>
        private static bool checkCacheApplications()
        {
            bool isOld = true;
            try
            {
                List<CacheRefresh> cList = CacheRefresh.selCacheRefresh();
                string xmlFileName = Utility.getPath() + "QL_Apps.xml";

                if (File.Exists(xmlFileName))
                {

                    string output = (from x in cList where x.CacheName.ToUpper() == "QL_Apps.xml" select x.RefreshInHours).First();
                    double time = double.Parse(output);
                    //double time = Utility.retHour();
                    if (File.GetLastWriteTime(xmlFileName) < DateTime.Now.AddHours(time * -1)) //if the file is older than 24 hours
                    {
                        try
                        {
                            File.Delete(xmlFileName);
                            isOld = true;
                        }
                        catch (IOException ie)
                        {
                            isOld = true;
                        }
                    }
                    else
                    {
                        isOld = false; //if the file exists and is not stale, use it
                    }
                }
            }
            catch (IOException ie)
            {
                isOld = true; //return true to delete and refresh
            }
            return isOld;
        }

        public static void refreshCache()
        {
            List<App> DbApps = new List<App>();
            DbApps = selApplications();
            saveApplicationsCache(DbApps);
        }
        

        #region "SELECTS"

        public static List<App> selApplications()
        {
//            string sql = @"SELECT app.*, (app.Title + ' (' + app.Value + ')') as TitleValueDisplay FROM SSO_Application app                                 
//                           order by title";
//            SqlParameter[] @params = new SqlParameter[1];

//            List<App> apps = BOHelper.FillCollection<App>(DBManager.ExecCommandDR(sql, @params));

            List<App> apps = new List<App>();
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selApplications", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();

                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            App a = new App()
                            {
                                id = int.Parse(sqlDataReader["id"].ToString()),
                                Title = sqlDataReader["Title"].ToString(),
                                Value = sqlDataReader["Value"].ToString(),
                                WindowTitle = sqlDataReader["WindowTitle"].ToString(),
                                AllowRoaming = bool.Parse(sqlDataReader["AllowRoaming"].ToString()),
                                DefaultImg = sqlDataReader["DefaultImg"].ToString(),
                                lup_user = sqlDataReader["lup_user"].ToString(),
                                lup_dt = DateTime.Now, //placeholder data
                                TitleValueDisplay = sqlDataReader["TitleValueDisplay"].ToString(),
                                LastLaunch = DateTime.Now, //placeholder data,
                                LaunchCt = 0
                            };

                            apps.Add(a);
                        }
                        catch (Exception ee)
                        { }
                    }
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch
            }//end sql


            return apps;
        }

        #endregion


    }
}
