﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLaunch.Models
{
    public class UserFarm
    {
        public int id { get; set; }

        public string ntID { get; set; }

        public string farmName { get; set; }

        public DateTime lup_dt { get; set; }

        public static void clearUserList(string ntID) //[dbo].[delUserFarm]
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.delUserFarm", sqlConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.AddWithValue("ntID", ntID);
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
        }

        public static List<XenAppInfo> getUserFarm(string ntID) //[dbo].[selFarmsByUser]
        {
            bool isDebug = false;

            try
            {
                string debug = ConfigurationManager.AppSettings["IsDebug"];


                if (!string.IsNullOrEmpty(debug))
                {
                    if (debug == "2")
                    {
                        isDebug = true;
                    }
                }
            }
            catch(Exception ee)
            { }

            List<XenAppInfo> xList = new List<XenAppInfo>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = sqlConn;

                    if(isDebug)
                    {
                        sqlCommand.CommandText = "dbo.selFarmsByUserTest";
                    }
                    else
                    {
                        sqlCommand.CommandText = "dbo.selFarmsByUser";
                    }

                    //SqlCommand sqlCommand = new SqlCommand("dbo.selFarmsByUser", sqlConn)
                    //{
                    //    CommandType = CommandType.StoredProcedure
                    //};

                    sqlCommand.Parameters.AddWithValue("ntID", ntID);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        XenAppInfo xenAppInfo = new XenAppInfo()
                        {
                            Farm = sqlDataReader["farmName"].ToString(),
                            XenAppHost1 = sqlDataReader["XenAppHost1"].ToString(),
                            XenAppHost2 = sqlDataReader["XenAppHost2"].ToString(),
                            XenAppPort = sqlDataReader["XenAppPort"].ToString(),
                        };
                        xList.Add(xenAppInfo);
                    }
                    sqlDataReader.Close();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
            return xList;
        }

        public static void setUserFarm(List<UserFarm> farmList) //[dbo].[upsUserFarm]
        {
            foreach(UserFarm farm in farmList)
            {
                SaveFarm(farm);
            }
        }

        private static void SaveFarm(UserFarm farm)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.upsUserFarm", sqlConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    sqlCommand.Parameters.AddWithValue("ntID", farm.ntID);
                    sqlCommand.Parameters.AddWithValue("farmName", farm.farmName);
                    sqlCommand.ExecuteNonQuery();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
        }

        public UserFarm()
        {

        }

        
        public static List<XenAppInfo> getFarmsDirect()
        {
            List<XenAppInfo> xList = new List<XenAppInfo>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "dbo.GetFarmsTest";

                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        XenAppInfo xenAppInfo = new XenAppInfo()
                        {
                            Farm = sqlDataReader["farmName"].ToString(),
                            XenAppHost1 = sqlDataReader["XenAppHost1"].ToString(),
                            XenAppHost2 = sqlDataReader["XenAppHost2"].ToString(),
                            XenAppPort = sqlDataReader["XenAppPort"].ToString(),
                        };
                        xList.Add(xenAppInfo);
                    }
                    sqlDataReader.Close();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
            return xList;
        }
    }
}
