﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLaunch.Models
{
    /* Shared Model Functions/Properties */
    public class BaseModel //moved to WCF
    {
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion
    }
}
