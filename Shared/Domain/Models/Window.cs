﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class Window : BaseModel, Abstract.IDataInfo //moved to WCF
    {

        #region Properties
        /// <summary>
        /// Gets/Sets the action
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Gets/Sets the process
        /// </summary>
        public string Process { get; set; }

        /// <summary>
        /// Gets/Sets the WindowText
        /// </summary>
        public string WindowText { get; set; }

        /// <summary>
        /// Gets/Sets the is all machines indicator
        /// </summary>
        public bool IsEverywhere { get; set; }

        /// <summary>
        /// Gets/Sets the rule enablement
        /// </summary>
        public bool IsEnabled { get; set; }

        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the applications
        /// </summary>
        public List<string> Applications { get; set; }
        #endregion

        #region Kill Window Summary
        public static List<Window> KillWindowSummary = new List<Window>();        
        public static DateTime LastWindowSummaryUpdate { get; set; }
        public static DateTime LastExclusionWindowSummaryUpdate { get; set; }

        public static List<string> ProcsToKill = new List<string>();
        public static List<Models.Window> AppsToKill = new List<Models.Window>();
        public static List<Models.Window> ExceptionList = new List<Models.Window>();

                
        public static void LoadKillWindowSummary(){
            //if (LastWindowSummaryUpdate < DateTime.Now.AddDays(-1)) //only check once a day
            //    return;
              
            List<Window> DbWins = new List<Window>{ //default list
                                             new Window { Action = EventAction.Include.ToString("F"), Process = "iexplore", WindowText = string.Empty, IsEverywhere = true, IsEnabled = true }
                                            ,new Window { Action = EventAction.Include.ToString("F"), Process = "firefox", WindowText = string.Empty, IsEverywhere = true, IsEnabled = true }
                                            ,new Window { Action = EventAction.Include.ToString("F"), Process = "notepad", WindowText = string.Empty, IsEverywhere = true, IsEnabled = true }
                                            ,new Window { Action = EventAction.Include.ToString("F"), Process = "cmd", WindowText = string.Empty, IsEverywhere = true, IsEnabled = true }
                                        };
            try
            {
                DbWins = selWindowsByDevice(System.Environment.MachineName);                
            } catch { } //offline mode
            if (DbWins.Count > 0)
            {
                KillWindowSummary.Clear();
                KillWindowSummary.AddRange(DbWins);
                LastWindowSummaryUpdate = DateTime.Now;
            }

            ProcsToKill.Clear(); AppsToKill.Clear(); ExceptionList.Clear();
            Models.Window.GetInclusions(); //loads procs and apps to kill
            Models.Window.GetExclusions(); //loads exceptions and removes uneeded inclusions
            
        }
        public static void GetInclusions()
        {         
            foreach (Models.Window w in KillWindowSummary.FindAll(w => w.Action == EventAction.Include.ToString("F")))
            {
                if (!ProcsToKill.Contains(w.Process) && string.IsNullOrEmpty(w.WindowText))
                    ProcsToKill.Add(w.Process);
                else if (!string.IsNullOrEmpty(w.WindowText))
                {
                    Models.Window killapp = AppsToKill.Find(a => a.WindowText == w.WindowText);
                    if (killapp != null)
                        AppsToKill.Add(w);
                }
            }
        }
        public static void GetExclusions()
        {         
            foreach (Models.Window w in KillWindowSummary.FindAll(w => w.Action == EventAction.Exclude.ToString("F")))
            {
                if (ProcsToKill.Contains(w.Process) && string.IsNullOrEmpty(w.WindowText))
                {
                    ProcsToKill.Remove(w.Process);
                    continue;
                }
                if (!string.IsNullOrEmpty(w.WindowText))
                {
                    Models.Window killapp = AppsToKill.Find(a => a.WindowText == w.WindowText);
                    if (killapp != null)
                        AppsToKill.Remove(killapp);
                }

                if (!string.IsNullOrEmpty(w.WindowText))
                {
                    Models.Window proc = ExceptionList.Find(e => e.Process == w.Process);
                    if (proc == null)
                    {
                        Models.Window hl = new Models.Window { Process = w.Process };
                        hl.Applications.Add(w.WindowText);
                        ExceptionList.Add(hl);
                        if (ProcsToKill.Contains(w.Process))
                            ProcsToKill.Remove(w.Process);
                    }
                    else
                        proc.Applications.Add(w.WindowText);
                }
            }            
        }
        public static bool IsAppContainedInWindow(List<string> apps, string title, string url = "_")
        {
            bool IsWindowOpen = false;
            for (int s = 0; s < apps.Count; s++) //check all exclusion apps for process
                if (title.StartsWith(apps[s], StringComparison.CurrentCultureIgnoreCase) || url.StartsWith(apps[s], StringComparison.CurrentCultureIgnoreCase))
                    IsWindowOpen = true; //there is an exclusion app matching this process and window title/url
            return IsWindowOpen;
        }
        #endregion

        #region Enums
        /// <summary>
        /// Event action
        /// @Exclude - Prevent process from closing
        /// @Include - Forcess process to close
        /// </summary>
        public enum EventAction { Exclude, Include }
        #endregion
             
        #region "SELECTS"
        public static Window selWindowById(int id)
        {
            string sql = "";
            sql = "SELECT * FROM SSO_Window WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<Window>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<Window> selWindows()
        {
            string sql = "SELECT v.* FROM SSO_Window v order by v.IsEnabled DESC, v.[Action] DESC, v.[Process], v.[WindowText]";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Window>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Window> selWindowsByDevice(string device)
        {
            string sql = @"SELECT v.* FROM SSO_Window v 
                              where IsEnabled = 1 AND ([IsEverywhere] = 1 OR 0 < (SELECT count(*) FROM SSO_WindowDevice wd WHERE wd.machine LIKE @Device AND WindowId = v.id))
                              order by [Action], [Process]";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@Device", device);
            return BOHelper.FillCollection<Window>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<WindowDevice> selWindowDevices(int WindowId)
        {
            string sql = "SELECT v.* FROM SSO_WindowDevice v WHERE [WindowId] = @WindowId order by v.id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@WindowId", WindowId);
            return BOHelper.FillCollection<WindowDevice>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insWindow(Window Window)
        {
            string sql = @"INSERT INTO SSO_Window ([Action], [Process], [WindowText], [IsEverywhere], [IsEnabled], [lup_user]) VALUES (@Action, @Process, @WindowText, @IsEverywhere, @IsEnabled, @lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[7];
            @params[0] = new SqlParameter("@Action", Window.Action);
            @params[1] = new SqlParameter("@Process", Window.Process);
            @params[2] = new SqlParameter("@WindowText", Window.WindowText);
            @params[3] = new SqlParameter("@IsEverywhere", Window.IsEverywhere);
            @params[4] = new SqlParameter("@IsEnabled", Window.IsEnabled);
            @params[5] = new SqlParameter("@lup_user", Window.lup_user);            

            return BOHelper.FillObject<Window>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static string insWindowDevice(WindowDevice device)
        {
            string sql = @"INSERT INTO SSO_WindowDevice ([WindowId], [Machine]) VALUES (@WindowId, @Machine); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@WindowId", device.WindowId);
            @params[1] = new SqlParameter("@Machine", device.Machine);           
            
            return BOHelper.FillObject<WindowDevice>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updWindow(Window Window)
        {
            string sql = @"UPDATE SSO_Window SET [Action] = @Action, [Process] = @Process, [WindowText] = @WindowText, [IsEverywhere] = @IsEverywhere, [IsEnabled] = @IsEnabled, [lup_user] = @lup_user, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[8];
            @params[0] = new SqlParameter("@Action", Window.Action);
            @params[1] = new SqlParameter("@Process", Window.Process);
            @params[2] = new SqlParameter("@WindowText", Window.WindowText);
            @params[3] = new SqlParameter("@IsEverywhere", Window.IsEverywhere);
            @params[4] = new SqlParameter("@IsEnabled", Window.IsEnabled);
            @params[5] = new SqlParameter("@lup_user", Window.lup_user);               
            @params[6] = new SqlParameter("@lup_dt", Window.lup_dt);
            @params[7] = new SqlParameter("@id", Window.id);
            
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delWindow(Window Window)
        {
            string sql = @"DELETE FROM SSO_WindowDevice WHERE [WindowId] = @id; DELETE FROM SSO_Window WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", Window.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delWindowDevice(WindowDevice device)
        {
            string sql = @"DELETE FROM SSO_WindowDevice WHERE [Id] = @id;";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", device.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
        
        #region Inner Classes
        public class WindowDevice : BaseModel, Abstract.IDataInfo
        {
            public int WindowId { get; set; }
            public string Machine { get; set; }
        }
        #endregion

        public Window() { Applications = new List<string>(); }
    }
}
