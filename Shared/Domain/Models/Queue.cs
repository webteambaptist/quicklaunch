﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace QuickLaunch.Models
{
    public class AppQueue : Abstract.IDataInfo
    {
        #region properties

        public int ID { get; set; }

        public string NTID { get; set; }

        public DateTime lastUpdate { get; set; }

        public string machine { get; set; }

        public string ipAddress { get; set; }

        public string impStatus { get; set; }

        public string userEvent { get; set; }

        public string app { get; set; }

        public AppQueue() { }

        #endregion

        #region workstationLogic

        public void insNewUser(AppQueue childQ)
        {
            //called from updUserChildEntry insMasterQ
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insMasterQ", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //  //sqlcmd.Connection.Open();
                    sqlcmd.Parameters.AddWithValue("NTID", childQ.NTID);
                    sqlcmd.ExecuteScalar();

                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }
        
        /// <summary>
        /// update, only for workstation locking
        /// </summary>
        /// <param name="childQ"></param>
        private void updChildEntry(AppQueue childQ)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.updChildQ", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", childQ.ID);
                    sqlcmd.Parameters.AddWithValue("impStatus", childQ.impStatus);
                    sqlcmd.ExecuteScalar();
                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }

        public void LogQueueEvent(string user, string impstatus)
        {
            AppQueue queue = new AppQueue
            {
                ID = 0,
                NTID = user,
                lastUpdate = DateTime.Now,
                machine = System.Environment.MachineName,
                ipAddress = QuickLaunch.Shared.SysHelper.GetIpAddress(),
                impStatus = impStatus,
                userEvent = "",
                app = ""
            };

            updUserChildEntry(queue); //to do some sort of caching
        }

        /// <summary>
        /// update entries related to workstation locking
        /// </summary>
        /// <param name="childQ"></param>
        public void updUserChildEntry(AppQueue childQ)
        {
            insNewUser(childQ);

            List<AppQueue> childList = selChildren(childQ.NTID);
            List<AppQueue> machList = (from q in childList where q.machine.ToLower() == childQ.machine.ToLower() select q).ToList();

            //add logic to check to see if current machine has an entry, if not, insert
            if ((machList.Count > 0) && (string.IsNullOrEmpty(childQ.app))) //use only for tracking workstation locks
            {
                try
                {
                    //update updChildQ where its the current user, machine, and date
                    AppQueue queue = (from q in childList where (q.NTID.ToLower() == childQ.NTID.ToLower()) && (q.machine.ToLower() == childQ.machine.ToLower()) && (q.lastUpdate.Date == DateTime.Today) select q).FirstOrDefault();

                    if (string.IsNullOrEmpty(childQ.impStatus))
                    {
                        childQ.impStatus = queue.impStatus;
                    }

                    if (queue != null)
                    {
                        childQ.ID = queue.ID;
                        updChildEntry(childQ);
                    }
                    else
                    {
                        insChildEntry(childQ);
                    }
                }
                catch(Exception ee)
                {
                    insChildEntry(childQ);
                }
            }
            else
            {
                //insert insChildQ
                insChildEntry(childQ);
            }
        }

        /// <summary>
        /// controls locking of all machines except for the current one for the user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="imprivataEvent"></param>
        /// <returns></returns>
        public void LogQueueEvent(Shared.UserInfo user, Shared.ImprivataEvent imprivataEvent)
        {

            AppQueue queue = new AppQueue
            {
                ID = 0,
                NTID = user.UserName,
                lastUpdate = DateTime.Now,
                machine = System.Environment.MachineName,
                ipAddress = QuickLaunch.Shared.SysHelper.GetIpAddress(),
                impStatus = imprivataEvent.ToString(),
                userEvent = "",
                app = ""
            };

            updUserChildEntry(queue); //to do some sort of caching

            //now get list and lock
        }
        #endregion

        /// <summary>
        /// get list of all records from today for a user, use for both locking and queueing
        /// </summary>
        /// <param name="NTID"></param>
        /// <returns></returns>
        public List<AppQueue> selChildren(string NTID)
        {
            List<AppQueue> cList = new List<AppQueue>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selChildQ", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("NTID", NTID);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        AppQueue b = new AppQueue();
                        b.ID = IntParse(sdr["id"].ToString());
                        b.NTID = sdr["NtId"].ToString();
                        b.lastUpdate = DTParse(sdr["lastUpdate"].ToString());
                        b.machine = sdr["machine"].ToString();
                        b.ipAddress = sdr["ipAddress"].ToString();
                        b.impStatus = sdr["impStatus"].ToString();
                        b.userEvent = sdr["userEvent"].ToString();
                        b.app = sdr["app"].ToString();
                        cList.Add(b);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return cList;
        }

        /// <summary>
        /// insert new record, use for both locking and queueing
        /// </summary>
        /// <param name="childQ"></param>
        private void insChildEntry(AppQueue childQ)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insChildQ", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Connection.Open();
                    sqlcmd.Parameters.AddWithValue("NTID", childQ.NTID);
                    sqlcmd.Parameters.AddWithValue("machine", childQ.machine);
                    sqlcmd.Parameters.AddWithValue("ipAddress", childQ.ipAddress);
                    sqlcmd.Parameters.AddWithValue("impStatus", childQ.impStatus);
                    sqlcmd.Parameters.AddWithValue("userEvent", childQ.userEvent);
                    sqlcmd.Parameters.AddWithValue("app", childQ.app);
                    sqlcmd.ExecuteScalar();
                    //user.UserName
                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql
        }

        /// <summary>
        /// Checks to see if a launch or roam is in process
        /// </summary>
        /// <param name="CurrentUser"></param>
        /// <returns></returns>
        public bool LogQueueEvent(UserInfo user, string userEvent, string appName)
        {
            bool isInProcess = false;

            isInProcess = checkLaunchStatus(user.UserName, userEvent, appName);

            if (!isInProcess)
            {
                AppQueue queue = new AppQueue
                {
                    ID = 0,
                    NTID = user.UserName,
                    lastUpdate = DateTime.Now,
                    machine = System.Environment.MachineName,
                    ipAddress = QuickLaunch.Shared.SysHelper.GetIpAddress(),
                    impStatus = "",
                    userEvent = "LAUNCH-START",
                    app = appName
                };

                insChildEntry(queue);
                XMLEntry xmlEntry = new XMLEntry();
                xmlEntry.timeStamp = DateTime.Now;
                xmlEntry.eventType = "LAUNCH-START";
                xmlEntry.ntID = user.UserName;
                xmlEntry.machine = System.Environment.MachineName;
                xmlEntry.app = appName;
                xmlEntry.timeElapsed = "0";
                Truncus.writeXMLEntry(xmlEntry);
            }

            return isInProcess;
        }

        /// <summary>
        /// check table to see if a launch already exists for a given user, machine, and app
        /// </summary>
        /// <param name="NTID"></param>
        /// <param name="userEvent"></param>
        /// <param name="appName"></param>
        /// <returns></returns>
        private bool checkLaunchStatus(string NTID, string userEvent, string appName)
        {
            bool launchExists = false;
            List<AppQueue> qList = new List<AppQueue>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selLaunchQ", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("NTID", NTID);
                    sqlcmd.Parameters.AddWithValue("userEvent", userEvent);
                    sqlcmd.Parameters.AddWithValue("app", appName);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        AppQueue b = new AppQueue();
                        b.ID = IntParse(sdr["id"].ToString());
                        b.NTID = sdr["NtId"].ToString();
                        b.lastUpdate = DTParse(sdr["lastUpdate"].ToString());
                        b.machine = sdr["machine"].ToString();
                        b.ipAddress = sdr["ipAddress"].ToString();
                        b.impStatus = sdr["impStatus"].ToString();
                        b.userEvent = sdr["userEvent"].ToString();
                        b.app = sdr["app"].ToString();
                        qList.Add(b);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                {
                    launchExists = false;
                }//end catch
                catch (Exception ee)
                {
                    launchExists = false;
                }//end catch

            }//end sql

            if(qList.Count > 0)
            {
                launchExists = true;
            }
            else
            {
                launchExists = false;
            }            

            return launchExists;
        }

        /// <summary>
        /// Finds closed app and clears record
        /// </summary>
        /// <param name="CurrentUser"></param>
        /// <param name="procsList"></param>
        /// <returns></returns>
        public bool ResetLaunch(UserInfo CurrentUser, List<string> procsList)
        {
            try
            {
                List<AppQueue> appList = selChildren(CurrentUser.UserName);
                List<AppQueue> appQ = (from q in appList where !procsList.Contains(q.app) select q).ToList();
                foreach (AppQueue aq in appQ)
                {
                    if (!string.IsNullOrEmpty(aq.app))
                    {
                        ClearLaunch(CurrentUser.UserName, aq.app);
                    }
                }
            }
            catch(Exception ee)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// clears all launches for current user, machines, and apps
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        public bool ClearLaunches(string userName)
        {
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.updChildQClearAll", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("NTID", userName);
                    sqlcmd.Parameters.AddWithValue("machine", Environment.MachineName);
                    sqlcmd.ExecuteNonQuery();
                }//end try

                catch (SqlException se)
                {
                    return false;
                }//end catch
                catch (Exception ee)
                {
                    return false;
                }//end catch

            }//end sql
            return true;
        }

        /// <summary>
        /// clears launch for current user, machine, and one app
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="app"></param>
        /// <returns></returns>
        public bool ClearLaunch(string userName, string app)
        {
            //updChildQClearApp
            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.updChildQClearApp", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("NTID", userName);
                    sqlcmd.Parameters.AddWithValue("machine", Environment.MachineName);
                    sqlcmd.Parameters.AddWithValue("app", app);
                    sqlcmd.ExecuteNonQuery();
                }//end try

                catch (SqlException se)
                {
                    return false;
                }//end catch
                catch (Exception ee)
                {
                    return false;
                }//end catch

            }//end sql
            return true;
        }

        #region util
        //simple methods for parsing and error handling

        private static double DBParse(string input)
        {
            try
            {
                return double.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        private static bool BoolParse(string input)
        {
            try
            {
                return bool.Parse(input);
            }
            catch
            {
                return false;
            }
        }

        private static int IntParse(string input)
        {
            try
            {
                return int.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        private static DateTime DTParse(string input)
        {
            try
            {
                return DateTime.Parse(input);
            }
            catch
            {
                return DateTime.Now;
            }
        }

        #endregion
    }


}
