﻿namespace QuickLaunch.Shared
{
    using System;
    using System.Security.Permissions;
    using System.Collections.Generic;
    using System.Security;

    [Serializable]
    public class UserInfo //not moved to WCF no connection to DB
    {
        private string deviceId = "QL" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8);

        /// <summary>
        /// Gets the device ID.
        /// </summary>
        public string DeviceID { get { return deviceId; } }

        /// <summary>
        /// Gets or sets the name of the user.
        /// </summary>
        /// <value>
        /// The name of the user.
        /// </value>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        /// <value>
        /// The password.
        /// </value>
        public string Password { get; set; }
      //  public SecureString Password { get; set; } //to do find workaround for serialization

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>
        /// The domain.
        /// </value>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets the type of the user.
        /// </summary>
        /// <value>
        /// The type of the user.
        /// </value>
        public string UserType { get; set; }

        ///// <summary>
        ///// Gets or sets the xen app host.
        ///// </summary>
        ///// <value>
        ///// The xen app host.
        ///// </value>
        //public string XenAppHost1 { get; set; }

        ///// <summary>
        ///// Gets or sets the xen app host2.
        ///// </summary>
        ///// <value>
        ///// The xen app host2.
        ///// </value>
        //public string XenAppHost2 { get; set; }

        ///// <summary>
        ///// Gets or sets the xen app port.
        ///// </summary>
        ///// <value>
        ///// The xen app port.
        ///// </value>
        //public string XenAppPort { get; set; }

        public List<XenAppInfo> XenAppInfo { get; set; }



        /// <summary>
        /// Gets or sets the application info.
        /// </summary>
        /// <value>
        /// The application info.
        /// </value>
        public Dictionary<string, string> ApplicationInfo { get; set; }

        /// <summary>
        /// Gets or sets the Ad groups
        /// </summary>
        public List<string> ADgroups { get; set; }

        /// <summary>
        /// Gets or sets the user active directory title
        /// </summary>
        public string UserAdTitle { get; set; }

        /// <summary>
        /// Gets or sets the user active directory department
        /// </summary>
        public string UserAdDept { get; set; }

        /// <summary>
        /// Gets or sets the Current Profile info.
        /// </summary>
        /// <value>
        /// The Current database user Profile info.
        /// </value>
        public Models.UserProfile CurrentProfile { get; set; }


        public UserInfo() { ADgroups = new List<string>(); }


    }

    [Serializable]
    public class XenAppInfo
    {
        public string XenAppHost1 { get; set; }
        public string XenAppHost2 { get; set; }
        public string XenAppPort { get; set; }
        public string Farm { get; set; }
    }
}
