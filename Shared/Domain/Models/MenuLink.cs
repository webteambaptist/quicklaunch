﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using QuickLaunch.Shared;
using System.Configuration;
using System.Xml.Linq;
using System.Linq;

namespace QuickLaunch.Models
{
    public class MenuLink : Abstract.IDataInfo //moved to WCF
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the name.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets/Sets the tool tip of the menu item
        /// </summary>
        public string toolTip { get; set; }

        /// <summary>
        /// Gets/Sets the click event. http denotes web url
        /// </summary>
        public string clickEvent { get; set; }

        /// <summary>
        /// Gets/Sets the image. Image should be stored as resource in app.
        /// </summary>
        public string img { get; set; }

        /// <summary>
        /// Gets/Sets the audience that sees this link.
        /// </summary>
        public string audience { get; set; }

        /// <summary>
        /// Gets/Sets the active directory filter for the menu item.
        /// when not empty, the menu item will only show if the user is in the specified AD group
        /// </summary>
        public string adGroupFilter { get; set; }

        /// <summary>
        /// Gets/Sets the order of the item
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// Gets/Sets the visibility
        /// </summary>
        public bool isVisible { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion

        public MenuLink() { }
        
        #region "SELECTS"

        public static List<MenuLink> selMenuLinks(bool ShowAll = false)
        {

            string xmlFileName = Utility.getPath() + "QL_MenuLinkCache.xml";
            List<MenuLink> mLinks = new List<MenuLink>();

            if (Utility.checkCache(xmlFileName, "QL_MENULINKCACHE"))
            {
                mLinks = getFromDB();

                if (mLinks.Count > 0)
                {
                    saveMLCache(mLinks, xmlFileName);
                }
            }
            else
            {
                mLinks = getMLCache(xmlFileName);
            }

            return mLinks;
        }

        private static List<MenuLink> getFromDB()
        {
            List<MenuLink> mLinks = new List<MenuLink>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selMenuLinks", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ShowAll", 1);
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        try
                        {
                            MenuLink b = new MenuLink()
                            {
                                id = IntParse(sqlDataReader["id"].ToString()),
                                name = sqlDataReader["name"].ToString(),
                                toolTip = sqlDataReader["toolTip"].ToString(),
                                clickEvent = sqlDataReader["clickEvent"].ToString(),
                                img = sqlDataReader["img"].ToString(),
                                audience = sqlDataReader["audience"].ToString(),
                                adGroupFilter = sqlDataReader["adGroupFilter"].ToString(),
                                orderId = IntParse(sqlDataReader["orderId"].ToString()),
                                isVisible = BoolParse(sqlDataReader["isVisible"].ToString()),
                                lup_dt = DateTime.Now
                            };

                            mLinks.Add(b);
                        }
                        catch (Exception ee)
                        { }
                    }

                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return mLinks;
        }

        private static List<MenuLink> getMLCache(string xmlFileName)
        {
            List<MenuLink> MLList = new List<MenuLink>();

            try
            {
                if (File.Exists(xmlFileName))
                {
                    XDocument xmlDoc = XDocument.Load(xmlFileName);
                    var appData = from app in xmlDoc.Descendants("MLItem")
                                  select new MenuLink()
                                  {
                                      id = int.Parse(app.Element("id").Value.ToString()),
                                      name = app.Element("name").Value,
                                      toolTip = app.Element("toolTip").Value,
                                      clickEvent = app.Element("clickEvent").Value,
                                      img = app.Element("img").Value,
                                      audience = app.Element("audience").Value,
                                      adGroupFilter = app.Element("adGroupFilter").Value,
                                      orderId = int.Parse(app.Element("orderId").Value.ToString()),
                                      isVisible = bool.Parse(app.Element("isVisible").Value.ToString()),
                                      lup_dt = DateTime.Now                                   
                                      
                                  };

                    foreach (MenuLink a in appData)
                    {
                        MLList.Add(a);
                    }
                }
            }
            catch (Exception ee)
            {
                MLList = getFromDB();
            }

            return MLList;
        }

        private static void saveMLCache(List<MenuLink> mLinks, string xmlFileName)
        {
            try
            {
                foreach (MenuLink a in mLinks)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("MLItems").Add(
                                new XElement("MLItem",
                                    new XElement("id", a.id.ToString()),
                                    new XElement("name", a.name),
                                    new XElement("toolTip", a.toolTip),
                                    new XElement("clickEvent", a.clickEvent),
                                    new XElement("img", a.img),
                                    new XElement("audience", a.audience),
                                    new XElement("adGroupFilter", a.adGroupFilter),
                                    new XElement("orderId", a.orderId.ToString()),
                                    new XElement("isVisible", a.isVisible.ToString()),
                                    new XElement("lup_dt", DateTime.Now.ToString())));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("Menu Link cache for QL"),
                            new XElement("MLItems",
                                new XElement("MLItem",
                                    new XElement("id", a.id.ToString()),
                                    new XElement("name", a.name),
                                    new XElement("toolTip", a.toolTip),
                                    new XElement("clickEvent", a.clickEvent),
                                    new XElement("img", a.img),
                                    new XElement("audience", a.audience),
                                    new XElement("adGroupFilter", a.adGroupFilter),
                                    new XElement("orderId", a.orderId.ToString()),
                                    new XElement("isVisible", a.isVisible.ToString()),
                                    new XElement("lup_dt", DateTime.Now.ToString()))));
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch (Exception ee)
            { }
        }
        #endregion

        public static List<MenuLink> DefaultMenu = new List<MenuLink>{
            new MenuLink { id = 100, name = "Power Chart", toolTip = "Open Power Chart", clickEvent = "EMR", img = "shield", audience = "ALL", adGroupFilter = "", orderId = 1, isVisible = true  },
            new MenuLink { id = 101, name = "FirstNet", toolTip = "Open FirstNet", clickEvent = "FN", img = "ambulance", audience = "ALL", adGroupFilter = "", orderId = 2, isVisible = true  },
            new MenuLink { id = 102, name = "SurgiNet", toolTip = "Open SurgiNet", clickEvent = "SN", img = "drhead", audience = "ALL", adGroupFilter = "", orderId = 3, isVisible = true  },
            new MenuLink { id = 103, name = "Email", toolTip = "Open Email", clickEvent = "EMAIL", img = "email", audience = "ALL", adGroupFilter = "", orderId = 4, isVisible = true  },
            new MenuLink { id = 104, name = "User Portal", toolTip = "Open My Portal", clickEvent = "PORTAL", img = "myportal", audience = "ALL", adGroupFilter = "", orderId = 5, isVisible = true  },
            new MenuLink { id = 105, name = "Intranet", toolTip = "Open Intranet", clickEvent = "INTRANET", img = "intranet", audience = "ALL", adGroupFilter = "", orderId = 6, isVisible = true  }
        };

        #region util
        //simple methods for parsing and error handling

        private static double DBParse(string input)
        {
            try
            {
                return double.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        private static bool BoolParse(string input)
        {
            try
            {
                return bool.Parse(input);
            }
            catch
            {
                return false;
            }
        }

        private static int IntParse(string input)
        {
            try
            {
                return int.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        private static DateTime DTParse(string input)
        {
            try
            {
                return DateTime.Parse(input);
            }
            catch
            {
                return DateTime.Now;
            }
        }

        #endregion
    }

}
