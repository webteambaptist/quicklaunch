﻿namespace QuickLaunch.Shared
{
    public interface IQuickLaunch
    {
        /// <summary>
        /// Sets the current user.
        /// </summary>
        /// <param name="user">The user.</param>
        void SetCurrentUser(UserInfo user, ImprivataEvent imprivataEvent);

    }
}