﻿

using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;


/// <summary>
/// Allows for easy connections to a datastore for select, insert, update, and delete of data
/// </summary>
/// 
    public class DBManagerWTS
    {        
        private const string ConnectionString = "Data Source=BHPMSQINF20S1;Trusted_Connection=False;Initial Catalog=WTSLocation;User ID=wts_user_read_only;Password=wt$read;"; //PROD Database
        private static SqlConnection ConnectionOleDb()
        {
            try
            {                
                string aConstr = ConfigurationManager.ConnectionStrings["WTSConnString"] != null ? ConfigurationManager.ConnectionStrings["WTSConnString"].ToString() : ConnectionString;
                
                SqlConnection conn = new SqlConnection();
                conn.ConnectionString = aConstr;
                return conn;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static bool HasDBConnection()
        {
            try
            {
                SqlConnection cnn = ConnectionOleDb();
                cnn.Open();
                cnn.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static IDataReader ExecQueryDR(string strQuery)
        {
            SqlConnection cnn = ConnectionOleDb();
            try
            {
                DataSet ds = new DataSet();
                SqlCommand cmd = new SqlCommand(strQuery);
                cmd.CommandType = CommandType.Text;

                IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);                       
                return dr;
            }
            catch (Exception ex)
            {
                cnn.Close();
                throw ex;
            }
            finally {
                //Datareader will close the connection
            }
        }

        public static IDataReader ExecCommandDR(string strQuery, SqlParameter[] @params, bool CloseConnImmediately = false)
        {            
            SqlConnection conn = ConnectionOleDb();
            SqlCommand cmd = new SqlCommand(strQuery, conn);
            for (int i = 0; i <= @params.Length - 1; i++)
            {
                try
                {                    
                    if (@params[i] != null)
                    {
                        if (@params[i].SqlDbType == SqlDbType.DateTime && (@params[i].Value.ToString().Contains("0001") || @params[i].Value.ToString().Contains("1900")))
                            @params[i].Value = DBNull.Value;

                        cmd.Parameters.AddWithValue(@params[i].ParameterName, @params[i].Value ?? string.Empty);
                    }
                }
                catch { }
            }            
            try
            {
                conn.Open();
                IDataReader dr = cmd.ExecuteReader(CommandBehavior.CloseConnection);                
                if (CloseConnImmediately) { dr.Close(); return null; } //for items that won't be closing their own datareader
                return dr;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {//Datareader will close the connection
                //conn.Close();
            }
        }
        public static DataSet ExecCommandDS(string strQuery, SqlParameter[] @params)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = ConnectionOleDb();
            SqlCommand myCommand = new SqlCommand(strQuery, conn);
            for (int i = 0; i <= @params.Length - 1; i++)
            {
                try
                {
                    if (@params[i] != null)
                    {
                        myCommand.Parameters.AddWithValue(@params[i].ParameterName, @params[i].Value);
                    }
                }
                catch { }
            }

            try
            {

                SqlDataAdapter dataAdapter = new SqlDataAdapter();
                dataAdapter.SelectCommand = myCommand;
                dataAdapter.SelectCommand.Connection = conn;
                dataAdapter.SelectCommand.CommandTimeout = 120;
                dataAdapter.Fill(ds);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                conn.Close();
            }
        }

    }