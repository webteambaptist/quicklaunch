﻿using QuickLaunch.Shared;
using QuickLaunch.Shared.Domain.Concrete;
using QuickLaunch.Shared.QL;
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;

public class DBManager
{
	private static string ConnectionString
	{
		get;
		set;
	}

	public DBManager()
	{

	}

    public static bool HasDBConnection()
    {
        bool flag;

        try
        {
            SqlConnection sqlConnection = DBManager.ConnectionOleDb();
            sqlConnection.Open();
            sqlConnection.Close();
            flag = true;
        }
        catch (Exception exception)
        {
            flag = false;
        }
        return flag;
    }

	private static SqlConnection ConnectionOleDb()
	{
		SqlConnection sqlConnection;

		try
		{
			//string tITOHash = (new QL()).GetTITOHash("");

 			if (string.IsNullOrEmpty(DBManager.ConnectionString))
			{
                //DBManager.ConnectionString = SysHelper.Decrypt("QtPJuwK27xZYhh7mOp3dKy/kwOYYBks7liIGKPzdBXOnkI0+lc/bV34X9NRd8bKu1ZpxeTAOqoLehXtIUXqwA80AyjTjBYtRAx8z0EVBVJjtWY9Rx03VljdCvmbO3UV3UTm3bKhspwzeq/Hv+aFULd8PNc7AulV5uuXRC/uictI="); //is encryption really neccessary? //updated to titodb_sql.bh.local FQDN
                DBManager.ConnectionString = SysHelper.Decrypt("QtPJuwK27xZYhh7mOp3dKy/kwOYYBks7liIGKPzdBXOnkI0+lc/bV34X9NRd8bKu1ZpxeTAOqoLehXtIUXqwA80AyjTjBYtRAx8z0EVBVJjtWY9Rx03VljdCvmbO3UV3UTm3bKhspwzeq/Hv+aFULQKrFn5Y6J9CgMTp6ZXVSkucwtodzEwhabIX9witeSRU"); //is encryption really neccessary? //updated to titodb_sql.bh.local FQDN

			}

			sqlConnection = new SqlConnection()
			{
				ConnectionString = (ConfigurationManager.ConnectionStrings["TITOConnString"] != null ? ConfigurationManager.ConnectionStrings["TITOConnString"].ToString() : DBManager.ConnectionString)
			};
		}
		catch (Exception exception)
		{
			sqlConnection = null;

            //if hard-coded fails use this

            string tITOHash = (new QL()).GetTITOHash("");

            if (string.IsNullOrEmpty(DBManager.ConnectionString))
            {
                DBManager.ConnectionString = SysHelper.Decrypt(tITOHash);
            }

            sqlConnection = new SqlConnection()
            {
                ConnectionString = (ConfigurationManager.ConnectionStrings["TITOConnString"] != null ? ConfigurationManager.ConnectionStrings["TITOConnString"].ToString() : DBManager.ConnectionString)
            };
		}
		return sqlConnection;
	}

    public static IDataReader ExecCommandDR(string sQuery, SqlParameter[] @params, bool CloseConnImmediately = false, bool IsRetry = false)
    {
        IDataReader dataReader;
        bool flag;
        SqlConnection sqlConnection = DBManager.ConnectionOleDb();
        SqlCommand sqlCommand = new SqlCommand(sQuery, sqlConnection);
        for (int i = 0; i <= (int)@params.Length - 1; i++)
        {
            try
            {
                if (@params[i] != null)
                {
                    if (@params[i].SqlDbType != SqlDbType.DateTime)
                    {
                        flag = true;
                    }
                    else
                    {
                        flag = (@params[i].Value.ToString().Contains("0001") ? false : !@params[i].Value.ToString().Contains("1900"));
                    }
                    if (!flag)
                    {
                        @params[i].Value = DBNull.Value;
                    }
                    SqlParameterCollection parameters = sqlCommand.Parameters;
                    string parameterName = @params[i].ParameterName;
                    object value = @params[i].Value;
                    if (value == null)
                    {
                        value = string.Empty;
                    }
                    parameters.AddWithValue(parameterName, value);
                }
            }
            catch
            {
            }
        }
        try
        {
            try
            {
                sqlConnection.Open();
                sqlCommand.CommandTimeout = 120;
                IDataReader dataReader1 = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
                if (!CloseConnImmediately)
                {
                    dataReader = dataReader1;
                }
                else
                {
                    dataReader1.Close();
                    dataReader = null;
                }
            }
            catch (SqlException sqlException1)
            {
                SqlException sqlException = sqlException1;
                if (IsRetry)
                {
                    throw sqlException;
                }
                dataReader = DBManager.ExecCommandDR(sQuery, @params, CloseConnImmediately, true);
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        finally
        {
        }
        return dataReader;
    }

    //public static DataSet ExecCommandDS(string strQuery, SqlParameter[] @params)
    //{
    //    //DataSet dataSet;
    //    DataSet dataSet1 = new DataSet();
    //    //SqlConnection sqlConnection = DBManager.ConnectionOleDb(); //old way

    //    string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

    //    using (SqlConnection sqlConnection = new SqlConnection(config))
    //    {
    //        SqlCommand sqlCommand = new SqlCommand(strQuery, sqlConnection);
    //        for (int i = 0; i <= (int)@params.Length - 1; i++)
    //        {
    //            try
    //            {
    //                if (@params[i] != null)
    //                {
    //                    sqlCommand.Parameters.AddWithValue(@params[i].ParameterName, @params[i].Value);
    //                }
    //            }
    //            catch(Exception ee)
    //            {
    //                Utility.XMLEntry xmlEntry = new Utility.XMLEntry();
    //                xmlEntry.timeStamp = DateTime.Now;
    //                xmlEntry.eventType = ee.Message;
    //                xmlEntry.ntID = "";
    //                xmlEntry.machine = System.Environment.MachineName;
    //                xmlEntry.app = "ExecCommandDS";
    //                xmlEntry.timeElapsed = "0";
    //                Utility.writeXMLEntry(xmlEntry);
    //            }
    //        }
    //        try
    //        {
    //            try
    //            {
    //                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter()
    //                {
    //                    SelectCommand = sqlCommand
    //                };
    //                sqlDataAdapter.SelectCommand.Connection = sqlConnection;
    //                sqlDataAdapter.SelectCommand.CommandTimeout = 120;
    //                sqlDataAdapter.Fill(dataSet1);
    //               // dataSet = dataSet1;
    //            }
    //            catch (Exception exception)
    //            {
    //                Utility.XMLEntry xmlEntry = new Utility.XMLEntry();
    //                xmlEntry.timeStamp = DateTime.Now;
    //                xmlEntry.eventType = exception.Message;
    //                xmlEntry.ntID = "";
    //                xmlEntry.machine = System.Environment.MachineName;
    //                xmlEntry.app = "ExecCommandDS";
    //                xmlEntry.timeElapsed = "0";
    //                Utility.writeXMLEntry(xmlEntry);
    //            }
    //        }
    //        finally
    //        {
    //           // sqlConnection.Close();
    //        }
    //    }
    //    return dataSet1;
    //}

    //public static bool HasDBConnection()
    //{
    //    bool flag;
    //    try
    //    {
    //        SqlConnection sqlConnection = DBManager.ConnectionOleDb();
    //        sqlConnection.Open();
    //        sqlConnection.Close();
    //        flag = true;
    //    }
    //    catch (Exception exception)
    //    {
    //        flag = false;
    //    }
    //    return flag;
    //}
}
