﻿using System;
using System.Collections.Generic;
using System.Text;
//using System.Reflection; //took out reflection because it was running too slow
using System.Data;

namespace QuickLaunch
{
    public static class BOHelper
    {
        /// <summary>
        /// Creates and populates an instance of the objType Type.
        /// </summary>
        /// <typeparam name="T">Type of object to return.</typeparam>
        /// <param name="objType">Type of the object to instantiate.</param>
        /// <param name="dr">IDataReader with the data to populate the object instance with.</param>
        /// <returns>An instance of the objType type.</returns>
        public static T FillObject<T>(IDataReader dr) where T : class, Abstract.IDataInfo, new()
        {
            T obj = null;

            try
            {
                if (dr.Read())
                {
                    obj = CreateObject<T>(dr);
                }
            }
            finally
            {
                if (dr.IsClosed == false)
                    dr.Close();
                dr.Dispose();
            }

            return obj;
        }

        /// <summary>
        /// Creates and populates a collection with instances of the objType object passed in.
        /// </summary>
        /// <typeparam name="T">Type of object to add to the collection.</typeparam>
        /// <param name="objType">Type of object to add to the collection.</param>
        /// <param name="dr">IDataReader that contains the data for the collection.</param>
        /// <param name="collection">ICollection object to populate.</param>
        /// 
        public static List<T> FillCollection<T>(IDataReader dr) where T : class, Abstract.IDataInfo, new() //, new() where C : List<T>
        {
            //C coll = new C();
            List<T> coll = new List<T>();
            try
            {

                while (dr.Read())
                {
                    T obj = CreateObject<T>(dr);
                    coll.Add(obj);
                }
            }
            finally
            {
                if (dr.IsClosed == false)
                    dr.Close();
                dr.Dispose();
            }
            return coll;
        }

        /// <summary>
        /// Iterates through the object type's properties and attempts to assign the value from the datareader to
        /// the matching property.
        /// </summary>
        /// <typeparam name="T">The type of object to populate.</typeparam>
        /// <param name="dr">The IDataReader that contains the data to populate the object with.</param>
        /// <returns>A populated instance of type T</returns>
        public static T CreateObject<T>(IDataReader dr) where T : class, Abstract.IDataInfo, new()
        {
            T obj = new T();

            //for (int i = 0; i < dr.FieldCount; i++)
            //{
            //    Object value = dr.GetValue(i);
            //    if (value.GetType() == typeof(System.DBNull))
            //        value = null;

            //    obj.SetMember(dr.GetName(i).ToLower(), value);
            //}

            for (int i = 0; i < dr.FieldCount; i++)
            {
                Object value = dr.GetValue(i);
                if (value.GetType() == typeof(System.DBNull))
                    continue;

                System.Reflection.PropertyInfo pi = obj.GetType().GetProperty(dr.GetName(i).Replace(" ", "_").Replace("-", "_")); //, System.Reflection.BindingFlags.IgnoreCase);
                if (pi == null) continue;
                if (pi.PropertyType == typeof(bool))
                    pi.SetValue(obj, bool.Parse(value.ToString()), null);
                else if (pi.PropertyType == typeof(int))
                    pi.SetValue(obj, int.Parse(value.ToString()), null);
                else if (pi.PropertyType == typeof(decimal))
                    pi.SetValue(obj, decimal.Parse(value.ToString()), null);
                else if (pi.PropertyType == typeof(double))
                    pi.SetValue(obj, double.Parse(value.ToString()), null);
                else if (pi.PropertyType == typeof(byte[]))
                    pi.SetValue(obj, (byte[])value, null);
                else if (pi.PropertyType == typeof(DateTime))
                    pi.SetValue(obj, DateTime.Parse(value.ToString()), null);
                else if (pi.PropertyType == typeof(Boolean))
                    pi.SetValue(obj, Boolean.Parse(value.ToString()), null);
                else
                    try
                    {
                        pi.SetValue(obj, value.ToString(), null);
                    }
                catch(Exception e){

                }

                //obj.SetMember(dr.GetName(i).ToLower(), value);
            }
            //obj.SetMember(dr);
            return obj;
        }

        //
        //var person = new Person();
        //ar age = InfoOf(() => person.Age);
        //
        //public static System.Reflection.PropertyInfo InfoOf<T>(System.Linq.Expressions.Expression<Func<T>> ex)
        //{
        //    return (System.Reflection.PropertyInfo)((System.Linq.Expressions.MemberExpression)ex.Body).Member;
        //}

    }
}