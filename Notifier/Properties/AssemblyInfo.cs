﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Quick Launch Notifier")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e61aa017-925b-4a1f-8935-8e269e0c998b")]

// Version_QL information for an assembly consists of the following four values:
//
//      Major Version_QL
//      Minor Version_QL 
//      Build Number
//      Revision
//
[assembly: CLSCompliant(true)]
[assembly: AllowPartiallyTrustedCallers]