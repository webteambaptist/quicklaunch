﻿namespace Notifier
{
    using System;
    using System.Diagnostics;
    using System.Windows.Forms;

    static class Program
    {

        #region Properties
        
        public static string CurrentUserId { get; set; }
        private delegate void AsyncMethodCaller();
        private static Process QuickLaunchProc = null;
        private static NotifyIcon niError = new NotifyIcon(); //only show once    
        private static string machine = System.Environment.MachineName;
        private static Stopwatch swatch = new Stopwatch();

        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
           // swatch.Start();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                Process[] RunningProcesses = Process.GetProcessesByName("Notifier");

                if (RunningProcesses.Length > 0)
                {
                    Application.Exit();
                }

                string[] ars = Environment.GetCommandLineArgs();

                Splash splashForm = new Splash(ars); //moved program code to form

                if(splashForm != null && !splashForm.IsDisposed)
                { 
                    Application.Run(splashForm);
                }
               
               Application.Exit();

            }
            catch (Exception ex)
            { //Something truely unexpected happened.
               // Truncus.LogException(ex, true, "NOTIFIER", "Notifier");
            }

           
        }        
    }
}