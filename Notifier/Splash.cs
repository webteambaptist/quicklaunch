﻿using QuickLaunch.Models;
using QuickLaunch.Shared;
using QuickLaunch.Shared.Domain.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;
using System.Windows.Forms;
using System.Xml.Linq;


namespace Notifier
{
    public partial class Splash : Form
    {
        public static string CurrentUserId { get; set; }
        private delegate void AsyncMethodCaller();
        private static Process QuickLaunchProc = null;
        private static NotifyIcon niError = new NotifyIcon(); //only show once    
        private static string machine = System.Environment.MachineName;
        private static Stopwatch swatch = new Stopwatch();
        private static ImprivataEvent WhatIsHappening = new ImprivataEvent();
        private static string logLevel = "1";
        List<CacheRefresh> cList = CacheRefresh.selCacheRefresh();

        public Splash()
        {
            InitializeComponent();
            logLevel = Truncus.setLogLevel();
        }

        public Splash(string[] ars)
        {
            InitializeComponent();
          //  this.Visible = false;
            logLevel = Truncus.setLogLevel();

            try
            {
                this.Visible = true;
                this.Show();

                WhatIsHappening = ars.Length < 5 ? ImprivataEvent.UserLogon : (ImprivataEvent)Enum.Parse(typeof(ImprivataEvent), ars[1]); //updated for new scripts instead of < 7

                UserInfo userInfo = new UserInfo();


                if (ars.Length > 4)
                {
                    if (string.IsNullOrEmpty(ars[2].ToUpper()))  //no sso user, fire NonOneSignLogon event
                    {
                        userInfo.UserName = "";
                        userInfo.Password = "";
                        userInfo.Domain = "";
                        WhatIsHappening = ImprivataEvent.NonOneSignLogon;
                    }
                    else
                    {
                        userInfo.UserName = ars[2].ToUpper();
                        userInfo.Password = ars[3];
                        userInfo.Domain = ars[4];
                    }

                    if (!userInfo.UserName.ToUpper().Contains("{VAR"))
                    {
                        try
                        {
                            try
                            {
                                double interval = double.Parse((from x in cList where x.CacheName.ToUpper() == "QL_FARMS_USER" select x.RefreshInHours).First());
                                NUtility.refreshSettings(interval * -1); //update settings for logging baselines
                            }
                            catch(Exception ee)
                            {
                                NUtility.refreshSettings(Utility.retHour()); //update settings for logging baselines
                            }
                           

                            if ((logLevel == "2") || (logLevel == "3"))
                            {
                                Truncus.NotifierLogEvents("1", "", userInfo.UserName, 0, WhatIsHappening.ToString());
                            }
                        }
                        catch (Exception ex)
                        { }

                        if(WhatIsHappening == ImprivataEvent.Reset)
                        {
                            try
                            {
                                Process[] pname = Process.GetProcessesByName("QuickLaunch");
                                foreach (Process p in pname)
                                {
                                    p.Kill(); //kill all
                                }
                            }
                            catch(Exception ex)
                            {

                            }
                            this.Close();
                        }

                        if (WhatIsHappening == ImprivataEvent.UserLogon)
                        {
                            try
                            {
                                Process[] pname = Process.GetProcessesByName("QuickLaunch");
                                foreach (Process p in pname)
                                {
                                    //  p
                                    p.Kill(); //kill all
                                }

                                LaunchQL();
                            }
                            catch (Exception ex)
                            {
                                Truncus.NotifierLogEvents(logLevel, "", "", 0, "Kill QL LOGON");
                            }

                            //this.Visible = true;
                            //this.Show();
                        }

                        if (WhatIsHappening == ImprivataEvent.Install) //flush cache
                        {
                            try
                            {
                                Process[] pname = Process.GetProcessesByName("QuickLaunch");
                                foreach (Process p in pname)
                                {
                                    p.Kill(); //kill all
                                }

                                LaunchQL();
                            }
                            catch (Exception ex)
                            {
                                Truncus.NotifierLogEvents(logLevel, "", "", 0, "Kill QL LOGON");
                            }

                            this.Visible = true;
                            this.Show();

                            if (IsNetworkAvailable())
                            {
                                try
                                {
                                    foreach (string f in Directory.GetFiles(Utility.getPath(), "QL_*.xml"))
                                    {
                                        File.Delete(f);
                                    }

                                     Truncus.NotifierLogEvents("1", "INSTALL CACHE FLUSH", userInfo.UserName, 0, WhatIsHappening.ToString());
                                }
                                catch (IOException ie)
                                {
                                     Truncus.NotifierLogEvents("1", "ERROR INSTALL CACHE FLUSH: " + ie.Message , userInfo.UserName, 0, WhatIsHappening.ToString());
                                }
                                catch(Exception ex)
                                {
                                    Truncus.NotifierLogEvents("1", "ERROR INSTALL CACHE FLUSH: " + ex.Message, userInfo.UserName, 0, WhatIsHappening.ToString());
                                }
                            }
                        }

                        if (WhatIsHappening == ImprivataEvent.NonOneSignLogon) //flush cache
                        {

                            if ((logLevel == "2") || (logLevel == "3"))
                            {
                                Truncus.NotifierLogEvents("1", "NonOneSignLogon", userInfo.UserName, 0, WhatIsHappening.ToString());
                            }
                        }

                        AsyncMethodCaller asyncCallerQL = new AsyncMethodCaller(FindQuickLaunch);
                        IAsyncResult resultFindQL = asyncCallerQL.BeginInvoke(null, null); // Initiate the async call.                

                        AsyncMethodCaller asyncCreateChannel = new AsyncMethodCaller(CreateChannel);
                        IAsyncResult resultChannel = asyncCreateChannel.BeginInvoke(null, null); // Initiate the async call.   

                        LaunchQL(asyncCallerQL, resultFindQL, asyncCreateChannel, resultChannel, WhatIsHappening, userInfo);
                    }
                    else
                    {

                        if ((logLevel == "2") || (logLevel == "3"))
                        {
                            Truncus.NotifierLogEvents("1", "INVALID USER", userInfo.UserName, 0, WhatIsHappening.ToString());
                        }

                        AutoClosingMessageBox.Show("Busy, please wait...", "Quick Launch", 9000);
                    }
                }
                else
                {
                    AutoClosingMessageBox.Show("User not detected. Quick Launch will close, please Tap Out and Tap In.", "Quick Launch Closing", 9000);
                    //  Application.Exit();
                }

            }
            catch (Exception ex)
            { //Something truely unexpected happened.
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            finally
            {
                this.Close();
            }
        }

        private void LaunchQL(AsyncMethodCaller asyncCallerQL, IAsyncResult resultFindQL, AsyncMethodCaller asyncCreateChannel, IAsyncResult resultChannel, ImprivataEvent WhatIsHappening, UserInfo userInfo)
        {
            SaveEvents(WhatIsHappening, userInfo); //replaces registry writes in VBS scripts

            double interval = Utility.retHour();

            try
            {

                string output = (from x in cList where x.CacheName.ToUpper() == "QL_FARMS_USER" select x.RefreshInHours).First();
                interval = double.Parse(output);
                NUtility.refreshSettings(interval * -1); //update settings for logging baselines
            }
            catch (Exception ee)
            {
                NUtility.refreshSettings(Utility.retHour()); //update settings for logging baselines
            }

            if ((WhatIsHappening == ImprivataEvent.UserLogon) || (WhatIsHappening == ImprivataEvent.WorkstationUnlock))
            {

                //if (!NUtility.cacheExists()) ;
                if (NUtility.checkCache(userInfo, interval)) //if true, QL_Farms.xml is missing or stale
                {
                    try
                    {
                        List<XenAppInfo> infos = GetFarmsFromSvc(userInfo);
                        saveCache(infos, userInfo);
                    }
                    catch (Exception ex)
                    { }

                }
                else
                {

                    try
                    {

                        userInfo.XenAppInfo = getCache(userInfo);

                        if (userInfo.XenAppInfo.Count < 1) //cache is bad, missing file, or process broke
                        {
                            List<XenAppInfo> xinfos = GetFarmsFromSvc(userInfo);
                            userInfo.XenAppInfo = xinfos;
                            saveCache(xinfos, userInfo);
                        }
                    }
                    catch (Exception ex)
                    {
                        Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
                    }
                }
            }

            CurrentUserId = userInfo.UserName;

            bool complete = false;
            Exception currentException = null;

            asyncCallerQL.EndInvoke(resultFindQL); //wait on Quick Launch
            asyncCreateChannel.EndInvoke(resultChannel); //wait on Server Channel for remoting

            var now = DateTime.Now;
            //   var end = now + TimeSpan.FromSeconds(25);
            var end = now + TimeSpan.FromSeconds(60);

            while (!complete && now < end)
            {
                try
                {
                    IQuickLaunch quickLaunch = (IQuickLaunch)Activator.GetObject(typeof(IQuickLaunch), "ipc://QuickLaunchUI/QuickLaunch");

                    //doublecheck xInfos
                    try
                    {
                        if (userInfo.XenAppInfo.Count < 1)
                        {
                            List<XenAppInfo> infos = GetFarmsFromSvc(userInfo);
                            saveCache(infos, userInfo);
                            userInfo.XenAppInfo = infos;
                        }
                    }
                    catch (Exception ex)
                    { }
                   
                   quickLaunch.SetCurrentUser(userInfo, WhatIsHappening);
                    
                    complete = true;
                    currentException = null;
                  //  Thread.Sleep(3999);
                }
                catch (RemotingException rex)
                {
                    /*NOTE: This can throw a bunch of useless remoting exceptions if the QuickLaunch.exe process
                     * has just started and isn't ready for messages. We keep a reference to the most recent of
                     * these just so we can report it should the process time out.
                     */
                    currentException = rex;
                    //System.Threading.Thread.Sleep(400); //don't overload the queue, or cold startup could be unpredictable
                    System.Threading.Thread.Sleep(800);
                }
                catch (Exception ex)
                {  //Any other kind of exception thrown results in failure.
                    complete = true;
                    currentException = ex;
                }
                now = DateTime.Now;
            }

            if (currentException != null)
            {
                AutoClosingMessageBox.Show("Error. Quick Launch will close, please Tap Out and Tap In.", "Quick Launch Closing", 9000);
                //DisplayOSBubble(niError, "Quick Launch Closed", "There was an error notifying Quick Launch of an event." + Environment.NewLine + "Quick Launch will now be closed. Please Tap Out and Tap In.");

                if (QuickLaunchProc != null)
                {
                    QuickLaunchProc.Kill();
                }

                Truncus.LogErrorEvents(currentException.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            

        }

        private static List<XenAppInfo> GetFarmsFromSvc(UserInfo userInfo) //changed output to list
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            //check DB first for user farm
            List<XenAppInfo> xInfos = UserFarm.getUserFarm(userInfo.UserName);
            //call TITO WCF

            if (xInfos.Count < 1) //user farm is empty for this user
            {
                bool isDebug = false;

                try
                {
                    string debug = ConfigurationManager.AppSettings["IsDebug"];


                    if (!string.IsNullOrEmpty(debug))
                    {
                        if (debug == "2")
                        {
                            isDebug = true;
                        }
                    }
                }
                catch (Exception ex)
                { }

                if (isDebug)
                {
                    DirectQuery(userInfo, xInfos);
                }
                else
                {
                    FarmsFromSVC(userInfo, xInfos);
                }

            }
            else
            {
                userInfo.XenAppInfo = xInfos;
            }

            sw.Stop();

            string flbase = Utility.getSettingFromCache("FarmsListBaseline");

            try
            {
                if (sw.ElapsedMilliseconds > long.Parse(flbase)) //log to DB
                {
                    Truncus.NotifierLogEvents(logLevel, "", userInfo.UserName, sw.ElapsedMilliseconds, "FARM LIST BASELINE");
                }
                else
                {
                    logLevel = "1"; //log locally only
                    Truncus.NotifierLogEvents(logLevel, "", userInfo.UserName, sw.ElapsedMilliseconds, "FARM LIST BASELINE");
                }
            }
            catch (Exception ex)
            { }

            if(xInfos.Count > 0)
            {
                saveCache(xInfos, userInfo);
            }

            return xInfos;
        }

        private static void DirectQuery(UserInfo userInfo, List<XenAppInfo> xInfos)
        {
            xInfos = UserFarm.getFarmsDirect();
            userInfo.XenAppInfo = xInfos;
        }

        private static void FarmsFromSVC(UserInfo userInfo, List<XenAppInfo> xInfos)
        {

            List<XenAppInfo> infos = NUtility.GetFarms();

            //List<XenAppInfo> infos = getFarmsfromDB();

            try
            {

                foreach (XenAppInfo x in infos)
                {
                    XenAppInfo xInfo = new XenAppInfo();
                    xInfo.Farm = x.Farm;
                    xInfo.XenAppHost1 = x.XenAppHost1;
                    xInfo.XenAppHost2 = x.XenAppHost2;
                    xInfo.XenAppPort = x.XenAppPort;
                    xInfos.Add(xInfo);
                }

                userInfo.XenAppInfo = xInfos;
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Retrieves list of farms from cache
        /// </summary>
        /// <returns></returns>
        private static List<XenAppInfo> getCache(UserInfo userInfo)
        {


            List<XenAppInfo> xInfos = new List<XenAppInfo>();
            string xmlFileName = Utility.getPath() + "QL_Farms" + userInfo.UserName + ".xml";

            try
            {
                if (Utility.checkCache(xmlFileName, "QL_FARMS_USER"))
                {

                    Stopwatch sw = new Stopwatch();
                    sw.Start();

                    XDocument xmlDoc = XDocument.Load(xmlFileName);
                    var farms = from farm in xmlDoc.Descendants("XenAppInfo")
                                select new
                                {
                                    Farm = farm.Element("Farm").Value,
                                    XenAppHost1 = farm.Element("XenAppHost1").Value,
                                    XenAppHost2 = farm.Element("XenAppHost2").Value,
                                    XenAppPort = farm.Element("XenAppPort").Value,
                                };

                    foreach (var x in farms)
                    {
                        XenAppInfo xInfo = new XenAppInfo();
                        xInfo.Farm = x.Farm;
                        xInfo.XenAppHost1 = x.XenAppHost1;
                        xInfo.XenAppHost2 = x.XenAppHost2;
                        xInfo.XenAppPort = x.XenAppPort;
                        xInfos.Add(xInfo);
                    }

                    sw.Stop();

                    Truncus.NotifierLogEvents("1", "", userInfo.UserName, sw.ElapsedMilliseconds, "Citrix Farm List Cache");
                }
                else
                {
                    //return empty list
                }
            }
            catch (Exception ex)
            { }

            //filter out duplicates, fixed 12/5/14
            xInfos = xInfos.GroupBy(x => x.Farm)
                .Select(y => y.First())
                .ToList();

            return xInfos;
        }

        /// <summary>
        /// saves list
        /// </summary>
        /// <param name="infos"></param>
        private static void saveCache(List<XenAppInfo> infos, UserInfo userInfo)
        {
            try
            {
                string xmlFileName = Utility.getPath() + "QL_Farms" + userInfo.UserName + ".xml";

                foreach (XenAppInfo x in infos)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("Farms").Add(
                            new XElement("XenAppInfo",
                                new XElement("Farm", x.Farm),
                                new XElement("XenAppHost1", x.XenAppHost1),
                                new XElement("XenAppHost2", x.XenAppHost2),
                                new XElement("XenAppPort", x.XenAppPort)));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("Farms List for QL"),
                            new XElement("Farms",
                                new XElement("XenAppInfo",
                                    new XElement("Farm", x.Farm),
                                    new XElement("XenAppHost1", x.XenAppHost1),
                                    new XElement("XenAppHost2", x.XenAppHost2),
                                    new XElement("XenAppPort", x.XenAppPort))
                                    )
                                );
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        private static void SaveEvents(ImprivataEvent WhatIsHappening, UserInfo userInfo)
        {
            try
            {
                switch (WhatIsHappening)
                {
                    case ImprivataEvent.WorkstationLock:
                        {
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "EventStatus", "WorkstationLock");
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "LastDesktopLock", DateTime.Now.ToString());
                            break;
                        }
                    case ImprivataEvent.UserLogon:
                        {
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "EventStatus", "UserLogon");
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "CurrentUserId", userInfo.UserName);
                            break;
                        }
                    case ImprivataEvent.NonOneSignLogon:
                        {
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "EventStatus", "NonOneSignLogon");
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "CurrentUserId", userInfo.UserName);
                            break;
                        }
                    case ImprivataEvent.WorkstationUnlock:
                        {
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "EventStatus", "WorkstationUnlock");
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "CurrentUserId", userInfo.UserName);
                            break;
                        }
                    default:
                        {
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "EventStatus", "UserLogon");
                            SysHelper.SetRegistrySetting(@"HKEY_CURRENT_USER\Software\TITO\", "CurrentUserId", userInfo.UserName);
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }

        }

        /// <summary>
        /// Make sure QuickLaunch.exe is runnning.  If not, start it
        /// </summary>
        private static void FindQuickLaunch()
        {
            try
            {
                if ((WhatIsHappening == ImprivataEvent.UserLogon) || WhatIsHappening == ImprivataEvent.Install)
                    {
                        //moved closer to start
                        //try
                        //{ 
                        //Process[] pname = Process.GetProcessesByName("QuickLaunch");
                        //foreach (Process p in pname)
                        //{
                        //    p.Kill(); //kill all
                        //}

                        //       LaunchQL();
                        //}
                        //catch (Exception ex)
                        //{
                        //    Truncus.NotifierLogEvents(logLevel, "", "", 0, "Kill QL LOGON");                        
                        //}
                    }
                    else
                    {
                        Process[] pname = Process.GetProcessesByName("QuickLaunch");
                        if (pname.Length == 0) //no QL avail
                        {
                            LaunchQL();
                        }
                        else if (pname.Length > 1) //multiple QL's
                        {
                            try
                            {
                                Truncus.NotifierLogEvents(logLevel, "", "", 0, "Kill MultiQLs");
                            }
                            catch (Exception ex)
                            { }

                            foreach (Process p in pname)
                            {
                                p.Kill(); //kill all
                            }

                            LaunchQL(); //relaunch
                        }
                        else
                        {
                            //And make sure it is responding.
                            if (!pname[0].Responding)
                            {
                                try
                                {
                                    Truncus.NotifierLogEvents(logLevel, "", "", 0, "QL unresponsive");
                                }
                                catch (Exception ex)
                                { }

                                pname[0].Kill();
                                LaunchQL();
                            }
                            else
                            {
                                QuickLaunchProc = pname[0];
                            }
                        }
                   }
                }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Establishes client server relationship channel
        /// </summary>
        private static void CreateChannel()
        {
            try
            {
                BinaryServerFormatterSinkProvider serverProvider = new BinaryServerFormatterSinkProvider();
                serverProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
                BinaryServerFormatterSinkProvider clientProvider = new BinaryServerFormatterSinkProvider();
                clientProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
                System.Collections.IDictionary props = new System.Collections.Hashtable();
                props["portName"] = "MyClient";
                props["exclusiveAddressUse"] = false;
                props["authorizedGroup"] = "Everyone";

                //Instantiate our server channel.
                var channel = new IpcChannel(props, null, serverProvider);

                //IpcChannel channel = new IpcChannel("MyClient");
                ChannelServices.RegisterChannel(channel, false);
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Launches the Quick Launch UI.
        /// </summary>
        private static void LaunchQL()
        {
            swatch.Stop();

            string ntbase = Utility.getSettingFromCache("LoginBaseline");

            try
            {
                if (swatch.ElapsedMilliseconds > long.Parse(ntbase)) //log to DB
                {
                    Truncus.NotifierLogEvents(logLevel, "", CurrentUserId, swatch.ElapsedMilliseconds, "NOTIFIER TERMINATES");
                }
                else
                {
                    logLevel = "1"; //log locally
                    Truncus.NotifierLogEvents(logLevel, "", CurrentUserId, swatch.ElapsedMilliseconds, "NOTIFIER TERMINATES");
                }
            }
            catch (Exception ex)
            { }

            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "QuickLaunch.exe");
            if (File.Exists(path))
            {
                Process p = Process.Start(path);
                p.WaitForInputIdle();
            }
            else if (File.Exists(@"..\..\PN\bin\QuickLaunch.exe"))
            {
                Process p = Process.Start(@"..\..\PN\bin\QuickLaunch.exe");
                p.WaitForInputIdle();
            }
            else
            {
                AutoClosingMessageBox.Show("Path to QL not found. Quick Launch will close, please Tap Out and Tap In.", "Quick Launch Closing", 9000);
                //throw new ApplicationException("Path to QL not found '" + path + "'");
            }
        }

        /// <summary>
        /// Indicates whether any network connection is available
        /// Filter connections below a specified spexd, as well as virtual network cards.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if a network connection is available; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNetworkAvailable()
        {
            return IsNetworkAvailable(0);
        }

        /// <summary>
        /// Indicates whether any network connection is available.
        /// Filter connections below a specified speed, as well as virtual network cards.
        /// </summary>
        /// <param name="minimumSpeed">The minimum speed required. Passing 0 will not filter connection using speed.</param>
        /// <returns>
        ///     <c>true</c> if a network connection is available; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNetworkAvailable(long minimumSpeed)
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
                return false;

            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                // discard because of standard reasons
                if ((ni.OperationalStatus != OperationalStatus.Up) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Loopback) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Tunnel))
                    continue;

                // this allow to filter modems, serial, etc.
                // I use 10000000 as a minimum speed for most cases
                if (ni.Speed < minimumSpeed)
                    continue;

                // discard virtual cards (virtual box, virtual pc, etc.)
                if ((ni.Description.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0) ||
                    (ni.Name.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0))
                    continue;

                // discard "Microsoft Loopback Adapter", it will not show as NetworkInterfaceType.Loopback but as Ethernet Card.
                if (ni.Description.Equals("Microsoft Loopback Adapter", StringComparison.OrdinalIgnoreCase))
                    continue;

                return true;
            }
            return false;
        }

    }
}
