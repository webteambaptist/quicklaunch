﻿using QuickLaunch.Shared;
using QuickLaunch.Shared.Domain.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Notifier
{
    public static class NUtility
    {


        /// <summary>
        /// check for XML file with farms list and see if it is stale, if stale, delete
        /// </summary>
        /// <returns></returns>
        public static bool checkCache(UserInfo userInfo, double interval)
        {
            bool isOld = true;
            try
            {
                string xmlFileName = Utility.getPath() + "QL_Farms" + userInfo.UserName + ".xml";

                if (File.Exists(xmlFileName))
                {
                   // double time = Utility.retHour();
                    if (File.GetLastWriteTime(xmlFileName) < DateTime.Now.AddHours(interval)) //if the file is older than 24 hours
                    {
                        try
                        {
                            File.Delete(xmlFileName);
                            isOld = true;
                        }
                        catch (IOException ie)
                        {
                            isOld = true;
                        }
                    }
                    else
                    {
                        isOld = false; //if the file exists and is not stale, use it
                    }
                }
            }
            catch (IOException ie)
            {
                isOld = true; //return true to delete and refresh
            }
            return isOld;
        }

        /// <summary>
        /// Checks to see if settings are older than 24 hours, if so, retrieve and delete old settings
        /// </summary>
        /// <returns></returns>
        public static void refreshSettings(double interval)
        {
            string xmlFileName = Utility.getPath() + "QL_Settings.xml";
            try
            {
                if (File.Exists(xmlFileName))
                {
                    if (File.GetLastWriteTime(xmlFileName) < DateTime.Now.AddHours(interval)) //if the file is older than 25-36 hours
                    {
                        try
                        {
                            Utility.writeSettingsCache();
                        }
                        catch (Exception ee)
                        {
                           
                        }
                    }   //there is no else, if less than 25 hours, use it                
                }
                else //no file, write a new one, same method call
                {
                    Utility.writeSettingsCache();
                }
            }
            catch (Exception ee)
            {
               
            }
        }

        internal static List<XenAppInfo> GetFarms()
        {
            List<XenAppInfo> xList = new List<XenAppInfo>();

            string config = SysHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Connection = sqlConn;
                    sqlCommand.CommandText = "dbo.GetFarms";
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        XenAppInfo xenAppInfo = new XenAppInfo()
                        {
                            Farm = sqlDataReader["farmName"].ToString(),
                            XenAppHost1 = sqlDataReader["XenAppHost1"].ToString(),
                            XenAppHost2 = sqlDataReader["XenAppHost2"].ToString(),
                            XenAppPort = sqlDataReader["XenAppPort"].ToString()
                        };
                        xList.Add(xenAppInfo);
                    }
                    sqlDataReader.Close();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
            return xList;
        }
    }
}

