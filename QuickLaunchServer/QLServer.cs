﻿  using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;

namespace QuickLaunchServer
{
    public partial class QLServer : ServiceBase
    {
        #region properties
        private static Timer CheckStatusTimer = new Timer();
        private static readonly int DefaultTimerInterval = 1000 * 60 * 12; //12 hours
        QLAutoUpdateController qlAutoUpdateController;
        public static DateTime LastRunDt;
        #endregion

        public QLServer()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CheckStatusTimer.AutoReset = true;
            CheckStatusTimer.Interval = 10000; //TO KICK OFF TIMER RIGHT AWAY
            CheckStatusTimer.Elapsed += new ElapsedEventHandler(CheckStatusTimer_Elapsed);
            CheckStatusTimer.Start();
            qlAutoUpdateController = new QLAutoUpdateController();
        }

        private void CheckStatusTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            qlAutoUpdateController.FindDeployments();
            SetUpdateFrequency();
        }

        private void SetUpdateFrequency()
        {
            throw new NotImplementedException();
        }

        protected override void OnStop()
        {
            try
            {
                qlAutoUpdateController = null;
                CheckStatusTimer.Stop();
                WriteToEventLog(string.Format("Service Stopped"), EventLogEntryType.Information);
                base.OnStop();
            }
            catch(Exception ee)
            { }
        }

        public static void WriteToEventLog(string sEvent, EventLogEntryType entryType) //to do, add write to XML log
        {
            string sSource = "QuickLaunch Install Server";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
                EventLog.CreateEventSource(sSource, sLog);

            EventLog.WriteEntry(sSource, sEvent, entryType);

            try
            {
                if (entryType == EventLogEntryType.Error)
                    QuickLaunch.Models.ErrorLog.AddErrorToLog(new Exception(sEvent), "N/A", "AUTOINSTALL", false);
            }
            catch { }
        }
    }
}
