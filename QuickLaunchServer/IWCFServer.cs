﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace QuickLaunchServer
{
    public interface IWCFServer
    {
        [OperationContract]
        void SendMessage(string message, string caption, int timeout);

        [OperationContract]
        void LockMachine();

        [OperationContract]
        void ClearCache();

        [OperationContract]
        void UpdateNotify(string versionNumber); //used for deployment ID
    }
}
