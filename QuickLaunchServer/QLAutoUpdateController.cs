﻿using QuickLaunch.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace QuickLaunchServer
{
    class QLAutoUpdateController
    {
        internal void FindDeployments()
        {
            try
            {
                //selActiveDeployment
                List<Deployment> dList = new List<Deployment>();
                dList = Deployment.selActiveDeployments();

                if (dList.Count > 0) //get list of affected machines, then push
                {
                    foreach (Deployment d in dList)
                    {
                        List<Machine> mList = new List<Machine>();

                        if (d.IsEverywhere)
                        {
                            mList.AddRange(Machine.GetAllActiveMachines(d.MSIBuildVersion));
                        }
                        else
                        {
                            List<DeploymentLocation> dl = new List<DeploymentLocation>();
                            dl = DeploymentLocation.selDeployLocations(d.id);

                            foreach (DeploymentLocation deploy in dl)
                            {
                                mList.AddRange(Machine.GetMachinesByLocationID(deploy.id.ToString(), d.MSIBuildVersion));
                            }
                        }

                        BroadcastUpdate(mList, d);
                    }

                }
            }
            catch(Exception ee)
            { }
        }

        private void BroadcastUpdate(List<Machine> mList, Deployment d)
        {
            try
            {
                Parallel.ForEach<Machine>(mList, Machine => BroadcastToMachine(d, Machine));
            }
            catch(Exception ee)
            { }
        }

        private void BroadcastToMachine(Deployment d, Machine m)
        {
            try
            {
                NetTcpBinding binding = new NetTcpBinding();
                string address = "net.tcp://" + m.IpAddress + ":8000/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());

                //client

                EndpointAddress endAddress = new EndpointAddress(baseAddress.ToString().ToLower());
                ChannelFactory<IWCFServer> channelFactory =
                    new ChannelFactory<IWCFServer>(binding, endAddress);
                IWCFServer server = channelFactory.CreateChannel();
                server.UpdateNotify(d.id.ToString());
            }
            catch (Exception ee)
            {
                
            }
        }
    }
}
