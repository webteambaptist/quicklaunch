﻿<%@ Control Language="C#" AutoEventWireup="false" ClientIDMode="Static" EnableViewState="false" %>
<div class="menu">
<ul>
    <li><a href="<%=Page.ResolveUrl("~/default.aspx") %>">Home</a></li>
<%if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.SystemAlert.RequiredRoleId).Any()){ %>                
    <li><a href="<%=Page.ResolveUrl("~/systemalert.aspx") %>" style="">System Alerts</a><ul><li><a href="<%=Page.ResolveUrl("~/systemalertcode.aspx") %>">Codes</a></li><li><a href="<%=Page.ResolveUrl("~/systemalertresponse.aspx") %>">Responses</a></li><li><a href="?act=calendar">Calendar</a></li></ul></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.Versioning.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/versioning.aspx") %>">Versioning</a></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.QLDeployment.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/qldeployment.aspx") %>">Deployment</a></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.Location.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/location.aspx") %>">Locations</a><ul><li><a href="?act=wts">WTS Locations</a></li><li><a href="?act=bulk">Apply Machines</a></li></ul></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.QLMenu.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/qlapplication.aspx") %>">Applications</a></li>
    <li><a href="<%=Page.ResolveUrl("~/qlmenu.aspx") %>">QL Menu</a></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.Help.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/help.aspx") %>">QL Help</a><ul><li><a href="?act=images">Images</a></li></ul></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.MachineInventory.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/machineinventory.aspx") %>">Machine Inventory</a><ul><li><a href="?act=slow">Slow Machines</a></li></ul></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.ErrorLog.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/errorlog.aspx") %>">Error Log</a><ul><li><a href="?only=QL">Quick Launch Only</a></li><li><a href="?only=WA">Web Admin Only</a></li></ul></li>
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.User.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/user.aspx") %>">Users</a><ul><li><a href="?act=role">Users by Role</a></li><li><a href="?act=userexport">User Export (.xls)</a></li></ul></li>                
<% } if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin.Reporting.RequiredRoleId).Any()){ %>              
    <li><a href="<%=Page.ResolveUrl("~/reporting.aspx") %>">Reporting</a></li>                
<% } %>              
</ul>
</div>  