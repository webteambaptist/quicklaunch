﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using QuickLaunch.Shared;

namespace QuickLaunch.WebAdmin2014
{
    public partial class _Default : QuickLaunch.WebAdmin2014.BasePages.SecureUserPage
    {
        protected Models.UserProfile.UserMetrics metrics { get; set; }
        protected Models.UserProfile.UserMetrics metrics90Day { get; set; }
        public bool IsSystemAlertAdmin = false;
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (common.user == null)
                {
                }
                else
                {
                    LoadModules();
                    try { IsSystemAlertAdmin = common.user.Roles.Count < 3 && common.user.Roles.Any(r => r.id == 3); } catch {}
                    try { //only updates if user session exists
                        Models.UserProfile up = Session["user"] as Models.UserProfile;
                        UserInfo user = new UserInfo{ Domain = "BH", UserName = common.ntID};
                        ADHelper.GetGroups(ref user);
                        up.lastLogin = DateTime.Now; up.userTitle = user.UserAdTitle; up.userDept = user.UserAdDept;
                        Models.UserProfile.updUserProfile(up);
                    } catch { }
                }               
                    
            }
        }

        private void LoadModules()
        {
            if (!WebAdmin2014.Cache.Exists("SystemAlerts"))
                WebAdmin2014.Cache.Insert("SystemAlerts", Models.SystemAlert.selSystemAlerts(),1);
            rSysAlert.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlert>>("SystemAlerts").Where(s => s.IsEnabled);
            rSysAlert.DataBind();
            
            if (!WebAdmin2014.Cache.Exists("MenuLinks"))
                WebAdmin2014.Cache.Insert("MenuLinks", Models.MenuLink.selMenuLinks(true),7);
            rQLMenu.DataSource = WebAdmin2014.Cache.Get<List<Models.MenuLink>>("MenuLinks").Where(m => m.isVisible);
            rQLMenu.DataBind();

            if (!WebAdmin2014.Cache.Exists("Versions"))
                WebAdmin2014.Cache.Insert("Versions", Models.Version.selVersions(),1);
            rVersion.DataSource = WebAdmin2014.Cache.Get<List<Models.Version>>("Versions");
            rVersion.DataBind();
            
            //if (!WebAdmin.Cache.Exists("ErrorLog"))
            //    WebAdmin.Cache.Insert("ErrorLog", Models.ErrorLog.selErrorLogs(20), null, DateTime.Now.AddMinutes(20), System.Web.Caching.Cache.NoSlidingExpiration);
            //rError.DataSource = WebAdmin.Cache.Get<T>("ErrorLog"] as List<Models.ErrorLog>;
            //rError.DataBind();

            //if (!WebAdmin.Cache.Exists("Machines"))
            //    WebAdmin.Cache.Insert("Machines", Models.Machine.selMachines(), null, DateTime.Now.AddMinutes(10), System.Web.Caching.Cache.NoSlidingExpiration);
            //rMachine.DataSource = (Cache["Machines"] as List<Models.Machine>).Take(20);
            //rMachine.DataBind();

            if (!WebAdmin2014.Cache.Exists("Apps"))
                WebAdmin2014.Cache.Insert("Apps", Models.Application.selApplications(), 1);
            rQLApp.DataSource = WebAdmin2014.Cache.Get<List<Models.Application>>("Apps").OrderByDescending(a=> a.LaunchCt).Take(10);
            rQLApp.DataBind();

            metrics = Models.UserProfile.selUserMetrics();
            if (!WebAdmin2014.Cache.Exists("Metrics"))
                WebAdmin2014.Cache.Insert("Metrics", Models.UserProfile.selEventLogMetrics(), MinutesTillExpiration: 2); 
            metrics90Day = WebAdmin2014.Cache.Get<Models.UserProfile.UserMetrics>("Metrics");
        }

    }
}
