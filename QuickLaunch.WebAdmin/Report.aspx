﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.Report" EnableEventValidation="false" EnableViewState="false" EnableViewStateMac="false" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Report</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />  
    <%if(!IsExcelOutput) { %>  
    <link href="css/Core.css" rel="stylesheet" type="text/css" />    
    <script src="js/jquery-1.8.1.min.js" type="text/javascript"></script>
    <script src="js/sort.min.js" type="text/javascript"></script>
    <%} %>
    <style type="text/css">
        div#main{background-color:White;color:Black;font-size:.9em;clear:both;padding:0 0 20px 0}
        h1{float:left;margin:0 0 0 5px;font-size:2.25em;padding:1px;}h2{float:left;margin:0px 2px 2px 25px;font-size:1.5em;clear:left;padding:1px;}
        h1 img{cursor:pointer;}
        <%if(CurrentReport != null) { %>
        table#report{clear:both;<%if (!CurrentReport.IsTransposed) { %> width:100%;<%}%>}
        table#report tr{background-color:rgb(245, 245, 245);}
        table#report tr.alt{background-color:rgb(230, 230, 230);}
        table#report tr:hover{background-color:rgb(254, 255, 237);}
        table#report tr th{background-color:#E1E1E1;padding:3px 3px 3px 3px;}
        table#report tr td{padding:2px 2px 2px 2px;text-align:center;}
        <% } %>
        div#footer{margin:15px 0 15px 0;padding:5px 10px 5px 10px;color:dimgray; clear:both;}
    </style>
</head>
<body>
<form id="f1" runat="server">
<div id="main">    
<%if(CurrentReport != null) { %>
    <%if(!IsExcelOutput) { %>  
    <h1><%=CurrentReport.Title%> <a href="?id=<%=id %>&excel=1" title="Export to Excel"><img src="images/excel-icon.gif" alt="Excel" /></a> <img src="images/print-icon.png" alt="Print" title="Print Report" onclick="window.print();" /></h1>        
    <img src="images/baptistLogo.jpg" class="right" alt="Baptist Health Logo"/><h2><%=CurrentReport.SubTitle%></h2>
    <% } %>
    <table id="report">
        <%if (CurrentReport.IsTransposed) { %>            
        <% for(int c = 0;c < CurrentReport.Columns.Count;c++) { %><tr><td><b><%=CurrentReport.Columns[c].Text%></b></td>
        <% for(int r = 0;r < CurrentReport.DataSource.Count;r++) { object value = QuickLaunch.Shared.SysHelper.GetPropValue(CurrentReport.DataSource[r], CurrentReport.Columns[c]); %><td<%=CurrentReport.Columns[c].GetHtmlAttributes(value)%>><%=value%></td><%} %></tr>
        <%} %>
        <% } else { %>
        <tr><% for(int c = 0;c < CurrentReport.Columns.Count;c++) { %><th><%=CurrentReport.Columns[c].Text%></th><%} %></tr>
    <% for(int r = 0;r < CurrentReport.DataSource.Count;r++) { %><tr<%= r % 2 == 0 ? string.Empty : " class='alt'" %>>
        <% for(int c = 0;c < CurrentReport.Columns.Count;c++) { object value = QuickLaunch.Shared.SysHelper.GetPropValue(CurrentReport.DataSource[r], CurrentReport.Columns[c]); %><td<%=CurrentReport.Columns[c].GetHtmlAttributes(value)%>><%=value%></td><%}%></tr><%}%>
    <% } %>
        <% if(CurrentReport.DataSource.Count == 0){ %>
            <tr><td colspan="<%=CurrentReport.Columns.Count%>" class="bgBeige f14">This report is currently empty</td></tr>
        <% } %>
    </table>
    <%if(!IsExcelOutput) { %>  
    <div id="footer"><center><span><%=CurrentReport.DataSource.Count%> row(s) returned</span></center><span class="right">Baptist Health, All Rights Reserved</span><span class="left">Generated <%=DateTime.Now.ToString() %></span></div>
    <script type="text/javascript">
        $().ready(function () {
            $("#report").tablesorter();
        });
    </script>

    <% } %>
<%} %>
</div>
</form>
</body>
</html>
