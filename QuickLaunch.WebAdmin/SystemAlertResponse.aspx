﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemAlertResponse.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.SystemAlertResponse" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<%--    <asp:ScriptManager runat=server ID="sm" ClientIDMode=Static ScriptMode=Release LoadScriptsBeforeUI=false EnableCdn=true /><uc:Progress runat=server id="ucProg" />--%>
<div>
    <asp:UpdatePanel runat=server ID="upAJAX" ClientIDMode=Static><Triggers><asp:PostBackTrigger ControlID="lbExport" /></Triggers><ContentTemplate>
    <h1>Alert Code Responses</h1>
    <div id="search">
    <table>
    <tr><th>By Alert:<br /><asp:DropDownList runat=server ID="ddAlerts" DataTextField="Name" DataValueField="id" /></th><th>By Code:<br /><asp:DropDownList runat=server ID="ddCode" DataTextField="Code" DataValueField="id" /></th><th>By User Id:<br /><input type=text runat=server id="tUser" /></th><th>By Date:<br /><input type=text runat=server ID="tStartDt" EnableViewState=true class="dp" /> to <input type=text runat=server ID="tEndDt" EnableViewState=true class="dp" /></th></tr>
    </table><asp:Button runat=server ID="bSearch" Text="Search Responses" OnClick="bSearch_Click" CssClass="m5" />
    </div>
    <asp:LinkButton runat=server ID="lbExport" CssClass="m5" OnClick="lbExport_Click">Export to Excel</asp:LinkButton>     
    <table id="rAlertCode">
    <asp:Repeater runat=server ID="rSysAlertRes" EnableViewState=false ClientIDMode=Static>
    <HeaderTemplate><tr><th>Code</th><th>Alert Name</th><th>User Id</th><th>Tray Toast Shown?</th><th>OS Bubble Shown?</th><th>Dialog Shown?</th><th>Dialog Accepted?</th><th>Date/Time</th></tr></HeaderTemplate>
    <ItemTemplate><tr id="<%#Eval("id") %>"><td><%#Eval("Code") %></td><td><%#Eval("AlertTitle")%></td><td><%#Eval("NtId")%></td><td><%#Eval("ToastShown")%></td><td><%#Eval("BubbleShown")%></td><td><%#Eval("DialogShown")%></td><td><%#Eval("DialogAccept")%></td><td><%#((DateTime)Eval("lup_dt")).ToString() %></td></tr></ItemTemplate>
    </asp:Repeater>
    <% if(rSysAlertRes.Items.Count == 0){ %>
        <tr><td colspan="8"><h2>No Responses Found</h2>Use search criteria above to filter</td></tr>
    <% } %>
    </table><p><asp:Button runat=server ID="bClearResponses" Text="Delete User Responses" OnClick="bClear_Click" OnClientClick="return confirm('Are you sure you want to permanently remove these user responses?');" CssClass="m5 cR right" /></p>
    </ContentTemplate></asp:UpdatePanel>
</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script type="text/javascript">
    $().ready(function () {        
        <%if(AlertId > 0){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        <% } %>
    });
    function SetGrid(){
        try { $(".dp").datepicker({ numberOfMonths: 2, showButtonPanel: true }); } catch(ex) {}
        //$("table#rAlertCode tr").click(function () { if($(this).index() > 0)popModal("SystemAlertCode.aspx?id=" + $(this).attr('id')); });        
    }
</script>
</asp:Content>