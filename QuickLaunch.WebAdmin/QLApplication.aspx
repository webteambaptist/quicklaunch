﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="QLApplication.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.QLApplication" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<div>
    <asp:MultiView runat=server ID="mv" ClientIDMode=Static>
        <asp:View runat=server ID="vList">
        <a href="#Add" class="right m5">Add Application</a>
        <h1>Quick Launch Applications <small class="f9">(App definitions)</small></h1><asp:UpdatePanel runat=server ID="upAJAX" ClientIDMode=Static><ContentTemplate>
        <table id="rApp">
        <asp:Repeater runat=server ID="rQLApp" EnableViewState=true>
        <HeaderTemplate><tr><th colspan=2>Title</th><th>Value</th><th>Window Title</th><th>Roamable?</th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>"><td><img src="images/menuicons/<%#Eval("DefaultImg") ?? "citrix" %>.png" width="32" height="32"></td><td><%#Eval("Title") %></td><td><%#Eval("Value")%></td><td><%#Eval("WindowTitle")%></td><td><%#Eval("AllowRoaming")%></td></tr></ItemTemplate>
        </asp:Repeater>
        </table><asp:LinkButton runat=server ID="bRefresh" ClientIDMode=Static CssClass="hidden" EnableViewState=false OnClick="bRefresh_Click" />        
        </ContentTemplate></asp:UpdatePanel>
        <a href="#Add" class="right m5">Add Application</a>
        </asp:View>        
        <asp:View runat=server ID="vDetail">
            <h1><%=(this.id == 0 ? "New" : "Update") %> Application</h1>
            <asp:UpdatePanel runat=server ID="UpdatePanel1" ClientIDMode=Static><ContentTemplate>
            <ul id="edit" class="detail">
                <li title="User friendly name"><span>Title:</span><input type=text runat=server id="tTitle" maxlength="150" /></li>                                
                <li title="Application name that Citrix is looking for"><span>Value:</span><input type=text runat=server id="tValue" maxlength="100" /></li>    
                <li title="The window title that should be focused when app is loaded"><span>Window Title:</span><input type=text runat=server id="tWindowTitle" maxlength="250" /></li>                
                <li><span>Default Image:</span><select runat=server id="ddIcon"><option value="citrix">Citrix</option><option value="link">Web Link</option><option value="shield">Power Chart</option><option value="ambulance">First Net</option><option value="drhead">SurgiNet</option><option value="email">E-Mail</option><option value="myportal">Portal</option><option value="intranet">Intranet</option><option value="obtv">OBTV</option><option value="appbar">App Bar</option><option value="powerchart">Power Chart</option><option value="firstnet">FirstNet</option><option value="surginet">SurgiNet</option><option value="eclipse">Purple Eclipse</option><option value="ie">Internet Explorer</option><option value="insight">Insight</option><option value="optimax">OptiMaxx</option><option value="reportreq">Report Request</option></select> <img src="images/menuicons/<%=ddIcon.Value %>.png" id="iIcon" width="25" height="25" align="absmiddle"></li>                    
                <li title="Allow roaming?"><span>Roaming Allowed?:</span><input type=checkbox runat=server id="cbAllowRoaming" checked=checked /></li>    
                <li><span>Last Update User:</span><input type=text runat=server id="tLupUser" disabled=disabled /></li>  
                <li><span>Last Update DateTime:</span><input type=text runat=server id="tLupDT" disabled=disabled /></li>  
            </ul>
            </ContentTemplate></asp:UpdatePanel>
            <asp:Button runat=server ID="bSave" Text="Save Application" OnClick="bSave_Click" CssClass="m5" />
            <%if (this.id > 0) { %>
            <asp:Button runat=server ID="bDelete" Text="Remove Application" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
            <% } %>
        </asp:View>
    </asp:MultiView>
</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script src="js/sort.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $().ready(function () {        
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        $("#ddIcon").change(function(){ $("#iIcon").attr("src","images/menuicons/" + $(this).val() + ".png"); });
        $("#cbAllowRoaming").change(function(){ if($(this).attr("checked") != "checked") alert("WARNING: If this checkbox is not checked users will not be able to roam this application"); });        
        <% } else { %>
        SetGrid();
        <% } %>
    });
    function SetGrid(){
        $("table#rApp tr td").click(function () { if($(this).parent().index() > 0)popModal("QLApplication.aspx?id=" + $(this).parent().attr('id')); });
        $("a[href=#Add]").click(function () { popModal("QLApplication.aspx?id=0"); });
        $("#rApp").tablesorter();
    }    
</script>
</asp:Content>
