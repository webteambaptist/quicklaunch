﻿using System;
using System.Web;

namespace QuickLaunch.WebAdmin2014
{
    public class CacheHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            try
            {
                if (context.Request.QueryString["clear"] != null)
                    Cache.Clear(context.Request.QueryString["clear"] as string);
                                
                context.Response.Write("Cache Updated!");
            }
            catch (Exception ex) { context.Response.Write(string.Format("Error Occurred : {0}", ex.ToString())); }
        }

        #endregion
    }
}
