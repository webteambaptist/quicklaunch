﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.User" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<div>
    <asp:MultiView runat=server ID="mv" ClientIDMode=Static>
        <asp:View runat=server ID="vList">
        <a href="#Add" class="right m5">Add User</a>
        <h1>Current Users</h1><asp:UpdatePanel runat=server ID="upAJAX" ClientIDMode=Predictable><ContentTemplate>
        <asp:TextBox runat=server ID="tFilter" EnableViewState=false /> <asp:Button runat=server ID="bFilter" Text="Filter" OnClick="tFilter_Click" EnableViewState=false />        
        <table id="rUser">
        <asp:Repeater runat=server ID="rUserList" EnableViewState=true>
        <HeaderTemplate><tr><th value="ntID" title="Sort"><span>Login Id</span></th><th value="userType" title="Sort"><span>User Type</span></th><th value="lastLogin" title="Sort"><span>Last Login</span></th><th value="lastLoginLength" title="Sort"><span>Last Login Length</span><br /><asp:LinkButton runat=server ID="lbLastLoginClear" OnClick="lbClear_Click" CommandArgument="lastLoginLength" EnableViewState=true>Clear</asp:LinkButton></th><th value="lastEMRLaunchLength" title="Sort"><span>Last EMR Launch Length</span><br /><asp:LinkButton runat=server ID="lbEMRLaunchClear" EnableViewState=false OnClick="lbClear_Click" CommandArgument="lastEMRLaunchLength">Clear</asp:LinkButton></th><th value="lastRoamLength" title="Sort"><span>Last Roam Length</span><br /><asp:LinkButton runat=server ID="lbRoamClear" EnableViewState=false OnClick="lbClear_Click" CommandArgument="lastRoamLength">Clear</asp:LinkButton></th><th value="roamingCt" title="Sort"><span>Roam Ct</span><br /><asp:LinkButton runat=server ID="lbRoamCtClear" EnableViewState=false OnClick="lbClear_Click" CommandArgument="roamingCt">Clear</asp:LinkButton></th><th value="RoleCt" title="Sort"><span>Roles</span></th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>"><td><%#Eval("ntID") %></td><td><%#Eval("userType") %></td><td><%#Eval("lastLogin") %></td><td><%#(double)Eval("lastLoginLength") / 1000 %></td><td><%#(double)Eval("lastEMRLaunchLength") / 1000%></td><td><%#(double)Eval("lastRoamLength") / 1000%></td><td><%#Eval("roamingCt") %></td><td><%#Eval("RoleCt") %></td></tr></ItemTemplate>
        </asp:Repeater><asp:LinkButton runat=server ID="bRefresh" CssClass="hidden" EnableViewState=false OnClick="bRefresh_Click" /><input type=hidden runat=server id="hideSort" />
        <% if(rUserList.Items.Count == 0){ %>
        <tr><td colspan="8"><h2>No users found</h2></td></tr>
        <% } %>
        </table></ContentTemplate></asp:UpdatePanel>
        <a href="#Add" class="right m5">Add User</a>
        </asp:View>
        <asp:View runat=server ID="vDetail">
            <h1><%=(this.id == 0 ? "New" : "Update") %> User [<%=tUsername.Value%>]</h1>
            <div id="tab-container" class="tab-container">
            <ul class='etabs'>
            <li class='tab'><a href="#tabInfo">Basic Info</a></li>
            <li class='tab'><a href="#tabActivity">Activity</a></li>
            <li class='tab'><a href="#tabError">Latest Errors</a></li>
            </ul>
            <div class="panel-container">
                <div id="tabInfo">
                    <ul id="edit" class="detail">
                        <li><span>Username:</span><input type=text runat=server id="tUsername" clientidmode=Static disabled=disabled maxlength="30" /></li>                
                        <li><span>Email:</span><input type=text runat=server id="tEmail" clientidmode=Static readonly=readonly /></li> 
                        <li title="QL fills this info in based on AD groups"><span>User Type:</span><input type=text runat=server id="tUserType" clientidmode=Static readonly=readonly /></li>
                        <li title="QL fills this in from AD"><span>User Title:</span><input type=text runat=server id="tUserTitle" clientidmode=Static readonly=readonly /></li>
                        <li title="QL fills this in from AD"><span>User Department:</span><input type=text runat=server id="tUserDept" clientidmode=Static readonly=readonly /></li>
                        <li title="TITO Accessibility/Security"><span>Roles:</span><asp:CheckBoxList runat=server ID="cblRoles" DataTextField="Role" DataValueField="id" /></li>                                
                        <li><span>Join Date:</span><input type=text runat=server id="tJoinDT" clientidmode=Static readonly=readonly /></li> 
                        <li><span>Last Login Date:</span><input type=text runat=server id="tLastLoginDt" clientidmode=Static readonly=readonly /></li> 
                        <li><span>Last Login Machine:</span><input type=text runat=server id="tLastLoginMachine" clientidmode=Static readonly=readonly /> <label runat=server id="lLocation"></label></li> 
                        <li><span>Last Login Length:</span><input type=text runat=server id="tLastLoginLength" clientidmode=Static readonly=readonly /></li>                 
                        <li><span>Last Roam Length:</span><input type=text runat=server id="tLastRoamLength" clientidmode=Static readonly=readonly /></li> 
                        <li><span>Last EMR Launch Length:</span><input type=text runat=server id="tLastEMRLaunchLength" clientidmode=Static readonly=readonly /></li> 
                        <li><span>Roam Count:</span><input type=text runat=server id="tRoamCt" clientidmode=Static readonly=readonly /></li> 
                        <li><span>Power Chart Launch Ct:</span><input type=text runat=server id="tPCLaunchCt" clientidmode=Static readonly=readonly /></li> 
                        <li><span>FirstNet Launch Ct:</span><input type=text runat=server id="tFirstNetLaunchCt" clientidmode=Static readonly=readonly /></li> 
                        <li><span>SurgiNet Launch Ct:</span><input type=text runat=server id="tSurgiNetLaunchCt" clientidmode=Static readonly=readonly /></li> 
                        <li><span>Last Metric Clear Ct:</span><input type=text runat=server id="tMetricClear" clientidmode=Static readonly=readonly /></li> 
                        <li><span>Last Update:</span><input type=text runat=server id="tLastUpdate" clientidmode=Static readonly=readonly /></li> 
                    </ul>
                    <asp:Button runat=server ID="bSave" Text="Save User" OnClick="bSave_Click" CssClass="m5" />
                </div>
                <div id="tabActivity"><h3>User Activity</h3>
                    <uc:EventChart runat=server ID="ecActivity" /><h3>Latest Activity <small>(max 100)</small></h3>
                    <table id="tActivity" class="f9">
                    <asp:Repeater runat=server ID="rActivity" EnableViewState=false ClientIDMode=Static>
                    <HeaderTemplate><tr><th>Event</th><th>Time Taken</th><th>S/N</th><th>Date/Time</th></tr></HeaderTemplate>
                    <ItemTemplate><tr id="<%#Eval("id") %>" title="<%#Eval("Application") %>"><td><%#Eval("EventCode") %></td><td><%#(Convert.ToDouble(Eval("LengthOfTime")) / 1000).ToString("N4") + " sec" %></td><td><%#Eval("Machine")%></td><td><%#((DateTime)Eval("lup_dt")) %></td></tr></ItemTemplate>
                    </asp:Repeater>
                    <% if(rActivity.Items.Count == 0){ %>
                        <tr><td colspan="4"><h2>No Activity Available</h2></td></tr>
                    <% } %>
                    </table>     
                </div>
                <div id="tabError"><h3>Most Recent Errors</h3>
                    <table id="rError" class="f9">
                    <asp:Repeater runat=server ID="rErrors" EnableViewState=false ClientIDMode=Static>
                    <HeaderTemplate><tr><th>Date/Time</th><th>Application</th><th>S/N</th><th>Exception</th></tr></HeaderTemplate>
                    <ItemTemplate><tr id="<%#Eval("id") %>"><td><%#((DateTime)Eval("lup_dt")) %></td><td<%# (((bool)Eval("IsFatal")) ? " class='cR'" : "") %>><%#Eval("Application") %></td><td><%#Eval("Serial")%></td><td class="tLeft"><%#(((string)Eval("Exception")).Length >= 350 ? ((string)Eval("Exception")).Substring(0,349) : Eval("Exception")) %></td></tr></ItemTemplate>
                    </asp:Repeater>
                    <% if(rErrors.Items.Count == 0){ %>
                        <tr><td colspan="4"><h2>No Errors to Report!</h2></td></tr>
                    <% } %>
                    </table>            
                </div>
            </div>
            </div>
        </asp:View>
        <asp:View runat=server ID="vRoles">        
        <h1>Current User Roles</h1>
        <table id="tblRole" class="w70">
        <asp:Repeater runat=server ID="rRole" EnableViewState=false OnItemDataBound="rRole_IDB">
        <HeaderTemplate><tr><th>Role</th><th>Users</th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>"><td class="bold"><%#Eval("role") %></td>
        <td class="tLeft" title="Click to view user"><ul>
            <asp:Repeater runat=server ID="rUsers">
                <ItemTemplate><li id="<%#Eval("id") %>"><%#Eval("ntID") %></li></ItemTemplate>
            </asp:Repeater>
            </ul>
        </td>        
        </tr></ItemTemplate>
        </asp:Repeater>
        </table>
        </asp:View>
    </asp:MultiView>
</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<%if(mv.ActiveViewIndex == 1){ %>
<script src="js/jquery-easytabs.min.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
<script language="javascript" type="text/javascript" src="js/flot/jquery.flot.stack.min.js"></script>
<script language="javascript" type="text/javascript" src="js/flot/EventLogSnapshotChart.js"></script>
<% } %>
<script type="text/javascript">
    $().ready(function () {
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        $("#tUsername").change(function () { $('#tEmail').val($(this).val() + "@bmcjax.com"); });
        $("#tab-container").easytabs();
        user = "<%=tUsername.Value%>";
        if(user.length > 0)
            RefreshChart(); 
        else {
            $("a[href=#tabActivity]").parent().hide();
            $("a[href=#tabError]").parent().hide();
        }
        <% } else if(mv.ActiveViewIndex == 0) { %>
        SetGrid();
        <% } else if(mv.ActiveViewIndex == 2) {%>
        $("table#tblRole ul li").click(function () { popModal("User.aspx?id=" + $(this).attr('id')); });
        $("table#tblRole ul li").css("cursor", "pointer");
        <% } %>
    });
    function SetGrid(){
        $("table#rUser tr").click(function () { if($(this).index() > 0)popModal("User.aspx?id=" + $(this).attr('id')); });
        $("table#rUser tr th span").click(function () { $('#hideSort').val($(this).parent().attr('value')); document.getElementById("bRefresh").click(); });        
        $("table#rUser tr th a").click(function () { return confirm('This action, for all of the listed rows, will clear the column value to zero.\n\nAre you sure you want to continue?'); });
        $("a[href=#Add]").click(function () { popModal("User.aspx?id=0"); });

    }
</script>

</asp:Content>