﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class QLMenu : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 1;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New Item
                    mv.ActiveViewIndex = 1;
                    ddApplication.Enabled = false;
                    bAddApp.Enabled = false;
                }
                else
                    LoadMenu();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.MenuLink ml = Models.MenuLink.selMenuLinkById(id);
            if (ml == null) return;
            tName.Value = ml.name;
            ddIcon.Value = ml.img;
            tToolTip.Value = ml.toolTip;
            ddAudience.Value = ml.audience;
            tAdGroupFilter.Value = ml.adGroupFilter;
            ddAction.Value = ml.clickEvent;
            if(ddAction.SelectedIndex == 0)
                tClickEvent.Value = ml.clickEvent;            
            cbIsVisible.Checked = ml.isVisible;
            ddApplication.DataSource = Models.Application.selApplications(); ddApplication.DataBind();
            rApplications.DataSource = Models.MenuApplication.selApplicationsByMenuId(ml.id); rApplications.DataBind();

        }
        private void LoadMenu()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("MenuLinks"))
                WebAdmin2014.Cache.Insert("MenuLinks", Models.MenuLink.selMenuLinks(true), 7);
            rQLMenu.DataSource = WebAdmin2014.Cache.Get<List<Models.MenuLink>>("MenuLinks");
            rQLMenu.DataBind();
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.MenuLink ml = new Models.MenuLink();
            ml.id = id;
            ml.name = tName.Value;
            ml.img = ddIcon.Value;
            ml.toolTip = tToolTip.Value;
            ml.clickEvent =  ddAction.SelectedIndex > 0 ? ddAction.Value : tClickEvent.Value;
            ml.audience = ddAudience.Value;
            ml.adGroupFilter = tAdGroupFilter.Value;
            ml.isVisible = cbIsVisible.Checked;            
            //ml.lup_user = common.user.ntID;
            ml.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    ml.orderId = Models.MenuLink.selMenuLinks(true).Count + 1;
                    Models.MenuLink.insMenuLink(ml);
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Menu Link Add Failed!');"); return; }
                tName.Value = string.Empty;
                tToolTip.Value = string.Empty;
            }
            else
            {
                List<Models.MenuApplication> ma = new List<Models.MenuApplication>();
                try
                {
                    foreach (RepeaterItem i in rApplications.Items)
                        ma.Add(new Models.MenuApplication { id = Convert.ToInt32(((LinkButton)i.FindControl("lbDeleteApp")).CommandArgument), Order_Id = Convert.ToInt32(((System.Web.UI.HtmlControls.HtmlInputText)i.FindControl("cbAppOrder")).Value) });

                    Models.MenuApplication.updReorderAppsForMenuItem(ma.OrderBy(o => o.Order_Id).ToList());
                } catch(Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); }

                try
                {
                    Models.MenuLink.updMenuLink(ml);                  
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Menu Link Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Menu Link Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("MenuLinks");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadMenu();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void bUpdOrder_Click(object sender, EventArgs e)
        {
            List<Models.MenuLink> mls = new List<Models.MenuLink>();
            try
            {
                foreach (RepeaterItem i in rQLMenu.Items)
                    mls.Add(new Models.MenuLink { id = Convert.ToInt32(((System.Web.UI.HtmlControls.HtmlInputHidden)i.FindControl("hideId")).Value), orderId = Convert.ToInt32(((System.Web.UI.HtmlControls.HtmlInputText)i.FindControl("tOrderId")).Value) });

                Models.MenuLink.updMenuLinkOrder(mls.OrderBy(o => o.orderId).ToList());
            }
            catch { common.SendAJAX_PopupMsg(Page, this.GetType(), "Menu Re-order Failed! Please ensure all values are numeric."); }
            WebAdmin2014.Cache.Clear("MenuLinks");
            bRefresh_Click(sender, e);
        }
        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.MenuLink.delMenuLink(new Models.MenuLink { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Menu Link Removed!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("MenuLinks");
            bRefresh_Click(sender, e);
        }
        protected void bAddApp_Click(object sender, EventArgs e)
        {            
            Models.MenuApplication.insMenuApplication(new Models.MenuApplication { MenuLink_Id = id, Application_Id = Convert.ToInt32(ddApplication.SelectedValue), Order_Id = rApplications.Items.Count + 1 });
            rApplications.DataSource = Models.MenuApplication.selApplicationsByMenuId(id);
            rApplications.DataBind();
        }
        protected void lbDeleteApp_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            Models.MenuApplication.delMenuApplication(new Models.MenuApplication { id = Convert.ToInt32(lb.CommandArgument) });
            rApplications.DataSource = Models.MenuApplication.selApplicationsByMenuId(id);
            rApplications.DataBind();
        }  
                
    }
}