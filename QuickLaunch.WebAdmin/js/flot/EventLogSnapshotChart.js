﻿var choiceContainer = $("#choices"); var datasets; var user = ""; var machine = "";
function RefreshChart() {
    ajax("GetEventLog", "{\"groupBy\":\"" + $("#ddData").val() + "\",\"userId\":\"" + user + "\",\"machine\":\"" + machine + "\"}", function (data) {
        datasets = data.GetEventLogResult;
        //        var datasets = {
        //            "logins": {
        //                label: "Logins",
        //                data: [[1994, 377867], [1995, 357382], [1996, 337946], [1997, 336185], [1998, 328611], [1999, 329421], [2000, 342172], [2001, 344932], [2002, 387303], [2003, 440813], [2004, 480451], [2005, 504638], [2006, 528692]]
        //            },
        //            "roams": {
        //                label: "Roams",
        //                data: [[1994, 36600], [1995, 21700], [1996, 19200], [1997, 21300], [1998, 13600], [1999, 14000], [2000, 19100], [2001, 21300], [2002, 23600], [2003, 25100], [2004, 26100], [2005, 31100], [2006, 34700]]
        //            },
        //            "launches": {
        //                label: "Launches",
        //                data: [[1994, 54579], [1995, 50818], [1996, 50554], [1997, 48276], [1998, 47691], [1999, 47529], [2000, 47778], [2001, 48760], [2002, 50949], [2003, 57452], [2004, 60234], [2005, 60076], [2006, 59213]]
        //            },
        //            "disconnects": {
        //                label: "Disconnects",
        //                data: [[1994, 43962], [1995, 43238], [1996, 42395], [1997, 40854], [1998, 40993], [1999, 41822], [2000, 41147], [2001, 40474], [2002, 40604], [2003, 40044], [2004, 38816], [2005, 38060], [2006, 36984]]
        //            }
        //        };
        var i = 0;
        $.each(datasets, function (key, val) {
            val.color = i;
            ++i;
        });
        choiceContainer.html("");
        $.each(datasets, function (key, val) { // insert checkboxes 
            choiceContainer.append('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="' + key +
                               '" checked="checked" id="id' + key + '"> ' +
                               '<label for="id' + key + '">'
                                + val.label + '</label>');
        });
        choiceContainer.find("input").click(plotWithOptions);
        plotWithOptions();
    });
}
$("#ddData").change(RefreshChart);
$(".graphControls input[type=checkbox]").click(plotWithOptions);
var stack = 0, bars = true, lines = false, filled = false;
function plotWithOptions() {
    var data = [];
    choiceContainer.find("input:checked").each(function () {
        var key = $(this).attr("name");
        if (key && datasets[key])
            data.push(datasets[key]);
    });

    stack = $("#isStacked").is(':checked') ? true : null;
    filled = $("#isFilled").is(':checked') ? true : null;
    var _minTickSize = ""; var _min = new Date(); var _max = new Date();
    var bWidth = 0.8;

    if ($("#ddData").val() == "Week" || $("#ddData").val() == "Month") {
        bWidth = 100 * 500 * 1000;
        _minTickSize = "day";
//        _min.setDate(_min.getDate() - 6);
//        _max.setDate(_max.getDate() + 1);          
    }
    else if ($("#ddData").val() == "Day") {
        bWidth = 20 * 100 * 1000;
        _minTickSize = "hour";
    }
    else {
        _minTickSize = "week";
        _min = 1; _max = 52;
    }
    $.plot($("#placeholder"), data, { //[d1, d2, d3]
        series: {
            stack: stack,
            lines: { show: lines, fill: filled, steps: null },
            bars: { show: bars, barWidth: bWidth }
        },
        yaxis: { min: 0 },
        xaxis: { tickDecimals: 0, mode: (bWidth > 0 && _minTickSize != "week" ? "time" : null), minTickSize: [1, _minTickSize], twelveHourClock: true }, //, timeformat: "%m/%d/%y", , twelveHourClock: true //$("#ddData").val().toLowerCase()
        grid: { hoverable: true, clickable: false },
        legend: { backgroundOpacity: .3, margin: 1 }
    });
}

$(".graphControls input[type=button]").click(function (e) {
    e.preventDefault();
    bars = $(this).val().indexOf("Bars") != -1;
    lines = $(this).val().indexOf("Lines") != -1;
    plotWithOptions();
});

function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        position: 'absolute',
        display: 'none',
        'border-radius': '5px',
        top: y + 15,
        left: x + 25,
        border: '1px solid #fdd',
        padding: '2px',
        'background-color': '#fee',
        opacity: 0.90
    }).appendTo("body").fadeIn(200);
}

var previousPoint = null;
$("#placeholder").bind("plothover", function (event, pos, item) {
    $("#x").text(pos.x.toFixed(2));
    $("#y").text(pos.y.toFixed(2));
    if (item) {
        var x = item.datapoint[0].toFixed(0),
                    y = item.datapoint[1].toFixed(0);

        if (previousPoint != x + "_" + y) {
            previousPoint = x + "_" + y;
            $("#tooltip").remove();
            var currDate = new Date(Math.floor(x));
            currDate.setHours(currDate.getHours() + 5, 0, 0, 0); //offset fix
            showTooltip(item.pageX, item.pageY,
                           "<b>" + (lines ? y : (item.datapoint[1] - item.datapoint[2]).toFixed(0)) + " " + item.series.label + "</b><br />" + (currDate.getYear() > 100 ? currDate.toDateString() + ($("#ddData").val().toLowerCase() == "hour" ? " " + currDate.toTimeString() : "") : "Week " + x));
        }
    }
    else {
        $("#tooltip").remove();
        previousPoint = null;
    }
});
//        $("#placeholder").bind("plotclick", function (event, pos, item) {
//            if (item) {
//                $("#clickdata").text("You clicked point " + item.dataIndex + " in " + item.series.label + ".");
//                plot.highlight(item.series, item.datapoint);
//            }
//        });