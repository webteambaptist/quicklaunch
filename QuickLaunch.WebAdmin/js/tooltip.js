document.write("<style type='text/css'>"); //style for tool tip
document.write(" .tooltiptitle{COLOR: #FFFFFF; TEXT-DECORATION: none; CURSOR: Default; font-family: arial; font-weight: bold; font-size: 8pt; padding:5px;}");
document.write(" .tooltipcontent{COLOR: #000000; TEXT-DECORATION: none; CURSOR: Default; font-family: arial; font-size: 8pt;padding:5px;}");
document.write(" #ToolTip{position:absolute; width: 220px; top: 0px; left: 0px; z-index:4; visibility:hidden;}");
document.write("</style>");

ContentInfo = "";
topColor = "#6588aa";
subColor = "whitesmoke";
var mouse_X;
var mouse_Y;
var tip_active = 0;
var adjustment = "Left"; //align the box
var ie = document.all ? true : false;
if (document.captureEvents && this.Event && Event.onClick) {
    document.captureEvents(Event.onClick);
}
document.onmousemove = getMouseXY;

function GetScroll(axis) {
    var scroll_pos;
    if (axis == "X") {
        scroll_pos = document.body.scrollLeft;
        if (scroll_pos == 0) {
            if (window.pageXOffset)
                scroll_pos = window.pageXOffset;
            else
                scroll_pos = (document.body.parentElement) ? document.body.parentElement.scrollLeft : 0;
        }
    } else { // Y
        scroll_pos = document.body.scrollTop;
        if (scroll_pos == 0) {
            if (window.pageYOffset)
                scroll_pos = window.pageYOffset;
            else
                scroll_pos = (document.body.parentElement) ? document.body.parentElement.scrollTop : 0;
        }
    }
    return scroll_pos;
}

function update_tip_pos() {        
	document.getElementById('ToolTip').style.left = (mouse_X + 10) + ((ie) ? "" : "px"); //gives space to mouse position	
	document.getElementById('ToolTip').style.top = (mouse_Y + 10) + ((ie) ? "" : "px"); //Adds scroll position to current screen position
	//alert(document.getElementById('ToolTip').style.top + "_" + mouse_Y + " ---- " + document.getElementById('ToolTip').style.left + "_" + mouse_X);
}

function getMouseXY(e) {
    try {
        if (ie) {
            mouse_X = event.clientX + GetScroll("X");
            mouse_Y = event.clientY + GetScroll("Y");

        } else {
            mouse_X = e.clientX + GetScroll("X");
            mouse_Y = e.clientY + GetScroll("Y");
        }
    } catch(ex){mouse_X = 0;mouse_Y = 0;}

    if(tip_active){update_tip_pos();}
}

function EnterContent(TTitle, TContent) { //
    ContentInfo = '<div style="border-radius: 4px; -moz-border-radius: 4px;border:solid 1px ' + topColor + ';">' +
        '<div class="tooltiptitle" style="background-color:' + topColor + '">' + TTitle + '</div>' +
        '<div class="tooltipcontent" style="background-color:' + subColor + '">' + TContent + '</div>' +
    '</div>';
}

function tip_it(which, TTitle, TContent){
	if(which){
		update_tip_pos();
		tip_active = 1;
		document.getElementById('ToolTip').style.visibility = "visible";
		EnterContent(TTitle, TContent);
		document.getElementById('ToolTip').innerHTML = ContentInfo;
	}else{
		tip_active = 0;
		document.getElementById('ToolTip').style.visibility = "hidden";
	}
}

function getTip(obj) {
    switch (obj.id) {
        case "unqID":
            tip_it(1, 'Unique ID', 'The unique id given when the sub topic was added to the Baptist database');
            break;
        case "stName":
            tip_it(1, 'SubTopic Name', 'The title and descriptive name of the subtopic');
            break;
        case "fURL":
            tip_it(1, 'Friendly URL', 'This URL will be how the browser interprets this particular subtopics folder location. No spaces are encouraged.');
            break;
        case "metaDesc":
            tip_it(1, 'META Description', 'Information that search engines will parse regarding this subtopics purpose');
            break;
        case "metaKey":
            tip_it(1, 'META Keywords', 'Descriptive words separated by spaces or commas that describe this subtopic');
            break;
        case "stDesc":
            tip_it(1, 'Description', 'Intro text that describes the subtopic');
            break;
        case "stSWID":
            tip_it(1, 'Staywell ID', 'This is the Staywell location that will be checked for new documents when this subtopic is loaded.');
            break;
        case "dTitle":
            tip_it(1, 'Title', 'Document title thatll show in the browser title bar');
            break;
        case "dIncl":
            tip_it(1, 'Included?', 'Determines whether or not the front-end should show this particular document in lists or search results');
            break;
        case "dOverride":
            tip_it(1, 'Override Staywell Content', 'If checked then the custom content in the box will be returned for the doc.  Otherwise the most recent copy in Staywell is returned.');
            break;
        case "dPub":
            tip_it(1, 'Date to Publish', 'The date the docoument should become visible');
            break;
        case "dExp":
            tip_it(1, 'Date to Expire', 'The date the document should no longer be visible');
            break;
        case "dBlurb":
            tip_it(1, 'Blurb', 'Intro text that describes the main content contained in the document');
            break;
        case "dCont":
            tip_it(1, 'Content', 'Text or images that make up the document');
            break;
        case "sName":
            tip_it(1, 'Name', 'Site Name that should be displayed');
            break;
        case "sURL":
            tip_it(1, 'URL', 'Base URL to the site');
            break;
        case "sDesc":
            tip_it(1, 'Description', 'Text that best describes the site');
            break;
        case "sAPIKey":
            tip_it(1, 'API Key', 'The unique identifier of the site that will need to be passed in order to use the admin provided API.');
            break;
        case "uNTID":
            tip_it(1, 'NT ID', 'The users worstation login. Domain not requried at this time.');
            break;
        case "uEmail":
            tip_it(1, 'Email', 'The preferred email address that the user would like to be sent notifications');
            break;
        case "uAccess":
            tip_it(1, 'User Access', 'The access matrix that the user will be able to access and their associated role in that site. Checking a site makes them a contributor for that site, and checking the publisher checkbox allows them to approve contributions.');
            break;
        default:
            break;
    }
    obj.onmouseout = function () { tip_it(0, '', ''); };
}

document.write("<div id='ToolTip'></div>"); //Do not delete...this is the tool tip place holder
