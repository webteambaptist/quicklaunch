﻿function StringBuilder(value) { this.strings = new Array(""); this.append(value); }
StringBuilder.prototype.append = function (value) { if (value) { this.strings.push(value); } }
StringBuilder.prototype.clear = function () { this.strings.length = 1; }
StringBuilder.prototype.toString = function () { return this.strings.join(""); }
function searchClick(obj) {if (obj.value == "Search...") obj.value = "";}
function searchBlur(obj) {if (obj.value.length == 0) obj.value = "Search...";}
function ConfirmDel(t) { return confirm("Are you sure you want to remove this " + t + "?"); }
function launchCenter(url, name, height, width) {
    var str = "height=" + height + ",innerHeight=" + height;
    str += ",width=" + width + ",innerWidth=" + width;
    if (window.screen) {
        var ah = screen.availHeight - 30;
        var aw = screen.availWidth - 10;
        var xc = (aw - width) / 2;
        var yc = (ah - height) / 2;
        str += ",left=" + xc + ",screenX=" + xc;
        str += ",top=" + yc + ",screenY=" + yc;
    }
    str += ",resizable,scrollbars";
    return window.open(url, name, str);
}
function checkKeyCode(obj, e) { if (e.which == '46') { obj.value = ""; } } //check for delete key      
function DropDown_OnKeyPress(dropdownlist, caseSensitive, e) {
    var undefined;
    if (dropdownlist.keypressBuffer == undefined) { dropdownlist.keypressBuffer = ''; }
    var key = String.fromCharCode(e.which);
    if (e.which == '8')
        dropdownlist.keypressBuffer = dropdownlist.keypressBuffer.substring(0, dropdownlist.keypressBuffer.length - 1);
    else
        dropdownlist.keypressBuffer += key;
    if (!caseSensitive){dropdownlist.keypressBuffer = dropdownlist.keypressBuffer.toLowerCase();}
    var optionsLength = dropdownlist.options.length;
    for (var n = 0; n < optionsLength; n++) {
        var optionText = dropdownlist.options[n].text;
        if (!caseSensitive) { optionText = optionText.toLowerCase(); }
        if (optionText.indexOf(dropdownlist.keypressBuffer, 0) == 0) {
            dropdownlist.selectedIndex = n;
            return false;
        }
    } dropdownlist.keypressBuffer = key;
    return true; // give default behavior
}
$("select").keypress(function (e) { return DropDown_OnKeyPress(this, false, e); });
$("select").keyup(function (e) { return checkKeyCode(this, e); });
function ajax(method, pData, successFn) {
    $.ajax({
        type: "POST",
        url: "./ajax.svc/" + method,     
        dataType: 'json',   
        data: pData,
        contentType: "application/json; charset=utf-8",
        success: successFn,
        error: function (e) { alert('AJAX Error Occurred: ' + e.toString()); }
    });
}
function notify(text, speed) { $("#notification").html(text).slideDown(1200); setTimeout("$('#notification').slideUp(700)", (speed ? speed : 3500)); }
function popModal(url) {
    //$("#imodal iframe").width($(window).width() - 150); 
    $("#imodal iframe").height($(window).height() - 110);
    $("#imodal iframe").attr("src", url);
    $("#imodal").modal({ overlayClose: true, onClose: function (dialog) { CloseModal(); },
        onOpen: function (dialog) {
	        dialog.overlay.fadeIn(200, function () {
		        dialog.data.hide();
		        dialog.container.fadeIn(200, function () {
			        dialog.data.slideDown(200);	 
		        });
	        });
        }
    });
}
function CloseModal() {$("#imodal iframe").html(); $("#imodal iframe").attr("src", "about:blank"); $.modal.close(); }
function RunReport(params) {
    url = "Report.aspx?" + params;    
    window.open(url, "_blank", "menubar=0,toolbar=0,top=10,left=10", false);
}
function IsNumeric(input) { return (input - 0) == input && input.length > 0; }
function async_load(script) {
    var s = document.createElement('script');
    s.type = 'text/javascript';
    s.async = true;
    s.src = script;
    var x = document.getElementsByTagName('script')[0];
    x.parentNode.insertBefore(s, x);
}