﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class Reporting : BasePages.SecureUserPage
    {
        #region Properties
        public static int RequiredRoleId = 11;
        public static int RequiredAdminRoleId = 12;
        public static bool IsReportingAdmin { get { try { return common.user.Roles.Where(r => r.id == RequiredAdminRoleId).Any(); } catch { return false; } } }
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }

        public static List<Models.HashList> hlDefinitions = new List<Models.HashList> {
             new Models.HashList { Title = "Custom", Value = "<report>\r\n\t<parameters>\r\n\t\t<param name=\"\" value=\"\" />\r\n\t</parameters>\r\n\t<columns>\r\n\t\t<column src=\"\" text=\"\" format=\"\" />\r\n\t</columns>\r\n</report>" }
            ,new Models.HashList { Title = "SlowMachines", Value = "<report>\r\n\t<parameters>\r\n\t\t<param name=\"LoginLengthLower\" value=\"3\" />\r\n\t\t<param name=\"RoamLengthLower\" value=\"20\" />\r\n\t\t<param name=\"LaunchLengthLower\" value=\"40\" />\r\n\t</parameters>\r\n\t<columns>\r\n\t\t<column src=\"id\" text=\"id\" />\r\n\t\t<column src=\"PCName\" text=\"S/N\" />\r\n\t\t<column src=\"LSID\" text=\"LSID\" />\r\n\t\t<column src=\"Description\" text=\"Description\" />\r\n\t\t<column src=\"Memory\" text=\"Memory\" />\r\n\t\t<column src=\"WTSLocation\" text=\"WTS Location\" />\r\n\t\t<column src=\"AvgLoginLength\" text=\"AVG Login Length\" />\r\n\t\t<column src=\"AvgRoamLength\" text=\"AVG Roam Length\" />\r\n\t\t<column src=\"AvgEMRLaunchLength\" text=\"AVG EMR Launch Length\" />\r\n\t</columns>\r\n</report>" }
            ,new Models.HashList { Title = "EventLogMetrics", Value = "<report>\r\n\t<parameters>\r\n\t</parameters>\r\n\t<columns>\r\n\t\t<column src=\"EventCt\" text=\"Total # of Events\" />\r\n\t\t<column src=\"LoginCt\" text=\"Login Ct\" />\r\n\t\t<column src=\"SumLoginLength\" text=\"Total Login Length\" />\r\n\t\t<column src=\"AvgLoginLength\" text=\"Avg Login Length\" />\r\n\t\t<column src=\"SumRoamCt\" text=\"Roam Ct\" />\r\n\t\t<column src=\"SumRoamLength\" text=\"Total Roam Length\" />\r\n\t\t<column src=\"AvgRoamLength\" text=\"Avg Roam Length\" />\r\n\t\t<column src=\"SumEMRLaunchLength\" text=\"Total EMR Launch Length\" />\r\n\t\t<column src=\"AvgEMRLaunchLength\" text=\"Avg EMR Launch Length\" />\r\n\t\t<column src=\"SumRoamLaunchSavings\" text=\"Total Roam/Launch Savings\" />\r\n\t\t<column src=\"AvgRoamLaunchSavings\" text=\"Avg Roam/Launch Savings\" />\r\n\t\t<column src=\"SumPCLaunchCt\" text=\"Power Chart Launch Ct\" />\r\n\t\t<column src=\"SumFNLaunchCt\" text=\"FirstNet Launch Ct\" />\r\n\t\t<column src=\"SumSNLaunchCt\" text=\"SurgiNet Launch Ct\" />\r\n\t\t<column src=\"SumAutoLaunchCt\" text=\"Auto Launch Ct\" />\r\n\t\t<column src=\"SumNewUsersCt\" text=\"New Users Ct\" />\r\n\t\t<column src=\"SumMachineCt\" text=\"QL Machine Ct\" />\r\n\t</columns>\r\n</report>" }
            ,new Models.HashList { Title = "InvalidICA", Value = "<report>\r\n\t<parameters>\r\n\t</parameters>\r\n\t<columns>\r\n\t\t<column src=\"id\" text=\"id\" />\r\n\t\t<column src=\"PCName\" text=\"S/N\" />\r\n\t\t<column src=\"LSID\" text=\"LSID\" />\r\n\t\t<column src=\"Description\" text=\"Description\" />\r\n\t\t<column src=\"WTSLocation\" text=\"WTS Location\" />\r\n\t\t<column src=\"QLVersion\" text=\"QLVersion\" />\r\n\t\t<column src=\"ICAVersion\" text=\"ICAVersion\" />\r\n\t\t<column src=\"LastLogin\" text=\"LastLogin\" />\r\n\t</columns>\r\n</report>" }
            };
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page
                if (id > 0)
                    LoadDetail();
                else if (id == 0)
                { //New Item
                    if (Request.QueryString["copy"] != null)
                    {
                        Models.Report x = Models.Report.selReportById(Convert.ToInt32((string)Request.QueryString["copy"]));
                        tTitle.Value = x.Title;
                        tSubTitle.Value = x.SubTitle;
                    }
                    tDefinition.Value = hlDefinitions.Find(d => d.Title == "Custom").Value;
                    tStartDt.Value = DateTime.Now.ToString();
                    mv.ActiveViewIndex = 1;
                    Session.Remove("Reports");
                }
                else
                    LoadReports();
                
            }
        }

        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.Report x = Models.Report.selReportById(id);
            if (x == null) return;
            
            tTitle.Value = x.Title;
            tSubTitle.Value = x.SubTitle ?? string.Empty;
            ddType.Value = x.Type ?? string.Empty;
            if(IsReportingAdmin) tSql.Value = x.Sql ?? string.Empty;
            tDefinition.Value = x.Definition ?? string.Empty;
            ddFrequency.Value = x.Frequency;
            tStartDt.Value = x.StartDt.ToString();
            cbIsRunSun.Checked = x.IsRunSun;
            cbIsRunMon.Checked = x.IsRunMon;
            cbIsRunTue.Checked = x.IsRunTues;
            cbIsRunWed.Checked = x.IsRunWed;
            cbIsRunThur.Checked = x.IsRunThur;
            cbIsRunFri.Checked = x.IsRunFri;
            cbIsRunSat.Checked = x.IsRunSat;
            tRecipients.Value = x.EmailRecipients ?? string.Empty;
            tCC.Value = x.EmailCC ?? string.Empty;
            tSubject.Value = x.EmailSubject ?? string.Empty; 
            tMsg.Value = x.EmailMsg ?? string.Empty; 
            cbIsExcelAttachment.Checked = x.IsExcelAttachment;
            cbIsTransposed.Checked = x.IsTransposed;
            tNotes.Value = x.Notes ?? string.Empty;
            tLastRunDt.Value = x.LastRunDt.ToString().Contains("0001") ? "Never" : x.LastRunDt.ToString();
            tLastSentDt.Value = x.LastSentDt.ToString().Contains("0001") ? "Never" : x.LastSentDt.ToString(); ;
            tAddDt.Value = x.add_dt.ToShortDateString();
            tLupUser.Value = x.lup_user;
            tLupDT.Value = x.lup_dt.ToString();
            rHistory.DataSource = Models.Report.selReportHistory(x.id); rHistory.DataBind();
        }
        private void LoadReports()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Reports"))
                WebAdmin2014.Cache.Insert("Reports", Models.Report.selReports(), 5);
            rReporting.DataSource = WebAdmin2014.Cache.Get<List<Models.Report>>("Reports");
            rReporting.DataBind();
        }
        private void BuildReport()
        {
            //Models.Report x = Models.Report.selReportById(id);
            //Response.Buffer = true;
            //Response.ContentType = x.MSIBuildContentType;
            //Response.AddHeader("Content-Disposition", "attachment;filename=Setup.QuickLaunch.msi");
            //Response.BinaryWrite(x.MSIBuild);
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Report a;
            if (id > 0)
                a = Models.Report.selReportById(id);
            else
                a = new Models.Report();
            a.id = id;
            a.Title = tTitle.Value;
            a.SubTitle = tSubTitle.Value;
            a.Type = ddType.Value;
            a.Sql = tSql.Value;
            a.Definition = tDefinition.Value;
            a.Frequency = ddFrequency.Value;
            try { a.StartDt = Convert.ToDateTime(tStartDt.Value); } catch { }
            a.IsRunSun = cbIsRunSun.Checked;
            a.IsRunMon = cbIsRunMon.Checked;
            a.IsRunTues = cbIsRunTue.Checked;
            a.IsRunWed = cbIsRunWed.Checked;
            a.IsRunThur = cbIsRunThur.Checked;
            a.IsRunFri = cbIsRunFri.Checked;
            a.IsRunSat = cbIsRunSat.Checked;
            a.EmailRecipients = tRecipients.Value;
            a.EmailCC = tCC.Value;
            a.EmailSubject = tSubject.Value;
            a.EmailMsg = tMsg.Value;
            a.IsExcelAttachment = cbIsExcelAttachment.Checked;
            a.IsTransposed = cbIsTransposed.Checked;
            a.Notes = tNotes.Value;
            a.lup_user = common.user.ntID;
            a.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    int dId = Convert.ToInt32(Models.Report.insReport(a));
                }
                catch(Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Report Add Failed!');"); return; }
            }
            else
            {
                try
                {
                    Models.Report.updReport(a);                  
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Report Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Report Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Reports");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadReports();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.Report.delReport(new Models.Report { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Report Removed!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Reports");
            bRefresh_Click(sender, e);
        }

        //protected void bAddLoc_Click(object sender, EventArgs e)
        //{
        //    Models.Location.ReportLocation dLocation = new Models.Location.ReportLocation { ReportId = id, LocationId = Convert.ToInt32(ddLocation.SelectedValue) };
        //    if (id > 0)
        //    {
        //        Models.Location.insReportLocation(dLocation);
        //        rLocations.DataSource = Models.Location.selDeployLocations(id);
        //    }
        //    else
        //    {
        //        List<Models.Location.ReportLocation> DeployLoc = Session["DeployLoc"] == null ? new List<Models.Location.ReportLocation>() : Session["DeployLoc"] as List<Models.Location.ReportLocation>;
        //        dLocation.id = DateTime.Now.Millisecond;
        //        dLocation.Title = ddLocation.SelectedItem.Text;
        //        DeployLoc.Add(dLocation);
        //        rLocations.DataSource = DeployLoc;
        //        Session["DeployLoc"] = DeployLoc;
        //    }
        //    rLocations.DataBind();
        //    common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        //}
        //protected void lbDeleteLoc_Click(object sender, EventArgs e)
        //{
        //    LinkButton lb = sender as LinkButton;
        //    if (id > 0)
        //    {
        //        Models.Location.delReportLocation(new Models.Location.ReportLocation { id = Convert.ToInt32(lb.CommandArgument) });
        //        rLocations.DataSource = Models.Location.selDeployLocations(id);
        //    }
        //    else
        //    {
        //        List<Models.Location.ReportLocation> DeployLoc = Session["DeployLoc"] == null ? new List<Models.Location.ReportLocation>() : Session["DeployLoc"] as List<Models.Location.ReportLocation>;
        //        DeployLoc.Remove(DeployLoc.Find(p => p.id == Convert.ToInt32(lb.CommandArgument)));
        //        rLocations.DataSource = DeployLoc;
        //        Session["DeployLoc"] = DeployLoc;
        //    }

        //    rLocations.DataBind();
        //    common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        //}  
    }
}