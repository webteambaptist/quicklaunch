﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class QLDeployment : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 7;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page
                if (id > 0 && Request.QueryString["msi"] != null)
                {
                    LoadMSI();
                    return;
                }
                else if (id > 0)
                    LoadDetail();
                else if (id == 0)
                { //New Item
                    if (Request.QueryString["copy"] != null)
                    {
                        Models.Deployment x = Models.Deployment.selDeploymentById(Convert.ToInt32((string)Request.QueryString["copy"]));
                        tTitle.Value = x.Title;
                        tDesc.Value = x.Description;
                        tInstallMessage.Value = x.InstallMessage;
                    }
                    mv.ActiveViewIndex = 1;
                    Session.Remove("DeployLoc");
                }
                else
                    LoadDeployments();

                ddLocation.DataSource = Models.Location.selLocations();
                ddLocation.DataBind();
                ddLocation.Items.Insert(0, new ListItem("Select Location", "0"));

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.Deployment x = Models.Deployment.selDeploymentById(id);
            if (x == null) return;
            tTitle.Value = x.Title;
            tDesc.Value = x.Description;
            tInstallMessage.Value = x.InstallMessage;
            cbIsPublished.Checked = x.IsPublished;
            cbIsEverywhere.Checked = x.IsEverywhere;            
            lblMSIBuild.InnerHtml =  x.MSIBuildVersion + " <small>(" +Shared.SysHelper.ConvertToMB(x.MSIBuildFileSize).ToString("N3") + " MB)</small>";
            tPublishDt.Value = x.publish_dt.ToString();
            tAddDt.Value = x.add_dt.ToShortDateString();
            tLupUser.Value = x.lup_user;
            tLupDT.Value = x.lup_dt.ToString();

            rLocations.DataSource = Models.Location.selDeployLocations(id);
            rLocations.DataBind();
        }
        private void LoadDeployments()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Deployments"))
                WebAdmin2014.Cache.Insert("Deployments", Models.Deployment.selActiveDeployments(), MinutesTillExpiration:5);
            rQLDeploy.DataSource = WebAdmin2014.Cache.Get<List<Models.Deployment>>("Deployments");
            rQLDeploy.DataBind();
        }
        private void LoadMSI()
        {
            Models.Deployment x = Models.Deployment.selDeploymentById(id);
            Response.Buffer = true;
            Response.ContentType = x.MSIBuildContentType;
            Response.AddHeader("Content-Disposition", "attachment;filename=Setup.QuickLaunch.msi");
            Response.BinaryWrite(x.MSIBuild);
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Deployment a;
            if (id > 0)
                a = Models.Deployment.selDeploymentById(id);
            else
                a = new Models.Deployment();
            a.id = id;
            a.Title = tTitle.Value;
            a.Description = tDesc.Value;
            a.InstallMessage = tInstallMessage.Value;            
            if (!a.IsPublished && cbIsPublished.Checked)
            {
                a.publish_dt = DateTime.Now;
                a.IsPublished = true;
            }
            else
                a.IsPublished = cbIsPublished.Checked;
            a.IsEverywhere = cbIsEverywhere.Checked;
            a.lup_user = common.user.ntID;
            a.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    if (fMSI.PostedFile.ContentLength == 0) { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('You must supply a .MSI build file in order to deploy');"); return; }                    

                    System.Web.HttpPostedFile postFile = fMSI.PostedFile;
                    postFile.SaveAs(Server.MapPath("~/App_Data/Setup.QuickLaunch.msi"));
                    a.MSIBuildVersion = common.GetMsiVersion(Server.MapPath("~/App_Data/Setup.QuickLaunch.msi")); //a.MSIBuildVersion = System.Reflection.AssemblyName.GetAssemblyName(Server.MapPath("~/resources/Setup.QuickLaunch.msi")).Version.ToString();                        
                    int nFileLen = postFile.ContentLength;
                    byte[] fileData = new byte[nFileLen];
                    postFile.InputStream.Read(fileData, 0, nFileLen);
                    a.MSIBuildFileSize = nFileLen.ToString();
                    a.MSIBuildContentType = fMSI.PostedFile.ContentType;                    
                    a.MSIBuild = fileData;

                    int dId = Convert.ToInt32(Models.Deployment.insDeployment(a));

                    try
                    { //load locations
                        List<Models.Location.DeploymentLocation> DeployLoc = Session["DeployLoc"] == null ? new List<Models.Location.DeploymentLocation>() : Session["DeployLoc"] as List<Models.Location.DeploymentLocation>;
                        foreach (Models.Location.DeploymentLocation dl in DeployLoc)
                        {
                            dl.DeploymentId = dId;
                            Models.Location.insDeploymentLocation(dl);
                        }
                    } catch { }
                
                }
                catch(Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Deployment Add Failed!');"); return; }
            }
            else
            {
                try
                {
                    Models.Deployment.updDeployment(a);                  
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Deployment Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Deployment Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Deployments");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadDeployments();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.Deployment.delDeployment(new Models.Deployment { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Deployment Archived!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Deployments");            
            bRefresh_Click(sender, e);
        }

        protected void bAddLoc_Click(object sender, EventArgs e)
        {
            Models.Location.DeploymentLocation dLocation = new Models.Location.DeploymentLocation { DeploymentId = id, LocationId = Convert.ToInt32(ddLocation.SelectedValue) };
            if (id > 0)
            {
                Models.Location.insDeploymentLocation(dLocation);
                rLocations.DataSource = Models.Location.selDeployLocations(id);
            }
            else
            {
                List<Models.Location.DeploymentLocation> DeployLoc = Session["DeployLoc"] == null ? new List<Models.Location.DeploymentLocation>() : Session["DeployLoc"] as List<Models.Location.DeploymentLocation>;
                dLocation.id = DateTime.Now.Millisecond;
                dLocation.Title = ddLocation.SelectedItem.Text;
                DeployLoc.Add(dLocation);
                rLocations.DataSource = DeployLoc;
                Session["DeployLoc"] = DeployLoc;
            }
            rLocations.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }
        protected void lbDeleteLoc_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (id > 0)
            {
                Models.Location.delDeploymentLocation(new Models.Location.DeploymentLocation { id = Convert.ToInt32(lb.CommandArgument) });
                rLocations.DataSource = Models.Location.selDeployLocations(id);
            }
            else
            {
                List<Models.Location.DeploymentLocation> DeployLoc = Session["DeployLoc"] == null ? new List<Models.Location.DeploymentLocation>() : Session["DeployLoc"] as List<Models.Location.DeploymentLocation>;
                DeployLoc.Remove(DeployLoc.Find(p => p.id == Convert.ToInt32(lb.CommandArgument)));
                rLocations.DataSource = DeployLoc;
                Session["DeployLoc"] = DeployLoc;
            }

            rLocations.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }  
    }
}