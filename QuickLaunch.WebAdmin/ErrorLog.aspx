﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ErrorLog.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.ErrorLog" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<div>
    <asp:MultiView runat=server ID="mv" ClientIDMode=Static>
        <asp:View runat=server ID="vList">        
        <h1>Error Log</h1><asp:LinkButton runat=server ID="lbExport" CssClass="right m5 cG" OnClick="lbExport_Click">Export to Excel</asp:LinkButton>        
        <asp:UpdatePanel runat=server ID="upAJAX" ClientIDMode=Static><ContentTemplate><span class="right m5 cR"><%=rErrorLog.Items.Count.ToString()%> Items</span>
        <select runat=server id="ddFilter"><option>By Date/Time</option><option>By Application</option><option>By Serial #</option><option>By LSID</option><option>By User Id</option><option>By QL Version</option><option>By ICA Version</option><option>By IE Version</option></select>        
        <div id="dFilterDt" class="iBlock m5"><asp:TextBox runat=server ID="tFilterStartDt" EnableViewState=false CssClass="dp" /> to <asp:TextBox runat=server ID="tFilterEndDt" EnableViewState=false CssClass="dp" /></div>
        <asp:TextBox runat=server ID="tFilter" EnableViewState=false CssClass="hidden" /> <asp:Button runat=server ID="bFilter" Text="Filter" OnClick="tFilter_Click" EnableViewState=false />        
        <table id="rError">
        <asp:Repeater runat=server ID="rErrorLog" EnableViewState=false ClientIDMode=Static>
        <HeaderTemplate><tr><th>Date/Time</th><th>Application</th><th>Serial #</th><th>LSID</th><th>User Id</th><th>QL Version</th><th>ICA Version</th><th>IE Version</th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>"><td><%#((DateTime)Eval("lup_dt")) %></td><td<%# (((bool)Eval("IsFatal")) ? " class='cR'" : "") %>><%#Eval("Application") %></td><td><%#Eval("Serial") %></td><td><%#common.cleanNTID((string)Eval("LSID")) %></td><td><%#Eval("CurrentUserId")%></td><td><%#Eval("QLVersion") %></td><td><%#Eval("ICAVersion") %></td><td><%#Eval("IEVersion") %></td></tr></ItemTemplate>
        </asp:Repeater><asp:LinkButton runat=server ID="bRefresh" ClientIDMode=Static CssClass="hidden" EnableViewState=false OnClick="bRefresh_Click" />
        <% if(rErrorLog.Items.Count == 0){ %>
        <tr><td colspan="8"><h2>No errors found</h2></td></tr>
        <% } %>
        </table></ContentTemplate></asp:UpdatePanel>        
        </asp:View>
        <asp:View runat=server ID="vDetail"><% if(id > 0) { %><div class="right m5"><input type="button" onclick="this.disabled=true;window.location='errorlog.aspx?id=' + (<%=id%>+1);" value="&uarr;"  title="Move up one error (newer)" /><br /><input type="button" onclick="this.disabled=true;window.location='errorlog.aspx?id=' + (<%=id%>-1);" value="&darr;" title="Move down one error (older)" /></div><% } %>
            <h1>Error Detail</h1>
            <ul id="edit" class="detail">
                <li><span>Date/Time:</span><input type=text runat=server id="tLupDt" clientidmode=Static readonly=readonly enableviewstate=false /></li> 
                <li><span>Application:</span><input type=text runat=server id="tApp" clientidmode=Static readonly=readonly enableviewstate=false /></li>               
                <li><span>Exception:</span><textarea runat=server id="tException" rows="10" clientidmode=Static readonly=readonly enableviewstate=false /></li>
                <li><span>Serial:</span><input type=text runat=server id="tSerial" clientidmode=Static readonly=readonly enableviewstate=false /> <label runat=server id="lLocation"></label></li>                  
                <li><span>LSID:</span><input type=text runat=server id="tLSID" clientidmode=Static readonly=readonly enableviewstate=false /></li> 
                <li><span>User Id:</span><input type=text runat=server id="tUser" clientidmode=Static readonly=readonly enableviewstate=false /></li> 
                <li><span>QL Version:</span><input type=text runat=server id="tQLV" clientidmode=Static readonly=readonly enableviewstate=false /></li>
                <li><span>ICA Version:</span><input type=text runat=server id="tICAV" clientidmode=Static readonly=readonly enableviewstate=false /></li> 
                <li><span>IE Version:</span><input type=text runat=server id="tIEV" clientidmode=Static readonly=readonly enableviewstate=false /></li>                 
                <li><span>Fatal:</span><input type=checkbox runat=server id="cbFatal" clientidmode=Static disabled=disabled enableviewstate=false /></li>                                        
            </ul>            
        </asp:View>
<%--        <asp:View runat=server ID="vClear">
            <h1>Clear Error Log</h1>
            <ul id="edit" class="detail">
                <li><span>Username:</span><input type=text runat=server id="Text1" clientidmode=Static disabled=disabled /></li>                
                <li><span>Email:</span><input type=text runat=server id="Text2" clientidmode=Static readonly=readonly /></li> 
                <li><span>Roles:</span><asp:CheckBoxList runat=server ID="CheckBoxList1" DataTextField="Role" DataValueField="id" /></li>                                
            </ul>
            <asp:Button runat=server ID="bClear" Text="Clear Errors" OnClick="bClear_Click" CssClass="m5" />
        </asp:View>--%>
    </asp:MultiView>
</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script type="text/javascript">
    $().ready(function () {                
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        <% } else if(mv.ActiveViewIndex == 0) { %>
        SetGrid();
        <% } %>        
    });
    function SetGrid(){
        $("table#rError tr").click(function () { if($(this).index() > 0)popModal("ErrorLog.aspx?id=" + $(this).attr('id')); });        
        $("#ddFilter").change(SetFilter);
        SetFilter();
        try { $(".dp").datepicker({ numberOfMonths: 2, showButtonPanel: true }); } catch(ex) {}
    }
    function SetFilter(){        
        if($("#ddFilter>option:selected").index() == 0){ $("#dFilterDt").show(); $("#tFilter").hide(); } else {$("#dFilterDt").hide(); $("#tFilter").show(); }
    }
</script>
</asp:Content>