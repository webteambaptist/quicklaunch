﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class SystemAlertCode : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 3;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New Alert Code
                    mv.ActiveViewIndex = 1;
                }
                else
                    LoadAlerts();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;            
            Models.SystemAlertCode sac = Models.SystemAlertCode.selSystemAlertCodeById(id);
            tCode.Value = sac.Code;
            tDesc.Value = sac.Description;
        }
        private void LoadAlerts()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("AlertCodes"))
                WebAdmin2014.Cache.Insert("AlertCodes", Models.SystemAlertCode.selSystemAlertCodes(),14);
            rSysAlertCode.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlertCode>>("AlertCodes");
            rSysAlertCode.DataBind();
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.SystemAlertCode sac = new Models.SystemAlertCode();
            sac.id = id;
            sac.Code = tCode.Value;
            sac.Description = tDesc.Value;
            sac.lup_user = common.user.ntID;
            sac.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    Models.SystemAlertCode.insSystemAlertCode(sac); 
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Alert Code Add Failed!');"); return; }
                tCode.Value = string.Empty;
                tDesc.Value = string.Empty;
            }
            else
            {
                try
                {
                    Models.SystemAlertCode.updSystemAlertCode(sac);                    
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Alert Code Update Failed!');"); return; }
            }            
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Alert Code Saved Successfully!'); parent.CloseModal();"); 
            WebAdmin2014.Cache.Clear("AlertCodes");
            WebAdmin2014.Cache.Clear("SystemAlerts");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadAlerts();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.SystemAlertCode.delSystemAlertCode(new Models.SystemAlertCode { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Alert Code Removed!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("AlertCodes");
            bRefresh_Click(sender, e);
        }         
        
    }
}