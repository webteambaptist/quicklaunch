﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class Location : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 10;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected string action { get { return ((string)System.Web.HttpContext.Current.Request.QueryString["act"]) ?? string.Empty; } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New Item
                    mv.ActiveViewIndex = 1;
                }
                else if (action == "wts")
                {
                    rWTS.DataSource = Models.Location.selWTSLocations();
                    rWTS.DataBind();
                    mv.ActiveViewIndex = 2;
                    return;
                }
                else if (action == "bulk")
                {
                    LoadBulkApply();                    
                    return;
                }
                else
                    LoadLocs();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.Location loc = Models.Location.selLocationById(id);
            if (loc == null) return;
            tTitle.Value = loc.Title;
            tDesc.Value = loc.Description;            
            tLupUser.Value = loc.lup_user;
            tLupDT.Value = loc.lup_dt.ToString();

            rIp.DataSource = Models.Location.selIpByLocation(id);
            rIp.DataBind();
        }
        private void LoadLocs()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Locations"))
                WebAdmin2014.Cache.Insert("Locations", Models.Location.selLocations(), 7);
            rLocation.DataSource = WebAdmin2014.Cache.Get<List<Models.Location>>("Locations");
            rLocation.DataBind();
        }
        private void LoadBulkApply()
        {
            mv.ActiveViewIndex = 3;
            if (!WebAdmin2014.Cache.Exists("Locations"))
                WebAdmin2014.Cache.Insert("Locations", Models.Location.selLocations(), 7);
            ddLocation.DataSource = WebAdmin2014.Cache.Get<List<Models.Location>>("Locations");
            ddLocation.DataBind(); ddLocation.Items.Insert(0, new ListItem("Select Location", "0"));
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Location a = new Models.Location();
            a.id = id;
            a.Title= tTitle.Value;
            a.Description = tDesc.Value;            
            a.lup_user = common.user.ntID;
            a.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    int lId = Convert.ToInt32(Models.Location.insLocation(a));

                    try
                    { //load locations
                        List<Models.Location.LocationIp> LocationIp = Session["LocIp"] == null ? new List<Models.Location.LocationIp>() : Session["LocIp"] as List<Models.Location.LocationIp>;
                        foreach (Models.Location.LocationIp dl in LocationIp)
                        {
                            dl.LocationId = lId;
                            Models.Location.insLocationIp(dl);
                        }
                        Session.Remove("LocIp");
                    }
                    catch { }
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Location Add Failed!');"); return; }
            }
            else
            {
                try
                {
                    Models.Location.updLocation(a);                  
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Location Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Location Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Locations");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadLocs();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.Location.delLocation(new Models.Location { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Location Removed!');parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Locations");
            bRefresh_Click(sender, e);
        }
        protected void bApplyBulkLocation_Click(object sender, EventArgs e)
        {
            if (ddLocation.SelectedIndex == 0){ common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('No location selected!');"); return;}
            if (tMachines.Value.Length == 0) { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Please enter at least one machine');"); return; }
            Models.Machine.updMachineLocationInBulk(tMachines.Value.Replace("'", string.Empty).Replace(" ",string.Empty), Convert.ToInt32(ddLocation.SelectedValue));
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "notify('S/N's applied successfully to location'); ");
            Msg.InnerHtml = string.Format("'{0}' location applied to {1} machines<p>{2}</p>", ddLocation.SelectedItem.Text, tMachines.Value.Split(',').Count().ToString(), tMachines.Value);
        }

        protected void bAddIp_Click(object sender, EventArgs e)
        {
            bool IsExclude = (sender as Button).CommandArgument == "EXCLUDE";
            Models.Location.LocationIp dLocation = new Models.Location.LocationIp { LocationId = id, Ip = tIp.Value, IsExcluded = IsExclude };            
            if (id > 0)
            {
                Models.Location.insLocationIp(dLocation);
                rIp.DataSource = Models.Location.selIpByLocation(id);
            }
            else
            {
                List<Models.Location.LocationIp> LocIp = Session["LocIp"] == null ? new List<Models.Location.LocationIp>() : Session["LocIp"] as List<Models.Location.LocationIp>;
                dLocation.id = DateTime.Now.Millisecond;                
                LocIp.Add(dLocation);
                rIp.DataSource = LocIp;
                Session["LocIp"] = LocIp;
            }
            rIp.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }
        protected void lbDeleteIp_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (id > 0)
            {
                Models.Location.delLocationIp(new Models.Location.LocationIp { id = Convert.ToInt32(lb.CommandArgument) });
                rIp.DataSource = Models.Location.selIpByLocation(id);
            }
            else
            {
                List<Models.Location.LocationIp> LocationIp = Session["LocIp"] == null ? new List<Models.Location.LocationIp>() : Session["LocIp"] as List<Models.Location.LocationIp>;
                LocationIp.Remove(LocationIp.Find(p => p.id == Convert.ToInt32(lb.CommandArgument)));
                rIp.DataSource = LocationIp;
                Session["LocIp"] = LocationIp;
            }

            rIp.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }  

    }
}
