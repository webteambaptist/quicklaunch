﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class SystemAlert : BasePages.SecureUserPage
    {
        #region Properties
        public static int RequiredRoleId = 3;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected string action { get { return ((string)System.Web.HttpContext.Current.Request.QueryString["act"]) ?? string.Empty; } }
        private static readonly string JSUpdateCalendar = "try{parent.document.getElementById('calendar').innerHTML='';parent.LoadCalendar();}catch(exx){}";
        private static readonly string JSUpdateTable = "try {parent.document.getElementById('bRefresh').click();}catch(ex){}";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (!WebAdmin2014.Cache.Exists("AlertCodes"))
                    WebAdmin2014.Cache.Insert("AlertCodes", Models.SystemAlertCode.selSystemAlertCodes(),1);
                ddCode.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlertCode>>("AlertCodes");
                ddCode.DataBind();
                ddCode.Items.Insert(0, new ListItem("Select","0"));
                if (id > 0)
                    LoadDetail();
                else if (id == 0)
                { //New Alert
                    mv.ActiveViewIndex = 1;
                    tStartDt.Text = DateTime.Now.ToString();
                    tEndDt.Text = DateTime.Now.AddDays(1).ToString();

                    tSysAlertSource.Items.FindByText("NetScaler").Value = "true";
                    tSysAlertSource.Items.FindByText("Workstations").Value = "true";
                    tSysAlertSource.Items.FindByText("PhysicianPortal").Value = "true";
                    tSysAlertSource.Items.FindByText("PFCSPortal").Value = "true";
                    tSysAlertSource.Items.FindByText("BHTHIN").Value = "true";
                    tSysAlertSource.Items.FindByText("CETech").Value = "true";
                    
                    cbIsTicker.Checked = true;
                } else if (action == "calendar")
                    mv.ActiveViewIndex = 2;
                else
                    LoadAlerts();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;            
            Models.SystemAlert sa = Models.SystemAlert.selSystemAlertById(id);
            if (sa == null) return;
            tName.Text = sa.Name;
            ddCode.SelectedValue = sa.CodeId.ToString();
            tMsg.Text = sa.Message ?? string.Empty;
            tApp.Value = sa.Application ?? string.Empty;
            tAttachmentLink.Value = sa.AttachmentLink ?? string.Empty;
            ddIcon.SelectedValue = sa.Icon ?? string.Empty;
            ddFont.SelectedValue = sa.FontColor ?? string.Empty;
            
            if(sa.IsNetScalerVisible)
            {
                ListItem lItem = tSysAlertSource.Items.FindByText("NetScaler");
                lItem.Value = sa.IsNetScalerVisible.ToString();
            }
            if (sa.IsPCVisible)
            {
                ListItem lItem = tSysAlertSource.Items.FindByText("Workstations");
                lItem.Value = sa.IsPCVisible.ToString();
            }
            if (sa.IsPhysicianPortalVisible)
            {
                ListItem lItem = tSysAlertSource.Items.FindByText("PhysicianPortal");
                lItem.Value = sa.IsPhysicianPortalVisible.ToString();
            }
            if (sa.IsPFCSVisible)
            {
                ListItem lItem = tSysAlertSource.Items.FindByText("PFCSPortal");
                lItem.Value = sa.IsPFCSVisible.ToString();
            }

            if (sa.IsBHTHINVisible)
            {
                ListItem lItem = tSysAlertSource.Items.FindByText("BHTHIN");
                lItem.Value = sa.IsBHTHINVisible.ToString();
            }
            if(sa.IsCETechVisible)
            {
                ListItem lItem = tSysAlertSource.Items.FindByText("");
                lItem.Value = sa.IsBHTHINVisible.ToString();
            }

            cbIsTicker.Checked = sa.IsTicker;
            cbIsDialog.Checked = sa.IsDialog;
            cbIsOSBubble.Checked = sa.IsOSBubble;
            cbIsToast.Checked = sa.IsToast;
            cbIsQLCitrixEnabled.Checked = sa.IsQLCitrixEnabled;
            try { cbIsQLCitrixEnabled.Disabled = !Convert.ToBoolean((string)System.Configuration.ConfigurationManager.AppSettings["AllowQuickLaunchDisable"]); }
            catch { cbIsQLCitrixEnabled.Checked = true; cbIsQLCitrixEnabled.Disabled = true; }
            cbIsEnabled.Checked = sa.IsEnabled;
            
            dFrequency.SelectedValue = sa.Frequency ?? string.Empty;
            tStartDt.Text = sa.StartDt.ToString();
            tEndDt.Text = sa.EndDt.ToString();

        }
        private void LoadAlerts()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("SystemAlerts"))
                WebAdmin2014.Cache.Insert("SystemAlerts", Models.SystemAlert.selSystemAlerts(),1);
            rSysAlert.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlert>>("SystemAlerts");
            rSysAlert.DataBind();
        }
        protected void bSave_Click(object sender, EventArgs e)
        {

            if (IsValid)
            {
                //Models.SystemAlert sa = new Models.SystemAlert();
                //sa.id = id;
                //sa.Name = tName.Value;
                //sa.Message = tMsg.Value;
                //sa.Application = tApp.Value;
                //sa.AttachmentLink = tAttachmentLink.Value;
                //sa.Icon = ddIcon.Value;
                //sa.FontColor = ddFont.Value;
                //sa.IsNetScalerVisible = cbIsNetScalerVisible.Checked;
                //sa.IsPCVisible = cbIsPCVisible.Checked;
                //sa.IsPhysicianPortalVisible = cbIsPhysicianPortalVisible.Checked;
                //sa.IsPFCSVisible = cbIsPFCSVisible.Checked;
                //sa.IsBHTHINVisible = cbIsBHTHINVisible.Checked;
                //sa.IsCETechVisible = cbIsCETechVisible.Checked;
                //sa.IsTicker = cbIsTicker.Checked;
                //sa.IsDialog = cbIsDialog.Checked;
                //sa.IsOSBubble = cbIsOSBubble.Checked;
                //sa.IsToast = cbIsToast.Checked;
                //sa.Frequency = ddFrequency.Value;
                //sa.IsQLCitrixEnabled = cbIsQLCitrixEnabled.Checked;
                //sa.IsEnabled = cbIsEnabled.Checked;
                //sa.CodeId = Convert.ToInt32(ddCode.SelectedValue);
                //try { sa.StartDt = Convert.ToDateTime(tStartDt.Value); }
                //catch { return; }
                //try { sa.EndDt = Convert.ToDateTime(tEndDt.Value); }
                //catch { return; }
                //sa.lup_user = common.user.ntID;
                //sa.lup_dt = DateTime.Now;
                //if (id == 0)
                //{
                //    try
                //    {
                //        Models.SystemAlert.insSystemAlert(sa);
                //    }
                //    catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('System Alert Add Failed!');"); return; }
                //    tName.Value = string.Empty;
                //    tMsg.Value = string.Empty;
                //}
                //else
                //{
                //    try
                //    {
                //        Models.SystemAlert.updSystemAlert(sa);
                //    }
                //    catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('System Alert Update Failed!');"); return; }
                //}
                //common.SendNonAJAX_Script(Page, this.GetType(), "Save", string.Format("{0} {1} parent.notify('Alert Saved Successfully!'); parent.CloseModal();", JSUpdateTable, JSUpdateCalendar));
                //WebAdmin.Cache.Clear("SystemAlerts");
            }
        }

        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadAlerts();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.SystemAlert.delSystemAlert(new  Models.SystemAlert { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", string.Format("{0} {1} parent.notify('System Alert Removed!'); parent.CloseModal();", JSUpdateTable, JSUpdateCalendar));
            WebAdmin2014.Cache.Clear("SystemAlerts");
            bRefresh_Click(sender, e);
        }         
        
    }

}