﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {       
        protected void Page_Load(object sender, EventArgs e)
        {
            try { if (common.user.Roles.Count < 3 && common.user.Roles.Any(r => r.id == SystemAlert.RequiredRoleId)) Page.Title = "System Alerts Admin"; } catch { }
        }
    }
}
