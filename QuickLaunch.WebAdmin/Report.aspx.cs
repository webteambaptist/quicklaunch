﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace QuickLaunch.WebAdmin2014
{
    public partial class Report : BasePages.PublicUserPage
    {
        #region Properties
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected bool IsExcelOutput = false;
        public Models.Report CurrentReport;        
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (id > 0)
                {
                    if (LoadReport())
                    { //report loaded correctly
                        Models.Report.ReportHistory rptHistory = new Models.Report.ReportHistory(CurrentReport);
                        rptHistory.lup_user = common.cleanNTID(Request.ServerVariables["AUTH_USER"]);
                        if (Request.QueryString["send"] != null)
                        {
                            if (Models.Report.GenerateEmail(CurrentReport, string.Format("http://{0}bhwebapp.bmcjax.com/TITO_Admin", Request.Url.AbsoluteUri.Contains("devbhwebapp") ? "dev" : string.Empty)))
                            {
                                rptHistory.IsSent = true;
                                common.SendNonAJAX_Script(Page, this.GetType(), "EmailSent", "alert('Report Sent Successfully!\n\n" + CurrentReport.EmailRecipients + "'); window.close();");
                                CurrentReport = null;  //dont allow page to render
                            }
                        }
                        else if (Request.QueryString["excel"] != null)
                        {
                            IsExcelOutput = true;
                            Page.Response.ClearContent();
                            Page.Response.AddHeader("content-disposition", "attachment; filename=" + CurrentReport.FileName + ".xls");
                            Page.Response.ContentType = "application/excel";
                        }
                        Models.Report.insReportHistory(rptHistory);
                    } //else error occurred
                    else
                    {
                        common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Report execution failed! \n\nYour error has been logged'); window.close();");
                        CurrentReport = null;  //dont allow page to render
                    }
                }
                else
                    return; //no param passed
            }
        }

        private bool LoadReport()
        {
            try
            {
                CurrentReport = Models.Report.selReportById(id);
                if (CurrentReport == null) { return false; } //Report not found
                Page.Title = CurrentReport.Title;
                Scheduler.Reporting.GenerateReport(ref CurrentReport);                                
            }
            catch (Exception ex) { CurrentReport.Exception = ex; }
            if(CurrentReport.Exception != null){
                Models.ErrorLog.AddErrorToLog(CurrentReport.Exception, common.ntID, Global.ApplicationName, false);
                return false; //error occurred
            }

            return true; //success
        }


    }
}   