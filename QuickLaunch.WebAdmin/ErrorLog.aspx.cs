﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class ErrorLog : BasePages.SecureUserPage
    {        
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected string specific { get { return (string)System.Web.HttpContext.Current.Request.QueryString["only"] ?? "ALL"; } }
        public static int RequiredRoleId = 9;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                tFilterStartDt.Text = DateTime.Now.AddDays(-7).ToShortDateString();
                tFilterEndDt.Text = DateTime.Now.AddDays(1).ToShortDateString();
                if (specific.ToUpper() == "QL")
                {
                    ddFilter.SelectedIndex = 1;
                    tFilter.Text = "QUICKLAUNCH";
                }
                else if (specific.ToUpper() == "WA")
                {
                    ddFilter.SelectedIndex = 1;
                    tFilter.Text = "WEBADMIN";
                }


                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                {
                    //LoadClearDetails();
                }
                else
                    LoadErrors(tFilter.Text);

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.ErrorLog e = Models.ErrorLog.selErrorLogById(id);
            if (e == null) { common.SendNonAJAX_Script(Page, this.GetType(), "ret", "alert('Error Not Found');history.go(-1);"); return; }
            tLupDt.Value = e.lup_dt.ToString();
            tApp.Value = e.Application;
            tException.Value = e.Exception;
            tSerial.Value = e.Serial;
            lLocation.InnerHtml = e.WTSLocation ?? string.Empty;
            tLSID.Value = e.LSID;
            tUser.Value = e.CurrentUserId;
            tQLV.Value = e.QLVersion;
            tICAV.Value = e.ICAVersion;
            tIEV.Value = e.IEVersion;            
            cbFatal.Checked = e.IsFatal;            
        }
        private void LoadClearDetails()
        {
            //mv.ActiveViewIndex = 2;
        }
        private List<Models.ErrorLog> LoadErrors(string filter = "")
        {
            mv.ActiveViewIndex = 0;
            filter = filter.ToLower();
            //if (!WebAdmin.Cache.Exists("ErrorLog"))
            //    WebAdmin.Cache.Insert("ErrorLog", Models.ErrorLog.selErrorLogs(20), null, DateTime.Now.AddMinutes(20), System.Web.Caching.Cache.NoSlidingExpiration);            
            List<Models.ErrorLog> v = Models.ErrorLog.selErrorLogs(10000);
            try {
                switch(ddFilter.SelectedIndex){
                    case 1: //application
                        v = v.Where(f => (f.Application ?? string.Empty).ToLower().Contains(filter)).ToList();
                        break;
                    case 2: //serial
                        v = v.Where(f => (f.Serial ?? string.Empty).ToLower().Contains(filter)).ToList();
                        break;
                    case 3: //lsid
                        v = v.Where(f => (f.LSID ?? string.Empty).ToLower().Contains(filter)).ToList();
                        break;
                    case 4: //user
                        v = v.Where(f => (f.CurrentUserId ?? string.Empty).ToLower().Contains(filter)).ToList();
                        break;
                    case 5: //ql ver
                        v = v.Where(f => (f.QLVersion ?? string.Empty).ToLower().Contains(filter)).ToList();
                        break;
                    case 6: //ica ver
                        v = v.Where(f => (f.ICAVersion ?? string.Empty).ToLower().Contains(filter)).ToList();
                        break;
                    case 7: //ie ver
                        v = v.Where(f => (f.IEVersion ?? string.Empty).ToLower().Contains(filter)).ToList();
                        break;
                    default: //date
                        v = v.Where(f => f.lup_dt >= Convert.ToDateTime(tFilterStartDt.Text) && f.lup_dt <= Convert.ToDateTime(tFilterEndDt.Text)).ToList();
                        break;
                }
            }
            catch { common.SendAJAX_PopupMsg(Page, this.GetType(), "There was an error filtering your results. If this continues, please contact an administrator."); }
            rErrorLog.DataSource = v.Take(5000);
            rErrorLog.DataBind();
            return v;
        }

        protected void lbExport_Click(object sender, EventArgs e)
        {
            GridView gv = new GridView();
            gv.DataSource = LoadErrors(tFilter.Text);
            gv.DataBind();
            common.ExportGridToExcel(Page, gv, "ErrorLogExport_" + DateTime.Now.ToShortDateString().Replace("/", "-"));
        }
        //protected void bClear_Click(object sender, EventArgs e)
        //{
  
        //    if (id == 0)
        //    {
        //        try
        //        {                    
                    
        //        }
        //        catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Clear Failed!');"); return; }              
        //    }
        //    //common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('User Saved Successfully!'); parent.CloseModal();");
        //    WebAdmin.Cache.Clear("ErrorLog");
        //}

        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadErrors(tFilter.Text);
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void tFilter_Click(object sender, EventArgs e)
        {
            LoadErrors(tFilter.Text);
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }        
    }
}