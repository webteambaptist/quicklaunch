﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Versioning.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.Versioning" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<div>
    <asp:MultiView runat=server ID="mv" ClientIDMode=Static>
        <asp:View runat=server ID="vList">        
        <h1>Versioning</h1><asp:UpdatePanel runat=server ID="upAJAX" ClientIDMode=Static><ContentTemplate>        
        <table id="rVersion">
        <asp:Repeater runat=server ID="rVersion" EnableViewState=false ClientIDMode=Static>
        <HeaderTemplate><tr><th>Title</th><th>Latest Version</th><th>Last Update</th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>"><td><%#Eval("Title") %></td><td><%#Eval("LatestVersion")%></td><td><%#Eval("lup_dt")%></td></tr></ItemTemplate>
        </asp:Repeater><asp:LinkButton runat=server ID="bRefresh" ClientIDMode=Static CssClass="hidden" EnableViewState=false OnClick="bRefresh_Click" />
        </table>
        </ContentTemplate></asp:UpdatePanel>        
        </asp:View>
        <asp:View runat=server ID="vDetail">
            <h1><%=(this.id == 0 ? "New" : "Update") %> Version</h1>
            <ul id="edit" class="detail">
                <li><span>Title:</span><input type=text runat=server id="tTitle" /></li>                
                <li><span>Latest Version:</span><input type=text runat=server id="tLatestVersion" /></li> 
                <li title="Not the preferred version, but has proven to work"><span>Acceptable Version 1:</span><input type=text runat=server id="tAcceptVersion1" /></li> 
                <li title="Not the preferred version, but has proven to work"><span>Acceptable Version 2:</span><input type=text runat=server id="tAcceptVersion2" /></li> 
                <li title="Not the preferred version, but has proven to work"><span>Acceptable Version 3:</span><input type=text runat=server id="tAcceptVersion3" /></li> 
                <li><span>Message:</span><textarea runat=server id="tMessage" /></li>                                
                <li title="This determines how often a computer checks for updates (only applicable to Quick Launch version)"><span>Update Check Frequency:</span><input type=text runat=server id="tUpdateFrequency" class="tCenter" style="width:30px;" /> <small>minutes</small></li> 
            </ul>
            <asp:Button runat=server ID="bSave" Text="Save Version" OnClick="bSave_Click" CssClass="m5" />
        </asp:View>
        <asp:View runat=server ID="vCrypto">        
        <h1>Cryptography</h1><asp:UpdatePanel runat=server ID="UpdatePanel1" ClientIDMode=Static><ContentTemplate>  
        <div><input type="text" runat=server id="tCrypto" class="w70" /> <asp:Button runat=server ID="bEncrypt" Text="Encrypt" OnClick="bEncrypt_Click" /> <asp:Button runat=server ID="bDecrypt" Text="Decrypt" OnClick="bDecrypt_Click" /><br /><span runat=server id="sCryptResult" /></div>
        </ContentTemplate></asp:UpdatePanel> 
        </asp:View>
    </asp:MultiView>
</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script type="text/javascript">
    $().ready(function () {
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        <% } else { %>
        SetGrid();
        <% } %>
    });
    function SetGrid(){
        $("table#rVersion tr").click(function () { if($(this).index() > 0)popModal("Versioning.aspx?id=" + $(this).attr('id')); });        
    }
</script>
</asp:Content>