﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemAlertCode.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.SystemAlertCode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<%--    <asp:ScriptManager runat=server ID="sm" ClientIDMode=Static ScriptMode=Release LoadScriptsBeforeUI=false EnableCdn=true /><uc:Progress runat=server id="ucProg" />--%>
<div>
    <asp:MultiView runat=server ID="mv" ClientIDMode=Static>
        <asp:View runat=server ID="vList">
        <a href="#Add" class="right m5">Add Alert Code</a>
        <h1>System Alert Codes</h1><asp:UpdatePanel runat=server ID="upAJAX" ClientIDMode=Static><ContentTemplate>
        <table id="rAlertCode">
        <asp:Repeater runat=server ID="rSysAlertCode" EnableViewState=false ClientIDMode=Static>
        <HeaderTemplate><tr><th>Code</th><th class="tLeft">Description</th><th>Last Updated</th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>"><td class="bold"><%#Eval("Code") %></td><td class="tLeft w50"><%#Eval("Description")%></td><td><%#((DateTime)Eval("lup_dt")).ToString() %></td></tr></ItemTemplate>
        </asp:Repeater>
        <% if(rSysAlertCode.Items.Count == 0){ %>
        <tr><td colspan="3"><h2>No alert codes available</h2></td></tr>
        <% } %>
        </table><asp:LinkButton runat=server ID="bRefresh" ClientIDMode=Static CssClass="hidden" EnableViewState=false OnClick="bRefresh_Click" />        
        </ContentTemplate></asp:UpdatePanel>
        <a href="#Add" class="right m5">Add Alert Code</a>
        </asp:View>        
        <asp:View runat=server ID="vDetail">
            <h1><%=(this.id == 0 ? "New" : "Update") %> Alert Code</h1>
            <ul id="edit" class="detail">
                <li><span>Code:</span><input type=text runat=server id="tCode" clientidmode=Static /></li>                                
                <li><span>Description:</span><textarea runat=server id="tDesc" clientidmode=Static></textarea></li> 
            </ul>
            <asp:Button runat=server ID="bSave" Text="Save Alert Code" OnClick="bSave_Click" CssClass="m5" />
            <%if (this.id > 0) { %>
            <asp:Button runat=server ID="bDelete" Text="Remove Alert Code" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to remove this item?');" />
            <% } %>
        </asp:View>
    </asp:MultiView>

</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script type="text/javascript">
    $().ready(function () {        
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        <% } else { %>
        SetGrid();
        <% } %>
    });
    function SetGrid(){
        $("table#rAlertCode tr").click(function () { if($(this).index() > 0)popModal("SystemAlertCode.aspx?id=" + $(this).attr('id')); });
        $("a[href=#Add]").click(function () { popModal("SystemAlertCode.aspx?id=0"); });
    }
</script>
</asp:Content>