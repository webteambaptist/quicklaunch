﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SystemAlert.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.SystemAlert" ClientIDMode="Static" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="css/calendar/calendar.css" rel="stylesheet" type="text/css" />    
    <link href="css/calendar/cupertinotheme.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<%--    <asp:ScriptManager runat=server ID="sm" ClientIDMode="Static" ScriptMode=Release LoadScriptsBeforeUI="false" EnableCdn=true /><uc:Progress runat=server id="ucProg" />--%>
<div>
    <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
        <asp:View runat="server" ID="vList">
        <a href="#Add" class="right m5">Add System Alert</a>
        <h1>System Alerts</h1><asp:UpdatePanel runat="server" ID="upAJAX" ClientIDMode="Static"><ContentTemplate>
        <table id="rAlert">
        <asp:Repeater runat="server" ID="rSysAlert" EnableViewState="false" ClientIDMode="Static">
        <HeaderTemplate><tr><th colspan="2">Title</th><th>Code</th><th>Application</th><th>Start</th><th>End</th><th>Response Ct</th><th>Enabled?</th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>" class="<%# !((bool)Eval("IsEnabled")) ? "bgLGray cDG" : string.Empty %>"><td><img src="images/systemalerticons/<%#Eval("Icon") %>-light.gif" /></td><td title="<%#Eval("Message") %>" class="tLeft"><%#Eval("Name") %></td><td title="<%#Eval("CodeDescription") %>"><%#Eval("Code") %></td><td><%#Eval("Application")%></td><td><%#((DateTime)Eval("StartDt")).ToShortDateString() %></td><td><%#((DateTime)Eval("EndDt")).ToShortDateString() %></td><td><a href="#"><%#Eval("ResponseCt") %></a></td><td><%#Eval("IsEnabled") %></td></tr></ItemTemplate>
        </asp:Repeater>
        <% if(rSysAlert.Items.Count == 0){ %>
        <tr><td colspan="7"><h2>No system alerts available</h2></td></tr>
        <% } %>
        </table><asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />        
        </ContentTemplate></asp:UpdatePanel>
        <a href="#Add" class="right m5">Add System Alert</a>
        </asp:View>        
        <asp:View runat="server" ID="vDetail">
            <h1><%=(this.id == 0 ? "New" : "Update") %> System Alert</h1>
            <ul id="edit" class="detail">
                <li>
                    <span>Title:</span><asp:Textbox runat="server" ID="tName" clientidmode="Static" MaxLength="100" CausesValidation="True" ValidationGroup="save"/>
                    <asp:RequiredFieldValidator ID="tNameValidator" runat="server"
                        ControlToValidate="tName" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Please select one</span>
                    </asp:RequiredFieldValidator>  
                </li>                 
                         
                <li>
                    <span>Code:</span><asp:DropDownList runat="server" ID="ddCode" DataTextField="Code" DataValueField="id" />
                    <asp:RequiredFieldValidator ID="ddCodeValidator" runat="server"
                        ControlToValidate="ddCode$ddCode_Textbox" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Cannot Be left blank</span>
                    </asp:RequiredFieldValidator>
                </li> 

                <li>
                    <span>Message:</span><asp:Textbox runat="server" ID="tMsg" clientidmode="Static" CausesValidation="true" TextMode="MultiLine" ValidationGroup="save" Width="191px" AutoPostBack="True"></asp:Textbox>
                    <asp:RequiredFieldValidator ID="tMsgValidator" runat="server"
                        ControlToValidate="tMsg" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Cannot Be left blank</span>
                    </asp:RequiredFieldValidator>
                </li> 

                <li><span>Application:</span><input type="text" runat="server" id="tApp" clientidmode="Static" maxlength="250" /></li>                
                
                <li title="Optional"><span>Attachment Link:</span><input type="text" runat="server" id="tAttachmentLink" clientidmode="Static" maxlength="2048" /></li>                                  
                
                <li>
                    <span>Icon:</span>
                    <asp:DropDownList runat="server" ID="ddIcon" AutoPostBack="True">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="red">Red</asp:ListItem>
                        <asp:ListItem value="orange">Orange</asp:ListItem>
                        <asp:ListItem value="yellow">Yellow</asp:ListItem>
                        <asp:ListItem value="green">Green</asp:ListItem>
                    </asp:DropDownList>
                   <img src="images/systemalerticons/<%=ddIcon.SelectedValue %>-light.gif" id="iIcon" width="25" height="25" style="vertical-align:middle; text-align:center"/>
                    <asp:RequiredFieldValidator ID="dIconValidator" runat="server"
                        ControlToValidate="ddIcon$ddIcon_Textbox" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">Please choose a color</span>
                    </asp:RequiredFieldValidator>
                </li> 

                <li>
                    <span>Font Color:</span>
                    <asp:DropDownList runat="server" ID="ddFont">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem value="black">Black</asp:ListItem>
                        <asp:ListItem value="darkblue">Dark Blue</asp:ListItem>
                        <asp:ListItem value="green">Green</asp:ListItem>
                        <asp:ListItem value="orange">Orange</asp:ListItem> 
                        <asp:ListItem value="red">Red</asp:ListItem>                                     
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ddFontValidator" runat="server"
                        ControlToValidate="ddFont$ddFont_Textbox" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">Please choose a Color</span>
                    </asp:RequiredFieldValidator>
                </li>  
                
                <li title="When active this designates where the alert should be displayed">
                    <span>Where to show:<br /><small>(source)</small><br /><br /></span>
                    <asp:CheckBoxList runat="server" ID="tSysAlertSource" RepeatColumns="2" TextAlign="Left">
                        <asp:ListItem Value="Workstations">Workstations</asp:ListItem>
                        <asp:ListItem Value="NetScaler">Net Scaler</asp:ListItem>
                        <asp:ListItem Value="PhysiciansPortal">Physicians' Portal</asp:ListItem>
                        <asp:ListItem Value="PFCSPortal">PFCS Portal</asp:ListItem>
                        <asp:ListItem Value="BHTHIN">BHTHIN</asp:ListItem>
                        <asp:ListItem Value="CETech">CE Tech</asp:ListItem>
                    </asp:CheckBoxList>
                        
                    <%--<asp:RequiredFieldValidator ID="tSysAlertSourceValidator" runat="server"
                        ControlToValidate="tSysAlertSource" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Check at least one</span>
                    </asp:RequiredFieldValidator> --%>                                       
                </li>          
                
                <li title="When active this designates how the alert should be displayed">
                    <span>How to show<br /><small>(mechanism)</small></span>
                    <asp:CheckBoxList runat="server" ID="tSystemDisplay" TextAlign="Left">
                        <asp:ListItem Value="Dialog">Dialog?</asp:ListItem>
                        <asp:ListItem Value="Ticker">Ticker/Marquee?</asp:ListItem>                        
                        <asp:ListItem Value="TrayToast">Tray Toast?</asp:ListItem>
                        <asp:ListItem Value="OSBubble">OS Bubble?</asp:ListItem>
                        <asp:ListItem Value="WebPortalList">Web Portal List?</asp:ListItem>
                    </asp:CheckBoxList>
                </li>

                <li title="# of times an alert shows. Applies to workstations only at this time">
                    <span>Frequency:</span>
                    <asp:DropDownList runat="server" ID="dFrequency">
                        <asp:ListItem></asp:ListItem>
                        <asp:ListItem Value="once">One Time</asp:ListItem>
                        <asp:ListItem Value="1HR">Once, every hour</asp:ListItem>
                        <asp:ListItem Value="2HR">Once, every other hour</asp:ListItem>
                        <asp:ListItem Value="4HR">Once, every four hours</asp:ListItem>
                        <asp:ListItem Value="1DAY">Once a day</asp:ListItem>
                        <asp:ListItem Value="7DAY">Once a week</asp:ListItem>
                        <asp:ListItem Value="30DAY">Once a month</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="ddfrequencyValidator" runat="server"
                        ControlToValidate="ddFrequency$ddFrequency_Textbox" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Must pick one</span>
                    </asp:RequiredFieldValidator>
                </li>              
                
                <li title="Designate which locations to display the alert">
                    <asp:CheckBox runat="server" ID="cbxEverywhere"/>
                    <asp:DropDownList runat="server" ID="ddLocations">

                    </asp:DropDownList>
                    <asp:Button runat ="server" ID="btnAddLocation" />

                    <asp:Label runat="server" ID="tbNoLocationsSet">No Locations</asp:Label>                    
                    <br />
                    <small>Add a location to specify alert scope</small>
                </li>
                
                <li title="Date/time the alert will start showing. User will not see this date/time">
                   <span>Active Start Date:</span><asp:Textbox runat="server" ID="tStartDt" CausesValidation="true" AutoPostBack="True"/>
                    <asp:ImageButton runat="Server" ID="ImageButton1" 
                        ImageUrl="http://www.asp.net/ajaxlibrary/ajaxcontroltoolkitsamplesite/images/Calendar_scheduleHS.png" 
                        CausesValidation="False" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="myCalendar" runat="server" 
                        TargetControlID="tStartDt" Format="MM/dd/yyyy"
                        FirstDayOfWeek="Sunday" ViewStateMode="Enabled" 
                        DaysModeTitleFormat="MM/dd/yyyy" Enabled="True" 
                        PopupButtonID="ImageButton1" TodaysDateFormat="MM/dd/yyyy" PopupPosition="Right"/>
                    <asp:RequiredFieldValidator ID="tStartDtValidator" runat="server"
                        ControlToValidate="tStartDt" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Must select a date</span>
                    </asp:RequiredFieldValidator>
                </li> 
               
                <li title="Date/time that the app is in actual downtime.QL will be disabled at this time">
                   <span>Active Start Date:</span><asp:Textbox runat="server" ID="tDowntime" CausesValidation="true" AutoPostBack="True"/>
                    <asp:ImageButton runat="Server" ID="ImageButton3" 
                        ImageUrl="http://www.asp.net/ajaxlibrary/ajaxcontroltoolkitsamplesite/images/Calendar_scheduleHS.png" 
                        CausesValidation="False" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender3" CssClass="myCalendar" runat="server" 
                        TargetControlID="tDowntime" Format="MM/dd/yyyy"
                        FirstDayOfWeek="Sunday" ViewStateMode="Enabled" 
                        DaysModeTitleFormat="MM/dd/yyyy" Enabled="True" 
                        PopupButtonID="ImageButton3" TodaysDateFormat="MM/dd/yyyy" PopupPosition="Right"/>
                    <asp:RequiredFieldValidator ID="tDowntimeValidator" runat="server"
                        ControlToValidate="tDowntime" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Must select a date</span>
                    </asp:RequiredFieldValidator>
                </li> 

                 <li title="Date/time the alert will stop showing. User will not see this date/time">
                    <span>Active End Date:</span><asp:Textbox runat="server" id="tEndDt" CausesValidation="true" AutoPostBack="True"/>
                     <asp:ImageButton runat="Server" ID="ImageButton2" 
                        ImageUrl="http://www.asp.net/ajaxlibrary/ajaxcontroltoolkitsamplesite/images/Calendar_scheduleHS.png" 
                        CausesValidation="False" />
                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" CssClass="myCalendar" runat="server" 
                        TargetControlID="tEndDt" Format="MM/dd/yyyy"
                        FirstDayOfWeek="Sunday" ViewStateMode="Enabled" 
                        DaysModeTitleFormat="MM/dd/yyyy" Enabled="True" 
                        PopupButtonID="ImageButton2" TodaysDateFormat="MM/dd/yyyy" PopupPosition="Right"/>
                     <asp:RequiredFieldValidator ID="tEndDtValidator" runat="server"
                        ControlToValidate="tEndDt" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Must select a date</span>
                    </asp:RequiredFieldValidator>
                 </li> 
                
                <li title="If NOT checked Quick Launch Citrix menu items will be disabled"><span>Quick Launch Citrix Menu Items Enabled?</span><input type="checkbox" runat="server" id="cbIsQLCitrixEnabled" checked="checked" />&nbsp;&nbsp;&nbsp;<label id="lblQLWarning" /></li>   
                <li title="Determines if alert is active"><span>Enabled?</span><input type="checkbox" runat="server" id="cbIsEnabled" /></li>              
            </ul>
            <p>
            <asp:Button runat="server" ID="bSave" Text="Save System Alert" OnClick="bSave_Click" CssClass="m5" />
            <%if (this.id > 0) { %>
            <asp:Button runat="server" ID="bDelete" Text="Remove System Alert" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
            <% } %></p>
        </asp:View>
        <asp:View runat="server" ID="vCalendar">
        <div id='calendar'></div><a href="#Add" class="f9">Add Alert</a>
        </asp:View>
    </asp:MultiView>

</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<%if(mv.ActiveViewIndex == 2){ %>
    <script src="js/jquery-calendar.min.js" type="text/javascript"></script>
    <script src="js/jquery-ui-1.8.23.calendar.min.js" type="text/javascript"></script>
<% } %>
<script type="text/javascript">
    $().ready(function () {                          
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        $("#ddIcon").change(function(){ $("#iIcon").attr("src","images/systemalerticons/" + $(this).val() + "-light.gif"); });
        $("#bSave").click(function(){if(!$("#cbIsQLCitrixEnabled").is(":checked"))return confirm("WARNING: The current settings disable Quick Launch Citrix Applications!\n\nAre you sure you want to continue?");});
        $("#cbIsQLCitrixEnabled").change(CheckQLEnablement);        
        CheckQLEnablement();
        <% } else if(mv.ActiveViewIndex == 2) { %>
        LoadCalendar();
        <% } else { %>
        SetGrid();
        <% } %>
    });
    function SetGrid(){
        $("table#rAlert tr td").click(function () { if($(this).parent().index() > 0 && $(this).index() == 6)popModal("SystemAlertResponse.aspx?aid=" + $(this).parent().attr('id')); else if($(this).parent().index() > 0 && $(this).index() != 6)popModal("SystemAlert.aspx?id=" + $(this).parent().attr('id')); });
        $("a[href=#Add]").click(function () { popModal("SystemAlert.aspx?id=0"); });
    }
    function LoadCalendar(){
        ajax("GetSystemAlerts", "", function (data) {            
            for(var i = 0;i<data.GetSystemAlertsResult.length;i++){
                data.GetSystemAlertsResult[i].start = new Date(data.GetSystemAlertsResult[i].start);
                data.GetSystemAlertsResult[i].end = new Date(data.GetSystemAlertsResult[i].end);
            }                    
        	$('#calendar').fullCalendar({
			    theme: true,
			    header: {
				    left: 'prev,next today',
				    center: 'title',
				    right: 'month,agendaWeek,agendaDay'
			    },
			    editable: true,
			    events: data.GetSystemAlertsResult
                , viewDisplay: function(view) {
		            $('a[href*="#edit"]').click(function(){                
                        popModal("systemalert.aspx?id=" + $(this).attr("href").replace("#edit",""));
                    });
                 }
		    });
        });
        SetGrid();
    }
    function CheckQLEnablement(){
        if(!$("#cbIsQLCitrixEnabled").is(":checked")){$("#lblQLWarning").addClass("cR"); $("#lblQLWarning").html("Quick Launch set to be disabled! User will be given appropriate message.");} else { $("#lblQLWarning").html("Quick Launch is enabled."); $("#lblQLWarning").removeClass("cR"); }
    }
</script>
</asp:Content>