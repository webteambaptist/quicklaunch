﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class Versioning : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 5;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (id > 0)
                    LoadDetail();
                else if (id == 0)
                { //New User                    
                    mv.ActiveViewIndex = 1;
                }
                else if (Request.Url.AbsoluteUri.Contains("crypto"))
                    mv.ActiveViewIndex = 2;
                else
                    LoadVersions();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.Version v = Models.Version.selVersionById(id);
            if (v == null) return;
            tTitle.Value = v.Title;
            tLatestVersion.Value = v.LatestVersion;
            tAcceptVersion1.Value = v.AcceptableVersion1;
            tAcceptVersion2.Value = v.AcceptableVersion2;
            tAcceptVersion3.Value = v.AcceptableVersion3;
            tMessage.Value = v.Message;
            tUpdateFrequency.Value = v.CheckFrequency.ToString();
        }
        private void LoadVersions()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Versions"))
                WebAdmin2014.Cache.Insert("Versions", Models.Version.selVersions(),3);
            rVersion.DataSource = WebAdmin2014.Cache.Get<List<Models.Version>>("Versions");
            rVersion.DataBind();
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Version v = new Models.Version();
            v.id = id;
            v.Title = tTitle.Value;
            v.LatestVersion = tLatestVersion.Value;
            v.AcceptableVersion1 = tAcceptVersion1.Value;
            v.AcceptableVersion2 = tAcceptVersion2.Value;
            v.AcceptableVersion3 = tAcceptVersion3.Value;
            v.Message = tMessage.Value;
            try { v.CheckFrequency = Convert.ToInt32(tUpdateFrequency.Value); }
            catch { v.CheckFrequency = 60; }
            v.lup_user = common.ntID;
            v.lup_dt = DateTime.Now;
            if (id == 0)
            {
                throw new NotImplementedException();             
            }
            else
            {
                try
                {
                    Models.Version.updVersion(v);                    
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Version Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Version Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Versions");
        }

        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadVersions();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        #region Crypto
        protected void bEncrypt_Click(object sender, EventArgs e)
        {
            sCryptResult.InnerHtml = Shared.SysHelper.Encrypt(tCrypto.Value);
        }
        protected void bDecrypt_Click(object sender, EventArgs e)
        {
            sCryptResult.InnerHtml = Shared.SysHelper.Decrypt(tCrypto.Value);
        }        
        #endregion

    }
}