﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.IO.Compression;
//using System.Web.Configuration;
//using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;

namespace QuickLaunch.WebAdmin2014.BasePages
{
    public class SecureUserPage : System.Web.UI.Page
    {
        public SecureUserPage(){}
        
        /// <summary>
        /// Ensures users entering this page are authorized. Called from Init
        /// </summary>
        private Models.UserProfile SessionCheck(string ntID)
        {
            try
            {
                if (HttpContext.Current.Session["IsLogged"] == null) 
                {
                    Models.UserProfile user = Models.UserProfile.selUserProfileById(ntID);
                    user.Roles = Models.UserProfile.selUserProfileRoles(user.id);
                    if (user.Roles.Count > 0) //They must have at least one role to use this tool
                    {                        
                        Session["user"] = user;
                        HttpContext.Current.Session["IsLogged"] = true;
                    }
                    else
                    { //NOT AUTHORIZED
                        Response.Redirect("NotAuthorized.htm");
                    }
                    return user;
                }
                else if (!Convert.ToBoolean(HttpContext.Current.Session["IsLogged"]))
                {
                    Response.Redirect("NotAuthorized.htm");
                }
            }
            catch { Response.Redirect("NotAuthorized.htm"); }
            return null;
        }
        /// <summary>
        /// Overrides Init
        /// </summary>
        protected override void OnInit(EventArgs e)
        {
            Session["ntID"] = common.cleanNTID(Request.ServerVariables["AUTH_USER"]);
            SessionCheck(Session["ntID"].ToString());
            base.OnInit(e);
        }
        
        #region ViewState Handling
        /// <summary>
        /// Saves the view state to the Database. Compression commented out (should help performance)
        /// </summary>
        //protected override void SavePageStateToPersistenceMedium(object p_ViewState)
        //{
        //    LosFormatter formatter = new LosFormatter();
        //    StringWriter writer = new StringWriter();
        //    formatter.Serialize(writer, p_ViewState);
        //    string viewState = writer.ToString();
        //    //byte[] data = Convert.FromBase64String(viewState);
        //    //byte[] compressedData = Compress(data);
        //    string strVS = writer.ToString(); //Convert.ToBase64String(compressedData);
            
        //    // save the string to db
        //    BusinessObjects.ViewState vs = new BusinessObjects.ViewState { GUID = VsKey, RefPage = "Default", Viewstate = strVS, lup_user = Session["ntID"].ToString() };
        //    if (IsPostBack)
        //    {
        //        BusinessObjects.ViewState.updViewState(vs);                                
        //    }
        //    else
        //    {
        //        CleanupViewState(); // Remove all references that have been there for X days!
        //        BusinessObjects.ViewState.insViewState(vs);                
        //    }                       
        //}

        ///// <summary>
        ///// Loads the page's view state from the Database. Compression commented out (should help performance)
        ///// </summary>
        //protected override object LoadPageStateFromPersistenceMedium()
        //{
        //    // Get ViewState from database
        //    BusinessObjects.ViewState vs = BusinessObjects.ViewState.selViewStateByGUID(VsKey);
        //    if (vs == null)
        //        return null;
            
        //    //byte[] data = Convert.FromBase64String(vs.Viewstate);
        //    //byte[] uncompressedData = Decompress(data);
        //    string str = vs.Viewstate; //Convert.ToBase64String(uncompressedData);
        //    LosFormatter formatter = new LosFormatter();
        //    return formatter.Deserialize(str); 
        //}

        ///// <summary>
        ///// A GUID is created to store viewstate with unique key
        ///// </summary>
        private string GenerateGUID()
        {
            return System.Guid.NewGuid().ToString("B");
        }

        ///// <summary>
        ///// Clean up rows to gain space on the DB server
        ///// </summary>
        //private void CleanupViewState()
        //{
        //    try
        //    { // Delete all files whose CreationTime is > X days.                
        //        DateTime _Threshold = DateTime.Now.AddDays(-1);
        //        BusinessObjects.ViewState.delViewState(_Threshold);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new ApplicationException("CleanupFiles Failed in PageBase");
        //    }
        //}
        #endregion

        #region Public properties
        /// <summary>
        /// Stores the HiddenField key representing the GUID of the ViewState of the current page
        /// </summary>
        public string VsKey
        {
            get
            {                
                string _VsKey = "";
                string _VsHiddenKeyValue = Request.Form["__vsKey"]; // Get the HiddenField value from the page

                if (_VsHiddenKeyValue != null)
                {
                    _VsKey = _VsHiddenKeyValue;
                }

                // First time access, generate a key for the ViewState file
                if (!Page.IsPostBack)
                {
                    _VsKey = GenerateGUID();
                    ScriptManager.RegisterHiddenField(Page, "__vsKey", _VsKey); //scriptmanager required for updatepanel
                }
                
                return _VsKey; // Return the VS key 
            }            
        }
        #endregion

        #region Compression
        public static byte[] Compress(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream stream = new GZipStream
            (ms, CompressionMode.Compress);
            stream.Write(data, 0, data.Length);
            stream.Close();
            return ms.ToArray();
        }
        public static byte[] Decompress(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            ms.Write(data, 0, data.Length);
            ms.Position = 0;
            GZipStream stream = new GZipStream(ms, CompressionMode.Decompress);
            MemoryStream temp = new MemoryStream();
            byte[] buffer = new byte[1024];
            while (true)
            {
                int read = stream.Read(buffer, 0, buffer.Length);
                if (read <= 0)
                {
                    break;
                }
                else
                {
                    temp.Write(buffer, 0, buffer.Length);
                }
            }
            stream.Close();
            return temp.ToArray();
        }
        #endregion

        #region "Rendering"
        protected override void Render(HtmlTextWriter writer)
        {
            if (!common.IsAjaxPostBackRequest(Page))
            {
                using (HtmlTextWriter htmlwriter = new HtmlTextWriter(new System.IO.StringWriter()))
                {
                    base.Render(htmlwriter);
                    writer.Write(common.CompressPage(htmlwriter));
                }
            }
            else
                base.Render(writer);
        }
        #endregion

        /// <summary>
        /// Handles page checks for permissions
        /// </summary>
        public static void CheckPageLevelUserPermission(HttpResponse hr, int RoleId, int[] RoleIds = null)
        {
            try
            {
                bool isValid = false;
                if (RoleIds != null)
                {
                    foreach (int i in RoleIds)
                        if (common.user.Roles.Where(r => r.id == i).Any())
                            isValid = true;
                }
                else
                    if (common.user.Roles.Where(r => r.id == RoleId).Any())
                        isValid = true;

                if(!isValid)
                    hr.Redirect("NotAuthorized.htm");
            }
            catch { hr.Redirect("NotAuthorized.htm"); }
        }

    }
}
