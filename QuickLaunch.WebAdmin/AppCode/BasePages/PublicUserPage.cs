﻿using System;
using System.Web;
using System.Web.UI;

namespace QuickLaunch.WebAdmin2014.BasePages
{
    public class PublicUserPage : System.Web.UI.Page
    {
        public PublicUserPage() { }
        
        ///// <summary>
        ///// Ensures users entering this page are authorized. Called from Init
        ///// </summary>
        //private void SessionCheck(string ntID)
        //{
        //    try
        //    {
        //        if (HttpContext.Current.Session["IsLogged"] == null)
        //        {
        //            Models.UserProfile user = Models.UserProfile.selUserProfileById(ntID);
        //            if (user != null)
        //            {
        //                Session["user"] = user;
        //                HttpContext.Current.Session["IsLogged"] = true;
        //            }
        //            else
        //            { //NOT AUTO LOGGED IN
        //                HttpContext.Current.Session["IsLogged"] = false;
        //                Session["user"] = null;
        //            }
        //        }
        //    }
        //    catch { HttpContext.Current.Session["IsLogged"] = false; }
        //}
        /// <summary>
        /// Overrides Init
        /// </summary>
        protected override void OnInit(EventArgs e)
        {
            Session["ntID"] = common.cleanNTID(Request.ServerVariables["AUTH_USER"]);
            //SessionCheck(Session["ntID"].ToString());
            base.OnInit(e);
        }
        
 
        #region "Rendering"
        protected override void Render(HtmlTextWriter writer)
        {
            if (!common.IsAjaxPostBackRequest(Page))
            {
                using (HtmlTextWriter htmlwriter = new HtmlTextWriter(new System.IO.StringWriter()))
                {
                    base.Render(htmlwriter);
                    writer.Write(common.CompressPage(htmlwriter));
                }
            }
            else
                base.Render(writer);
        }
        #endregion

    }
}
