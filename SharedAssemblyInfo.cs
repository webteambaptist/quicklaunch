﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System;
using System.Security;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyCompany("Baptist Health")]
[assembly: AssemblyCopyright("Copyright © Baptist Health 2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyProduct("Quick Launch")]

// Version_QL information for an assembly consists of the following four values:
//
//      Major Version_QL
//      Minor Version_QL 
//      Build Number
//      Revision
//

// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.9.*")]