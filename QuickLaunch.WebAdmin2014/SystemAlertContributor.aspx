﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="SystemAlertContributor.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.SystemAlertUser" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<div>
    <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
        <asp:View runat="server" ID="vList">
        <a href="#Add" class="right add">Add Alert Contributor</a>
        <h1>Alert Contributors</h1><asp:UpdatePanel runat="server" ID="upAJAX" ClientIDMode="Predictable"><ContentTemplate>              
        <table id="rUser">
        <asp:Repeater runat="server" ID="rAlertUser" EnableViewState="true">
        <HeaderTemplate><tr><th value="ntID" title="Sort"><span>Login Id</span></th><th value="userType" title="Sort"><span>User Type</span></th><th value="RoleCt" title="Sort"><span>Roles</span></th></tr></HeaderTemplate>
        <ItemTemplate><tr id='<%#Eval("id") %>'><td><%#Eval("ntID") %></td><td><%#Eval("userType") %></td><td><%#Eval("RoleCt") %></td></tr></ItemTemplate>
        </asp:Repeater><asp:LinkButton runat="server" ID="bRefresh" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" /><input type="hidden" runat="server" id="hideSort" />
        <% if(rAlertUser.Items.Count == 0){ %>
        <tr><td colspan="8"><h2>No contributors found</h2></td></tr>
        <% } %>
        </table></ContentTemplate></asp:UpdatePanel>
        <a href="#Add" class="right add">Add Alert Contributor</a>
        </asp:View>        
        <asp:View runat="server" ID="vDetail">
            <h1><%=(this.id == 0 ? "New Alert Contributor" : "Updating " + tUsername.Value)%></h1>
            <ul id="edit" class="detail">
                <li><span>Username:</span><input type="text" runat="server" id="tUsername" clientidmode="Static" disabled="disabled" maxlength="30" /></li>                                
                <%if (this.id > 0) { %>
                <li title="QL fills this info in based on AD groups"><span>User Type:</span><input type="text" runat="server" id="tUserType" clientidmode="Static" readonly="readonly" /></li>
                <li title="QL fills this in from AD"><span>User Title:</span><input type="text" runat="server" id="tUserTitle" clientidmode="Static" readonly="readonly" /></li>
                <li title="QL fills this in from AD"><span>User Department:</span><input type="text" runat="server" id="tUserDept" clientidmode="Static" readonly="readonly" /></li>
                <asp:UpdatePanel runat="server" ID="up1" ClientIDMode="Static" style="width:85%;padding:0; border:none;"><ContentTemplate>  
                <li>
                <span>Codes:</span> <asp:DropDownList runat="server" ID="ddCode" DataTextField="Code" DataValueField="id" /> <asp:Button runat="server" ID="bAddCode" Text="Add Code" OnClick="bAddCode_Click" CssClass="m5" />
                <ul>
                <asp:Repeater runat="server" ID="rCodes">
                <ItemTemplate>
                    <li><%# Eval("Code")%> <asp:LinkButton runat="server" ID="lbDeleteCode" ClientIDMode="Predictable" CommandArgument='<%# Eval("id") %>' OnClick="lbDeleteCode_Click" ToolTip="Remove Code"><img src="images/delete.gif" align=right /></asp:LinkButton></li>
                </ItemTemplate>
                </asp:Repeater>       
                <%if (this.rCodes.Items.Count == 0) { %>
                    <p>No Codes assigned</p>
                <% } %>
                </ul>                
                </li> 
                <li>
                <span>Locations:</span>
                <asp:DropDownList runat="server" ID="ddLocation" DataTextField="Title" DataValueField="id" /> <asp:Button runat="server" ID="bAddLoc" Text="Add Location" OnClick="bAddLoc_Click" CssClass="m5" />
                <ul>
                <asp:Repeater runat="server" ID="rLocations">
                <ItemTemplate>
                    <li><%# Eval("Title")%> <asp:LinkButton runat="server" ID="lbDeleteLoc" ClientIDMode="Predictable" CommandArgument='<%# Eval("id") %>' OnClick="lbDeleteLoc_Click" ToolTip="Remove Location"><img src="images/delete.gif" align=right /></asp:LinkButton></li>
                </ItemTemplate>
                </asp:Repeater>       
                <%if (this.rLocations.Items.Count == 0) { %>
                    <p>No Locations assigned</p>
                <% } %>
                </ul>                
                </li>
                </ContentTemplate></asp:UpdatePanel>
                <% } %>
            </ul>
            <asp:Button runat="server" ID="bSave" Text="Save Alert Contributor" OnClick="bSave_Click" CssClass="m5" />
            <%if (this.id > 0) { %>
            <asp:Button runat="server" ID="bDelete" Text="Remove Alert Contributor" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to remove this item?');" />
            <% } %>
        </asp:View>
    </asp:MultiView>

</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script type="text/javascript">
    $().ready(function () {        
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        <% } else { %>
        SetGrid();
        <% } %>
    });
    function SetGrid(){
        $("table#rUser tr").click(function () { if ($(this).index() > 0) popModal("SystemAlertContributor.aspx?id=" + $(this).attr('id')); });
        $("a[href=#Add]").click(function () { popModal("SystemAlertContributor.aspx?id=0"); });
    }
</script>
</asp:Content>