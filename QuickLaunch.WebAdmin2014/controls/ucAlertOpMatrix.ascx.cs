﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014.controls
{
    public partial class ucAlertOpMatrix : System.Web.UI.UserControl
    {
        public List<Models.SystemAlertCode> AlertCodes
        {
            get
            {
                if (!WebAdmin2014.Cache.Exists("AlertCodes"))
                    WebAdmin2014.Cache.Insert("AlertCodes", Models.SystemAlertCode.selSystemAlertCodes(), 14);
                return WebAdmin2014.Cache.Get<List<Models.SystemAlertCode>>("AlertCodes");
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}