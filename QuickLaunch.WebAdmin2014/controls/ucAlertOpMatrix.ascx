﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucAlertOpMatrix.ascx.cs" Inherits="QuickLaunch.WebAdmin2014.controls.ucAlertOpMatrix" %>
<%if (AlertCodes!=null){ %>
<table><tr><th>Code</th>
    <th>Bubble Color</th>
    <th>Font Color</th>
    <th>Back Color</th>
    <th>Uses Ticker</th>
    <th>Uses Dialog</th>
    <th>Uses OS Bubble</th>
    <th>Uses Tray Toast</th>
   </tr>
<% foreach (QuickLaunch.Models.SystemAlertCode code in AlertCodes) {%>
   <tr>
    <td class="tLeft"><strong><%=code.Code %></strong></td>
    <td><%=code.IconColor %></td>
    <td><%=code.ForeColor %></td>
    <td><%=code.BackColor %></td>
    <td><%=code.UseTicker ? "<img src='images/check.png' alt='Yes' width='20'>" : string.Empty %></td>
    <td><%=code.UseDialog ? "<img src='images/check.png' alt='Yes' width='20'>" : string.Empty %></td>
    <td><%=code.UseOSBubble ? "<img src='images/check.png' alt='Yes' width='20'>" : string.Empty %></td>
    <td><%=code.UseTrayToast ? "<img src='images/check.png' alt='Yes' width='20'>" : string.Empty %></td>
   </tr>
<% } %>
</table>
<% } %>