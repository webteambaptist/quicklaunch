﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014.controls
{
    public partial class ucMenu : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.SystemAlert.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/systemalert.aspx");
                        string url2 = Page.ResolveUrl("~/systemalertcode.aspx");
                        string url3 = Page.ResolveUrl("~/systemalertresponse.aspx");

                        if (!common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.ErrorLog.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.MachineInventory.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Location.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Reporting.RequiredAdminRoleId).Any())
                        {
                            litList.Text = litList.Text + "<li><a href=\"" + url + "\">System Alerts</a><ul><li><a href=\"" + url3 + "\">Responses</a></li></ul></li>";
                        }
                        else
                        {
                            litList.Text = litList.Text + "<li><a href=\"" + url + "\">System Alerts</a><ul><li><a href=\"" + url2 + "\">Codes</a></li><li><a href=\"" + url3 + "\">Responses</a></li></ul></li>";
                        }
                     }  

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Versioning.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/versioning.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Versioning</a></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.QLDeployment.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/qldeployment.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Deployment</a></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Location.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/location.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Locations</a><ul><li><a href=\"?act=wts\">WTS Locations</a></li><li><a href=\"?act=bulk\">Apply Machines</a></li></ul></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.QLMenu.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/qlapplication.aspx");
                        string url2 = Page.ResolveUrl("~/qlmenu.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Applications</a></li><li><a href=\"" + url2 + "\">QL Menu</a></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Help.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/help.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">QL Help</a><ul><li><a href=\"?act=images\">Images</a></li></ul></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.MachineInventory.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/machineinventory.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Machine Inventory</a><ul><li><a href=\"?act=slow\">Slow Machines</a></li></ul></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.ErrorLog.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/errorlog.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Error Log</a><ul><li><a href=\"?only=QL\">Quick Launch Only</a></li><li><a href=\"?only=WA\">Web Admin Only</a></li></ul></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.User.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/user.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Users</a><ul><li><a href=\"?act=role\">Users by Role</a></li><li><a href=\"?act=userexport\">User Export (.xls)</a></li></ul></li>";
                    }

                    if (common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Reporting.RequiredRoleId).Any())
                    {
                        string url = Page.ResolveUrl("~/reporting.aspx");

                        litList.Text = litList.Text + "<li><a href=\"" + url + "\">Reporting</a></li>";
                    }
                }
            }
            catch (Exception ee)
            {
                Response.Write(ee.Message + " " + ee.Source + " " + ee.StackTrace);
            }
        }
    }
}