﻿using System;
using System.Web;

namespace QuickLaunch.WebAdmin2014
{
    public class AlertHandler : IHttpHandler
    {
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Return false in case your Managed Handler cannot be reused for another request.
            // Usually this would be false in case you have some state information preserved per request.
            get { return true; }
        }

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            try
            {
                context.Response.Write("<h1>System Alert</h1>");
                if (context.Request.QueryString["act"] != null && context.Request.QueryString["key"] != null)
                {
                    string action = context.Request.QueryString["act"] as string;
                    string key = context.Request.QueryString["key"] as string;
                    Models.SystemAlert sa = Models.SystemAlert.selSystemAlertByDisableKey(new Guid(key));
                    if (sa == null) { context.Response.Write("<h3>Your key has expired, and is no longer valid. Please contact an admin to get the new valid link.</h3>"); }
                    context.Response.Write(string.Format("<h3><i>{0}</i></h3>", sa.Name));
                    sa.lup_user = common.cleanNTID(context.Request.ServerVariables["AUTH_USER"].ToString());
                    sa.lup_dt = DateTime.Now;
                    if (action == "disable")
                    {
                        if (!sa.IsEnabled) { context.Response.Write(string.Format("<strong style='color:red;'>Alert has already been disabled</strong>")); return; }
                        sa.IsEnabled = false;
                        Models.SystemAlert.updToggleIsEnabled(sa);
                        context.Response.Write(string.Format("<div style='padding:10px;background-color:#99FF66;font-weight:bold;color:green;'>ALERT DISABLED</div>"));
                    }
                    else if (action == "reenable")
                    {
                        if (sa.IsEnabled) { context.Response.Write(string.Format("<strong style='color:red;'>Alert is already enabled</strong>")); return; }
                        if (sa.DowntimeDt > DateTime.Now) { context.Response.Write(string.Format("<strong style='color:red;'>Alert active downtime datetime has not started ({0}). Please wait, or contact an admin to update it.</strong>", sa.DowntimeDt.ToString())); return; }
                        if (sa.EndDt < DateTime.Now) { context.Response.Write(string.Format("<strong style='color:red;'>Alert end date has already passed, so it is too late to reenable. Please have an admin update it.</strong>")); return; }
                        sa.IsEnabled = true;
                        Models.SystemAlert.updToggleIsEnabled(sa);
                        context.Response.Write(string.Format("<div style='padding:10px;background-color:#99FF66;font-weight:bold;color:green;'>ALERT ENABLED</div>"));
                    }
                } else
                    context.Response.Write("No action specified");
            }
            catch (Exception ex) { context.Response.Write(string.Format("Error Occurred : {0}", ex.ToString())); }
        }

        #endregion
    }
}
