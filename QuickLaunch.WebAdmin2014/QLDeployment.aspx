﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" ValidateRequest="false" CodeBehind="QLDeployment.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.QLDeployment" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
        <asp:UpdatePanel runat="server" ID="upMain"><ContentTemplate>
        <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
            <asp:View runat="server" ID="vList">
                <asp:LinkButton ID="bAdd1" runat="server" class="right add" Text="Add Deployment" OnClick="bAdd_Click"></asp:LinkButton>
                <h1>Quick Launch Deployments <small class="f9"></small></h1>
                <asp:Panel runat="server" ID="upAJAX" ClientIDMode="Static">
                        <table id="tDeploy">
                            <asp:Repeater runat="server" ID="rQLDeploy" EnableViewState="true">
                                <HeaderTemplate>
                                    <tr>
                                        <th colspan="2">Title</th>
                                        <th>QL Version</th>
                                        <th>Build Size</th>
                                        <th>Mch<br />
                                            Ct</th>
                                        <th>Publish Date</th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr class="<%#((bool)Eval("IsPublished")) ? "bgLGreen" : "bgWSmoke"%>" id="<%#Eval("id") %>" title="<%#Eval("Description")%>">
                                        <td title="Copy deployment as new"><a href="#Copy">
                                            <img src="images/copy.png" alt="Copy" /></a></td>
                                        <td class="tLeft"><%#Eval("Title") %></td>
                                        <td><%#Eval("MSIBuildVersion")%></td>
                                        <td><%#QuickLaunch.Shared.SysHelper.ConvertToMB(Eval("MSIBuildFileSize")).ToString("N3")%> MB</td>
                                        <td title="Click count to view applied machines"><%#Eval("MachineCt") %></td>
                                        <td class="<%#((bool)Eval("IsPublished")) ? "cG" : "cR"%>" title="<%#((bool)Eval("IsPublished")) ? "Deployment Published" : "Not Published"%>"><%#Eval("publish_dt")%></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                        <asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />
                    
                </asp:Panel>
                <asp:LinkButton ID="bAdd2" runat="server" class="right add" Text="Add Deployment" OnClick="bAdd_Click"></asp:LinkButton>
                
                <p class="clear"><b>Note:</b> The interval at which a machine checks for a deployment update is modifiable in the versioning section</p>
                <p class="clear"><b>Note:</b> If a machine has a newer(higher version) install than a published deployment assigned to it, it will not accept the newly assigned deployment package. Consequently, a rollback can have older source code, but will require a higher version number. Deleting a new deployment package with a high version number, will not result in older packages overtaking that newer version.</p>
            </asp:View>
            <asp:View runat="server" ID="vDetail">
                <h1><%=(this.id == 0 ? "New" : "Update") %> Deployment</h1>

                <ul id="edit" class="detail">
                    <li title="Deployment Title"><span>Title:</span><input type="text" runat="server" id="tTitle" maxlength="150" /></li>
                    <li title="Deployment details to better describe its purpose"><span>Description:</span><textarea runat="server" id="tDesc" /></li>
                    <li title="This is the message the user will see while the install is taking place"><span>Install Message:</span><textarea runat="server" id="tInstallMessage">Quick Launch is installing updates to better your experience.<br />Please contact a SSO specialist if you have any issues.</textarea></li>
                    <li title="MSI File Details"><span>MSI Build:</span>
                        <%if (this.id == 0) { %>
                        <input type="file" runat="server" id="fMSI" />
                        <% } else { %>
                        <label runat="server" id="lblMSIBuild"></label>
                        <a href="?id=<%=this.id%>&msi=1" target="_blank">Download</a>
                        <% } %>
                    </li>
                    <li>
                        <asp:Panel runat="server" ID="up1" ClientIDMode="Static" style="width: 85%; padding: 0; border: none;">
                                <span>Publish Locations:</span><input type="checkbox" runat="server" id="cbIsEverywhere" />
                                <label for="cbIsEverywhere">Everywhere</label><br />
                                <div style="width: 85%; <%=cbIsEverywhere.Checked ? "display:none;": string.Empty %>" id="pLocations" class="m5">
                                    <asp:CheckBoxList runat="server" RepeatColumns="3" RepeatDirection="Horizontal" ID="ddLocation" DataTextField="Title" DataValueField="id" />
                                </div>
                        </asp:Panel>
                    </li>
                    <li title="Determines if deployment is active or not"><span>Published?:</span><input type="checkbox" runat="server" id="cbIsPublished" /></li>
                    <li title="DateTime the deployment package was set to publish"><span>Publish Date:</span><input type="text" runat="server" id="tPublishDt" disabled="disabled" /></li>
                    <li><span>Date Added:</span><input type="text" runat="server" id="tAddDt" disabled="disabled" /></li>
                    <li><span>Last Update User:</span><input type="text" runat="server" id="tLupUser" disabled="disabled" /></li>
                    <li><span>Last Update DateTime:</span><input type="text" runat="server" id="tLupDT" disabled="disabled" /></li>
                </ul>

                <asp:Button runat="server" ID="bSave" Text="Save Deployment" OnClick="bSave_Click" CssClass="m5" />
                <%if (this.id > 0)
                  { %>
                <asp:Button runat="server" ID="bDelete" Text="Remove Deployment" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
                <% } %>
            </asp:View>
        </asp:MultiView>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="bSave" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>

<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
    <script type="text/javascript">
        $().ready(function () {
        //<%if (mv.ActiveViewIndex == 1)
          { %>
        //$("#nav").hide();
        //$("#mainRight").css("width", "97%");
        //$(".page").css("width", "97%");
        //$(".main").css("margin", "0");
        SetPage();
        <% }
          else
          { %>
        SetGrid();
        <% } %>
    });
        function SetPage() {
            $("#cbIsEverywhere").click(function () { $("#pLocations").slideToggle(); }); //if($(this).checked) $("#divLocations").hide(); else $("#divLocations").slideDown();
        }
        function redirect(url) {
            //console.log(url);
            window.location = url;
        }
        function SetGrid() {
            $("table#tDeploy tr td").click(function () {
                if ($(this).index() == 4) {
                    window.location = 'machineinventory.aspx?q=' + $(this).prev().prev().html();
                } else if ($(this).parent().index() > 0 && $(this).index() > 0) redirect("QLDeployment.aspx?id=" + $(this).parent().attr('id'));
            });
            //$("a[href=#Add]").click(function () { popModal("QLDeployment.aspx?id=0"); });
            $("a[href=#Copy]").click(function () { redirect("QLDeployment.aspx?id=0&copy=" + $(this).parent().parent().attr("id")); });
        }
    </script>
</asp:Content>
