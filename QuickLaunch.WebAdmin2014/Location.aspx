﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="Location.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.Location" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
        <asp:UpdatePanel runat="server" ID="upMain">
            <ContentTemplate>
                <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
                    <asp:View runat="server" ID="vList">
                        <asp:LinkButton runat="server" ID="bAdd1" class="right add" Text="Add Location" OnClick="bAdd_Click" />
                        <h1>Locations <small class="f9"></small></h1>
                        <asp:Panel runat="server" ID="upAJAX" ClientIDMode="Static">
                                <table id="rLoc">
                                    <asp:Repeater runat="server" ID="rLocation" EnableViewState="true">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Last Updated</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr id="<%#Eval("id") %>">
                                                <td><%#Eval("Title") %></td>
                                                <td><%#Eval("Description")%></td>
                                                <td><%#Eval("lup_dt")%></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </table>
                                <asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />
                        </asp:Panel>
                        <asp:LinkButton runat="server" ID="bAdd2" class="right add" Text="Add Location" OnClick="bAdd_Click" />
                    </asp:View>
                    <asp:View runat="server" ID="vDetail">
                        <h1><%=(this.id == 0 ? "New" : "Update") %> Location</h1>
                        <asp:Panel runat="server" ID="UpdatePanel1" ClientIDMode="Static">
                                <ul id="edit" class="detail">
                                    <li title="Location title"><span>Title:</span><input type="text" runat="server" id="tTitle" maxlength="150" /></li>
                                    <li title="Detailed info on how to find the location"><span>Description:</span><input type="text" runat="server" id="tDesc" maxlength="100" /></li>
                                    <li>
                                        <asp:UpdatePanel runat="server" ID="up1" ClientIDMode="Static" style="width: 85%; padding: 0; border: none;">
                                            <ContentTemplate>
                                                <span>IP Addresses:</span><input type="text" runat="server" id="tIp" maxlength="25" />
                                                <asp:Button runat="server" ID="bAddIp" Text="Add IP" OnClick="bAddIp_Click" CssClass="m5" CommandArgument="INCLUDE" />
                                                <asp:Button runat="server" ID="bExcludeIp" Text="Exclude IP" CommandArgument="EXCLUDE" OnClick="bAddIp_Click" CssClass="m5 cR" />
                                                <ul>
                                                    <asp:Repeater runat="server" ID="rIp">
                                                        <ItemTemplate>
                                                            <li class="<%# ((bool)Eval("IsExcluded")) ? "cR" : string.Empty%>"><%# Eval("Ip")%>
                                                                <asp:LinkButton runat="server" ID="lbDeleteIp" ClientIDMode="Predictable" CommandArgument='<%# Eval("id") %>' OnClick="lbDeleteIp_Click" ToolTip="Remove Ip"><img src="images/delete.gif" align="right" /></asp:LinkButton></li>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                    <%if (this.rIp.Items.Count == 0)
                                                      { %>
                                                    <p>No IP addresses assigned</p>
                                                    <% } %>
                                                </ul>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </li>
                                    <li><span>Last Update User:</span><input type="text" runat="server" id="tLupUser" disabled="disabled" /></li>
                                    <li><span>Last Update DateTime:</span><input type="text" runat="server" id="tLupDT" disabled="disabled" /></li>
                                </ul>
                        </asp:Panel>
                        <asp:Button runat="server" ID="bSave" Text="Save Location" OnClick="bSave_Click" CssClass="m5" />
                        <%if (this.id > 0)
                          { %>
                        <asp:Button runat="server" ID="bDelete" Text="Remove Location" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
                        <% } %>
                    </asp:View>
                    <asp:View runat="server" ID="vWTS">
                        <h1>WTS Locations [<%=rWTS.Items.Count.ToString()%> results]</h1>
                        <table id="tWTS">
                            <asp:Repeater runat="server" ID="rWTS" EnableViewState="false">
                                <HeaderTemplate>
                                    <tr>
                                        <th><span>Client Name</span></th>
                                        <th><span>Device Location</span></th>
                                        <th><span>Default Printer</span></th>
                                        <th><span>Last Update</span></th>
                                    </tr>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%#Eval("ClientName")%></td>
                                        <td class="tLeft"><%#Eval("Device_Location")%></td>
                                        <td><%#Eval("Default_Printer")%></td>
                                        <td><%#((DateTime)Eval("lup_dt")) %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </table>
                    </asp:View>
                    <asp:View runat="server" ID="vBulkApply">
                        <h1>Bulk Apply Location</h1>
                        <div class="StaticNotify" style="background-color: whitesmoke;" runat="server" enableviewstate="false" id="Msg"></div>
                        <ul id="Ul1" class="detail">
                            <li><span>Location:</span><asp:DropDownList runat="server" ID="ddLocation" DataTextField="Title" DataValueField="id" /></li>
                            <li title="Comma separated S/N's that should be associated to the selected location"><span>S/N's:<br />
                                <small>comma separated</small></span><textarea runat="server" id="tMachines" /></li>
                        </ul>
                        <asp:Button runat="server" ID="bApplyBulkLocation" Text="Apply Location" OnClick="bApplyBulkLocation_Click" CssClass="m5" />
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>

</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
    <script src="js/sort.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $().ready(function () {
        <%if (mv.ActiveViewIndex != 1)
          { %>
        SetGrid();
        <% } %>
        });
        function redirect(url) {
            //console.log(url);
            window.location = url;
        }
        function SetGrid() {
            $("table#rLoc tr td").click(function () {
                if ($(this).parent().index() > 0) redirect("Location.aspx?id=" + $(this).parent().attr('id'));
            });
            //$("a[href=#Add]").click(function () { popModal("Location.aspx?id=0"); });
            $("#tWTS,#rLoc").tablesorter();
        }
    </script>
</asp:Content>
