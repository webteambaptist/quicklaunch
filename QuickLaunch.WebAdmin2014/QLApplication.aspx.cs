﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class QLApplication : QuickLaunch.WebAdmin2014.BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 1;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New Item
                    mv.ActiveViewIndex = 1; 
                }
                else
                    LoadApps();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.Application app = Models.Application.selApplicationById(id);
            if (app == null) return;
            tTitle.Value = app.Title;
            tValue.Value = app.Value;
            tWindowTitle.Value = app.WindowTitle;
            cbAllowRoaming.Checked = app.AllowRoaming;
            ddIcon.Value = app.DefaultImg ?? string.Empty;
            tLupUser.Value = app.lup_user;
            tLupDT.Value = app.lup_dt.ToString();
        }
        private void LoadApps()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Apps"))
                WebAdmin2014.Cache.Insert("Apps", Models.Application.selApplications(), 7);
            rQLApp.DataSource = WebAdmin2014.Cache.Get<List<Models.Application>>("Apps");
            rQLApp.DataBind();
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Application a = new Models.Application();
            a.id = id;
            a.Title= tTitle.Value;
            a.Value = tValue.Value;
            a.WindowTitle = tWindowTitle.Value;
            a.AllowRoaming = cbAllowRoaming.Checked;
            a.DefaultImg = ddIcon.Value;
            a.lup_user = common.user.ntID;
            a.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    Models.Application.insApplication(a);
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Application Add Failed!');"); return; }
            }
            else
            {
                try
                {
                    Models.Application.updApplication(a);                  
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Application Update Failed!');"); return; }
            }
            //common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Application Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Apps");

            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "QLApplication.aspx");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadApps();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.Application.delApplication(new Models.Application { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Application Removed!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Apps");

            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "QLApplication.aspx");
        }

        protected void bAdd_Click(object sender, EventArgs e)
        {
            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "QLApplication.aspx?id=0");
        }
                
    }
}