﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="MachineInventory.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.MachineInventory" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
        <asp:UpdatePanel runat="server" ID="upMain">
            <ContentTemplate>
                <input type="hidden" runat="server" id="hideSort" />
                <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
                    <asp:View runat="server" ID="vList">
                        <asp:LinkButton runat="server" ID="lbExport" CssClass="right m5 cG" OnClick="lbExport_Click">Export to Excel</asp:LinkButton>
                        <h1>Machine Inventory</h1>
                        <asp:Panel runat="server" ID="upAJAX" ClientIDMode="Predictable">
                            <span class="right m5 cR"><%=rMachine.Rows.Count.ToString()%> Items</span>
                            <asp:TextBox runat="server" ID="tFilter" EnableViewState="false" />
                            <asp:Button runat="server" ID="bFilter" Text="Filter" OnClick="tFilter_Click" EnableViewState="false" />
                            <asp:GridView ID="rMachine" runat="server" AllowPaging="true" PageSize="50" EmptyDataText="No Data Available" OnPageIndexChanging="rMachine_PageIndexChanging" ShowHeaderWhenEmpty="True" PageIndex="1">
                                <PagerSettings Mode="NextPreviousFirstLast" Position="Bottom" Visible="true" />

                            </asp:GridView>
                            <asp:LinkButton runat="server" ID="bRefresh" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />
                        </asp:Panel>
                    </asp:View>
                    <asp:View runat="server" ID="vDetail">
                        <h1><%=(this.id == "-1" ? "New" : "View") %> Machine [<%=tMachineName.Value%>]</h1>
                        <div id="tab-container" class="tab-container">
                            <ul class='etabs'>
                                <li class='tab'><a href="#tabInfo">Basic Info</a></li>
                                <li class='tab'><a href="#tabSystem">System Info</a></li>
                                <li class='tab'><a href="#tabOS">Operating System</a></li>
                                <li class='tab'><a href="#tabProcs">Processes</a></li>
                                <li class='tab'><a href="#tabUsers">Users</a></li>
                                <li class='tab'><a href="#tabActivity">Activity</a></li>
                                <li class='tab'><a href="#tabError">Latest Errors</a></li>
                            </ul>
                            <div class="panel-container">
                                <div id="tabInfo">
                                    <ul id="edit" class="detail">
                                        <li><span>Machine Name:</span><input type="text" runat="server" id="tMachineName" disabled="disabled" /></li>
                                        <li><span>LSID:</span><input type="text" runat="server" id="tLSID" readonly="readonly" /></li>
                                        <li><span>Location:</span><asp:DropDownList runat="server" ID="ddLocation" DataTextField="Title" DataValueField="id" /></li>
                                        <li title="Based on IP"><span>Hospital:</span><input type="text" runat="server" id="tHospital" readonly="readonly" /></li>
                                        <li><span>WTS Location:</span><input type="text" runat="server" id="tWTSLocation" readonly="readonly" /></li>
                                        <li><span>Description:</span><asp:TextBox runat="server" ID="tDesc" /></li>
                                        <li><span>QL Version:</span><input type="text" runat="server" id="tQLVersion" readonly="readonly" /></li>
                                        <li><span>ICA Version:</span><input type="text" runat="server" id="tICAVersion" readonly="readonly" /></li>
                                        <li><span>IE Version:</span><input type="text" runat="server" id="tIEVersion" readonly="readonly" /></li>
                                        <li title="RAM"><span>Memory:</span><input type="text" runat="server" id="tMemory" readonly="readonly" /></li>
                                        <li><span>IP Address:</span><asp:TextBox runat="server" ID="tIpAddress" /></li>
                                        <li><span>Auto Install Version:</span><input type="text" runat="server" id="tAIVersion" readonly="readonly" />&nbsp;<input type="checkbox" runat="server" id="cbIsUpdateSvcRunning" disabled="disabled" title="Indicates whether or not the update service is running on the machine" />&nbsp;<small>Running?</small></li>
                                        <li><span>Is Excluded?
                                    <asp:CheckBox runat="server" ID="cbIsExcluded" /></span></li>
                                        <li><span>Last Login:</span><input type="text" runat="server" id="tLastLogin" readonly="readonly" /></li>
                                        <li><span>Last Update:</span><input type="text" runat="server" id="tLastUpdate" readonly="readonly" /></li>

                                        <asp:Button runat="server" ID="bSave" Text="Save Machine" OnClick="bSave_Click" CssClass="m5" />
                                        <asp:Label runat="server" ID="lSuccess" Visible="false" Width="200px"></asp:Label>
                                    </ul>
                                </div>
                                <div id="tabSystem">
                                    <span class="right bgBeige">Last Refreshed: <%=tLastUpdate.Value %></span>
                                    <span runat="server" id="lblSysInfo">System Info</span>
                                </div>
                                <div id="tabOS">
                                    <span class="right bgBeige">Last Refreshed: <%=tLastUpdate.Value %></span>
                                    <span runat="server" id="lblOS">Operating System</span>
                                </div>
                                <div id="tabProcs">
                                    <span class="right bgBeige">Last Refreshed: <%=tLastUpdate.Value %></span>
                                    <table id="rProc" class="f9">
                                        <asp:Repeater runat="server" ID="rProcs" EnableViewState="false">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Window Title</th>
                                                    <th>Memory Size</th>
                                                    <th>Thread Count</th>
                                                    <th>IsResponding?</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%#Eval("Name") %></td>
                                                    <td><%#Eval("WindowTitle") %></td>
                                                    <td><%#Eval("MemorySize")%></td>
                                                    <td><%#Eval("ThreadCt")%></td>
                                                    <td><%#Eval("IsResponding")%></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (rProcs.Items.Count == 0)
                                   { %>
                                        <tr>
                                            <td colspan="5">
                                                <h2>No Processes Found</h2>
                                            </td>
                                        </tr>
                                        <% } %>
                                    </table>
                                </div>
                                <div id="tabUsers">
                                    <table id="tUser" class="f9">
                                        <asp:Repeater runat="server" ID="rUser" EnableViewState="false">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th value="ntID"><span>Login Id</span></th>
                                                    <th value="userType"><span>User Type</span></th>
                                                    <th value="lastLogin"><span>Last Login</span></th>
                                                    <th value="lastLoginLength"><span>Last Login Length</span></th>
                                                    <th value="lastEMRLaunchLength"><span>Last EMR Launch Length</span></th>
                                                    <th value="lastRoamLength"><span>Last Roam Length</span></th>
                                                    <th value="roamingCt"><span>Roam Ct</span></th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr id="<%#Eval("id") %>">
                                                    <td><%#Eval("ntID") %></td>
                                                    <td><%#Eval("userType") %></td>
                                                    <td><%#Eval("lastLogin") %></td>
                                                    <td><%#(double)Eval("lastLoginLength") / 1000 %></td>
                                                    <td><%#(double)Eval("lastEMRLaunchLength") / 1000%></td>
                                                    <td><%#(double)Eval("lastRoamLength") / 1000%></td>
                                                    <td><%#Eval("roamingCt") %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (rUser.Items.Count == 0)
                                   { %>
                                        <tr>
                                            <td colspan="7">
                                                <h2>No Users have used this machine recently</h2>
                                            </td>
                                        </tr>
                                        <% } %>
                                    </table>
                                </div>
                                <div id="tabActivity">
                                    <h3>Machine Activity</h3>
                                    <uc:EventChart runat="server" ID="ecActivity" />
                                    <h3>Latest Activity <small>(max 100)</small></h3>
                                    <table id="tActivity" class="f9">
                                        <asp:Repeater runat="server" ID="rActivity" EnableViewState="false" ClientIDMode="Static">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Event</th>
                                                    <th>Time Taken</th>
                                                    <th>User</th>
                                                    <th>Date/Time</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr id="<%#Eval("id") %>" title="<%#Eval("Application") %>">
                                                    <td><%#Eval("EventCode") %></td>
                                                    <td><%#(Convert.ToDouble(Eval("LengthOfTime")) / 1000).ToString("N4") + " sec" %></td>
                                                    <td><%#Eval("NtId")%></td>
                                                    <td><%#((DateTime)Eval("lup_dt")) %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (rActivity.Items.Count == 0)
                                   { %>
                                        <tr>
                                            <td colspan="4">
                                                <h2>No Activity Available</h2>
                                            </td>
                                        </tr>
                                        <% } %>
                                    </table>
                                </div>
                                <div id="tabError">
                                    <h3>Most Recent Errors</h3>
                                    <table id="rError" class="f9">
                                        <asp:Repeater runat="server" ID="rErrors" EnableViewState="false" ClientIDMode="Static">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Date/Time</th>
                                                    <th>Application</th>
                                                    <th>Affected User</th>
                                                    <th>Exception</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr id="<%#Eval("id") %>">
                                                    <td><%#((DateTime)Eval("lup_dt")) %></td>
                                                    <td<%# (((bool)Eval("IsFatal")) ? " class='cR'" : "") %>><%#Eval("Application") %></td><td><%#Eval("CurrentUserId")%></td><td class="tLeft"><%#(((string)Eval("Exception")).Length >= 350 ? ((string)Eval("Exception")).Substring(0,349) : Eval("Exception")) %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                        <% if (rErrors.Items.Count == 0)
                                   { %>
                                        <tr>
                                            <td colspan="4">
                                                <h2>No Errors to Report!</h2>
                                            </td>
                                        </tr>
                                        <% } %>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                    <asp:View runat="server" ID="vSlow">
                        <asp:LinkButton runat="server" ID="lbSlowMachineExport" CssClass="right m5 cG" OnClick="lbSlowMachineExport_Click">Export to Excel</asp:LinkButton>
                        <asp:Panel runat="server" ID="upAJAXSlow" ClientIDMode="Predictable">
                            <h1>Slowest Machines on 30 Day Avg [<%=rSlowMach.Items.Count.ToString()%>results]</h1>
                            <table id="tSlowFilter" class="w100 tCenter m3 bgWSmoke">
                                <tr>
                                    <td>Login Length Lower:
                                    <asp:TextBox runat="server" ID="tLoginLower" EnableViewState="true" Text="3" /><small>sec</small></td>
                                    <td>Launch Length Lower:
                                    <asp:TextBox runat="server" ID="tLaunchLower" EnableViewState="true" Text="40" /><small>sec</small></td>
                                    <td>Roam Length Lower:
                                    <asp:TextBox runat="server" ID="tRoamLower" EnableViewState="true" Text="20" /><small>sec</small><asp:Button runat="server" ID="bFilterSlowRpt" Text="Filter" OnClick="bFilterSlowRpt_Click" EnableViewState="false" CssClass="hidden" /><asp:Button runat="server" ID="bFilterSlowRptSort" Text="Sort" OnClick="bFilterSlowRptSort_Click" EnableViewState="false" CssClass="hidden" /></td>
                                </tr>
                            </table>
                            <table id="tSlowMach">
                                <asp:Repeater runat="server" ID="rSlowMach" EnableViewState="false">
                                    <HeaderTemplate>
                                        <tr>
                                            <th value="PCName"><span>Machine Name</span></th>
                                            <th value="LSID"><span>LSID</span></th>
                                            <th value="Memory"><span>Memory</span></th>
                                            <th value="LocationTitle"><span>Location</span></th>
                                            <th value="AvgLoginLength"><span>AVG<br />
                                                Login Length</span></th>
                                            <th value="AvgEMRLaunchLength"><span>AVG<br />
                                                EMR Launch Length</span></th>
                                            <th value="AvgRoamLength"><span>AVG<br />
                                                Roam Length</span></th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="<%#Eval("id") %>" title="<%#Eval("Description") %>">
                                            <td><%#Eval("PCName")%></td>
                                            <td><%#Eval("LSID")%></td>
                                            <td><%#QuickLaunch.Shared.SysHelper.ConvertToGB(Eval("Memory")).ToString("N3") + "GB"%></td>
                                            <td width="150"><%#Eval("WTSLocation")%></td>
                                            <td<%#Convert.ToDouble(Eval("AvgLoginLength") != DBNull.Value ? Eval("AvgLoginLength") : "0") >= Convert.ToDouble(tLoginLower.Text) ? " class='cR'" : ""%>><%#Convert.ToDouble(Eval("AvgLoginLength") != DBNull.Value ? Eval("AvgLoginLength") : "0").ToString("N2")%>s</td><td<%#Convert.ToDouble(Eval("AvgEMRLaunchLength") != DBNull.Value ? Eval("AvgEMRLaunchLength") : "0") >= Convert.ToDouble(tLaunchLower.Text) ? " class='cR'" : ""%>><%#Convert.ToDouble(Eval("AvgEMRLaunchLength") != DBNull.Value ? Eval("AvgEMRLaunchLength") : "0").ToString("N2")%>s</td><td<%#Convert.ToDouble(Eval("AvgRoamLength") != DBNull.Value ? Eval("AvgRoamLength") : "0") >= Convert.ToDouble(tRoamLower.Text) ? " class='cR'" : ""%>><%#Convert.ToDouble(Eval("AvgRoamLength") != DBNull.Value ? Eval("AvgRoamLength") : "0").ToString("N2")%>
                                            s</td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                        </asp:Panel>
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>

        </asp:UpdatePanel>
    </div>
</asp:Content>

<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
    <%if (mv.ActiveViewIndex == 1)
      { %>
    <script src="js/jquery-easytabs.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript" src="js/flot/jquery.flot.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/flot/jquery.flot.stack.min.js"></script>
    <script language="javascript" type="text/javascript" src="js/flot/EventLogSnapshotChart.js"></script>
    <% } %>
    <script type="text/javascript">
        $().ready(function () {
        <%if (mv.ActiveViewIndex == 1)
          { %>
            //$("#nav").hide();
            //$("#mainRight").css("width", "97%");
            //$(".page").css("width", "97%");
            //$(".main").css("margin", "0");
            $("#tab-container").easytabs();
            machine = "<%=tMachineName.Value%>";
        if (machine.length > 0)
            RefreshChart();
        <% }
          else if (mv.ActiveViewIndex == 0 || mv.ActiveViewIndex == 2)
          { %>
            SetGrid();
        <% } %>
        });
        function redirect(url) {
            //console.log(url);
            window.location = url;
        }
        function SetGrid() {
            $("table#tSlowMach tr").click(function () {
                if ($(this).index() > 0) redirect("MachineInventory.aspx?id=" + $(this).find('td:first-child').text());
            });
            $("table#rMachine tr").click(function () {
                if ($(this).index() > 0) redirect("MachineInventory.aspx?id=" + $(this).find('td:first-child').text());
            });
            $("table#rMachine tr th span").click(function () {
                $('#hideSort').val($(this).parent().attr('value')); document.getElementById("bRefresh").click();
            });
            $("table#tSlowFilter input[type=text]").change(function () {
                document.getElementById("bFilterSlowRpt").click();
            });
            $("table#tSlowMach tr th span").click(function () {
                $('#hideSort').val($(this).parent().attr('value')); document.getElementById("bFilterSlowRptSort").click();
            });
        }
    </script>
</asp:Content>
