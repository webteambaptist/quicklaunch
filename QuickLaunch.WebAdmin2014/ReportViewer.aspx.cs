﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class ReportViewer : QuickLaunch.WebAdmin2014.BasePages.SecureUserPage
    {
        #region Properties
        public static int RequiredRoleId = 11;
        public static int RequiredAdminRoleId = 12;
        public static bool IsReportingAdmin { get { try { return common.user.Roles.Where(r => r.id == RequiredAdminRoleId).Any(); } catch { return false; } } }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string url = Request.Url.ToString();
                string id = Request.QueryString["id"];
                //  string orderNumber = "47";

                if (!string.IsNullOrEmpty(id))
                {
                    getReport(id);
                }
            }
        }

        private static void getReport(string id)
        {
            Models.Report rep = Models.Report.selReportById(int.Parse(id));

            int ind = rep.ReportURL.LastIndexOf("=");
            ind = ind + 1;
            string path = rep.ReportURL.Substring(ind);
            path = path.Replace("%2f", "/");
            path = path.Replace("+", " ");
            path = path.Trim();
            //path = path.Substring(1);
            
            
            //ReportViewer1.ServerReport.ReportPath = path; //"TITOFolder/620PerformanceReport.rdlc";
        }
    }
}