﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace QuickLaunch.WebAdmin2014
{
    public class Global : System.Web.HttpApplication
    {
        public static string ApplicationName = "WEBADMIN";
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup

        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            //if (!Request.Url.ToString().Contains("localhost"))
            //{
                System.Exception error = Server.GetLastError().GetBaseException();
                Models.ErrorLog.AddErrorToLog(error, common.ntID, ApplicationName, false);
                //mail.sendMail(new mail { from = "TITO_Web_Admin@bmcjax.com", sendto = new List<string> { "jonathan.ehlen@bmcjax.com" }, subject = "An error occurred with TITO Web Admin", body = "User (If Available): " + Request.ServerVariables["AUTH_USER"].ToString() + "<br /><br />Message: " + error.Message + "<br /><br />URL:" + Request.Url.ToString() + "<br /><br />Stack Trace:" + error.StackTrace, priority = 1 });
            //}
        }

        //void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    // Bug fix for MS SSRS Blank.gif 500 server error missing parameter IterationId
        //    // https://connect.microsoft.com/VisualStudio/feedback/details/556989/
        //    //if (HttpContext.Current.Request.Url.PathAndQuery.Contains("/Reserved.ReportViewerWebControl.axd"))
        //    //{
        //    //    Context.RewritePath(String.Concat(HttpContext.Current.Request.Url.PathAndQuery, "&IterationId=0"));
        //    //}
        //}

        void Session_Start(object sender, EventArgs e)
        {
            // Code that runs when a new session is started

        }

        void Session_End(object sender, EventArgs e)
        {
            // Code that runs when a session ends. 
            // Note: The Session_End event is raised only when the sessionstate mode
            // is set to InProc in the Web.config file. If session mode is set to StateServer 
            // or SQLServer, the event is not raised.

        }

    }
}
