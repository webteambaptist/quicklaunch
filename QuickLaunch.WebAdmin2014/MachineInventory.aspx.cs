﻿using QuickLaunch.WebAdmin2014;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace QuickLaunch.WebAdmin2014
{
    public partial class MachineInventory : QuickLaunch.WebAdmin2014.BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 8;
        protected string id { get { return (string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"; } }
        protected string action { get { return ((string)System.Web.HttpContext.Current.Request.QueryString["act"]) ?? string.Empty; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (!common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.ErrorLog.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.MachineInventory.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Location.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Reporting.RequiredAdminRoleId).Any())
                {
                    bSave.Visible = false;
                }

                if (action == "export")
                { //Machine Export
                    lbExport_Click(null, null);
                    return;
                }
                else if (action == "slow")
                {                    
                    bFilterSlowRpt_Click(null, null);
                    mv.ActiveViewIndex = 2;
                    return;
                }

                if (!WebAdmin2014.Cache.Exists("Locations"))
                    WebAdmin2014.Cache.Insert("Locations", Models.Location.selLocations(), 7);
                ddLocation.DataSource = WebAdmin2014.Cache.Get<List<Models.Location>>("Locations");
                ddLocation.DataBind();
                ddLocation.Items.Insert(0, new ListItem("Select Location","0"));

                if (id != "-1")
                    LoadDetail();
                else {
                    if (Request.QueryString["q"] != null)
                        tFilter.Text = Request.QueryString["q"] as string;
                    LoadMachines(tFilter.Text);
                }
            }
        }

        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.Machine m = Models.Machine.selMachineById(id);
            
            if (m == null) return;

            cbIsExcluded.Checked = m.isExcluded;
            tMachineName.Value = m.PCName;
            try {ddLocation.SelectedValue = m.LocationId.ToString(); } catch { }
            tHospital.Value = Shared.SysHelper.GetHospitalFromIp(m.IpAddress ?? string.Empty); 
            tWTSLocation.Value = m.WTSLocation ?? string.Empty;
            tDesc.Text = m.Description;
            tLSID.Value = m.LSID;
            tQLVersion.Value = m.QLVersion;
            tICAVersion.Value = m.ICAVersion;
            tIEVersion.Value = m.IEVersion;
            tAIVersion.Value = m.AutoInstallVersion ?? string.Empty;
            tIpAddress.Text = m.IpAddress ?? string.Empty;
            cbIsUpdateSvcRunning.Checked = (m.Processes ?? string.Empty).Contains("QuickLaunch.Install"); //m.IsUpdateSvcRunning;
            try { tMemory.Value = Shared.SysHelper.ConvertToGB(Convert.ToDouble(m.Memory)).ToString("N3") + "GB"; } catch { tMemory.Value = "N/A"; }
            tLastLogin.Value = m.LastLogin.ToString();
            tLastUpdate.Value = m.lup_dt.ToString();

            lblSysInfo.InnerHtml = m.ComputerInfo ?? "No Information Available";
            lblSysInfo.InnerHtml = lblSysInfo.InnerHtml.Replace(Environment.NewLine, "<br />");
            lblOS.InnerHtml = m.OSInfo ?? "No Information Available";
            lblOS.InnerHtml = lblOS.InnerHtml.Replace(Environment.NewLine, "<br />");

            try
            {
                XDocument doc = XDocument.Parse(m.Processes);
                var result = from n in doc.Descendants("Process")
                             select new
                             {                                 
                                 Name = n.Element("Name").Value,
                                 WindowTitle = n.Element("WindowTitle").Value,
                                 MemorySize = n.Element("MemorySize").Value,
                                 ThreadCt = n.Element("ThreadCt").Value,
                                 IsResponding = n.Element("IsResponding").Value
                             };
                rProcs.DataSource = result.ToList();
                rProcs.DataBind();
            } catch{}

            rUser.DataSource = Models.UserProfile.selUserProfilesByMachine(m.PCName);
            rUser.DataBind();

            rActivity.DataSource = Models.EventLog.selEventsByMachine(m.PCName, 100);
            rActivity.DataBind();

            rErrors.DataSource = Models.ErrorLog.selErrorsByMachine(m.PCName);
            rErrors.DataBind();

        }
        private IEnumerable<Models.Machine> LoadMachines(string filter = "")
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Machines"))
                WebAdmin2014.Cache.Insert("Machines", Models.Machine.selMachines(), MinutesTillExpiration: 10);
            IEnumerable<Models.Machine> v = WebAdmin2014.Cache.Get<List<Models.Machine>>("Machines"); //Models.Machine.selMachines(); 
            if (!string.IsNullOrEmpty(filter)) v = v.Where(f => (f.PCName ?? string.Empty).ToLower().Contains(filter.ToLower()) || (f.LSID ?? string.Empty).ToLower().Contains(filter.ToLower()) || (f.LocationTitle ?? string.Empty).ToLower().Contains(filter.ToLower()) || (f.WTSLocation ?? string.Empty).ToLower().Contains(filter.ToLower()) || (f.Description ?? string.Empty).ToLower().Contains(filter.ToLower()) || (f.QLVersion ?? string.Empty).ToLower().Contains(filter.ToLower()) || (f.ICAVersion ?? string.Empty).ToLower().Contains(filter.ToLower())).AsEnumerable();
            if (!string.IsNullOrEmpty(hideSort.Value) || !string.IsNullOrEmpty((string)Session["lastSort"])) v = OrderMachines(v);


            rMachine.DataSource = (from a in v select new { a.PCName, a.LSID, a.LocationTitle, a.QLVersion, a.ICAVersion, a.IEVersion, a.LastLogin }).ToList();
            rMachine.DataBind();
            return v;
        }

        protected void bSave_Click(object sender, EventArgs e)
        {            
            if (id == "-1")
            {
                throw new NotImplementedException(); 
            }
            else
            {
                try
                {
                    Models.Machine m = Models.Machine.selMachineById(id);
                    m.LocationId = Convert.ToInt32(ddLocation.SelectedValue);
                    m.IpAddress = tIpAddress.Text;
                    m.Description = tDesc.Text;
                    m.isExcluded = cbIsExcluded.Checked;
                    Models.Machine.updAdminMachine(m);
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Machine Update Failed!');"); return; }
            }
            //common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Machine Saved Successfully!'); parent.CloseModal();");
            lSuccess.Text = "Machine Saved Successfully!";
            lSuccess.Visible = true;
            Session["lastDirection"] = (string)Session["lastDirection"] == "ASC" ? "DESC" : "ASC"; //keep refresh from toggling sort
            WebAdmin2014.Cache.Clear("Machines");

            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "MachineInventory.aspx");
        }
        
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadMachines(tFilter.Text);
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void tFilter_Click(object sender, EventArgs e)
        {
            LoadMachines(tFilter.Text);
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        private IEnumerable<Models.Machine> OrderMachines(IEnumerable<Models.Machine> uMachines, bool switchDirection = true)
        {
            bool IsDesc = ((string)Session["lastSort"] == hideSort.Value && (string)Session["lastDirection"] != "DESC");
            IsDesc = !switchDirection ? !IsDesc : IsDesc; //flips it back to previous
            switch(hideSort.Value){
                case "PCName":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.PCName) : uMachines.OrderBy(d => d.PCName);
                    break;
                case "LSID":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.LSID) : uMachines.OrderBy(d => d.LSID);
                    break;
                case "LocationTitle":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.LocationTitle) : uMachines.OrderBy(d => d.LocationTitle);
                    break;
                case "ICAVersion":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.ICAVersion) : uMachines.OrderBy(d => d.ICAVersion);
                    break;
                case "QLVersion":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.QLVersion) : uMachines.OrderBy(d => d.QLVersion);
                    break;
                case "IEVersion":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.IEVersion) : uMachines.OrderBy(d => d.IEVersion);
                    break;
                case "LastLogin":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.LastLogin) : uMachines.OrderBy(d => d.LastLogin);
                    break;
                case "AvgLoginLength":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.AvgLoginLength) : uMachines.OrderBy(d => d.AvgLoginLength);
                    break;
                case "AvgRoamLength":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.AvgRoamLength) : uMachines.OrderBy(d => d.AvgRoamLength);
                    break;
                case "AvgEMRLaunchLength":
                    uMachines = IsDesc ? uMachines.OrderByDescending(d => d.AvgEMRLaunchLength) : uMachines.OrderBy(d => d.AvgEMRLaunchLength);
                    break;                
            }
            Session["lastSort"] = hideSort.Value;
            Session["lastDirection"] = IsDesc ? "DESC" : "ASC";
            
            return uMachines.AsEnumerable();
        }
        protected void bFilterSlowRpt_Click(object sender, EventArgs e)
        {
            try
            {
                var v = Models.Machine.selSlowestMachines(Convert.ToInt32(tLoginLower.Text), Convert.ToInt32(tRoamLower.Text), Convert.ToInt32(tLaunchLower.Text)).AsEnumerable();
                if (!string.IsNullOrEmpty(hideSort.Value) || !string.IsNullOrEmpty((string)Session["lastSort"])) v = OrderMachines(v, false);
                rSlowMach.DataSource = v;
                rSlowMach.DataBind();                
            }
            catch { }
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void bFilterSlowRptSort_Click(object sender, EventArgs e)
        {
            try
            {
                var v = Models.Machine.selSlowestMachines(Convert.ToInt32(tLoginLower.Text), Convert.ToInt32(tRoamLower.Text), Convert.ToInt32(tLaunchLower.Text)).AsEnumerable();
                if (!string.IsNullOrEmpty(hideSort.Value) || !string.IsNullOrEmpty((string)Session["lastSort"])) v = OrderMachines(v);
                rSlowMach.DataSource = v;
                rSlowMach.DataBind();
            }
            catch { }
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void lbExport_Click(object sender, EventArgs e)
        {
            GridView gv = new GridView();
            gv.DataSource = LoadMachines(tFilter.Text);
            gv.DataBind();
            common.ExportGridToExcel(Page, gv, "MachineInventoryExport_" + DateTime.Now.ToShortDateString().Replace("/", "-")); 
        }
        protected void lbSlowMachineExport_Click(object sender, EventArgs e)
        {
            GridView gv = new GridView();
            gv.DataSource = Models.Machine.selSlowestMachines(Convert.ToInt32(tLoginLower.Text), Convert.ToInt32(tRoamLower.Text), Convert.ToInt32(tLaunchLower.Text));
            gv.DataBind();
            common.ExportGridToExcel(Page, gv, "MachineInventoryExport_" + DateTime.Now.ToShortDateString().Replace("/", "-"));
        }

        protected void rMachine_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            rMachine.PageIndex = e.NewPageIndex;
        }
    }
}
