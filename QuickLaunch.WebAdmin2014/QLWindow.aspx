﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="QLWindow.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.QLWindow" ClientIDMode="Static" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<div>
    <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
        <asp:View runat="server" ID="vList">
        <a href="#Add" class="right add">Add Window Rule</a>
        <h1>Quick Launch Window Rules</h1><asp:UpdatePanel runat="server" ID="upAJAX" ClientIDMode="Static"><ContentTemplate>
        <table id="rWindow">
        <asp:Repeater runat="server" ID="rQLWindow" EnableViewState="true">
        <HeaderTemplate><tr><th>Action</th><th>Process</th><th>Window Text</th><th>Applied to All?</th></tr></HeaderTemplate>
        <ItemTemplate><tr class="<%#((bool)Eval("IsEnabled")) ? string.Empty : "bgWSmoke"%>" id="<%#Eval("id") %>"><td><%#((string)Eval("Action") == "Include" ? "Close Window" : "Keep Window Open") %></td><td><%#Eval("Process") %></td><td><%#(string.IsNullOrEmpty((string)Eval("WindowText")) ? "All Windows" : Eval("WindowText"))  %></td><td><%#Eval("IsEverywhere") %></td></tr></ItemTemplate>
        </asp:Repeater>
        </table><asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />        
        </ContentTemplate></asp:UpdatePanel>
        <a href="#Add" class="right add">Add Window Rule</a>
        <p class="clear"><b>Note:</b> Imprivata automatically closes profiled applications (by PID) on user switch</p>
        </asp:View>        
        <asp:View runat="server" ID="vDetail">
            <h1><%=(this.id == 0 ? "New Window Rule" : "Updating " + tProcess.Value + " " + tWindowText.Value)%></h1>
            <asp:UpdatePanel runat="server" ID="up1" ClientIDMode="Static"><ContentTemplate>
            <ul id="edit" class="detail">
                <li><span>Action:</span><select runat="server" id="ddAction"><option value="Exclude">Keep Window Open</option><option value="Include">Close Window</option></select> on user switch</li>
                <li><span>Process:</span><input type="text" runat="server" id="tProcess" clientidmode="Static" maxlength="75" /></li>                                
                <li><span>Window Text:</span><input type="text" runat="server" id="tWindowText" clientidmode="Static" maxlength="256" /> OPTIONAL <small>IE proc can be done by url as well.</small></li>                                  
                <li><asp:UpdatePanel runat="server" ID="up2" ClientIDMode="Static" style="width:85%;padding:0; border:none;"><ContentTemplate>  
                <span>Applied Devices:</span><input type="checkbox" runat="server" id="cbIsEverywhere" /> <label for="cbIsEverywhere">Everywhere</label><br />
                <div style="width:85%;<%=cbIsEverywhere.Checked ? "display:none;" : string.Empty %>" id="pDevice" class="m5"><input type="text" runat="server" id="tDevice" clientidmode="Static" maxlength="50" /> <asp:Button runat="server" ID="bAddMach" Text="Add Device" OnClick="bAddMach_Click" CssClass="m5" />
                <ul>
                <asp:Repeater runat="server" ID="rDevices">
                <ItemTemplate>
                    <li><%# Eval("Machine")%> <asp:LinkButton runat="server" ID="lbDeleteMach" ClientIDMode="Predictable" CommandArgument='<%# Eval("id") %>' OnClick="lbDeleteMach_Click" ToolTip="Remove Device"><img src="images/delete.gif" align=right /></asp:LinkButton></li>
                </ItemTemplate>
                </asp:Repeater>       
                <%if (this.rDevices.Items.Count == 0)
                  { %>
                    <p>No Devices</p> <small>Add a device to be applied to</small>
                <% } %>
                </ul></div>
                </ContentTemplate></asp:UpdatePanel>
                </li>                                
                <li><span>Enabled?</span><input type="checkbox" runat="server" id="cbIsEnabled" clientidmode="Static" checked="checked" /></li>              
            </ul>
            </ContentTemplate></asp:UpdatePanel>
            <asp:Button runat="server" ID="bSave" Text="Save Window Rule" OnClick="bSave_Click" CssClass="m5" />
            <%if (this.id > 0) { %>
            <asp:Button runat="server" ID="bDelete" Text="Remove Window Rule" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
            <% } %>
        </asp:View>
    </asp:MultiView>
</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script type="text/javascript">
    $().ready(function () {        
        <%if(mv.ActiveViewIndex == 1){ %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%"); 
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");        
        SetPage();
        <% } else { %>
        SetGrid();
        <% } %>
    });
    function SetGrid(){
        $("table#rWindow tr td").click(function () { if($(this).parent().index() > 0)popModal("QLWindow.aspx?id=" + $(this).parent().attr('id')); });
        $("a[href=#Add]").click(function () { popModal("QLWindow.aspx?id=0"); });
    }
    function SetPage(){
        $("#cbIsEverywhere").click(function () { $("#pDevice").slideToggle(); }); 
    }
</script>
</asp:Content>