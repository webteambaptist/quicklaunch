﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="SystemAlertCode.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.SystemAlertCode" %>
<%@ Register Src="~/controls/ucAlertOpMatrix.ascx" TagPrefix="uc1" TagName="ucOpMatrix" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
<%--    <asp:ScriptManager runat="server" ID="sm" ClientIDMode="Static" ScriptMode=Release LoadScriptsBeforeUI=false EnableCdn=true /><uc:Progress runat=server id="ucProg" />--%>
<div>
    <asp:UpdatePanel runat="server" ID="upMain">
        <ContentTemplate>
    <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
        <asp:View runat="server" ID="vList">
            <asp:LinkButton runat="server" ID="badd1" class="right add" Text="Add Alert Code" OnClick="bAdd_Click"></asp:LinkButton>
            <h1>System Alert Codes</h1>
            <asp:Panel runat="server" ID="upAJAX" ClientIDMode="Static">
                <ContentTemplate>
                    <table id="rAlertCode">
                        <asp:Repeater runat="server" ID="rSysAlertCode" EnableViewState="false" ClientIDMode="Static">
                            <HeaderTemplate>
                                <tr>
                                    <th>Code</th>
                                    <th class="tLeft">Description</th>
                                    <th>Last Updated</th>
                                </tr>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr id="<%#Eval("id") %>">
                                    <td class="bold"><%#Eval("Code") %></td>
                                    <td class="tLeft w50"><%#Eval("Description")%></td>
                                    <td><%#((DateTime)Eval("lup_dt")).ToString() %></td>
                                </tr>
                            </ItemTemplate>
                        </asp:Repeater>
                        <% if (rSysAlertCode.Items.Count == 0)
                           { %>
                        <tr>
                            <td colspan="3">
                                <h2>No alert codes available</h2>
                            </td>
                        </tr>
                        <% } %>
                    </table>
                    <asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />
                </ContentTemplate>
            </asp:Panel>
            <asp:LinkButton runat="server" ID="bAdd2" class="right add" Text="Add Alert Code" OnClick="bAdd_Click"></asp:LinkButton>
        </asp:View>
        <asp:View runat="server" ID="vDetail">
            <h1><%=(this.id == 0 ? "New" : "Update") %> Alert Code</h1>
            <div id="tab-container-alert" class="tab-container">
                <ul class='etabs'>
                    <li class='tab'><a href="#tabMain">Details</a></li>
                    <li class='tab'><a href="#tabMatrix">Op Matrix</a></li>
                </ul>
                <div class="panel-container">
                    <div id="tabMain">
                        <ul id="edit" class="detail">
                            <li><span>Code:</span><input type="text" runat="server" id="tCode" clientidmode="Static" /></li>
                            <li><span>Description:</span><textarea runat="server" id="tDesc" clientidmode="Static"></textarea></li>
                            <li>
                                <asp:Panel runat="server" ID="up1" ClientIDMode="Static" style="width: 85%; padding: 0; border: none;">                                    
                                        <span>Templates:</span><textarea type="text" runat="server" id="tTemplate" style="width: 380px; height: 75px;" />
                                        <asp:Button runat="server" ID="bAddTemplate" Text="Add Template" OnClick="bAddTemplate_Click" CssClass="right" />

                                        <asp:Repeater runat="server" ID="rTemplates">
                                            <HeaderTemplate>
                                                <ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <li><%# Eval("Text")%>
                                                    <asp:LinkButton runat="server" ID="lbDeleteTemp" ClientIDMode="Predictable" CommandArgument='<%# Eval("id") %>' OnClick="lbDeleteTemp_Click" ToolTip="Remove Ip"><img src="images/delete.gif" /></asp:LinkButton></li>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </ul>
                            <%if (this.rTemplates.Items.Count == 0)
                              { %>
                                                <p>No Templates assigned</p>
                                                <% } %>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                </asp:Panel>
                            </li>
                        </ul>
                    </div>
                    <div id="tabMatrix">
                        <ul id="Ul1" class="detail">
                            <li><span>Usable Bubble Color(s):</span><input type="text" runat="server" id="tIconColor" maxlength="50" /></li>
                            <li><span>Usable Font Color(s):</span><input type="text" runat="server" id="tForeColor" maxlength="50" /></li>
                            <li><span>Usable Back Color(s):</span><input type="text" runat="server" id="tBackColor" maxlength="50" /></li>
                            <li><span>Uses Ticker?</span><input type="checkbox" runat="server" id="cbUseTicker" /></li>
                            <li><span>Uses Dialog?</span><input type="checkbox" runat="server" id="cbUseDialog" /></li>
                            <li><span>Uses OS Bubble?</span><input type="checkbox" runat="server" id="cbUseOSBubble" /></li>
                            <li><span>Uses Tray Toast?</span><input type="checkbox" runat="server" id="cbUseTrayToast" /></li>
                        </ul>
                        <br />
                        <h3>Required Settings by Alert Code</h3>
                        <uc:AlertOpMatrix runat="server" ID="opMatrix" />
                        <br />
                    </div>
                </div>
            </div>
            <asp:Button runat="server" ID="bSave" Text="Save Alert Code" OnClick="bSave_Click" CssClass="m5" />
            <%if (this.id > 0)
              { %>
            <asp:Button runat="server" ID="bDelete" Text="Remove Alert Code" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to remove this item?');" />
            <% } %>
        </asp:View>
    </asp:MultiView>
            </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="bAdd1" />
            <asp:AsyncPostBackTrigger ControlID="bAdd2" />
            <asp:AsyncPostBackTrigger ControlID="bSave" />
        </Triggers>
    </asp:UpdatePanel>
</div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
<script type="text/javascript">
    $().ready(function () {        
        //<%if(mv.ActiveViewIndex == 1){ %>
        //$("#nav").hide();
        //$("#mainRight").css("width", "97%"); 
        //$(".page").css("width", "97%");
        //$(".main").css("margin", "0");
        $("#tab-container-alert").easytabs();
        <% } else { %>
        SetGrid();
        <% } %>
    });
    function redirect(url) {
        //console.log(url);
        window.location = url;
    }
    function SetGrid(){
        $("table#rAlertCode tr").click(function () {
            if ($(this).index() > 0) redirect("SystemAlertCode.aspx?id=" + $(this).attr('id'));
        });
        //$("a[href=#Add]").click(function () { popModal("SystemAlertCode.aspx?id=0"); });
    }
</script>
</asp:Content>
