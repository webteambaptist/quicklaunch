﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" Async="true" CodeBehind="Help.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.Help" ClientIDMode="Static" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<link href="css/rte.css" rel="stylesheet" type="text/css" />
<style type="text/css">
    table {width:auto;}
    table tr td {border:0px none transparent;}
    table tr td img {margin:2px 7px 0 10px;}
    ul#ulImgs{padding:3px 3px 5px 3px;}
    ul#ulImgs li{display:inline-block;margin:2px 3px 2px 3px; position:relative;filter:alpha(opacity=80);opacity:0.80; list-style:none;}
    ul#ulImgs li:hover{filter:alpha(opacity=100);opacity:1;}
    ul#ulImgs img.icon{width:140px;z-index:99;}
    ul#ulImgs input.delete{width:17px; position:absolute;top:-6px;right:-6px;z-index:101; cursor:pointer;border:none;}
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
        <asp:MultiView runat="server" ID="mv" ClientIDMode="Static" ActiveViewIndex="0">
            <asp:View runat="server" ID="vList">
                <h1>Quick Launch Help</h1>
                <asp:UpdatePanel runat="server" ID="upAJAX" ClientIDMode="Static">
                    <ContentTemplate>
                        <div>
                            <div runat="server" id="dTopic" style="border: 2px solid #F1F1F1">
                                <br />
                                        <asp:Image runat="server" ImageUrl="images/helpTopic.png" style="padding-left: 50px"/>
                                        <label style="text-indent: 30px">Help Topic: </label>
                                        <asp:DropDownList runat="server" ID="ddTopic" OnSelectedIndexChanged="ddTopic_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                <br />
                                <br />
                                </div>

                                <asp:Panel runat="server" ID="vDetail" Visible="false" Style="border:1px solid #F1F1F1">
                                    <ul id="edit" class="detail">
                                        <li><span>Title:</span><asp:TextBox runat="server" id="tName" clientidmode="Static" /></li>
                                        <li><span>Parent:</span><asp:DropDownList runat="server" ID="ddParent"/></li>
                                        <li><span>Body:</span><asp:TextBox runat="server" id="tBody" TextMode="MultiLine"></asp:TextBox><small>* TIP: To link to another help topic use a regular link with the id set (ex. <%= Server.HtmlEncode("<a href=\"id=5\">Help Topic</a>")%>).</small></li>
                                        <li><span>Open Onload?</span><asp:CheckBox runat="server" id="cbIsOpen" clientidmode="Static" /></li>
                                        <li><span>Topic Selectable?</span><asp:CheckBox runat="server" id="cbIsSelectable" clientidmode="Static"/></li>
                                        <li><span>Order Id:</span><asp:TextBox runat="server" id="tOrderId"  clientidmode="Static" /></li>
                                    </ul>
                                    <asp:Button runat="server" ID="bSave" Text="Save Help Topic" OnClick="bSave_Click" CssClass="m5" />
                                    <asp:Button runat="server" ID="bDelete" Text="Remove Help Topic" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
                                                                     
                                </asp:Panel>
                            <br />
                                <asp:Button runat="server" ID="lbAdd1" CssClass="right m5" style="background:#F0F0F0 url('../images/addIcon.png') no-repeat 7px 7px;padding:7px 9px 8px 22px" EnableViewState="false" OnClick="lbAdd1_Click" Text="Add Help Topic"></asp:Button>
                            
                            </div>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="lbAdd1" />
                        <asp:AsyncPostBackTrigger ControlID="bSave" />
                        <asp:AsyncPostBackTrigger ControlID="bDelete" />
                        <asp:AsyncPostBackTrigger ControlID="ddTopic"/>
                    </Triggers>
                </asp:UpdatePanel>
            </asp:View>
            <asp:View runat="server" ID="vImages">
                <h1>TITO Images</h1>
                <div class="StaticNotify" style="background-color: whitesmoke;" runat="server" enableviewstate="false" id="uploadMsg"></div>
                <ul id="upload">
                    <li>
                        <input type="file" id="f0" runat="server" /></li>
                    <li>
                        <input type="file" id="f1" runat="server" /></li>
                    <li>
                        <input type="file" id="f2" runat="server" /></li>
                    <li>
                        <input type="file" id="f3" runat="server" /></li>
                    <li>
                        <input type="file" id="f4" runat="server" /></li>
                </ul>
                <asp:Button runat="server" ID="bUpload" Text="Upload Images" OnClick="bUpload_Click" CssClass="m5" />
                <div class="StaticNotify bgBeige">
                    <%for (int i = 0; i < baseUrl.Length; i++)
                                                    { %> <%=baseUrl[i] + imageDir + "<br />"%> <%}%>
                </div>
                <ul id="ulImgs">
                    <asp:Repeater runat="server" ID="rImage">
                        <ItemTemplate>
                            <li id="<%#Container.DataItem%>"><a href="<%#baseUrl[0] + imageDir + Container.DataItem%>" target="_blank">
                                <img src="<%#baseUrl[0] + imageDir + Container.DataItem%>" title="<%#Container.DataItem%>" class="icon" /></a><asp:ImageButton runat="server" ID="bImgDelete" CommandArgument='<%#Container.DataItem%>' Text="Delete Images" CssClass="delete" OnClick="iDel_Click" ImageUrl="~/images/delete.gif" OnClientClick="return confirm('Are you sure you want to permanently delete the selected images?');" /></li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </asp:View>
        </asp:MultiView>

    </div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
    <script src="js/jquery-rte.js" type="text/javascript"></script>
    <script src="js/Site.js" type="text/javascript"></script>
    <%--<script type="text/javascript">
        $().ready(function () {
            <%if (mv.ActiveViewIndex == 2)
          { %>
            $("#nav").hide();
            $("#mainRight").css("width", "97%");
            $(".page").css("width", "97%");
            $(".main").css("margin", "0");
            $("#tBody").rte({
                content_css_url: "../css/rte.css",
                media_url: "images/"
            });
            <% }
          else
          { %>

            <% } %>
        });
    </script>--%>
</asp:Content>
