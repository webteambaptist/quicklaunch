﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="Reporting.aspx.cs" ValidateRequest="false" Inherits="QuickLaunch.WebAdmin2014.Reporting" ClientIDMode="Static" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
       <%-- <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
            <asp:View runat="server" ID="vList">--%>
                <%if (IsReportingAdmin) { %><a href="http://bhtmsqinf02v/ReportServer/ReportBuilder/ReportBuilder_3_0_0_0.application" class="right m5">Add Report</a><%} %>
                <asp:LinkButton runat="server" ID="lbBack2" Visible="false" autopostback="true" Text="Back" OnClick="lbBack_Click" Style="font-size: 14px;margin: 4px;text-decoration: underline;float: right"></asp:LinkButton>
                <h1>Reporting <small class="f9"></small></h1>
                <asp:UpdatePanel runat="server" ID="upAJAX" ClientIDMode="Static">
                    <ContentTemplate>
                        <div id="dReporting">                            
                            <asp:Panel runat="server" ID="pReports">
                            <asp:Repeater runat="server" ID="rReporting" EnableViewState="true" OnItemCommand="rReporting_ItemCommand">
                                <HeaderTemplate></HeaderTemplate>
                                <ItemTemplate>
                                    <div class="item" id="<%#Eval("id") %>"><a href='Reporting.aspx?id=<%#Eval("id") %>' title="<%#Eval("SubTitle") %>">
                                        <h3><%#Eval("Title") %></h3>
                                        <img src="images/<%#((string)Eval("Type")) == "Custom" ? "report-product-icon.png" : "report-icon-blue.png"%>" alt="Report" align="left" style="margin: 0 5px 0 0;width: 100px" /></a><%#Eval("Frequency")%> Report<br />
                                        Data: <%#Eval("Type")%><br />
                                        <div class="options">
                                            <%--<input type="button" class="<%# !string.IsNullOrEmpty((string)Eval("EmailRecipients")) ? "email" : "hidden"%>" title="Send to <%#Eval("EmailRecipients")%>" value="Send" />--%>
                                            <asp:Button runat="server" Text="Run" class="run" CommandName="run" CommandArgument='<%#Eval("id") %>' /></div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>

                            <%if (IsReportingAdmin)
                              { %><a href="http://bhtmsqinf02v/ReportServer/ReportBuilder/ReportBuilder_3_0_0_0.application"><div class="item">
                                <h3 class="cG f14">Add Report</h3>
                                <img src="images/add.png" alt="Add Report" style="width: 95px;" align="right" /></div>
                            </a><%} %>
                                </asp:Panel>
                            <asp:Panel runat="server" ID="pReportViewer" style="height:850px" >
                                <rsweb:ReportViewer ID="ReportViewer1" Height="850px" Width="100%" runat="server" ProcessingMode="Remote" ShowBackButton="true" 
                                    ShowExportControls="true" ShowPrintButton="false" ValidateRequestMode="Enabled" 
                                    WaitControlDisplayAfter="100" CssClass="reportViewer" AsyncRendering="true" InternalBorderStyle="None" BorderColor="Transparent"
                                    KeepSessionAlive="true" PageCountMode="Actual" >
                                    <ServerReport ReportServerUrl="http://bhtmsqinf02v/reportserver"></ServerReport>
                                </rsweb:ReportViewer>

                                <asp:LinkButton runat="server" ID="lbBack" autopostback="true" Text="Back" OnClick="lbBack_Click" Style="font-size: 14px;margin: 15px;text-decoration: underline;float: right"></asp:LinkButton>
                                <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt">
                                    <ServerReport ReportServerUrl="http://bhtmsqinf02v/Reports/"></ServerReport>
                                </rsweb:ReportViewer>--%>
                            </asp:Panel>
                        </div>
                        <asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />
                    </ContentTemplate>
                </asp:UpdatePanel>
                <%--<a href="#Add" class="right m5">Add Report</a>--%>
                <%--<p class="clear"><b>Note:</b> Actual scheduled run time depends on how often the scheduler runs</p>--%>
            <%--</asp:View>
            <asp:View runat="server" ID="vDetail">
                <h1><%=(this.id == 0 ? "New Report" : "Report Details") %> [<%=tTitle.Value%>]</h1>
                <div id="tab-container" class="tab-container">
                    <ul class='etabs'>
                        <li class='tab'><a href="#tabInfo">General</a></li>
                        <li class='tab'><a href="#tabContent">Content</a></li>
                        <li class='tab'><a href="#tabSched">Scheduling</a></li>
                        <li class='tab'><a href="#tabHist">Run History</a></li>
                    </ul>
                    <div class="panel-container">
                        <div id="tabInfo">
                            <ul id="edit" class="detail">
                                <li title="Report Title"><span>Title:</span><input type="text" runat="server" id="tTitle" maxlength="250" /></li>
                                <li title="Report Sub Title"><span>Sub-Title:</span><input type="text" runat="server" id="tSubTitle" maxlength="500" /></li>
                                <li title="Report details to better describe its purpose"><span>Notes:</span><textarea runat="server" id="tNotes" /></li>
                                <li><span>Last Run Date:</span><input type="text" runat="server" id="tLastRunDt" disabled="disabled" /></li>
                                <li><span>Last Sent Date:</span><input type="text" runat="server" id="tLastSentDt" disabled="disabled" /></li>
                                <li><span>Date Added:</span><input type="text" runat="server" id="tAddDt" disabled="disabled" /></li>
                                <li><span>Last Update User:</span><input type="text" runat="server" id="tLupUser" disabled="disabled" /></li>
                                <li><span>Last Update DateTime:</span><input type="text" runat="server" id="tLupDT" disabled="disabled" /></li>
                            </ul>
                        </div>
                        <div id="tabContent">
                            <ul class="detail">
                                <li title="Report Type"><span>Data Source:</span><select id="ddType" runat="server"><option value="Custom">Custom SQL</option>
                                    <option value="SlowMachines">Slow Machines</option>
                                    <option value="EventLogMetrics">Event Log Metrics</option>
                                    <option value="InvalidICA">Invalid ICA Machines</option>
                                </select></li>
                                <li id="liSQL" title="SQL to make up a custom report"><span>SQL:</span><textarea runat="server" id="tSql" class="large"></textarea></li>
                                <li title="XML parameters and columns that make up the definition of the report"><span>Definition:</span><textarea runat="server" id="tDefinition" class="large"></textarea></li>
                                <li title="Transpose data. Columns as rows, and rows as columns"><span>Transpose Results?:</span><input type="checkbox" runat="server" id="cbIsTransposed" /></li>
                            </ul>
                        </div>
                        <div id="tabSched">
                            <ul id="Ul1" class="detail">
                                <li title="Report Frequency. The # of times you want the report to be run & sent"><span>Frequency:</span><select id="ddFrequency" runat="server"><option>Ad-Hoc</option>
                                    <option>One Time</option>
                                    <option>Daily</option>
                                    <option>Weekly</option>
                                    <option>Bi-Weekly</option>
                                    <option>Monthly</option>
                                    <option>Quarterly</option>
                                    <option>Bi-Yearly</option>
                                    <option>Yearly</option>
                                </select></li>
                                <li><span>Start Date:</span><input type="text" runat="server" id="tStartDt" class="dp" /></li>
                                <li style="padding-left: 75px;">
                                    <input type="checkbox" runat="server" id="cbIsRunSun" />
                                    <span>Sunday</span><input type="checkbox" runat="server" id="cbIsRunMon" />
                                    <span>Monday</span><input type="checkbox" runat="server" id="cbIsRunTue" />
                                    <span>Tuesday</span><input type="checkbox" runat="server" id="cbIsRunWed" />
                                    <span>Wednesday</span>
                                    <br />
                                    <input type="checkbox" runat="server" id="cbIsRunThur" />
                                    <span>Thursday</span><input type="checkbox" runat="server" id="cbIsRunFri" />
                                    <span>Friday</span><input type="checkbox" runat="server" id="cbIsRunSat" />
                                    <span>Saturday</span></li>
                                <br />
                                <br />
                                <li title="Comma separated email addresses"><span>To Recipients:</span><input type="text" runat="server" id="tRecipients" maxlength="2000" /></li>
                                <li title="Comma separated email addresses"><span>CC Recipients:</span><input type="text" runat="server" id="tCC" maxlength="2000" /></li>
                                <li title="Subject of the email"><span>Subject:</span><input type="text" runat="server" id="tSubject" maxlength="250" /></li>
                                <li title="Text you want to appear in the body of the message"><span>Message Body:</span><textarea runat="server" id="tMsg"></textarea></li>
                                <li title="Attach report as CSV"><span>Attach as CSV?:</span><input type="checkbox" runat="server" id="cbIsExcelAttachment" checked="checked" /></li>
                            </ul>
                        </div>
                        <div id="tabHist">
                            <table id="tHistory" class="f9">
                                <asp:Repeater runat="server" ID="rHistory" EnableViewState="false" ClientIDMode="Static">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>Date/Time</th>
                                            <th>Executor</th>
                                            <th># of rows</th>
                                            <th>Sent?</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="<%#Eval("id") %>">
                                            <td><%#((DateTime)Eval("lup_dt")) %></td>
                                            <td><%#Eval("lup_user")%></td>
                                            <td><%#Eval("ResultCt")%></td>
                                            <td><%#Eval("IsSent")%></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <% if (rHistory.Items.Count == 0)
                                   { %>
                                <tr>
                                    <td colspan="4">
                                        <h2>No History Available</h2>
                                    </td>
                                </tr>
                                <% } %>
                            </table>
                        </div>
                    </div>
                </div>
                <%if (IsReportingAdmin)
                  { %>
                <asp:Button runat="server" ID="bSave" Text="Save Report" OnClick="bSave_Click" CssClass="m5" />
                <%if (this.id > 0)
                  { %>
                <asp:Button runat="server" ID="bDelete" Text="Remove Report" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
                <% }
                  } %>
            </asp:View>
        </asp:MultiView>--%>
    </div>
</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
    <script src="js/jquery-easytabs.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $().ready(function () {
      <%--  <%if (mv.ActiveViewIndex == 1)
          { %>
        $("#nav").hide();
        $("#mainRight").css("width", "97%");
        $(".page").css("width", "97%");
        $(".main").css("margin", "0");
        $("#tab-container").easytabs();
        CheckTypeDisplay();
        $("#ddType").change(function () { CheckTypeDisplay(); GetReportDefinition(); });
        $("#tTitle").change(function () { if ($("#tSubject").val().length == 0) { $("#tSubject").val($(this).val()); } if ($("#tMsg").val().length == 0) { $("#tMsg").val("Report results for the " + $(this).val()); } });
        <%if (!IsReportingAdmin)
          { %>
        $("select,input,textarea").attr("disabled", "disabled");
        $("#liSQL").hide();
        <% } %>
        <% }
          else--%>
          //{ %>
        SetGrid();
        <%--<% } %>--%>
    });
        function SetGrid() {
            $("#dReporting div a[href=#edit]").click(function () { popModal("reporting.aspx?id=" + $(this).parent().attr('id')); });
            //$("#dReporting div input.run").click(function () { RunReport("id=" + $(this).parent().parent().attr('id')); });
            $("#dReporting div input.email").click(function () { if (confirm("Are you sure you want to run and send this report right now?\n\n" + $(this).attr("title") + "\nNOTE: Modify report to change recipients")) { RunReport("send=1&id=" + $(this).parent().parent().attr('id')); } });
            //$("a[href=#Add]").click(function () { popModal("reporting.aspx?id=0"); });
            //$("a[href=#Copy]").click(function () { popModal("QLReport.aspx?id=0&copy=" + $(this).parent().parent().attr("id")); });
        }
        function CheckTypeDisplay() {
            if ($("#ddType :selected").index() == 0) {
                $("#liSQL").show();
            } else {
                $("#liSQL").hide();
            }
        }
        function GetReportDefinition() {
            ajax("GetReportDefinition", "{\"id\":\"" + $("#ddType").val() + "\"}", function (data) {
                $("#tDefinition").val(data.GetReportDefinitionResult);
            });
        }
    </script>
</asp:Content>
