﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace QuickLaunch.WebAdmin2014
{    
    [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class ajax
    {
        [OperationContract]
        [WebGet(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Bare)]
        public List<string> GetAutoSuggest()
        {
            List<string> results = new List<string>();

            return results;            
        }

        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public Models.EventLog.EventChart GetEventLog(string groupBy, string userId, string machine)
        {
            DateTime dtNow = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, 1, 0);
            groupBy = groupBy.ToLower();
            DateTime DataMinimum;
            string sqlGroupBy = "hour";
            if (groupBy == "year"){
                DataMinimum = dtNow.AddDays(-91);
                sqlGroupBy = "week";
            } else if(groupBy == "week") {
                DataMinimum = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)).AddDays(-6);
                sqlGroupBy = "day";
            } else if (groupBy == "month") {
                DataMinimum = (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day)).AddDays(-27); //cant do 30, because of overlapping #'s skew...like in the case of february
                sqlGroupBy = "day";
            } else if(groupBy == "day")
                DataMinimum = dtNow.AddHours(-23);
            else
                return null;

            List<Models.EventLog> el = Models.EventLog.selEventLogsGroupedBy(sqlGroupBy, DataMinimum, userId, machine);
            
            foreach(Models.EventLog eDate in el){
                switch (groupBy)
                {
                    case "month":
                        for (int i = 30; i >= 0; i--)
                            if (dtNow.AddDays(-i).Day == eDate.id)
                                eDate.JSTimeStamp = common.GetJavascriptTimestamp(dtNow.AddDays(-i));
                        break;
                    case "week":
                        for (int i = 7; i >= 0 ; i--)
                            if (dtNow.AddDays(-i).Day == eDate.id)
                                eDate.JSTimeStamp = common.GetJavascriptTimestamp(dtNow.AddDays(-i));
                        break;
                    case "day":
                        for (int i = 24; i >= 0; i--)
                            if (dtNow.AddHours(-i).Hour == eDate.id)
                                eDate.JSTimeStamp = common.GetJavascriptTimestamp(dtNow.AddHours(-i));
                        break;
                    case "year":
                        eDate.JSTimeStamp = eDate.id;
                        break;
                    default:
                        break;
                }
            }

            Models.EventLog.EventChart ec = new Models.EventLog.EventChart();
            var elLogins = el.Where(p => p.EventCode == "LOGIN").ToArray();
            ec.logins.data = new long[elLogins.Count()][];
            for (int i = 0; i < elLogins.Count(); i++)
                ec.logins.data[i] = new long[] { elLogins[i].JSTimeStamp, elLogins[i].GroupCount };

            var elRoams = el.Where(p => p.EventCode == "ROAM").ToArray();
            ec.roams.data = new long[elRoams.Count()][];
            for (int i = 0; i < elRoams.Count(); i++)
                ec.roams.data[i] = new long[] { elRoams[i].JSTimeStamp, elRoams[i].GroupCount };

            var elLaunches = el.Where(p => p.EventCode == "LAUNCH").ToArray();
            ec.launches.data = new long[elLaunches.Count()][];
            for (int i = 0; i < elLaunches.Count(); i++)
                ec.launches.data[i] = new long[] { elLaunches[i].JSTimeStamp, elLaunches[i].GroupCount };

            var elDisconnects = el.Where(p => p.EventCode == "DISCONNECT").ToArray();
            ec.disconnects.data = new long[elDisconnects.Count()][];
            for (int i = 0; i < elDisconnects.Count(); i++)
                ec.disconnects.data[i] = new long[] { elDisconnects[i].JSTimeStamp, elDisconnects[i].GroupCount };

            var elLogoffs = el.Where(p => p.EventCode == "LOGOFF").ToArray();
            ec.logoffs.data = new long[elLogoffs.Count()][];
            for (int i = 0; i < elLogoffs.Count(); i++)
                ec.logoffs.data[i] = new long[] { elLogoffs[i].JSTimeStamp, elLogoffs[i].GroupCount };

            return ec;
        }

        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public List<Models.SystemAlert.EventCalendar> GetSystemAlerts()
        {            
            List<Models.SystemAlert.EventCalendar> ec = new List<Models.SystemAlert.EventCalendar>();
            foreach(Models.SystemAlert sa in Models.SystemAlert.selSystemAlerts())
                ec.Add(new Models.SystemAlert.EventCalendar {  id = sa.id, title = sa.Name, url = string.Format("#edit{0}", sa.id.ToString()), allDay = false, start = sa.StartDt.ToString(), end = sa.EndDt.ToString(), textColor = sa.FontColor, backgroundColor = sa.IsEnabled ? "beige" : "whitesmoke"});            

            return ec;
        }

        [OperationContract]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.Wrapped)]
        public string GetReportDefinition(string id)
        {
            try
            {
                return QuickLaunch.WebAdmin2014.Reporting.hlDefinitions.Find(d => d.Title == id).Value;
            }
            catch
            {
                return QuickLaunch.WebAdmin2014.Reporting.hlDefinitions.Find(d => d.Title == "Custom").Value;
            }            
        }
    }
}
