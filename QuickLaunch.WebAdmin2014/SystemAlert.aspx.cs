﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickLaunch.Shared;

namespace QuickLaunch.WebAdmin2014
{
    public partial class SystemAlert : BasePages.SecureUserPage
    {
        #region Properties
        public static int RequiredRoleId = 3;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected string action { get { return ((string)System.Web.HttpContext.Current.Request.QueryString["act"]) ?? string.Empty; } }
        protected bool IsAlertAdmin { get { return common.user != null ? common.user.Roles.Where(r => r.id == RequiredRoleId).Any() : false; } }
        private static readonly string JSUpdateCalendar = "try{parent.document.getElementById('calendar').innerHTML='';parent.LoadCalendar();}catch(exx){}";
        private static readonly string JSUpdateTable = "try {parent.document.getElementById('bRefresh').click();}catch(ex){}";
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {            
                switch (ddIcon.SelectedValue){
                    case "red":
                        iIcon.ImageUrl = "images/systemalerticons/red-light.gif";
                        iIcon.Visible = true;
                        break;
                    case "orange":
                        iIcon.ImageUrl = "images/systemalerticons/orange-light.gif";
                        iIcon.Visible = true;
                        break;
                    case "yellow":
                        iIcon.ImageUrl = "images/systemalerticons/yellow-light.gif";
                        iIcon.Visible = true;
                        break;
                    case "green":
                        iIcon.ImageUrl = "images/systemalerticons/green-light.gif";
                        iIcon.Visible = true;
                        break;
                    default:
                        iIcon.Visible = false;
                        break;
                }//end switch
            
                if (rHistoryRepeater.Items.Count == 0)
                {
                    rLab.Visible = true;
                }

            if (!IsPostBack)
            {
                if (!common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.ErrorLog.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.MachineInventory.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Location.RequiredRoleId).Any() || !common.user.Roles.Where(r => r.id == QuickLaunch.WebAdmin2014.Reporting.RequiredAdminRoleId).Any())
                {
                    ddBackground.Enabled = false;
                    ddFont.Enabled = false;
                    ddIcon.Enabled = false;
                    ddBackground.Style.Add("-webkit-appearance", "none");
                    ddFont.Style.Add("-webkit-appearance", "none");
                    ddIcon.Style.Add("-webkit-appearance", "none");
                    string str = "This control has been disabled!";
                    nonAdmin.Text = str;
                    nonAdmin2.Text = str;
                    nonAdmin3.Text = str;
                    nonAdmin.Visible = true;
                    nonAdmin2.Visible = true;
                    nonAdmin3.Visible = true;

                }
                //checks to make sure the user is in the correct role to view this page and manipulate some of the functions

                //KnownColor[] colors = (KnownColor[])Enum.GetValues(typeof(KnownColor));
                //ddBgColor.DataSource = colors; ddBgColorDialog.DataSource = colors; ddBgColorTicker.DataSource = colors; ddBgColorToast.DataSource = colors;
                //dFont.DataSource = colors; ddFontDialog.DataSource = colors; ddFontTicker.DataSource = colors; ddFontToast.DataSource = colors;
                //Page.DataBind();

                if (!WebAdmin2014.Cache.Exists("AlertCodes"))
                    WebAdmin2014.Cache.Insert("AlertCodes", Models.SystemAlertCode.selSystemAlertCodes(), 1);

                foreach (Models.SystemAlertCode sa in WebAdmin2014.Cache.Get<List<Models.SystemAlertCode>>("AlertCodes"))
                {
                    ddCode.Items.Add(new ListItem(sa.Code.ToString().ToUpper(), sa.id.ToString()));
                    ddCode.DataBind();
                }
                ddCode.Items.Insert(0, new ListItem("--", "0"));

                ddLocation.DataSource = IsAlertAdmin ? Models.Location.selLocations() : Models.Location.selSystemAlertUserLocationsWithLocId(common.user.id);
                ddLocation.DataBind();
               
                if (id > 0)
                    LoadDetail();
                else if (id == 0)
                { //New Alert
                    mv.ActiveViewIndex = 1;
                    tStartDt.Text = DateTime.Now.ToString();
                    tEndDt.Text = DateTime.Now.AddDays(1).ToString();                    
                    
                } else if (action == "calendar")
                    mv.ActiveViewIndex = 2;
                else
                    LoadAlerts();
            }
            
        }//end Page_Load()

        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;            
            Models.SystemAlert sa = Models.SystemAlert.selSystemAlertById(id);
            if (sa == null) return;
            tName.Text = sa.Name;
            ddCode.SelectedValue = sa.CodeId.ToString();
            if (!IsAlertAdmin && ddCode.SelectedIndex < 1) { ddCode.BackColor = Color.Red; ddCode.Enabled = false; bSave.Visible = false; bDelete.Visible = false; }
            tMsg.Text = sa.Message ?? string.Empty;
            tbApp.Text = sa.Application ?? string.Empty;
            tbAttachmentLink.Text = sa.AttachmentLink ?? string.Empty;
            //ddBackground.SelectedValue = sa.BackColor.ToString().ToLower();
            //ddFont.SelectedValue = sa.FontColor.ToString().ToLower();

            if (sa.FontColor != "" && sa.FontColor != null)
                {
                    string str = sa.FontColor;
                    if (str.Contains("/"))
                    {
                        int ind = str.LastIndexOf("/");
                        ind = str.Length - ind;
                        str = str.Substring(0, ind);
                        ddFont.SelectedValue = str.ToLower();
                    }
                    else
                    {
                        ddFont.SelectedValue = sa.FontColor.ToLower();
                    }
                }//gets the font color of the system alert 

                if (sa.BackColor != "" && sa.BackColor != null)
                {
                    string str = sa.BackColor;
                    if (str.Contains("/"))
                    {
                        int ind = str.LastIndexOf("/");
                        str = str.Substring(0, ind);
                        ddBackground.SelectedValue = str.ToLower();
                    }
                    else if (str == "Dark Orange")
                    {
                        str = "#F88017";
                        ddBackground.SelectedValue = str;
                    }
                     else if (str == "Forest Green")
                    {
                        str = "#4E9258";
                        ddBackground.SelectedValue = str;
                    }
                    else
                    {
                        ddBackground.SelectedValue = sa.BackColor.ToLower();
                    }
                }//gets the background color of the system alert

                if (sa.Icon != "" && sa.Icon != null)
                {
                    string str = sa.Icon;
                    if (str.Contains("/"))
                    {
                        int ind = str.LastIndexOf("/");
                        str = str.Substring(0, ind);
                        ddIcon.SelectedValue = str.ToLower();
                        iIcon.ImageUrl = "images/systemalerticons/" + ddIcon.SelectedValue + "-light.gif";
                        iIcon.Visible = true;
                    }
                    else
                    {
                        ddIcon.SelectedValue = sa.Icon.ToLower();
                        iIcon.ImageUrl = "images/systemalerticons/" + ddIcon.SelectedValue + "-light.gif";
                        iIcon.Visible = true;
                    }
                }//get the color of the icon used for the system alert


            cbIsNetScalerVisible.Checked = sa.IsNetScalerVisible;
            cbIsPCVisible.Checked = sa.IsPCVisible;
            cbIsPhysicianPortalVisible.Checked = sa.IsPhysicianPortalVisible;
            cbIsPFCSVisible.Checked = sa.IsPFCSVisible;
            cbIsBHTHINVisible.Checked = sa.IsBHTHINVisible;
            cbIsIntranetVisible.Checked = sa.IsIntranetVisible;

            cbIsTicker.Checked = sa.IsTicker;
            cbIsDialog.Checked = sa.IsDialog;
            cbIsOSBubble.Checked = sa.IsOSBubble;
            cbIsToast.Checked = sa.IsToast;
           
            cbIsEverywhere.Checked = sa.IsEverywhere;
            cbIsEnabled.Checked = sa.IsEnabled;
            ddFrequency.SelectedValue = sa.Frequency ?? string.Empty;
            tStartDt.Text = sa.StartDt.ToString();
            //try { tDowntime.Text =  sa.DowntimeDt.ToString(); } catch {tDowntime.Text = sa.EndDt.AddHours(-4).ToString();}
            tEndDt.Text = sa.EndDt.ToString();

            List<Models.Location> al = Models.Location.selAlertLocations(id);
            //rLocations.DataSource = al;
            //rLocations.DataBind();

            rHistoryRepeater.DataSource = Models.Audit.selAuditByPKId("SSO_SystemAlert", string.Format("<id={0}>", sa.id.ToString())).Take(500);
            rHistoryRepeater.DataBind();
        }//end loadDetail()

        private void LoadAlerts()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("SystemAlerts"))
                WebAdmin2014.Cache.Insert("SystemAlerts", Models.SystemAlert.selSystemAlerts(),1);
            rSysAlert.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlert>>("SystemAlerts").Where(s => s.EndDt > DateTime.Now);
            rSysAlert.DataBind();
            rSysAlertInactive.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlert>>("SystemAlerts").Where(s => s.EndDt < DateTime.Now);
            rSysAlertInactive.DataBind();

            if (rSysAlert.Items.Count == 0)
            {
                lActive.Visible = true;
            }
            else
            {
                lActive.Visible = false;
            }
            if (rSysAlertInactive.Items.Count == 0)
            {
                lInactive.Visible = true;
            }
            else
            {
                lInactive.Visible = false;
            }
        }

        public int getTableValidation(Table tb)
        {
            int val = 0;
            foreach (var tr in tb.Controls.OfType<TableRow>())
            {
                foreach (var td in tr.Controls.OfType<TableCell>())
                {
                    foreach (var cbx in td.Controls.OfType<CheckBox>())
                    {
                        if (cbx.Checked)
                        {  
                            val++;
                        }//end if
                    }//end foreach
                }//end foreach
            }//end foreach
            return val;
        }//end getTableValidation()

        public int getCheckBoxListValidation(CheckBoxList cl)
        {
            int val = 0;
            foreach (ListItem li in cl.Items)
            {
                        if (li.Selected)
                        {
                            val++;
                        }//end if
                   
            }//end foreach
            return val;
        }//end getTableValidation()

        protected void bSave_Click(object sender, EventArgs e)
        {
            //validate tables before submitting

            int var = getTableValidation(tSysAlertSource);
            if (var > 0)
            {
                sourceVal.IsValid = true;
            }
            else
            {
                sourceVal.IsValid = false;
            }//end validation for tSysAlertSource

            int var2 = getTableValidation(tSystemDisplay);
            if (var2 > 0)
            {
                displayVal.IsValid = true;
            }
            else
            {
                displayVal.IsValid = false;
            }//end validation for tSystemDisplay

            if (!cbIsPCVisible.Checked)
            {
                ddFreqVal.IsValid = true;
            }//end validation for frequency

            if (!cbIsEverywhere.Checked)
            {
                int var3 = getCheckBoxListValidation(ddLocation);
                if (var3 > 0)
                {
                    locVal.IsValid = true;
                }
                else
                {
                    locVal.IsValid = false;
                }
            }else{
                locVal.IsValid = true;
            }//end validation for location

            if (IsValid)
            {
                //when the page has no more validation errors then it may submit new system alert

                Models.Location.AlertLocation dLocation;
                Models.SystemAlert sa = new Models.SystemAlert();
                if (cbIsEverywhere.Checked)
                {
                    sa.IsEverywhere = true;
                }
                else
                {
                    foreach (ListItem li in ddLocation.Items)
                    {
                        if (li.Selected)
                        {
                            dLocation = new Models.Location.AlertLocation { AlertId = id, LocationId = Convert.ToInt32(li.Value) };
                            Models.Location.insAlertLocation(dLocation);
                        }//end if

                    }//end foreach
                }

                sa.id = id;
                sa.Name = tName.Text;
                sa.Message = tMsg.Text;
                sa.Application = tbApp.Text;
                sa.AttachmentLink = tbAttachmentLink.Text;
                sa.Icon = ddIcon.SelectedValue;
                sa.FontColor = ddFont.SelectedValue;
                sa.FontColorDialog = ddFont.SelectedValue;
                
                if(ddCode.SelectedValue == "13"){
                    sa.FontColorTicker = "red";
                } 
                else { 
                    sa.FontColorTicker = ddFont.SelectedValue; 
                }

                sa.FontColorToast = ddFont.SelectedValue;
                sa.BackColor = ddBackground.SelectedValue;
                sa.BackColorDialog = ddBackground.SelectedValue;
                sa.BackColorTicker = ddBackground.SelectedValue;
                sa.BackColorToast = ddBackground.SelectedValue;
                sa.IsNetScalerVisible = cbIsNetScalerVisible.Checked;
                sa.IsPCVisible = cbIsPCVisible.Checked;
                sa.IsPhysicianPortalVisible = cbIsPhysicianPortalVisible.Checked;
                sa.IsPFCSVisible = cbIsPFCSVisible.Checked;
                sa.IsBHTHINVisible = cbIsBHTHINVisible.Checked;
                sa.IsTicker = cbIsTicker.Checked;
                sa.IsDialog = cbIsDialog.Checked;
                sa.IsOSBubble = cbIsOSBubble.Checked;
                sa.IsToast = cbIsToast.Checked;
                sa.Frequency = ddFrequency.SelectedValue;
                sa.IsEnabled = cbIsEnabled.Checked;
                sa.CodeId = Convert.ToInt32(ddCode.SelectedValue);
                
                try { sa.StartDt = Convert.ToDateTime(tStartDt.Text); }
                catch { return; }
                try { sa.EndDt = Convert.ToDateTime(tEndDt.Text); }
                catch { return; }
                //try { sa.DowntimeDt = Convert.ToDateTime(tDowntime.Text); }
                //catch { return; }
                
                sa.lup_user = common.user.ntID;
                sa.lup_dt = DateTime.Now;
                if (id == 0)
                {
                    try
                    {
                        Models.SystemAlert.insSystemAlert(sa);
                    }
                    catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('System Alert Add Failed!');"); return; }
                    tName.Text = string.Empty;
                    tMsg.Text = string.Empty;
                }
                else
                {
                    try
                    {
                        Models.SystemAlert.updSystemAlert(sa);
                    }
                    catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('System Alert Update Failed!');"); return; }
                }
                //common.SendNonAJAX_Script(Page, this.GetType(), "Save", string.Format("{0} {1} parent.notify('Alert Saved Successfully!'); window.close();", JSUpdateTable));
                WebAdmin2014.Cache.Clear("SystemAlerts");

                //bRefresh_Click(sender, e);
                //Response.Write("<script language='javascript' type='text/javascript'>document.close();</javascript>");
                string url = "";
                url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

                Response.Redirect(url + "SystemAlert.aspx");
                

            }
        }//end btnSave_Click()

        protected void ddCode_TextChanged(object sender, EventArgs e)
        {

            if (ddCode.SelectedValue != "" && ddCode.SelectedValue != "0")
            {
                cbIsPCVisible.Checked = false;
                cbIsPhysicianPortalVisible.Checked = false;

                Models.SystemAlertCode sc = Models.SystemAlertCode.selSystemAlertCodeById(int.Parse(ddCode.SelectedValue));
                cbIsTicker.Checked = sc.UseTicker;
                cbIsDialog.Checked = sc.UseDialog;
                cbIsOSBubble.Checked = sc.UseOSBubble;
                cbIsToast.Checked = sc.UseTrayToast;

                if (cbIsDialog.Checked)
                {
                    cbIsPCVisible.Checked = true;
                    cbIsPhysicianPortalVisible.Checked = true;
                }
                if (cbIsOSBubble.Checked || cbIsToast.Checked || cbIsTicker.Checked)
                {
                    cbIsPCVisible.Checked = true;
                }


                if (sc.ForeColor != "" && sc.ForeColor != null)
                {
                    string str = sc.ForeColor;
                    if (str.Contains("/"))
                    {
                        int ind = str.LastIndexOf("/");
                        ind = str.Length - ind;
                        str = str.Substring(0, ind);
                        ddFont.SelectedValue = str.ToLower();
                    }
                    else
                    {
                        ddFont.SelectedValue = sc.ForeColor.ToLower();
                    }
                }//gets the font color in the system alert code template

                if (sc.BackColor != "" && sc.BackColor != null)
                {
                    string str = sc.BackColor;
                    if (str.Contains("/"))
                    {
                        int ind = str.LastIndexOf("/");
                        str = str.Substring(0, ind);
                        ddBackground.SelectedValue = str.ToLower();
                    }
                    else if (str == "Dark Orange")
                    {
                        str = "#F88017";
                        ddBackground.SelectedValue = str;
                    }
                     else if (str == "Forest Green")
                    {
                        str = "#4E9258";
                        ddBackground.SelectedValue = str;
                    }
                    else
                    {
                        ddBackground.SelectedValue = sc.BackColor.ToLower();
                    }
                }//gets the background color of the system alert code template

                if (sc.IconColor != "" && sc.IconColor != null)
                {
                    string str = sc.IconColor;
                    if (str.Contains("/"))
                    {
                        int ind = str.LastIndexOf("/");
                        str = str.Substring(0, ind);
                        ddIcon.SelectedValue = str.ToLower();
                        iIcon.ImageUrl = "images/systemalerticons/" + ddIcon.SelectedValue + "-light.gif";
                        iIcon.Visible = true;
                    }                   
                    else
                    {
                        ddIcon.SelectedValue = sc.IconColor.ToLower();
                        iIcon.ImageUrl = "images/systemalerticons/" + ddIcon.SelectedValue + "-light.gif";
                        iIcon.Visible = true;
                    }
                }//gets the color of the icon in system alert code template

                List<Models.SystemAlertCode.SystemAlertTemplate> st = Models.SystemAlertCode.SystemAlertTemplate.selSysAlertTemplate(int.Parse(ddCode.SelectedValue));
                if (st.Count > 0)
                {
                    lTemplate.Visible = true;
                    ddTemplate.Visible = true;
                    if (ddTemplate.Items.Count > 0)
                    {
                        ddTemplate.Items.Clear();
                    }
                    foreach(Models.SystemAlertCode.SystemAlertTemplate s in st){
                        ddTemplate.Items.Add(new ListItem(s.Text.ToString()));
                    }
                    ddTemplate.Items.Insert(0, new ListItem("--", "0"));
                }
                else{
                    lTemplate.Visible = false;
                    ddTemplate.Visible = false;
                }


            }
        }//end ddCode_TextChanged()

        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadAlerts();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }//end bRefresh_Click()

        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.SystemAlert.delSystemAlert(new  Models.SystemAlert { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", string.Format("{0} {1} parent.notify('System Alert Removed!'); popModal.close();", JSUpdateTable, JSUpdateCalendar));
            
            WebAdmin2014.Cache.Clear("SystemAlerts");
            //bRefresh_Click(sender, e);
            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "SystemAlert.aspx"); ;
        }//end bRemore_Click()

        protected void startDateChanged_TextChanged(object sender, EventArgs e)
        {
            if(tStartDt.Text.Length <= 10){

            var time = DateTime.Now.ToString("HH:mm:ss tt");        
            tStartDt.Text = tStartDt.Text +" " + time.ToString();
            }
        }//end startDateChanged_TextChanged()

        //protected void downtimeDateChanged_TextChanged(object sender, EventArgs e)
        //{
        //    if (tDowntime.Text.Length <= 10)
        //    {
        //        var time = DateTime.Now.ToString("HH:mm:ss tt");
        //        tDowntime.Text = tDowntime.Text + " " + time.ToString();
        //    }
        //}//end downtimeDateChanged_TextChanged

        protected void endDateChanged_TextChanged(object sender, EventArgs e)
        {
            if (tEndDt.Text.Length <= 10)
            {
                var time = DateTime.Now.ToString("HH:mm:ss tt");
                tEndDt.Text = tEndDt.Text + " " + time.ToString();
            }
        }

        protected void ddTemplate_TextChanged(object sender, EventArgs e)
        {
            if (ddTemplate.Text != "")
            {
                if (tMsg.Text == "")
                {
                    tMsg.Text = ddTemplate.Text;
                }
                else
                {
                    tMsg.Text = "";
                    tMsg.Text = ddTemplate.Text;
                }
            }
        }

        protected void bAdd_Click(object sender, EventArgs e)
        {
            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "SystemAlert.aspx?id=0");
        }//end endDateChanged_TextChanged
        
    }//end partial class
}//end