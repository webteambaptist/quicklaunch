﻿<%@ Page Language="C#" MasterPageFile="~/App_Master/Site.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.Default" ClientIDMode="Static" %>

<%@ Register Src="~/controls/ucEventChart.ascx" TagPrefix="uc1" TagName="ucEventChart" %>

<asp:Content ID="h1" runat="server" ContentPlaceHolderID="head">
</asp:Content>
<asp:Content ID="b1" runat="server" ContentPlaceHolderID="body">
<div id="b1Body">
    <div class="moduleBox clear" style="width:780px;">
        <h3>Activity Snapshot</h3>
        <uc1:ucEventChart runat="server" ID="ecActivity" />
    </div>
    <div class="moduleBox left">
        <h3>At a Glance Metrics</h3>
        <ul>
        <li title="Total # of active users using QL"><label>Active User Count:</label> <%=metrics.SumQLUserCt %></li>
        <li title="Users who joined in the last 30 days"><label>New Users Count:</label> <%=metrics.SumNewUsersCt%></li>
        <li title="Time from tap to completion of preparing desktop. Last 90 Days."><label>Avg Login Length:</label> <%=metrics90Day.AvgLoginLength.ToString("N4")%> <small>sec</small></li>
        <%--<li title="Time from tap to completion of preparing desktop"><label>Total Login Length:</label> <%=metrics90Day.SumLoginLength.ToString("N4") %> <small>sec</small></li>--%>
        <li title="Time from EMR click to window display. Last 90 Days."><label>Avg EMR Launch Length:</label> <%=metrics90Day.AvgEMRLaunchLength.ToString("N4")%> <small>sec</small></li>
        <%--<li title="Time from EMR click to window display"><label>Total EMR Launch Length:</label> <%=metrics90Day.SumEMRLaunchLength.ToString("N4")%> <small>sec</small></li>--%>
        <li title="Time from EMR session found to window display. Last 90 Days."><label>Avg Roam Length:</label> <%=metrics90Day.AvgRoamLength.ToString("N4")%> <small>sec</small></li>
        <%--<li title="Time from EMR session found to window display"><label>Total Roam Length:</label> <%=metrics90Day.SumRoamLength.ToString("N4")%> <small>sec</small></li>--%>
        <li title="Difference between avg roam and avg launch. Last 90 Days."><label>Avg Roam/Launch Savings:</label> <%=metrics90Day.AvgRoamLaunchSavings.ToString("N4")%> <small>sec</small></li>
        <li title="Difference between sum roam and sum launch. Last 90 Days."><label>Total Roam/Launch Savings:</label> <%=((metrics90Day.AvgRoamLaunchSavings * metrics.SumQLUserCt) / 60).ToString("N2")%> <small>min</small></li>
        <li title="Last cleared <%=metrics.LastMetricClear%>. Last 90 Days."><label>Roam Count:</label> <%=metrics90Day.SumRoamCt%></li>
        <li title="Last cleared <%=metrics.LastMetricClear%>. Last 90 Days."><label>Power Chart Launch Count:</label> <%=metrics90Day.SumPCLaunchCt%></li>
        <li title="Last cleared <%=metrics.LastMetricClear%>. Last 90 Days."><label>FirstNet Launch Count:</label> <%=metrics90Day.SumFNLaunchCt%></li>
        <li title="Last cleared <%=metrics.LastMetricClear%>. Last 90 Days."><label>SurgiNet Launch Count:</label> <%=metrics90Day.SumSNLaunchCt%></li>
        <li title="# of auto launch users"><label>Auto Launch Count:</label> <%=metrics.SumAutoLaunchCt%> <small>users</small></li>        
        <li title="Only machines using the Quick Launch"><label>QL Machine Count:</label> <%=metrics.SumMachineCt%></li>
        </ul>    
    </div>    
    <div id="dProfile" class="moduleBox right">
        <h3>User Profile</h3>
        <asp:Literal ID="litList" runat="server" />
        <ul>
            <li><span>Username: </span><asp:Label runat="server" id="userID"></asp:Label></li>                
            <li><span>Email:</span> <asp:Label runat="server" id="userID2"></asp:Label>@bmcjax.com</li>                       
        </ul>
    </div>
    <div id="dVersioning" class="moduleBox right">
        <h3>Versioning</h3>
        <table>
        <asp:Repeater runat="server" ID="rVersion" EnableViewState="false" ClientIDMode="Static">
        <HeaderTemplate><tr><th class="tLeft">Title</th><th>Latest Version</th><th>Machine Ct</th></tr></HeaderTemplate>
        <ItemTemplate><tr><td class="tLeft"><%#Eval("Title") %></td><td><%#Eval("LatestVersion")%></td><td><%#Eval("MachineCt")%></td></tr></ItemTemplate>
        </asp:Repeater>
        </table>     
    </div>
    <div id="dQLMenu" class="moduleBox right">
        <h3>Quick Launch Menu</h3>
        <table>
        <asp:Repeater runat="server" ID="rQLMenu" EnableViewState="false" ClientIDMode="Static">
        <HeaderTemplate><tr><th colspan="2">Title</th><th>Action</th><th>Audience</th></tr></HeaderTemplate>
        <ItemTemplate><tr><td><img src="images/menuicons/<%#Eval("img") %>.png" width="25" height="25"></td><td><%#Eval("Name") %></td><td><%#Eval("clickEvent") %></td><td><%#Eval("Audience") %></td></tr></ItemTemplate>
        </asp:Repeater>
        </table>
    </div>
    <div id="dSysAlert" class="moduleBox left">
        <h3>System Alerts</h3>
        <table>
        <asp:Repeater runat="server" ID="rSysAlert" EnableViewState="false" ClientIDMode="Static">
        <HeaderTemplate><tr><th colspan="2">Title</th><th>Start</th><th>End</th></tr></HeaderTemplate>
        <ItemTemplate><tr class="<%# !((bool)Eval("IsEnabled")) ? "bgLGray cDG" : string.Empty %>"><td title="<%#Eval("Code") %>"><img src="images/systemalerticons/<%#Eval("Icon") %>-light.gif" /></td><td><%#Eval("Name") %></td><td class="p3 <%#(((DateTime)Eval("StartDt")) <= DateTime.Now && ((DateTime)Eval("EndDt")) >= DateTime.Now ? "cG o90" : "bgLRed o75") %>"><%#((DateTime)Eval("StartDt")).ToShortDateString() %></td><td class="p3 <%#(((DateTime)Eval("StartDt")) <= DateTime.Now && ((DateTime)Eval("EndDt")) >= DateTime.Now ? "cG o90" : "bgLRed o75") %>"><%#((DateTime)Eval("EndDt")).ToShortDateString() %></td></tr></ItemTemplate>
        </asp:Repeater>
        </table>
    </div>
    <div id="dApps" class="moduleBox left">
        <h3>Top 10 Applications</h3>
        <table>
        <asp:Repeater runat="server" ID="rQLApp" EnableViewState="true">
        <HeaderTemplate><tr><th colspan="2">Title</th><th>Launch Ct</th><th>Last Launch</th></tr></HeaderTemplate>
        <ItemTemplate><tr id="<%#Eval("id") %>"><td><img src="images/menuicons/<%#Eval("DefaultImg") ?? "citrix" %>.png" width="25" height="25"></td><td><%#Eval("Value")%></td><td><%#Eval("LaunchCt")%></td><td><%#Eval("LastLaunch")%></td></tr></ItemTemplate>
        </asp:Repeater>
        </table>
    </div>
<%--    <div id="dMachines" class="moduleBox left">
        <h3>Latest Machine Usage</h3>
        <table>
        <asp:Repeater runat=server ID="rMachine" EnableViewState=false ClientIDMode=Static>
        <HeaderTemplate><tr><th>Machine Name</th><th>LSID</th><th>Last Login</th></tr></HeaderTemplate>
        <ItemTemplate><tr><td><%#Eval("PCName")%></td><td><%#Eval("LSID") %></td><td><%#((DateTime)Eval("LastLogin")) %></td></tr></ItemTemplate>
        </asp:Repeater>
        </table>
    </div>
    <div id="dErrors" class="moduleBox right">
        <h3>Latest Errors</h3>
        <table>
        <asp:Repeater runat=server ID="rError" EnableViewState=false ClientIDMode=Static>
        <HeaderTemplate><tr><th>Serial #</th><th>User Id</th><th>Date/Time</th></tr></HeaderTemplate>
        <ItemTemplate><tr><td><%#Eval("Serial") %></td><td><%#Eval("CurrentUserId")%></td><td><%#((DateTime)Eval("lup_dt")) %></td></tr></ItemTemplate>
        </asp:Repeater>
        </table>
    </div>--%>

</div>
</asp:Content>
<asp:Content ID="cJS" runat="server" ContentPlaceHolderID="js">
<%--<script type="text/javascript" src="~/js/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="~/js/flot/jquery.flot.stack.min.js"></script>
<script type="text/javascript" src="~/js/flot/EventLogSnapshotChart.js"></script>--%>
<script type="text/javascript">
    //function pageLoad() {        
    //    //$("ul#myProjs li span#title").click(function () { alert($(this).parent().attr('id')); window.location = "Default.aspx?id=" + $(this).parent().attr('id'); });        
    //}
    $(function () {
        <% if(IsSystemAlertAdmin){ %>
        notify('<h1>System Alerts Admin</h1><br />', 3000);
        <% } else { %>
        notify('<h1>TITO Web Admin</h1><br />', 3000);
        <% } %>
        $(".moduleBox h3").click(function () { $(this).next().toggle(); });

        RefreshChart();
    });

</script>
</asp:Content>
