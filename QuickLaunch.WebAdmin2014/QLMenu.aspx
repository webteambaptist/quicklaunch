﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="QLMenu.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.QLMenu" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div>
        <asp:UpdatePanel runat="server" ID="upMain">
            <ContentTemplate>
                <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
                    <asp:View runat="server" ID="vList">
                        <asp:LinkButton runat="server" ID="bAdd1" class="right add" Text="Add Menu Item" OnClick="bAdd_Click"/>
                        <h1>Quick Launch Menu</h1>
                        <asp:Panel runat="server" ID="upAJAX" ClientIDMode="Static">
                            <table id="rMenu">
                                <asp:Repeater runat="server" ID="rQLMenu" EnableViewState="true">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>Order</th>
                                            <th colspan="2">Title</th>
                                            <th>Action</th>
                                            <th>Audience</th>
                                            <th>Visible?</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr id="<%#Eval("id") %>" title="<%#Eval("tooltip") %>">
                                            <td>
                                                <input type="text" runat="server" id="tOrderId" value='<%#Eval("orderid") %>' /></td>
                                            <td>
                                                <img src="images/menuicons/<%#Eval("img") %>.png" width="45" height="45"></td>
                                            <td><b><%#Eval("Name") %></b></td>
                                            <td><%#Eval("clickEvent") %></td>
                                            <td><%#Eval("Audience") %></td>
                                            <td><%#Eval("isVisible") %></td>
                                        </tr>
                                        <input type="hidden" runat="server" id="hideId" value='<%#Eval("id") %>' />
                                    </ItemTemplate>
                                </asp:Repeater>
                            </table>
                            <asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" CssClass="hidden" EnableViewState="false" OnClick="bRefresh_Click" />
                            <asp:Button runat="server" ID="bUpdOrder" Text="Update Order" OnClick="bUpdOrder_Click" CssClass="m5" />
                        </asp:Panel>
                        <asp:LinkButton runat="server" ID="bAdd2" class="right add" Text="Add Menu Item" OnClick="bAdd_Click" />
                    </asp:View>
                    <asp:View runat="server" ID="vDetail">
                        <h1><%=(this.id == 0 ? "New" : "Update") %> Menu Item</h1>
                        <asp:Panel runat="server" ID="UpdatePanel1" ClientIDMode="Static">
                            <ul id="edit" class="detail">
                                <li><span>Title:</span><input type="text" runat="server" id="tName" clientidmode="Static" maxlength="50" /></li>
                                <li><span>Tool Tip:</span><textarea runat="server" id="tToolTip" clientidmode="Static"></textarea></li>
                                <li><span>Visible?</span><input type="checkbox" runat="server" id="cbIsVisible" clientidmode="Static" /></li>
                                <li><span>Icon:</span><select runat="server" id="ddIcon"><option value="link">Web Link</option>
                                    <option value="citrix">Citrix</option>
                                    <option value="shield">Power Chart</option>
                                    <option value="ambulance">First Net</option>
                                    <option value="drhead">SurgiNet</option>
                                    <option value="email">E-Mail</option>
                                    <option value="myportal">Portal</option>
                                    <option value="intranet">Intranet</option>
                                    <option value="obtv">OBTV</option>
                                    <option value="appbar">App Bar</option>
                                    <option value="powerchart">Power Chart</option>
                                    <option value="firstnet">FirstNet</option>
                                    <option value="surginet">SurgiNet</option>
                                    <option value="eclipse">Purple Eclipse</option>
                                    <option value="ie">Internet Explorer</option>
                                    <option value="insight">Insight</option>
                                    <option value="optimax">OptiMaxx</option>
                                    <option value="reportreq">Report Request</option>
                                </select>
                                    <img src="images/menuicons/<%=ddIcon.Value %>.png" id="iIcon" width="25" height="25" align="absmiddle"></li>
                                <li><span>Click Event?</span><select runat="server" id="ddAction"><option value="Other">Link</option>
                                    <option value="CITRIX">Citrix</option>
                                    <option value="PC">Power Chart</option>
                                    <option value="FN">First Net</option>
                                    <option value="SN">Surgi Net</option>
                                    <option value="EMAIL">E-Mail</option>
                                    <option value="PORTAL">User Portal</option>
                                    <option value="INTRANET">Intranet</option>
                                </select>&nbsp;<input type="text" runat="server" id="tClickEvent" /></li>
                                <li><span>Audience:</span><select runat="server" id="ddAudience"><option value="ALL">Everyone</option>
                                    <option value="PHY">Physicians Only</option>
                                    <option value="CLI">Clinicians Only</option>
                                </select></li>
                                <li title="Active Directory Group Filter"><span>AD Group Filter:</span><input type="text" runat="server" id="tAdGroupFilter" clientidmode="Static" maxlength="250" />
                                    <small>Group name is case sensitive</small></li>
                                <li><span>Applications:</span><asp:DropDownList runat="server" ID="ddApplication" DataTextField="TitleValueDisplay" DataValueField="id" />
                                    <asp:Button runat="server" ID="bAddApp" Text="Add Application" OnClick="bAddApp_Click" CssClass="m5" />
                                    <ul>
                                        <asp:Repeater runat="server" ID="rApplications">
                                            <ItemTemplate>
                                                <li>
                                                    <input type="text" runat="server" id="cbAppOrder" value='<%# Eval("Order_Id") %>' />
                                                    <%# Eval("TitleValueDisplay")%>
                                                    <asp:LinkButton runat="server" ID="lbDeleteApp" CommandArgument='<%# Eval("id") %>' OnClick="lbDeleteApp_Click" ToolTip="Remove Application"><img src="images/delete.gif" align="right" /></asp:LinkButton></li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </li>
                            </ul>
                        </asp:Panel>
                        <asp:Button runat="server" ID="bSave" Text="Save Menu Item" OnClick="bSave_Click" CssClass="m5" />
                        <%if (this.id > 0)
                  { %>
                        <asp:Button runat="server" ID="bDelete" Text="Remove Menu Item" OnClick="bRemove_Click" CssClass="m5 cR right" OnClientClick="return confirm('Are you sure you want to permanently remove this item?');" />
                        <% } %>
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>

        </asp:UpdatePanel>
    </div>

</asp:Content>
<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
    <script type="text/javascript">
        $().ready(function () {
        //<%if (mv.ActiveViewIndex == 1)
          { %>
        //    $("#nav").hide();
        //    $("#mainRight").css("width", "97%");
        //    $(".page").css("width", "97%");
        //    $(".main").css("margin", "0");
            $("#ddAction").change(SetAction);
            $("#ddIcon").change(function () { $("#iIcon").attr("src", "images/menuicons/" + $(this).val() + ".png"); });
            SetAction();
        <% }
          else
          { %>
            SetGrid();
        <% } %>
        });
        function redirect(url) {
            //console.log(url);
            window.location = url;
        }
        function SetGrid() {
            $("table#rMenu tr td").click(function () {
                if ($(this).parent().index() > 0 && $(this).index() > 0) redirect("QLMenu.aspx?id=" + $(this).parent().attr('id'));
            });
            //$("a[href=#Add]").click(function () { popModal("QLMenu.aspx?id=0"); });

        }
        function SetAction() {
            if ($("#ddAction :selected").index() == 0) { $("#tClickEvent").show(); } else { $("#tClickEvent").hide(); }
        }
    </script>
</asp:Content>
