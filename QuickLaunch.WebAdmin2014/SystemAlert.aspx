﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App_Master/Site.Master" AutoEventWireup="true" CodeBehind="SystemAlert.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.SystemAlert" ClientIDMode="Static" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Src="~/controls/ucAlertOpMatrix.ascx" TagPrefix="uc1" TagName="ucOpMatrix" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
   <link href="css/calendar/calendar.css" rel="stylesheet" type="text/css" />
   <link href="css/calendar/cupertinotheme.css" rel="stylesheet" type="text/css" />
</asp:Content>
     
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
            <div>
                <asp:UpdatePanel runat="server" ID="upAjax">
                    <ContentTemplate>
                        <asp:MultiView runat="server" ID="mv" ClientIDMode="Static">
                            <asp:View runat="server" ID="vList">
                                <asp:LinkButton runat="server" ID="bAdd1" class="right add" Text="Add System Alert" OnClick="bAdd_Click" />
                                <h1>System Alerts</h1>
                                <asp:Panel runat="server" ID="upAJ" ClientIDMode="Static">
                                    <div id="tab-container" class="tab-container">
                                        <ul class='etabs'>
                                            <li class='tab'><a href="#tabActive">Active</a></li>
                                            <li class='tab'><a href="#tabInActive">InActive</a></li>
                                        </ul>
                                        <div class="panel-container">
                                            <div id="tabActive">

                                                <asp:Repeater runat="server" ID="rSysAlert" EnableViewState="false" ClientIDMode="Static">
                                                    <HeaderTemplate>
                                                        <table id="rAlert">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th colspan="1">Title</th>
                                                                    <th>Code</th>
                                                                    <th>Application</th>
                                                                    <th>Start</th>
                                                                    <th>End</th>
                                                                    <th>Response Ct</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr id="<%#Eval("id") %>">
                                                            <td>
                                                                <img src="images/systemalerticons/<%#Eval("Icon") %>-light.gif" /></td>
                                                            <td title="<%#Eval("Message") %>" class="tLeft"><%#Eval("Name") %></td>
                                                            <td title="<%#Eval("CodeDescription") %>"><%#Eval("Code") %></td>
                                                            <td><%#Eval("Application")%></td>
                                                            <td class="p3 <%#(((DateTime)Eval("StartDt")) <= DateTime.Now && ((DateTime)Eval("EndDt")) >= DateTime.Now ? "cG o90" : "bgLRed o75") %>"><%#((DateTime)Eval("StartDt")).ToShortDateString() %></td>
                                                            <td class="p3 <%#(((DateTime)Eval("StartDt")) <= DateTime.Now && ((DateTime)Eval("EndDt")) >= DateTime.Now ? "cG o90" : "bgLRed o75") %>"><%#((DateTime)Eval("EndDt")).ToShortDateString() %></td>
                                                            <td><a href="#"><%#Eval("ResponseCt") %></a></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                            </table>
                                                <asp:Label runat="server" ID="lActive" Visible="false">No active system alerts available.</asp:Label>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Label runat="server" ID="lActive" Visible="false">No active system alerts available.</asp:Label>
                                            </div>

                                            <div id="tabInActive">

                                                <asp:Repeater runat="server" ID="rSysAlertInactive" EnableViewState="false" ClientIDMode="Static">
                                                    <HeaderTemplate>
                                                        <table id="rAlert2">
                                                            <thead>
                                                                <tr>
                                                                    <th></th>
                                                                    <th colspan="1">Title</th>
                                                                    <th>Code</th>
                                                                    <th>Application</th>
                                                                    <th>Start</th>
                                                                    <th>End</th>
                                                                    <th>Response Ct</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr id="<%#Eval("id") %>" class="<%# !((bool)Eval("IsEnabled")) ? "bgWSmoke o90" : string.Empty %>">
                                                            <td>
                                                                <img src="images/systemalerticons/<%#Eval("Icon") %>-light.gif" /></td>
                                                            <td title="<%#Eval("Message") %>" class="tLeft"><%#Eval("Name") %></td>
                                                            <td title="<%#Eval("CodeDescription") %>"><%#Eval("Code") %></td>
                                                            <td><%#Eval("Application")%></td>
                                                            <td class="p3 <%#(((DateTime)Eval("StartDt")) <= DateTime.Now && ((DateTime)Eval("EndDt")) >= DateTime.Now ? "cG o90" : "bgLRed o75") %>"><%#((DateTime)Eval("StartDt")).ToShortDateString() %></td>
                                                            <td class="p3 <%#(((DateTime)Eval("StartDt")) <= DateTime.Now && ((DateTime)Eval("EndDt")) >= DateTime.Now ? "cG o90" : "bgLRed o75") %>"><%#((DateTime)Eval("EndDt")).ToShortDateString() %></td>
                                                            <td><a href="#"><%#Eval("ResponseCt") %></a></td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </tbody>
                                            </table>
                                                
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Label runat="server" ID="lInactive" Visible="false">No inactive system alerts available.</asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                    <asp:LinkButton runat="server" ID="bRefresh" ClientIDMode="Static" class="simplemodal-close" Visible="false" EnableViewState="false" OnClick="bRefresh_Click" />
                                </asp:Panel>

                                <asp:LinkButton runat="server" ID="bAdd2" class="right add" Text="Add System Alert" OnClick="bAdd_Click"></asp:LinkButton>

                            </asp:View>
                            <asp:View runat="server" ID="vDetail">
                                <asp:Panel runat="server" ID="upP">
                                    <h1><%=(this.id == 0 ? "New System Alert" : "Updating " + tName.Text )%></h1>
                                    <div id="tab-container-alert" class="tab-container">
                                        <ul class='etabs'>
                                            <li class='tab'><a href="#tabMain">Main</a></li>
                                            <li class='tab'><a href="#tabHistory">History</a></li>
                                        </ul>
                                        <div class="panel-container">
                                            <div id="tabMain">

                                                <ul id="edit" class="detail">
                                                    <li>
                                                        <span>Title:</span><asp:TextBox runat="server" ID="tName" ClientIDMode="Static" MaxLength="100" ValidationGroup="Save" />
                                                        <asp:RequiredFieldValidator ID="tNameValidator" runat="server"
                                                            ControlToValidate="tName" ValidationGroup="Save" ErrorMessage="Is required."> <span style="color:red">*Cannot Be left blank</span>
                                                        </asp:RequiredFieldValidator>
                                                    </li>

                                                    <li>
                                                        <span>Code:</span>
                                                        <asp:DropDownList runat="server" ID="ddCode" DataTextField="Code" ValidationGroup="Save" AutoPostBack="True" OnTextChanged="ddCode_TextChanged">
                                                        </asp:DropDownList>

                                                        <asp:Label runat="server" ID="lTemplate" Visible="false" Style="width: 50px">Template:</asp:Label>
                                                        <asp:DropDownList runat="server" ID="ddTemplate" AutoPostBack="True"  OnTextChanged="ddTemplate_TextChanged" Visible="false" Style="width: 200px"></asp:DropDownList>

                                                        <asp:RequiredFieldValidator ID="ddCodeValidator" runat="server"
                                                            ControlToValidate="ddCode" ValidationGroup="Save" InitialValue="0" ErrorMessage="Is required."> <span style="color:red">*Please select one</span>
                                                        </asp:RequiredFieldValidator>
                                                    </li>

                                                    <li>
                                                        <span style="float: left">Message:                                       
                                                        </span>
                                                        <asp:TextBox runat="server" ID="tMsg" ClientIDMode="Static" TextMode="MultiLine" ValidationGroup="Save" AutoPostBack="True"></asp:TextBox>
                                                        <br />
                                                        <asp:RequiredFieldValidator ID="msgVal" runat="server"
                                                            ControlToValidate="tMsg" ValidationGroup="Save" ErrorMessage="Is required."> <span style="color:red">*Cannot be left blank</span>
                                                        </asp:RequiredFieldValidator>
                                                    </li>

                                                    <li><span>Application:</span><asp:TextBox runat="server" ID="tbApp" ClientIDMode="Static" MaxLength="250" /></li>
                                                    <li title="Optional"><span>Attachment Link:</span><asp:TextBox runat="server" ID="tbAttachmentLink" ClientIDMode="Static" MaxLength="2048" /></li>

                                                    <li id="liIcon" runat="server">

                                                        <span>Icon:</span>
                                                        <asp:DropDownList runat="server" ID="ddIcon" AutoPostBack="True" ValidationGroup="Save">
                                                            <asp:ListItem Value="white">--</asp:ListItem>
                                                            <asp:ListItem Value="red">Red</asp:ListItem>
                                                            <asp:ListItem Value="orange">Orange</asp:ListItem>
                                                            <asp:ListItem Value="yellow">Yellow</asp:ListItem>
                                                            <asp:ListItem Value="green">Green</asp:ListItem>
                                                            <asp:ListItem Value="blue">Blue</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:Image runat="server" ID="iIcon" Style="vertical-align: middle; text-align: center" />
                                                        <asp:Label runat="server" ID="nonAdmin" Visible="false" Style="width: 200px;"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="dIconValidator" runat="server"
                                                            ControlToValidate="ddIcon" InitialValue="white" ValidationGroup="Save" ErrorMessage="Is required."> <span style="color:red">*Please choose a color</span>
                                                        </asp:RequiredFieldValidator>
                                                    </li>

                                                    <li id="liFontColor" runat="server">
                                                        <span>Font Color:</span>
                                                        <asp:DropDownList runat="server" ID="ddFont" ValidationGroup="Save">
                                                            <asp:ListItem Value="0">--</asp:ListItem>
                                                            <asp:ListItem Value="white">White</asp:ListItem>
                                                            <asp:ListItem Value="black">Black</asp:ListItem>
                                                            <asp:ListItem Value="blue">Blue</asp:ListItem>
                                                            <asp:ListItem Value="green">Green</asp:ListItem>
                                                            <asp:ListItem Value="orange">Orange</asp:ListItem>
                                                            <asp:ListItem Value="red">Red</asp:ListItem>
                                                            <asp:ListItem Value="grey">Grey</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:Label runat="server" ID="nonAdmin2" Visible="false" Style="width: 200px;"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="ddFontVal" runat="server"
                                                            ControlToValidate="ddFont" InitialValue="0" ValidationGroup="Save" ErrorMessage="Is required."> <span style="color:red">*Please choose a Color</span>
                                                        </asp:RequiredFieldValidator>
                                                    </li>

                                                    <li id="liBackColor" runat="server">
                                                        <span>Background Color:</span>
                                                        <asp:DropDownList runat="server" ID="ddBackground" ValidationGroup="Save">
                                                            <asp:ListItem Value="0">--</asp:ListItem>
                                                            <asp:ListItem Value="white">White</asp:ListItem>
                                                            <asp:ListItem Value="black">Black</asp:ListItem>
                                                            <asp:ListItem Value="blue">Blue</asp:ListItem>
                                                            <asp:ListItem Value="green">Green</asp:ListItem>
                                                            <asp:ListItem Value="#4E9258">Forest Green</asp:ListItem>
                                                            <asp:ListItem Value="orange">Orange</asp:ListItem>
                                                            <asp:ListItem Value="#F88017">Dark Orange</asp:ListItem>
                                                            <asp:ListItem Value="red">Red</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <asp:Label runat="server" ID="nonAdmin3" Visible="false" Style="width: 200px;"></asp:Label>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                            ControlToValidate="ddBackground" InitialValue="0" ValidationGroup="Save" ErrorMessage="Is required."> <span style="color:red">*Please choose a Color</span>
                                                        </asp:RequiredFieldValidator>
                                                    </li>

                                                    <li title="When active this designates where the alert should be displayed" style="display: compact; height: 90px">
                                                        <span>Where to show:<br />
                                                            <small>(source)</small><br />
                                                            <asp:CustomValidator ID="sourceVal" runat="server" ErrorMessage="Required Field" ><span style="color:red;display:initial">*Please check at least one</span></asp:CustomValidator>
                                                            <br />
                                                        </span>
                                                        <asp:Table runat="server" Style="width: 500px; float: right" ID="tSysAlertSource">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">Workstations</asp:Label><asp:CheckBox runat="server" ID="cbIsPCVisible" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">Net Scaler</asp:Label><asp:CheckBox runat="server" ID="cbIsNetScalerVisible" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">Physician's Portal</asp:Label><asp:CheckBox runat="server" ID="cbIsPhysicianPortalVisible" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">PFCS Portal</asp:Label><asp:CheckBox runat="server" ID="cbIsPFCSVisible" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">BHTHIN</asp:Label><asp:CheckBox runat="server" ID="cbIsBHTHINVisible" />
                                                                </asp:TableCell>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">Intranet</asp:Label><asp:CheckBox runat="server" ID="cbIsIntranetVisible" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>
                                                    </li>

                                                    <li title="When active this designates how the alert should be displayed" style="display: inline-flex; height: 175px; width: 100%">
                                                        <span>How to show<br />
                                                            <small>(mechanism)</small><br />
                                                            <asp:CustomValidator ID="displayVal" runat="server" ErrorMessage="Required Field"><span style="color:red;display:initial">*Please check at least one</span></asp:CustomValidator>
                                                            <br />
                                                            <a class="OpMatrix">Op Matrix
                                                        <div class="f9" id="divOpMatrix">
                                                            <uc:AlertOpMatrix runat="server" ID="opMatrix" />
                                                        </div>
                                                            </a>
                                                        </span>
                                                        <asp:Table runat="server" Style="float: right; display: compact; width: 75%" ID="tSystemDisplay">
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">Dialog?</asp:Label><asp:CheckBox runat="server" ID="cbIsDialog" />
                                                                </asp:TableCell>
                                                                <asp:TableCell RowSpan="5" Style="width: 100px"><div id="dPreview" style="color:<%=ddFont.SelectedValue%>;">
                                                                <div class="dialog1" style="background-color:<%=ddBackground.SelectedValue%>;<%=!(cbIsDialog.Checked && cbIsPhysicianPortalVisible.Checked) ? "display:none;" : string.Empty %>"><label><img src="images/systemalerticons/<%=ddIcon.SelectedValue %>-light.gif" width="14" height="14"/> Web Dialog</label></div>
                                                                <div class="dialog2" style="background-color:<%=ddBackground.SelectedValue%>;<%=!(cbIsDialog.Checked && cbIsPCVisible.Checked) ? "display:none;" : string.Empty %>"><img src="images/systemalerticons/<%=ddIcon.SelectedValue %>-light.gif" width="11" height="11" class="right"/> <label>Desktop<br />Dialog</label></div>
                                                                <div class="osBubble" style="background-color:<%=ddBackground.SelectedValue%>;<%=!(cbIsOSBubble.Checked && cbIsPCVisible.Checked) ? "display:none;" : string.Empty %>">OS Bubble</div>
                                                                <div class="trayToast" style="background-color:<%=ddBackground.SelectedValue%>;<%=!(cbIsToast.Checked && cbIsPCVisible.Checked) ? "display:none;" : string.Empty %>"><img src="images/systemalerticons/<%=ddIcon.SelectedValue %>-light.gif" width="11" height="11" align="left"/><label>Tray<br />Toast</label></div>
                                                                <marquee style="background-color:<%=ddBackground.SelectedValue%>;<%=!cbIsTicker.Checked ? "display:none;" : string.Empty %>"><img src="images/systemalerticons/<%=ddIcon.SelectedValue %>-light.gif" width="11" height="11"/> Marquee ... Marquee ... Marquee ... Marquee ... Marquee ...</marquee>
                                                                <div class="taskBar"><div class="startBtn">Start</div></div>                                                                
                                                            </div>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">Ticker/Marquee?</asp:Label><asp:CheckBox runat="server" ID="cbIsTicker" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">Tray Toast?</asp:Label><asp:CheckBox runat="server" ID="cbIsToast" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                            <asp:TableRow>
                                                                <asp:TableCell>
                                                                    <asp:Label runat="server">OS Bubble?</asp:Label><asp:CheckBox runat="server" ID="cbIsOSBubble" />
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                        </asp:Table>

                                                    </li>
                                                    <li style="display: inline-block; width: 100%; padding: 0 0px 0 4px; border-left: 1px solid #CCC; zoom: 1; *display: inline;">
                                                        <asp:Panel runat="server" ID="up1" ClientIDMode="Static" Style="width: 100%">
                                                            <div style="display: inline-flex; width: 50%; float: left">
                                                                <span style="float: left">Who to show:<br />
                                                                    <small>(location)</small></span><br />
                                                                <span style="float: right">
                                                                    <asp:CheckBox runat="server" ID="cbIsEverywhere" Checked="true" />
                                                                    <label for="cbIsEverywhere">Everywhere</label></span>
                                                                <br />
                                                            </div>
                                                            <br />
                                                            <div style="width: 85%; <%=cbIsEverywhere.Checked ? "display:none;": string.Empty %>; float: left" id="pLocations" class="m5">
                                                                <asp:Label runat="server" Style="display: initial"><b>Select which Locations to show:</b></asp:Label><br />
                                                                <br />
                                                                <asp:CheckBoxList runat="server" RepeatColumns="3" RepeatDirection="Horizontal" ID="ddLocation" DataTextField="Title" DataValueField="id" />
                                                                <asp:CustomValidator runat="server" ID="locVal" ErrorMessage="Required Field"><span style="color:red;display:initial">*Please check at least one</span></asp:CustomValidator>
                                                            </div>
                                                        </asp:Panel>
                                                    </li>

                                                    <li title="# of times an alert shows. Applies to workstations only at this time" style="<%=!cbIsPCVisible.Checked ? "display:none;": string.Empty %>" id="liFrequency" class="m5">
                                                        <span>
                                                            <asp:Label runat="server" ID="Label1">Frequency:</asp:Label></span>
                                                        <asp:DropDownList runat="server" ID="ddFrequency" CausesValidation="False" ValidationGroup="Save">
                                                            <asp:ListItem Value="0">--</asp:ListItem>
                                                            <asp:ListItem Value="ONCE">One Time</asp:ListItem>
                                                            <asp:ListItem Value="1HR">Once, every hour</asp:ListItem>
                                                            <asp:ListItem Value="2HR">Once, every other hour</asp:ListItem>
                                                            <asp:ListItem Value="4HR">Once, every four hours</asp:ListItem>
                                                            <asp:ListItem Value="1DAY">Once a day</asp:ListItem>
                                                            <asp:ListItem Value="7DAY">Once a week</asp:ListItem>
                                                            <asp:ListItem Value="30DAY">Once a month</asp:ListItem>
                                                        </asp:DropDownList>
                                                        <small>For Workstations Only!</small>
                                                        <asp:RequiredFieldValidator ID="ddFreqVal" runat="server"
                                                            ControlToValidate="ddFrequency" InitialValue="0" ErrorMessage="Is required."> <span style="color:red">*Must pick one</span>
                                                        </asp:RequiredFieldValidator>
                                                    </li>

                                                    <li title="Date/time the alert will start showing. User will not see this date/time">
                                                        <div class="myCalendarW" style="width: 100%" id="cal1 ">
                                                            <span>Active Start Date:</span><asp:TextBox runat="server" ID="tStartDt" AutoPostBack="true" CausesValidation="true" OnTextChanged="startDateChanged_TextChanged" />
                                                            <asp:ImageButton runat="Server" ID="ImageButton1"
                                                                ImageUrl="http://www.asp.net/ajaxlibrary/ajaxcontroltoolkitsamplesite/images/Calendar_scheduleHS.png"
                                                                CausesValidation="False" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender1" CssClass="myCalendar" runat="server"
                                                                TargetControlID="tStartDt" Format="MM/dd/yyyy"
                                                                FirstDayOfWeek="Sunday" ViewStateMode="Enabled"
                                                                DaysModeTitleFormat="MM/dd/yyyy" Enabled="True"
                                                                PopupButtonID="ImageButton1" TodaysDateFormat="MM/dd/yyyy" PopupPosition="Right" ClientIDMode="Static" EnableViewState="True" />

                                                            <asp:RequiredFieldValidator ID="tStartDtValidator" runat="server"
                                                                ControlToValidate="tStartDt" ValidationGroup="Save" ErrorMessage="Is required."> <span style="color:red">*Must select a date</span>
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </li>

                                                    <%--<li title="Date/time that the app is in actual downtime.QL will be disabled at this time">
                                    
                                    <div class="myCalendarW" style="width: 100%" id="cal2">
                                        <span>Active Downtime Date:</span><asp:TextBox runat="server" ID="tDowntime" CausesValidation="true" AutoPostBack="true" OnTextChanged="downtimeDateChanged_TextChanged" />
                                        <asp:ImageButton runat="Server" ID="ImageButton3"
                                            ImageUrl="http://www.asp.net/ajaxlibrary/ajaxcontroltoolkitsamplesite/images/Calendar_scheduleHS.png"
                                            CausesValidation="False" />                                    
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender3" CssClass="myCalendar" runat="server"
                                            TargetControlID="tDowntime" Format="MM/dd/yyyy"
                                            FirstDayOfWeek="Sunday" ViewStateMode="Enabled"
                                            DaysModeTitleFormat="MM/dd/yyyy" Enabled="True"
                                            PopupButtonID="ImageButton3" TodaysDateFormat="MM/dd/yyyy" PopupPosition="Right" ClientIDMode="Static" />                                    
                                        <asp:RequiredFieldValidator ID="tDowntimeValidator" runat="server"
                                        ControlToValidate="tDowntime" ValidationGroup="save" ErrorMessage="Is required."> <span style="color:red">*Must select a date</span>
                                        </asp:RequiredFieldValidator>
                                        </div>
                                </li>--%>

                                                    <li title="Date/time the alert will stop showing. User will not see this date/time">
                                                        <div class="myCalendarW" style="width: 100%" id="cal3">
                                                            <span>Active End Date:</span><asp:TextBox runat="server" ID="tEndDt" CausesValidation="true" AutoPostBack="true" OnTextChanged="endDateChanged_TextChanged" />
                                                            <asp:ImageButton runat="Server" ID="ImageButton2"
                                                                ImageUrl="http://www.asp.net/ajaxlibrary/ajaxcontroltoolkitsamplesite/images/Calendar_scheduleHS.png"
                                                                CausesValidation="False" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" CssClass="myCalendar" runat="server"
                                                                TargetControlID="tEndDt" Format="MM/dd/yyyy"
                                                                FirstDayOfWeek="Sunday" ViewStateMode="Enabled"
                                                                DaysModeTitleFormat="MM/dd/yyyy" Enabled="True"
                                                                PopupButtonID="ImageButton2" TodaysDateFormat="MM/dd/yyyy" PopupPosition="Right" ClientIDMode="Static" />
                                                            <asp:RequiredFieldValidator ID="tEndDtValidator" runat="server"
                                                                ControlToValidate="tEndDt" ValidationGroup="Save" ErrorMessage="Is required."> <span style="color:red">*Must select a date</span>
                                                            </asp:RequiredFieldValidator>
                                                        </div>
                                                    </li>
                                                    <%--<li title="If NOT checked Quick Launch Citrix menu items will be disabled">
                                    <span>Quick Launch Citrix Menu Items Enabled?</span><input type="checkbox" runat="server" id="cbIsQLCitrixEnabled" checked="checked" />&nbsp;&nbsp;<label id="lblQLWarning" />
                                </li>--%>
                                                </ul>
                                            </div>

                                            <div id="tabHistory">
                                                <h3>History</h3>
                                                <asp:Repeater runat="server" ID="rHistoryRepeater" EnableViewState="false" ClientIDMode="Static">
                                                    <HeaderTemplate>
                                                        <table id="rHistory" class="f9">
                                                            <thead>
                                                                <tr>
                                                                    <th>Type</th>
                                                                    <th>FieldName</th>
                                                                    <th>Old Value</th>
                                                                    <th>New Value</th>
                                                                    <th>User</th>
                                                                    <th>Date/Time</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td><%#Eval("TYPE")%></td>
                                                            <td><%#Eval("FieldName")%></td>
                                                            <td><%#Eval("OldValue")%></td>
                                                            <td><%#Eval("NewValue")%></td>
                                                            <td><%#Eval("UserName")%></td>
                                                            <td><%#((DateTime)Eval("UpdateDate"))%></td>
                                                        </tr>
                                                        </tbody>                                   
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table> 
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:Label ID="rLab" runat="server" Visible="false" Font-Size="Medium">There are not recent requests</asp:Label>
                                            </div>

                                        </div>
                                        <p>
                                            <asp:Button runat="server" ID="bSave" Text="Save System Alert" CausesValidation="true" OnClick="bSave_Click" CssClass="m5" AutoPostBack="true" ValidationGroup="Save" /><span><input type="checkbox" runat="server" id="cbIsEnabled" /><asp:Label runat="server" ID="lblEnabled">Enabled?</asp:Label></span>
                                            <%if (this.id > 0)
                                              { %>
                                            <asp:Button runat="server" ID="bDelete" Text="Remove System Alert" OnClick="bRemove_Click" CssClass="m5 cR right" AutoPostBack="true" OnClientClick="return confirm('Are you sure you want to permanently remove this item?'); pageClose();" />
                                            <% } %>
                                        </p>
                                        </div>
                                </asp:Panel>
                            </asp:View>
                        </asp:MultiView>

                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddIcon" />
                        <asp:AsyncPostBackTrigger ControlID="ddCode" />
                        <asp:AsyncPostBackTrigger ControlID="tStartDt" />
                        <%--<asp:AsyncPostBackTrigger ControlID="tDowntime" />--%>
                        <asp:AsyncPostBackTrigger ControlID="tEndDt" />
                        <asp:PostBackTrigger ControlID="bSave" />
                        <asp:PostBackTrigger ControlID="bDelete" />
                        <asp:AsyncPostBackTrigger ControlID="ddTemplate" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
</asp:Content>


<asp:Content ID="cJS" ContentPlaceHolderID="js" runat="server">
    <script type="text/javascript">
        function pageLoad(sender, args) {
        <%if (mv.ActiveViewIndex == 1)
          { %>
            //$("#bRefresh").click(function () { event.preventDefault(); $(document).close(); });
            $("div#tabDisplay select").change(function () { $(this).parent().find("label").css("background-color", $(this).val()); SetPreview(); });
            $("#dPreview2").html($("#dPreview").html());
            SetPage();
            SetPreview();
        <% }
          else
          {%>
        SetGrid();
        <% } %>
    };
        function redirect(url) {
            //console.log(url);
            window.location = url;
        }
        function SetGrid() {
            $("table#rAlert tr td").click(function () {
                if ($(this).index() > -1)
                    redirect("systemalert.aspx?id=" + $(this).parent().attr('id'));
            });
            $("table#rAlert2 tr td").click(function () {
                if ($(this).index() > -1)
                    redirect("systemalert.aspx?id=" + $(this).parent().attr('id'));
            });
            $("#tab-container").easytabs();
            $("#rAlert,#rAlert2").tablesorter();
        }
    </script>
    <script src="js/SystemAlert.js"></script>
</asp:Content>
