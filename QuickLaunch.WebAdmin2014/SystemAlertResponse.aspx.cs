﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace QuickLaunch.WebAdmin2014
{
    public partial class SystemAlertResponse : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 3;
        protected int AlertId { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["aid"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page
                
                if (!WebAdmin2014.Cache.Exists("AlertCodes"))
                    WebAdmin2014.Cache.Insert("AlertCodes", Models.SystemAlertCode.selSystemAlertCodes(), 1);
                ddCode.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlertCode>>("AlertCodes");
                ddCode.DataBind();
                ddCode.Items.Insert(0, new ListItem("Select", "0"));

                if (!WebAdmin2014.Cache.Exists("SystemAlerts"))
                    WebAdmin2014.Cache.Insert("SystemAlerts", Models.SystemAlert.selSystemAlerts(), 1);
                ddAlerts.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlert>>("SystemAlerts");
                ddAlerts.DataBind();
                ddAlerts.Items.Insert(0, new ListItem("Select", "0"));

                if (AlertId > 0)
                {
                    ddAlerts.SelectedValue = AlertId.ToString();
                    if(ddAlerts.SelectedIndex > 0)
                        LoadAlertResponses();
                    else
                        common.SendNonAJAX_Script(Page, this.GetType(), "NoAlertFound", "notify('Alert Not Found!<br />Unable to preselect the alert you specified.');"); 

                }


            }
        }

        private List<Models.SystemAlertResponse> LoadAlertResponses()
        {
            DateTime startDt, endDt;
            if (string.IsNullOrEmpty(tStartDt.Value))
                startDt = new DateTime(1999, 1, 1);
            else
                startDt = Convert.ToDateTime(tStartDt.Value);

            if (string.IsNullOrEmpty(tEndDt.Value))
                endDt = DateTime.Now.AddYears(1);
            else
                endDt = Convert.ToDateTime(tEndDt.Value);
            List<Models.SystemAlertResponse> sarList = Models.SystemAlertResponse.selSystemAlertResponses(startDt, endDt, AlertId: Convert.ToInt32(ddAlerts.SelectedValue), NtId: tUser.Value, CodeId: Convert.ToInt32(ddCode.SelectedValue));
            rSysAlertRes.DataSource = sarList;
            rSysAlertRes.DataBind();
            return sarList;
        }
        protected void bSearch_Click(object sender, EventArgs e)
        {
            LoadAlertResponses();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void lbExport_Click(object sender, EventArgs e)
        {
            GridView gv = new GridView();
            gv.DataSource = LoadAlertResponses();
            gv.DataBind();
            common.ExportGridToExcel(Page, gv, "SysAlertResponseExport_" + DateTime.Now.ToShortDateString().Replace("/", "-"));
        }
        protected void bClear_Click(object sender, EventArgs e)
        {
            Models.SystemAlertResponse.delSystemAlertResponses(LoadAlertResponses());
            LoadAlertResponses();
            WebAdmin2014.Cache.Clear("SystemAlerts"); //clear response ct
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();notify('Responses have been cleared<br />If the alert is active, responses will continue to be received.');");
        }                
    }
}