﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class SystemAlertUser : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 3;
        public static int AlertContributorId = 14;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New Alert User
                    tUsername.Disabled = false;
                    mv.ActiveViewIndex = 1;
                }
                else
                    LoadMain();
            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.UserProfile u = Models.UserProfile.selUserProfileById(id);
            if (u == null) return;
            tUsername.Value = u.ntID;
            tUserType.Value = u.userType;
            tUserTitle.Value = u.userTitle ?? string.Empty;
            tUserDept.Value = u.userDept ?? string.Empty;

            ddLocation.DataSource = Models.Location.selLocations();
            ddLocation.DataBind();
            ddLocation.Items.Insert(0, new ListItem("Select Location", "0"));
                        
            ddCode.DataSource = Models.SystemAlertCode.selSystemAlertCodes();
            ddCode.DataBind();
            ddCode.Items.Insert(0, new ListItem("Select Alert Code", "0"));

            rLocations.DataSource = Models.Location.selSystemAlertUserLocations(id);
            rLocations.DataBind();
            rCodes.DataSource = Models.SystemAlertCode.selSystemAlertUserCodes(id);
            rCodes.DataBind();
        }
        private void LoadMain()
        {
            mv.ActiveViewIndex = 0;
            rAlertUser.DataSource = Models.UserProfile.selUserProfilesInRole(AlertContributorId);
            rAlertUser.DataBind();
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.UserProfile up = new Models.UserProfile();
            up.id = id;
            if (tUsername.Value.Length < 5) { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Invalid User ID. Please check the id before continuing.');"); return; }
            up.ntID = tUsername.Value.ToUpper();
            if (id == 0)
            {
                try
                {
                    up.userType = string.Empty;
                    up.userTitle = string.Empty;
                    up.userDept = string.Empty;
                    up.lastLogin = DateTime.Now;
                    Convert.ToInt32(Models.UserProfile.insUserProfile(up));
                    Models.UserProfile Profile = Models.UserProfile.selUserProfileById(up.ntID);
                    if (Profile != null)
                        Models.UserProfile.insUserProfileRole(Profile.id, AlertContributorId, common.ntID);
                    else
                    {
                        common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('User add failed!');");
                        return;
                    }

                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Alert User Add Failed!');"); return; }
            }
            else
            {
                try
                {
                    if (up.ntID == common.ntID) Session.Remove("IsLogged");
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Alert User Update Failed!');"); return; }
            } 
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Alert User Saved Successfully!'); parent.CloseModal();"); 
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadMain();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.SystemAlertUser.delSystemAlertUserCodeByUser(new Models.SystemAlertUser { UserId = id });
            Models.SystemAlertUser.delSystemAlertUserLocationByUser(new Models.SystemAlertUser { UserId = id });
            Models.UserProfile.delUserRoleId(id, AlertContributorId);
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Alert User Removed!'); parent.CloseModal();");            
            bRefresh_Click(sender, e);
        }
        protected void bAddCode_Click(object sender, EventArgs e)
        {
            if (ddCode.SelectedIndex < 1) return;
            Models.SystemAlertUser dSystemAlertUser = new Models.SystemAlertUser { UserId = id, CodeId = Convert.ToInt32(ddCode.SelectedValue) };
            if (id > 0)
            {
                Models.SystemAlertUser.insSystemAlertUserCode(dSystemAlertUser);
                rCodes.DataSource = Models.SystemAlertCode.selSystemAlertUserCodes(id);
            }
            else
            {
                List<Models.SystemAlertUser> LocCode = Session["saUserCode"] == null ? new List<Models.SystemAlertUser>() : Session["saUserCode"] as List<Models.SystemAlertUser>;
                dSystemAlertUser.id = DateTime.Now.Millisecond;
                dSystemAlertUser.Title = ddCode.SelectedItem.Text;
                LocCode.Add(dSystemAlertUser);
                rCodes.DataSource = LocCode;
                Session["saUserCode"] = LocCode;
            }
            rCodes.DataBind();
            //common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }
        protected void lbDeleteCode_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (id > 0)
            {
                Models.SystemAlertUser.delSystemAlertUserCode(new Models.SystemAlertUser { id = Convert.ToInt32(lb.CommandArgument) });
                rCodes.DataSource = Models.SystemAlertCode.selSystemAlertUserCodes(id);
            }
            else
            {
                List<Models.SystemAlertUser> SystemAlertCode = Session["saUserCode"] == null ? new List<Models.SystemAlertUser>() : Session["saUserCode"] as List<Models.SystemAlertUser>;
                SystemAlertCode.Remove(SystemAlertCode.Find(p => p.id == Convert.ToInt32(lb.CommandArgument)));
                rCodes.DataSource = SystemAlertCode;
                Session["saUserCode"] = SystemAlertCode;
            }

            rCodes.DataBind();
            //common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }

        protected void bAddLoc_Click(object sender, EventArgs e)
        {
            if (ddLocation.SelectedIndex < 1) return;
            Models.SystemAlertUser dLocation = new Models.SystemAlertUser { UserId = id, LocationId = Convert.ToInt32(ddLocation.SelectedValue) };
            if (id > 0)
            {
                Models.SystemAlertUser.insSystemAlertUserLocation(dLocation);
                rLocations.DataSource = Models.Location.selSystemAlertUserLocations(id);
            }
            else
            {
                List<Models.SystemAlertUser> Loc = Session["userLoc"] == null ? new List<Models.SystemAlertUser>() : Session["userLoc"] as List<Models.SystemAlertUser>;
                dLocation.id = DateTime.Now.Millisecond;
                dLocation.Title = ddLocation.SelectedItem.Text;
                Loc.Add(dLocation);
                rLocations.DataSource = Loc;
                Session["userLoc"] = Loc;
            }
            rLocations.DataBind();
            //common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }
        protected void lbDeleteLoc_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (id > 0)
            {
                Models.SystemAlertUser.delSystemAlertUserLocation(new Models.SystemAlertUser { id = Convert.ToInt32(lb.CommandArgument) });
                rLocations.DataSource = Models.Location.selSystemAlertUserLocations(id);
            }
            else
            {
                List<Models.SystemAlertUser> userLoc = Session["userLoc"] == null ? new List<Models.SystemAlertUser>() : Session["userLoc"] as List<Models.SystemAlertUser>;
                userLoc.Remove(userLoc.Find(p => p.id == Convert.ToInt32(lb.CommandArgument)));
                rLocations.DataSource = userLoc;
                Session["userLoc"] = userLoc;
            }

            rLocations.DataBind();
            //common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }  
    }
}