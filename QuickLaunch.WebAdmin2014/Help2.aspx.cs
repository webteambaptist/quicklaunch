﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI.HtmlControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class Help : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 4;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected string action { get { return ((string)System.Web.HttpContext.Current.Request.QueryString["act"]) ?? string.Empty; } }
        protected int parentId { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["parentid"] ?? "-1"); } }
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    tvHelp.SelectedNodeChanged += new EventHandler(tvHelp_SelectedNodeChanged);
        //}
                
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                List<Models.Help> HelpList = GetHelpList();
                ddParent.DataSource = HelpList;
                ddParent.DataBind();
                ddParent.Items.Insert(0, new ListItem("Help","0"));
                if (id > 0)
                    LoadDetail(HelpList);
                //else if (id == 0)
                //{ //New Item                    
                //    LoadAdd();
                //}
                else if (action == "images")
                { //Image Management      
                    mv.ActiveViewIndex = 2;
                    LoadImages();
                }
                else
                    LoadHelp(HelpList);
            }
        }

        private List<Models.Help> GetHelpList()
        {
            if (!WebAdmin2014.Cache.Exists("Help"))
                WebAdmin2014.Cache.Insert("Help", Models.Help.selHelps(), 7);

            return WebAdmin2014.Cache.Get<List<Models.Help>>("Help");
        }
        private void LoadHelp(List<Models.Help> helpList)
        {
            AddHelpChildNodes(tvHelp.Nodes[0], helpList);
            tvHelp.Nodes[0].Expand();
        }

        private void AddHelpChildNodes(TreeNode parent, List<Models.Help> HelpList)
        {
            List<Models.Help> children = HelpList.FindAll(f => f.ParentId == Convert.ToInt32(parent.Value));
            if (children != null)
            {
                foreach (Models.Help h in children)
                {
                    TreeNode tn = new TreeNode(h.Title + " [" + h.id.ToString() + "]", h.id.ToString());                                        
                    tn.Expanded = h.IsOpenOnload;
                    //tn.NavigateUrl = "Javascript:OpenDetail(" + h.id.ToString() + ");";
                    tn.SelectAction = TreeNodeSelectAction.SelectExpand;
                    parent.ChildNodes.Add(tn);
                    AddHelpChildNodes(tn, HelpList);
                }
            }
        }

        //void tvHelp_SelectedNodeChanged(object sender, EventArgs e)
        //{
        //    if(Convert.ToInt32(tvHelp.SelectedNode.Value) > 0)
        //        common.SendAJAX_Script(Page, this.GetType(), "Open", "popModal('help.aspx?id=" + 141 +"');");
        //}
        //private void LoadAdd()
        //{
        //    mv.ActiveViewIndex = 1;            
        //    ddParent.SelectedValue = parentId.ToString();
        //    cbIsSelectable.Checked = true;
        //}
        private void LoadDetail(List<Models.Help> HelpList)
        {
            mv.ActiveViewIndex = 1;
            Models.Help h = Models.Help.selHelpById(id);
            tName.Value = h.Title ?? string.Empty;
            tBody.Value = h.Body ?? string.Empty;
            try { ddParent.SelectedValue = h.ParentId.ToString(); } catch { }
            cbIsOpen.Checked = h.IsOpenOnload;
            cbIsSelectable.Checked = h.IsSelectable;
            tOrderId.Value = h.OrderId.ToString();
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Help h = new Models.Help();
            h.id = id;
            h.Title = tName.Value;
            h.Body = tBody.Value;
            h.IsOpenOnload = cbIsOpen.Checked;
            h.IsSelectable = cbIsSelectable.Checked;
            h.ParentId = Convert.ToInt32(ddParent.SelectedValue);
            try
            {
                h.OrderId = Convert.ToInt32(tOrderId.Value);
            }
            catch { h.OrderId = 100; }
            h.lup_user = common.user.ntID;
            h.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {                    
                    Models.Help.insHelp(h);
                    Models.Help.updReorderHelpByParent(Models.Help.selHelpByParentId(h.ParentId));
                } catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Help topic Add Failed!');"); return; }
            }
            else
            {
                try
                {
                    Models.Help.updHelp(h);
                    Models.Help.updReorderHelpByParent(Models.Help.selHelpByParentId(h.ParentId));
                } catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Help Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Help Topic Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Help");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            string LastSelected = tvHelp.SelectedNode != null ? tvHelp.SelectedNode.ValuePath : "0";
            tvHelp.Nodes[0].ChildNodes.Clear();
            LoadHelp(GetHelpList());
            try
            {
                tvHelp.FindNode(LastSelected).Select();
                tvHelp.SelectedNode.Parent.ExpandAll();
            } catch { }
            //common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void bRemove_Click(object sender, EventArgs e)
        {
            if (id > 0)
            {
                Models.Help.delHelp(new Models.Help { id = id });                
                common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Help Topic Removed!'); parent.CloseModal();");
                WebAdmin2014.Cache.Clear("Help");
            }
            bRefresh_Click(sender, e);
        }
        //protected void lbAdd_Click(object sender, EventArgs e)
        //{            
        //    if(tvHelp.SelectedNode == null)
        //        common.SendAJAX_Script(Page, this.GetType(), "NoNode", "notify('No topic has been selected. Please select a parent topic.');");
        //    else
        //        common.SendAJAX_Script(Page, this.GetType(), "Open", "popModal('Help.aspx?id=0&parentid=" + tvHelp.SelectedNode.Value + "');");
        //}
        protected void lbCollapse_Click(object sender, EventArgs e)
        {
            tvHelp.CollapseAll();
        }
        protected void lbExpand_Click(object sender, EventArgs e)
        {
            tvHelp.ExpandAll();
        }

        #region Image Management
        public string[] _baseUrl;
        public string[] baseUrl { get { if (_baseUrl == null) { _baseUrl = common.BaseUrl; for (int i = 0; i < _baseUrl.Length; i++) _baseUrl[i] = _baseUrl[i].Replace("TITO_Admin", "BHApiHub"); } return _baseUrl; } }
        //                              "http://bhwebapp01.bmcjax.com/BHapiHub/"
        //                            , "http://bhwebapp02.bmcjax.com/BHapiHub/" 
        //                            // "http://devbhwebapp.bmcjax.com/BHapiHub/" 
        //                            // "http://localhost:62218/"
        //                          }; 
        public string imageDir = "Content/images/TITO/";
        private string TITO_AuthorizationGuid = "{1B011A8E-8B87-4A75-87F4-48BFF241BEAD}";
        protected void bUpload_Click(object sender, EventArgs e)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["auth"] = TITO_AuthorizationGuid;
            string sSuccess = string.Empty, url = string.Empty;

            for (int i = 0; i < baseUrl.Length; i++)
            {
                url = baseUrl[i] + "Image/Upload";
                sSuccess += "<b>Upload Server: " + " " + url + "</b>";
                if (f0.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 1: " + HttpUploadFile(url, f0.PostedFile, "file", f0.PostedFile.ContentType, nvc);
                if (f1.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 2: " + HttpUploadFile(url, f1.PostedFile, "file", f1.PostedFile.ContentType, nvc);
                if (f2.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 3: " + HttpUploadFile(url, f2.PostedFile, "file", f2.PostedFile.ContentType, nvc);
                if (f3.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 4: " + HttpUploadFile(url, f3.PostedFile, "file", f3.PostedFile.ContentType, nvc);
                if (f4.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 5: " + HttpUploadFile(url, f4.PostedFile, "file", f4.PostedFile.ContentType, nvc);
                sSuccess += "<br /><br />";
            }
            LoadImages();

            //common.SendNonAJAX_Script(Page, this.GetType(), "upload", "notify('Images Upload Results:<br />" + (string.IsNullOrEmpty(sSuccess) ? "No Files Attached" : sSuccess) + "');");
            uploadMsg.InnerHtml = "Images Upload Results:<br />" + (string.IsNullOrEmpty(sSuccess) ? "No Files Attached" : sSuccess);

        }
        private string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
        public string HttpUploadFile(string url, HttpPostedFile file, string paramName, string contentType, NameValueCollection nvc)
        {   
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

            Stream rs = wr.GetRequestStream();
            string shortname = file.FileName.Substring(file.FileName.LastIndexOf(@"\")+1);
            string localfile = Server.MapPath(string.Format(@"~/App_Data/{0}", shortname));
            file.SaveAs(localfile);
            nvc["name"] = shortname;

            AddNameValueCollectionToForm(ref rs, nvc);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(localfile, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse(); 
                wr = null;
                return "Success";
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                wr = null;
                return "Failed";
            }
        }
        private void AddNameValueCollectionToForm(ref Stream rs, NameValueCollection nvc)
        {
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
        }

        private void LoadImages()
        {
            var wr = WebRequest.Create(baseUrl[0] + "Image");
            wr.ContentType = "charset=utf-8";
            string jsonp = string.Empty;
            try
            {
                var response = (HttpWebResponse)wr.GetResponse();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    jsonp = sr.ReadToEnd();
                }
            } catch { }
            rImage.DataSource = string.IsNullOrEmpty(jsonp) ? null : jsonp.Split(',');
            rImage.DataBind();
        }
        protected void iDel_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = sender as ImageButton;
            if(!ib.CommandArgument.Contains('.')) return;
            try
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc["auth"] = TITO_AuthorizationGuid;
                nvc["id"] = ib.CommandArgument;

                for (int i = 0; i < baseUrl.Length; i++)
                {
                    var wr = HttpWebRequest.Create(baseUrl[i] + string.Format("Image/Delete", ib.CommandArgument));
                    wr.Method = "POST";
                    wr.ContentType = "multipart/form-data; boundary=" + boundary;

                    Stream rs = wr.GetRequestStream();
                    AddNameValueCollectionToForm(ref rs, nvc);

                    var response = (HttpWebResponse)wr.GetResponse();
                }
                uploadMsg.InnerHtml = ib.CommandArgument + " removed";                
                File.Delete(Server.MapPath(string.Format("~/App_Data/{0}", ib.CommandArgument))); //remove from app_data just in case its still floating around
            }
            catch { }
            LoadImages();
        }


        #endregion

    }
}


