﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class QLWindow : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 13;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New Item
                    mv.ActiveViewIndex = 1;
                }
                else
                    LoadWindow();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;
            Models.Window w = Models.Window.selWindowById(id);
            if (w == null) return;
            ddAction.Value = w.Action;
            tProcess.Value = w.Process;
            tWindowText.Value = w.WindowText;            
            cbIsEverywhere.Checked = w.IsEverywhere;          
            cbIsEnabled.Checked = w.IsEnabled;            
            rDevices.DataSource = Models.Window.selWindowDevices(w.id); rDevices.DataBind();
        }
        private void LoadWindow()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Windows"))
                WebAdmin2014.Cache.Insert("Windows", Models.Window.selWindows(), 3);
            rQLWindow.DataSource = WebAdmin2014.Cache.Get<List<Models.Window>>("Windows");
            rQLWindow.DataBind();
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Window w = new Models.Window();
            w.id = id;
            w.Action = ddAction.Value;
            w.Process = tProcess.Value;
            w.WindowText = tWindowText.Value;
            w.IsEverywhere = cbIsEverywhere.Checked;
            w.IsEnabled = cbIsEnabled.Checked;            
            w.lup_user = common.user.ntID;
            w.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    int wId = Convert.ToInt32(Models.Window.insWindow(w));
                    try
                    { //load devices
                        List<Models.Window.WindowDevice> wdList = Session["rDevices"] == null ? new List<Models.Window.WindowDevice>() : Session["rDevices"] as List<Models.Window.WindowDevice>;
                        foreach (Models.Window.WindowDevice wd in wdList)
                        {
                            wd.WindowId = wId;
                            Models.Window.insWindowDevice(wd);
                        }
                        Session.Remove("rDevices");
                    }
                    catch { }
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Window Rule Add Failed!');"); return; }                
            }
            else
            {
                try
                {
                    Models.Window.updWindow(w);                  
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Window Rule Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Window Rule Saved Successfully!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Windows");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadWindow();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.Window.delWindow(new Models.Window { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Window Rule Removed!');parent.CloseModal();");
            WebAdmin2014.Cache.Clear("Windows");
            bRefresh_Click(sender, e);
        }
        protected void bAddMach_Click(object sender, EventArgs e)
        {
            Models.Window.WindowDevice wd = new Models.Window.WindowDevice { WindowId = id, Machine = tDevice.Value.ToUpper() };
            if (id > 0)
            {
                Models.Window.insWindowDevice(wd);
                rDevices.DataSource = Models.Window.selWindowDevices(id);
            }
            else
            {
                List<Models.Window.WindowDevice> wdList = Session["rDevices"] == null ? new List<Models.Window.WindowDevice>() : Session["rDevices"] as List<Models.Window.WindowDevice>;
                wd.id = DateTime.Now.Millisecond;
                wdList.Add(wd);
                rDevices.DataSource = wdList;
                Session["rDevices"] = wdList;
            }
            rDevices.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }
        protected void lbDeleteMach_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (id > 0)
            {
                Models.Window.delWindowDevice(new Models.Window.WindowDevice { id = Convert.ToInt32(lb.CommandArgument) });
                rDevices.DataSource = Models.Window.selWindowDevices(id);
            }
            else
            {
                List<Models.Window.WindowDevice> wdList = Session["rDevices"] == null ? new List<Models.Window.WindowDevice>() : Session["rDevices"] as List<Models.Window.WindowDevice>;
                wdList.Remove(wdList.Find(p => p.id == Convert.ToInt32(lb.CommandArgument)));
                rDevices.DataSource = wdList;
                Session["rDevices"] = wdList;
            }

            rDevices.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }         
    }
}