﻿//function SetGrid() {
//    $("table#rAlert tr td,table#rAlert2 tr td").click(function () { if ($(this).parent().index() > -1 && $(this).index() == 6) popModal("SystemAlertResponse.aspx?aid=" + $(this).parent().attr('id')); else if ($(this).parent().index() > -1 && $(this).index() != 6) popModal("SystemAlert.aspx?id=" + $(this).parent().attr('id')); });
//    $("a[href=#Add]").click(function () { popModal("SystemAlert.aspx?id=0"); });
//    $("#tab-container").easytabs();
//    $("#rAlert,#rAlert2").tablesorter();
//}
function LoadCalendar() {
    ajax("GetSystemAlerts", "", function (data) {
        for (var i = 0; i < data.GetSystemAlertsResult.length; i++) {
            data.GetSystemAlertsResult[i].start = new Date(data.GetSystemAlertsResult[i].start);
            data.GetSystemAlertsResult[i].end = new Date(data.GetSystemAlertsResult[i].end);
        }
        $('#calendar').fullCalendar({
            theme: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay'
            },
            editable: true,
            events: data.GetSystemAlertsResult
            , viewDisplay: function (view) {
                $('a[href*="#edit"]').click(function () {
                    popModal("systemalert.aspx?id=" + $(this).attr("href").replace("#edit", ""));
                });
            }
        });
    });
    SetGrid();
}

function SetPage() {
    //$("#ddCode").change(function () { GetCodeTemplates(); });
    $("#cbIsEverywhere").click(function () { $("#pLocations").slideToggle(); });
    $("#cbIsPCVisible").click(function () { $("#liFrequency").slideToggle(); SetPreview(); });
    $("#cbIsTicker").click(function () { $("marquee").slideToggle(); });
    $("#cbIsToast").click(function () { SetPreview(); });
    $("#cbIsOSBubble").click(function () { SetPreview(); });
    $("#cbIsDialog").click(function () { SetPreview(); });
    $("#tab-container-alert").easytabs();
    $("#rHistory").tablesorter();
    //GetCodeTemplates();
    $(".OpMatrix").click(function () { var div = $(this).find("div"); if (div.html() == "") div.html($("#divOpMatrix").html()); div.slideToggle(); });
}

function SetPreview() {
    if ($("#cbIsToast").is(':checked') && $("#cbIsPCVisible").is(':checked')) $(".trayToast").slideDown(); else $(".trayToast").hide();
    if ($("#cbIsOSBubble").is(':checked') && $("#cbIsPCVisible").is(':checked')) $(".osBubble").fadeIn(); else $(".osBubble").fadeOut();
    if ($("#cbIsDialog").is(':checked') && $("#cbIsPCVisible").is(':checked')) $(".dialog2").show(); else $(".dialog2").hide();
    if ($("#cbIsDialog").is(':checked') && $("#cbIsPhysicianPortalVisible").is(':checked')) $(".dialog1").slideDown(); else $(".dialog1").slideUp();
    $("#dPreview img,#dPreview2 img").attr('src', 'images/systemalerticons/' + $("#ddIcon").val() + '-light.gif');
    $(".trayToast label").css("background-color", $("#ddBgColorToast").val()).css("color", $("#ddFontToast").val());
    $("#dPreview marquee,#dPreview2 marquee").css("background-color", $("#ddBgColorTicker").val()).css("color", $("#ddFontTicker").val());
    $(".dialog2").css("border-color", $("#ddIcon").val());
    $(".dialog2 label").css("background-color", $("#ddBgColorDialog").val()).css("color", $("#ddFontDialog").val())
    $(".dialog1 label").css("background-color", $("#ddBgColor").val()).css("color", $("#ddFont").val());
    
}