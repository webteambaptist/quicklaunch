﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class User : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 2;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected string action { get { return ((string)System.Web.HttpContext.Current.Request.QueryString["act"]) ?? string.Empty; } }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (action == "userexport")
                { //User Export
                    GridView gv = new GridView();
                    List<Models.UserProfile> uProfiles = Models.UserProfile.selUserProfiles();
                    foreach (Models.UserProfile up in uProfiles)
                    {
                        up.lastEMRLaunchLength = up.lastEMRLaunchLength / 1000;
                        up.lastLoginLength = up.lastLoginLength / 1000;
                        up.lastRoamLength = up.lastRoamLength / 1000;
                        up.Launch_Roam_Avg = up.Launch_Roam_Avg / 1000;
                    }
                    gv.DataSource = uProfiles.Take(50);
                    gv.DataBind();
                    common.ExportGridToExcel(Page, gv, "UserListExport_" + DateTime.Now.ToShortDateString().Replace("/", "-"));
                    return;
                }

                if (!WebAdmin2014.Cache.Exists("Roles"))
                    WebAdmin2014.Cache.Insert("Roles", Models.Role.selRoles(), 14);
                cblRoles.DataSource = WebAdmin2014.Cache.Get<List<Models.Role>>("Roles");
                cblRoles.DataBind();

                 if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New User
                    tUsername.Disabled = false;
                    mv.ActiveViewIndex = 1;
                }
                else if (action == "role")
                { //View roles                    
                    rRole.DataSource = WebAdmin2014.Cache.Get<List<Models.Role>>("Roles");
                    rRole.DataBind();
                    mv.ActiveViewIndex = 2;
                }
                else
                {
                    //Models.UserProfile.delExtinctUserProfiles();
                    LoadUsers();
                }

            }
        }

        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;            
            Models.UserProfile u = Models.UserProfile.selUserProfileById(id);
            if (u == null) return;
            tUsername.Value = u.ntID;
            tEmail.Value = u.ntID + "@bmcjax.com";
            tUserType.Value = u.userType;
            tUserTitle.Value = u.userTitle ?? string.Empty;
            tUserDept.Value = u.userDept ?? string.Empty;
            tJoinDT.Value = u.firstLogin.ToString();
            tLastLoginDt.Value = u.lastLogin.ToString();
            tLastLoginLength.Value = (u.lastLoginLength / 1000).ToString();
            tLastLoginMachine.Value = u.lastLoginMachine ?? string.Empty;
            lLocation.InnerHtml = u.WTSLocation ?? string.Empty;
            tLastRoamLength.Value = (u.lastRoamLength / 1000).ToString();
            tLastEMRLaunchLength.Value = (u.lastEMRLaunchLength / 1000).ToString();
            tRoamCt.Value = u.roamingCt.ToString();
            tPCLaunchCt.Value = u.pcLaunchCt.ToString();
            tFirstNetLaunchCt.Value = u.fnLaunchCt.ToString();
            tSurgiNetLaunchCt.Value = u.snLaunchCt.ToString();
            tMetricClear.Value = u.lastMetricCtClear.ToString();
            tLastUpdate.Value = u.lastUpdate.ToString();

            List<Models.Role> roles = Models.UserProfile.selUserProfileRoles(id);
            foreach (ListItem li in cblRoles.Items)
                li.Selected = roles.Where(r => r.id == Convert.ToInt32(li.Value)).Any();

            rActivity.DataSource = Models.EventLog.selEventsByUser(u.ntID, 100);
            rActivity.DataBind();

            rErrors.DataSource = Models.ErrorLog.selErrorsByUser(u.ntID);
            rErrors.DataBind();
        }
        private IEnumerable<Models.UserProfile> LoadUsers(string filter = "")
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("Users"))
                WebAdmin2014.Cache.Insert("Users", Models.UserProfile.selUserProfiles(), MinutesTillExpiration: 10);
            IEnumerable<Models.UserProfile> v = WebAdmin2014.Cache.Get<List<Models.UserProfile>>("Users"); //Cache["Users"] as List<Models.UserProfile>;            
            if (!string.IsNullOrEmpty(filter)) v = v.Where(f => (f.ntID ?? string.Empty).ToLower().Contains(filter.ToLower())).AsEnumerable();

            if (Session["lastSort"] != null)
            {
                if (!string.IsNullOrEmpty(hideSort.Value) || !string.IsNullOrEmpty((string)Session["lastSort"])) v = OrderUsers(v);
            }

            rUser.DataSource = (from a in v select new { a.id, a.ntID, a.userType, a.lastLogin, a.lastEMRLaunchLength, a.lastRoamLength, a.roamingCt, a.RoleCt }).ToList(); 
            rUser.DataBind();
            return v;
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.UserProfile up = new Models.UserProfile();
            up.id = id;
            if (tUsername.Value.Length < 5) { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Invalid User ID. Please check the id before continuing.');"); return; }
            up.ntID = tUsername.Value.ToUpper();
            if (id == 0)
            {
                try
                {
                    up.userType = string.Empty;
                    up.userTitle = string.Empty;
                    up.userDept = string.Empty;
                    up.lastLogin = DateTime.Now;
                    int ProfileId = Convert.ToInt32(Models.UserProfile.insUserProfile(up));
                    if (ProfileId > 0)
                        InsertProfileRoles(ProfileId);
                    else
                    {
                        common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('User already exists!');");                        
                        return;
                    }

                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('User Add Failed!');"); return; }
                tUsername.Value = string.Empty;                
                tEmail.Value = string.Empty;                
            }
            else
            {
                try
                {                    
                    Models.UserProfile.delUserProfileRolesByUser(id);
                    InsertProfileRoles(id);
                    if (up.ntID == common.ntID) Session.Remove("IsLogged");
                }
                catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('User Update Failed!');"); return; }
            }
            //common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('User Saved Successfully!'); parent.CloseModal();");
            Session["lastDirection"] = (string)Session["lastDirection"] == "ASC" ? "DESC" : "ASC"; //keep refresh from toggling sort
            WebAdmin2014.Cache.Clear("Users");

            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "User.aspx");
        }
        private void InsertProfileRoles(int ProfileId)
        {
            foreach (ListItem li in cblRoles.Items)
                if (li.Selected)
                    Models.UserProfile.insUserProfileRole(ProfileId, Convert.ToInt32(li.Value), common.ntID);
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadUsers(tFilter.Text);
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void tFilter_Click(object sender, EventArgs e)
        {
            LoadUsers(tFilter.Text);
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }

        protected void rRole_IDB(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Models.Role r = e.Item.DataItem as Models.Role;
                Repeater rUser = e.Item.FindControl("rUsers") as Repeater;
                rUser.DataSource = Models.UserProfile.selUserProfilesByRoleId(r.id);
                rUser.DataBind();
            }
        }
        private IEnumerable<Models.UserProfile> OrderUsers(IEnumerable<Models.UserProfile> uProfiles){
            bool IsDesc = ((string)Session["lastSort"] == hideSort.Value && (string)Session["lastDirection"] != "DESC");            
            switch(hideSort.Value){
                case "ntID":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.ntID) : uProfiles.OrderBy(d => d.ntID);
                    break;
                case "userType":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.userType) : uProfiles.OrderBy(d => d.userType);
                    break;
                case "lastLoginLength":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.lastLoginLength) : uProfiles.OrderBy(d => d.lastLoginLength);
                    break;
                case "lastEMRLaunchLength":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.lastEMRLaunchLength) : uProfiles.OrderBy(d => d.lastEMRLaunchLength);
                    break;
                case "lastRoamLength":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.lastRoamLength) : uProfiles.OrderBy(d => d.lastRoamLength);
                    break;
                case "roamingCt":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.roamingCt) : uProfiles.OrderBy(d => d.roamingCt);
                    break;
                case "RoleCt":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.RoleCt) : uProfiles.OrderBy(d => d.RoleCt);
                    break;
                case "lastLogin":
                    uProfiles = IsDesc ? uProfiles.OrderByDescending(d => d.lastLogin) : uProfiles.OrderBy(d => d.lastLogin);
                    break;
            }
            Session["lastSort"] = hideSort.Value;
            Session["lastDirection"] = IsDesc ? "DESC" : "ASC";
            return uProfiles.AsEnumerable();
        }

        protected void lbClear_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lb = sender as LinkButton;
                IEnumerable<Models.UserProfile> up = LoadUsers(tFilter.Text);
                switch (lb.CommandArgument)
                {
                    case "lastLoginLength":
                        Models.UserProfile.updClearUserProfileRoamingMetrics(up, LastLoginLength: true);
                        Models.EventLog.updClearEventLogMetrics(up, Logins: true); //90 day history
                        break;
                    case "lastEMRLaunchLength":
                        Models.UserProfile.updClearUserProfileRoamingMetrics(up, LastEMRLaunchLength: true);
                        Models.EventLog.updClearEventLogMetrics(up, Launches: true); //90 day history
                        break;
                    case "lastRoamLength":
                        Models.UserProfile.updClearUserProfileRoamingMetrics(up, LastRoamLength: true);
                        Models.EventLog.updClearEventLogMetrics(up, Roams: true); //90 day history
                        break;
                    case "roamingCt":
                        Models.UserProfile.updClearUserProfileRoamingMetrics(up, Roaming: true);
                        Models.EventLog.updClearEventLogMetrics(up, Roams: true); //90 day history
                        break;
                    default:
                        throw new Exception("Invalid command argument.");                        
                }

                LoadUsers(tFilter.Text);
                common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid(); notify('Clear of selected metrics successful.');");
            }
            catch { common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid(); notify('Metrics clear failed!');"); }

        }

        protected void rUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            rUser.PageIndex = e.NewPageIndex;
        }

        protected void bAdd_Click(object sender, EventArgs e)
        {
            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "User.aspx?id=0");
        }
        
    }
}