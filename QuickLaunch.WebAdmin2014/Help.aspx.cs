﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Collections.Specialized;
using System.Text;
using System.Web.UI.HtmlControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class Help : BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 4;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected string action { get { return ((string)System.Web.HttpContext.Current.Request.QueryString["act"]) ?? string.Empty; } }
        protected int parentId { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["parentid"] ?? "-1"); } }
        //protected void Page_Init(object sender, EventArgs e)
        //{
        //    tvHelp.SelectedNodeChanged += new EventHandler(tvHelp_SelectedNodeChanged);
        //}
                
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
               

                BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (!WebAdmin2014.Cache.Exists("Help"))
                    WebAdmin2014.Cache.Insert("Help", Models.Help.selHelpTopic(), 7);

                List<Models.Help> HelpList = new List<Models.Help>();
                foreach (Models.Help h in Models.Help.selHelps())
                {
                    HelpList.Add(h);
                    ddTopic.Items.Add(new ListItem(h.Title, h.id.ToString()));
                }
                ddTopic.Items.Insert(0, new ListItem("--", "0"));

                List<Models.Help> he = Models.Help.selParentId();
                foreach (Models.Help h in he)
                {
                    Models.Help help = Models.Help.selHelpSubjects(h.ParentId);
                    ddParent.Items.Add(new ListItem(help.Title, help.id.ToString()));
                }//gets the Title and id of each parent node

             if (action == "images")
                { //Image Management      
                    mv.ActiveViewIndex = 1;
                    LoadImages();
                }
            }
        }


        private void LoadDetail(List<Models.Help> HelpList)
        {
            mv.ActiveViewIndex = 1;
            Models.Help h = Models.Help.selHelpById(id);
            tName.Text = h.Title ?? string.Empty;
            tBody.Text = h.Body ?? string.Empty;
            try { ddParent.SelectedValue = h.ParentId.ToString(); } catch { }
            cbIsOpen.Checked = h.IsOpenOnload;
            cbIsSelectable.Checked = h.IsSelectable;
            tOrderId.Text = h.OrderId.ToString();
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.Help h = new Models.Help();
            
            h.Title = tName.Text;
            h.Body = tBody.Text;
            h.IsOpenOnload = cbIsOpen.Checked;
            h.IsSelectable = cbIsSelectable.Checked;
            h.ParentId = int.Parse(ddParent.SelectedValue);
            try
            {
                h.OrderId = int.Parse(tOrderId.Text);
            }
            catch { h.OrderId = 100; }
            h.lup_user = common.user.ntID;
            h.lup_dt = DateTime.Now;
            if (int.Parse(ddTopic.SelectedValue) == 0)
            {
                try
                {                    
                    Models.Help.insHelp(h);
                    Models.Help.updReorderHelpByParent(Models.Help.selHelpByParentId(h.ParentId));
                } catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Help topic Add Failed!');"); return; }
            }
            else
            {
                try
                {
                    h.id = int.Parse(ddTopic.SelectedValue);
                    Models.Help.updHelp(h);
                    Models.Help.updReorderHelpByParent(Models.Help.selHelpByParentId(h.ParentId));
                } catch (Exception ex) { Models.ErrorLog.AddErrorToLog(ex, common.ntID, Global.ApplicationName, false); common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Help Update Failed!');"); return; }
            }
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "return confirm('Item has been successfully saved!');");
            Response.BufferOutput = true;

            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "help.aspx");
            
        }
        //protected void bRefresh_Click(object sender, EventArgs e)
        //{
        //    string LastSelected = tvHelp.SelectedNode != null ? tvHelp.SelectedNode.ValuePath : "0";
        //    tvHelp.Nodes[0].ChildNodes.Clear();
        //    LoadHelp(GetHelpList());
        //    try
        //    {
        //        tvHelp.FindNode(LastSelected).Select();
        //        tvHelp.SelectedNode.Parent.ExpandAll();
        //    } catch { }
        //    //common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        //}

        protected void bRemove_Click(object sender, EventArgs e)
        {
            if (int.Parse(ddTopic.SelectedValue) != 0)
            {
                Models.Help.delHelp(new Models.Help { id = int.Parse(ddTopic.SelectedValue) });
                common.SendNonAJAX_Script(Page, this.GetType(), "Save", "return confirm('Item has been successfully removed!');");
                WebAdmin2014.Cache.Clear("Help");
                Response.BufferOutput = true;

                string url = "";
                url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

                Response.Redirect(url + "help.aspx");
            }
        }

        protected void lbAdd1_Click(object sender, EventArgs e)
        {
            vDetail.Visible = true;
            dTopic.Visible = false;
            ddTopic.SelectedValue = "0";
            tName.Text = "";
            ddParent.SelectedValue = "0";
            tBody.Text = "";
            cbIsOpen.Checked = false;
            cbIsSelectable.Checked = false;
            tOrderId.Text = "99";
            bDelete.Visible = false;
            lbAdd1.Visible = false;
        }

        protected void ddTopic_SelectedIndexChanged(object sender, EventArgs e)
        {
            vDetail.Visible = true;
            if (ddTopic.SelectedValue != "" && ddTopic.SelectedValue != "0")
            {
                Models.Help he = Models.Help.selHelpSubjects(int.Parse(ddTopic.SelectedValue));
                tName.Text = he.Title;
                ddParent.SelectedValue = he.ParentId.ToString();
                tBody.Text = he.Body;
                cbIsOpen.Checked = he.IsOpenOnload;
                cbIsSelectable.Checked = he.IsSelectable;
                tOrderId.Text = he.OrderId.ToString();
            }
        }
        
        #region Image Management
        public string[] _baseUrl;
        public string[] baseUrl { get { if (_baseUrl == null) { _baseUrl = common.BaseUrl; for (int i = 0; i < _baseUrl.Length; i++) _baseUrl[i] = _baseUrl[i].Replace("TITO_Admin", "BHApiHub"); } return _baseUrl; } }
        //                              "http://bhwebapp01.bmcjax.com/BHapiHub/"
        //                            , "http://bhwebapp02.bmcjax.com/BHapiHub/" 
        //                            // "http://devbhwebapp.bmcjax.com/BHapiHub/" 
        //                            // "http://localhost:62218/"
        //                          }; 
        public string imageDir = "Content/images/TITO/";
        private string TITO_AuthorizationGuid = "{1B011A8E-8B87-4A75-87F4-48BFF241BEAD}";
        protected void bUpload_Click(object sender, EventArgs e)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc["auth"] = TITO_AuthorizationGuid;
            string sSuccess = string.Empty, url = string.Empty;

            for (int i = 0; i < baseUrl.Length; i++)
            {
                url = baseUrl[i] + "Image/Upload";
                sSuccess += "<b>Upload Server: " + " " + url + "</b>";
                if (f0.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 1: " + HttpUploadFile(url, f0.PostedFile, "file", f0.PostedFile.ContentType, nvc);
                if (f1.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 2: " + HttpUploadFile(url, f1.PostedFile, "file", f1.PostedFile.ContentType, nvc);
                if (f2.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 3: " + HttpUploadFile(url, f2.PostedFile, "file", f2.PostedFile.ContentType, nvc);
                if (f3.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 4: " + HttpUploadFile(url, f3.PostedFile, "file", f3.PostedFile.ContentType, nvc);
                if (f4.PostedFile.ContentLength > 0)
                    sSuccess += "<br />File 5: " + HttpUploadFile(url, f4.PostedFile, "file", f4.PostedFile.ContentType, nvc);
                sSuccess += "<br /><br />";
            }
            LoadImages();

            //common.SendNonAJAX_Script(Page, this.GetType(), "upload", "notify('Images Upload Results:<br />" + (string.IsNullOrEmpty(sSuccess) ? "No Files Attached" : sSuccess) + "');");
            uploadMsg.InnerHtml = "Images Upload Results:<br />" + (string.IsNullOrEmpty(sSuccess) ? "No Files Attached" : sSuccess);

        }
        private string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
        public string HttpUploadFile(string url, HttpPostedFile file, string paramName, string contentType, NameValueCollection nvc)
        {   
            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

            Stream rs = wr.GetRequestStream();
            string shortname = file.FileName.Substring(file.FileName.LastIndexOf(@"\")+1);
            string localfile = Server.MapPath(string.Format(@"~/App_Data/{0}", shortname));
            file.SaveAs(localfile);
            nvc["name"] = shortname;

            AddNameValueCollectionToForm(ref rs, nvc);

            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);

            FileStream fileStream = new FileStream(localfile, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();

            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();

            WebResponse wresp = null;
            try
            {
                wresp = wr.GetResponse(); 
                wr = null;
                return "Success";
            }
            catch (Exception ex)
            {
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                wr = null;
                return "Failed";
            }
        }
        private void AddNameValueCollectionToForm(ref Stream rs, NameValueCollection nvc)
        {
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
        }

        private void LoadImages()
        {
            var wr = WebRequest.Create(baseUrl[0] + "Image");
            wr.ContentType = "charset=utf-8";
            string jsonp = string.Empty;
            try
            {
                var response = (HttpWebResponse)wr.GetResponse();
                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    jsonp = sr.ReadToEnd();
                }
            } catch { }
            rImage.DataSource = string.IsNullOrEmpty(jsonp) ? null : jsonp.Split(',');
            rImage.DataBind();
        }
        protected void iDel_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton ib = sender as ImageButton;
            if(!ib.CommandArgument.Contains('.')) return;
            try
            {
                NameValueCollection nvc = new NameValueCollection();
                nvc["auth"] = TITO_AuthorizationGuid;
                nvc["id"] = ib.CommandArgument;

                for (int i = 0; i < baseUrl.Length; i++)
                {
                    var wr = HttpWebRequest.Create(baseUrl[i] + string.Format("Image/Delete", ib.CommandArgument));
                    wr.Method = "POST";
                    wr.ContentType = "multipart/form-data; boundary=" + boundary;

                    Stream rs = wr.GetRequestStream();
                    AddNameValueCollectionToForm(ref rs, nvc);

                    var response = (HttpWebResponse)wr.GetResponse();
                }
                uploadMsg.InnerHtml = ib.CommandArgument + " removed";                
                File.Delete(Server.MapPath(string.Format("~/App_Data/{0}", ib.CommandArgument))); //remove from app_data just in case its still floating around
            }
            catch { }
            LoadImages();
        }


        #endregion

        
        

    }
}


