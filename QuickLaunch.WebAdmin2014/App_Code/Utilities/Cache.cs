﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuickLaunch.WebAdmin2014
{
    public static class Cache
    {
        /// <summary>
        /// Insert value into the cache 
        /// </summary>
        /// <param name="key">Name of item</param>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="o">Item to be cached</param>        
        public static void Insert<T>(string key, T obj, int DaysTillExpiration = 3, int MinutesTillExpiration = 0) where T : class
        {
            HttpRuntime.Cache.Insert(
                                    key,
                                    obj,
                                    null,
                                    (MinutesTillExpiration > 0 ? DateTime.Now.AddMinutes(MinutesTillExpiration) : DateTime.Now.AddDays(DaysTillExpiration)),
                                    System.Web.Caching.Cache.NoSlidingExpiration);
        }

        /// <summary>
        /// Remove item from cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        public static void Clear(string key)
        {
            HttpRuntime.Cache.Remove(key);
            try
            {
                if (common.BaseUrl.Length > 1) //clear cache on both servers
                    foreach (string s in common.BaseUrl)
                        common.CallWebClient(string.Format("{0}CacheHandler.ashx?clear={1}", s, key));
            }
            catch { }
        }

        /// <summary>
        /// Check for item in cache
        /// </summary>
        /// <param name="key">Name of cached item</param>
        /// <returns></returns>
        public static bool Exists(string key)
        {
            return HttpRuntime.Cache[key] != null;
        }

        /// <summary>
        /// Retrieve cached item
        /// </summary>
        /// <typeparam name="T">Type of cached item</typeparam>
        /// <param name="key">Name of cached item</param>
        /// <returns>Cached item as type</returns>
        public static T Get<T>(string key) where T : class
        {
            try
            {
                return (T)HttpRuntime.Cache[key];
            }
            catch
            {
                return null;
            }
        }
    }
}
