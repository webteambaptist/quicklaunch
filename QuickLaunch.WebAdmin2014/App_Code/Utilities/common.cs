﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using QuickLaunch;
using WindowsInstaller;

namespace QuickLaunch.WebAdmin2014
{
    public class common
    {
        public static QuickLaunch.Models.UserProfile user { get { return (QuickLaunch.Models.UserProfile)System.Web.HttpContext.Current.Session["user"] ?? null; } }
        public static string ntID { get { return (string)System.Web.HttpContext.Current.Session["ntID"]; } }        
        public static string[] BaseUrl { get { return ((string)ConfigurationManager.AppSettings["BaseUrl"]).Split(','); } }

        public common()
        {
        }

        public static void ExportGridToExcel(Page refPg, GridView gv, string filename)
        {

            refPg.Response.ClearContent();
            refPg.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".xls");
            refPg.Response.ContentType = "application/excel";
            //refPg.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            gv.RenderControl(htw);

            //refPg.Response.Write(@"<style> .text { mso-number-format:\@; } </style> ");
            refPg.Response.Write(sw.ToString());
            refPg.Response.End();


        }
        public static void bindGrid(GridView gv, object e)
        {
            gv.DataSource = e;
            gv.DataBind();
        }
        public static void SendAJAX_Alert(Page page, Type type, string name, string message)
        {
            ScriptManager.RegisterClientScriptBlock(page, type, name, "alert('" + message.Replace("'", "") + "');", true);
        }
        public static void SendAJAX_Script(Page page, Type type, string name, string script)
        {
            ScriptManager.RegisterClientScriptBlock(page, type, name, script, true);
        }
        public static void SendAJAX_PopupMsg(Page page, Type type, string msg, int timeout = 4000)
        {
            ScriptManager.RegisterClientScriptBlock(page, type, "notifyMsg", "notify('<br />" + msg + "', " + timeout.ToString() + ");", true);
        }
        public static void SendNonAJAX_Script(Page page, Type type, string name, string script)
        {
            page.ClientScript.RegisterClientScriptBlock(type, name, script, true);
        }
        public static void SendNonAJAX_PopupMsg(Page page, Type type, string msg, int timeout = 4000)
        {
            page.ClientScript.RegisterClientScriptBlock(type, "notifyMsg", "notify('<br />" + msg + "', " + timeout.ToString() + ");", true);
        }
        public static void sendMasterPopupMsg(Page page, string msg)
        {
            ((Panel)page.Form.FindControl("pnlMessage")).Visible = true;
            ((Label)page.Form.FindControl("lblMessage")).Text = msg;
        }

        public static string fixNull(object dbvalue)
        {
            //Used for balancing dates with nulls and default values 
            try
            {
                if (dbvalue == System.DBNull.Value)
                {
                    return "N/A";
                }
                DateTime d = (DateTime)dbvalue;
                if (d.Year == 1900 | d.Year == 9999)
                {
                    return "N/A";
                }
            }
            //Means it's a valid value 
            catch (Exception ex)
            {
                if (dbvalue.ToString().Length == 0)
                {
                    return "N/A";
                }
            }

            return dbvalue.ToString().Trim();
        }

        public static double fixNumberNull(object dbvalue)
        {
            try
            {
                if (dbvalue == System.DBNull.Value)
                {
                    return 0;
                }
            }

            //Means it's a valid value 
            catch (Exception ex)
            {
                if (dbvalue.ToString().Length == 0)
                {
                    return 0;
                }
            }

            return Convert.ToDouble(dbvalue);

        }

        public static string cleanNTID(string nt)
        {
            int findslash = nt.IndexOf("\\");
            nt = nt.Substring(findslash + 1);
            return nt.ToUpper();
        }

        public static string clean(string value)
        {
            //For user input 
            value = value.Replace("'", "''");
            return value;
        }


        public static string findFileType(string name)
        {
            //Return Image to Use 
            string[] extArray = name.Split('.');
            string ext = extArray[extArray.Length - 1];
            switch (ext.ToLower())
            {
                case "doc":
                case "docx":
                    ext = "word.gif";
                    break;
                case "xls":
                case "xlsx":
                    ext = "excel.gif";
                    break;
                case "ppt":
                case "pptx":
                    ext = "ppt.gif";
                    break;
                case "htm":
                case "html":
                    ext = "ie.gif";
                    break;
                case "txt":
                case "prn":
                case "npd":
                    ext = "text.gif";
                    break;
                default:
                    ext = "arrow.gif";
                    break;
            }
            return ext;

        }

        public static string GetDb2Date(DateTime dtweb)
        {
            string db2date = "";
            db2date = dtweb.Year.ToString() + "-"
                    + dtweb.Month.ToString() + "-"
                    + dtweb.Day.ToString() + "-00.00.00.000000";
            return (db2date);
        }
        public static string GetDb2ShortDate(DateTime dtest)
        {
            string db2date = "";
            db2date = dtest.Year.ToString() + "-"
                    + dtest.Month.ToString().PadLeft(2, '0') + "-"
                    + dtest.Day.ToString().PadLeft(2, '0');
            return (db2date);
        }
        public static long GetJavascriptTimestamp(System.DateTime input)
        {
            System.TimeSpan span = new System.TimeSpan(System.DateTime.Parse("1/1/1970").Ticks);
            System.DateTime time = input.Subtract(span);
            return (long)(time.Ticks / 10000);
        }

        private static string GetAttribute(string container, string attributeStart, string attributeEnd)
        {
            int firstOccurence = container.IndexOf(attributeStart, 0) + attributeStart.Length;
            int lastOccurence = container.IndexOf(attributeEnd, firstOccurence) - firstOccurence;
            return container.Substring(firstOccurence, lastOccurence); //425,69
        }
        public static string createWebKey()
        {
            return DateTime.Now.ToString("F") + " " + new Random().Next(1000, 1000000).ToString();
        }
        public static string CompressPage(HtmlTextWriter htmlwriter)
        {
            string html = htmlwriter.InnerWriter.ToString();
            html = REGEX_BETWEEN_TAGS.Replace(html, "> <");
            html = REGEX_LINE_BREAKS.Replace(html, string.Empty);

            return html;
        }
        private static readonly Regex REGEX_BETWEEN_TAGS = new Regex(@">\s+<", RegexOptions.Compiled);
        private static readonly Regex REGEX_LINE_BREAKS = new Regex(@"\n\s+", RegexOptions.Compiled);
        public static bool IsAjaxPostBackRequest(Page pg)
        {
            return pg.Request.Headers["X-MicrosoftAjax"] != null;
        }
        public static bool CheckIfFilled(Page pg, object pf)
        {
            foreach (System.Reflection.PropertyInfo pi in pf.GetType().GetProperties())
                if (!string.IsNullOrEmpty((string)pi.GetValue(pf, null)))
                    if ((string)pi.GetValue(pf, null) != "0")
                        return true;
            return false;
        }

        public static string GetMsiVersion(string installerPath)
        {
            Type t = Type.GetTypeFromProgID("WindowsInstaller.Installer");
            Installer inst = (Installer)Activator.CreateInstance(t);
            Database d = inst.OpenDatabase(
                installerPath,
                MsiOpenDatabaseMode.msiOpenDatabaseModeReadOnly);
            WindowsInstaller.View v = d.OpenView(
                "SELECT * FROM Property WHERE Property = 'ProductVersion'");
            v.Execute(null);
            Record r = v.Fetch();
            string result = r.get_StringData(2);
            return result;
        }
        public static void CallWebClient(string url)
        {
            using (WebClient client = new WebClient())
            {
                using (StreamReader reader = new StreamReader(client.OpenRead(url)))
                {
                    string s = reader.ReadToEnd();
                    Console.WriteLine(s);
                }
            }
        }
    }
}