﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/App_Master/Site.Master" CodeBehind="ReportViewer.aspx.cs" Inherits="QuickLaunch.WebAdmin2014.ReportViewer" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>


<asp:Content runat="server" ContentPlaceHolderID="body">
    <asp:UpdatePanel runat="server">
        <ContentTemplate>            
                    <div>
                        <rsweb:ReportViewer ID="ReportViewer1" runat="server"></rsweb:ReportViewer>
                        <%--<rsweb:ReportViewer ID="ReportViewer1" runat="server" ProcessingMode="Remote" ShowBackButton="true" ShowExportControls="true" ShowPrintButton="true" ValidateRequestMode="Enabled" WaitControlDisplayAfter="10">
                            <ServerReport ReportServerUrl="http://bhtmsqinf02v/reportserver" />
                        </rsweb:ReportViewer>--%>                        
                    </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
