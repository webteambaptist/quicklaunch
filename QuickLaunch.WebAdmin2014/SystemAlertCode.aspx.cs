﻿using QuickLaunch.WebAdmin2014;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuickLaunch.WebAdmin2014
{
    public partial class SystemAlertCode : QuickLaunch.WebAdmin2014.BasePages.SecureUserPage
    {
        public static int RequiredRoleId = 3;
        protected int id { get { return Convert.ToInt32((string)System.Web.HttpContext.Current.Request.QueryString["id"] ?? "-1"); } }
        protected void Page_Load(object sender, EventArgs e)
        {            
            if (!IsPostBack)
            {
                QuickLaunch.WebAdmin2014.BasePages.SecureUserPage.CheckPageLevelUserPermission(Response, RequiredRoleId); //checks to make sure the user is in the correct role to view this page

                if (id > 0)                
                    LoadDetail();
                else if (id == 0)
                { //New Alert Code
                    mv.ActiveViewIndex = 1;
                }
                else
                    LoadAlerts();

            }
        }
        private void LoadDetail()
        {
            mv.ActiveViewIndex = 1;            
            Models.SystemAlertCode sac = Models.SystemAlertCode.selSystemAlertCodeById(id);
            tCode.Value = sac.Code.ToUpper();
            tDesc.Value = sac.Description;
            tIconColor.Value = sac.IconColor;
            tBackColor.Value = sac.BackColor;
            tForeColor.Value = sac.ForeColor;
            cbUseTicker.Checked = sac.UseTicker;
            cbUseDialog.Checked = sac.UseDialog;
            cbUseOSBubble.Checked = sac.UseOSBubble;
            cbUseTrayToast.Checked = sac.UseTrayToast;
            rTemplates.DataSource = Models.SystemAlertCode.selSystemAlertTemplates(sac.id);
            rTemplates.DataBind();
        }
        private void LoadAlerts()
        {
            mv.ActiveViewIndex = 0;
            if (!WebAdmin2014.Cache.Exists("AlertCodes"))
                WebAdmin2014.Cache.Insert("AlertCodes", Models.SystemAlertCode.selSystemAlertCodes(), 14);
            rSysAlertCode.DataSource = WebAdmin2014.Cache.Get<List<Models.SystemAlertCode>>("AlertCodes");
            rSysAlertCode.DataBind();
        }
        protected void bSave_Click(object sender, EventArgs e)
        {
            Models.SystemAlertCode sac = new Models.SystemAlertCode();
            sac.id = id;
            sac.Code = tCode.Value;
            sac.Description = tDesc.Value;
            sac.IconColor = tIconColor.Value;
            sac.BackColor = tBackColor.Value;
            sac.ForeColor = tForeColor.Value;
            sac.UseTicker = cbUseTicker.Checked;
            sac.UseDialog = cbUseDialog.Checked;
            sac.UseOSBubble = cbUseOSBubble.Checked;
            sac.UseTrayToast = cbUseTrayToast.Checked;
            sac.lup_user = common.user.ntID;
            sac.lup_dt = DateTime.Now;
            if (id == 0)
            {
                try
                {
                    int sId = Convert.ToInt32(Models.SystemAlertCode.insSystemAlertCode(sac));
                    try
                    { //load temps
                        List<Models.SystemAlertCode.SystemAlertTemplate> saTemplate = Session["CodeTemplate"] == null ? new List<Models.SystemAlertCode.SystemAlertTemplate>() : Session["CodeTemplate"] as List<Models.SystemAlertCode.SystemAlertTemplate>;
                        foreach (Models.SystemAlertCode.SystemAlertTemplate dl in saTemplate)
                        {
                            dl.CodeId = sId;
                            Models.SystemAlertCode.insSystemAlertTemplate(dl);
                        }
                        Session.Remove("CodeTemplate");
                    }
                    catch { }
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Alert Code Add Failed!');"); return; }
                tCode.Value = string.Empty;
                tDesc.Value = string.Empty;
            }
            else
            {
                try
                {
                    Models.SystemAlertCode.updSystemAlertCode(sac);                    
                }
                catch { common.SendNonAJAX_Script(Page, this.GetType(), "Error", "alert('Alert Code Update Failed!');"); return; }
            }            
            //common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Alert Code Saved Successfully!'); parent.CloseModal();"); 
            WebAdmin2014.Cache.Clear("AlertCodes");
            WebAdmin2014.Cache.Clear("SystemAlerts");

            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "SystemAlertCode.aspx");
        }
        protected void bRefresh_Click(object sender, EventArgs e)
        {
            LoadAlerts();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Grid", "SetGrid();");
        }
        protected void bRemove_Click(object sender, EventArgs e)
        {
            Models.SystemAlertCode.delSystemAlertCode(new Models.SystemAlertCode { id = id });
            common.SendNonAJAX_Script(Page, this.GetType(), "Save", "parent.document.getElementById('bRefresh').click(); parent.notify('Alert Code Removed!'); parent.CloseModal();");
            WebAdmin2014.Cache.Clear("AlertCodes");

            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "SystemAlertCode.aspx");
        }
        protected void bAddTemplate_Click(object sender, EventArgs e)
        {
            Models.SystemAlertCode.SystemAlertTemplate dSystemAlertTemplate = new Models.SystemAlertCode.SystemAlertTemplate { CodeId = id, Text = tTemplate.Value };
            if (id > 0)
            {
                Models.SystemAlertCode.insSystemAlertTemplate(dSystemAlertTemplate);
                rTemplates.DataSource = Models.SystemAlertCode.selSystemAlertTemplates(id);
            }
            else
            {
                List<Models.SystemAlertCode.SystemAlertTemplate> LocTemplate = Session["CodeTemplate"] == null ? new List<Models.SystemAlertCode.SystemAlertTemplate>() : Session["CodeTemplate"] as List<Models.SystemAlertCode.SystemAlertTemplate>;
                dSystemAlertTemplate.id = DateTime.Now.Millisecond;
                LocTemplate.Add(dSystemAlertTemplate);
                rTemplates.DataSource = LocTemplate;
                Session["CodeTemplate"] = LocTemplate;
            }
            rTemplates.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }
        protected void lbDeleteTemp_Click(object sender, EventArgs e)
        {
            LinkButton lb = sender as LinkButton;
            if (id > 0)
            {
                Models.SystemAlertCode.delSystemAlertTemplate(new Models.SystemAlertCode.SystemAlertTemplate { id = Convert.ToInt32(lb.CommandArgument) });
                rTemplates.DataSource = Models.SystemAlertCode.selSystemAlertTemplates(id);
            }
            else
            {
                List<Models.SystemAlertCode.SystemAlertTemplate> SystemAlertTemplate= Session["CodeTemplate"] == null ? new List<Models.SystemAlertCode.SystemAlertTemplate>() : Session["CodeTemplate"] as List<Models.SystemAlertCode.SystemAlertTemplate>;
                SystemAlertTemplate.Remove(SystemAlertTemplate.Find(p => p.id == Convert.ToInt32(lb.CommandArgument)));
                rTemplates.DataSource = SystemAlertTemplate;
                Session["CodeTemplate"] = SystemAlertTemplate;
            }

            rTemplates.DataBind();
            common.SendAJAX_Script(Page, this.GetType(), "Set_Page", "SetPage();");
        }

        protected void bAdd_Click(object sender, EventArgs e)
        {
            string url = "";
            url = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";

            Response.Redirect(url + "SystemAlertCode.aspx?id=0");
        }         
    }
}
