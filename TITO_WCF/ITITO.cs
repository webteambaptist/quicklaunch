﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace TITO_WCF
{
    [ServiceContract]
    public interface IQL
    {
        [OperationContract]
        List<XenAppInfo> GetFarms();

        [OperationContract]
        string GetTITOHash(string environment);

        [OperationContract]
        bool isBPC(string machine);

        [OperationContract]
        List<string> GetKillList(string machine);

        [OperationContract]
        bool InsertErrorLog(QErrorLog errLog);

    }
}
