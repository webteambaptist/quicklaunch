using System;

public static class ConvertDBNull
{
	public static T To<T>(object value, T defaultValue)
	{
		if (value == DBNull.Value)
		{
			return defaultValue;
		}
		return (T)Convert.ChangeType(value, typeof(T));
	}
}