﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TITO_WCF
{
    public class DAL
    {
        public DAL()
        {
        }

        internal List<XenAppInfo> GetFarmsFromDB()
        {
            List<XenAppInfo> xenAppInfos = new List<XenAppInfo>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.GetFarms", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        XenAppInfo xenAppInfo = new XenAppInfo()
                        {
                            Farm = sqlDataReader["farmName"].ToString(),
                            XenAppHost1 = sqlDataReader["XenAppHost1"].ToString(),
                            XenAppHost2 = sqlDataReader["XenAppHost2"].ToString(),
                            XenAppPort = sqlDataReader["XenAppPort"].ToString()
                        };
                        xenAppInfos.Add(xenAppInfo);
                    }
                    sqlDataReader.Close();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
            return xenAppInfos;
        }

        internal List<string> GetBPCMachines()
        {
            List<string> machlist = new List<string>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["altiris"].ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.usp_getBPCmachines", sqlConnection)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        string s = sqlDataReader[0].ToString();
                        machlist.Add(s);
                    }
                    sqlDataReader.Close();
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }
            }
            return machlist;
        }

        #region QApplication

        internal QApplication selApplicationById(int id)
        {
            QApplication app = new QApplication();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selApplicationById", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        app.id = id;
                        app.Title = sdr["Title"].ToString();
                        app.Value = sdr["Value"].ToString();
                        app.WindowTitle = sdr["WindowTitle"].ToString();
                        app.AllowRoaming = Boolean.Parse(sdr["AllowRoaming"].ToString());
                        app.DefaultImg = sdr["DefaultImg"].ToString();
                        app.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        app.lup_user = sdr["lup_user"].ToString();
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return app;
        }

        internal List<QApplication> selApplications()
        {
            List<QApplication> apps = new List<QApplication>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selApplications", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QApplication app = new QApplication();

                        app.id = int.Parse(sdr["id"].ToString());
                        app.Title = sdr["Title"].ToString();
                        app.Value = sdr["Value"].ToString();
                        app.WindowTitle = sdr["WindowTitle"].ToString();
                        app.AllowRoaming = Boolean.Parse(sdr["AllowRoaming"].ToString());
                        app.DefaultImg = sdr["DefaultImg"].ToString();
                        app.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        app.lup_user = sdr["lup_user"].ToString();
                        app.TitleValueDisplay = sdr["TitleValueDisplay"].ToString();

                        apps.Add(app);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return apps;
        }

        internal string insApplication(QApplication Application)
        {
            string id = "";
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insApplication";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", Application.id);
                    sqlcmd.Parameters.AddWithValue("Title", Application.Title);
                    sqlcmd.Parameters.AddWithValue("Value", Application.Value);
                    sqlcmd.Parameters.AddWithValue("WindowTitle", Application.WindowTitle);
                    sqlcmd.Parameters.AddWithValue("AllowRoaming", Application.AllowRoaming);
                    sqlcmd.Parameters.AddWithValue("DefaultImg", Application.DefaultImg);
                    sqlcmd.Parameters.AddWithValue("lup_user", Application.lup_user);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql

            return id;
        }

        internal void updApplication(QApplication Application)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updApplication";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", Application.id);
                    sqlcmd.Parameters.AddWithValue("Title", Application.Title);
                    sqlcmd.Parameters.AddWithValue("Value", Application.Value);
                    sqlcmd.Parameters.AddWithValue("WindowTitle", Application.WindowTitle);
                    sqlcmd.Parameters.AddWithValue("AllowRoaming", Application.AllowRoaming);
                    sqlcmd.Parameters.AddWithValue("DefaultImg", Application.DefaultImg);
                    sqlcmd.Parameters.AddWithValue("lup_user", Application.lup_user);
                    sqlcmd.Parameters.AddWithValue("lup_dt", Application.lup_dt);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql
        }

        internal void delApplication(QApplication Application)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delApplication";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", Application.id);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql
        }

        #endregion

        #region QBookmark

        internal QBookmark selBookmarkById(int id)
        {
            QBookmark b = new QBookmark();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selBookmarkById", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        b.id = id;
                        b.NtId = sdr["NtId"].ToString();
                        b.Title = sdr["Title"].ToString();
                        b.Url = sdr["Url"].ToString();
                        b.OrderId = int.Parse(sdr["OrderId"].ToString());
                        b.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        b.add_dt = DateTime.Parse(sdr["add_dt"].ToString());
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return b;
        }

        internal List<QBookmark> selBookmarkByUserId(string userId)
        {
            List<QBookmark> bm = new List<QBookmark>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selBookmarkByUserId", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("userId", userId);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QBookmark b = new QBookmark();

                        b.id = int.Parse(sdr["id"].ToString());
                        b.NtId = sdr["NtId"].ToString();
                        b.Title = sdr["Title"].ToString();
                        b.Url = sdr["Url"].ToString();
                        b.OrderId = int.Parse(sdr["OrderId"].ToString());
                        b.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        b.add_dt = DateTime.Parse(sdr["add_dt"].ToString());

                        bm.Add(b);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return bm;
        }

        internal List<QBookmark> selBookmark()
        {
            List<QBookmark> bm = new List<QBookmark>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selBookmark", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("userId", userId);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QBookmark b = new QBookmark();

                        b.id = int.Parse(sdr["id"].ToString());
                        b.NtId = sdr["NtId"].ToString();
                        b.Title = sdr["Title"].ToString();
                        b.Url = sdr["Url"].ToString();
                        b.OrderId = int.Parse(sdr["OrderId"].ToString());
                        b.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        b.add_dt = DateTime.Parse(sdr["add_dt"].ToString());

                        bm.Add(b);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return bm;
        }

        internal string insBookmark(QBookmark fav)
        {
            string id = "";

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insBookmark", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("NtId", fav.NtId);
                    sqlcmd.Parameters.AddWithValue("Title", fav.Title);
                    sqlcmd.Parameters.AddWithValue("Url", fav.Url);
                    sqlcmd.Parameters.AddWithValue("OrderId", fav.OrderId);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return id;
        }

        internal void insBookmark(List<QBookmark> fav)
        {
            foreach (QBookmark b in fav)
            {
                insBookmark(b);
            }
        }

        internal void updBookmark(QBookmark fav)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updBookmark";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("NtId", fav.NtId);
                    sqlcmd.Parameters.AddWithValue("Title", fav.Title);
                    sqlcmd.Parameters.AddWithValue("Url", fav.Url);
                    sqlcmd.Parameters.AddWithValue("OrderId", fav.OrderId);
                    sqlcmd.Parameters.AddWithValue("id", fav.id);
                    sqlcmd.Parameters.AddWithValue("lup_dt", fav.lup_dt);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql
        }

        internal void updBookmark(List<QBookmark> fav)
        {
            foreach (QBookmark b in fav)
            {
                updBookmark(b);
            }
        }

        internal void updBookmarkOrder(List<QBookmark> favs)
        {
            string sql = string.Empty;
            for (int i = 0; i < favs.Count; i++)
            {
                sql = "UPDATE SSO_Bookmark SET [OrderId] = " + (i + 1).ToString() + " WHERE [id] = " + favs[i].id.ToString() + "; ";
                SqlCommand sqlcmd = new SqlCommand();
                using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
                {
                    try
                    {
                        sqlcmd.Connection = sqlConn;
                        sqlcmd.CommandType = CommandType.Text;
                        sqlcmd.Connection.Open();
                        sqlcmd.ExecuteNonQuery();
                        sqlcmd.Connection.Close();
                    }
                    catch (Exception e)
                    {
                        //isSaved = false;
                    }
                }//end sql
            }
        }

        internal void delBookmark(QBookmark Bookmark)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delBookmark";
                    sqlcmd.Connection.Open();
                    ;
                    sqlcmd.Parameters.AddWithValue("id", Bookmark.id);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql
        }

        internal void delBookmark(List<QBookmark> fav)
        {
            foreach (QBookmark b in fav)
            {
                delBookmark(b);
            }
        }

        #endregion

        #region QDeployment

        #endregion

        #region errorlog
        internal List<QErrorLog> selErrorLogs(int MaxCt)
        {
            if (MaxCt == 0)
            {
                MaxCt = 100;
            }

            List<QErrorLog> e = new List<QErrorLog>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selErrorLogs", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QErrorLog el = new QErrorLog();

                        el.id = int.Parse(sdr["id"].ToString());
                        el.Exception = sdr["Exception"].ToString();
                        el.QLVersion = sdr["QLVersion"].ToString();
                        el.Serial = sdr["Serial"].ToString();
                        el.LSID = sdr["LSID"].ToString();
                        el.CurrentUserId = sdr["CurrentUserId"].ToString();
                        el.IsFatal = Boolean.Parse(sdr["IsFatal"].ToString());
                        el.ICAVersion = sdr["ICAVersion"].ToString();
                        el.IEVersion = sdr["IEVersion"].ToString();
                        el.Application = sdr["Application"].ToString();
                        el.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        el.WTSLocation = sdr["WTSLocation"].ToString();

                        e.Add(el);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return e;
        }

        internal static List<QErrorLog> selErrorsByMachine(string machine, int MaxCt)
        {
            if (MaxCt == 0)
            {
                MaxCt = 10;
            }

            List<QErrorLog> e = new List<QErrorLog>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selErrorsByMachine", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("Serial", machine);
                    sqlcmd.Parameters.AddWithValue("MaxCt", MaxCt);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QErrorLog el = new QErrorLog();

                        el.id = int.Parse(sdr["id"].ToString());
                        el.Exception = sdr["Exception"].ToString();
                        el.QLVersion = sdr["QLVersion"].ToString();
                        el.Serial = sdr["Serial"].ToString();
                        el.LSID = sdr["LSID"].ToString();
                        el.CurrentUserId = sdr["CurrentUserId"].ToString();
                        el.IsFatal = Boolean.Parse(sdr["IsFatal"].ToString());
                        el.ICAVersion = sdr["ICAVersion"].ToString();
                        el.IEVersion = sdr["IEVersion"].ToString();
                        el.Application = sdr["Application"].ToString();
                        el.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        //el.WTSLocation = sdr["WTSLocation"].ToString();

                        e.Add(el);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return e;
        }

        internal List<QErrorLog> selErrorsByUser(string userId, int MaxCt)
        {
            if (MaxCt == 0)
            {
                MaxCt = 10;
            }

            List<QErrorLog> e = new List<QErrorLog>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selErrorsByUser", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("CurrentUserId", userId);
                    sqlcmd.Parameters.AddWithValue("MaxCt", MaxCt);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QErrorLog el = new QErrorLog();

                        el.id = int.Parse(sdr["id"].ToString());
                        el.Exception = sdr["Exception"].ToString();
                        el.QLVersion = sdr["QLVersion"].ToString();
                        el.Serial = sdr["Serial"].ToString();
                        el.LSID = sdr["LSID"].ToString();
                        el.CurrentUserId = sdr["CurrentUserId"].ToString();
                        el.IsFatal = Boolean.Parse(sdr["IsFatal"].ToString());
                        el.ICAVersion = sdr["ICAVersion"].ToString();
                        el.IEVersion = sdr["IEVersion"].ToString();
                        el.Application = sdr["Application"].ToString();
                        el.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        //el.WTSLocation = sdr["WTSLocation"].ToString();

                        e.Add(el);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return e;
        }

        internal string insErrorLog(QErrorLog ErrorLog)
        {
            bool isSaved = true;
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insErrorLog";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("CurrentUserId", ErrorLog.CurrentUserId);
                    sqlcmd.Parameters.AddWithValue("CodeId", ErrorLog.Exception);
                    sqlcmd.Parameters.AddWithValue("Message", ErrorLog.QLVersion);
                    sqlcmd.Parameters.AddWithValue("Application", ErrorLog.Serial);
                    sqlcmd.Parameters.AddWithValue("AttachmentLink", ErrorLog.LSID);
                    sqlcmd.Parameters.AddWithValue("Icon", ErrorLog.IsFatal);
                    sqlcmd.Parameters.AddWithValue("FontColor", ErrorLog.ICAVersion);
                    sqlcmd.Parameters.AddWithValue("FontColorTicker", ErrorLog.IEVersion);
                    sqlcmd.Parameters.AddWithValue("FontColorToast", ErrorLog.Application);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    isSaved = false;
                }
            }//end sql

            return id;
        }

        internal static void delErrorLog(QErrorLog ErrorLog)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delErrorLog";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", ErrorLog.id);

                    //id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql
        }

        internal static void delExtinctErrors(int days = 180)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delExtinctErrors";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("days", days);

                    //id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql
        }

        #endregion

        #region event

        #endregion

        #region eventlog

        public static QEventLog selEventLogById(int id)
        {
            QEventLog el = new QEventLog();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selEventLogById", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        //el.id = id;
                        el.EventCode = sdr["EventCode"].ToString();
                        el.NtId = sdr["NtId"].ToString();
                        el.Machine = sdr["Machine"].ToString();
                        el.Application = sdr["Application"].ToString();
                        el.MenuItemId = int.Parse(sdr["MenuItemId"].ToString());
                        el.LengthOfTime = double.Parse(sdr["LengthOfTime"].ToString());

                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return el;
        }

        public static List<QEventLog> selEventLogs(int MaxCt = 100)
        {
            List<QEventLog> e = new List<QEventLog>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selEventLogs", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("MaxCt", MaxCt);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QEventLog el = new QEventLog();
                        //el.id = id;
                        el.EventCode = sdr["EventCode"].ToString();
                        el.NtId = sdr["NtId"].ToString();
                        el.Machine = sdr["Machine"].ToString();
                        el.Application = sdr["Application"].ToString();
                        el.MenuItemId = int.Parse(sdr["MenuItemId"].ToString());
                        el.LengthOfTime = double.Parse(sdr["LengthOfTime"].ToString());

                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return e;
        }

        /// <summary>
        /// Event Log grouped by datetime
        /// </summary>
        /// <param name="GroupBy">week,day,hour</param>
        /// <param name="userId">NTID</param>
        /// <param name="machine">S/N</param>
        /// <returns></returns>
        //public static List<EventLog> selEventLogsGroupedBy(string GroupBy, DateTime DataYoungerThan, string userId = "", string machine = "")
        //{
        //    List<EventLog> e = new List<EventLog>();

        //    using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
        //    {
        //        try
        //        {
        //            sqlConn.Open();
        //            SqlCommand sqlcmd = new SqlCommand("dbo.selEventLogs", sqlConn);
        //            sqlcmd.CommandType = CommandType.StoredProcedure;
        //            sqlcmd.Parameters.AddWithValue("MaxCt", MaxCt);
        //            SqlDataReader sdr = sqlcmd.ExecuteReader();

        //            while (sdr.Read())
        //            {
        //                EventLog el = new EventLog();
        //                //el.id = id;
        //                el.EventCode = sdr["EventCode"].ToString();
        //                el.NtId = sdr["NtId"].ToString();
        //                el.Machine = sdr["Machine"].ToString();
        //                el.Application = sdr["Application"].ToString();
        //                el.MenuItemId = int.Parse(sdr["MenuItemId"].ToString());
        //                el.LengthOfTime = double.Parse(sdr["LengthOfTime"].ToString());

        //            }//end while

        //            sdr.Close();
        //        }//end try

        //        catch (SqlException se)
        //        { }//end catch
        //        catch (Exception ee)
        //        { }//end catch

        //    }//end sql

        //    return e;
        //}

        public static List<QEventLog> selEventsByMachine(string machine, int MaxCt = 10)
        {
            List<QEventLog> e = new List<QEventLog>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selEventsByMachine", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("MaxCt", MaxCt);
                    sqlcmd.Parameters.AddWithValue("Machine", machine);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QEventLog el = new QEventLog();
                        //el.id = id;
                        el.EventCode = sdr["EventCode"].ToString();
                        el.NtId = sdr["NtId"].ToString();
                        el.Machine = sdr["Machine"].ToString();
                        el.Application = sdr["Application"].ToString();
                        el.MenuItemId = int.Parse(sdr["MenuItemId"].ToString());
                        el.LengthOfTime = double.Parse(sdr["LengthOfTime"].ToString());

                        e.Add(el);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return e;
        }

        public static List<QEventLog> selEventsByUser(string userId, int MaxCt = 10)
        {
            List<QEventLog> e = new List<QEventLog>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selEventsByUser", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("MaxCt", MaxCt);
                    sqlcmd.Parameters.AddWithValue("NtId", userId);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        QEventLog el = new QEventLog();
                        //el.id = id;
                        el.EventCode = sdr["EventCode"].ToString();
                        el.NtId = sdr["NtId"].ToString();
                        el.Machine = sdr["Machine"].ToString();
                        el.Application = sdr["Application"].ToString();
                        el.MenuItemId = int.Parse(sdr["MenuItemId"].ToString());
                        el.LengthOfTime = double.Parse(sdr["LengthOfTime"].ToString());

                        e.Add(el);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql

            return e;
        }

        //public static List<EventLog.DataWarehouse> selExtinctingEventLogs(int days = 90)
        //{
        //    List<EventLog.DataWarehouse> e = new List<EventLog.DataWarehouse>();

        //    using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
        //    {
        //        try
        //        {
        //            sqlConn.Open();
        //            SqlCommand sqlcmd = new SqlCommand("dbo.selExtinctingEventLogs", sqlConn);
        //            sqlcmd.CommandType = CommandType.StoredProcedure;
        //            sqlcmd.Parameters.AddWithValue("Days", days);
        //            SqlDataReader sdr = sqlcmd.ExecuteReader();

        //            while (sdr.Read())
        //            {
        //                EventLog.DataWarehouse el = new EventLog.DataWarehouse();
        //                //el.id = id;
        //                el.ActivityDt = DateTime.Parse(sdr["ActivityDt"].ToString());
        //                el.UserCt = int.Parse(sdr["UserCt"].ToString());
        //                el.MachineCt = int.Parse(sdr["MachineCt"].ToString());
        //                el.LoginCt = int.Parse(sdr["LoginCt"].ToString());
        //                el.LoginAvg = double.Parse(sdr["LoginAvg"].ToString());
        //                el.RoamCt = int.Parse(sdr["RoamCt"].ToString());
        //                el.RoamAvg = double.Parse(sdr["RoamAvg"].ToString());
        //                el.LaunchCt = int.Parse(sdr["LaunchCt"].ToString());
        //                el.LaunchAvg = double.Parse(sdr["LaunchAvg"].ToString());
        //                el.DisconnectCt = int.Parse(sdr["DisconnectCt"].ToString());
        //                el.DisconnectAvg = double.Parse(sdr["DisconnectAvg"].ToString());
        //                el.LogoffCt = int.Parse(sdr["LogoffCt"].ToString());
        //                el.LogoffAvg = double.Parse(sdr["LogoffAvg"].ToString());

        //                e.Add(el);
        //            }//end while

        //            sdr.Close();
        //        }//end try

        //        catch (SqlException se)
        //        { }//end catch
        //        catch (Exception ee)
        //        { }//end catch

        //    }//end sql

        //    return e;
        //}

        public static string insEventLog(QEventLog EventLog)
        {
            bool isSaved = true;
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insEventLog";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("EventCode", EventLog.EventCode);
                    sqlcmd.Parameters.AddWithValue("NtId", EventLog.NtId);
                    sqlcmd.Parameters.AddWithValue("Machine", EventLog.Machine);
                    sqlcmd.Parameters.AddWithValue("Application", EventLog.Application);
                    sqlcmd.Parameters.AddWithValue("MenuItemId", EventLog.MenuItemId);
                    sqlcmd.Parameters.AddWithValue("LengthOfTime", EventLog.LengthOfTime);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    isSaved = false;
                }
            }//end sql

            return id;
        }

        /// <summary>
        /// UserCt and MachineCt are snapshot #'s.
        /// The rest of the counts and avgs are what's being removed. So in the case where there's a record because of timing or missed run day, we need to append that data to the existing row.
        /// </summary>
        /// <param name="dw">Record to be inserted or updated</param>
        //public static void insDataWarehouse(EventLog.DataWarehouse dw)
        //{
        //    SqlCommand sqlcmd = new SqlCommand();
        //    using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
        //    {
        //        try
        //        {
        //            sqlcmd.Connection = sqlConn;
        //            sqlcmd.CommandType = CommandType.StoredProcedure;
        //            sqlcmd.CommandText = "dbo.insEventLog";
        //            sqlcmd.Connection.Open();

        //            sqlcmd.Parameters.AddWithValue("ActivityDt", dw.ActivityDt);
        //            sqlcmd.Parameters.AddWithValue("UserCt", dw.UserCt);
        //            sqlcmd.Parameters.AddWithValue("LoginCt", dw.LoginCt);
        //            sqlcmd.Parameters.AddWithValue("LaunchCt", dw.LaunchCt);
        //            sqlcmd.Parameters.AddWithValue("RoamCt", dw.RoamCt);
        //            sqlcmd.Parameters.AddWithValue("DisconnectCt", dw.DisconnectCt);
        //            sqlcmd.Parameters.AddWithValue("LogoffCt", dw.LogoffCt);
        //            sqlcmd.Parameters.AddWithValue("MachineCt", dw.MachineCt);
        //            sqlcmd.Parameters.AddWithValue("LoginAvg", dw.LoginAvg);
        //            sqlcmd.Parameters.AddWithValue("LaunchAvg", dw.LaunchAvg);
        //            sqlcmd.Parameters.AddWithValue("RoamAvg", dw.RoamAvg);
        //            sqlcmd.Parameters.AddWithValue("LogoffAvg", dw.LogoffAvg);
        //            sqlcmd.Parameters.AddWithValue("DisconnectAvg", dw.DisconnectAvg);
        //            sqlcmd.Parameters.AddWithValue("lup_dt", DateTime.Now);

        //            sqlcmd.Connection.Close();
        //        }
        //        catch (Exception e)
        //        {

        //        }
        //    }//end sql
        //}

        //public static void updClearEventLogMetrics(IEnumerable<UserProfile> profilesToClear, bool Roams = false, bool Logins = false, bool Launches = false, bool Disconnects = false)
        //{
        //    SqlCommand sqlcmd = new SqlCommand();
        //    using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
        //    {
        //        if (Roams)
        //        {

        //            try
        //            {
        //                sqlcmd.Connection = sqlConn;
        //                sqlcmd.CommandType = CommandType.StoredProcedure;
        //                sqlcmd.CommandText = "dbo.updClearUserProfileRoamingMetricsFirstNet";
        //                sqlcmd.Connection.Open();

        //                sqlcmd.Parameters.AddWithValue("NtId", profilesToClear);
        //                sqlcmd.Parameters.AddWithValue("EventCode", DateTime.Now);

        //                //id = sqlcmd.ExecuteScalar().ToString();

        //                sqlcmd.Connection.Close();
        //            }
        //            catch (Exception e) { }
        //        }

        //        if (Logins)
        //        {

        //            try
        //            {
        //                sqlcmd.Connection = sqlConn;
        //                sqlcmd.CommandType = CommandType.StoredProcedure;
        //                sqlcmd.CommandText = "dbo.updClearUserProfileRoamingMetricsFirstNet";
        //                sqlcmd.Connection.Open();

        //                sqlcmd.Parameters.AddWithValue("NtId", profilesToClear);
        //                sqlcmd.Parameters.AddWithValue("EventCode", DateTime.Now);

        //                //id = sqlcmd.ExecuteScalar().ToString();

        //                sqlcmd.Connection.Close();
        //            }
        //            catch (Exception e) { }
        //        }



        //        sqlcmd.Connection.Close();
        //    }//end sql
        //}

        public static void delEventLog(QEventLog EventLog)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delEventLog";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", EventLog.NtId);


                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                }
            }//end sql
        }

        public static void delExtinctEventLogs(int days = 90)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delExtinctEventLogs";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("Days", days);


                    //id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    //isSaved = false;
                }
            }//end sql
        }
        #endregion

        #region QL Settings
            private string getEmail()
            {
                string email = "";
                using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.selDataValue", sqlConn);
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.AddWithValue("datakey", "HelpDeskForm");
                        object obj = sqlcmd.ExecuteScalar();
                        email = obj.ToString();
                    }//end try

                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch

                }//end sql

                return email;
            }
        #endregion
        
        private delegate bool TryParseDelegate<T>(string s, out T t);

        private static T? TryParseNullable<T>(string s, TryParseDelegate<T> tryParse) where T : struct
        {
            if (string.IsNullOrEmpty(s))

                return null;
            T t;
            if (tryParse(s, out t))
                return t;
            return null;
        }//end T? TryParseNullable<T>


    }
}
