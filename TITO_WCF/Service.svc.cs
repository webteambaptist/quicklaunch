﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace TITO_WCF
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.

    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class QL : IQL
    {
        public QL()
        {

        }

        public List<XenAppInfo> GetFarms()
        {
            
            return (new DAL()).GetFarmsFromDB();
        }

        public string GetTITOHash(string environment)
        {
            string hash = "";

            if (environment.ToLower() == "debug")
            {
                hash = ConfigurationManager.AppSettings["DEBUG"].ToString();
            }
            else
            {

                hash = ConfigurationManager.AppSettings["PROD"].ToString();
            }

            return hash;
        }

        public bool isBPC(string machine)
        {
            bool isbpc = false;
            List<string> machineList = new DAL().GetBPCMachines();
            //machineList.Add("6YVMCH1");
            isbpc = (from m in machineList where String.Compare(machine, m, true) == 0 select m).Any();
            
            return isbpc;
        }

        public List<string> GetKillList(string machine)
        {
            //to do move list to db
            List<string> klist = new List<string>(){ "iexplore", "firefox", "chrome", "notepad", "cmd" };
            if (!isBPC(machine))
            {
                klist.Add("natspeak");
            }

            return klist;

        }



        public bool InsertErrorLog(QErrorLog errLog)
        {
            throw new NotImplementedException();
        }

        
    }
}
