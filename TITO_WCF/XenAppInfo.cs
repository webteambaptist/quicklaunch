﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace TITO_WCF
{

    [DataContract]
    public class XenAppInfo
    {
        private string _XenAppHost1;

        private string _XenAppHost2;

        private string _XenAppPort;

        private string _Farm;

        [DataMember]
        public string Farm
        {
            get
            {
                return this._Farm;
            }
            set
            {
                this._Farm = value;
            }
        }

        [DataMember]
        public string XenAppHost1
        {
            get
            {
                return this._XenAppHost1;
            }
            set
            {
                this._XenAppHost1 = value;
            }
        }

        [DataMember]
        public string XenAppHost2
        {
            get
            {
                return this._XenAppHost2;
            }
            set
            {
                this._XenAppHost2 = value;
            }
        }

        [DataMember]
        public string XenAppPort
        {
            get
            {
                return this._XenAppPort;
            }
            set
            {
                this._XenAppPort = value;
            }
        }

        public XenAppInfo()
        {
        }
    }
}
