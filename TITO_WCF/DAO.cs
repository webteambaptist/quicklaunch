﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace TITO_WCF
{

    [DataContract]
    public class QApplication
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public int LaunchCt { get; set; }

        [DataMember]
        public DateTime LastLaunch { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Value { get; set; }

        [DataMember]
        public string WindowTitle { get; set; }

        [DataMember]
        public bool AllowRoaming { get; set; }

        [DataMember]
        public string DefaultImg { get; set; }

        [DataMember]
        public string lup_user { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        [DataMember]
        public string TitleValueDisplay { get; set; }

        public QApplication() { }
    }

    public class BaseModel
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }
    }

    [DataContract]
    public class QBookmark
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string NtId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public int OrderId { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        [DataMember]
        public DateTime add_dt { get; set; }

        [DataMember]
        public bool IsArchived { get; set; }

        public QBookmark() { }
    }

    [DataContract]
    public class QDeployment
    {

        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string InstallMessage { get; set; }

        [DataMember]
        public byte[] MSIBuild { get; set; }

        [DataMember]
        public string MSIBuildContentType { get; set; }

        [DataMember]
        public string MSIBuildFileSize { get; set; }

        [DataMember]
        public string MSIBuildVersion { get; set; }

        [DataMember]
        public List<string> Locations { get; set; }

        [DataMember]
        public string lup_user { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        [DataMember]
        public DateTime add_dt { get; set; }

        [DataMember]
        public DateTime publish_dt { get; set; }

        [DataMember]
        public bool IsEverywhere { get; set; }

        [DataMember]
        public bool IsPublished { get; set; }

        [DataMember]
        public bool IsArchived { get; set; }

        [DataMember]
        public int MachineCt { get; set; }

        public QDeployment() { }   
    }

    [DataContract]
    public class QErrorLog
    {
        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        [DataMember]
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Exception.
        /// </summary>
        [DataMember]
        public string Exception { get; set; }

        /// <summary>
        /// Gets/Sets the QLVersion
        /// </summary>
        [DataMember]
        public string QLVersion { get; set; }

        /// <summary>
        /// Gets/Sets the serial number
        /// </summary>
        [DataMember]
        public string Serial { get; set; }

        /// <summary>
        /// Gets/Sets the LSID
        /// </summary>
        [DataMember]
        public string LSID { get; set; }

        /// <summary>
        /// Gets/Sets the Current UserId
        /// </summary>
        [DataMember]
        public string CurrentUserId { get; set; }

        /// <summary>
        /// Gets/Sets whether the error is fatal
        /// </summary>
        [DataMember]
        public bool IsFatal { get; set; }

        /// <summary>
        /// Gets/Sets the ICAVersion
        /// </summary>
        [DataMember]
        public string ICAVersion { get; set; }

        /// <summary>
        /// Gets/Sets the IEVersion
        /// </summary>
        [DataMember]
        public string IEVersion { get; set; }

        /// <summary>
        /// Gets/Sets the Application
        /// </summary>
        [DataMember]
        public string Application { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        [DataMember]
        public DateTime lup_dt { get; set; }

        /* Display Fields */

        /// <summary>
        /// Gets/Sets the WTS Printer Location.
        /// </summary>
        [DataMember]
        public string WTSLocation { get; set; }

        /* End Display Fields */
    }

    [DataContract]
    public class QEvent
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        public QEvent() { }
    }

    [DataContract]
    public class QEventLog
    {
        [DataMember]
        public string EventCode { get; set; }

        [DataMember]
        public string NtId { get; set; }

        [DataMember]
        public string Machine { get; set; }

        [DataMember]
        public string Application { get; set; }

        [DataMember]
        public int MenuItemId { get; set; }

        [DataMember]
        public double LengthOfTime { get; set; }

        [DataMember]
        public int GroupCount { get; set; }

        [DataMember]
        public long JSTimeStamp { get; set; }

        public QEventLog() { }
    }

    [DataContract]
    public class QHelp
    {

        [DataMember]
        public int id { get; set; }

        [DataMember]
        public int ParentId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Body { get; set; }

        [DataMember]
        public bool IsOpenOnload { get; set; }

        [DataMember]
        public bool IsSelectable { get; set; }

        [DataMember]
        public int OrderId { get; set; }

        [DataMember]
        public string lup_user { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        public QHelp() { }
    }

    [DataContract]
    public class QLocation
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string lup_user { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        [DataMember]
        public int MachineCt { get; set; }

        public QLocation() { }
    }

    [DataContract]
    public class QMachine
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string PCName { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string LSID { get; set; }

        [DataMember]
        public int LocationId { get; set; }

        [DataMember]
        public string QLVersion { get; set; }

        [DataMember]
        public string ICAVersion { get; set; }

        [DataMember]
        public string IEVersion { get; set; }

        [DataMember]
        public string AutoInstallVersion { get; set; }

        [DataMember]
        public string ComputerInfo { get; set; }

        [DataMember]
        public string OSInfo { get; set; }

        [DataMember]
        public string Processes { get; set; }

        [DataMember]
        public string Memory { get; set; }

        [DataMember]
        public string IpAddress { get; set; }

        [DataMember]
        public DateTime LastLogin { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        [DataMember]
        public string LocationTitle { get; set; }

        [DataMember]
        public string WTSLocation { get; set; }

        [DataMember]
        public double AvgLoginLength { get; set; }
        
        [DataMember]
        public double AvgEMRLaunchLength { get; set; }

        [DataMember]
        public double AvgRoamLength { get; set; }

        public QMachine() { }
    }

    [DataContract]
    public class QMenuApplication : QApplication
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public int MenuLink_Id { get; set; }

        [DataMember]
        public int Application_Id { get; set; }

        [DataMember]
        public int Order_Id { get; set; }


        [DataMember]
        public static List<QMenuApplication> MenuApplications = new List<QMenuApplication> {  
                    new QMenuApplication { MenuLink_Id = 100, Application_Id = 100, Order_Id = 1},
                    new QMenuApplication { MenuLink_Id = 100, Application_Id = 101, Order_Id = 2},
                    new QMenuApplication { MenuLink_Id = 100, Application_Id = 102, Order_Id = 3},
                    new QMenuApplication { MenuLink_Id = 101, Application_Id = 103, Order_Id = 1},
                    new QMenuApplication { MenuLink_Id = 101, Application_Id = 104, Order_Id = 2},
                    new QMenuApplication { MenuLink_Id = 102, Application_Id = 105, Order_Id = 1},
                    new QMenuApplication { MenuLink_Id = 102, Application_Id = 106, Order_Id = 2}
        };

        [DataMember]
        public static DateTime LastMenuApplicationsUpdate { get; set; }

        public QMenuApplication() { }
    }

    [DataContract]
    public class QMenuLink
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string name { get; set; }

        [DataMember]
        public string toolTip { get; set; }

        [DataMember]
        public string clickEvent { get; set; }

        [DataMember]
        public string img { get; set; }

        [DataMember]
        public string audience { get; set; }

        [DataMember]
        public string adGroupFilter { get; set; }

        [DataMember]
        public int orderId { get; set; }

        [DataMember]
        public bool isVisible { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        public QMenuLink() { }
    }

    [DataContract]
    public class QQueue
    {

    }

    [DataContract]
    public class QRole
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string role { get; set; }

        [DataMember]
        public string lup_user { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        public QRole() { }
    }

    [DataContract]
    public class QServiceFormData
    {

    }

    [DataContract]
    public class QUserFarm
    {

    }

    [DataContract]
    public class QUserInfo
    {
        private string deviceId = "QL" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 8);

        [DataMember]
        public string DeviceID { get { return deviceId; } }

        [DataMember]
        public string UserName { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Domain { get; set; }

        [DataMember]
        public string UserType { get; set; }

        [DataMember]
        public List<XenAppInfo> XenAppInfo { get; set; }

        [DataMember]
        public Dictionary<string, string> ApplicationInfo { get; set; }

        [DataMember]
        public List<string> ADgroups { get; set; }

        [DataMember]
        public string UserAdTitle { get; set; }

        [DataMember]
        public string UserAdDept { get; set; }

        [DataMember]
        public QUserProfile CurrentProfile { get; set; }

        public QUserInfo() { ADgroups = new List<string>(); }

    }

    [DataContract]
    public class QUserProfile
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string ntID { get; set; }

        [DataMember]
        public string userType { get; set; }

        [DataMember]
        public string userTitle { get; set; }

        [DataMember]
        public string userDept { get; set; }

        [DataMember]
        public DateTime lastLogin { get; set; }

        [DataMember]
        public DateTime lastPortalLaunch { get; set; }

        [DataMember]
        public DateTime firstLogin { get; set; }

        [DataMember]
        public string CitrixAutoLaunchApp { get; set; }

        [DataMember]
        public string formHorizontalStartup { get; set; }

        [DataMember]
        public string formVerticalStartup { get; set; }

        [DataMember]
        public DateTime lastUpdate { get; set; }

        [DataMember]
        public double lastLoginLength { get; set; }

        [DataMember]
        public double lastRoamLength { get; set; }

        [DataMember]
        public double lastEMRLaunchLength { get; set; }

        [DataMember]
        public string lastLoginMachine { get; set; }

        [DataMember]
        public int roamingCt { get; set; }

        [DataMember]
        public int pcLaunchCt { get; set; }

        [DataMember]
        public int fnLaunchCt { get; set; }

        [DataMember]
        public int snLaunchCt { get; set; }

        [DataMember]
        public DateTime lastMetricCtClear { get; set; }

        [DataMember]
        public int RoleCt { get; set; }

        [DataMember]
        public List<QRole> Roles { get; set; }

        [DataMember]
        public double Launch_Roam_Avg { get; set; }

        [DataMember]
        public string WTSLocation { get; set; }

        [DataContract]
        public class UserMetrics
        {
            [DataMember]
            public double AvgLoginLength { get; set; }

            [DataMember]
            public double SumLoginLength { get; set; }

            [DataMember]
            public double AvgEMRLaunchLength { get; set; }

            [DataMember]
            public double SumEMRLaunchLength { get; set; }

            [DataMember]
            public double AvgRoamLength { get; set; }

            [DataMember]
            public double SumRoamLength { get; set; }

            [DataMember]
            public double AvgRoamLaunchSavings { get; set; }

            [DataMember]
            public double SumRoamLaunchSavings { get; set; }

            [DataMember]
            public int SumRoamCt { get; set; }

            [DataMember]
            public int SumPCLaunchCt { get; set; }

            [DataMember]
            public int SumFNLaunchCt { get; set; }

            [DataMember]
            public int SumSNLaunchCt { get; set; }

            [DataMember]
            public int SumAutoLaunchCt { get; set; }

            [DataMember]
            public int SumQLUserCt { get; set; }

            [DataMember]
            public int SumNewUsersCt { get; set; }

            [DataMember]
            public int SumMachineCt { get; set; }

            [DataMember]
            public DateTime LastMetricClear { get; set; }
        }

        public QUserProfile() { }
    }

    [DataContract]
    public class QVersion
    {
        [DataMember]
        public int id { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string LatestVersion { get; set; }

        [DataMember]
        public string AcceptableVersion1 { get; set; }

        [DataMember]
        public string AcceptableVersion2 { get; set; }

        [DataMember]
        public string AcceptableVersion3 { get; set; }

        [DataMember]
        public int CheckFrequency { get; set; }

        [DataMember]
        public string lup_user { get; set; }

        [DataMember]
        public DateTime lup_dt { get; set; }

        [DataMember]
        public int MachineCt { get; set; }

        public QVersion() { }
    }

    [DataContract]
    public class QWindow
    {
        [DataMember]
        public string Action { get; set; }

        [DataMember]
        public string Process { get; set; }

        [DataMember]
        public string WindowText { get; set; }

        [DataMember]
        public bool IsEverywhere { get; set; }

        [DataMember]
        public bool IsEnabled { get; set; }

        [DataMember]
        public string lup_user { get; set; }

        [DataMember]
        public List<string> Applications { get; set; }

        [DataContract]
        public class WindowDevice : BaseModel
        {
            [DataMember]
            public int WindowId { get; set; }

            [DataMember]
            public string Machine { get; set; }
        }


        public QWindow() { Applications = new List<string>(); }

    }

    [DataContract]
    public class XenAppInfo
    {
        private string _XenAppHost1;

        private string _XenAppHost2;

        private string _XenAppPort;

        private string _Farm;

        [DataMember]
        public string Farm
        {
            get
            {
                return this._Farm;
            }
            set
            {
                this._Farm = value;
            }
        }

        [DataMember]
        public string XenAppHost1
        {
            get
            {
                return this._XenAppHost1;
            }
            set
            {
                this._XenAppHost1 = value;
            }
        }

        [DataMember]
        public string XenAppHost2
        {
            get
            {
                return this._XenAppHost2;
            }
            set
            {
                this._XenAppHost2 = value;
            }
        }

        [DataMember]
        public string XenAppPort
        {
            get
            {
                return this._XenAppPort;
            }
            set
            {
                this._XenAppPort = value;
            }
        }

        public XenAppInfo()
        {
        }
    }

    [DataContract]
    public sealed class XMLEntry
    {
        [DataMember]
        public DateTime timeStamp { get; set; }

        [DataMember]
        public string eventType { get; set; }

        [DataMember]
        public string ntID { get; set; }

        [DataMember]
        public string machine { get; set; }

        [DataMember]
        public string app { get; set; }

        [DataMember]
        public string timeElapsed { get; set; }

        public XMLEntry() { }
    }



}