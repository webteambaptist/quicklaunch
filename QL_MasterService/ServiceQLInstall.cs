﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace QL_MasterService
{
    [RunInstaller(true)]
    public partial class ServiceQLInstall : System.Configuration.Install.Installer
    {
        private ServiceInstaller serviceInstaller;
        private ServiceProcessInstaller processInstaller;
        private string _ServiceName = "BH_QuickLaunch_MasterSVC";

        public ServiceQLInstall()
        {
            InitializeComponent();

            processInstaller = new ServiceProcessInstaller();
            serviceInstaller = new ServiceInstaller();

            processInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;

            serviceInstaller.StartType = ServiceStartMode.Automatic;
            serviceInstaller.ServiceName = _ServiceName;
            serviceInstaller.DisplayName = "Baptist Health QuickLaunch MasterSVC";
            serviceInstaller.Description = "Baptist Health generated service that retrieves and save logs from QL.";

            Installers.Add(serviceInstaller);
            Installers.Add(processInstaller);
        }

        #region Access parameters

        /// <summary>
        /// Return the value of the parameter in dicated by key
        /// </summary>
        /// <param name="key">Context parameter key</param>
        /// <returns>Context parameter specified by key</returns>
        public string GetContextParameter(string key)
        {
            string sValue = "";
            try
            {
                sValue = this.Context.Parameters[key].ToString();
            }
            catch
            {
                sValue = "";
            }

            return sValue;
        }

        #endregion

        #region Overrides

        protected override void OnAfterInstall(IDictionary savedState)
        {
            base.OnAfterInstall(savedState);
            try
            {
                ServiceController controller = new ServiceController(_ServiceName);
                controller.Start(); //Start Service
                controller.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromMilliseconds(10000));
            }
            catch (Exception ee)
            {
                // ServiceQLInstall.WriteToEventLog("Error Starting Base Quick Launch Auto Update Service: " + ex.ToString(), System.Diagnostics.EventLogEntryType.Error);
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error Starting Base Quick Launch Auto Update Service: " + ee.ToString(), "0");
            }
        }

        /// <summary>
        /// This method is run before the install process.
        /// This method is overriden to set the following parameters:
        /// service name (/name switch)
        /// account type (/account switch)
        /// for a user account user name (/user switch)
        /// for a user account password (/password switch)
        /// Note that when using a user account, if the user name or password is not set,
        /// the installing user is prompted for the credentials to use.
        /// </summary>
        /// <param name="savedState"></param>
        protected override void OnBeforeInstall(IDictionary savedState)
        {
            base.OnBeforeInstall(savedState);

            // Decode the command line switches
        }

        /// <summary>
        /// Modify the registry to install the new service
        /// </summary>
        /// <param name="stateServer"></param>
        public override void Install(IDictionary stateServer)
        {
            base.Install(stateServer);
        }

        /// <summary>
        /// Uninstall based on the service name
        /// </summary>
        /// <param name="savedState"></param>
        protected override void OnBeforeUninstall(IDictionary savedState)
        {
            base.OnBeforeUninstall(savedState);
        }

        /// <summary>
        /// Modify the registry to remove the service
        /// </summary>
        /// <param name="stateServer"></param>
        public override void Uninstall(IDictionary stateServer)
        {
            base.Uninstall(stateServer);
        }
        #endregion
    }
}
