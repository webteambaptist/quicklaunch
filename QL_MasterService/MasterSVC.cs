﻿using QuickLaunch.Models;
using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace QL_MasterService
{
    public partial class MasterSVC : ServiceBase
    {
        private static System.Timers.Timer timer;
        int portion = 1;

        public MasterSVC()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            retrieveLogs(); //start right away
            initTimer();   
        }

        private void initTimer()
        {
            timer = new System.Timers.Timer(3600000); //hourly
            timer.Elapsed += Timer_Elapsed;
            EventLog.WriteEntry("QuickLaunch Service Started", EventLogEntryType.Information);
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                string debug = ConfigurationManager.AppSettings["IsDebug"];


                if (!string.IsNullOrEmpty(debug))
                {
                    if (debug == "1")
                    {
                        List<int> locList = new List<int>();
                        locList.Add(1);
                        locList.Add(36);
                        retrieveLogs(locList);

                    }
                    else
                    {
                        retrieveLogs();
                    }
                }
            }
            catch (Exception ex)
            { }
           
        }

        private void retrieveLogs()
        {
            try
            {
                List<Machine> machList = Machine.GetAllActiveMachines();

                int machCount = machList.Count / 2;

                if (portion == 1) //get the first half
                {
                    machList = machList.Take(machCount).ToList();
                    portion = 2;
                }

                if (portion == 2)
                {
                    machList = machList.Skip(machCount).ToList();
                    portion = 1;
                }

                if (machList.Count > 0)
                {
                    Parallel.ForEach<Machine>(machList, _machine => Truncus.ProcessErrorLogs());
                    Parallel.ForEach<Machine>(machList, _machine => Truncus.ProcessEventLogs());
                }
            }
            catch (Exception ex) //to do add error handling
            {
                try
                {
                    XMLErrorEntry xmlEntry = new XMLErrorEntry();
                    xmlEntry.timeStamp = DateTime.Now;
                    xmlEntry.exception = ex.Message;
                    xmlEntry.qlVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    xmlEntry.serial = System.Environment.MachineName;
                    xmlEntry.LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper();
                    xmlEntry.currentUserId = "";
                    xmlEntry.isFatal = false;
                    xmlEntry.ICAVersion = "";
                    xmlEntry.IEVersion = "";
                    xmlEntry.application = "";
                    writeXMLAsyncErrorEntry(xmlEntry);
                }
                catch (Exception ee)
                {
                    EventLog.WriteEntry("QuickLaunch Service", ee.Message, EventLogEntryType.Error);
                }
            }
        }

        private void retrieveLogs(List<int> locList)
        {
            try
            {
                List<Machine> machList = new List<Machine>();

                foreach(int j in locList)
                {
                    machList.AddRange(Machine.GetMachinesByLocationID(j));
                }
//

                if (machList.Count > 0)
                {
                    Parallel.ForEach<Machine>(machList, _machine => Truncus.ProcessErrorLogs());
                    Parallel.ForEach<Machine>(machList, _machine => Truncus.ProcessEventLogs());
                }
            }
            catch (Exception ex) //to do add error handling
            {
                try
                {
                    XMLErrorEntry xmlEntry = new XMLErrorEntry();
                    xmlEntry.timeStamp = DateTime.Now;
                    xmlEntry.exception = ex.Message;
                    xmlEntry.qlVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    xmlEntry.serial = System.Environment.MachineName;
                    xmlEntry.LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper();
                    xmlEntry.currentUserId = "";
                    xmlEntry.isFatal = false;
                    xmlEntry.ICAVersion = "";
                    xmlEntry.IEVersion = "";
                    xmlEntry.application = "";
                    writeXMLAsyncErrorEntry(xmlEntry);
                }
                catch (Exception ee)
                {
                    EventLog.WriteEntry("QuickLaunch Service", ee.Message, EventLogEntryType.Error);
                }
            }
        }

        protected override void OnStop()
        {
            EventLog.WriteEntry("QuickLaunch Service Stopped", EventLogEntryType.Information);
        }

        public static void writeXMLAsyncErrorEntry(XMLErrorEntry xmlEntry)
        {
            try
            {
                Random r = new Random();

                string logFileName = Path.GetTempPath() + "QL_MasterSVC" + r.Next(99999) + DateTime.Now.ToString("yyyy-MM-dd") + ".err";

                Utility.Serialize<XMLErrorEntry>(xmlEntry, logFileName);

            }
            catch (IOException ioe)
            {
                EventLog.WriteEntry("QuickLaunch", "Error Log Write Failed: " + ioe.Message);
            }
            catch (Exception ee)
            {
                EventLog.WriteEntry("QuickLaunch", "Error Log Write Failed: " + ee.Message);
            }
        }
    }
}
