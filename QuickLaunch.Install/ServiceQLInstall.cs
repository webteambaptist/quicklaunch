﻿using QuickLaunch.Shared;
using System;
using System.Diagnostics;
using System.IO;
using System.ServiceModel;
using System.ServiceProcess;
using System.Timers;

namespace QuickLaunch.Install
{
    public partial class ServiceQLInstall : ServiceBase
    {

        #region Properties        
        private static System.Timers.Timer CheckStatusTimer = new System.Timers.Timer();
        private static System.Timers.Timer CleanLogsTimer = new System.Timers.Timer();
        private static readonly int CleanLogTimerInterval = 1000 * 60 * 24; //24 hours
        private static readonly int DefaultTimerInterval = 1000 * 60 * 60; //1 hour
        QLAutoUpdateController qlAutoUpdateController;
        public static DateTime LastRunDt;
        //OneSignEventProviderClass onesignEvents = new OneSignEventProviderClass(); //new
        
        #endregion



        #region Start/Stop Service overrides
        protected override void OnStart(string[] args)
        {
            //add events
           // Thread.Sleep(20000); //FOR DEBUG ONLY
            
                                   // onesignEvents.OnLocked += onesignEvents_OnLocked;
            Log.WriteToEventLog(string.Format("Service Started"), EventLogEntryType.Information);
            Log.writeXMLEntryLog(DateTime.Now, "Service Started", "", Environment.MachineName, "", "");
            CheckStatusTimer.AutoReset = true;
            CheckStatusTimer.Interval = 10000; //TO KICK OFF TIMER RIGHT AWAY
            CheckStatusTimer.Elapsed += new ElapsedEventHandler(CheckStatusTimer_Elapsed);
            CheckStatusTimer.Start();
            CleanLogsTimer.AutoReset = true;
            CleanLogsTimer.Interval = 10000; //so it runs on start
            CleanLogsTimer.Elapsed += CleanLogsTimer_Elapsed;
            CleanLogsTimer.Start();
            qlAutoUpdateController = new QLAutoUpdateController();
           // qlAutoUpdateController.DeleteSetupFile(); //no longer delete these, messes up uninstalls     
            StartSVC(); //start service for remote calls to client, remove from QL
        }



        private void StartSVC()
        {
            try
            {
                NetTcpBinding binding = new NetTcpBinding();
                //  string address = "net.tcp://" + System.Environment.MachineName + ".BH.LOCAL:8000/wcfserver";
                string address = "net.tcp://" + SysHelper.GetIpAddress() + ":8001/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());
                ServiceHost serviceHost = new ServiceHost(typeof(WCFServer), baseAddress);
                serviceHost.AddServiceEndpoint(typeof(IWCFServer), binding, baseAddress);
                serviceHost.Open();
                //  MessageBox.Show("started");
            }
            catch (Exception ee)
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error: " + ee.Message, "0");
            }

        }
        
        protected override void OnStop()
        {
            qlAutoUpdateController = null;
            CheckStatusTimer.Stop();
            CleanLogsTimer.Stop();
            Log.WriteToEventLog(string.Format("Service Stopped"), EventLogEntryType.Information);
            Log.writeXMLEntryLog(DateTime.Now, "Service Stopped", "", Environment.MachineName, "", "");
            base.OnStop();
        }

        #endregion

        #region Events
        /// <summary>
        /// Install check timer. At specified intervals it starts the install check process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CheckStatusTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PerformInstall(); //set to true when testing MSI functions
            SetUpdateFrequency(); 
        }

        /// <summary>
        /// clears logs older than 7 days
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CleanLogsTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            CleanLogs();
            CleanLogsTimer.Interval = CleanLogTimerInterval; //so after initial start it only runs daily
        }

        /// <summary>
        /// installs update using Imprivata events, for future release
        /// </summary>
        void onesignEvents_OnLocked()
        {
            PerformInstall();
        }


        #endregion

        #region Methods

        /// <summary>
        /// Clears EventLogs
        /// </summary>
        public static void CleanLogs(double d = 7)
        {
            bool isClean = true;

            try
            {
                string workingPath = QLAutoUpdateController.SetWorkingPath();

                if (Directory.Exists(workingPath + "\\Log\\"))
                {
                    foreach (string f in Directory.GetFiles(workingPath + "\\Log\\", "*.log"))
                    {
                        if (File.GetLastWriteTime(f) < DateTime.Now.AddDays(-1 * (d)))
                        {
                            File.Delete(f);
                        }
                    }
                }

            }
            catch (IOException ie)
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error: " + ie.Message, "0");
                isClean = false;
            }
            catch (Exception ee)
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error: " + ee.Message, "0");
                isClean = false;
            }
            finally
            {
                if(isClean)
                {
                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Logs cleaned", "0");
                }
                else
                {
                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Logs clean failed", "0");
                }
            }
        }

        /// <summary>
        /// Checks installation criteria, and starts main controller to look for Quick Launch Updates
        /// </summary>
        private void PerformInstall(bool isForced = false)//(bool isTimeElapsed = false) //to do, find most recent MSI in folder and install
        {
            try
            {
                if (LastRunDt != null)
                {
                    if (DateTime.Now.Subtract(LastRunDt) < TimeSpan.FromSeconds(45)) //min svc run interval is one minute. this is the safe # that does not affect run intervals
                    {
                        return; //when machine is restarting...service has a bad habit of starting multiple times. that's fine, as long as they're not right top on each other.
                    }
                }

                LastRunDt = DateTime.Now;

                string username = string.Empty;
                string sid = string.Empty;      
                Log.GetUserObject(ref username, ref sid);

                if (System.IO.File.Exists(QLAutoUpdateController.QuickLaunchX86Path + QLAutoUpdateController.QLFileName))
                {
                    QLAutoUpdateController.QuickLaunchPath = QLAutoUpdateController.QuickLaunchX86Path; //64bit machine. keep controller on task by adjusting its working directory
                }

                bool result = qlAutoUpdateController.Execute(username, sid, isForced); //(false); //does all the actual work of the install

                if (result)
                {
                    Log.WriteToEventLog(string.Format("{0}\n Ran from {1} to {2}\n Machine: {3}\n Published Deployment Package: {4}", (result ? "Successfully updated  Quick Launch" : "No Quick Launch updates found"), LastRunDt.ToString(), DateTime.Now.ToString(), System.Environment.MachineName.ToUpper(), qlAutoUpdateController.deployment.Title), EventLogEntryType.Information);
                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Successful Install", "0");
                }
            }
            catch (Exception ee) 
            {
                //WriteToEventLog(ex.ToString(), EventLogEntryType.Error); 
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error: " + ee.Message, "0");
            }
        }
        
        /// <summary>
        /// Checks to see how often we should be checking for updates
        /// </summary>
        private void SetUpdateFrequency() //24 hours is 86400
        {
            try
            {
                Models.Version_QL vQL = Models.Version_QL.selVersions().Find(v => v.Code == "QL");
                if (vQL.CheckFrequency > 0)
                {
                    CheckStatusTimer.Interval = 1000 * 60 * vQL.CheckFrequency; //minutes
                }
                else
                {
                    CheckStatusTimer.Interval = DefaultTimerInterval;
                }
            }
            catch (Exception ee)
            {
                //WriteToEventLog(ex.ToString(), EventLogEntryType.Error); CheckStatusTimer.Interval = DefaultTimerInterval; 
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error: " + ee.Message, "0");
                CheckStatusTimer.Interval = DefaultTimerInterval;
            }
        }


        /// <summary>
        /// Checks to see how often we should be checking for updates
        /// </summary>
        public string SetUpdateFrequency(string freq)
        {
            try
            {
                Models.Version_QL vQL = Models.Version_QL.selVersions().Find(v => v.Code == "QL");
                if (vQL.CheckFrequency > 0)
                {
                    CheckStatusTimer.Interval = 1000 * 60 * int.Parse(freq); //minutes
                }
                else
                {
                    CheckStatusTimer.Interval = DefaultTimerInterval;
                }
            }
            catch (Exception ee)
            {
                //WriteToEventLog(ex.ToString(), EventLogEntryType.Error); CheckStatusTimer.Interval = DefaultTimerInterval; 
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error: " + ee.Message, "0");
                CheckStatusTimer.Interval = DefaultTimerInterval;
            }
            return CheckStatusTimer.Interval.ToString();
        }




        #endregion

        public ServiceQLInstall()
        {
            this.CanPauseAndContinue = false;
            this.ServiceName = "ServiceQLInstall";
        }
    
    }
}
