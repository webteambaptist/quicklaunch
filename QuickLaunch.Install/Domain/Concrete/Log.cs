﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Management;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace QuickLaunch.Install
{
    public class Log
    {
        public static string WorkingPath = @"C:\Program Files (x86)\Baptist Health\QL Auto Install\Log\"; //@"C:\Progra~1\Baptis~1\QLAUTO~1\MSI\";

        /// <summary>
        /// Because the windows service runs as a system account, it doesnt have the ability to get the username or access the registry of the current user. 
        /// So this is used to find the current logged in user, and then generate it's SID which then gives us the registry folder of the user logged in.
        /// </summary>
        /// <param name="username">returns the user</param>
        /// <param name="sid">returns the current users sid</param>
        public static void GetUserObject(ref string username, ref string sid)
        {
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT UserName FROM Win32_ComputerSystem");
                username = string.Empty;
                foreach (ManagementObject mo in searcher.Get())

                {//get the first entry
                    username = mo["UserName"].ToString();
                    break;
                }

                NTAccount f = new NTAccount(username);
                SecurityIdentifier s = (SecurityIdentifier)f.Translate(typeof(SecurityIdentifier));
                sid = s.ToString();
                //WriteToEventLog(string.Format("sid: {0}", sidString), EventLogEntryType.Information);
            }
            catch (System.Runtime.InteropServices.COMException cme)
            { } //no one is logged in, do nothing
            catch (NullReferenceException nre)
            { } //no one is logged in, do nothing
            catch (Exception ex)
            {
                //WriteToEventLog(ex.ToString(), EventLogEntryType.Warning);
                Log.writeXMLEntryLog(DateTime.Now, "GetUserObject", username, Environment.MachineName, "", "");
            }
            finally //for debugging only
            {
               // WriteToEventLog(username + " " + sid, EventLogEntryType.Information);
            }
        }

        /// <summary>
        /// Writes to event log
        /// </summary>
        /// <param name="sEvent"></param>
        /// <param name="entryType"></param>
        public static void WriteToEventLog(string sEvent, EventLogEntryType entryType)
        {
            string sSource = "BH_QuickLaunchAutoUpdate";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
            }
            EventLog.WriteEntry(sSource, sEvent, entryType);

            try
            {
                if (entryType == EventLogEntryType.Error)
                {
                    Models.ErrorLog.AddErrorToLog(new Exception(sEvent), "N/A", "AUTOINSTALL", false);
                }
            }
            catch { }
        }

        /// <summary>
        /// Creates XML entry and writes it to QL Install log
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="eventType"></param>
        /// <param name="ntID"></param>
        /// <param name="machine"></param>
        /// <param name="app"></param>
        /// <param name="timeElapsed"></param>
        public static void writeXMLEntryLog(DateTime timeStamp, string eventType, string ntID, string machine, string appName, string timeElapsed)
        {
            try
            {
                XMLEntry xmlEntry = new XMLEntry();
                xmlEntry.timeStamp = timeStamp;
                xmlEntry.eventType = eventType.Trim();
                xmlEntry.ntID = ntID.Trim();
                xmlEntry.machine = machine.Trim();
                xmlEntry.app = appName.Trim();
                xmlEntry.timeElapsed = timeElapsed;
                writeXMLEntry(xmlEntry);
            }
            catch(Exception ee)
            { }
        }

        /// <summary>
        /// Writes to local QL log. File name is machine name 
        /// </summary>
        public static bool writeXMLEntry(XMLEntry xmlEntry)
        {
            bool isSaved = true;

            try
            {
                try
                {
                    if (Directory.Exists(@"C:\Program Files (x86)\"))  //is OS 64 bit?
                    {
                        WorkingPath = @"C:\Program Files (x86)\Baptist Health\QL Auto Install\Log\";
                        if (!Directory.Exists(WorkingPath))
                        {
                            Directory.CreateDirectory(WorkingPath);
                        }

                    }
                    else
                    {
                        WorkingPath = @"C:\Program Files\Baptist Health\QL Auto Install\Log\";
                        if (!Directory.Exists(WorkingPath))
                        {
                            Directory.CreateDirectory(WorkingPath);
                        }

                    }
                }
                catch(Exception ee)
                {
                   // WriteToEventLog("Directory Creation Fail", EventLogEntryType.Warning);

                }

                //string logFileName = Path.GetTempPath() + "QuickLaunch" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";

                string logFileName = WorkingPath + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
              

                if (File.Exists(logFileName))
                {
                    if (!FileInUse(logFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(logFileName);
                        xmlDoc.Element("Entries").Add(
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed)
                                    )
                                );
                        xmlDoc.Save(logFileName);
                    }
                    else
                    {
                        Thread.Sleep(5); //wait 5ms or forget it
                        XDocument xmlDoc = XDocument.Load(logFileName);
                        xmlDoc.Element("Entries").Add(
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed)
                                    )
                                );
                        xmlDoc.Save(logFileName);
                    }
                }
                else
                {
                    XDocument xmlDoc = new XDocument(
                        new XDeclaration("1.0", "utf-16", "true"),
                        new XComment("Quick Launch Log"),
                        new XElement("Entries",
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed))
                                    )
                            );
                    xmlDoc.Save(logFileName);
                }
            }
            catch (IOException ioe)
            {
                // EventLog_QL.WriteEntry("Quick Launch", "Log Write Failed" + ioe.Message, EventLogEntryType.Information);
                isSaved = false;
            }
            catch (Exception ee)
            {
                // EventLog_QL.WriteEntry("Quick Launch", "Log Write Failed" + ee.Message, EventLogEntryType.Information);
                isSaved = false;
            }

            return isSaved;
        }

        public sealed class XMLEntry
        {
            public XMLEntry() { }
            public DateTime timeStamp { get; set; }
            public string eventType { get; set; }
            public string ntID { get; set; }
            public string machine { get; set; }
            public string app { get; set; }
            public string timeElapsed { get; set; }
        }

        static bool FileInUse(string path)
        {
            bool writeable = false;

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    writeable = fs.CanWrite;
                }
            }
            catch (IOException ex)
            {
                return true;
            }

            return writeable;
        }

    }
}
