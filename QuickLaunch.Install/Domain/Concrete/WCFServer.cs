﻿using System;
using System.Diagnostics;
using System.IO;
using System.Xml.Linq;

namespace QuickLaunch.Install
{
    public class WCFServer : IWCFServer
    {
        string deploymentID = "";

        public void ClearCache() //rewritten to run with service authority
        {
            string username = string.Empty;
            string sid = string.Empty;
            Log.GetUserObject(ref username, ref sid);

            if (username.Contains("BH\\"))
            {
                username = username.Substring(3);
            }//end if


            string quicklaunch = "\\\\" + Environment.MachineName + "\\c$\\Documents and Settings\\" + username + "\\Local Settings\\Temp\\";

            string quicklaunch7 = "\\\\" + Environment.MachineName + "\\c$\\Users\\" + username + "\\AppData\\Local\\Temp\\";

            string notifier = "\\\\" + Environment.MachineName + "\\c$\\Documents and Settings\\" + username + "\\Local Settings\\Temp\\";

            string notifier7 = "\\\\" + Environment.MachineName + "\\c$\\Users\\" + username + "\\AppData\\Local\\Temp\\";

            string userPath = "";

            if (Directory.Exists(quicklaunch))
            {
                userPath = quicklaunch;
            }

            if (Directory.Exists(quicklaunch7))
            {
                userPath = quicklaunch7;
            }

            if (Directory.Exists(notifier))
            {
                userPath = notifier;
            }

            if (Directory.Exists(notifier7))
            {
                userPath = notifier7;
            }

            if (!string.IsNullOrEmpty(userPath))
            {
                DeleteCache(userPath);
            }
        }

        private static void DeleteCache(string userPath)
        {
            try
            {
                foreach (string f in Directory.GetFiles(userPath, "QL_*.xml"))
                {
                    File.Delete(f);
                }
            }
            catch (IOException ie)
            {

            }
            catch (Exception ee)
            {

            }
        }

        public void ClearLog(double days)
        {
            ServiceQLInstall.CleanLogs(days);
        }

        public void SendMessage(string message, string caption, int timeout)
        {
            AutoClosingMessageBox.Show(message, caption, timeout);
            
        }

        public void UpdateNotify(string versionNumber)
        {
            string fileName = RetrieveMSI(setMSIPath(), deploymentID);
            WriteXML(fileName);
        }

        //Retrieve MSI for install
        private string RetrieveMSI(string path, string deploymentID)
        {
            string filename = "";

            try
            {
                Models.Deployment MSI = Models.Deployment.selDeploymentById(deploymentID);
                string msiPath = path + @"Setup.QuickLaunch" + MSI.MSIBuildVersion + ".msi";
                File.WriteAllBytes(msiPath, MSI.MSIBuild);
                //ServiceQLInstall.WriteToEventLog("New MSI Downloaded", EventLogEntryType.Information);
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "New MSI Downloaded", "0");
                Models.DeploymentResponse.Upsert(System.Environment.MachineName, MSI.MSIBuildVersion);
                filename = @"Setup.QuickLaunch" + MSI.MSIBuildVersion + ".msi";
            }
            catch (Exception ee)
            { }

            return filename;
        }

        private void WriteXML(string filename)
        {
            string xmlFileName = setMSIPath() + "\\" + "latest.xml"; ; //get path of executing assembly, store in subfolder //if subfolder doesn't exist, create it
            try
            {
                if (File.Exists(xmlFileName))
                {
                    File.Delete(xmlFileName);
                }

                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "utf-16", "true"),
                    new XComment("QL Installer"),
                    new XElement("MSI", filename));
                xmlDoc.Save(xmlFileName);
            }
            catch (Exception ee)
            {
                EventLog.CreateEventSource("QuickLaunch AutoInstaller", "Application");
                EventLog.WriteEntry("QuickLaunch AutoInstaller", "Failed to create file: " + ee.Message);
            }
        }

        private string setMSIPath()
        {
            string path = "";

            try
            {

                if (Directory.Exists(Environment.GetEnvironmentVariable("%ProgramW6432%")))  //is OS 64 bit?
                {
                    path = @"C:\Progra~2\Baptis~1\QLAUTO~1\MSI\";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                }
                else
                {
                    path = @"C:\Progra~1\Baptis~1\QLAUTO~1\MSI\";
                    if (!Directory.Exists(path))
                    {
                        Directory.CreateDirectory(path);
                    }
                }

            }
            catch (Exception ee)
            {
                EventLog.WriteEntry("setMSIPath", ee.Message);
            }
            finally
            {
                EventLog.WriteEntry("setMSIPath", "Path: " + path);
            }

            return path;
        }

    }
}
