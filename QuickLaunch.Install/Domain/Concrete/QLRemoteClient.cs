﻿using QuickLaunch.Shared;
using System;
using System.Diagnostics;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Ipc;

namespace QuickLaunch.Install
{

    //old method, to be replaced with WCF soon
    class QLRemoteClient
    {
        private delegate void AsyncMethodCaller();
        private static Process QuickLaunchProc = null;

        public static bool CallQuickLaunch(ImprivataEvent iEvent){
            try
            {
                AsyncMethodCaller asyncCallerQL = new AsyncMethodCaller(FindQuickLaunch);
                IAsyncResult resultFindQL = asyncCallerQL.BeginInvoke(null, null); // Initiate the async call.                

                if (ChannelServices.RegisteredChannels.Length == 0)
                {
                    CreateChannel();
                }

                bool complete = false;
                Exception currentException = null;

                asyncCallerQL.EndInvoke(resultFindQL); //wait on Quick Launch
                if (QuickLaunchProc == null)
                {
                    return false; //QuickLaunch isn't running...
                }

                var now = DateTime.Now;
                var end = now + TimeSpan.FromSeconds(10);
                while (!complete && now < end)
                {
                    try
                    {
                        IQuickLaunch quickLaunch = (IQuickLaunch)Activator.GetObject(typeof(IQuickLaunch), "ipc://QuickLaunchUI/QuickLaunch");
                        quickLaunch.SetCurrentUser(new UserInfo { UserType = "ServiceApp" }, iEvent);
                        complete = true;
                        currentException = null;
                    }
                    catch (RemotingException rex)
                    {
                        /*NOTE: This can throw a bunch of useless remoting exceptions if the QuickLaunch.exe process
                         * has just started and isn't ready for messages. We keep a reference to the most recent of
                         * these just so we can report it should the process time out.
                         */
                        currentException = rex;
                        System.Threading.Thread.Sleep(400); //don't overload the queue, or cold startup could be unpredictable
                    }
                    catch (Exception ex)
                    {  //Any other kind of exception thrown results in failure.
                        complete = true;
                        currentException = ex;
                    }
                    now = DateTime.Now;
                }

                if (currentException != null)
                {   //There was an error notifying Quick Launch of an event.
                    try
                    {
                        if (QuickLaunchProc != null)
                        {
                            QuickLaunchProc.Kill();
                        }
                    }
                    catch (InvalidOperationException ioe)
                    {

                    } //proc has exited already

                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Failed to Notify QL of an event : " + currentException.ToString(), "0");

                    // ServiceQLInstall.WriteToEventLog("Failed to Notify QL of an event : " + currentException.ToString(), EventLogEntryType.Error);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ee)
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, "", Environment.MachineName, "Error: " + ee.Message, "0");
                // ServiceQLInstall.WriteToEventLog(ex.ToString(), EventLogEntryType.Error); 
                return false;
            }
        }


        /// <summary>
        /// Make sure QuickLaunch.exe is runnning. 
        /// </summary>
        private static void FindQuickLaunch()
        {
            Process[] pname = Process.GetProcessesByName("QuickLaunch");
            if (pname.Length > 0)
            {
                if (!pname[0].Responding)
                {
                    pname[0].Kill();
                }
                else
                {
                    QuickLaunchProc = pname[0];
                }
            }
        }

        /// <summary>
        /// Establishes client server relationship channel
        /// </summary>
        private static void CreateChannel()
        {
            BinaryServerFormatterSinkProvider serverProvider = new BinaryServerFormatterSinkProvider();
            serverProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
            BinaryServerFormatterSinkProvider clientProvider = new BinaryServerFormatterSinkProvider();
            clientProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
            System.Collections.IDictionary props = new System.Collections.Hashtable();
            //props["portName"] = "MyClient";
            props["portName"] = "ServiceAppClient";
            props["exclusiveAddressUse"] = false;
            props["authorizedGroup"] = "Everyone";

            //Instantiate our server channel.
            var channel = new IpcChannel(props, null, serverProvider);

            //IpcChannel channel = new IpcChannel("MyClient");
            ChannelServices.RegisterChannel(channel, false);
        }

        public QLRemoteClient() { }
    
    }
}
