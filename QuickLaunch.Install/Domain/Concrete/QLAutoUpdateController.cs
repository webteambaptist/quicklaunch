﻿using QuickLaunch.Shared;
using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

namespace QuickLaunch.Install
{
    public class QLAutoUpdateController
    {
        #region Properties
        public int deploymentId { get; set; }        
        public Models.Deployment deployment { get; set; }        
        public string userSID { get; set; }
        public string userName { get; set; }
        public static string WorkingPath = @"C:\Program Files (x86)\Baptist Health\QL Auto Install\MSI\"; //@"C:\Progra~1\Baptis~1\QLAUTO~1\MSI\";
        public static string QuickLaunchPath = @"C:\Program Files\Baptist Health\Quick Launch\"; //@"C:\Progra~1\Baptis~1\QuickL~1\";
        public static string QuickLaunchX86Path = @"C:\Program Files (x86)\Baptist Health\Quick Launch\"; //@"C:\Progra~2\Baptis~1\QuickL~1\";
        public static string SetupFileName = @"Setup.QuickLaunch.msi";
        public static readonly string QLFileName = @"QuickLaunch.exe";
        public static readonly string NotifyFileName = @"Notifier.exe";
        public static bool exists = true;

        #endregion

        #region Methods
        /// <summary>
        /// Main logic for getting Quick Launch Auto Updates, works if
        /// </summary>
        /// <returns>bool stating whether update was done</returns>
        public bool Execute(string username, string sid, bool IsForcedInstall)     //(bool isTimeElapsed = false)
        {
            try
            {
                userName = username;
                userSID = sid;
                string version = FindDeployments();

                if (!string.IsNullOrEmpty(version)) //Is there a deployment that relates to this machine
                {
                    DownloadQLMSI(version); //  Get and download msi (if needed) use version number to specify  
                    if (CheckImprivataEventStatus() || !IsQLRunning() || string.IsNullOrEmpty(userSID))
                    {
                        InstallQuickLaunch(version);
                        return true;
                    } //else the user is logged in and currently using QL

                    if (!exists) //no ql at all, okay to install :)
                    {
                        DownloadQLMSI(version); //  Get and download msi (if needed)             
                        InstallQuickLaunch(version);
                        return true;
                    }

                }

            }
            catch(Exception ee)
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Error: " + ee.Message, "0");
            }
            return false;            
        }


        /// <summary>
        /// Installs the msi, and then restarts QuickLaunch
        /// </summary>
        private void InstallQuickLaunch(string version)
        {
            try
            {
                //to do add version check
                if (!string.IsNullOrEmpty(userSID)) //means no LSID is logged in
                {
                    SysHelper.SetRegistrySetting(@"HKEY_USERS\" + userSID + @"\SOFTWARE\TITO", "IsInstalling", "1");
                }

                bool qlNotified = QLRemoteClient.CallQuickLaunch(ImprivataEvent.Install);

                if (!qlNotified) 
                {
                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Unable to alert Quick Launch of pending install event", "0");
                   // ServiceQLInstall.WriteToEventLog("Unable to alert Quick Launch of pending install event", EventLogEntryType.Warning);
                    KillProcesses("wfica32"); 
                } //last resort, to ensure QL starts

                KillProcesses("QuickLaunch");

                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Starting Install...", "0");

              //  ProcessStartInfo psi = new ProcessStartInfo("msiexec.exe", string.Format("/i \"{0}{1}\" /norestart", WorkingPath + "\\" + version + "\\", SetupFileName));  //start silent ui, no restart installer  

                ProcessStartInfo psi = new ProcessStartInfo("msiexec.exe", string.Format("/i \"{0}{1}\" /qn /norestart", WorkingPath + "\\" + version + "\\", SetupFileName));  //start silent ui, no restart installer                
                psi.WindowStyle = ProcessWindowStyle.Normal;
                Process p = new Process();                
                p = Process.Start(psi);
                p.WaitForExit(30000); //willing to wait 30sec

                if (p.HasExited)
                {
                    //file no longer deleted DeleteSetupFile();
                    //  ServiceQLInstall.WriteToEventLog("Quick Launch Installation Complete.", EventLogEntryType.Information); //TO DO LOG THIS

                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Quick Launch Installation Complete.", "0");
                }
            }
            catch (Exception ee) 
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Error: " + ee.Message, "0");
            }
            finally 
            { 
                if (!string.IsNullOrEmpty(userSID)) 
                { 
                    SysHelper.SetRegistrySetting(@"HKEY_USERS\" + userSID + @"\SOFTWARE\TITO", "IsInstalling", "0"); 
                } 
            }
            System.Threading.Thread.Sleep(2000);
        }

        /// <summary>
        /// Checks to see if workstation is locked and if it is, for how long
        /// </summary>
        /// <returns>bool that determines if we're ok with the current workstation status</returns>
        private bool CheckImprivataEventStatus()
        {
            try
            {
                DateTime LastLock = Convert.ToDateTime(SysHelper.GetRegistrySetting(@"HKEY_USERS\" + userSID + @"\SOFTWARE\TITO", "LastDesktopLock"));
                ImprivataEvent ie = (ImprivataEvent)Enum.Parse(typeof(ImprivataEvent), SysHelper.GetRegistrySetting(@"HKEY_USERS\" + userSID + @"\SOFTWARE\TITO", "EventStatus"));
                return (ie == ImprivataEvent.WorkstationLock && DateTime.Now.Subtract(LastLock) > TimeSpan.FromSeconds(10));
                       //|| ie == ImprivataEvent.NonOneSignLogon; //for testing only
            }
            catch (FormatException fe) 
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Error: " + "Invalid datetime in TITO Registry Keys", "0");
            } //bad datetime            
            catch (ArgumentNullException ne) 
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Error: " + "TITO Registry Keys do not exist!\n\nEither Imprivata is not enabled, LSID can't write to registry, or the computer is in incorrect profile", "0");
               // ServiceQLInstall.WriteToEventLog("TITO Registry Keys do not exist!\n\nEither Imprivata is not enabled, LSID can't write to registry, or the computer is in incorrect profile", EventLogEntryType.Error); 
            } //registry keys do not exist - Either Imprivata is not enabled, LSID can't write to registry, or computer is in incorrect profile
            catch (Exception ee) 
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Error: " + ee.Message, "0");
            } 
            return false;
        }

        /// <summary>
        /// Checks to see if Quick Launch is running
        /// </summary>
        /// <returns>bool stating the existence of QL in processes</returns>
        private bool IsQLRunning()
        {
            Process[] qlProcess = Process.GetProcessesByName("QuickLaunch");
            if (qlProcess == null)
            {
                return false;
            }
            else
            {
                return qlProcess.Length > 0;
            }
        }

        /// <summary>
        /// Gets the 2MB plus file from the database, and saves it if needed
        /// </summary>
        private void DownloadQLMSI(string version)
        {
            if (deploymentId != 0)
            {
                if (!Directory.Exists(WorkingPath + "\\" + version))
                {
                    Directory.CreateDirectory(WorkingPath + "\\" + version);
                }

                if (!File.Exists(WorkingPath + "\\" + version + "\\" + SetupFileName))
                {
                    deployment = Models.Deployment.selDeploymentById(deploymentId);
                    File.WriteAllBytes(WorkingPath + "\\" + version + "\\" + SetupFileName, deployment.MSIBuild);
                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "New MSI Downloaded " + WorkingPath + "\\" + version + "\\" + SetupFileName, "0");
                }
            }
        }

        private void KillProcesses(string process)
        {
            try
            {
                Process[] procs = Process.GetProcessesByName(process);
                if (procs == null) return; //none open
                foreach (var proc in procs)
                {
                    proc.WaitForExit(5000); //wait 5 sec                   
                    proc.CloseMainWindow();
                }

                System.Threading.Thread.Sleep(300);
                procs = Process.GetProcessesByName(process);  //confirm citrix closed. If sub windows are open the request to close the main window doesnt always work

                foreach (var proc in procs)
                {
                    proc.Kill();
                }
            }
            catch(Exception ee)
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Error: " + ee.Message, "0");
            }
        }

        /// <summary>
        /// Finds out if there are any deployments available for this particular machine
        /// </summary>
        /// <returns></returns>
        public string FindDeployments()
        {
            string versionNumber = string.Empty;

            try
            {                
                deployment = Models.Deployment.selPublishedDeployment(System.Environment.MachineName.ToUpper());
                if (deployment == null) 
                { 
                    deploymentId = 0; 
                    return ""; 
                } //no published deployments for this machine
                else
                {
                    Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Published Device Deployment Package : " + deployment.Title + "\n\nUsing path: " + QuickLaunchPath + QLFileName, "0");

                    Version vMSIDeploy = new Version(deployment.MSIBuildVersion);

                    if (File.Exists(QuickLaunchPath + QLFileName)) //32-bit
                    { 
                        //check if QL exists, and if it really needs the deployment found
                        Version vQLInstalled = AssemblyName.GetAssemblyName(QuickLaunchPath + QLFileName).Version;

                        

                        int result = vMSIDeploy.CompareTo(vQLInstalled);

                        if (result < 0) 
                        {
                            versionNumber = "";
                        }//this machine already has this deployment or better. so QL is up to date
                        else
                        {
                            versionNumber = vMSIDeploy.ToString();
                        }
                        exists = true;
                    }


                    if (File.Exists(QuickLaunchX86Path + QLFileName)) //64-bit
                    {
                        //check if QL exists, and if it really needs the deployment found
                        Version vQLInstalled = AssemblyName.GetAssemblyName(QuickLaunchX86Path + QLFileName).Version;

                       // int result = vMSIDeploy.CompareTo(vQLInstalled);

                       // if (result < 0)
                        if(vQLInstalled.ToString() == vMSIDeploy.ToString())
                        {
                            versionNumber = "";
                        }//this machine already has this deployment or better. so QL is up to date
                        else
                        {
                            versionNumber = vMSIDeploy.ToString();
                        }
                        exists = true;
                    }
                    else
                    {
                        versionNumber = vMSIDeploy.ToString(); //doesn't exist, so install it
                        exists = false;
                    }

                                            
                                        
                    deploymentId = deployment.id; //marks which one we're going to use
                }
            }
            catch (Exception ee) 
            {
                Log.writeXMLEntryLog(DateTime.Now, System.Reflection.MethodBase.GetCurrentMethod().Name, userName, Environment.MachineName, "Error: " + ee.Message, "0");
            }

            return versionNumber;
        }
        #endregion

        public QLAutoUpdateController()
        {
            deploymentId = 0;
            SetWorkingPath();
        }

        public static string SetWorkingPath()
        {
            string appPath = "";

            if (Directory.Exists(@"C:\Program Files (x86)\"))   //is OS 64 bit?
            {
                WorkingPath = @"C:\Program Files (x86)\Baptist Health\QL Auto Install\MSI\";
                appPath = @"C:\Program Files (x86)\Baptist Health\QL Auto Install\";

                if (!Directory.Exists(WorkingPath))
                {
                    Directory.CreateDirectory(WorkingPath);
                }

            }
            else
            {
                WorkingPath = @"C:\Program Files\Baptist Health\QL Auto Install\MSI\";
                appPath = @"C:\Program Files\Baptist Health\QL Auto Install\";

                if (!Directory.Exists(WorkingPath))
                {
                    Directory.CreateDirectory(WorkingPath);
                }

            }

            return appPath;
        }
    }
}
