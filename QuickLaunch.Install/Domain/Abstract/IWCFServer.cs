﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace QuickLaunch.Install
{
    [ServiceContract]
    public interface IWCFServer
    {
        [OperationContract]
        void SendMessage(string message, string caption, int timeout);

        [OperationContract]
        void ClearCache();

        [OperationContract]
        void UpdateNotify(string versionNumber); //used for deployment ID

        [OperationContract]
        void ClearLog(double days);
    }
}
