﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLaunch.Install
{
    class InstallHTA
    {
        public static string Source = @"<html>
            <head>
                <title>Baptist Health Quick Launch [{VAR SSOUSR}]</title>
                <hta:application application id='htaBHQL' applicationname='Baptist Health Quick Launch'
                    scroll='no' border='thin' singleinstance='yes' windowstate='normal' showintaskbar='no'
                    maximizebutton='no' minimizebutton='no' icon='http://www.e-baptisthealth.com/favicon.ico'
                    caption='no' sysmenu='yes'>
                    <style type='text/css'>
                        body{margin:0;padding:0;text-align:center;overflow:hidden;border-radius: 10px;font:normal .8em/1.5em Helvetica,Arial,sans-serif;color:#ffffff; background-color:#CECECE; opacity: 0.65;filter:alpha(opacity = 65);}
                        div#Container{width:500px; height:105px;border:3px solid #19526d;margin:200px;}
                        div#title{background-color:#19526d; color:#ffffff;text-align:left;vertical-align:middle;font-weight:550; font-family:Segoe UI; font-size:.95em; padding:5px 5px 5px 0px; border-bottom:1px solid #551A8B;}div#title img{margin:0 5px 0 3px;}div#title span{margin:0 0 3px 5px;color:silver;font-size:1.1em;font-weight:bold;}
                        h4{color:#19526d;font-size:1.35em;margin:0;padding:0;}
                        img{border:0;}a{color:#477fa9;text-decoration:none;}
                        div#content{background-color:#ffffff; color:Black; font-size:13pt;padding:10px; line-height:144%;}
                    </style>
            </head>
            <body id='body'>        
                <div id='Container'>
                    <div name='title' id='title' unselectable='on'>
                        <img id='favicon' src='http://www.e-baptisthealth.com/favicon.ico' align='absmiddle' width='16' />Baptist
                        Health Quick Launch
                        <span>AUTO INSTALLER</span>
                    </div>          
                    <div id='content'>{MESSAGE}</div>
                </div>" +
                "<script language=\"VBScript\" type=\"text/vbscript\"> " + Environment.NewLine +
                    "Dim oWSH,iHeight, iWidth, intHorizontal, intVertical, user, domain, sComputer " + Environment.NewLine +
                    "Sub Window_Onload " + Environment.NewLine +
                    "    user = UCase(\"AUTO INSTALLER\") " + Environment.NewLine +
                    "    Set oWSH = CreateObject(\"WScript.Shell\")  " + Environment.NewLine +
                    "    With window.Document.ParentWindow.Screen  " + Environment.NewLine +
                    "        window.resizeTo .AvailWidth, .AvailHeight " + Environment.NewLine +
                    "        window.moveTo 0,0 " + Environment.NewLine +
                    "    End With  " + Environment.NewLine +
                    "End Sub " + Environment.NewLine +
                "</script>" + Environment.NewLine +
             @"</body>
        </html>";
    }
}
