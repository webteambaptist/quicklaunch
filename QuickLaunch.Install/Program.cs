﻿using System.ServiceProcess;

namespace QuickLaunch.Install
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
			{ 
				new ServiceQLInstall() 
			};
            ServiceBase.Run(ServicesToRun);
        }
    }
}
