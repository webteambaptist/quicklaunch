﻿namespace QuickLaunch
{
    using System.ComponentModel;

    /// <summary>
    /// <para>This enum represents the applications currently available through Quick Launch.</para>
    /// <para>Long term, this should be changed to a List-like structure for more flexibility.</para>
    /// <para>The description attribute is the published application name in Citrix.</para>
    /// </summary>
    public enum CitrixApplication
    {
        /// <summary>
        /// Power Chart
        /// </summary>
        [Description("EMR 1024x768 - SSOI")]
        PowerChart_SSOI,

        /// <summary>
        /// Power Chart
        /// </summary>
        [Description("EMR 1024x768 - SP")]
        PowerChart_SP,

        /// <summary>
        /// 4.6 EMR Test
        /// </summary>
        [Description("EMR Imprivata 4-6 SSO Test")]
        PowerChart_SSO_TEST,

        /// <summary>
        /// Firstnet
        /// </summary>
        [Description("Firstnet - SSOI")]
        Firstnet_SSOI,

        /// <summary>
        /// Firstnet
        /// </summary>
        [Description("Firstnet - SP")]
        Firstnet_SP,

        /// <summary>
        /// Surginet
        /// </summary>
        [Description("Surginet - SSOI")]
        Surginet_SSOI,

        /// <summary>
        /// Surginet
        /// </summary>
        [Description("Surginet - SP")]
        Surginet_SP,

        /// <summary>
        /// Allscripts
        /// </summary>
        [Description("Allscripts")]
        Allscripts
    }
}
