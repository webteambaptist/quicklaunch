﻿namespace QuickLaunch
{
    using System.ComponentModel;

    /// <summary>
    /// <para>This enum represents the applications currently available through Quick Launch.</para>
    /// </summary>
    public enum DesktopIcon
    {
        /// <summary>
        /// Bhthin - Cerner Millenium
        /// </summary>
        [Description("Cerner Millennium")]
        CernerMillenium_BHTHIN,

        /// <summary>
        /// Baptist Health Phone Directory
        /// </summary>
        [Description("Phone Directory")]
        BH_PhoneDirectory

    }
}
