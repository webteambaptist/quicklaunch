﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLaunch.Domain.Models
{
    public class MenuTimer : System.Windows.Forms.Timer
    {
        public string direction { get; set; }
        public string picName { get; set; }

        public MenuTimer()
            : base()
        {
        }
    }
}
