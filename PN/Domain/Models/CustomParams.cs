﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLaunch.Domain.Models
{
    public class ClearParam
    {
        public int Seconds { get; set; }
        public Delegate Callback { get; set; }
    }

    public class TSDisconnectParam
    {
        public int Seconds { get; set; }
        public bool IsLogOff { get; set; }
        public object WorkCompleteResult { get; set; }
        public Delegate ClearMsgCallback { get; set; }
        public string ErrorMsg { get; set; }
        public string SuccessMsg { get; set; }
    }
}
