﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using QuickLaunch.Models;
using QuickLaunch.Shared;
using System.Reflection;
using System.IO;

namespace QuickLaunch.Domain.Models
{
    public class FishMenuItem : PictureBox
    {
        public FishMenuItem(string _name, int tabIndex, string image)
        {            
            BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            //Location = new Point(0, 0); 
            Location = new Point(11, 5);
            Anchor = AnchorStyles.Top;            
            Name = _name;            
            timer = new MenuTimer();
            //Size = new System.Drawing.Size(60, 60);
            Size = new System.Drawing.Size(38, 38);
            SizeMode = PictureBoxSizeMode.Zoom;
            Margin = new System.Windows.Forms.Padding(6, 0, 6, 2);
            Cursor = Cursors.Hand;
            TabIndex = tabIndex;
            TabStop = false;
            Image = GetImage(image, _name);
            apps = new List<MenuApplication>();
        }

        public System.Drawing.Point pictureLocation { get; set; }        
        public MenuTimer timer { get; set; }
        public string link { get; set; }        
        public MenuApplication CurrentApplication { get; set; }
        public List<MenuApplication> apps { get; set; }

        /// <summary>
        /// Sets up a label to be seen over the fish menu item picture
        /// </summary>
        /// <param name="text">Menu item text</param>
        /// <param name="location">The point location that the label should be shown</param>
        /// <returns>Polished label</returns>
        public static Label SetupItemLabelOverlay(Label lText, string text, Point location)
        {
            try
            {
                lText.Text = text;
                lText.Font = new Font("Microsoft Sans Serif", (float)7.7);
                lText.ForeColor = Color.SteelBlue;
                lText.BackColor = Color.White;
                lText.Padding = new System.Windows.Forms.Padding(0, 3, 0, 1);
                lText.Width = 70;
                lText.Size = new Size(70, 30);
                lText.TextAlign = ContentAlignment.TopCenter;
                lText.Location = location;
                lText.Cursor = Cursors.Hand;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            return lText;
        }

        public static Bitmap GetImage(string img, string name) //note: for now, the case value needs to match the option value in QLApplication.aspx really need to move this to a database
        {
            if (img == null)
            {
                img = string.Empty;
            }

            Bitmap bitmap = global::QuickLaunch.Properties.Resources.citrix;

            img = img.ToLower();
            switch (img)
            {
                case "shield":
                    bitmap = global::QuickLaunch.Properties.Resources.shield;
                    break;
                case "ambulance":
                    bitmap = global::QuickLaunch.Properties.Resources.ambulance;
                    break;
                case "drhead":
                    bitmap = global::QuickLaunch.Properties.Resources.drhead;
                    break;
                case "citrix":
                    bitmap = global::QuickLaunch.Properties.Resources.citrix;
                    break;
                case "email":
                    bitmap = global::QuickLaunch.Properties.Resources.email;
                    break;
                case "myportal":
                    bitmap = global::QuickLaunch.Properties.Resources.myPortalIcon;
                    break;
                case "intranet":
                    bitmap = global::QuickLaunch.Properties.Resources.intranetIcon;
                    break;
                case "obtv":
                    bitmap = global::QuickLaunch.Properties.Resources.OBTV;
                    break;
                case "appbar":
                    bitmap = global::QuickLaunch.Properties.Resources.appbar;
                    break;
                case "powerchart":
                    bitmap = global::QuickLaunch.Properties.Resources.powerchart;
                    break;
                case "firstnet":
                    bitmap = global::QuickLaunch.Properties.Resources.Firstnet;
                    break;
                case "surginet":
                    bitmap = global::QuickLaunch.Properties.Resources.surginet;
                    break;
                case "eclipse":
                    bitmap = global::QuickLaunch.Properties.Resources.eclipse;
                    break;
                case "ie":
                    bitmap = global::QuickLaunch.Properties.Resources.ie;
                    if (name.Trim().Contains("124"))
                    {
                        bitmap = global::QuickLaunch.Properties.Resources.cps;
                    }
                    break;
                case "insight":
                    bitmap = global::QuickLaunch.Properties.Resources.insight;
                    break;
                case "optimax":
                    bitmap = global::QuickLaunch.Properties.Resources.optimax;
                    break;
                case "reportreq":
                    bitmap = global::QuickLaunch.Properties.Resources.reportReq;
                    break;
                case "link":
                    bitmap = global::QuickLaunch.Properties.Resources.WebLink;
                    break;
                case "allscripts":
                    bitmap = global::QuickLaunch.Properties.Resources.allscripts;
                    break;
                case "nemours":
                    bitmap = global::QuickLaunch.Properties.Resources.nemours;
                    break;
                case "magview":
                    bitmap = global::QuickLaunch.Properties.Resources.magview;
                    break;
                case "anesthesia":
                    bitmap = global::QuickLaunch.Properties.Resources.Anesthesia;
                    break;
                case "cps":
                    bitmap = global::QuickLaunch.Properties.Resources.cps;
                    break;
                case "touchworks15":
                    bitmap = global::QuickLaunch.Properties.Resources.touchworks15;
                    break;
                default: //try to figure it out
                    if (img.Contains("surginet"))
                    {
                        bitmap = global::QuickLaunch.Properties.Resources.surginet;
                    }
                    else if (img.Contains("firstnet"))
                    {
                        bitmap = global::QuickLaunch.Properties.Resources.Firstnet;
                    }
                    else if (img.Contains("powerchart") || img.Contains("emr"))
                    {
                        bitmap = global::QuickLaunch.Properties.Resources.powerchart;
                    }
                    else
                    {
                        bitmap = findResource(img.ToLower().Trim());
                                                    
                    }                       
                    break;
            }

            return bitmap;
        }


        /// <summary>
        /// check filesystem for icon, if not exists, download from DB
        /// </summary>
        /// <param name="img"></param>
        /// <returns></returns>
        private static Bitmap findResource(string img)
        {
            Bitmap _bitmap = global::QuickLaunch.Properties.Resources.citrix;

            //check resource folder
            try
            {
                string path = @"C:\Program Files (x86)\Baptist Health\Quick Launch\images\";

                if (Directory.Exists(@"C:\Program Files (x86)\Baptist Health\Quick Launch\"))  //is OS 64 bit?
                {
                    if (!Directory.Exists(@"C:\Program Files (x86)\Baptist Health\Quick Launch\images"))
                    {
                        Directory.CreateDirectory(@"C:\Program Files (x86)\Baptist Health\Quick Launch\images");
                    }

                }
                else
                {
                    path = @"C:\Program Files\Baptist Health\Quick Launch\images\";

                    if (!Directory.Exists(@"C:\Program Files\Baptist Health\Quick Launch\images"))
                    {
                        Directory.CreateDirectory(@"C:\Program Files\Baptist Health\Quick Launch\images");
                    }
                }

                string[] fileList = Directory.GetFiles(path, img + "*", SearchOption.AllDirectories);

                if (fileList.Length > 0)
                {
                    Image _image = Image.FromFile(fileList[0]);
                    _bitmap = (Bitmap)_image;
                }
                else  //if not in resource folder, check DB
                {
                    MenuImage menuImage = new MenuImage();
                    menuImage = MenuImage.getFile(img);

                    if(menuImage.ImgFile != null)
                    {
                        File.WriteAllBytes(path + menuImage.ImgName, menuImage.ImgFile);
                    }
                }

               
            }
            catch (Exception ee)
            {

            }
            //else use the default
            return _bitmap;

        }

    }
}
