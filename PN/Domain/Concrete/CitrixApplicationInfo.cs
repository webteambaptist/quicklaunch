﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickLaunch
{
    public class CitrixApplicationInfo
    {
        public string ID { get; set; }
        public String AppTitle { get; set; }
        public String AppLaunchURL { get; set; }
        public String AppIcon { get; set; }
        public String AppDesc { get; set; }
    }
}
