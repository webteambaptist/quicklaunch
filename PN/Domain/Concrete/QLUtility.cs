﻿using Microsoft.Exchange.WebServices.Data;
using QuickLaunch.Models;
using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace QuickLaunch
{
    /// <summary>
    /// Utility class for QL, frequently called methods not related to the form
    /// </summary>
    public static class QLUtility
    {

        public static string logLevel = Truncus.setLogLevel();

        /// <summary>
        /// Checks to see if settings are older than set time, if so, retrieve and delete old settings
        /// </summary>
        /// <returns></returns>
        public static void refreshSettings(int time)
        {
            string xmlFileName = Utility.getPath() + "QL_Settings.xml";
            try
            {
                if (File.Exists(xmlFileName))
                {
                    if (File.GetLastWriteTime(xmlFileName) < DateTime.Now.AddHours(time)) //if the file is older than set time
                    {
                        try
                        {
                            Utility.writeSettingsCache();
                        }
                        catch (Exception ee)
                        {
                            Program.LogException(ee, false);
                        }
                    }   //there is no else, if less than set time              
                }
                else //no file, write a new one, same method call
                {
                    Utility.writeSettingsCache();
                }
            }
            catch(Exception ee)
            {
                Program.LogException(ee, false);
            }
        }

        public static void refreshSettings()
        {
            string xmlFileName = Utility.getPath() + "QL_Settings.xml";
            try
            {
                if (File.Exists(xmlFileName))
                {
                    File.Delete(xmlFileName);

                    try
                    {
                        Utility.writeSettingsCache();
                    }
                    catch (Exception ee)
                    {
                        Program.LogException(ee, false);
                    }
            
                }
                else //no file, write a new one, same method call
                {
                    Utility.writeSettingsCache();
                }
            }
            catch (Exception ee)
            {
                Program.LogException(ee, false);
            }
        }

        /// <summary>
        /// check at rando intervals to see if there's an immediate call for cache flush
        /// </summary>
        /// <returns></returns>
        internal static bool SetandCheckCache()
        {
            bool toPurge = false;
            Random r = new Random();
            refreshSettings(-1*(r.Next(1, 4)));
            string purgeEval = Utility.getSettingFromCache("PurgeCache");
            try
            {
                DateTime purgeDT = DateTime.Parse(purgeEval);
                if(purgeDT < DateTime.Now)
                {
                    toPurge = true;
                }
            }
            catch(Exception ee)
            {
                toPurge = false;
            }
           
            return toPurge;
        }


        /// <summary>
        /// delete local logs after 7 days or longer/shorter
        /// </summary>
        internal static void ShrinkLogs()
        {
            string days = Utility.getSettingFromCache("PurgeLog");
            if (string.IsNullOrEmpty(days))
            {
                days = "7";
            }

            double d = 7;

            try
            {
                d = double.Parse(days);
            }
            catch(Exception ee)
            {
                d = 7; //something went wrong, just use 7
            }

            try
            {
                foreach (string f in Directory.GetFiles(Utility.getPath(), "QuickLaunch*.log"))
                {
                    if (File.GetLastWriteTime(f) < DateTime.Now.AddDays(-1 * (d)))
                    {
                        File.Delete(f);
                    }
                }

                foreach (string f in Directory.GetFiles(Utility.getPath(), "Notifier*.log"))
                {
                    if (File.GetLastWriteTime(f) < DateTime.Now.AddDays(-1 * (d)))
                    {
                        File.Delete(f);
                    }
                }

            }
            catch (IOException ie)
            {

            }
            catch (Exception ee)
            {

            }


        }

        /// <summary>
        /// Updates (or Inserts to) the machine inventory
        /// </summary>
        public static void UpdateMachineInventory(UserInfo CurrentUser)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {

                string ram = string.Empty;
                StringBuilder sbComputer = new StringBuilder();
                try
                {
                    System.Collections.ArrayList al = Shared.SysHelper.GetWMIAttributes("Win32_ComputerSystem", string.Empty);
                    foreach (string s in al.ToArray(typeof(string)))
                    {
                        sbComputer.AppendLine(s);
                        if (s.StartsWith("TotalPhysicalMemory = "))
                            ram = s.Substring("TotalPhysicalMemory = ".Length);
                    }
                }
                catch { }

                StringBuilder sbOS = new StringBuilder();
                try
                {
                    System.Collections.ArrayList al2 = Shared.SysHelper.GetWMIAttributes("Win32_OperatingSystem", "where Primary='true'");
                    foreach (string s in al2.ToArray(typeof(string)))
                        sbOS.AppendLine(s);
                }
                catch { }

                Models.Machine.insMachine(new Models.Machine
                {
                    PCName = System.Environment.MachineName,
                    LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper(),
                    QLVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    ICAVersion = Models.Machine.GetIcaVersion(),
                    IEVersion = Models.Machine.GetIEVersion(),
                    ComputerInfo = sbComputer.ToString(),
                    OSInfo = sbOS.ToString(),
                    Processes = Shared.SysHelper.GetSystemProcesses().ToString(),
                    Memory = ram,
                    IpAddress = SysHelper.GetIpAddress(),
                    AutoInstallVersion = Models.Machine.GetAutoInstallVersion(),
                    LastLogin = DateTime.Now
                });
            }
            catch (Exception ex) 
            { //Program.LogException(ex, false); 
                Truncus.LogEvents(logLevel, "", CurrentUser.UserName, 0, "Update Machine Inventory: "  + ex.Message);
            }

            sw.Stop();

            try
            {
                Truncus.LogEvents(logLevel, "", CurrentUser.UserName, sw.ElapsedMilliseconds, "Update Machine Inventory");
            }
            catch (Exception ee)
            { }

           // EventLog.WriteEntry("machineinventory", "");

            SetMachineUpdateToken();

        }

        private static void SetMachineUpdateToken()
        {
            try
            {
                string build = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                string xmlFileName = Utility.getPath() + "QL_MachineCache.xml";
                if (File.Exists(xmlFileName))
                {
                    File.Delete(xmlFileName);
                }

                XDocument xmlDoc = new XDocument(
                           new XDeclaration("1.0", "utf-16", "true"),
                           new XComment("Update Machine token for QL"),
                           new XElement("Machine"));

                xmlDoc.Save(xmlFileName);
            }
            catch (Exception ee)
            {

            }
        }

        internal static void checkQueue(UserInfo user, ImprivataEvent imprivataEvent)
        {
            Models.AppQueue queue = new Models.AppQueue();
            queue.LogQueueEvent(user, imprivataEvent);
            List<Models.AppQueue> qList = queue.selChildren(user.UserName);
            List<string> machList = new List<string>();
            string pcname = System.Environment.MachineName.ToLower();

            foreach(Models.AppQueue q in qList)
            {
                if ((q.impStatus.ToLower() == "userlogon") && (q.machine.ToLower() != pcname))
                {
                    machList.Add(q.machine);
                }

                if ((q.impStatus.ToLower() == "workstationunlock") && (q.machine.ToLower() != pcname))
                {
                    machList.Add(q.machine);
                }

                if ((q.impStatus.ToLower() == "nononesignlogon") && (q.machine.ToLower() != pcname))
                {
                    machList.Add(q.machine);
                }
            }

            //sendLock(user.UserName, machList);
        }

        public static void deleteApps(bool deleteall) //flush all cache files
        {
            bool isDelete = true;

            if (deleteall) //just an extra check
            {
                try
                {
                    foreach (string f in Directory.GetFiles(Utility.getPath(), "QL_*.xml"))
                    {
                        File.Delete(f);
                    }

                }
                catch (IOException ie)
                {
                    isDelete = false;
                }
                catch (Exception ee)
                {
                    isDelete = false;
                }
                finally
                {
                    if(isDelete)
                    {
                        Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "Cache deleted");
                    }
                    else
                    {
                        Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "Cache delete failed");
                    }
                }
            }
        }


        /// <summary>
        /// checks the freshness of caches, update if old
        /// </summary>
        internal static void UpdateAllCaches(UserInfo currentUser)
        {
            //specify each machine level cache individually
            //for Q4 and variability
            List<string> PcCacheList = new List<string>{
                "QL_KillList.xml",
                "QL_MachineCache.xml",
                "QL_RegistryCache.xml",
                "QL_Settings.xml",
                "QL_VCache.xml"
            };

            foreach (string s in PcCacheList)
            {
                refreshPcCache(s, currentUser);
            }

        }


        /// <summary>
        /// handles each individual cache update for machine level cache
        /// </summary>
        /// <param name="cacheFile"></param>
        private static void refreshPcCache(string fileName, UserInfo currentUser)
        {
            //check freshness first
            try
            {
                string xmlFileName = Utility.getPath() + fileName;

                if (File.Exists(xmlFileName))
                {
                        //machine level cache

                       if (fileName.ToLower().Trim() == "ql_killlist.xml")
                        {
                            if (Utility.checkCache(xmlFileName, "QL_KILLLIST"))
                            {
                                Models.KillList.refreshKillList();
                            }
                        }

                        if (fileName.ToLower().Trim() == "ql_machinecache.xml")
                        {
                            if (Utility.checkCache(xmlFileName, "QL_MACHINECACHE"))
                            {
                                UpdateMachineInventory(currentUser);
                            }
                        }

                        if (fileName.ToLower().Trim() == "ql_registrycache.xml")
                        {
                            if (Utility.checkCache(xmlFileName, "QL_REGISTRYCACHE"))
                            {
                                SetPrinterClearToken();
                            }
                        }

                        if (fileName.ToLower().Trim() == "ql_settings.xml")
                        {
                            if (Utility.checkCache(xmlFileName, "QL_SETTINGS"))
                            {
                                refreshSettings();
                            }
                        }

                        if (fileName.ToLower().Trim() == "ql_vcache.xml")
                        {
                            if (Utility.checkCache(xmlFileName, "QL_VCACHE"))
                            {
                                Version_QL.selVersions();
                            }
                        }
                    
                }
                else //if it's missing, create it
                {
                    if (fileName.ToLower().Trim() == "ql_killlist.xml")
                    {
                        Models.KillList.refreshKillList();
                    }

                    if (fileName.ToLower().Trim() == "ql_machinecache.xml")
                    {
                        UpdateMachineInventory(currentUser);
                    }

                    if (fileName.ToLower().Trim() == "ql_settings.xml")
                    {
                        refreshSettings();
                    }

                    if (fileName.ToLower().Trim() == "ql_vcache.xml")
                    {
                        Version_QL.selVersions();
                    }

                    if (fileName.ToLower().Trim() == "ql_registrycache.xml")
                    {
                          SetPrinterClearToken();
                    }
                }
            }
            catch(Exception ee)
            {
                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, ee.Message);
            }
        }

        /// <summary>
        /// log workstation lock event when done without tap
        /// </summary>
        /// <param name="user"></param>
        /// <param name="impstatus"></param>
        internal static void checkQueue(string user, string impstatus)
        {
            Models.AppQueue queue = new Models.AppQueue();
            queue.LogQueueEvent(user, impstatus);
        }

        /// <summary>
        /// client method to send lock command
        /// </summary>
        /// <param name="userName"></param>
        private static void sendLock(string userName, List<string> machList)
        {
            foreach (string machine in machList)
            {
                var addresslist = Dns.GetHostAddresses(machine + ".bh.local");
                // var addresslist = Dns.GetHostAddresses("5vmmng1.bh.local");
                NetTcpBinding binding = new NetTcpBinding();
                // string address = "net.tcp://" + System.Environment.MachineName + ".BH.LOCAL:8000/wcfserver";
                IPAddress ipa = (from i in addresslist where i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork select i).First();
                string address = "net.tcp://" + ipa.ToString() + ":8000/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());

                //client

                EndpointAddress endAddress = new EndpointAddress(baseAddress.ToString().ToLower());
                ChannelFactory<IWCFServer> channelFactory =
                    new ChannelFactory<IWCFServer>(binding, endAddress);
                IWCFServer server = channelFactory.CreateChannel();
                try
                {
                    server.LockMachine(userName);
                }
                catch (Exception ee)
                {

                }
            }
        }

        /// <summary>
        /// Kills the IE cookies.
        /// </summary>
        public static void KillIECookies(string userName)
        {
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();

                //TODO: Replace this method with safer code
                //Such as http://www.codeproject.com/Articles/15319/A-Cleanup-API-for-Windows#cookies%5Fcleanup
                ProcessStartInfo pi = new ProcessStartInfo();
                pi.WindowStyle = ProcessWindowStyle.Hidden;
                pi.CreateNoWindow = false;
                pi.FileName = "RunDll32.exe";
                pi.Arguments = "InetCpl.cpl,ClearMyTracksByProcess 2";
                pi.UseShellExecute = false;
                try
                {
                    var p = Process.Start(pi);
                    //NOTE: Don't EVER wait for this to exit - it will cause the UI to hang randomly.
                    //p.WaitForExit();
                }
                catch { }
              //  IECache.ClearIECache();



                sw.Stop();
                try
                {
                    Truncus.LogEvents(logLevel, "", userName, sw.ElapsedMilliseconds, "Kill IE Cookies");
                }
                catch (Exception ee)
                { }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

        }
                  

        /// <summary>
        /// Delete printer registry settings and restart Citrix
        /// </summary>
        internal static void ClearPrinters()
        {
            List<string> kList = new List<string>();
            //kList.Add("wfcrun32");
            kList.Add("wfica32");

            foreach (string s in kList)
            {
                Process[] pname = Process.GetProcessesByName(s);
                foreach (var proc in pname)
                { //NOTE: Added try w/ empty catch because the kill can fail and that is OK.
                    try
                    {
                        proc.Kill();
                    }
                    catch (Exception ee)
                    {

                    }
                }
            }

            //HKCU\Software\Citrix\PrinterProperties
            Stopwatch swatch = new Stopwatch();
            swatch.Start();
            int keycount = SysHelper.DeleteRegistrySetting(); //HKCU\Software\Citrix\PrinterProperties
            swatch.Stop();

            try
            {
                Truncus.LogEvents(logLevel, "Key Count: " + keycount.ToString(), "", swatch.ElapsedMilliseconds, "ClearPrinters");
            }
            catch (Exception ee)
            {

            }

            if (keycount > -1)
            {
                SetPrinterClearToken();
            }
        }

        private static void SetPrinterClearToken()
        {
            try
            {
                string xmlFileName = Utility.getPath() + "QL_RegistryCache.xml";
                if (File.Exists(xmlFileName))
                {
                    File.Delete(xmlFileName);
                }

                XDocument xmlDoc = new XDocument(
                           new XDeclaration("1.0", "utf-16", "true"),
                           new XComment("Clear Printer token for QL"),
                           new XElement("Token"));
                xmlDoc.Save(xmlFileName);
            }
            catch (Exception ee)
            {

            }
        }

        /// <summary>
        /// Uses EWS to get count of user's current unread emails
        /// </summary>
        /// <param name="currentUser"></param>
        /// <returns></returns>
        internal static int GetEmailCount(UserInfo currentUser)
        {
            int count = 0;
            try
            {
                ExchangeService exService = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
                exService.UseDefaultCredentials = false;
                exService.Credentials = new WebCredentials(currentUser.UserName, currentUser.Password, "BH");
                exService.AutodiscoverUrl(currentUser.UserName + "@bmcjax.com", redirect => true);
                count = Folder.Bind(exService, WellKnownFolderName.Inbox).UnreadCount;
            }
            catch(Exception ee)
            {
                count = 0;    
            }

            return count;
        }

        internal static ExchangeService CreateEWS(UserInfo currentUser)
        {

            ExchangeService exService = new ExchangeService(ExchangeVersion.Exchange2010_SP2);
            try
            {
                Stopwatch sw = new Stopwatch();
                sw.Start();
                exService.UseDefaultCredentials = false;
                exService.Credentials = new WebCredentials(currentUser.UserName, currentUser.Password, "BH");
                exService.AutodiscoverUrl(currentUser.UserName + "@bmcjax.com", redirect => true);
                sw.Stop();
                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, currentUser.UserName, sw.ElapsedMilliseconds, "CreateEWS");
            }
            catch(Exception ee)
            {
                //Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, currentUser.UserName, 0, "ERROR: " + ee.Message);
            }

            return exService;
        }

        internal static void dragonCleaner()
        {

            try
            {
                string dragonspath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

                //C:\Users\tomtest2\AppData\Roaming\Nuance\NaturallySpeaking12\Cache\tomtest2_456ddf30-a4b5-476c-b58b-62dfdb5f6c40

                if (Directory.Exists(dragonspath + "\\Nuance\\NaturallySpeaking12\\Cache\\")) ;
                {
                    foreach (string f in Directory.GetDirectories(dragonspath + "\\Nuance\\NaturallySpeaking12\\Cache\\"))
                    {
                        Directory.Delete(f, true);
                    }
                }

            }
            catch (Exception ex)
            {
                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "Dragon: " + ex.Message);
            }
            finally
            {
                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "Dragon Cache deleted");
            }

        }

        [Obsolete]
        public static void StopISXLock(string username)
        {
            bool iskilled = true;

            Process[] pname = Process.GetProcessesByName("ISXLockOnIdleProcessExit");
            foreach (var proc in pname)
            {
                try
                {
                    proc.Kill();

                }
                catch (Exception ee)
                {
                    iskilled = false;
                }

                if (iskilled)
                {
                    Truncus.LogEvents("1", "", username, 0, "STOP ISXLOCK");
                }
            }
        }

        /// <summary>
        /// checks to see if current QL user is also the same as the PC user, if so, log it
        /// </summary>
        /// <param name="currentUser"></param>

        public static void LSIDCheck(UserInfo currentUser)
        {
            try
            {
               if(Environment.UserName.ToLower() == currentUser.UserName.ToLower())
                {
                    Truncus.insEventLog("LSID USER", currentUser.UserName, Environment.MachineName, "", 0, "");
                }
            }
            catch (Exception ex)
            { }
        }

        [Obsolete]
        public static void StartISXLock(string username)
        {
            bool isStart = true;

            try
            {
                if (Directory.Exists(Environment.GetEnvironmentVariable("%ProgramW6432%")))  //is OS 64 bit?
                {
                    if (File.Exists(@"C:\Program Files (x86)\Imprivata\OneSign Agent\ISXLockOnIdleProcessExit.exe"))
                    {
                        ProcessStartInfo pi = new ProcessStartInfo();
                        pi.WindowStyle = ProcessWindowStyle.Hidden;
                        pi.CreateNoWindow = false;
                        pi.FileName = @"C:\Program Files (x86)\Imprivata\OneSign Agent\ISXLockOnIdleProcessExit.exe";
                        pi.Arguments = @"/l wfica 5";
                        pi.UseShellExecute = false;
                        var p = Process.Start(pi);
                    }
                }
                else
                {
                    ProcessStartInfo pi = new ProcessStartInfo();
                    pi.WindowStyle = ProcessWindowStyle.Hidden;
                    pi.CreateNoWindow = false;
                    pi.FileName = @"C:\Program Files\Imprivata\OneSign Agent\ISXLockOnIdleProcessExit.exe";
                    pi.Arguments = @"/l wfica 5";
                    pi.UseShellExecute = false;
                    var p = Process.Start(pi);
                    //NOTE: Don't EVER wait for this to exit - it will cause the UI to hang randomly.
                }
            }
            catch (Exception ee)
            {
                isStart = false;
            }

            if (isStart)
            {
                Truncus.LogEvents("1", "", username, 0, "START ISXLOCK");
            }
        }

    }

}
