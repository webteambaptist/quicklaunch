﻿using global::QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuickLaunch
{
    /// <summary>
    /// Citrix related methods
    /// </summary>
    public class CitrixMethods
    {
        private static string logLevel = "1";

        public CitrixMethods() 
        {
            try
            {
                logLevel = Utility.getSettingFromCache("LogLevel");
            }
            catch (Exception ee)
            {
                logLevel = "1";
            }        
        }

        #region events
        internal static void ica_OnACRReconnected()
        {
            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnACRReconnectFailed");
            }
        }

        internal static void ica_OnACRReconnectFailed()
        {
            //Debug.WriteLine("ica_OnACRReconnectFailed");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnACRReconnectFailed");
            }
        }

        internal static void ica_OnCGPDisconnect()
        {
            //Debug.WriteLine("ica_OnCGPDisconnect");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnCGPDisconnect");
            }
        }

        internal static void ica_OnCGPReconnect()
        {
            //Debug.WriteLine("ica_OnCGPReconnect");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnCGPReconnect");
            }
        }

        internal static void ica_OnCGPUnwarn()
        {
            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnCGPUnwarn");
            }
        }

        internal static void ica_OnCGPWarn()
        {
            // Debug.WriteLine("ica_OnCGPWarn");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnCGPWarn");
            }
        }

        internal static void ica_OnChannelDataReceived(string ChannelName)
        {
            //Debug.WriteLine(string.Format("ica_OnChannelDataReceived: {0}", ChannelName));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnChannelDataReceived");
            }
        }

        internal static void ica_OnWindowUndocked()
        {
            // Debug.WriteLine("ica_OnWindowUndocked");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowUndocked");
            }

        }

        internal static void ica_OnWindowSized(int WndType, int Width, int Height)
        {
            // Debug.WriteLine(string.Format("ica_OnWindowSized: ({0}), ({1}), ({2})", WndType, Width, Height));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowSized");
            }
        }

        internal static void ica_OnWindowRestored()
        {
            //Debug.WriteLine("ica_OnWindowRestored");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowRestored");
            }
        }

        internal static void ica_OnWindowMoved(int WndType, int XPos, int YPos)
        {
            //Debug.WriteLine(string.Format("ica_OnWindowMoved: ({0}), ({1}), ({2})", WndType, XPos, YPos));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowMoved");
            }
        }

        internal static void ica_OnWindowMinimized()
        {
            //Debug.WriteLine("ica_OnWindowMinimized");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowMinimized");
            }
        }

        internal static void ica_OnWindowMaximized()
        {
            //Debug.WriteLine("ica_OnWindowMaximized");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowMaximized");
            }
        }

        internal static void ica_OnWindowHidden(int WndType)
        {
            //Debug.WriteLine("ica_OnWindowHidden");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowHidden");
            }
        }

        internal static void ica_OnWindowFullscreened()
        {
            //Debug.WriteLine("ica_OnWindowFullscreened");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowFullscreened");
            }
        }

        internal static void ica_OnWindowDocked()
        {
            //Debug.WriteLine("ica_OnWindowDocked");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowDocked");
            }
        }

        internal static void ica_OnWindowCreated(int WndType, int XPos, int YPos, int Width, int Height)
        {
            // Debug.WriteLine(string.Format("ica_OnWindowCreated: ({0}), ({1}), ({2})", WndType, XPos, YPos, Width, Height));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowCreated");
            }
        }

        internal static void ica_OnWindowCloseRequest()
        {
            // Debug.WriteLine("ica_OnWindowCloseRequest");
            
            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowCloseRequest");
            }
        }

        internal static void ica_OnReadyStateChange(int lReadyState)
        {
            //Debug.WriteLine(string.Format("ica_OnReadyStateChange: ({0})", lReadyState));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnReadyStateChange");
            }
        }

        internal static void ica_OnPublishedAppFailed()
        {
            //Debug.WriteLine("ica_OnPublishedAppFailed");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnPublishedAppFailed");
            }
        }



        internal static void ica_OnLogoffSessionsFailed(int hCommand)
        {
            //Debug.WriteLine(string.Format("ica_OnLogoffSessionsFailed: ({0})", hCommand));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnLogoffSessionsFailed");
            }
        }

        internal static void ica_OnLogoffSessions(int hCommand)
        {
            // Debug.WriteLine(string.Format("ica_OnLogoffSessions: ({0})", hCommand));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnLogoffSessions");
            }
        }

        internal static void ica_OnInitialProp()
        {
            // Debug.WriteLine("ica_OnInitialProp");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnInitialProp");
            }
        }

        internal static void ica_OnWindowDisplayed(int WndType)
        {
            // Debug.WriteLine(string.Format("ica_OnWindowDisplayed: {0}", WndType));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnWindowDisplayed");
            }
        }

        internal static void ica_OnSessionDetach(int hSession)
        {
            // Debug.WriteLine("ica_OnSessionDetach");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnSessionDetach");
            }
        }
        internal static void ica_OnSessionAttach(int hSession)
        {
            // Debug.WriteLine("ica_OnSessionAttach");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnSessionAttach");
            }
        }

        internal static void ica_OnLogoffFailed()
        {
            // Debug.WriteLine("ica_OnLogoffFailed");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnLogoffFailed");
            }
        }

        internal static void ica_OnDisconnectSessionsFailed(int hCommand)
        {
            // Debug.WriteLine(string.Format("ica_OnDisconnectSessionsFailed: {0}", hCommand));
            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnDisconnectSessionsFailed");
            }
        }

        internal static void ica_OnDisconnectSessions(int hCommand)
        {
            //  Debug.WriteLine(string.Format("ica_OnDisconnectSessions: {0}", hCommand));

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnDisconnectSessions");
            }
        }

        internal static void ica_OnDisconnectFailed()
        {
            // Debug.WriteLine("ica_OnDisconnectFailed");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnDisconnectFailed");
            }
        }

        #endregion
    }
}
