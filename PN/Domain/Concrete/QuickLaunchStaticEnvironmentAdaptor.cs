﻿namespace QuickLaunch
{
    using System.Collections.Generic;
    using com.citrix.wing;
    using com.citrix.wing.util;
    using java.util;

    /// <summary>
    /// This is required for the ICA client.
    /// </summary>
    public class QuickLaunchStaticEnvironmentAdaptor : StaticEnvironmentAdaptor
    {
        private readonly Logger logger;
        Dictionary<string, object> attributes = new Dictionary<string, object>();

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickLaunchStaticEnvironmentAdaptor"/> class.
        /// </summary>
        /// <param name="logger">The logger.</param>
        public QuickLaunchStaticEnvironmentAdaptor(Logger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// Gets the application attribute.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns></returns>
        public object getApplicationAttribute(string name) { return attributes.ContainsKey(name) ? attributes[name] : null; }

        /// <summary>
        /// Sets the application attribute.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public void setApplicationAttribute(string name, object value) { attributes[name] = value; }

        /// <summary>
        /// Gets the diagnostic logger.
        /// </summary>
        /// <returns></returns>
        public Logger getDiagnosticLogger() { return logger; }

        /// <summary>
        /// Gets the configuration value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        public string getConfigurationValue(string key) { return null; }

        /// <summary>
        /// Gets the resource bundle factory.
        /// </summary>
        /// <param name="bundleName">Name of the bundle.</param>
        /// <returns></returns>
        public ResourceBundleFactory getResourceBundleFactory(string bundleName) { return null; }

        /// <summary>
        /// Gets the system locale.
        /// </summary>
        /// <returns></returns>
        public Locale getSystemLocale() { return null; }

        /// <summary>
        /// Overrides the system locale.
        /// </summary>
        /// <param name="locale">The locale.</param>
        public void overrideSystemLocale(Locale locale) { }

        /// <summary>
        /// Lists the resource path.
        /// </summary>
        /// <param name="root">The root.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public string[] listResourcePath(string root, string path) { return null; }

        /// <summary>
        /// Gets the component vendor.
        /// </summary>
        /// <returns></returns>
        public string getComponentVendor() { return "Baptist Health"; }

        /// <summary>
        /// Gets the component description.
        /// </summary>
        /// <returns></returns>
        public string getComponentDescription() { return "Quick Launch"; }

        /// <summary>
        /// Gets the component version major.
        /// </summary>
        /// <returns></returns>
        public int getComponentVersionMajor() { return 1; }

        /// <summary>
        /// Gets the component version minor.
        /// </summary>
        /// <returns></returns>
        public int getComponentVersionMinor() { return 0; }

        /// <summary>
        /// Gets the component revision.
        /// </summary>
        /// <returns></returns>
        public long getComponentRevision() { return 0; }

        /// <summary>
        /// Gets the child components.
        /// </summary>
        /// <returns></returns>
        public com.citrix.wing.Component[] getChildComponents() { return null; }

        /// <summary>
        /// Gets the resource stream.
        /// </summary>
        /// <param name="root">The root.</param>
        /// <param name="path">The path.</param>
        /// <returns></returns>
        public java.io.InputStream getResourceStream(string root, string path) { return null; }
    }
}