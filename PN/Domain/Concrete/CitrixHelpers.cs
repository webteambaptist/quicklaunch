﻿using Newtonsoft.Json.Linq;
using QuickLaunch.Shared;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace QuickLaunch
{
    public static class CitrixHelpers
    {
        static string baseSFURL = "bhtctxsfs01v//citrix/BaptistTestWeb"; //TO DO, get from DB

        public static Dictionary<string, string> AuthenticateWithPost(string Username, string Password, string Domain, bool UseSSL)
        {
            Dictionary<string, string> _returnValues = new Dictionary<string, string>();
            string _csrfToken = Guid.NewGuid().ToString();
            string _aspnetSessionID = Guid.NewGuid().ToString();
            string _username = Username;
            string _password = Password;
            string _domain = Domain;
            string SFURL = null;

            string _authenticationBody = string.Format("username={0}\\{1}&password={2}", _domain, _username, _password);

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }

            RestClient _rc = new RestClient(SFURL);
            RestRequest _authReq = new RestRequest("/PostCredentialsAuth/Login", Method.POST);
            if (UseSSL)
            {
                _authReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _authReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }
            _authReq.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            _authReq.AddHeader("Csrf-Token", _csrfToken);
            _authReq.AddCookie("csrtoken", _csrfToken);
            _authReq.AddCookie("asp.net_sessionid", _aspnetSessionID);
            _authReq.AddParameter("application/x-www-form-urlencoded", _authenticationBody, ParameterType.RequestBody);

            try
            {
                RestSharp.IRestResponse _authResp = _rc.Execute(_authReq);

                foreach (var header in _authResp.Headers.Where(i => i.Name == "Set-Cookie"))
                {
                    Console.WriteLine(header.Name);
                    string[] cookieValues = header.Value.ToString().Split(',');
                    foreach (string cookieValue in cookieValues)
                    {
                        string[] cookieElements = cookieValue.Split(';');
                        string[] keyValueElements = cookieElements[0].Split('=');
                        _returnValues.Add(keyValueElements[0], keyValueElements[1]);
                    }
                }

                foreach (var cookie in _authResp.Cookies)
                {
                    switch (cookie.Name.ToLower())
                    {
                        case "ctxsauthid":
                            Truncus.LogEvents("1", cookie.Name.ToLower() + " " + cookie.Value, Username, 0, "Authenticate");
                            _returnValues.Add(cookie.Name.ToLower(), cookie.Value);
                            break;
                        case "csrftoken":
                            Truncus.LogEvents("1", cookie.Name.ToLower() + " " + cookie.Value, Username, 0, "Authenticate");
                            _returnValues.Add(cookie.Name.ToLower(), cookie.Value);
                            break;
                        case "asp.net_sessionid":
                            Truncus.LogEvents("1", cookie.Name.ToLower() + " " + cookie.Value, Username, 0, "Authenticate");
                            _returnValues.Add(cookie.Name.ToLower(), cookie.Value);
                            break;
                    }

                }

            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }



            return _returnValues;
        }

        public static List<CitrixApplicationInfo> GetResources(string SessionID, string AuthID, string CsrfToken, bool UseSSL)
        {
            string SFURL = null;

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }


            List<CitrixApplicationInfo> _applicationList = new List<CitrixApplicationInfo>();
            RestClient _rc = new RestClient(SFURL);
            RestRequest _getResourcesReq = new RestRequest(@"Resources/List", Method.POST);
            if (UseSSL)
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }
            _getResourcesReq.AddHeader("Accept", "application/json");
            _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
            _getResourcesReq.AddCookie("csrftoken", CsrfToken);
            _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
            _getResourcesReq.AddCookie("CtxsAuthId", AuthID);
            //_getResourcesReq.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

            string json = _resourceListResp.Content;

            JObject a = JObject.Parse(json);
            JArray resources = (JArray)a["resources"];

            foreach (var o in resources)
            {
                CitrixApplicationInfo r = new CitrixApplicationInfo();
                r.AppTitle = o["name"].ToString();
                try
                {
                    r.AppDesc = o["description"].ToString();
                }
                catch (Exception e)
                {
                    r.AppDesc = "";
                }
                r.AppIcon = o["iconurl"].ToString();
                r.AppLaunchURL = o["launchurl"].ToString();
                r.ID = o["id"].ToString();
                _applicationList.Add(r);
            }

            return _applicationList;
        }

        public static string GetIcaString(string SessionID, string AuthID, string CsrfToken, string AppID, string AppLaunchURL, bool UseSSL, string baseSFURL)
        {
            string SFURL = null;

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }

            RestClient _rc = new RestClient(SFURL);

            RestRequest _getResourcesReq = new RestRequest(string.Format(AppLaunchURL), Method.GET);
            if (UseSSL)
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }
            _getResourcesReq.AddHeader("Content-Type", "application/octet-stream");
            _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
            _getResourcesReq.AddCookie("csrftoken", CsrfToken);
            _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
            _getResourcesReq.AddCookie("CtxsAuthId", AuthID);
            IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

            string _icaFileString = _resourceListResp.Content;

            return _icaFileString;
        }

        public static string GetIcaStringAsJSON(string SessionID, string AuthID, string CsrfToken, string AppID, string AppLaunchURL, bool UseSSL, string baseSFURL)
        {
            string SFURL = null;

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }

            RestClient _rc = new RestClient(SFURL);

            RestRequest _getResourcesReq = new RestRequest(string.Format(AppLaunchURL), Method.GET);
            if (UseSSL)
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }
            _getResourcesReq.AddHeader("Content-Type", "application/json");
            _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
            _getResourcesReq.AddCookie("csrftoken", CsrfToken);
            _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
            _getResourcesReq.AddCookie("CtxsAuthId", AuthID);
            IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

            string _icaFileString = _resourceListResp.Content;

            return _icaFileString;
        }

        public static string GetIcaStringAsFile(string SessionID, string AuthID, string CsrfToken, string AppID, string AppLaunchURL, bool UseSSL, string baseSFURL)
        {
            string SFURL = null;

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }

            RestClient _rc = new RestClient(SFURL);
            RestRequest _getResourcesReq = new RestRequest(string.Format(AppLaunchURL), Method.GET);

            if (UseSSL)
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }
            _getResourcesReq.AddHeader("Accept", "application/octet-stream");
            _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
            _getResourcesReq.AddCookie("csrftoken", CsrfToken);
            _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
            _getResourcesReq.AddCookie("CtxsAuthId", AuthID);
            IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

            string _icaFileString = _resourceListResp.Content;

            return _icaFileString;
        }

        public static bool LaunchApplication(string SessionID, string AuthID, string CsrfToken, string AppID, string AppLaunchURL, bool UseSSL, string LocalPath, string BaseSFURL)
        {
            bool isLaunch = true;

            try
            {
                string _ica = GetIcaString(SessionID, AuthID, CsrfToken, AppID, AppLaunchURL, UseSSL, BaseSFURL);
                //save the file            
                string FileNamdAndPath = string.Format(@"{0}\launch.ica", LocalPath);
                File.WriteAllText(FileNamdAndPath, _ica);
                System.Diagnostics.Process.Start(FileNamdAndPath);
            }
            catch (Exception ee)
            {
                isLaunch = false;
            }

            return isLaunch;
        }

        public static bool ReconnectRoamingSessions(string SessionID, string AuthID, string CsrfToken, bool UseSSL)
        {
            string SFURL = null;
            bool sessionsExist = false;

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }

            List<CitrixApplicationInfo> _applicationList = new List<CitrixApplicationInfo>();
            RestClient _rc = new RestClient(SFURL);
            RestRequest _getResourcesReq = new RestRequest(@"Sessions/ListAvailable", Method.POST);
            if (UseSSL)
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }
            _getResourcesReq.AddHeader("Accept", "application/json");
            _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
            _getResourcesReq.AddCookie("csrftoken", CsrfToken);
            _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
            _getResourcesReq.AddCookie("CtxsAuthId", AuthID);

            IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);

            string json = _resourceListResp.Content;

            //  JObject a = JObject.Parse(json);
            //  JArray resources = (JArray)a["resources"];
            JArray resources = JArray.Parse(json);
            CitrixApplicationInfo r = new CitrixApplicationInfo();
            if (resources != null)
            {
                sessionsExist = true;

                foreach (var o in resources)
                {

                    //r.AppTitle = o["name"].ToString();
                    //try
                    //{
                    //    r.AppDesc = o["description"].ToString();
                    //}
                    //catch (Exception e)
                    //{
                    //    r.AppDesc = "";
                    //}
                    //  r.AppIcon = o["iconurl"].ToString();
                    r.AppLaunchURL = o["launchurl"].ToString();
                    //  r.ID = o["id"].ToString();
                    // _applicationList.Add(r);
                }
            }

            LaunchApplication(SessionID, AuthID, CsrfToken, "", r.AppLaunchURL, false, Path.GetTempPath(), baseSFURL);

            return sessionsExist;
        }

        public static string KillSessions(string SessionID, string AuthID, string CsrfToken, bool UseSSL, string baseSFURL) //logoff
        {
            string SFURL = null;

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }

            RestClient _rc = new RestClient(SFURL);
            RestRequest _getResourcesReq = new RestRequest(@"Sessions/Logoff", Method.POST);

            if (UseSSL)
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }

            _getResourcesReq.AddHeader("Accept", "application/json");
            _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
            _getResourcesReq.AddCookie("csrftoken", CsrfToken);
            _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
            _getResourcesReq.AddCookie("CtxsAuthId", AuthID);

            IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);
            var response = _resourceListResp.StatusCode;


            return response.ToString();
        }

        public static string DisconnectSessions(string SessionID, string AuthID, string CsrfToken, bool UseSSL, string baseSFURL) //disconnect
        {
            string SFURL = null;

            if (UseSSL)
            {
                SFURL = "https://" + baseSFURL;
            }
            else
            {
                SFURL = "http://" + baseSFURL;
            }

            RestClient _rc = new RestClient(SFURL);
            RestRequest _getResourcesReq = new RestRequest(@"Sessions/Disconnect", Method.POST);

            if (UseSSL)
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "Yes");
            }
            else
            {
                _getResourcesReq.AddHeader("X-Citrix-IsUsingHTTPS", "No");
            }

            _getResourcesReq.AddHeader("Accept", "application/json");
            _getResourcesReq.AddHeader("Csrf-Token", CsrfToken);
            _getResourcesReq.AddCookie("csrftoken", CsrfToken);
            _getResourcesReq.AddCookie("asp.net_sessionid", SessionID);
            _getResourcesReq.AddCookie("CtxsAuthId", AuthID);

            IRestResponse _resourceListResp = _rc.Execute(_getResourcesReq);
            var response = _resourceListResp.ResponseStatus;


            return response.ToString();
        }


    }
}
