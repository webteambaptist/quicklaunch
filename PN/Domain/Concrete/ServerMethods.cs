﻿namespace QuickLaunch
{
    using System;
    using System.Runtime.Remoting;
    using System.Runtime.Remoting.Channels;
    using System.Runtime.Remoting.Channels.Ipc;
    using global::QuickLaunch.Shared;
    using System.Reflection;

    /// <summary>
    /// This class provides the IPC hooks that allow Notifier to send messages to the Quick Launch UI.
    /// </summary>
    public class QLServerMethods : MarshalByRefObject, IQuickLaunch
    {
        /// <summary>
        /// This method creates the IPC Server channel. The port is named QuickLaunchUI, 
        /// and it exposes a single method, SetCurrentUser which accepts a user and an event.
        /// </summary>
        public static void Setup()
        {
            try
            {
                BinaryServerFormatterSinkProvider serverProvider = new BinaryServerFormatterSinkProvider();
                serverProvider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
                System.Collections.IDictionary props = new System.Collections.Hashtable();
                props["portName"] = "QuickLaunchUI";
                props["exclusiveAddressUse"] = false;
                props["authorizedGroup"] = "Everyone";
                //Instantiate our server channel.
                var channel = new IpcServerChannel(props, serverProvider);

                //Register the server channel.
                ChannelServices.RegisterChannel(channel, false);

                //Register this service type.
                RemotingConfiguration.RegisterWellKnownServiceType(
                                            typeof(QLServerMethods),
                                            "QuickLaunch",
                                            WellKnownObjectMode.Singleton);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

        }

        /// <summary>
        /// Sets the current user.
        /// 
        /// This method is exposed via IPC to the Notifier program.
        /// </summary>
        /// <param name="user">The user.</param>
        public void SetCurrentUser(UserInfo user, ImprivataEvent imprivataEvent)
        {
            try
            {
                Program.SetCurrentUser(user, imprivataEvent);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
    }
}