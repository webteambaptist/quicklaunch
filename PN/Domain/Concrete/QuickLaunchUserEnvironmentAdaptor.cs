﻿namespace QuickLaunch
{
    using com.citrix.wing;
    using java.util;

    /// <summary>
    /// This is required for the ICA client.
    /// </summary>
    public class QuickLaunchUserEnvironmentAdaptor : UserEnvironmentAdaptor
    {
        private Map sessionCache = new HashMap();
        private Map deviceState = new HashMap();
        private Map userState = new HashMap();
        private Map clientSessionState = new HashMap();
        private Map sessionState = new HashMap();

        /// <summary>
        /// Gets the session cache.
        /// </summary>
        /// <returns></returns>
        public java.util.Map getSessionCache() { return sessionCache; }

        /// <summary>
        /// Gets the state of the device.
        /// </summary>
        /// <returns></returns>
        public java.util.Map getDeviceState() { return deviceState; }

        /// <summary>
        /// Gets the state of the user.
        /// </summary>
        /// <returns></returns>
        public java.util.Map getUserState() { return userState; }

        /// <summary>
        /// Gets the state of the client session.
        /// </summary>
        /// <returns></returns>
        public java.util.Map getClientSessionState() { return clientSessionState; }

        /// <summary>
        /// Gets the state of the session.
        /// </summary>
        /// <returns></returns>
        public java.util.Map getSessionState() { return sessionState; }

        /// <summary>
        /// Determines whether this instance is committed.
        /// </summary>
        /// <returns>
        ///   <c>true</c> if this instance is committed; otherwise, <c>false</c>.
        /// </returns>
        public bool isCommitted() { return false; }

        /// <summary>
        /// Commits the state.
        /// </summary>
        public void commitState() { }

        /// <summary>
        /// Destroys this instance.
        /// </summary>
        public void destroy() { }

        /// <summary>
        /// Gets the component vendor.
        /// </summary>
        /// <returns></returns>
        public string getComponentVendor() { return "Baptist Health"; }

        /// <summary>
        /// Gets the component description.
        /// </summary>
        /// <returns></returns>
        public string getComponentDescription() { return "Quick Launch"; }

        /// <summary>
        /// Gets the component version major.
        /// </summary>
        /// <returns></returns>
        public int getComponentVersionMajor() { return 1; }

        /// <summary>
        /// Gets the component version minor.
        /// </summary>
        /// <returns></returns>
        public int getComponentVersionMinor() { return 0; }

        /// <summary>
        /// Gets the component revision.
        /// </summary>
        /// <returns></returns>
        public long getComponentRevision() { return 0; }

        /// <summary>
        /// Gets the child components.
        /// </summary>
        /// <returns></returns>
        public com.citrix.wing.Component[] getChildComponents() { return null; }
    }
}