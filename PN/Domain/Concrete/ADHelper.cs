﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices;

namespace QuickLaunch
{
    class ADHelper
    {        
        public ADHelper()
        {
        }

        /// <summary>
        /// Determines whether [is in group] [the specified user].
        /// </summary>
        /// <param name="groups">Available AD groups.</param>
        /// <param name="group">The group.</param>
        /// <returns>
        ///   <c>true</c> if [is in group] [the specified user]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInGroup(List<string> groups, string group)
        {
            return groups.Contains(group);
        }

        /// <summary>
        /// Gets the groups.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public static bool GetGroups(Shared.UserInfo user, out List<string> groups)
        {
            groups = new List<string>();
            bool result = true;
            DirectoryEntry de = new DirectoryEntry(string.Format("WinNT://{0}/{1},user", user.Domain, user.UserName));
            try
            {
                var gs = (IEnumerable)de.Invoke("Groups");
                foreach (var g in gs)
                {
                    var gn = new DirectoryEntry(g);
                    groups.Add(gn.Name);
                }
            }
            catch (Exception ex)
            {
                result = false;
                Program.LogException(ex, false);
            }
            return result;
        }
    }
}
