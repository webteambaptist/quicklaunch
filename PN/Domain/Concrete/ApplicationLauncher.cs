﻿using Cassia;
using com.citrix.authentication.tokens;
using com.citrix.wing;
using com.citrix.wing.config;
using com.citrix.wing.types;
using com.citrix.wing.webpn;
using global::QuickLaunch.Shared;
using QuickLaunch.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security;
using System.Security.Permissions;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;
using WFICALib;


[assembly: SecurityPermissionAttribute(SecurityAction.RequestMinimum, UnmanagedCode = true)]
[assembly: PermissionSetAttribute(SecurityAction.RequestMinimum, Name = "FullTrust")]
namespace QuickLaunch
{

    /// <summary>
    /// This class is responsible for building ICA files used to launch or reconnect to applications.
    /// </summary>
    public static class ApplicationLauncher
    {

        #region imports
        [DllImport("advapi32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool LogonUser(String lpszUsername, String lpszDomain, String lpszPassword,
            int dwLogonType, int dwLogonProvider, ref IntPtr phToken);

        [DllImport("kernel32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        private unsafe static extern int FormatMessage(int dwFlags, ref IntPtr lpSource,
            int dwMessageId, int dwLanguageId, ref String lpBuffer, int nSize, IntPtr* Arguments);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
        public extern static bool CloseHandle(IntPtr handle);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public extern static bool DuplicateToken(IntPtr ExistingTokenHandle,
            int SECURITY_IMPERSONATION_LEVEL, ref IntPtr DuplicateTokenHandle);
        #endregion

        public static string lastapp = string.Empty;
        public static string lastserver = string.Empty;
        public static string logLevel = Truncus.setLogLevel();

        private static readonly ITerminalServicesManager _manager = new TerminalServicesManager();

        /// <summary>
        /// Gets an ICA client.
        /// </summary>
        /// <returns></returns>
        public static ICAClientClass GetICAClient()
        {
            var ICA = new ICAClientClass();
            return ICA;
        }

        /// <summary>
        /// Gets the user context.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public static com.citrix.wing.webpn.UserContext GetUserContext(UserInfo user)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            WebPNBuilder builder;
            WebPN webpn;
            UserContext userContext;
            QuickLaunchLogger logger = new QuickLaunchLogger();
            Configuration config = new Configuration();
            List<XenAppInfo> xInfos = user.XenAppInfo;

            //try
            //{
            //    XenAppInfo xInfox = new XenAppInfo();
            //    xInfox.XenAppHost1 = "bpectxctl01v.bh.local";
            //    xInfox.XenAppHost2 = "bpectxctl02v.bh.local";
            //    xInfox.Farm = "BPEXA78";
            //    xInfox.XenAppPort = "8181";

            //    xInfos.Add(xInfox);
            //}
            //catch (Exception ee)
            //{ }




            try
            {

                foreach (XenAppInfo xInfo in xInfos)
                {
                    MPSFarmConfig farmConfig = new MPSFarmConfig(xInfo.Farm);
                    farmConfig.addConnection(new MPSConnectionConfig(new ServiceAddress(xInfo.XenAppHost1, xInfo.XenAppPort)));
                    farmConfig.addConnection(new MPSConnectionConfig(new ServiceAddress(xInfo.XenAppHost2, xInfo.XenAppPort)));
                    config.addMPSFarmConfig(farmConfig);
                }

                builder = WebPNBuilder.getInstance();
                webpn = builder.createWebPN(config, new QuickLaunchStaticEnvironmentAdaptor(logger));
                userContext = builder.createInitialUserContext(webpn, new QuickLaunchUserEnvironmentAdaptor());

                AccessToken userToken = new UserDomainPasswordCredentials(user.UserName, user.Domain, user.Password);

                foreach (XenAppInfo xInfo in xInfos)
                {
                    userContext.getActiveFarms().add(new FarmBinding(xInfo.Farm, userToken));
                }

                sw.Stop();
                builder = null; 
                webpn = null; 
                logger = null;
                
                
                Truncus.LogEvents("1", "", user.UserName, sw.ElapsedMilliseconds, "Build User Context");
                string fdcBaseline = Utility.getSettingFromCache("FDCBaseline");

                if (sw.ElapsedMilliseconds > long.Parse(fdcBaseline))
                {
                    try
                    {
                        Truncus.LogEvents(logLevel, "", user.UserName, sw.ElapsedMilliseconds, "BUILD USER CONTEXT BASELINE");
                    }
                    catch(Exception ee)
                    { }
                }
            }
            catch(Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
                return userContext = GetUserContextSwitchProviders(user); //can be null
                
            }

            return userContext;
        }

        private static UserContext GetUserContextSwitchProviders(UserInfo user) //same as above with switched addresses
        {

            Stopwatch sw = new Stopwatch();
            sw.Start();
            WebPNBuilder builder;
            WebPN webpn;
            QuickLaunchLogger logger = new QuickLaunchLogger();
            Configuration config = new Configuration();

            try
            {
                List<XenAppInfo> xInfos = user.XenAppInfo;

                foreach (XenAppInfo xInfo in xInfos)
                {
                    MPSFarmConfig farmConfig = new MPSFarmConfig(xInfo.Farm);
                    farmConfig.addConnection(new MPSConnectionConfig(new ServiceAddress(xInfo.XenAppHost2, xInfo.XenAppPort)));
                    farmConfig.addConnection(new MPSConnectionConfig(new ServiceAddress(xInfo.XenAppHost1, xInfo.XenAppPort)));
                    config.addMPSFarmConfig(farmConfig);
                }

                builder = WebPNBuilder.getInstance();
                webpn = builder.createWebPN(config, new QuickLaunchStaticEnvironmentAdaptor(logger));
                UserContext userContext = builder.createInitialUserContext(webpn, new QuickLaunchUserEnvironmentAdaptor());

                AccessToken userToken = new UserDomainPasswordCredentials(user.UserName, user.Domain, user.Password);

                foreach (XenAppInfo xInfo in xInfos)
                {
                    userContext.getActiveFarms().add(new FarmBinding(xInfo.Farm, userToken));
                }

                sw.Stop();
                builder = null;
                webpn = null;
                logger = null;

                return userContext;
            }
            catch(Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            return null;
        }

        /// <summary>
        /// Gets a dictionary of applications the user has access to, keyed by display name with the resource ID as the value.
        /// </summary>
        /// <param name="userContext">The user context.</param>
        /// <returns></returns>
        public static Dictionary<string, string> GetApplicationInfo(UserContext userContext, UserInfo userInfo)
        {
                Dictionary<string, string> apps = new Dictionary<string, string>();
                ResourceInfoSet resources = null;
                List<string> nonapps = new List<string>();
                string xmlFileName = getAppsPath(userInfo.UserName);

                if (Utility.checkCache(xmlFileName, "QL_USER")) //get apps and recreate XML file
                {               
                    
                    try
                    {
                        try
                        {
                            Stopwatch watch = new Stopwatch();
                            watch.Start();
                            //This definitely throws when the username and password are wrong. It might throw under other circumstances.
                            resources = userContext.findAllVisibleResources();
                            
                            watch.Stop();

                            Truncus.LogEvents("1", "", userInfo.UserName, watch.ElapsedMilliseconds, "Apps List Build");

                            string albBaseline = Utility.getSettingFromCache("App List Baseline");

                            try
                            {
                                if (watch.ElapsedMilliseconds > long.Parse(albBaseline)) //log activity
                                {
                                    Truncus.LogEvents(logLevel, "", userInfo.UserName, watch.ElapsedMilliseconds, "APP LIST BUILD");
                                }
                            }
                            catch(Exception ee)
                            { }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Unable to retrieve Citrix resources. EMR links are disabled.\r\nThis is usually caused by an incorrect password or expired account.", "Error");
                            Program.LogException(ex, false);
                        }

                        if (resources != null)
                        {
                            List<UserFarm> uList = new List<UserFarm>();

                            foreach (ResourceInfo res in resources.getResources())
                            {
                                if (res is ApplicationInfo) //&& res.isAccessMethodAvailable(AccessMethod.DISPLAY))
                                {
                                    ApplicationInfo ai = (ApplicationInfo)res;
                                    apps[ai.getDisplayName()] = res.getId();
                                   // MessageBox.Show(ai.getNameOfSource());
                                    try
                                    {
                                        string source = ai.getNameOfSource();
                                        //uList.Any(x => x.farmName.ToLower() != source.ToLower())
                                        if (uList.Count(x=> x.farmName.ToLower() == source.ToLower()) < 1)
                                        {
                                            UserFarm uFarm = new UserFarm();
                                            uFarm.ntID = userInfo.UserName;
                                            uFarm.farmName = ai.getNameOfSource();
                                            uList.Add(uFarm);
                                        }
                                    }
                                    catch(Exception ee)
                                    {

                                    }
                                    
                                }

                                //if ((res is FolderInfo))
                                //{
                                //    FolderInfo fi = (FolderInfo)res;
                                //    string rsource = fi.getDescription();
                                //    FolderContentInfo fci = userContext.findFolderContent(rsource);
                                //    int cnt = fci.getNumChildren();
                                //    FolderInfo fis = fci.getSelf();
                                //    string name = fis.getDisplayName();
                                //    string path = fis.getDisplayPath();

                                //    if(cnt > 0)
                                //    {
                                //        ResourceInfo[] riChildren = fci.getLeafResources();
                                //        foreach(ResourceInfo rif in riChildren)
                                //        {
                                //            if ((rif is ApplicationInfo) && rif.isAccessMethodAvailable(AccessMethod.DISPLAY))
                                //            {
                                //                ApplicationInfo ai = (ApplicationInfo)rif;
                                //                apps[ai.getDisplayName()] = rif.getId();
                                //            }
                                //        }
                                //    }

                           // }

                                //if (res is DesktopInfo)
                                //{
                                //    DesktopInfo di = (DesktopInfo)res;
                                //    bool hasclients = di.getHasClientHostedApps();
                                //}
                            }

                            
                            UserFarm.setUserFarm(uList);

                        }
                    }
                    catch (Exception ee)
                    {
                        Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
                    }

                    bool isSaved = storeApps(apps, userInfo.UserName); //serializeApps(apps, userInfo.UserName);
                }
                else
                {
                    apps = retrieveApps(userInfo.UserName); //deserializeApps(userInfo.UserName); 

                }

                
            return apps;
        
        }


        private static Dictionary<string, string> retrieveApps(string username)
        {
            Dictionary<string, string> apps = new Dictionary<string, string>();
            Stopwatch watch = new Stopwatch();
            watch.Start();

            try
            {
                string xmlFileName = getAppsPath(username);
                XDocument xmlDoc = XDocument.Load(xmlFileName);
                var userApps = from userApp in xmlDoc.Descendants("UserApp")
                               select new
                               {
                                   KeyName = userApp.Element("KeyName").Value,
                                   ValueName = userApp.Element("ValueName").Value
                               };

                foreach (var u in userApps)
                {
                    apps.Add(u.KeyName, u.ValueName);
                }
                
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            watch.Stop();

            Truncus.LogEvents("1", "", username, watch.ElapsedMilliseconds, "Apps List Build Cache");

            return apps;
        }

        /// <summary>
        /// Method to store applications per user in local DB cache
        /// </summary>
        /// <param name="apps"></param>
        /// <param name="userContext"></param>
        /// <returns></returns>
        private static bool storeApps(Dictionary<string, string> apps, string username)
        {
            try
            {
                string xmlFileName = getAppsPath(username);

                foreach (KeyValuePair<string, string> app in apps)
                {
                    if (File.Exists(xmlFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(xmlFileName);
                        xmlDoc.Element("UserApps").Add(new XElement("UserApp", new XElement("KeyName", app.Key), new XElement("ValueName", app.Value)));
                        xmlDoc.Save(xmlFileName);
                    }
                    else
                    {
                        //generate XML document first and save first record

                        XDocument xmlDoc = new XDocument(
                            new XDeclaration("1.0", "utf-16", "true"),
                            new XComment("UserApps List for QL"),
                            new XElement("UserApps",
                                    new XElement("UserApp",
                                        new XElement("KeyName", app.Key),
                                        new XElement("ValueName", app.Value))
                                    )
                                );
                        xmlDoc.Save(xmlFileName);
                    }
                }
            }
            catch(Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            return false;
        }

        /// <summary>
        /// serialize apps in local cache
        /// </summary>
        /// <param name="apps"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        private static bool serializeApps(Dictionary<string, string> apps, string username)
        {
            try
            {
                string xmlFileName = getAppsPath(username);

                if (File.Exists(xmlFileName))
                {
                    File.Delete(xmlFileName);
                }

                Stream stream = File.Open(xmlFileName, FileMode.Create);
                BinaryFormatter binFormatter = new BinaryFormatter();
                binFormatter.Serialize(stream, apps);
                stream.Close();
            }
            catch(Exception ee)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// deserialize apps and retrieve from local cache
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        private static Dictionary<string, string> deserializeApps(string username)
        {
            string xmlFileName = getAppsPath(username);
            Dictionary<string, string> dictionary = new Dictionary<string, string>();

            try
            {
                Stream stream = File.Open(xmlFileName, FileMode.Open);
                BinaryFormatter binFormatter = new BinaryFormatter();
                dictionary = (Dictionary<string, string>)binFormatter.Deserialize(stream);
                stream.Close();
            }
            catch(Exception ee)
            { }
            return dictionary;
        }
        
        /// <summary>
        /// get path for User Apps xml
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        private static string getAppsPath(string username)
        {
            string xmlFileName = "";

            if (username.ToLower().Contains("bh"))
            {
                username = username.Substring(3);
            }

            xmlFileName = Utility.getPath() + "QL_" + username + ".xml";
            
            return xmlFileName;
        }

        /// <summary>
        /// Launches the application.
        /// </summary>
        /// <param name="application">The application.</param>
        /// <param name="user">The user.</param>
        public static bool LaunchApplication(BackgroundWorker backgroundWorker, ICAClientClass result, Models.App citrixApp, UserContext userContext, UserInfo user)
        {
            bool ICAFileExists = false;
            try
            {
                string sessionId = string.Empty;
                string icaFileName = string.Empty;

                bool reconnect = BuildICAFile(backgroundWorker, userContext, user, citrixApp, out sessionId, out icaFileName);
                ICAFileExists = !string.IsNullOrEmpty(icaFileName);
                if (ICAFileExists)
                {
                  //  result.AutoAppResize = true; //new for screen resize

                    try
                    {
                        result.LoadIcaFile(icaFileName);
                    }
                    catch(Exception ee)
                    {
                        Program.LogException(ee, false);
                    }

                    //result.SetSessionGroupId(user.UserName);
                    if (reconnect)
                    {
                        result.Launch = false;
                      //  Debug.WriteLine(string.Format("Reconnectable session detected. {0}", sessionId));
                        int ds = result.SetSessionId(sessionId);
                        int newSession = result.AttachSession(sessionId);
                        backgroundWorker.ReportProgress(1, "ICAFileBuilt");
                        backgroundWorker.ReportProgress(5, "Reconnecting");

                    }
                    else
                    {
                      //  Debug.WriteLine("No reconnectable session detected");
                        result.Launch = true;
                        backgroundWorker.ReportProgress(1, "ICAFileBuilt");
                    }

                    result.AutoAppResize = true;
                    result.Connect();

                }
                else
                {
                    backgroundWorker.ReportProgress(0, "Unable to start application");
                    QuickLaunch ql = new QuickLaunch();
                    ql.ResetConnection();
                    backgroundWorker.CancelAsync();
                    
                }
            }
            catch(Exception ee)
            {
                MessageBox.Show("Error LaunchApplication " + ee.Message + " " + ee.InnerException, "Error");
                Program.LogException(ee, false);
            }
            return ICAFileExists;
        }

        /// <summary>
        /// Roams the users applications.
        /// </summary>
        /// <param name="backgroundWorker">The background worker.</param>
        /// <param name="result">The result.</param>
        /// <param name="userContext">The user context.</param>
        /// <param name="user">The user.</param>
        /// <returns><c>True</c> if the user had a session to roam.</returns>
        public static bool RoamUsersApplications(BackgroundWorker backgroundWorker, ICAClientClass result, UserContext userContext, UserInfo user, ref Stopwatch timerRoamSearch)
        {
            string sessionId = string.Empty;
            string icaFileName = string.Empty;
            bool canConnect = false;
            SessionInfoSet rcs = null;

            try
            {
                //This throws if the username & password are wrong.
                rcs = userContext.findReconnectableSessions(true);
            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
                timerRoamSearch.Stop(); //stop timer
            }

            try
            {

                if (rcs != null)
                {

                    var sessions = rcs.getSessions();

                    if (sessions.Length != 0)
                    {
                        timerRoamSearch.Stop(); //stop timer, roaming will occur

                        foreach (SessionInfo si in sessions)
                        {
                            if (UserCanReconnectToApplication(backgroundWorker, userContext, user, Models.App.CitrixApplications.ToArray(), false, si, out sessionId, out icaFileName))
                            {
                                result.LoadIcaFile(icaFileName);
                                result.Launch = true;

                                int ds = result.SetSessionId(sessionId);
                                int newSession = result.AttachSession(sessionId);
                                backgroundWorker.ReportProgress(0, "ApplicationsFound");

                                result.Connect();
                                canConnect = true;
                            }
                            else
                            {
                                canConnect = false;
                                try
                                {

                                    if ((logLevel == "2") || (logLevel == "3"))
                                    {
                                        Truncus.LogEvents("1", "", user.UserName, 0, "Session Fail");
                                    }
                                }
                                catch(Exception ee)
                                {
                                    Truncus.LogEvents("3", "RoamUsersApplications", "", 0, "ROAM FAILED " + ee.Message);
                                }
                            }

                            Thread.Sleep(1000);

                        }

                      //  return true;


                    }
                }
                else
                {
                   // Truncus.LogEvents("3", "RoamUsersApplications", "", 0, "RCS_COUNT IS NULL");
                }

             //   Truncus.LogEvents("3", "RoamUsersApplications", "", 0, "RTEST_ START " + canConnect.ToString());
                return canConnect;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            return canConnect;
        }

        /// <summary>
        /// Determines if the user can reconnect to an application, and builds an ICA file if they can.
        /// </summary>
        /// <param name="backgroundWorker">The background worker.</param>
        /// <param name="userContext">The user context.</param>
        /// <param name="user">The user.</param>
        /// <param name="applicationNames">The application names.</param>
        /// <param name="launch">if set to <c>true</c> launches the application.</param>
        /// <param name="sessionId">The session id.</param>
        /// <param name="icaFileName">Name of the ica file.</param>
        /// <returns><c>True</c> if the user has a session for this application to reconnect.</returns>
        private static bool UserCanReconnectToApplication(BackgroundWorker backgroundWorker, UserContext userContext, UserInfo user, Models.App[] citrixApps, bool launch, SessionInfo si, out string sessionId, out string icaFileName)
        {
            sessionId = string.Empty;
            icaFileName = string.Empty;
            var did = Environment.GetEnvironmentVariable("COMPUTERNAME").Replace("-", "").Replace("_", "").Replace(".", "");
            userContext.getClientDevice().setDeviceId(did);

            // Pass in the user's Access Token
            bool result = false;
            try
            {
                backgroundWorker.ReportProgress(0, "Searching for session");
                ////SessionInfoSet rcs = null;
                ////try
                ////{
                ////    //This throws if the username & password are wrong.
                ////    rcs = userContext.findReconnectableSessions(true);
                ////}
                ////catch (Exception ex)
                ////{
                ////    //MessageBox.Show("Unable to retrieve Citrix resources. EMR links are disabled.\r\nThis is usually caused by an incorrect password or expired account.", "Error");
                ////    Program.LogException(ex, false);
                ////}

                ////if (rcs == null)
                ////{
                ////    return false;
                ////}
                ////else
                ////{
                    backgroundWorker.ReportProgress(0, "Searching complete");
                ////    var sessions = rcs.getSessions();
                    backgroundWorker.ReportProgress(0, "getSessions");
                    ////if (sessions.Length == 0 && !launch)
                    ////{
                    ////    result = false;
                    ////    return result;
                    ////}

                    string applicationName = string.Empty;


                    ////foreach (SessionInfo sess in sessions)
                    ////{
                        foreach (var app in citrixApps)
                        {
                            ////string name = app.Value;

                            backgroundWorker.ReportProgress(0, "Checking " + app.Value);
                           //// if (sess.getDisplayName() == app.Value && app.AllowRoaming) //AllowRoaming gives the ability to an admin to turn off roaming for an application
                            if (si.getDisplayName() == app.Value && app.AllowRoaming) //AllowRoaming gives the ability to an admin to turn off roaming for an application
                            {
                                backgroundWorker.ReportProgress(0, "Found " + app.Value);
                                applicationName = app.Value;
                                QuickLaunch.CurrentLaunchApplication = new Models.MenuApplication { id = app.id, Title = app.Title, Value = app.Value, WindowTitle = app.WindowTitle };
                                result = true;
                                Thread.Sleep(TimeSpan.FromMilliseconds(500));
                                //sessionId = sess.getId();
                                sessionId = si.getId();

                                break;
                            }
                            if (result)
                                break;

                        }
                    ////}

                    if (result == false && launch == false)
                    {
                        return result;
                    }
                    else if (citrixApps.Length == 1)
                    {
                        applicationName = citrixApps[0].Value;
                    }
                    var seamless = AppDisplaySize.fromSeamless();
                    userContext.getAccessPrefs().setAppDisplaySize(seamless);

                    string resourceId = string.Empty;
                    backgroundWorker.ReportProgress(0, "Checking for resource");
                    if (user.ApplicationInfo.ContainsKey(applicationName))
                    {
                        resourceId = user.ApplicationInfo[applicationName];
                    }

                    if (string.IsNullOrEmpty(resourceId))
                    {
                        backgroundWorker.ReportProgress(0, "Resource not found.");
                    }
                    else
                    {
                        backgroundWorker.ReportProgress(0, "Resource found.");
                        AppLaunchInfo appLaunchInfo = null;
                        try
                        {
                            appLaunchInfo = (AppLaunchInfo)userContext.launchApp(resourceId, new AppLaunchParams(ClientType.ICA_30));
                            bool isLaunched = new Models.AppQueue().LogQueueEvent(user, "LAUNCH-START", applicationName); //record as a launch in queue
                        }
                        catch (Exception ex)
                        {
                            backgroundWorker.ReportProgress(0, "Unable to start application");
                            Program.LogException(ex, false);
                            Thread.Sleep(3000);
                            return false;
                        }
                        backgroundWorker.ReportProgress(0, "Application launch info created");

                        WriteICAFile(userContext, appLaunchInfo, backgroundWorker, applicationName, out icaFileName);
                    }
                ////}
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
                backgroundWorker.CancelAsync();
            }
            return result;
            ////  }
        }

        /// <summary>
        /// Determines if the user can reconnect to an application, and builds an ICA file if they can.
        /// </summary>
        /// <param name="backgroundWorker">The background worker.</param>
        /// <param name="userContext">The user context.</param>
        /// <param name="user">The user.</param>
        /// <param name="applicationNames">The application names.</param>
        /// <param name="launch">if set to <c>true</c> launches the application.</param>
        /// <param name="sessionId">The session id.</param>
        /// <param name="icaFileName">Name of the ica file.</param>
        /// <returns><c>True</c> if the user has a session for this application to reconnect.</returns>
        private static bool UserCanReconnectToApplication(BackgroundWorker backgroundWorker, UserContext userContext, UserInfo user, Models.App[] citrixApps, bool launch, out string sessionId, out string icaFileName)
        {
            sessionId = string.Empty;
            icaFileName = string.Empty;
            var did = Environment.GetEnvironmentVariable("COMPUTERNAME").Replace("-", "").Replace("_", "").Replace(".", "");

            userContext.getClientDevice().setDeviceId(did);
            // Pass in the user's Access Token
            bool result = false;
            backgroundWorker.ReportProgress(0, "Searching for session");
            SessionInfoSet rcs = null;
            try
            {
                //This throws if the username & password are wrong.
                rcs = userContext.findReconnectableSessions(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to retrieve Citrix resources. EMR links are disabled.\r\nThis is usually caused by an incorrect password or expired account.", "Error");
                Program.LogException(ex, false);
            }

                if (rcs == null)
                {
                    return false;
                }
                else
                {
                    backgroundWorker.ReportProgress(0, "Searching complete");
                    var sessions = rcs.getSessions();
                    //backgroundWorker.ReportProgress(0, "getSessions");
                    if (sessions.Length == 0 && !launch)
                    {
                        result = false;
                        return result;
                    }
                    string applicationName = string.Empty;

                    foreach (SessionInfo si in sessions)
                    {
                        foreach (var app in citrixApps)
                        {
                            //TODO: Clean this up.
                            string name = app.Value;
                            backgroundWorker.ReportProgress(0, "Checking " + name);
                            if (si.getDisplayName() == name && app.AllowRoaming) //AllowRoaming gives the ability to an admin to turn off roaming for an application
                            {
                                backgroundWorker.ReportProgress(0, "Found " + name);
                                applicationName = name;
                                QuickLaunch.CurrentLaunchApplication = new Models.MenuApplication { id = app.id, Title = app.Title, Value = app.Value, WindowTitle = app.WindowTitle };
                                result = true;
                                Thread.Sleep(TimeSpan.FromMilliseconds(500));
                                sessionId = si.getId();
                                Debug.WriteLine(string.Format("sessionId {0}", sessionId));
                                break;
                            }
                            if (result)
                                break;
                        }
                    }
                    if (result == false && launch == false)
                    {
                        return result;
                    }
                    else if (citrixApps != null)
                    {
                        if (citrixApps[0].Value != null)
                        {
                            applicationName = citrixApps[0].Value;
                        }
                        else
                        {
                            return result;
                        }
                    }
                    else
                    {
                        return result;
                    }

                    var seamless = AppDisplaySize.fromSeamless();
                    userContext.getAccessPrefs().setAppDisplaySize(seamless);

                    string resourceId = string.Empty;
                    backgroundWorker.ReportProgress(0, "Checking for resource");
                    if (user.ApplicationInfo.ContainsKey(applicationName))
                    {
                        resourceId = user.ApplicationInfo[applicationName];
                    }

                    if (string.IsNullOrEmpty(resourceId))
                    {
                        backgroundWorker.ReportProgress(0, "Resource not found.");
                    }
                    else
                    {
                        backgroundWorker.ReportProgress(0, "Resource found.");
                        AppLaunchInfo appLaunchInfo = null;
                        try
                        {
                            appLaunchInfo = (AppLaunchInfo)userContext.launchApp(resourceId, new AppLaunchParams(ClientType.ICA_30));
                        }
                        catch (Exception ex)
                        {
                            backgroundWorker.ReportProgress(0, "Unable to start application");
                            Program.LogException(ex, false);
                            backgroundWorker.CancelAsync();
                            return false;
                        }
                        backgroundWorker.ReportProgress(0, "Application launch info created");

                        WriteICAFile(userContext, appLaunchInfo, backgroundWorker, applicationName, out icaFileName);
                    }
                }
                return result;

        }

        public static void WriteICAFile(UserContext userContext, AppLaunchInfo appLaunchInfo, BackgroundWorker backgroundWorker, string applicationName, out string icaFileName)
        {

            icaFileName = "";
            try
            {
                ICAFile icaFile = userContext.convertToICAFile(appLaunchInfo, null, null);
                if (backgroundWorker != null) backgroundWorker.ReportProgress(0, "Creating ICA file");
                icaFile.setICAEncoding("UTF8");
                icaFile.setValue("WFClient", "CPMAllowed", "On");
                icaFile.setValue("WFClient", "ProxyFavorIEConnectionSetting", "Yes");
                icaFile.setValue("WFClient", "ProxyTimeout", "30000");
                // Have the ICA client delete the file after it's been used
                icaFile.setValue("WFClient", "RemoveICAFile", "yes");
                icaFile.setValue("WFClient", "TransportReconnectEnabled", "Off");
                icaFile.setValue("WFClient", "VSLAllowed", "On");
                icaFile.setValue("WFClient", "Version", "2");
                icaFile.setValue("WFClient", "ProxyUseFQDN", "Off");
                icaFile.setValue("WFClient", "VirtualCOMPortEmulation", "Off");

                icaFile.setValue(applicationName, "AutologonAllowed", "On");
                icaFile.setValue(applicationName, "BrowserProtocol", "HTTPonTCP");
                var hres = Screen.PrimaryScreen.Bounds.Width;
                var vres = Screen.PrimaryScreen.Bounds.Height;
                string desiredColor = "8";
                switch (Screen.PrimaryScreen.BitsPerPixel)
                {
                    case 8:
                        desiredColor = "2";
                        break;
                    case 15:
                    case 16:
                        desiredColor = "4";
                        break;
                    case 32:
                        desiredColor = "8";
                        break;
                }
                icaFile.setValue(applicationName, "DesiredColor", desiredColor);
                icaFile.setValue(applicationName, "DesiredHRES", hres.ToString());
                icaFile.setValue(applicationName, "DesiredVRES", vres.ToString());
                icaFile.setValue(applicationName, "DoNotUseDefaultCSL", "On");
                icaFile.setValue(applicationName, "LPWD", "15"); //15,0
                icaFile.setValue(applicationName, "NRWD", "0");
                icaFile.setValue(applicationName, "ProxyTimeout", "30000");
                icaFile.setValue(applicationName, "SecureChannelProtocol", "Detect");
                icaFile.setValue(applicationName, "TRWD", "16");
                icaFile.setValue(applicationName, "UILocale", "en");
                icaFile.setValue(applicationName, "WinStationDriver", "ICA 3.0");
                icaFile.setValue(applicationName, "AutologonAllowed", "On");
                icaFile.setValue(applicationName, "AutologonAllowed", "On");

                icaFile.setValue("Compress", "DriverNameWin16", "pdcompw.dll");
                icaFile.setValue("Compress", "DriverNameWin32", "pdcompn.dll");

                icaFile.setValue("EncRC5-0", "DriverNameWin16", "pdc0w.dll");
                icaFile.setValue("EncRC5-0", "DriverNameWin32", "pdc0n.dll");

                icaFile.setValue("EncRC5-128", "DriverNameWin16", "pdc128w.dll");
                icaFile.setValue("EncRC5-128", "DriverNameWin32", "pdc128n.dll");

                icaFile.setValue("EncRC5-40", "DriverNameWin16", "pdc40w.dll");
                icaFile.setValue("EncRC5-40", "DriverNameWin32", "pdc40n.dll");

                icaFile.setValue("EncRC5-56", "DriverNameWin16", "pdc56w.dll");
                icaFile.setValue("EncRC5-56", "DriverNameWin32", "pdc56n.dll");
                
                
               // Render the ICA file
                //check OS
                var os = Environment.OSVersion;

                if (os.Version.Major > 5)
                {
                    string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                    if (userName.ToLower().Contains("bh"))
                    {
                        userName = userName.Substring(3);
                    }
                    icaFileName = "C:\\users\\" + userName + "\\appdata\\local\\Temp\\" + new FileInfo(Path.GetTempFileName()).Name;
                }
                else
                {
                    icaFileName = Path.GetTempFileName();
                }

                //var path = Environment.GetFolderPath(Environment.SpecialFolder.InternetCache);
                //icaFileName = path + new FileInfo(Path.GetTempFileName()).Name;
                
                StreamWriter sw = new StreamWriter(icaFileName);
                sw.Write(icaFile.ToString());
                sw.Close();

                try
                {
                    string serverAddress = icaFile.getValue(applicationName, "Address");
                    string appName = icaFile.getAppName();

                    lastapp = appName;
                    lastserver = serverAddress;
                    Truncus.LogEvents(logLevel, appName + ": " + serverAddress, "", 0, "WRITE ICA FILE");
                }
                catch(Exception ee)
                {

                }
               
            }
            catch (Exception ee)
            {
                backgroundWorker.ReportProgress(0, "ICA Write Failure");
                Program.LogException(ee, false);
            }

            if (backgroundWorker != null) backgroundWorker.ReportProgress(0, "ICA file created");
        }

        /// <summary>
        /// Determines whether this user can user reconnect to the specified application name.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <returns>
        ///   <c>true</c> if this user can reconnect to application] the specified application name; otherwise, <c>false</c>.
        /// </returns>
        private static bool BuildICAFile(BackgroundWorker backgroundWorker, UserContext userContext, UserInfo user, Models.App citrixApp, out string sessionId, out string icaFileName)
        {
            return UserCanReconnectToApplication(backgroundWorker, userContext, user, new Models.App[] { citrixApp }, true, out sessionId, out icaFileName);
        }

        /// <summary>
        /// Uses terminal services to do a citrix disconnect
        /// </summary>
        /// <param name="CurrentUser">The user to perform the TS disconnect for</param>
        /// <param name="IsLogoff">Indicates whether this is a TS LOGOFF insteadr</param>
        /// <returns>An exception if one is caught, otherwise it returns null</returns>
        public static Exception TerminalServicesDisconnect(ICAClientClass ica, UserInfo CurrentUser, bool IsLogOff = false, bool LogEntry = true)
        {
            
            Exception caught = null;
            try
            {

                ////start client machine disconnect
                UserContext userContext = ApplicationLauncher.GetUserContext(CurrentUser); //reset user context            
                SessionInfoSet rcs = userContext.findReconnectableSessions(true);
                Thread.Sleep(250);  //do not remove this duplication...the initial one does a disconnect which allows sessions to jump servers. After that it should stay on the same server, so the second findReconnectableSessions gets that server info
                userContext = ApplicationLauncher.GetUserContext(CurrentUser); //reset user context            
                rcs = userContext.findReconnectableSessions(true); //get actual servers
                ////end client machine disconnect

                caught = CitrixLogOffCmd(CurrentUser, IsLogOff, LogEntry, caught, userContext, rcs, ica); //new way

               // caught = TermServicesCmd(CurrentUser, IsLogOff, LogEntry, caught, userContext, rcs); //old way
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            return caught;
        }

        private static ITerminalServer GetServerFromName(string serverName)
        {
            return
                (serverName.Equals("local", StringComparison.InvariantCultureIgnoreCase)
                     ? _manager.GetLocalServer()
                     : _manager.GetRemoteServer(serverName));
        }

        /// <summary>
        /// Uses Logoff session via .Net wrapper for TS
        /// </summary>
        /// <param name="CurrentUser"></param>
        /// <param name="IsLogOff"></param>
        /// <param name="LogEntry"></param>
        /// <param name="caught"></param>
        /// <param name="userContext"></param>
        /// <param name="rcs"></param>
        /// <returns></returns>
        [PermissionSetAttribute(SecurityAction.Demand, Name = "FullTrust")]
        private static Exception CitrixLogOffCmd(UserInfo CurrentUser, bool IsLogOff, bool LogEntry, Exception caught, UserContext userContext, SessionInfoSet rcs, ICAClientClass ica)
        {
            DateTime startTime = DateTime.Now;
            var sessions = rcs.getSessions();

            foreach (SessionInfo si in sessions)
            {
                try
                {
                    string app = si.getDisplayName();
                    string sessionId = si.getSessionId();
                    string resourceId = CurrentUser.ApplicationInfo[app];
                    string domain = CurrentUser.Domain;
                    string user = CurrentUser.UserName;
                    //SecureString pass = Shared.SysHelper.ToSecurePassword(CurrentUser.Password); //securePwd;
                    IntPtr tokenHandle = new IntPtr(0);
                    IntPtr dupeTokenHandle = new IntPtr(0);
                    const int LOGON32_PROVIDER_DEFAULT = 0;
                    //This parameter causes LogonUser to create a primary token. 
                    const int LOGON32_LOGON_INTERACTIVE = 2;
                    tokenHandle = IntPtr.Zero;
                    // Call LogonUser to obtain a handle to an access token. 
                    bool returnValue = LogonUser(user, domain, CurrentUser.Password, LOGON32_LOGON_INTERACTIVE, LOGON32_PROVIDER_DEFAULT, ref tokenHandle);

                    com.citrix.wing.AppLaunchInfo appLaunchInfo = (com.citrix.wing.AppLaunchInfo)userContext.launchApp(resourceId, new com.citrix.wing.AppLaunchParams(com.citrix.wing.types.ClientType.ICA_30));

                    string server = appLaunchInfo.getServerAddress().toICAAddress();

                    if (server.Contains(":"))
                    {
                        server = server.Remove(server.LastIndexOf(':')); //TS session id gets appended to the end of the string
                    }
                    using (WindowsImpersonationContext wim = new WindowsIdentity(tokenHandle).Impersonate())
                    {
                        using (ITerminalServer ts = GetServerFromName(server))
                        {
                            ts.Open();
                            ITerminalServicesSession session = ts.GetSession(int.Parse(si.getSessionId()));
                            if (IsLogOff)
                            {
                                Stopwatch sw = new Stopwatch();
                                sw.Start();

                                session.Logoff(); //KILL

                                sw.Stop();

                                if (sw.ElapsedMilliseconds < 20000)
                                {
                                    try
                                    {
                                        long nap = 20000 - sw.ElapsedMilliseconds;
                                        Thread.Sleep(Convert.ToInt16(nap)); //give kill time to finish up
                                    }
                                    catch(Exception ee)
                                    {
                                        Thread.Sleep(20000); //give kill time to finish up
                                    }
                                }

                                Truncus.LogEvents("2", session.Server.ServerName + " " + app, user, sw.ElapsedMilliseconds, "KILL EMR");
                            }
                            else
                            {
                                Stopwatch sw = new Stopwatch();
                                sw.Start();

                                session.Disconnect(); //roam

                                sw.Stop();
                                Truncus.LogEvents("2", session.Server.ServerName + " " + app, user, sw.ElapsedMilliseconds, "DISCONNECT");
                            }
                        }

                        wim.Undo();
                    }

                    startTime = DateTime.Now; //for next iteration...if there is one.
                }
                catch (Exception ex)
                {
                    caught = ex;
                }
            }

            bool isLaunched = new Models.AppQueue().ClearLaunches(CurrentUser.UserName);

            return caught;
        }



        #region deprecated
        /// <summary>
        /// Kill Citrix Sessions by using Terminal Services commands
        /// </summary>
        /// <param name="CurrentUser"></param>
        /// <param name="IsLogOff"></param>
        /// <param name="LogEntry"></param>
        /// <param name="caught"></param>
        /// <param name="userContext"></param>
        /// <param name="rcs"></param>
        /// <returns></returns>
        private static Exception TermServicesCmd(UserInfo CurrentUser, bool IsLogOff, bool LogEntry, Exception caught, UserContext userContext, SessionInfoSet rcs)
        {
            ////start remote machine terminal services command            
            DateTime startTime = DateTime.Now;
            var sessions = rcs.getSessions();
            foreach (SessionInfo si in sessions)
            {

                try
                {
                    string app = si.getDisplayName();
                    string sessionId = si.getSessionId();
                    string resourceId = CurrentUser.ApplicationInfo[app];
                    com.citrix.wing.AppLaunchInfo appLaunchInfo = (com.citrix.wing.AppLaunchInfo)userContext.launchApp(resourceId, new com.citrix.wing.AppLaunchParams(com.citrix.wing.types.ClientType.ICA_30));
                    string server = appLaunchInfo.getServerAddress().toICAAddress();

                    Process p = new Process();
                    p.StartInfo.UseShellExecute = false;
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.CreateNoWindow = true;
                    p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    p.StartInfo.UserName = CurrentUser.UserName;
                    p.StartInfo.Password = Utility.ConvertToSecureString(CurrentUser.Password); //securePwd;
                    p.StartInfo.WorkingDirectory = @"C:\Windows\System32";
                    p.StartInfo.FileName = "cmd.exe";
                    if (server.Contains(":"))
                        server = server.Remove(server.LastIndexOf(':')); //TS session id gets appended to the end of the string
                    p.StartInfo.Arguments = string.Format(@"cmd /c {0} {1} /server:{2}", (IsLogOff ? "rwinsta" : "TSDISCON"), sessionId, server);
                    p.StartInfo.RedirectStandardOutput = true;
                    p.StartInfo.RedirectStandardInput = true;
                    p.StartInfo.RedirectStandardError = true;
                    p.EnableRaisingEvents = false; //  /c
                    p.OutputDataReceived += (sender, args) => Console.WriteLine(args.Data); // debug only
                    p.ErrorDataReceived += (sender, args) => Console.WriteLine(args.Data); // debug only
                    p.Exited += (sender, args) => Console.WriteLine("Exit captured"); // debug only
                    p.Start();
                    Thread.Sleep(1000); //GIVE THE WINDOW A CHANCE TO DISPLAY BEFORE HIDING
                    try
                    {
                        IntPtr lHwnd = QuickLaunch.UnsafeNativeMethods.FindWindow(@"C:\Windows\System32", null); //minimizes all windows
                        QuickLaunch.UnsafeNativeMethods.ShowWindow(p.MainWindowHandle, 0);
                    }
                    catch { } //dont want to report on cases where the window is gone before it can hide it.
                    p.WaitForExit(120000); //willing to wait 2min  

                    //if (LogEntry)
                    //    Models.EventLog_QL.LogEvent(IsLogOff ? "LOGOFF" : "DISCONNECT", CurrentUser.UserName, string.Format("{0} [{1}]", app, server), LengthOfTime: DateTime.Now.Subtract(startTime).TotalMilliseconds);

                    startTime = DateTime.Now; //for next iteration...if there is one.
                }
                catch (Exception ex)
                {
                    caught = ex;
                }
            }
            //end remote machine terminal services command
            return caught;
        }
        #endregion

    }
}