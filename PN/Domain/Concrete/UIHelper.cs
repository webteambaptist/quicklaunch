﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using QuickLaunch.Shared;
using System.Reflection;

namespace QuickLaunch
{
    class UIHelper
    {
        public static void DisplayOSBubble(NotifyIcon ni, string title, string text, int timeout = 2000)
        {
            try
            {
                var bitmap = new System.Drawing.Bitmap(global::QuickLaunch.Properties.Resources.favicon); // or get it from resource
                var iconHandle = bitmap.GetHicon();
                var icon = System.Drawing.Icon.FromHandle(iconHandle);
                ni.Icon = icon;
                //ni.Click = new delegate void () {  };
                ni.Visible = true;
                ni.ShowBalloonTip(timeout, title, text, ToolTipIcon.Info);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            
        }

        public static Region RoundEdges(int width, int height, int radius = 10)
        {
            
                int x = 0, y = 0;
                GraphicsPath path = new GraphicsPath();
            try
            {
                path.AddLine(x + radius, y, x + width - (radius * 2), y); // Line
                path.AddArc(x + width - (radius * 2), y, radius * 2, radius * 2, 270, 90); // Top Right Corner
                path.AddLine(x + width, y + radius, x + width, y + height - (radius * 2)); // Line
                path.AddArc(x + width - (radius * 2), y + height - (radius * 2), radius * 2, radius * 2, 0, 90); // Bottom Right Corner
                path.AddLine(x + width - (radius * 2), y + height, x + radius, y + height); // Line
                path.AddArc(x, y + height - (radius * 2), radius * 2, radius * 2, 90, 90); //Bottom Left Corner
                path.AddLine(x, y + height - (radius * 2), x, y + radius); // Line
                path.AddArc(x, y, radius * 2, radius * 2, 180, 90); // Top Left Corner

            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            return new Region(path);
        }
        public static void SetupQLForm(QuickLaunch ql, string vertical = "TOP", string horizontal = "MID")
        {
            try
            {
              //  ql.Region = UIHelper.RoundEdges(ql.Width, ql.Height, 10);

                var hres = Screen.PrimaryScreen.Bounds.Width;
                var vres = Screen.PrimaryScreen.Bounds.Height;
                switch (vertical)
                {
                    case "MID":
                    case "BOTTOM":
                        ql.Top = vres - ql.Height - (Screen.PrimaryScreen.Bounds.Bottom - Screen.PrimaryScreen.WorkingArea.Bottom) + 3; //bottom of screen
                        break;
                    default: //TOP
                        ql.Top = 8;
                        break;
                }
                switch (horizontal)
                {
                    case "LEFT":
                        ql.Left = 0;
                        break;
                    case "RIGHT":
                        ql.Left = hres - ql.Width;
                        break;
                    default: //MID
                        ql.Left = (hres / 2) - (ql.Width / 2);
                        break;
                }
                //MessageBox.Show(this.Left.ToString() + " --- width: " + this.Width.ToString() + " --- hres: " + hres.ToString());
                if (ql.WindowState != FormWindowState.Normal)
                    ql.WindowState = FormWindowState.Normal;

             //   ql.pnlMain.Region = UIHelper.RoundEdges(ql.pnlMain.Width, ql.pnlMain.Height, 10); //pnlMain must be public
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        public static ToolTip SetupToolTip(ToolTip toolTip)
        {
            try
            {
                toolTip.AutoPopDelay = 5000;
                toolTip.InitialDelay = 500;
                toolTip.ReshowDelay = 500;
                toolTip.UseAnimation = true;
                toolTip.ShowAlways = true;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            return toolTip;
        }

        public static ToolStripMenuItem SetupToolStripMenuItem(ToolStripMenuItem tmi)
        {
            try
            {
                tmi.AutoSize = true;
                tmi.TextAlign = ContentAlignment.MiddleLeft;
                tmi.Anchor = AnchorStyles.Left;
                tmi.Margin = new System.Windows.Forms.Padding(0, 0, 0, 0);
                tmi.RightToLeft = System.Windows.Forms.RightToLeft.No;
                tmi.Font = new Font("Segoe UI", 9);
                tmi.ForeColor = Color.FromArgb(25, 80, 106);
                tmi.BackColor = Color.White;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }            
            return tmi;
        }

        public static void DisposeControls(Control.ControlCollection cc)
        {
            foreach (Control c in cc)
                c.Dispose();
        }
        public static void KillChildWindows(Form form, Type[] OfType)
        {
            try
            {
                foreach (Form f in form.OwnedForms)
                    if (f != null)
                        if (!f.IsDisposed)
                        {
                            for (int i = 0; i < OfType.Length; i++)
                            {
                                if (f.GetType() == OfType[i])
                                {
                                    f.Dispose();
                                    f.Close();
                                }
                            }
                        }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        public static void GrayOutImage(ref Image image)
        {
            try
            {
                ColorMatrix matrix = new ColorMatrix();
                var set = 1 / 3f; // Build color matrix set at 1/3
                for (int i = 0; i <= 2; i++)
                {
                    matrix[i, 0] = set;
                    matrix[i, 1] = set;
                    matrix[i, 2] = set;
                }

                ImageAttributes attributes = new ImageAttributes();
                attributes.SetColorMatrix(matrix);

                Graphics g = null;
                try
                {
                    g = Graphics.FromImage(image); // Get the graphics object from the image

                    // Redraw the image on the graphics object using the grayscale color matrix
                    g.DrawImage(image,
                        new Rectangle(0, 0, image.Width, image.Height),
                        0,
                        0,
                        image.Width,
                        image.Height,
                        GraphicsUnit.Pixel,
                        attributes);
                }
                finally
                {
                    if (g != null)
                        g.Dispose();
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
    }

}
