﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickLaunch
{
    
    public class WCFServer: IWCFServer
    {
        public void SendMessage(string user, string message)
        {
            
        }

        public void LockMachine(string user) //to do make configurable
        {
            try
            {
                QuickLaunch ql = getForm<QuickLaunch>();
                if (ql.CurrentUser.UserName.ToLower() == user.ToLower())
                { 
                    System.Type objBLType = System.Type.GetTypeFromProgID("WScript.Shell");
                    object objBL = System.Activator.CreateInstance(objBLType);
                    objBLType.InvokeMember("SendKeys", System.Reflection.BindingFlags.InvokeMethod, null, objBL, new object[] { "^q" });
                }
                QLUtility.checkQueue(user, "WorkstationLock");
            }
            catch (Exception) { }
        }

        private TForm getForm<TForm>()
    where TForm : Form
        {
            return (TForm)Application.OpenForms.OfType<TForm>().FirstOrDefault();
        }

        public void ClearCache()
        {
            try
            {
                foreach (string f in Directory.GetFiles(Utility.getPath(), "QL_*.xml"))
                {
                    File.Delete(f);
                }
            }
            catch (IOException ie)
            {

            }
        }

        //to do implement method
        public void SetCurrentUser(Shared.UserInfo user, Shared.ImprivataEvent imprivataEvent)
        {
            //throw new NotImplementedException();
        }
    }
}
