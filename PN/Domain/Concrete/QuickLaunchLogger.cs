﻿namespace QuickLaunch
{
    using com.citrix.wing;

    /// <summary>
    /// This is a component required for the ICA client. Add actual logging code to the method bodies if needed.
    /// </summary>
    public class QuickLaunchLogger : Logger
    {
        /// <summary>
        /// Logs the specified component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public string log(Component component, Message msg)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        public string log(Component component, MessageType severity, com.citrix.wing.util.LocalizableString msg, java.lang.Throwable t)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        public string log(Component component, MessageType severity, string msg, java.lang.Throwable t)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public string log(Component component, MessageType severity, com.citrix.wing.util.LocalizableString msg)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified component.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public string log(Component component, MessageType severity, string msg)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified MSG.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public string log(Message msg)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified severity.
        /// </summary>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        public string log(MessageType severity, com.citrix.wing.util.LocalizableString msg, java.lang.Throwable t)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified severity.
        /// </summary>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <param name="t">The t.</param>
        /// <returns></returns>
        public string log(MessageType severity, string msg, java.lang.Throwable t)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified severity.
        /// </summary>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public string log(MessageType severity, com.citrix.wing.util.LocalizableString msg)
        {
            return string.Empty;
        }

        /// <summary>
        /// Logs the specified severity.
        /// </summary>
        /// <param name="severity">The severity.</param>
        /// <param name="msg">The MSG.</param>
        /// <returns></returns>
        public string log(MessageType severity, string msg)
        {
            return string.Empty;
        }
    }
}