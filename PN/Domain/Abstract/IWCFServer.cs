﻿using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace QuickLaunch
{

    [ServiceContract]
    interface IWCFServer
    {
        [OperationContract]
        void SendMessage(string user, string message);

        [OperationContract]
        void LockMachine(string user);

        [OperationContract]
        void ClearCache();

        [OperationContract]
        void SetCurrentUser(UserInfo user, ImprivataEvent imprivataEvent);

    }
}
