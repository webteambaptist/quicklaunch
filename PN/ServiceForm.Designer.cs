﻿namespace QuickLaunch
{
    partial class ServiceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlStep1 = new System.Windows.Forms.Panel();
            this.lblPrn = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.txtSN = new System.Windows.Forms.TextBox();
            this.txtAltName = new System.Windows.Forms.TextBox();
            this.txtPhone2 = new System.Windows.Forms.MaskedTextBox();
            this.rbNo = new System.Windows.Forms.RadioButton();
            this.rbYes = new System.Windows.Forms.RadioButton();
            this.txtMsg = new System.Windows.Forms.TextBox();
            this.txtPhone1 = new System.Windows.Forms.MaskedTextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlStep2 = new System.Windows.Forms.Panel();
            this.lblTicket = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnQuit = new System.Windows.Forms.Button();
            this.txtIssue = new System.Windows.Forms.TextBox();
            this.lblAltName = new System.Windows.Forms.Label();
            this.lblPhone = new System.Windows.Forms.Label();
            this.lblLSID = new System.Windows.Forms.Label();
            this.lblSN = new System.Windows.Forms.Label();
            this.lblUID = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCampus = new System.Windows.Forms.TextBox();
            this.txtDept = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtFloor = new System.Windows.Forms.TextBox();
            this.pnlStep1.SuspendLayout();
            this.pnlStep2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlStep1
            // 
            this.pnlStep1.Controls.Add(this.txtFloor);
            this.pnlStep1.Controls.Add(this.label12);
            this.pnlStep1.Controls.Add(this.label11);
            this.pnlStep1.Controls.Add(this.label10);
            this.pnlStep1.Controls.Add(this.txtDept);
            this.pnlStep1.Controls.Add(this.txtCampus);
            this.pnlStep1.Controls.Add(this.label1);
            this.pnlStep1.Controls.Add(this.lblPrn);
            this.pnlStep1.Controls.Add(this.btnExit);
            this.pnlStep1.Controls.Add(this.btnSubmit);
            this.pnlStep1.Controls.Add(this.txtSN);
            this.pnlStep1.Controls.Add(this.txtAltName);
            this.pnlStep1.Controls.Add(this.txtPhone2);
            this.pnlStep1.Controls.Add(this.rbNo);
            this.pnlStep1.Controls.Add(this.rbYes);
            this.pnlStep1.Controls.Add(this.txtMsg);
            this.pnlStep1.Controls.Add(this.txtPhone1);
            this.pnlStep1.Controls.Add(this.label7);
            this.pnlStep1.Controls.Add(this.label6);
            this.pnlStep1.Controls.Add(this.label5);
            this.pnlStep1.Controls.Add(this.label4);
            this.pnlStep1.Controls.Add(this.label3);
            this.pnlStep1.Controls.Add(this.label2);
            this.pnlStep1.Location = new System.Drawing.Point(9, 10);
            this.pnlStep1.Margin = new System.Windows.Forms.Padding(2);
            this.pnlStep1.Name = "pnlStep1";
            this.pnlStep1.Size = new System.Drawing.Size(568, 431);
            this.pnlStep1.TabIndex = 0;
            // 
            // lblPrn
            // 
            this.lblPrn.AutoSize = true;
            this.lblPrn.Location = new System.Drawing.Point(311, 97);
            this.lblPrn.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPrn.Name = "lblPrn";
            this.lblPrn.Size = new System.Drawing.Size(187, 13);
            this.lblPrn.TabIndex = 10;
            this.lblPrn.Text = "If printer, please provide details below,";
            this.lblPrn.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(262, 387);
            this.btnExit.Margin = new System.Windows.Forms.Padding(2);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(75, 33);
            this.btnExit.TabIndex = 13;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(172, 387);
            this.btnSubmit.Margin = new System.Windows.Forms.Padding(2);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 33);
            this.btnSubmit.TabIndex = 12;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // txtSN
            // 
            this.txtSN.BackColor = System.Drawing.SystemColors.Info;
            this.txtSN.Location = new System.Drawing.Point(231, 94);
            this.txtSN.Margin = new System.Windows.Forms.Padding(2);
            this.txtSN.Name = "txtSN";
            this.txtSN.Size = new System.Drawing.Size(76, 20);
            this.txtSN.TabIndex = 3;
            this.txtSN.Visible = false;
            // 
            // txtAltName
            // 
            this.txtAltName.Location = new System.Drawing.Point(173, 151);
            this.txtAltName.Margin = new System.Windows.Forms.Padding(2);
            this.txtAltName.Name = "txtAltName";
            this.txtAltName.Size = new System.Drawing.Size(76, 20);
            this.txtAltName.TabIndex = 5;
            // 
            // txtPhone2
            // 
            this.txtPhone2.Location = new System.Drawing.Point(253, 151);
            this.txtPhone2.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhone2.Mask = "(999) 000-0000";
            this.txtPhone2.Name = "txtPhone2";
            this.txtPhone2.Size = new System.Drawing.Size(76, 20);
            this.txtPhone2.TabIndex = 6;
            this.txtPhone2.Click += new System.EventHandler(this.txtPhone2_Click);
            // 
            // rbNo
            // 
            this.rbNo.AutoSize = true;
            this.rbNo.Location = new System.Drawing.Point(270, 70);
            this.rbNo.Margin = new System.Windows.Forms.Padding(2);
            this.rbNo.Name = "rbNo";
            this.rbNo.Size = new System.Drawing.Size(39, 17);
            this.rbNo.TabIndex = 2;
            this.rbNo.TabStop = true;
            this.rbNo.Text = "No";
            this.rbNo.UseVisualStyleBackColor = true;
            this.rbNo.Click += new System.EventHandler(this.rbNo_Click);
            // 
            // rbYes
            // 
            this.rbYes.AutoSize = true;
            this.rbYes.Checked = true;
            this.rbYes.Location = new System.Drawing.Point(226, 70);
            this.rbYes.Margin = new System.Windows.Forms.Padding(2);
            this.rbYes.Name = "rbYes";
            this.rbYes.Size = new System.Drawing.Size(43, 17);
            this.rbYes.TabIndex = 1;
            this.rbYes.TabStop = true;
            this.rbYes.Text = "Yes";
            this.rbYes.UseVisualStyleBackColor = true;
            this.rbYes.Click += new System.EventHandler(this.rbYes_Click);
            // 
            // txtMsg
            // 
            this.txtMsg.BackColor = System.Drawing.SystemColors.Info;
            this.txtMsg.Location = new System.Drawing.Point(12, 250);
            this.txtMsg.Margin = new System.Windows.Forms.Padding(2);
            this.txtMsg.Multiline = true;
            this.txtMsg.Name = "txtMsg";
            this.txtMsg.Size = new System.Drawing.Size(546, 130);
            this.txtMsg.TabIndex = 11;
            // 
            // txtPhone1
            // 
            this.txtPhone1.BackColor = System.Drawing.SystemColors.Info;
            this.txtPhone1.Location = new System.Drawing.Point(382, 123);
            this.txtPhone1.Margin = new System.Windows.Forms.Padding(2);
            this.txtPhone1.Mask = "(999) 000-0000";
            this.txtPhone1.Name = "txtPhone1";
            this.txtPhone1.Size = new System.Drawing.Size(76, 20);
            this.txtPhone1.TabIndex = 4;
            this.txtPhone1.Click += new System.EventHandler(this.txtPhone1_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(22, 154);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(147, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Alternate Name and Number: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 123);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(358, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Please provide a contact number we can call if we need more information: ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 97);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(204, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "If no, please enter the computer\'s serial #:";
            this.label5.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 72);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Is assistance needed with this computer?";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 42);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(250, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "For immediate assistance, please call 904.202.7565";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(533, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "To submit a Service Desk ticket, please provide the following info:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // pnlStep2
            // 
            this.pnlStep2.Controls.Add(this.lblTicket);
            this.pnlStep2.Controls.Add(this.label8);
            this.pnlStep2.Controls.Add(this.btnQuit);
            this.pnlStep2.Controls.Add(this.txtIssue);
            this.pnlStep2.Controls.Add(this.lblAltName);
            this.pnlStep2.Controls.Add(this.lblPhone);
            this.pnlStep2.Controls.Add(this.lblLSID);
            this.pnlStep2.Controls.Add(this.lblSN);
            this.pnlStep2.Controls.Add(this.lblUID);
            this.pnlStep2.Controls.Add(this.label9);
            this.pnlStep2.Controls.Add(this.lblName);
            this.pnlStep2.Location = new System.Drawing.Point(0, 2);
            this.pnlStep2.Margin = new System.Windows.Forms.Padding(2);
            this.pnlStep2.Name = "pnlStep2";
            this.pnlStep2.Size = new System.Drawing.Size(566, 431);
            this.pnlStep2.TabIndex = 16;
            this.pnlStep2.Visible = false;
            // 
            // lblTicket
            // 
            this.lblTicket.AutoSize = true;
            this.lblTicket.Location = new System.Drawing.Point(223, 68);
            this.lblTicket.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblTicket.Name = "lblTicket";
            this.lblTicket.Size = new System.Drawing.Size(50, 13);
            this.lblTicket.TabIndex = 27;
            this.lblTicket.Text = "Ticket #:";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(9, 336);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(547, 28);
            this.label8.TabIndex = 26;
            this.label8.Text = "Thank you. This issue is now submitted to the Service Desk, for a Service Desk ag" +
    "ent to contact you.";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // btnQuit
            // 
            this.btnQuit.Location = new System.Drawing.Point(225, 387);
            this.btnQuit.Margin = new System.Windows.Forms.Padding(2);
            this.btnQuit.Name = "btnQuit";
            this.btnQuit.Size = new System.Drawing.Size(75, 33);
            this.btnQuit.TabIndex = 25;
            this.btnQuit.Text = "Exit";
            this.btnQuit.UseVisualStyleBackColor = true;
            this.btnQuit.Click += new System.EventHandler(this.btnQuit_Click);
            // 
            // txtIssue
            // 
            this.txtIssue.BackColor = System.Drawing.Color.LightGray;
            this.txtIssue.Location = new System.Drawing.Point(11, 175);
            this.txtIssue.Margin = new System.Windows.Forms.Padding(2);
            this.txtIssue.Multiline = true;
            this.txtIssue.Name = "txtIssue";
            this.txtIssue.ReadOnly = true;
            this.txtIssue.Size = new System.Drawing.Size(546, 159);
            this.txtIssue.TabIndex = 24;
            // 
            // lblAltName
            // 
            this.lblAltName.AutoSize = true;
            this.lblAltName.Location = new System.Drawing.Point(19, 149);
            this.lblAltName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblAltName.Name = "lblAltName";
            this.lblAltName.Size = new System.Drawing.Size(190, 13);
            this.lblAltName.TabIndex = 23;
            this.lblAltName.Text = "Alternate Contact and Phone Number: ";
            // 
            // lblPhone
            // 
            this.lblPhone.AutoSize = true;
            this.lblPhone.Location = new System.Drawing.Point(19, 123);
            this.lblPhone.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPhone.Name = "lblPhone";
            this.lblPhone.Size = new System.Drawing.Size(127, 13);
            this.lblPhone.TabIndex = 22;
            this.lblPhone.Text = "Contact Phone Number:  ";
            // 
            // lblLSID
            // 
            this.lblLSID.AutoSize = true;
            this.lblLSID.Location = new System.Drawing.Point(19, 97);
            this.lblLSID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLSID.Name = "lblLSID";
            this.lblLSID.Size = new System.Drawing.Size(105, 13);
            this.lblLSID.TabIndex = 21;
            this.lblLSID.Text = "UserID of computer: ";
            // 
            // lblSN
            // 
            this.lblSN.AutoSize = true;
            this.lblSN.Location = new System.Drawing.Point(19, 68);
            this.lblSN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSN.Name = "lblSN";
            this.lblSN.Size = new System.Drawing.Size(89, 13);
            this.lblSN.TabIndex = 20;
            this.lblSN.Text = "S/N of computer:";
            // 
            // lblUID
            // 
            this.lblUID.AutoSize = true;
            this.lblUID.Location = new System.Drawing.Point(223, 41);
            this.lblUID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblUID.Name = "lblUID";
            this.lblUID.Size = new System.Drawing.Size(49, 13);
            this.lblUID.TabIndex = 19;
            this.lblUID.Text = "User ID: ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 9);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(167, 20);
            this.label9.TabIndex = 18;
            this.label9.Text = "Service Desk Ticket";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(19, 41);
            this.lblName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(41, 13);
            this.lblName.TabIndex = 17;
            this.lblName.Text = "Name: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 185);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Please provide the location of the computer or printer:";
            // 
            // txtCampus
            // 
            this.txtCampus.Location = new System.Drawing.Point(77, 205);
            this.txtCampus.Margin = new System.Windows.Forms.Padding(2);
            this.txtCampus.Name = "txtCampus";
            this.txtCampus.Size = new System.Drawing.Size(92, 20);
            this.txtCampus.TabIndex = 7;
            // 
            // txtDept
            // 
            this.txtDept.Location = new System.Drawing.Point(267, 205);
            this.txtDept.Margin = new System.Windows.Forms.Padding(2);
            this.txtDept.Name = "txtDept";
            this.txtDept.Size = new System.Drawing.Size(113, 20);
            this.txtDept.TabIndex = 8;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(22, 208);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Campus: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(195, 208);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Department: ";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(399, 208);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(39, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Floor:  ";
            // 
            // txtFloor
            // 
            this.txtFloor.Location = new System.Drawing.Point(442, 205);
            this.txtFloor.Margin = new System.Windows.Forms.Padding(2);
            this.txtFloor.Name = "txtFloor";
            this.txtFloor.Size = new System.Drawing.Size(113, 20);
            this.txtFloor.TabIndex = 9;
            // 
            // ServiceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 451);
            this.Controls.Add(this.pnlStep1);
            this.Controls.Add(this.pnlStep2);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ServiceForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Submit a Service Desk Ticket For: ";
            this.pnlStep1.ResumeLayout(false);
            this.pnlStep1.PerformLayout();
            this.pnlStep2.ResumeLayout(false);
            this.pnlStep2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlStep1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MaskedTextBox txtPhone1;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.TextBox txtSN;
        private System.Windows.Forms.TextBox txtAltName;
        private System.Windows.Forms.MaskedTextBox txtPhone2;
        private System.Windows.Forms.RadioButton rbNo;
        private System.Windows.Forms.RadioButton rbYes;
        private System.Windows.Forms.TextBox txtMsg;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Panel pnlStep2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnQuit;
        private System.Windows.Forms.TextBox txtIssue;
        private System.Windows.Forms.Label lblAltName;
        private System.Windows.Forms.Label lblPhone;
        private System.Windows.Forms.Label lblLSID;
        private System.Windows.Forms.Label lblSN;
        private System.Windows.Forms.Label lblUID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblPrn;
        private System.Windows.Forms.Label lblTicket;
        private System.Windows.Forms.TextBox txtFloor;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtDept;
        private System.Windows.Forms.TextBox txtCampus;
        private System.Windows.Forms.Label label1;
    }
}