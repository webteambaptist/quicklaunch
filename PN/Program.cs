namespace QuickLaunch
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    using global::QuickLaunch.Shared;
    using System.Reflection;
    using System.Threading;

    /// <summary>
    /// This class provides the entry point for the Quick Launch UI.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Loads the library.
        /// </summary>
        /// <param name="lpFileName">Name of the lp file.</param>
        /// <returns></returns>
        [DllImport("kernel32", SetLastError = true)]
        static extern IntPtr LoadLibrary(string lpFileName);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        /// <summary>
        /// This reference allows the <see cref="QuickLaunch.ServerMethods">ServerMethods</see> class
        /// to make calls to the <see cref="QuickLaunch.QuickLaunch">QuickLaunch</see> form.
        /// </summary>
        public static QuickLaunch quickLaunch;

        public delegate void AsyncMethodCaller();

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool createdNew = true;

            using (Mutex mutex = new Mutex(true, "Quick Launch", out createdNew))
            {
                if (createdNew)
                {

                    try
                    {
                        AsyncMethodCaller setupCaller = new AsyncMethodCaller(SetupProgram);
                        IAsyncResult resultSetup = setupCaller.BeginInvoke(null, null); // Initiate the async call.    
                        TC.Animation.Animator.Initialize(); //QuickLaunch dependent on this
                        quickLaunch = new QuickLaunch(); //time consuming            
                        QLServerMethods.Setup(); //this is dependent on notifications being set
                        setupCaller.EndInvoke(resultSetup); //wait on result setup (should be done already)
                                                            //start service


                        Application.Run(quickLaunch);
                    }
                    catch (Exception ee)
                    {
                        Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
                    }
                }
                else
                {
                    Process current = Process.GetCurrentProcess();
                    foreach (Process process in Process.GetProcessesByName(current.ProcessName))
                    {
                        if (process.Id != current.Id)
                        {
                            SetForegroundWindow(process.MainWindowHandle);
                            break;
                        }
                    }
                }
            }          
        }

        /// <summary>
        /// Setups common application settings. Performed async for faster startup.
        /// </summary>
        private static void SetupProgram()
        {
            try
            {
                //This was added so that the program can be migrated to .Net 4.x if needed.
                if (Environment.Version.Major >= 4)
                {
                    string folder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), @"..\Microsoft.NET\Framework\v2.0.50727");
                    folder = Path.GetFullPath(folder);
                    LoadLibrary(Path.Combine(folder, "vjsnativ.dll"));
                }

                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
                Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }                        
        }

        /// <summary>
        /// Handles the UnhandledException event of the CurrentDomain control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.UnhandledExceptionEventArgs"/> instance containing the event data.</param>
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                MessageBox.Show(
                    "A fatal error occured in Quick Launch. Tap out and tap back in to restart Quick Launch.", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogException((Exception)e.ExceptionObject, true);
            }
            finally
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Handles the ThreadException event of the App control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.Threading.ThreadExceptionEventArgs"/> instance containing the event data.</param>
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            try
            {
                MessageBox.Show(
                    "A fatal error occured in Quick Launch. Tap out and tap back in to restart Quick Launch.", "Fatal Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogException(e.Exception, true);
            }
            finally
            {
                Application.Exit();
            }
        }

        /// <summary>
        /// Logs the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void LogException(Exception ex, bool IsFatal)
        {
            try
            {
                if (IsFatal)
                {
                    EventLog myLog = new EventLog();
                    myLog.Source = "Quick Launch";
                    myLog.WriteEntry("Version " + Assembly.GetExecutingAssembly().GetName().Version + "\n\n" + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace, EventLogEntryType.Error);
                }

                //if (DBManager.HasDBConnection())

                try
                {
                    Models.ErrorLog.AddErrorToLog(ex, QuickLaunch.CurrentUserId ?? "N/A", "QUICKLAUNCH", IsFatal);
                }
                catch(Exception ee)
                { }

                if (ex.InnerException != null)
                {
                    LogException(ex.InnerException, IsFatal);
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            finally
            {
                try
                {
                    Truncus.LogEvents("1", System.Reflection.MethodBase.GetCurrentMethod().Name, QuickLaunch.CurrentUserId ?? "N/A", 0, "ERROR");
                }
                catch (Exception ee) { }
            }
        }

        /// <summary>
        /// Sets the current user.
        /// </summary>
        /// <param name="user">The user.</param>
        public static void SetCurrentUser(UserInfo user, ImprivataEvent imprivataEvent)
        {
            if (user != null)
            {
                try
                {
                    // quickLaunch.SetCurrentUserStartTime = DateTime.Now; //record how long logins are taking

                    if (quickLaunch.timerLogin.IsRunning)
                    {
                        quickLaunch.timerLogin.Stop();
                        quickLaunch.timerLogin.Reset();
                        quickLaunch.timerLogin.Start();
                    }
                    else
                    {
                        quickLaunch.timerLogin.Reset();
                        quickLaunch.timerLogin.Start();
                    }

                    if (quickLaunch.InvokeRequired)
                    {
                        quickLaunch.BeginInvoke(new MethodInvoker(() => SetCurrentUser(user, imprivataEvent)));
                    }
                    else
                    {
                        quickLaunch.SetCurrentUser(user, imprivataEvent);
                    }
                }
                catch (Exception ex)
                {
                    AutoClosingMessageBox.Show("Error setting user", "Quick Launch Closing", 9000);
                    Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, user.UserName, 0, "ERROR");

                    //MessageBox.Show(user.UserName + imprivataEvent.ToString() + (quickLaunch == null ? " : Yep its null" : quickLaunch.CurrentUser.UserType) + "\n" + ex.ToString());
                } //Quick Launch not ready for invocation
            }
            else
            {
                AutoClosingMessageBox.Show("Error, user is null or missing. Quick Launch will close, please Tap Out and Tap In.", "Quick Launch Closing", 9000);
                Truncus.LogEvents("1", "ERROR - NULL USER", "", 0, "SetCurrentUser");
                Application.Exit();
            }
        }



   }
}