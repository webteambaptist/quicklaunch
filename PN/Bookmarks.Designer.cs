﻿namespace QuickLaunch
{
    partial class Bookmarks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Bookmarks");
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.pbBookmarkHelp = new System.Windows.Forms.PictureBox();
            this.pbClose = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pbIcon = new System.Windows.Forms.PictureBox();
            this.bSave = new System.Windows.Forms.Button();
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.lblDropFiles = new System.Windows.Forms.Label();
            this.pnlModify = new System.Windows.Forms.Panel();
            this.bCancel = new System.Windows.Forms.Button();
            this.gbUrl = new System.Windows.Forms.GroupBox();
            this.tUrl = new System.Windows.Forms.TextBox();
            this.gbTitle = new System.Windows.Forms.GroupBox();
            this.tTitle = new System.Windows.Forms.TextBox();
            this.lAddbookmark = new System.Windows.Forms.Label();
            this.tvBookmarks = new System.Windows.Forms.TreeView();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBookmarkHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).BeginInit();
            this.pnlContainer.SuspendLayout();
            this.pnlModify.SuspendLayout();
            this.gbUrl.SuspendLayout();
            this.gbTitle.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.pnlHeader.Controls.Add(this.pbBookmarkHelp);
            this.pnlHeader.Controls.Add(this.pbClose);
            this.pnlHeader.Controls.Add(this.lblTitle);
            this.pnlHeader.Controls.Add(this.pbIcon);
            this.pnlHeader.Location = new System.Drawing.Point(-17, -4);
            this.pnlHeader.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(631, 37);
            this.pnlHeader.TabIndex = 17;
            // 
            // pbBookmarkHelp
            // 
            this.pbBookmarkHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbBookmarkHelp.Image = global::QuickLaunch.Properties.Resources.HelpPNG;
            this.pbBookmarkHelp.Location = new System.Drawing.Point(520, 6);
            this.pbBookmarkHelp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbBookmarkHelp.Name = "pbBookmarkHelp";
            this.pbBookmarkHelp.Size = new System.Drawing.Size(20, 18);
            this.pbBookmarkHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbBookmarkHelp.TabIndex = 21;
            this.pbBookmarkHelp.TabStop = false;
            this.pbBookmarkHelp.Visible = false;
            this.pbBookmarkHelp.Click += new System.EventHandler(this.pbBookmarkHelp_Click);
            // 
            // pbClose
            // 
            this.pbClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbClose.Image = global::QuickLaunch.Properties.Resources.Close;
            this.pbClose.Location = new System.Drawing.Point(551, 0);
            this.pbClose.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(63, 27);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbClose.TabIndex = 2;
            this.pbClose.TabStop = false;
            this.pbClose.Click += new System.EventHandler(this.pbClose_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(47, 12);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(146, 16);
            this.lblTitle.TabIndex = 20;
            this.lblTitle.Text = "Manage Bookmarks";
            // 
            // pbIcon
            // 
            this.pbIcon.Image = global::QuickLaunch.Properties.Resources.favicon;
            this.pbIcon.Location = new System.Drawing.Point(24, 10);
            this.pbIcon.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pbIcon.Name = "pbIcon";
            this.pbIcon.Size = new System.Drawing.Size(21, 20);
            this.pbIcon.TabIndex = 0;
            this.pbIcon.TabStop = false;
            // 
            // bSave
            // 
            this.bSave.Location = new System.Drawing.Point(169, 169);
            this.bSave.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bSave.Name = "bSave";
            this.bSave.Size = new System.Drawing.Size(119, 42);
            this.bSave.TabIndex = 3;
            this.bSave.Text = "Save";
            this.bSave.UseVisualStyleBackColor = true;
            this.bSave.Click += new System.EventHandler(this.bSave_Click);
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.SystemColors.Control;
            this.pnlContainer.Controls.Add(this.lblDropFiles);
            this.pnlContainer.Controls.Add(this.pnlModify);
            this.pnlContainer.Controls.Add(this.lAddbookmark);
            this.pnlContainer.Controls.Add(this.tvBookmarks);
            this.pnlContainer.Location = new System.Drawing.Point(4, 31);
            this.pnlContainer.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(595, 313);
            this.pnlContainer.TabIndex = 16;
            // 
            // lblDropFiles
            // 
            this.lblDropFiles.AllowDrop = true;
            this.lblDropFiles.BackColor = System.Drawing.Color.Honeydew;
            this.lblDropFiles.Location = new System.Drawing.Point(12, 265);
            this.lblDropFiles.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblDropFiles.Name = "lblDropFiles";
            this.lblDropFiles.Size = new System.Drawing.Size(571, 42);
            this.lblDropFiles.TabIndex = 13;
            this.lblDropFiles.Text = "Drop Text or Urls Here";
            this.lblDropFiles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlModify
            // 
            this.pnlModify.BackColor = System.Drawing.Color.SeaGreen;
            this.pnlModify.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlModify.Controls.Add(this.bCancel);
            this.pnlModify.Controls.Add(this.bSave);
            this.pnlModify.Controls.Add(this.gbUrl);
            this.pnlModify.Controls.Add(this.gbTitle);
            this.pnlModify.Location = new System.Drawing.Point(12, 10);
            this.pnlModify.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pnlModify.Name = "pnlModify";
            this.pnlModify.Size = new System.Drawing.Size(569, 250);
            this.pnlModify.TabIndex = 18;
            this.pnlModify.Visible = false;
            // 
            // bCancel
            // 
            this.bCancel.Location = new System.Drawing.Point(291, 169);
            this.bCancel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.bCancel.Name = "bCancel";
            this.bCancel.Size = new System.Drawing.Size(119, 42);
            this.bCancel.TabIndex = 4;
            this.bCancel.Text = "Cancel";
            this.bCancel.UseVisualStyleBackColor = true;
            this.bCancel.Click += new System.EventHandler(this.bCancel_Click);
            // 
            // gbUrl
            // 
            this.gbUrl.Controls.Add(this.tUrl);
            this.gbUrl.ForeColor = System.Drawing.Color.FloralWhite;
            this.gbUrl.Location = new System.Drawing.Point(29, 81);
            this.gbUrl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUrl.Name = "gbUrl";
            this.gbUrl.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbUrl.Size = new System.Drawing.Size(507, 52);
            this.gbUrl.TabIndex = 11;
            this.gbUrl.TabStop = false;
            this.gbUrl.Text = "Url";
            // 
            // tUrl
            // 
            this.tUrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tUrl.Location = new System.Drawing.Point(4, 19);
            this.tUrl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tUrl.Name = "tUrl";
            this.tUrl.Size = new System.Drawing.Size(499, 22);
            this.tUrl.TabIndex = 2;
            // 
            // gbTitle
            // 
            this.gbTitle.Controls.Add(this.tTitle);
            this.gbTitle.ForeColor = System.Drawing.Color.FloralWhite;
            this.gbTitle.Location = new System.Drawing.Point(29, 15);
            this.gbTitle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTitle.Name = "gbTitle";
            this.gbTitle.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gbTitle.Size = new System.Drawing.Size(507, 52);
            this.gbTitle.TabIndex = 10;
            this.gbTitle.TabStop = false;
            this.gbTitle.Text = "Title";
            // 
            // tTitle
            // 
            this.tTitle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tTitle.Location = new System.Drawing.Point(4, 19);
            this.tTitle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tTitle.Name = "tTitle";
            this.tTitle.Size = new System.Drawing.Size(499, 22);
            this.tTitle.TabIndex = 1;
            // 
            // lAddbookmark
            // 
            this.lAddbookmark.AutoSize = true;
            this.lAddbookmark.BackColor = System.Drawing.Color.Transparent;
            this.lAddbookmark.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lAddbookmark.ForeColor = System.Drawing.Color.DarkGreen;
            this.lAddbookmark.Location = new System.Drawing.Point(511, 11);
            this.lAddbookmark.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lAddbookmark.Name = "lAddbookmark";
            this.lAddbookmark.Size = new System.Drawing.Size(45, 17);
            this.lAddbookmark.TabIndex = 10;
            this.lAddbookmark.Text = "+ Add";
            this.lAddbookmark.Click += new System.EventHandler(this.lAddbookmark_Click);
            // 
            // tvBookmarks
            // 
            this.tvBookmarks.AllowDrop = true;
            this.tvBookmarks.LabelEdit = true;
            this.tvBookmarks.Location = new System.Drawing.Point(12, 10);
            this.tvBookmarks.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tvBookmarks.Name = "tvBookmarks";
            treeNode1.Name = "nodeBookmark";
            treeNode1.Tag = "0";
            treeNode1.Text = "Bookmarks";
            this.tvBookmarks.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.tvBookmarks.ShowNodeToolTips = true;
            this.tvBookmarks.Size = new System.Drawing.Size(569, 250);
            this.tvBookmarks.TabIndex = 8;
            // 
            // Bookmarks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(603, 348);
            this.Controls.Add(this.pnlHeader);
            this.Controls.Add(this.pnlContainer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Bookmarks";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbBookmarkHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.pnlContainer.PerformLayout();
            this.pnlModify.ResumeLayout(false);
            this.gbUrl.ResumeLayout(false);
            this.gbUrl.PerformLayout();
            this.gbTitle.ResumeLayout(false);
            this.gbTitle.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox pbIcon;
        private System.Windows.Forms.Button bSave;
        private System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.GroupBox gbUrl;
        private System.Windows.Forms.TextBox tUrl;
        private System.Windows.Forms.GroupBox gbTitle;
        private System.Windows.Forms.TextBox tTitle;
        private System.Windows.Forms.TreeView tvBookmarks;
        private System.Windows.Forms.Panel pnlModify;
        private System.Windows.Forms.Button bCancel;
        private System.Windows.Forms.Label lAddbookmark;
        private System.Windows.Forms.PictureBox pbClose;
        private System.Windows.Forms.Label lblDropFiles;
        private System.Windows.Forms.PictureBox pbBookmarkHelp;
    }
}