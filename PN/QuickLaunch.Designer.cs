﻿namespace QuickLaunch
{
    partial class QuickLaunch
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuickLaunch));
            this.pnlMain = new System.Windows.Forms.Panel();
            this.pnlFooter = new System.Windows.Forms.Panel();
            this.lblVersion = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.lblTapOut = new System.Windows.Forms.Label();
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.menuStripUser = new System.Windows.Forms.MenuStrip();
            this.toolStripUser = new System.Windows.Forms.ToolStripMenuItem();
            this.myProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.myFavsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsMyApps = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiEMRSession = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDisconnect = new System.Windows.Forms.ToolStripMenuItem();
            this.tsLogoff = new System.Windows.Forms.ToolStripMenuItem();
            this.resetConnectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlIconNotifications = new System.Windows.Forms.FlowLayoutPanel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pbIcon = new System.Windows.Forms.PictureBox();
            this.pnlBtnShadow = new System.Windows.Forms.Panel();
            this.pnlNotifier = new System.Windows.Forms.Panel();
            this.progressBar = new MetroFramework.Controls.MetroProgressBar();
            this.lblNotifier = new System.Windows.Forms.Label();
            this.pnlFlowContainer = new System.Windows.Forms.FlowLayoutPanel();
            this.PowerChart = new System.Windows.Forms.PictureBox();
            this.FirstNet = new System.Windows.Forms.PictureBox();
            this.SurgiNet = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pnlMain.SuspendLayout();
            this.pnlFooter.SuspendLayout();
            this.pnlHeader.SuspendLayout();
            this.menuStripUser.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).BeginInit();
            this.pnlBtnShadow.SuspendLayout();
            this.pnlNotifier.SuspendLayout();
            this.pnlFlowContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PowerChart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurgiNet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlMain
            // 
            this.pnlMain.AutoSize = true;
            this.pnlMain.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(36)))), ((int)(((byte)(123)))), ((int)(((byte)(165)))));
            this.pnlMain.Controls.Add(this.pnlFooter);
            this.pnlMain.Controls.Add(this.pnlHeader);
            this.pnlMain.Controls.Add(this.pnlBtnShadow);
            this.pnlMain.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pnlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlMain.Location = new System.Drawing.Point(3, 0);
            this.pnlMain.Margin = new System.Windows.Forms.Padding(0);
            this.pnlMain.Name = "pnlMain";
            this.pnlMain.Size = new System.Drawing.Size(346, 136);
            this.pnlMain.TabIndex = 8;
            // 
            // pnlFooter
            // 
            this.pnlFooter.AllowDrop = true;
            this.pnlFooter.AutoSize = true;
            this.pnlFooter.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlFooter.BackColor = System.Drawing.Color.Transparent;
            this.pnlFooter.Controls.Add(this.lblVersion);
            this.pnlFooter.Controls.Add(this.linkLabel1);
            this.pnlFooter.Controls.Add(this.lblTapOut);
            this.pnlFooter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlFooter.Location = new System.Drawing.Point(0, 97);
            this.pnlFooter.Name = "pnlFooter";
            this.pnlFooter.Padding = new System.Windows.Forms.Padding(5);
            this.pnlFooter.Size = new System.Drawing.Size(346, 39);
            this.pnlFooter.TabIndex = 16;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVersion.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblVersion.Location = new System.Drawing.Point(6, 2);
            this.lblVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(13, 13);
            this.lblVersion.TabIndex = 13;
            this.lblVersion.Text = "v";
            // 
            // linkLabel1
            // 
            this.linkLabel1.ActiveLinkColor = System.Drawing.Color.White;
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.linkLabel1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(6, 20);
            this.linkLabel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(149, 13);
            this.linkLabel1.TabIndex = 12;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Click to Contact Service Desk";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel1.VisitedLinkColor = System.Drawing.Color.White;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // lblTapOut
            // 
            this.lblTapOut.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTapOut.AutoSize = true;
            this.lblTapOut.BackColor = System.Drawing.Color.Transparent;
            this.lblTapOut.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblTapOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTapOut.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblTapOut.Location = new System.Drawing.Point(284, 18);
            this.lblTapOut.Name = "lblTapOut";
            this.lblTapOut.Size = new System.Drawing.Size(57, 15);
            this.lblTapOut.TabIndex = 10;
            this.lblTapOut.Text = "Tap Out";
            this.lblTapOut.Click += new System.EventHandler(this.lblTapOut_Click);
            this.lblTapOut.MouseEnter += new System.EventHandler(this.lblTapOut_MouseEnter);
            this.lblTapOut.MouseLeave += new System.EventHandler(this.lblTapOut_MouseLeave);
            // 
            // pnlHeader
            // 
            this.pnlHeader.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlHeader.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(88)))), ((int)(((byte)(118)))));
            this.pnlHeader.Controls.Add(this.menuStripUser);
            this.pnlHeader.Controls.Add(this.pnlIconNotifications);
            this.pnlHeader.Controls.Add(this.lblTitle);
            this.pnlHeader.Controls.Add(this.pbIcon);
            this.pnlHeader.Location = new System.Drawing.Point(0, 0);
            this.pnlHeader.Margin = new System.Windows.Forms.Padding(0);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(378, 25);
            this.pnlHeader.TabIndex = 5;
            // 
            // menuStripUser
            // 
            this.menuStripUser.AllowItemReorder = true;
            this.menuStripUser.BackColor = System.Drawing.Color.Transparent;
            this.menuStripUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStripUser.Dock = System.Windows.Forms.DockStyle.Right;
            this.menuStripUser.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripUser.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripUser});
            this.menuStripUser.Location = new System.Drawing.Point(286, 0);
            this.menuStripUser.Name = "menuStripUser";
            this.menuStripUser.Padding = new System.Windows.Forms.Padding(0);
            this.menuStripUser.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStripUser.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.menuStripUser.Size = new System.Drawing.Size(92, 25);
            this.menuStripUser.TabIndex = 4;
            this.menuStripUser.Text = "menuStrip1";
            this.menuStripUser.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.menuStripUser_ItemClicked);
            this.menuStripUser.MouseClick += new System.Windows.Forms.MouseEventHandler(this.menuStripUser_MouseClick);
            this.menuStripUser.MouseHover += new System.EventHandler(this.menuStripUser_MouseHover);
            // 
            // toolStripUser
            // 
            this.toolStripUser.AutoToolTip = true;
            this.toolStripUser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(88)))), ((int)(((byte)(118)))));
            this.toolStripUser.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripUser.DoubleClickEnabled = true;
            this.toolStripUser.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.myProfileToolStripMenuItem,
            this.myFavsMenuItem,
            this.tsMyApps,
            this.tsmiEMRSession,
            this.resetConnectionToolStripMenuItem});
            this.toolStripUser.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.toolStripUser.ForeColor = System.Drawing.Color.DarkGray;
            this.toolStripUser.Margin = new System.Windows.Forms.Padding(0, 0, 26, 0);
            this.toolStripUser.Name = "toolStripUser";
            this.toolStripUser.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripUser.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.toolStripUser.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.U)));
            this.toolStripUser.Size = new System.Drawing.Size(65, 20);
            this.toolStripUser.Text = "User";
            this.toolStripUser.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripUser.ToolTipText = "Click to view more options";
            this.toolStripUser.DoubleClick += new System.EventHandler(this.myProfileToolStripMenuItem_Click);
            this.toolStripUser.MouseEnter += new System.EventHandler(this.myProfileToolStripMenuItem_MouseEnter);
            this.toolStripUser.MouseHover += new System.EventHandler(this.myProfileToolStripMenuItem_MouseEnter);
            // 
            // myProfileToolStripMenuItem
            // 
            this.myProfileToolStripMenuItem.AutoToolTip = true;
            this.myProfileToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.myProfileToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.myProfileToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.myProfileToolStripMenuItem.Name = "myProfileToolStripMenuItem";
            this.myProfileToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.myProfileToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.myProfileToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.P)));
            this.myProfileToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.myProfileToolStripMenuItem.Text = "My Profile";
            this.myProfileToolStripMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.myProfileToolStripMenuItem.ToolTipText = "Opens user profile allowing updates to such items as auto EMR launch";
            this.myProfileToolStripMenuItem.Click += new System.EventHandler(this.myProfileToolStripMenuItem_Click);
            // 
            // myFavsMenuItem
            // 
            this.myFavsMenuItem.AutoToolTip = true;
            this.myFavsMenuItem.BackColor = System.Drawing.Color.Gainsboro;
            this.myFavsMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.myFavsMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.myFavsMenuItem.Name = "myFavsMenuItem";
            this.myFavsMenuItem.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.myFavsMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.myFavsMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.B)));
            this.myFavsMenuItem.Size = new System.Drawing.Size(184, 26);
            this.myFavsMenuItem.Text = "Bookmarks";
            this.myFavsMenuItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.myFavsMenuItem.ToolTipText = "Click to modify your bookmarks. You can drag and drop urls onto the Quick Launch," +
    " and use these bookmarks on any computer you tap into.";
            this.myFavsMenuItem.Click += new System.EventHandler(this.myFavsMenuItem_Click);
            // 
            // tsMyApps
            // 
            this.tsMyApps.AutoToolTip = true;
            this.tsMyApps.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsMyApps.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsMyApps.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.tsMyApps.Name = "tsMyApps";
            this.tsMyApps.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.tsMyApps.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsMyApps.Size = new System.Drawing.Size(184, 26);
            this.tsMyApps.Text = "Applications";
            this.tsMyApps.ToolTipText = "My Citrix Applications";
            // 
            // tsmiEMRSession
            // 
            this.tsmiEMRSession.AutoToolTip = true;
            this.tsmiEMRSession.BackColor = System.Drawing.Color.Gainsboro;
            this.tsmiEMRSession.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsDisconnect,
            this.tsLogoff});
            this.tsmiEMRSession.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.tsmiEMRSession.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.tsmiEMRSession.Name = "tsmiEMRSession";
            this.tsmiEMRSession.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.tsmiEMRSession.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsmiEMRSession.Size = new System.Drawing.Size(184, 26);
            this.tsmiEMRSession.Text = "EMR Sessions";
            this.tsmiEMRSession.ToolTipText = "My current EMR sessions";
            // 
            // tsDisconnect
            // 
            this.tsDisconnect.AutoToolTip = true;
            this.tsDisconnect.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tsDisconnect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.tsDisconnect.Name = "tsDisconnect";
            this.tsDisconnect.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsDisconnect.Size = new System.Drawing.Size(178, 22);
            this.tsDisconnect.Text = "Roam EMR Session";
            this.tsDisconnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsDisconnect.ToolTipText = "Quick EMR Reset, that takes about 3 to 5 seconds. It disconnects all EMR sessions" +
    " on the network.";
            this.tsDisconnect.Click += new System.EventHandler(this.tsDisconnect_Click);
            // 
            // tsLogoff
            // 
            this.tsLogoff.AutoToolTip = true;
            this.tsLogoff.BackColor = System.Drawing.Color.Gainsboro;
            this.tsLogoff.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.tsLogoff.Name = "tsLogoff";
            this.tsLogoff.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tsLogoff.Size = new System.Drawing.Size(178, 22);
            this.tsLogoff.Text = "Kill EMR Session";
            this.tsLogoff.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.tsLogoff.ToolTipText = "Full EMR Reset, that takes about 65 seconds. It logs off all EMR sessions on the " +
    "network. Please do not interrupt this process.";
            this.tsLogoff.Click += new System.EventHandler(this.tsLogoff_Click);
            // 
            // resetConnectionToolStripMenuItem
            // 
            this.resetConnectionToolStripMenuItem.BackColor = System.Drawing.Color.WhiteSmoke;
            this.resetConnectionToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Bold);
            this.resetConnectionToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.resetConnectionToolStripMenuItem.Name = "resetConnectionToolStripMenuItem";
            this.resetConnectionToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 3, 0, 3);
            this.resetConnectionToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.resetConnectionToolStripMenuItem.Size = new System.Drawing.Size(184, 26);
            this.resetConnectionToolStripMenuItem.Text = "Reset Quick Launch";
            this.resetConnectionToolStripMenuItem.Click += new System.EventHandler(this.resetConnectionToolStripMenuItem_Click);
            // 
            // pnlIconNotifications
            // 
            this.pnlIconNotifications.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pnlIconNotifications.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlIconNotifications.Location = new System.Drawing.Point(188, 3);
            this.pnlIconNotifications.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.pnlIconNotifications.Name = "pnlIconNotifications";
            this.pnlIconNotifications.Size = new System.Drawing.Size(71, 19);
            this.pnlIconNotifications.TabIndex = 3;
            // 
            // lblTitle
            // 
            this.lblTitle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lblTitle.Location = new System.Drawing.Point(19, 7);
            this.lblTitle.Margin = new System.Windows.Forms.Padding(5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(159, 14);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Baptist Health Quick Launch";
            // 
            // pbIcon
            // 
            this.pbIcon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.pbIcon.Image = global::QuickLaunch.Properties.Resources.favicon;
            this.pbIcon.Location = new System.Drawing.Point(1, 5);
            this.pbIcon.Margin = new System.Windows.Forms.Padding(0);
            this.pbIcon.Name = "pbIcon";
            this.pbIcon.Size = new System.Drawing.Size(16, 22);
            this.pbIcon.TabIndex = 0;
            this.pbIcon.TabStop = false;
            this.pbIcon.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pbIcon_MouseDoubleClick);
            // 
            // pnlBtnShadow
            // 
            this.pnlBtnShadow.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBtnShadow.AutoSize = true;
            this.pnlBtnShadow.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlBtnShadow.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.pnlBtnShadow.Controls.Add(this.pnlNotifier);
            this.pnlBtnShadow.Controls.Add(this.pnlFlowContainer);
            this.pnlBtnShadow.Location = new System.Drawing.Point(5, 26);
            this.pnlBtnShadow.Margin = new System.Windows.Forms.Padding(3, 3, 3, 35);
            this.pnlBtnShadow.Name = "pnlBtnShadow";
            this.pnlBtnShadow.Padding = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.pnlBtnShadow.Size = new System.Drawing.Size(336, 73);
            this.pnlBtnShadow.TabIndex = 7;
            // 
            // pnlNotifier
            // 
            this.pnlNotifier.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlNotifier.BackColor = System.Drawing.Color.Gainsboro;
            this.pnlNotifier.Controls.Add(this.progressBar);
            this.pnlNotifier.Controls.Add(this.lblNotifier);
            this.pnlNotifier.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.pnlNotifier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlNotifier.Location = new System.Drawing.Point(3, 2);
            this.pnlNotifier.Name = "pnlNotifier";
            this.pnlNotifier.Size = new System.Drawing.Size(330, 68);
            this.pnlNotifier.TabIndex = 0;
            this.pnlNotifier.Visible = false;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.progressBar.Location = new System.Drawing.Point(77, 36);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(176, 20);
            this.progressBar.Style = MetroFramework.MetroColorStyle.Green;
            this.progressBar.TabIndex = 1;
            this.progressBar.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // lblNotifier
            // 
            this.lblNotifier.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblNotifier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblNotifier.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.lblNotifier.Location = new System.Drawing.Point(0, 0);
            this.lblNotifier.Margin = new System.Windows.Forms.Padding(5);
            this.lblNotifier.Name = "lblNotifier";
            this.lblNotifier.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.lblNotifier.Size = new System.Drawing.Size(330, 68);
            this.lblNotifier.TabIndex = 0;
            this.lblNotifier.Text = "Searching for session";
            this.lblNotifier.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnlFlowContainer
            // 
            this.pnlFlowContainer.AutoSize = true;
            this.pnlFlowContainer.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pnlFlowContainer.BackColor = System.Drawing.Color.White;
            this.pnlFlowContainer.Controls.Add(this.PowerChart);
            this.pnlFlowContainer.Controls.Add(this.FirstNet);
            this.pnlFlowContainer.Controls.Add(this.SurgiNet);
            this.pnlFlowContainer.Controls.Add(this.pictureBox1);
            this.pnlFlowContainer.Controls.Add(this.pictureBox2);
            this.pnlFlowContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFlowContainer.Location = new System.Drawing.Point(3, 2);
            this.pnlFlowContainer.Name = "pnlFlowContainer";
            this.pnlFlowContainer.Size = new System.Drawing.Size(330, 68);
            this.pnlFlowContainer.TabIndex = 17;
            // 
            // PowerChart
            // 
            this.PowerChart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PowerChart.Image = global::QuickLaunch.Properties.Resources.citrix_shield_normal;
            this.PowerChart.Location = new System.Drawing.Point(3, 3);
            this.PowerChart.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.PowerChart.Name = "PowerChart";
            this.PowerChart.Size = new System.Drawing.Size(60, 60);
            this.PowerChart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PowerChart.TabIndex = 0;
            this.PowerChart.TabStop = false;
            this.PowerChart.Click += new System.EventHandler(this.Citrix_Click);
            // 
            // FirstNet
            // 
            this.FirstNet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.FirstNet.Image = global::QuickLaunch.Properties.Resources.citrix_ambulance_normal;
            this.FirstNet.Location = new System.Drawing.Point(69, 3);
            this.FirstNet.Name = "FirstNet";
            this.FirstNet.Size = new System.Drawing.Size(60, 60);
            this.FirstNet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.FirstNet.TabIndex = 1;
            this.FirstNet.TabStop = false;
            this.FirstNet.Click += new System.EventHandler(this.Citrix_Click);
            // 
            // SurgiNet
            // 
            this.SurgiNet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SurgiNet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SurgiNet.Image = global::QuickLaunch.Properties.Resources.citrix_drhead_normal;
            this.SurgiNet.Location = new System.Drawing.Point(135, 3);
            this.SurgiNet.Name = "SurgiNet";
            this.SurgiNet.Size = new System.Drawing.Size(60, 60);
            this.SurgiNet.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SurgiNet.TabIndex = 2;
            this.SurgiNet.TabStop = false;
            this.SurgiNet.Click += new System.EventHandler(this.Citrix_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::QuickLaunch.Properties.Resources.citrix_shield_normal;
            this.pictureBox1.Location = new System.Drawing.Point(201, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(60, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::QuickLaunch.Properties.Resources.citrix_ambulance_normal;
            this.pictureBox2.Location = new System.Drawing.Point(267, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(60, 60);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // QuickLaunch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.ClientSize = new System.Drawing.Size(352, 139);
            this.ControlBox = false;
            this.Controls.Add(this.pnlMain);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStripUser;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuickLaunch";
            this.Padding = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.QuickLaunch_FormClosing);
            this.LostFocus += new System.EventHandler(this.QuickLaunch_LostFocus);
            this.MouseLeave += new System.EventHandler(this.QuickLaunch_LostFocus);
            this.pnlMain.ResumeLayout(false);
            this.pnlMain.PerformLayout();
            this.pnlFooter.ResumeLayout(false);
            this.pnlFooter.PerformLayout();
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            this.menuStripUser.ResumeLayout(false);
            this.menuStripUser.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbIcon)).EndInit();
            this.pnlBtnShadow.ResumeLayout(false);
            this.pnlBtnShadow.PerformLayout();
            this.pnlNotifier.ResumeLayout(false);
            this.pnlFlowContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PowerChart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SurgiNet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Panel pnlMain;
        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox pbIcon;
        private System.Windows.Forms.Label lblTapOut;
        private System.Windows.Forms.Panel pnlBtnShadow;
        private System.Windows.Forms.Panel pnlNotifier;
        private System.Windows.Forms.Label lblNotifier;
        private System.Windows.Forms.PictureBox FirstNet;
        private System.Windows.Forms.PictureBox PowerChart;
        private System.Windows.Forms.FlowLayoutPanel pnlIconNotifications;
        private System.Windows.Forms.Panel pnlFooter;
        private System.Windows.Forms.FlowLayoutPanel pnlFlowContainer;
        private System.Windows.Forms.PictureBox SurgiNet;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.MenuStrip menuStripUser;
        private System.Windows.Forms.ToolStripMenuItem toolStripUser;
        private System.Windows.Forms.ToolStripMenuItem myProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem myFavsMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsDisconnect;
        private System.Windows.Forms.ToolStripMenuItem tsLogoff;
        private System.Windows.Forms.ToolStripMenuItem tsmiEMRSession;
        private System.Windows.Forms.ToolStripMenuItem tsMyApps;
        private System.Windows.Forms.ToolStripMenuItem resetConnectionToolStripMenuItem;
        private MetroFramework.Controls.MetroProgressBar progressBar;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label lblVersion;

    }
}