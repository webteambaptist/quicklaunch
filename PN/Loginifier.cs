﻿using OneSignAuthInterop;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickLaunch
{
    /// <summary>
    /// Windows Form for SSO, Notifier-less version of QL
    /// </summary>
    public partial class Loginifier : Form
    {
        public string secret { get; set; }
        private int counter { get; set; }

        public Loginifier()
        {
            InitializeComponent();
        }

        private void GetUser()
        {
            try
            {
                OneSignAuthClass authClass2 = new OneSignAuthClass();
                IOneSignAuth auth = (IOneSignAuth)authClass2;
                IComUserIdentity userdent = (IComUserIdentity)auth.CurrentOneSignUser;
                lblName.Text = userdent.Username;
            }
            catch (Exception ex)
            {

            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            Validate();
        }

        private void Validate()
        {
            try
            {
                if ((!String.IsNullOrEmpty(txtPass.Text.Trim())) && (txtPass.Text.Trim().Length > 7))
                {
                    secret = txtPass.Text.Trim();

                    OneSignAuthClass authClass = new OneSignAuthClass();

                    IOneSignAuth12 auth12 = (IOneSignAuth12)authClass;

                    OneSignAuthClass authClass2 = new OneSignAuthClass();
                    IOneSignAuth auth = (IOneSignAuth)authClass2;
                    IComUserIdentity userdent = (IComUserIdentity)auth.CurrentOneSignUser;

                    string user = userdent.Username;
                    //  user = "TOMTEST2";

                    if (counter < 10)
                    {
                        try
                        {
                            if (auth12.PasswordAuthenticateOneSignUser(user, "bh", secret))
                            {
                                this.Close();
                            }
                        }
                        catch (Exception ex)
                        {
                            lblWarn.Text = ex.Message;
                            //lblWarn.Text = "Password incorrect, retry.";
                            lblWarn.Visible = true;
                            counter = counter + 1;
                            txtPass.Text = "";
                        }
                    }
                    else
                    {
                        lblWarn.Text = "Number of allowed attempts exceeded";
                        lblWarn.Visible = true;
                        txtPass.Text = "";
                    }
                }
                else
                {
                    lblWarn.Text = "Password is blank or too short, retry.";
                    lblWarn.Visible = true;
                    txtPass.Text = "";
                }
            }
            catch (Exception ex)
            {
                lblWarn.Text = "Password incorrect, retry.";
                lblWarn.Visible = true;
                counter = counter + 1;
                txtPass.Text = "";
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Environment.Exit(99);
        }

        private void Login_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void Login_KeyDown(object sender, KeyEventArgs e)
        {

        }
    }
}
