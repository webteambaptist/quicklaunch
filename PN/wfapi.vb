Attribute VB_Name = "WFAPI"
Option Explicit

'**********************************************************************
'
' WFAPI.BAS
'
' Citrix Server SDK - WFAPI Visual Basic Interface.
'
' Copyright 1999, Citrix Systems Inc.
'
' This file contains the basic declarations for Visual Basic programs
' to access the Citrix APIs available in WFAPI.DLL.
'
' The ANSI version of the APIs are defined in wfapia.bas.
'
' The UNICODE version of the APIs are defined in wfapiw.bas.
'
' Wfapia.bas and Wfapiw.bas can not be used in the same project.  Both
' need to have wfapi.bas included in a project.
'
' The file wfapix.bas contains wrapper functions that allow easier use
' of the raw Citrix API calls.  Only the ANSI version is available now.
'
'**********************************************************************

'**********************************************************************
' Constants
'**********************************************************************

'
' Specifies the current server
'
Public Const WF_CURRENT_SERVER = &H0
Public Const WF_CURRENT_SERVER_HANDLE = &H0
Public Const WF_CURRENT_SERVER_NAME = &H0

'
' Specifies the current session (SessionId)
'
Public Const WF_CURRENT_SESSION = -1

'
' Possible pResponse values from WFSendMessage()
'
Public Const IDTIMEOUT = 32000
Public Const IDASYNC = 32001

'
' Server shutdown flags
'
Public Const WF_WSD_LOGOFF = &H1       ' log off all users except
                                       ' current user; deletes
                                       ' WinStations (a reboot is
                                       ' required to recreate the
                                       ' WinStations)
Public Const WF_WSD_SHUTDOWN = &H2     ' shutdown system
Public Const WF_WSD_REBOOT = &H4       ' shutdown and reboot
Public Const WF_WSD_POWEROFF = &H8     ' shutdown and power off (on
                                       ' machines that support power
                                       ' off through software)
Public Const WF_WSD_FASTREBOOT = &H10  ' reboot without logging users
                                       ' off or shutting down


'**********************************************************************
' WF_CONNECTSTATE_CLASS - Session connect state
'**********************************************************************

Public Enum WF_CONNECTSTATE_CLASS
    WFActive               ' User logged on to WinStation
    WFConnected            ' WinStation connected to client
    WFConnectQuery         ' In the process of connecting to client
    WFShadow               ' Shadowing another WinStation
    WFDisconnected         ' WinStation logged on without client
    WFIdle                 ' Waiting for client to connect
    WFListen               ' WinStation is listening for connection
    WFReset                ' WinStation is being reset
    WFDown                 ' WinStation is down due to error
    WFInit                 ' WinStation in initialization
End Enum


'**********************************************************************
' WF_SERVER_INFO - returned by WFEnumerateServers (version 1)
'**********************************************************************

Type WF_SERVER_INFO
     pServerName As Long    '// server name pointer
End Type
 
'**********************************************************************
' WF_INFO_CLASS - WFQuerySessionInformation
'  (See additional typedefs for more info on structures)
'**********************************************************************

Public Enum WF_INFO_CLASS
    WFVersion
    WFInitialProgram
    WFWorkingDirectory
    WFOEMId
    WFSessionId
    WFUserName
    WFWinstationName
    WFDomainName
    WFConnectState
    WFClientBuildNumber
    WFClientName
    WFClientDirectory
    WFClientProductID
    WFClientHardwareId
    WFClientAddress
    WFClientDisplay
    WFClientCache
    WFClientDrives
    WFICABufferLength
    WFLicenseEnabler
    RESERVED2
    WFApplicationName
    WFVersionEx
    WFClientInfo
    WFUserInfo
    WFAppInfo
    WFClientLatency
    WFSessionTime
End Enum

'**********************************************************************
' WFQuerySessionInformation - (WFClientProductId)
'
'   NOTE: the following are Citrix-defined client product IDs.  OEM
'         clients will have different, OEM-specific values.
'**********************************************************************

Public Const CLIENTID_CITRIX_DOS = &H1           ' citrix dos client
Public Const CLIENTID_CITRIX_TEXT_TERM = &H4     ' citrix text terminals
Public Const CLIENTID_CITRIX_DIRECTICA = &H7     ' citrix DirectICA clients


'**********************************************************************
' WFQuerySessionInformation - (WFClientAddress)
'**********************************************************************

'from "Winsock.h"
Public Const AF_INET = 2        ' internetwork: UDP, TCP, etc.
Public Const AF_IPX = 6         ' IPX and SPX
Public Const AF_NETBIOS = 17    ' NetBios-style addresses
Public Const AF_UNKNOWN1 = 20   '

Public Type WF_CLIENT_ADDRESS
    AddressFamily As Long       ' AF_INET, AF_IPX, AF_NETBIOS, AF_UNSPEC
    Address As String * 20      ' client network address
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFClientDisplay)
'**********************************************************************

Type WF_CLIENT_DISPLAY
    HorizontalResolution As Long    ' horizontal dimensions, in pixels
    VerticalResolution As Long      ' vertical dimensions, in pixels
    ColorDepth As Long              ' 1=16, 2=256, 4=64K, 8=16M
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFClientLatency)
'**********************************************************************
Type WF_CLIENT_LATENCY
    Average As Long            'Average latency in ms
    Last As Long               'Last latency in ms.
    Deviation As Long           'Latency deviation in ms.
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFClientInfo)
'**********************************************************************

Public Type WF_CLIENT_INFO
    Name As String
    Directory As String
    BuildNumber As Long
    ProductID As Long
    HardwareID As Long
    Address As WF_CLIENT_ADDRESS
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFAppInfo)
'**********************************************************************
Public Type WF_APP_INFO
    pInitialProgram As String
    pWorkingDirectory As String
    pApplicationName As String
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFUserInfo)
'**********************************************************************
Public Type WF_USER_INFO
    pUserName As String
    pDomainName As String
    pConnectionName As String
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFClientCache)
'**********************************************************************

Type WF_CLIENT_CACHE
    CacheTiny As Long               ' size of cache for small objects (32
                                    ' bytes)
    CacheLowMem As Long             ' size of cache (low memory for DOS)
                                    ' virtual memory for Win16/Win32)
    CacheXms As Long                ' size of cache in XMS memory (DOS only)
    CacheDisk As Long               ' size of cache in disk (DOS only)
    DimCacheSize As Long            ' persistent cache size
    DimBitmapMin As Long            ' minimum bitmap size for persistent
                                    ' cache
    DimSignatureLevel As Long       ' version of persistent caching
    DimFilesysOverhead As Long      ' The amount of overhead that the file
                                    ' system on the client requires
                                    ' per bitmap
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFClientDrives)
'**********************************************************************

Type WF_DRIVES
    DriveLetter As Long ' ASCII value
    Flags As Long       ' see below for flag values
End Type

'
' Client drive mapping flags (Flags)
'
Public Const WF_DRIVE_REMOVEABLE = &H1
Public Const WF_DRIVE_FIXED = &H2
Public Const WF_DRIVE_REMOTE = &H4
Public Const WF_DRIVE_CDROM = &H8

Type WF_CLIENT_DRIVES
    fAutoClientDrives As Long       ' are client drives auto-connected?
    Count As Long                   ' number of actual client drives
    Drives(26) As WF_DRIVES         ' client drives (room for 26 letters)
End Type

'**********************************************************************
' WF_CONFIG_CLASS - WFQueryUserConfig/WFSetUserConfig
'**********************************************************************

Public Enum WF_CONFIG_CLASS
    WFUserConfigInitialProgram         ' string returned/expected
    WFUserConfigWorkingDirectory       ' string returned/expected
    WFUserConfigfInheritInitialProgram ' DWORD returned/expected
End Enum

'**********************************************************************
' WF_EVENT - Event flags for WFWaitSystemEvent
'**********************************************************************

Public Const WF_EVENT_NONE = &H0            ' return no event
Public Const WF_EVENT_CREATE = &H1          ' new WinStation created
Public Const WF_EVENT_DELETE = &H2          ' existing WinStation deleted
Public Const WF_EVENT_RENAME = &H4          ' existing WinStation renamed
Public Const WF_EVENT_CONNECT = &H8         ' WinStation connect to client
Public Const WF_EVENT_DISCONNECT = &H10     ' WinStation disconnected from
                                            ' client
Public Const WF_EVENT_LOGON = &H20          ' user logged on to existing
                                            ' WinStation
Public Const WF_EVENT_LOGOFF = &H40         ' user logged off from
                                            ' existing WinStation
Public Const WF_EVENT_STATECHANGE = &H80    ' WinStation state change
Public Const WF_EVENT_LICENSE = &H100       ' license state change
Public Const WF_EVENT_ALL = &H7FFFFFFF      ' wait for all event types
Public Const WF_EVENT_FLUSH = &H80000000    ' unblock all waiters

'**********************************************************************
' WF_VIRTUAL_CLASS - WFVirtualChannelQuery
'**********************************************************************

Public Enum WF_VIRTUAL_CLASS
    WFVirtualClientData                     ' Virtual channel client module
                                            ' data (C2H data)
End Enum

'**********************************************************************
' WF_SESSION_INFO - returned by WFEnumerateSessions (version 1)
'**********************************************************************

Type WF_SESSION_INFO
    SessionId As Long               ' session id
    pWinStationName As String       ' name of WinStation this session is
                                    ' connected to
    State As WF_CONNECTSTATE_CLASS  ' connection state (see enum)
End Type

'**********************************************************************
' WF_PROCESS_INFO - returned by WFEnumerateProcesses (version 1)
'**********************************************************************

Type WF_PROCESS_INFO
    SessionId As Long       ' session id
    ProcessId As Long       ' process id
    pProcessName As String  ' name of process
    pUserSid As Long        ' user's SID
End Type

Public Const WF_PROCESS_INFO_SIZE = 16

'**********************************************************************
' WFQuerySessionInformation - (WFVersion)
'**********************************************************************

Type OSVERSIONINFO          'from "Winbase.h"
    OSVersionInfoSize As Long
    MajorVersion As Long
    MinorVersion As Long
    BuildNumber As Long
    PlatformId As Long
    pCSDVersion As String * 128
End Type

'**********************************************************************
' WFQuerySessionInformation - (WFSessionTime)
'**********************************************************************

Type WF_SESSION_TIME
    ConnectTime As Double
    DisconnectTime As Double
    LastInputTime As Double
    LogonTime As Double
    CurrentTime As Double
End Type

'**********************************************************************
' Citrix server public API declarations
'**********************************************************************

Public Declare Sub WFCloseServer Lib "wfapi.dll" (ByVal hServer As Long)

Public Declare Function WFTerminateProcess Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal ProcessId As Long, _
                            ByVal exitCode As Long) As Boolean

Public Declare Function WFDisconnectSession Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal bWait As Boolean) As Boolean

Public Declare Function WFDisconnectSessionEx Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal Flags As Long, _
                            ByVal pBuffer As Long, _
                            ByVal BufferLength As Long) As Boolean

Public Declare Function WFLogoffSession Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal bWait As Boolean) As Boolean

Public Declare Function WFShutdownSystem Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal ShutdownFlag As Long) As Boolean

Public Declare Function WFWaitSystemEvent Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal EventMask As Long, _
                            ByRef pEventFlags As Long) As Boolean

Public Declare Sub WFFreeMemory Lib "wfapi.dll" (ByVal pMemory As Long)

'
' Server virtual channel API's
'

Public Declare Function WFVirtualChannelOpen Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal pVirtualName As String) As Long

Public Declare Function WFVirtualChannelClose Lib "wfapi.dll" ( _
                            ByVal hChannelHandle As Long) As Boolean

Public Declare Function WFVirtualChannelRead Lib "wfapi.dll" ( _
                            ByVal hChannelHandle As Long, _
                            ByVal timeOut As Long, _
                            ByVal Buffer As String, _
                            ByVal BufferSize As Long, _
                            ByRef pBytesRead As Long) As Boolean

Public Declare Function WFVirtualChannelWrite Lib "wfapi.dll" ( _
                            ByVal hChannelHandle As Long, _
                            ByVal Buffer As String, _
                            ByVal Length As Long, _
                            ByRef pBytesWritten As Long) As Boolean

Public Declare Function WFVirtualChannelPurgeInput Lib "wfapi.dll" ( _
                            ByVal hChannelHandle As Long) As Boolean

Public Declare Function WFVirtualChannelPurgeOutput Lib "wfapi.dll" ( _
                            ByVal hChannelHandle As Long) As Boolean

Public Declare Function WFVirtualChannelQuery Lib "wfapi.dll" ( _
                            ByVal hChannelHandle As Long, _
                            ByVal VirtualClass As WF_VIRTUAL_CLASS, _
                            ByRef ppBuffer As Long, _
                            ByRef pBytesReturned As Long) As Boolean

'
' APIs that require both ANSI and UNICODE definitions.
'

'**********************************************************************
' Citrix server public API declarations
'**********************************************************************

Public Declare Function WFEnumerateServersA Lib "wfapi.dll" ( _
                            ByVal pDomainName As String, _
                            ByVal Reserved As Long, _
                            ByVal Version As Long, _
                            ByRef pServerName As Long, _
                            ByRef pCount As Long) As Boolean

Public Declare Function WFEnumerateServersW Lib "wfapi.dll" ( _
                            ByVal pDomainName As String, _
                            ByVal Reserved As Long, _
                            ByVal Version As Long, _
                            ByRef pServerName As Long, _
                            ByRef pCount As Long) As Boolean

Public Declare Function WFOpenServerA Lib "wfapi.dll" ( _
                            ByVal pServerName As String) As Long

Public Declare Function WFOpenServerW Lib "wfapi.dll" ( _
                            ByVal pServerName As String) As Long

Public Declare Function WFEnumerateSessionsA Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal Reserved As Long, _
                            ByVal Version As Long, _
                            ByRef pSessionInfo As Long, _
                            ByRef pCount As Long) As Boolean

Public Declare Function WFEnumerateSessionsW Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal Reserved As Long, _
                            ByVal Version As Long, _
                            ByRef pSessionInfo As Long, _
                            ByRef pCount As Long) As Boolean

Public Declare Function WFEnumerateProcessesA Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal Reserved As Long, _
                            ByVal Version As Long, _
                            ByRef ppProcessInfo As Long, _
                            ByRef pCount As Long) As Boolean

Public Declare Function WFEnumerateProcessesW Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal Reserved As Long, _
                            ByVal Version As Long, _
                            ByRef ppProcessInfo As Long, _
                            ByRef pCount As Long) As Boolean

Public Declare Function WFQuerySessionInformationA Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal WFInfoClass As WF_INFO_CLASS, _
                            ByRef ppBuffer As Long, _
                            ByRef pBytesReturned As Long) As Boolean

Public Declare Function WFQuerySessionInformationW Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal WFInfoClass As WF_INFO_CLASS, _
                            ByRef ppBuffer As Long, _
                            ByRef pBytesReturned As Long) As Boolean

Public Declare Function WFQueryUserConfigA Lib "wfapi.dll" ( _
                            ByVal pServerName As String, _
                            ByVal pUserName As String, _
                            ByVal WFConfigClass As WF_CONFIG_CLASS, _
                            ByRef ppBuffer As Long, _
                            ByRef pBytesReturned As Long) As Boolean

Public Declare Function WFQueryUserConfigW Lib "wfapi.dll" ( _
                            ByVal pServerName As String, _
                            ByVal pUserName As String, _
                            ByVal WFConfigClass As WF_CONFIG_CLASS, _
                            ByRef ppBuffer As Long, _
                            ByRef pBytesReturned As Long) As Boolean

Public Declare Function WFSetUserConfigA Lib "wfapi.dll" ( _
                            ByVal pServerName As String, _
                            ByVal pUserName As String, _
                            ByVal WFConfigClass As WF_CONFIG_CLASS, _
                            ByVal pBuffer As String, _
                            ByVal DataLength As Long) As Boolean

Public Declare Function WFSetUserConfigW Lib "wfapi.dll" ( _
                            ByVal pServerName As String, _
                            ByVal pUserName As String, _
                            ByVal WFConfigClass As WF_CONFIG_CLASS, _
                            ByVal pBuffer As String, _
                            ByVal DataLength As Long) As Boolean

Public Declare Function WFSendMessageA Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal pTitle As String, _
                            ByVal TitleLength As Long, _
                            ByVal pMessage As String, _
                            ByVal MessageLength As Long, _
                            ByVal Style As Long, _
                            ByVal timeOut As Long, _
                            ByRef pResponse As Long, _
                            ByVal bWait As Boolean) As Boolean

Public Declare Function WFSendMessageW Lib "wfapi.dll" ( _
                            ByVal hServer As Long, _
                            ByVal SessionId As Long, _
                            ByVal pTitle As String, _
                            ByVal TitleLength As Long, _
                            ByVal pMessage As String, _
                            ByVal MessageLength As Long, _
                            ByVal Style As Long, _
                            ByVal timeOut As Long, _
                            ByRef pResponse As Long, _
                            ByVal bWait As Boolean) As Boolean

Public Declare Function WFShadowSessionA Lib "wfapi.dll" ( _
                            ByVal ServerName As String, _
                            ByVal SessionId As Long, _
                            ByVal Hotkey As Long, _
                            ByVal HKModifier As Long) As Integer

Public Declare Function WFShadowSessionW Lib "wfapi.dll" ( _
                            ByVal ServerName As String, _
                            ByVal SessionId As Long, _
                            ByVal Hotkey As Long, _
                            ByVal HKModifier As Long) As Integer

'
' Useful Windows API's
'
Public Declare Sub RtlMoveMemory Lib "kernel32" ( _
                            dest As Any, _
                            ByVal src As Any, _
                            ByVal byteLen As Long)
Public Declare Function GetLastError Lib "kernel32" () As Long

