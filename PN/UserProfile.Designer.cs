﻿namespace QuickLaunch
{
    partial class UserProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlHeader = new System.Windows.Forms.Panel();
            this.pbHelp = new System.Windows.Forms.PictureBox();
            this.pbClose = new System.Windows.Forms.PictureBox();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.bOk = new System.Windows.Forms.Button();
            this.gbLocationSettings = new System.Windows.Forms.GroupBox();
            this.tblQLLocation = new System.Windows.Forms.TableLayoutPanel();
            this.pnlBottomRight = new System.Windows.Forms.Panel();
            this.pnlBottomCenter = new System.Windows.Forms.Panel();
            this.pnlBottomLeft = new System.Windows.Forms.Panel();
            this.pnlTopRight = new System.Windows.Forms.Panel();
            this.pnlTopCenter = new System.Windows.Forms.Panel();
            this.lblScreen = new System.Windows.Forms.Label();
            this.pnlTopLeft = new System.Windows.Forms.Panel();
            this.gbOnStartup = new System.Windows.Forms.GroupBox();
            this.rbLaunchAS = new System.Windows.Forms.RadioButton();
            this.rbLaunchSN = new System.Windows.Forms.RadioButton();
            this.rbLaunchPC = new System.Windows.Forms.RadioButton();
            this.rbLaunchFN = new System.Windows.Forms.RadioButton();
            this.rbNoLaunch = new System.Windows.Forms.RadioButton();
            this.rbLaunchAN = new System.Windows.Forms.RadioButton();
            this.pnlHeader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlContainer.SuspendLayout();
            this.gbLocationSettings.SuspendLayout();
            this.tblQLLocation.SuspendLayout();
            this.gbOnStartup.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlHeader
            // 
            this.pnlHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(25)))), ((int)(((byte)(80)))), ((int)(((byte)(106)))));
            this.pnlHeader.Controls.Add(this.pbHelp);
            this.pnlHeader.Controls.Add(this.pbClose);
            this.pnlHeader.Controls.Add(this.lblTitle);
            this.pnlHeader.Controls.Add(this.pictureBox1);
            this.pnlHeader.Location = new System.Drawing.Point(-13, -3);
            this.pnlHeader.Name = "pnlHeader";
            this.pnlHeader.Size = new System.Drawing.Size(329, 28);
            this.pnlHeader.TabIndex = 6;
            // 
            // pbHelp
            // 
            this.pbHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbHelp.Image = global::QuickLaunch.Properties.Resources.HelpPNG;
            this.pbHelp.Location = new System.Drawing.Point(238, 5);
            this.pbHelp.Name = "pbHelp";
            this.pbHelp.Size = new System.Drawing.Size(15, 15);
            this.pbHelp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbHelp.TabIndex = 22;
            this.pbHelp.TabStop = false;
            this.pbHelp.Visible = false;
            this.pbHelp.Click += new System.EventHandler(this.pbHelp_Click);
            // 
            // pbClose
            // 
            this.pbClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pbClose.Image = global::QuickLaunch.Properties.Resources.Close;
            this.pbClose.Location = new System.Drawing.Point(261, 0);
            this.pbClose.Name = "pbClose";
            this.pbClose.Size = new System.Drawing.Size(47, 22);
            this.pbClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbClose.TabIndex = 3;
            this.pbClose.TabStop = false;
            this.pbClose.Click += new System.EventHandler(this.pbClose_Click);
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblTitle.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(35, 10);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(62, 14);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "My Profile";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::QuickLaunch.Properties.Resources.favicon;
            this.pictureBox1.Location = new System.Drawing.Point(18, 8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pnlContainer
            // 
            this.pnlContainer.BackColor = System.Drawing.SystemColors.Control;
            this.pnlContainer.Controls.Add(this.bOk);
            this.pnlContainer.Controls.Add(this.gbLocationSettings);
            this.pnlContainer.Controls.Add(this.gbOnStartup);
            this.pnlContainer.Location = new System.Drawing.Point(3, 25);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(294, 380);
            this.pnlContainer.TabIndex = 8;
            // 
            // bOk
            // 
            this.bOk.Location = new System.Drawing.Point(100, 337);
            this.bOk.Name = "bOk";
            this.bOk.Size = new System.Drawing.Size(89, 34);
            this.bOk.TabIndex = 15;
            this.bOk.Text = "Ok";
            this.bOk.UseVisualStyleBackColor = true;
            this.bOk.Click += new System.EventHandler(this.bOk_Click);
            // 
            // gbLocationSettings
            // 
            this.gbLocationSettings.Controls.Add(this.tblQLLocation);
            this.gbLocationSettings.Location = new System.Drawing.Point(16, 203);
            this.gbLocationSettings.Name = "gbLocationSettings";
            this.gbLocationSettings.Size = new System.Drawing.Size(262, 118);
            this.gbLocationSettings.TabIndex = 14;
            this.gbLocationSettings.TabStop = false;
            this.gbLocationSettings.Text = "Startup Location";
            // 
            // tblQLLocation
            // 
            this.tblQLLocation.BackColor = System.Drawing.Color.White;
            this.tblQLLocation.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tblQLLocation.ColumnCount = 3;
            this.tblQLLocation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tblQLLocation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34F));
            this.tblQLLocation.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tblQLLocation.Controls.Add(this.pnlBottomRight, 2, 2);
            this.tblQLLocation.Controls.Add(this.pnlBottomCenter, 1, 2);
            this.tblQLLocation.Controls.Add(this.pnlBottomLeft, 0, 2);
            this.tblQLLocation.Controls.Add(this.pnlTopRight, 2, 0);
            this.tblQLLocation.Controls.Add(this.pnlTopCenter, 1, 0);
            this.tblQLLocation.Controls.Add(this.lblScreen, 0, 1);
            this.tblQLLocation.Controls.Add(this.pnlTopLeft, 0, 0);
            this.tblQLLocation.Location = new System.Drawing.Point(19, 19);
            this.tblQLLocation.Name = "tblQLLocation";
            this.tblQLLocation.RowCount = 3;
            this.tblQLLocation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblQLLocation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tblQLLocation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tblQLLocation.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tblQLLocation.Size = new System.Drawing.Size(222, 90);
            this.tblQLLocation.TabIndex = 0;
            // 
            // pnlBottomRight
            // 
            this.pnlBottomRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.pnlBottomRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlBottomRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottomRight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlBottomRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottomRight.Location = new System.Drawing.Point(151, 65);
            this.pnlBottomRight.Name = "pnlBottomRight";
            this.pnlBottomRight.Size = new System.Drawing.Size(67, 21);
            this.pnlBottomRight.TabIndex = 6;
            this.pnlBottomRight.Tag = "BOTTOM_RIGHT";
            this.pnlBottomRight.Click += new System.EventHandler(this.pnlLocationSetting_Click);
            this.pnlBottomRight.MouseEnter += new System.EventHandler(this.pnlLocationSetting_OnMouseOver);
            this.pnlBottomRight.MouseLeave += new System.EventHandler(this.pnlLocationSetting_OnMouseOut);
            // 
            // pnlBottomCenter
            // 
            this.pnlBottomCenter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.pnlBottomCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlBottomCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottomCenter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlBottomCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottomCenter.Location = new System.Drawing.Point(76, 65);
            this.pnlBottomCenter.Name = "pnlBottomCenter";
            this.pnlBottomCenter.Size = new System.Drawing.Size(68, 21);
            this.pnlBottomCenter.TabIndex = 5;
            this.pnlBottomCenter.Tag = "BOTTOM_MID";
            this.pnlBottomCenter.Click += new System.EventHandler(this.pnlLocationSetting_Click);
            this.pnlBottomCenter.MouseEnter += new System.EventHandler(this.pnlLocationSetting_OnMouseOver);
            this.pnlBottomCenter.MouseLeave += new System.EventHandler(this.pnlLocationSetting_OnMouseOut);
            // 
            // pnlBottomLeft
            // 
            this.pnlBottomLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.pnlBottomLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlBottomLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBottomLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlBottomLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlBottomLeft.Location = new System.Drawing.Point(4, 65);
            this.pnlBottomLeft.Name = "pnlBottomLeft";
            this.pnlBottomLeft.Size = new System.Drawing.Size(65, 21);
            this.pnlBottomLeft.TabIndex = 4;
            this.pnlBottomLeft.Tag = "BOTTOM_LEFT";
            this.pnlBottomLeft.Click += new System.EventHandler(this.pnlLocationSetting_Click);
            this.pnlBottomLeft.MouseEnter += new System.EventHandler(this.pnlLocationSetting_OnMouseOver);
            this.pnlBottomLeft.MouseLeave += new System.EventHandler(this.pnlLocationSetting_OnMouseOut);
            // 
            // pnlTopRight
            // 
            this.pnlTopRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.pnlTopRight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTopRight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTopRight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlTopRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopRight.Location = new System.Drawing.Point(151, 4);
            this.pnlTopRight.Name = "pnlTopRight";
            this.pnlTopRight.Size = new System.Drawing.Size(67, 19);
            this.pnlTopRight.TabIndex = 3;
            this.pnlTopRight.Tag = "TOP_RIGHT";
            this.pnlTopRight.Click += new System.EventHandler(this.pnlLocationSetting_Click);
            this.pnlTopRight.MouseEnter += new System.EventHandler(this.pnlLocationSetting_OnMouseOver);
            this.pnlTopRight.MouseLeave += new System.EventHandler(this.pnlLocationSetting_OnMouseOut);
            // 
            // pnlTopCenter
            // 
            this.pnlTopCenter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.pnlTopCenter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTopCenter.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTopCenter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlTopCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopCenter.Location = new System.Drawing.Point(76, 4);
            this.pnlTopCenter.Name = "pnlTopCenter";
            this.pnlTopCenter.Size = new System.Drawing.Size(68, 19);
            this.pnlTopCenter.TabIndex = 2;
            this.pnlTopCenter.Tag = "TOP_MID";
            this.pnlTopCenter.Click += new System.EventHandler(this.pnlLocationSetting_Click);
            this.pnlTopCenter.MouseEnter += new System.EventHandler(this.pnlLocationSetting_OnMouseOver);
            this.pnlTopCenter.MouseLeave += new System.EventHandler(this.pnlLocationSetting_OnMouseOut);
            // 
            // lblScreen
            // 
            this.lblScreen.AutoSize = true;
            this.tblQLLocation.SetColumnSpan(this.lblScreen, 3);
            this.lblScreen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblScreen.Location = new System.Drawing.Point(4, 27);
            this.lblScreen.Name = "lblScreen";
            this.lblScreen.Size = new System.Drawing.Size(214, 34);
            this.lblScreen.TabIndex = 0;
            this.lblScreen.Text = "Screen";
            this.lblScreen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlTopLeft
            // 
            this.pnlTopLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(191)))), ((int)(((byte)(220)))), ((int)(((byte)(184)))));
            this.pnlTopLeft.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlTopLeft.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTopLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pnlTopLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTopLeft.Location = new System.Drawing.Point(4, 4);
            this.pnlTopLeft.Name = "pnlTopLeft";
            this.pnlTopLeft.Size = new System.Drawing.Size(65, 19);
            this.pnlTopLeft.TabIndex = 1;
            this.pnlTopLeft.Tag = "TOP_LEFT";
            this.pnlTopLeft.Click += new System.EventHandler(this.pnlLocationSetting_Click);
            this.pnlTopLeft.MouseEnter += new System.EventHandler(this.pnlLocationSetting_OnMouseOver);
            this.pnlTopLeft.MouseLeave += new System.EventHandler(this.pnlLocationSetting_OnMouseOut);
            // 
            // gbOnStartup
            // 
            this.gbOnStartup.Controls.Add(this.rbLaunchAN);
            this.gbOnStartup.Controls.Add(this.rbLaunchAS);
            this.gbOnStartup.Controls.Add(this.rbLaunchSN);
            this.gbOnStartup.Controls.Add(this.rbLaunchPC);
            this.gbOnStartup.Controls.Add(this.rbLaunchFN);
            this.gbOnStartup.Controls.Add(this.rbNoLaunch);
            this.gbOnStartup.Location = new System.Drawing.Point(16, 20);
            this.gbOnStartup.Name = "gbOnStartup";
            this.gbOnStartup.Size = new System.Drawing.Size(262, 177);
            this.gbOnStartup.TabIndex = 10;
            this.gbOnStartup.TabStop = false;
            this.gbOnStartup.Text = "Auto Launch";
            // 
            // rbLaunchAS
            // 
            this.rbLaunchAS.AutoSize = true;
            this.rbLaunchAS.Location = new System.Drawing.Point(19, 121);
            this.rbLaunchAS.Name = "rbLaunchAS";
            this.rbLaunchAS.Size = new System.Drawing.Size(125, 17);
            this.rbLaunchAS.TabIndex = 14;
            this.rbLaunchAS.Tag = "AS";
            this.rbLaunchAS.Text = "Autolaunch AllScripts";
            this.rbLaunchAS.UseVisualStyleBackColor = true;
            // 
            // rbLaunchSN
            // 
            this.rbLaunchSN.AutoSize = true;
            this.rbLaunchSN.Location = new System.Drawing.Point(19, 98);
            this.rbLaunchSN.Name = "rbLaunchSN";
            this.rbLaunchSN.Size = new System.Drawing.Size(123, 17);
            this.rbLaunchSN.TabIndex = 13;
            this.rbLaunchSN.Tag = "SN";
            this.rbLaunchSN.Text = "Autolaunch SurgiNet";
            this.rbLaunchSN.UseVisualStyleBackColor = true;
            // 
            // rbLaunchPC
            // 
            this.rbLaunchPC.AutoSize = true;
            this.rbLaunchPC.Location = new System.Drawing.Point(19, 52);
            this.rbLaunchPC.Name = "rbLaunchPC";
            this.rbLaunchPC.Size = new System.Drawing.Size(140, 17);
            this.rbLaunchPC.TabIndex = 12;
            this.rbLaunchPC.Tag = "PC";
            this.rbLaunchPC.Text = "Autolaunch Power Chart";
            this.rbLaunchPC.UseVisualStyleBackColor = true;
            // 
            // rbLaunchFN
            // 
            this.rbLaunchFN.AutoSize = true;
            this.rbLaunchFN.Location = new System.Drawing.Point(19, 75);
            this.rbLaunchFN.Name = "rbLaunchFN";
            this.rbLaunchFN.Size = new System.Drawing.Size(118, 17);
            this.rbLaunchFN.TabIndex = 11;
            this.rbLaunchFN.Tag = "FN";
            this.rbLaunchFN.Text = "Autolaunch FirstNet";
            this.rbLaunchFN.UseVisualStyleBackColor = true;
            // 
            // rbNoLaunch
            // 
            this.rbNoLaunch.AutoSize = true;
            this.rbNoLaunch.Checked = true;
            this.rbNoLaunch.Location = new System.Drawing.Point(19, 29);
            this.rbNoLaunch.Name = "rbNoLaunch";
            this.rbNoLaunch.Size = new System.Drawing.Size(117, 17);
            this.rbNoLaunch.TabIndex = 10;
            this.rbNoLaunch.TabStop = true;
            this.rbNoLaunch.Text = "Disable Autolaunch";
            this.rbNoLaunch.UseVisualStyleBackColor = true;
            // 
            // rbLaunchAN
            // 
            this.rbLaunchAN.AutoSize = true;
            this.rbLaunchAN.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.rbLaunchAN.Location = new System.Drawing.Point(19, 144);
            this.rbLaunchAN.Name = "rbLaunchAN";
            this.rbLaunchAN.Size = new System.Drawing.Size(140, 18);
            this.rbLaunchAN.TabIndex = 15;
            this.rbLaunchAN.Tag = "AN";
            this.rbLaunchAN.Text = "Autolaunch Anesthesia";
            this.rbLaunchAN.UseVisualStyleBackColor = true;
            // 
            // UserProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.ClientSize = new System.Drawing.Size(300, 408);
            this.Controls.Add(this.pnlContainer);
            this.Controls.Add(this.pnlHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserProfile";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.pnlHeader.ResumeLayout(false);
            this.pnlHeader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbHelp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlContainer.ResumeLayout(false);
            this.gbLocationSettings.ResumeLayout(false);
            this.tblQLLocation.ResumeLayout(false);
            this.tblQLLocation.PerformLayout();
            this.gbOnStartup.ResumeLayout(false);
            this.gbOnStartup.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlHeader;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.GroupBox gbOnStartup;
        private System.Windows.Forms.RadioButton rbLaunchPC;
        private System.Windows.Forms.RadioButton rbLaunchFN;
        private System.Windows.Forms.RadioButton rbNoLaunch;
        private System.Windows.Forms.RadioButton rbLaunchSN;
        private System.Windows.Forms.GroupBox gbLocationSettings;
        private System.Windows.Forms.TableLayoutPanel tblQLLocation;
        private System.Windows.Forms.Label lblScreen;
        private System.Windows.Forms.Panel pnlBottomRight;
        private System.Windows.Forms.Panel pnlBottomCenter;
        private System.Windows.Forms.Panel pnlBottomLeft;
        private System.Windows.Forms.Panel pnlTopRight;
        private System.Windows.Forms.Panel pnlTopCenter;
        private System.Windows.Forms.Panel pnlTopLeft;
        private System.Windows.Forms.PictureBox pbClose;
        private System.Windows.Forms.PictureBox pbHelp;
        private System.Windows.Forms.Button bOk;
        private System.Windows.Forms.RadioButton rbLaunchAS;
        private System.Windows.Forms.RadioButton rbLaunchAN;
    }
}