﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickLaunch
{
    public class CustomMaskedControl : MaskedTextBox
    {
        private Color mColor = Color.Black;
        public CustomMaskedControl()
        {
            this.BorderStyle = BorderStyle.FixedSingle;
        }

        [DefaultValue(typeof(Color), "Black")]
        public Color BorderColor 
        {
            get { return mColor; }
            set { mColor = value; Invalidate(); }
        }

        private void drawBorder() 
        {
            using (Graphics gr = this.CreateGraphics()) 
            {
                ControlPaint.DrawBorder(gr, this.DisplayRectangle, mColor, ButtonBorderStyle.Solid);
            }
        }

        protected override void WndProc(ref Message m) 
        {
            base.WndProc(ref m);
            if (m.Msg == 15) drawBorder();
        }
    }    
}
