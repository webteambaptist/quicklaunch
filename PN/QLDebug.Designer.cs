﻿namespace QuickLaunch
{
    partial class QLDebug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabs = new System.Windows.Forms.TabControl();
            this.tbSystemInfo = new System.Windows.Forms.TabPage();
            this.lblSysInfo = new System.Windows.Forms.Label();
            this.tbOS = new System.Windows.Forms.TabPage();
            this.lblOS = new System.Windows.Forms.Label();
            this.tabProcs = new System.Windows.Forms.TabPage();
            this.gvProcs = new System.Windows.Forms.DataGridView();
            this.tbEventLog = new System.Windows.Forms.TabPage();
            this.gvEvents = new System.Windows.Forms.DataGridView();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.fToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commandPromptToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.registryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.servicesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.networkConnectionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eventViewerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addRemoveProgramsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taskManagerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.refreshToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.browserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabs.SuspendLayout();
            this.tbSystemInfo.SuspendLayout();
            this.tbOS.SuspendLayout();
            this.tabProcs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvProcs)).BeginInit();
            this.tbEventLog.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvEvents)).BeginInit();
            this.mainMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabs
            // 
            this.tabs.Controls.Add(this.tbSystemInfo);
            this.tabs.Controls.Add(this.tbOS);
            this.tabs.Controls.Add(this.tabProcs);
            this.tabs.Controls.Add(this.tbEventLog);
            this.tabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabs.Location = new System.Drawing.Point(0, 24);
            this.tabs.Name = "tabs";
            this.tabs.SelectedIndex = 0;
            this.tabs.Size = new System.Drawing.Size(790, 644);
            this.tabs.TabIndex = 0;
            // 
            // tbSystemInfo
            // 
            this.tbSystemInfo.AutoScroll = true;
            this.tbSystemInfo.Controls.Add(this.lblSysInfo);
            this.tbSystemInfo.Location = new System.Drawing.Point(4, 22);
            this.tbSystemInfo.Name = "tbSystemInfo";
            this.tbSystemInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tbSystemInfo.Size = new System.Drawing.Size(782, 618);
            this.tbSystemInfo.TabIndex = 1;
            this.tbSystemInfo.Text = "System Info";
            this.tbSystemInfo.UseVisualStyleBackColor = true;
            // 
            // lblSysInfo
            // 
            this.lblSysInfo.AutoSize = true;
            this.lblSysInfo.Location = new System.Drawing.Point(3, 3);
            this.lblSysInfo.Name = "lblSysInfo";
            this.lblSysInfo.Size = new System.Drawing.Size(62, 13);
            this.lblSysInfo.TabIndex = 0;
            this.lblSysInfo.Text = "System Info";
            // 
            // tbOS
            // 
            this.tbOS.AutoScroll = true;
            this.tbOS.Controls.Add(this.lblOS);
            this.tbOS.Location = new System.Drawing.Point(4, 22);
            this.tbOS.Name = "tbOS";
            this.tbOS.Size = new System.Drawing.Size(782, 618);
            this.tbOS.TabIndex = 2;
            this.tbOS.Text = "Operating System Info";
            this.tbOS.UseVisualStyleBackColor = true;
            // 
            // lblOS
            // 
            this.lblOS.AutoSize = true;
            this.lblOS.Location = new System.Drawing.Point(0, 0);
            this.lblOS.Name = "lblOS";
            this.lblOS.Size = new System.Drawing.Size(111, 13);
            this.lblOS.TabIndex = 1;
            this.lblOS.Text = "Operating System Info";
            // 
            // tabProcs
            // 
            this.tabProcs.AutoScroll = true;
            this.tabProcs.Controls.Add(this.gvProcs);
            this.tabProcs.Location = new System.Drawing.Point(4, 22);
            this.tabProcs.Name = "tabProcs";
            this.tabProcs.Padding = new System.Windows.Forms.Padding(3);
            this.tabProcs.Size = new System.Drawing.Size(782, 618);
            this.tabProcs.TabIndex = 0;
            this.tabProcs.Text = "Processes";
            this.tabProcs.UseVisualStyleBackColor = true;
            // 
            // gvProcs
            // 
            this.gvProcs.AllowUserToAddRows = false;
            this.gvProcs.AllowUserToDeleteRows = false;
            this.gvProcs.AllowUserToOrderColumns = true;
            this.gvProcs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvProcs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gvProcs.Location = new System.Drawing.Point(3, 3);
            this.gvProcs.Name = "gvProcs";
            this.gvProcs.ReadOnly = true;
            this.gvProcs.Size = new System.Drawing.Size(776, 636);
            this.gvProcs.TabIndex = 0;
            // 
            // tbEventLog
            // 
            this.tbEventLog.AutoScroll = true;
            this.tbEventLog.Controls.Add(this.gvEvents);
            this.tbEventLog.Location = new System.Drawing.Point(4, 22);
            this.tbEventLog.Name = "tbEventLog";
            this.tbEventLog.Padding = new System.Windows.Forms.Padding(3);
            this.tbEventLog.Size = new System.Drawing.Size(782, 618);
            this.tbEventLog.TabIndex = 3;
            this.tbEventLog.Text = "Event Log";
            this.tbEventLog.UseVisualStyleBackColor = true;
            // 
            // gvEvents
            // 
            this.gvEvents.AllowUserToAddRows = false;
            this.gvEvents.AllowUserToDeleteRows = false;
            this.gvEvents.AllowUserToOrderColumns = true;
            this.gvEvents.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvEvents.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gvEvents.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.gvEvents.DefaultCellStyle = dataGridViewCellStyle2;
            this.gvEvents.Location = new System.Drawing.Point(3, 3);
            this.gvEvents.Name = "gvEvents";
            this.gvEvents.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gvEvents.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.gvEvents.Size = new System.Drawing.Size(776, 612);
            this.gvEvents.TabIndex = 1;
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fToolStripMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(790, 24);
            this.mainMenu.TabIndex = 2;
            this.mainMenu.Text = "menuStrip1";
            // 
            // fToolStripMenuItem
            // 
            this.fToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.refreshToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fToolStripMenuItem.Name = "fToolStripMenuItem";
            this.fToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fToolStripMenuItem.Text = "File";
            // 
            // newToolStripMenuItem
            // 
            this.newToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commandPromptToolStripMenuItem,
            this.registryToolStripMenuItem,
            this.servicesToolStripMenuItem,
            this.networkConnectionsToolStripMenuItem,
            this.eventViewerToolStripMenuItem,
            this.addRemoveProgramsToolStripMenuItem,
            this.taskManagerToolStripMenuItem,
            this.browserToolStripMenuItem});
            this.newToolStripMenuItem.Name = "newToolStripMenuItem";
            this.newToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.newToolStripMenuItem.Text = "Open";
            // 
            // commandPromptToolStripMenuItem
            // 
            this.commandPromptToolStripMenuItem.Name = "commandPromptToolStripMenuItem";
            this.commandPromptToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.commandPromptToolStripMenuItem.Text = "Command Prompt";
            this.commandPromptToolStripMenuItem.Click += new System.EventHandler(this.commandPromptToolStripMenuItem_Click);
            // 
            // registryToolStripMenuItem
            // 
            this.registryToolStripMenuItem.Name = "registryToolStripMenuItem";
            this.registryToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.registryToolStripMenuItem.Text = "Registry";
            this.registryToolStripMenuItem.Click += new System.EventHandler(this.registryToolStripMenuItem_Click);
            // 
            // servicesToolStripMenuItem
            // 
            this.servicesToolStripMenuItem.Name = "servicesToolStripMenuItem";
            this.servicesToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.servicesToolStripMenuItem.Text = "Services";
            this.servicesToolStripMenuItem.Click += new System.EventHandler(this.servicesToolStripMenuItem_Click);
            // 
            // networkConnectionsToolStripMenuItem
            // 
            this.networkConnectionsToolStripMenuItem.Name = "networkConnectionsToolStripMenuItem";
            this.networkConnectionsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.networkConnectionsToolStripMenuItem.Text = "Network Connections";
            this.networkConnectionsToolStripMenuItem.Click += new System.EventHandler(this.networkConnectionsToolStripMenuItem_Click);
            // 
            // eventViewerToolStripMenuItem
            // 
            this.eventViewerToolStripMenuItem.Name = "eventViewerToolStripMenuItem";
            this.eventViewerToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.eventViewerToolStripMenuItem.Text = "Event Viewer";
            this.eventViewerToolStripMenuItem.Click += new System.EventHandler(this.eventViewerToolStripMenuItem_Click);
            // 
            // addRemoveProgramsToolStripMenuItem
            // 
            this.addRemoveProgramsToolStripMenuItem.Name = "addRemoveProgramsToolStripMenuItem";
            this.addRemoveProgramsToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.addRemoveProgramsToolStripMenuItem.Text = "Add/Remove Programs";
            this.addRemoveProgramsToolStripMenuItem.Click += new System.EventHandler(this.addRemoveProgramsToolStripMenuItem_Click);
            // 
            // taskManagerToolStripMenuItem
            // 
            this.taskManagerToolStripMenuItem.Name = "taskManagerToolStripMenuItem";
            this.taskManagerToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.taskManagerToolStripMenuItem.Text = "Task Manager";
            this.taskManagerToolStripMenuItem.Click += new System.EventHandler(this.taskManagerToolStripMenuItem_Click);
            // 
            // refreshToolStripMenuItem
            // 
            this.refreshToolStripMenuItem.Name = "refreshToolStripMenuItem";
            this.refreshToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.refreshToolStripMenuItem.Text = "Refresh";
            this.refreshToolStripMenuItem.Click += new System.EventHandler(this.refreshToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // browserToolStripMenuItem
            // 
            this.browserToolStripMenuItem.Name = "browserToolStripMenuItem";
            this.browserToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.browserToolStripMenuItem.Text = "Browser";
            this.browserToolStripMenuItem.Click += new System.EventHandler(this.browserToolStripMenuItem_Click);
            // 
            // QLDebug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(790, 668);
            this.Controls.Add(this.tabs);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "QLDebug";
            this.Text = "Quick Launch Debug";
            this.tabs.ResumeLayout(false);
            this.tbSystemInfo.ResumeLayout(false);
            this.tbSystemInfo.PerformLayout();
            this.tbOS.ResumeLayout(false);
            this.tbOS.PerformLayout();
            this.tabProcs.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvProcs)).EndInit();
            this.tbEventLog.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvEvents)).EndInit();
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabs;
        private System.Windows.Forms.TabPage tabProcs;
        private System.Windows.Forms.DataGridView gvProcs;
        private System.Windows.Forms.TabPage tbSystemInfo;
        private System.Windows.Forms.TabPage tbOS;
        private System.Windows.Forms.Label lblSysInfo;
        private System.Windows.Forms.Label lblOS;
        private System.Windows.Forms.TabPage tbEventLog;
        private System.Windows.Forms.DataGridView gvEvents;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem fToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commandPromptToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem refreshToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem registryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem servicesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem networkConnectionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eventViewerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addRemoveProgramsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taskManagerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem browserToolStripMenuItem;


    }
}