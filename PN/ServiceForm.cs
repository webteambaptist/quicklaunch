﻿using QuickLaunch.Shared;
using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Windows.Forms;

namespace QuickLaunch
{
    public partial class ServiceForm : Form
    {
        private string email;
        private string uname;
        private string uid;

        public ServiceForm()
        {
            InitializeComponent();
        }

        public ServiceForm(string user)
        {
            InitializeComponent();
            this.CenterToScreen();
            pnlStep1.Visible = true;
            pnlStep1.BringToFront();
            pnlStep2.Visible = false;
            this.Text = "Submit a Service Desk Issue For: " + getName(user);
            txtPhone1.SelectionStart = txtPhone1.MaskedTextProvider.LastAssignedPosition + 1;
            txtPhone1.SelectionLength = 0;
            txtPhone2.SelectionStart = txtPhone2.MaskedTextProvider.LastAssignedPosition + 1;
            txtPhone2.SelectionLength = 0;
            this.Refresh();
        }

        private string getName(string user)
        {
            try
            {
                pplsearch.People ps = new pplsearch.People();
                ps.UseDefaultCredentials = true;
                pplsearch.PrincipalInfo[] plist = ps.SearchPrincipals(user, 1, pplsearch.SPPrincipalType.User);
                if (plist.Length > 0)
                {
                    uname = plist[0].DisplayName;
                    email = plist[0].Email;
                    uid = plist[0].AccountName;

                }
            }
            catch (Exception ee)
            {

            }
            return uname;
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            bool isvalid = validateEntry();
            if (isvalid)
            {
                isvalid = gatherData();
            }

            if (isvalid)
            {
                pnlStep1.Visible = false;
                pnlStep2.BringToFront();
                pnlStep2.Visible = true;
                this.Refresh();
            }
        }

        private bool gatherData()
        {
            bool isvalid = true;
            try
            {
                string ram = string.Empty;
                StringBuilder sbComputer = new StringBuilder();
                try
                {
                    System.Collections.ArrayList al = Shared.SysHelper.GetWMIAttributes("Win32_ComputerSystem", string.Empty);
                    foreach (string s in al.ToArray(typeof(string)))
                    {
                        sbComputer.AppendLine(s);
                        if (s.StartsWith("TotalPhysicalMemory = "))
                            ram = s.Substring("TotalPhysicalMemory = ".Length);
                    }
                }
                catch
                {
                    isvalid = false;
                }

                StringBuilder sbOS = new StringBuilder();
                try
                {
                    System.Collections.ArrayList al2 = Shared.SysHelper.GetWMIAttributes("Win32_OperatingSystem", "where Primary='true'");
                    foreach (string s in al2.ToArray(typeof(string)))
                        sbOS.AppendLine(s);
                }
                catch
                {
                    isvalid = false;
                }

                Models.Machine machine = (new Models.Machine
                {
                    PCName = System.Environment.MachineName,
                    LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper(),
                    QLVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    ICAVersion = Models.Machine.GetIcaVersion(),
                    IEVersion = Models.Machine.GetIEVersion(),
                    ComputerInfo = sbComputer.ToString(),
                    OSInfo = sbOS.ToString(),
                    Processes = Shared.SysHelper.GetSystemProcesses().ToString(),
                    Memory = ram,
                    IpAddress = SysHelper.GetIpAddress(),
                    AutoInstallVersion = Models.Machine.GetAutoInstallVersion(),
                    LastLogin = DateTime.Now
                });

                if (machine.PCName.ToLower().StartsWith("vclin")) //VDI
                {
                    try
                    {
                        string clientName = SysHelper.GetClientName(machine.PCName);
                        machine.ComputerInfo = "HostName: " + clientName + ", " + machine.ComputerInfo;
                        machine.IpAddress = SysHelper.GetClientIP(machine.PCName);
                    }
                    catch(Exception ex)
                    {
                        //just use the defaults then

                    }
                }

                if (isvalid)
                {
                    //now populate results on the second panel
                    pnlStep1.Visible = false;
                    lblName.Text = lblName.Text + uname;
                    txtMsg.Text = "Campus: " + txtCampus.Text + " Department: " + txtDept.Text + " Floor: " + txtFloor.Text + " " + txtMsg.Text;
                    lblPhone.Text = lblPhone.Text + txtPhone1.Text;
                    lblUID.Text = lblUID.Text + uid;
                    lblLSID.Text = lblLSID.Text + machine.LSID;
                    lblAltName.Text = txtAltName.Text + " " + txtPhone2.Text;
                    txtIssue.Text = txtMsg.Text;
                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("Phone: " + txtPhone1.Text);
                    sb.AppendLine(txtMsg.Text);

                    string serial = machine.PCName;
                    string output = "";
                    

                    if (string.IsNullOrEmpty(txtSN.Text)) //only add info if problem is for this machine
                    {
                        sb.AppendLine();
                        sb.AppendLine("Machine Info: ");
                        sb.AppendLine();
                        sb.AppendLine("Description: " + machine.Description);
                        sb.AppendLine("QL Version: " + machine.QLVersion);
                        sb.AppendLine("ICA Version: " + machine.ICAVersion);
                        sb.AppendLine("IE Version: " + machine.IEVersion);
                        //sb.AppendLine("AutoInstallVersion: " + machine.AutoInstallVersion);
                        //sb.AppendLine("Computer Info: " + machine.ComputerInfo);
                        //sb.AppendLine("OS Info: " + machine.OSInfo);
                        sb.AppendLine("IP Address: " + machine.IpAddress);

                        lblSN.Text = lblSN.Text + machine.PCName;

                        if (!string.IsNullOrEmpty(txtAltName.Text))
                        {
                            sb.AppendLine("Alt Name & Number: " + txtAltName.Text + " " + txtPhone2.Text);
                        }

                        output = sb.ToString();
                    }
                    else
                    {
                        serial = txtSN.Text;
                        lblSN.Text = lblSN.Text + txtSN.Text;

                        if (!string.IsNullOrEmpty(txtAltName.Text))
                        {
                            output = "Phone: " + txtPhone1.Text + " " + txtMsg.Text + " " + "Alt Name & Number: " + txtAltName.Text + " " + txtPhone2.Text;
                        }
                        else
                        {
                            output = "Phone: " + txtPhone1.Text + " " + txtMsg.Text;
                        }
                       
                    }
                    
                    pnlStep2.Visible = true;



                    if (!createTicket(uname, serial, txtPhone1.Text, uid, machine.LSID, output, email))
                    {
                        sendEmail(machine);
                    }

                    try
                    {
                        Models.ServiceFormData sfd = new Models.ServiceFormData();
                        sfd.Email = email;
                        sfd.UserName = uname;
                        sfd.UserID = uid;
                        sfd.PCName = machine.PCName;
                        sfd.LSID = machine.LSID;
                        sfd.ContactPhone = txtPhone1.Text;
                        sfd.AltName = txtAltName.Text;
                        sfd.AltPhone = txtPhone2.Text;
                        sfd.Message = "Campus: " + txtCampus.Text + " Department: " + txtDept.Text + " Floor: " + txtFloor.Text + " " + lblTicket.Text; //txtMsg.Text;
                        Models.ServiceFormData.SaveFormData(sfd);
                    }
                    catch (Exception ee)
                    { }

                }
                else
                {
                    MessageBox.Show("An error occured, please try again later.");
                }



            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
                isvalid = false;
            }

            return isvalid;
        }

        private bool createTicket(string name, string serial, string phone, string userID, string LSID, string txtMsg, string email)
        {
            bool isSuccess = true;

            try
            {
                string user = ConfigurationManager.AppSettings["esmUser"].ToString();
                string pass = ConfigurationManager.AppSettings["esmPass"].ToString();
                string acct = ConfigurationManager.AppSettings["esmAccount"].ToString();
                string guid = ConfigurationManager.AppSettings["esmGUID"].ToString();

                string userout = SysHelper.Decrypt(user);
                string passout = SysHelper.Decrypt(pass);

                EZV.WebService ws = new EZV.WebService();

                
                string ezvTask = ws.EZV_CreateRequest(acct, userout, passout, guid, "", "", "", serial, "3", "41", "", phone, "", email, name, "", "", "", "", "", "", email, name, "12", txtMsg, "", "", "", "", "");

                if (!ezvTask.ToUpper().Trim().StartsWith("I"))
                {
                    isSuccess = false;
                    lblTicket.Text = "Ticket #: EMAIL";
                }
                else
                {
                    lblTicket.Text = "Ticket #: " + ezvTask;
                }

            }
            catch(Exception ex)
            {
                isSuccess = false;
            }

            return isSuccess;
        }

        private void sendEmail(Models.Machine machine)
        {
            string mailto = Utility.getEmail();
            if(string.IsNullOrEmpty(mailto))
            {
                mailto = "servicedesk@bmcjax.com";
            }

            //mailto = "samuel.bartlett@bmcjax.com";

            if (!string.IsNullOrEmpty(email))
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(email);
                message.To.Add(new MailAddress(mailto));
                // message.Bcc.Add(new MailAddress("servicedesk@bmcjax.com"));
                message.Subject = "Issue Submitted from QL Help Form";

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Please review the information below:");
                sb.AppendLine();
                sb.AppendLine("Name: " + uname);
                sb.AppendLine("UserID: " + uid);
                sb.AppendLine("S/N of machine: " + machine.PCName);
                sb.AppendLine("UserID of machine: " + machine.LSID);
                sb.AppendLine();
                sb.AppendLine("Contact Phone Number: " + txtPhone1.Text);
                sb.AppendLine("Alt Name & Number: " + txtAltName.Text + " " + txtPhone2.Text);
                sb.AppendLine();
                sb.AppendLine(txtMsg.Text);
                sb.AppendLine();
                sb.AppendLine("Machine Info: ");
                sb.AppendLine();
                sb.AppendLine("Description: " + machine.Description);
                sb.AppendLine("QL Version: " + machine.QLVersion);
                sb.AppendLine("ICA Version: " + machine.ICAVersion);
                sb.AppendLine("IE Version: " + machine.IEVersion);
                sb.AppendLine("AutoInstallVersion: " + machine.AutoInstallVersion);
                sb.AppendLine("Computer Info: " + machine.ComputerInfo);
                sb.AppendLine("OS Info: " + machine.OSInfo);
                sb.AppendLine("IP Address: " + machine.IpAddress);

                message.Body = sb.ToString();

                //try to attach most recent log

                try
                {
                    string logFileName = Utility.getPath() + "QuickLaunch" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                    if (File.Exists(logFileName))
                    {
                        Attachment attach = new Attachment(logFileName);
                        message.Attachments.Add(attach);
                    }
                }
                catch(Exception ee)
                { }
             

                try
                {
                    SmtpClient smtpClient = new SmtpClient("smtp.bmcjax.com");
                    smtpClient.Port = 25;
                    smtpClient.Send(message);
                    //Models.EventLog_QL.LogEvent("FORM SENT", uname, "", 0);
                    //Models.ServiceFormData sfd = new Models.ServiceFormData();
                    //sfd.Email = email;
                    //sfd.UserName = uname;
                    //sfd.UserID = uid;
                    //sfd.PCName = machine.PCName;
                    //sfd.LSID = machine.LSID;
                    //sfd.ContactPhone = txtPhone1.Text;
                    //sfd.AltName = txtAltName.Text;
                    //sfd.AltPhone = txtPhone2.Text;
                    //sfd.Message = txtMsg.Text;
                 //   Models.ServiceFormData.SaveFormData(sfd);
                    Truncus.LogEvents("FORM SENT", "", uname, 0, "0");

                }//end try
                catch (Exception e)
                {
                    //ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                    //log.Error(e.Message);
                }

            }
        }



        private bool validateEntry()
        {
            bool isValid = true;

            if (txtSN.Visible == true)
            {
                if(string.IsNullOrEmpty(txtSN.Text))
                {
                    isValid = false;
                }

                if(string.IsNullOrEmpty(txtMsg.Text))
                {
                    isValid = false;
                }

                if(string.IsNullOrEmpty(txtPhone1.Text))
                {
                    isValid = false;
                }

            }
            else
            {
                if (string.IsNullOrEmpty(txtMsg.Text))
                {
                    isValid = false;
                }

                if (string.IsNullOrEmpty(txtPhone1.Text))
                {
                    isValid = false;
                }
            }

            return isValid;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void rbYes_Click(object sender, EventArgs e)
        {
            label5.Visible = false;
            txtSN.Visible = false;
            lblPrn.Visible = false;
        }

        private void rbNo_Click(object sender, EventArgs e)
        {
            label5.Visible = true;
            txtSN.Visible = true;
            lblPrn.Visible = true;
        }

        private void btnQuit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtPhone1_Click(object sender, EventArgs e)
        {
            this.txtPhone1.Select(0, 0);
        }

        private void txtPhone2_Click(object sender, EventArgs e)
        {
            this.txtPhone2.Select(0, 0);
        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

       
    }
}
