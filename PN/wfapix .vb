Attribute VB_Name = "WFAPIX"
Option Explicit

'**********************************************************************
'
' WFAPIX.BAS
'
' Citrix Server SDK - WFAPI Visual Basic Interface.
'
' Copyright 1999, Citrix Systems Inc.
'
' This file contains functions for easier Visual Basic access of Citrix
' Server SDK functions.  The module wfapi.bas must be included in the
' same project as this file.
'
'**********************************************************************

'
' CTXOpenServer:
'
' Wrapper for WFOpenServerA.  Returns TRUE/FALSE and returns handle
' as the first parameter.
'

Public Function CTXOpenServer(svrHandle As Long, _
                              svrName As String) As Boolean

On Error GoTo ERR_CTXOpenServer

    svrHandle = WFOpenServerA(svrName)
    If svrHandle <> 0 Then
        CTXOpenServer = True
    Else
        CTXOpenServer = False
    End If
    Exit Function

ERR_CTXOpenServer:
    Exit Function
End Function

'
' CTXEnumerateServers:
'
' Wrapper for WFEnumerateServerA.  Returns a list of server names.
'

Public Function CTXEnumerateServers(domain As String, _
                                    svrNames() As String) As Boolean
    Dim svrCnt As Long
    Dim cnt As Integer
    Dim svrArrayPtr As Long
    Dim svrInfoPtr  As Long
    Dim tmpName() As Byte
    Dim svrNameLen As Integer
    Dim X As Integer
    Dim domainName As String
    
On Error GoTo ERR_CTXEnumerateServers

    CTXEnumerateServers = False
    
    domainName = domain & vbNullChar
    If WFEnumerateServersA(domainName, 0, 1, svrArrayPtr, svrCnt) Then
        If svrCnt > 0 Then
            ReDim svrNames(svrCnt)
            For cnt = 0 To svrCnt - 1
                Call RtlMoveMemory(svrInfoPtr, svrArrayPtr + (cnt * 4), 4)

                svrNameLen = StrLenByRef(svrInfoPtr)
                ReDim tmpName(svrNameLen)
                
                Call RtlMoveMemory(tmpName(0), svrInfoPtr, svrNameLen)
                
                For X = 0 To svrNameLen - 1
                   svrNames(cnt) = svrNames(cnt) & Chr$(tmpName(X))
                Next X
                
                Erase tmpName
            Next cnt
            CTXEnumerateServers = True
            Call WFFreeMemory(svrArrayPtr)
        End If
    End If
    Exit Function
    
ERR_CTXEnumerateServers:
    Exit Function
End Function

'
' CTXEnumerateSessions:
'
' Enumerate sessions.
'

Public Function CTXEnumerateSessions( _
                    svrName As String, _
                    sessionAttrib() As WF_SESSION_INFO) _
                    As Boolean

    Dim sessionCnt     As Long
    Dim sessionPtr     As Long
    Dim svrHndl        As Long
    Dim sessionInfoPtr As Long
    Dim winStnNamePtr  As Long
    Dim winStnNameLen  As Long
    Dim tmpName()      As Byte
    Dim cnt            As Integer
    Dim X              As Integer

On Error GoTo ERR_CTXEnumerateSessions

    CTXEnumerateSessions = False
    
    If CTXOpenServer(svrHndl, svrName) Then
        If WFEnumerateSessionsA(svrHndl, 0, 1, sessionPtr, sessionCnt) Then
            If sessionCnt > 0 Then
                ReDim sessionAttrib(sessionCnt)
                For cnt = 0 To sessionCnt - 1
                    'retrieve session ID
                    Call RtlMoveMemory(sessionAttrib(cnt).SessionId, _
                                       sessionPtr + _
                                       (cnt * Len(sessionAttrib(cnt))), _
                                       4)
                    
                    'retrieve Winstation name
                    Call RtlMoveMemory(winStnNamePtr, _
                                       sessionPtr + _
                                       (cnt * Len(sessionAttrib(cnt))) + 4, _
                                       4)
                    
                    winStnNameLen = StrLenByRef(winStnNamePtr)
                    ReDim tmpName(winStnNameLen)
                    
                    Call RtlMoveMemory(tmpName(0), _
                                       winStnNamePtr, _
                                       winStnNameLen)
                    
                    For X = 0 To winStnNameLen - 1
                        sessionAttrib(cnt).pWinStationName = _
                            sessionAttrib(cnt).pWinStationName & _
                            Chr$(tmpName(X))
                    Next X
                    
                    Erase tmpName
                    
                    'retrieve winstation status
                    Call RtlMoveMemory(sessionAttrib(cnt).State, _
                                       sessionPtr + _
                                       (cnt * Len(sessionAttrib(cnt))) + 8, _
                                       4)
                    
                Next cnt
                CTXEnumerateSessions = True
            End If
            Call WFFreeMemory(sessionPtr)
        End If
        Call WFCloseServer(svrHndl)
    End If
    Exit Function
    
ERR_CTXEnumerateSessions:
    Exit Function
End Function

'
' CTXQuerySessionString:
'
' Wrapper for WFQuerySessionInformationA.
'

Private Function CTXQuerySessionString(svrHandle As Long, _
                                       SessionId As Long, _
                                       sessionParam As Long, _
                                       retString As String) As Boolean

    Dim bufPtr As Long
    Dim bufLen As Long
    Dim tmpStr() As Byte
    Dim X As Integer
    
On Error GoTo ERR_CTXQuerySessionString

    CTXQuerySessionString = False

    If WFQuerySessionInformationA(svrHandle, _
                                  SessionId, _
                                  sessionParam, _
                                  bufPtr, _
                                  bufLen) Then
        If bufLen > 0 Then
            ReDim tmpStr(bufLen)
            
            Call RtlMoveMemory(tmpStr(0), bufPtr, bufLen)
                
            For X = 0 To bufLen - 2
                retString = retString & Chr$(tmpStr(X))
            Next X
                
            Erase tmpStr
        End If
        Call WFFreeMemory(bufPtr)
        CTXQuerySessionString = True
    End If
    Exit Function
    
ERR_CTXQuerySessionString:
    Exit Function
End Function

'
' CTXQuerySessionInformation:
'
' Wrapper for WFQuerySessionInformationA.
'
Public Function CTXQuerySessionInformation(svrName As String, _
                                    SessionId As Long, _
                                    sessionParam As WF_INFO_CLASS, _
                                    retString As String) As Boolean

    Dim svrHndl         As Long
    Dim idx             As Integer
    Dim X               As Integer
    Dim addressType     As Long
    Dim bufPtr          As Long
    Dim bufLen          As Long
    Dim retLong         As Long
    Dim clientDisplay   As WF_CLIENT_DISPLAY
    Dim osVersion       As OSVERSIONINFO
    Dim cltCache        As WF_CLIENT_CACHE
    Dim cltDrives       As WF_CLIENT_DRIVES
    Dim cltDrive        As WF_DRIVES
    Dim cltLatency      As WF_CLIENT_LATENCY
    Dim headptr         As Long
    Dim ptrFamilyType   As Long
    Dim tmpName()       As Byte
    Dim tmpRet          As String
    Dim ptrClAddress    As String * 20
    Dim StrPart         As String
    Dim retStruct       As String
    Dim tmpVal          As Long
    Dim tmpStr          As String
    Dim tmpIPX          As String
    Dim tmpLongValue    As Long
    
On Error GoTo ERR_CTXQuerySessionInformation

    CTXQuerySessionInformation = False
        
    If CTXOpenServer(svrHndl, svrName) Then
        retString = ""
        Select Case sessionParam
            Case WFApplicationName, WFUserName, WFDomainName, _
                 WFInitialProgram, WFWorkingDirectory, WFOEMId, _
                 WFClientName, WFClientDirectory, WFWinstationName
                If CTXQuerySessionString(svrHndl, _
                                         SessionId, _
                                         sessionParam, _
                                         retString) Then
                    CTXQuerySessionInformation = True
                End If
            Case WFConnectState
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(retLong, bufPtr, bufLen)
                        Select Case retLong
                            Case WFActive
                                retString = "Active"
                            Case WFConnected
                                retString = "Connected"
                            Case WFConnectQuery
                                retString = "Connect Query"
                            Case WFShadow
                                retString = "Shadow"
                            Case WFDisconnected
                                retString = "Disconnected"
                            Case WFIdle
                                retString = "Idle"
                            Case WFListen
                                retString = "Listen"
                            Case WFReset
                                retString = "Reset"
                            Case WFDown
                                retString = "Down"
                            Case WFInit
                                retString = "Initializing"
                        End Select
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFClientAddress
                If CTXQuerySessionString(svrHndl, _
                                         SessionId, _
                                         sessionParam, _
                                         retString) Then
                    Call RtlMoveMemory(addressType, retString, 4)
                    ReDim tmpName(20)
                    Call RtlMoveMemory(tmpName(0), retString, 20)
                    For X = 0 To 20 - 1
                        tmpStr = tmpStr & Chr$(tmpName(X))
                    Next X
                    Select Case addressType
                        Case AF_INET
                            retString = Asc(Mid$(retString, 7, 1)) & "." & _
                                        Asc(Mid$(retString, 8, 1)) & "." & _
                                        Asc(Mid$(retString, 9, 1)) & "." & _
                                        Asc(Mid$(retString, 10, 1))
                        Case Else
                        If addressType = AF_IPX Then
                            For X = 4 To 7
                                tmpIPX = tmpIPX & Hex(tmpName(X))
                            Next X
                                tmpIPX = tmpIPX & ":"
                            For X = 8 To 13
                                tmpIPX = tmpIPX & Hex(tmpName(X))
                            Next X
                            StrPart = tmpIPX
                        Else
                            StrPart = Mid(tmpStr, 5, 12)
                        End If
                        Erase tmpName
                        retStruct = retStruct & StrPart
                        retString = retStruct
                    End Select
                    CTXQuerySessionInformation = True
                End If
            Case WFClientBuildNumber, WFClientProductID
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(retLong, bufPtr, bufLen)
                        retString = CStr(retLong)
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFICABufferLength, WFSessionId
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(retLong, bufPtr, bufLen)
                        retString = CStr(retLong)
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFClientDisplay
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(clientDisplay, bufPtr, bufLen)
                        Select Case clientDisplay.ColorDepth
                            Case 1
                                retString = "16 "
                            Case 2
                                retString = "256 "
                            Case 4
                                retString = "64K "
                            Case 8
                                retString = "16M "
                        End Select
                        retString = "Color depth: " & _
                                    retString & _
                                    " Resolution: " & _
                                    CStr(clientDisplay.HorizontalResolution) & _
                                    " x " & _
                                    CStr(clientDisplay.VerticalResolution)
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFVersion
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(osVersion, bufPtr, bufLen)
                        retString = "Citrix Server Version: major " & _
                                    osVersion.MajorVersion & _
                                    ", minor " & osVersion.MinorVersion & _
                                    ", build " & osVersion.BuildNumber & _
                                    ", CSD: " & osVersion.pCSDVersion
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFUserInfo
             'WFUserInfo
            If WFQuerySessionInformationA(svrHndl, _
                                           SessionId, _
                                           sessionParam, _
                                           bufPtr, _
                                           bufLen) Then
                If bufPtr <> 0 Then
                    'retrive User Name
                    headptr = bufPtr
                    StrPart = GetStringValue(headptr)
                    retStruct = StrPart
                    'retrieve Working directory
                    headptr = headptr + 4
                    StrPart = GetStringValue(headptr)
                    retStruct = retStruct & StrPart
                    'retrieve Application Name
                    headptr = headptr + 4
                    StrPart = GetStringValue(headptr)
                    retStruct = retStruct & StrPart
                    CTXQuerySessionInformation = True
                End If
                retString = retStruct
                Call WFFreeMemory(bufPtr)
            End If
            Case WFAppInfo
             'WFUserInfo
            If WFQuerySessionInformationA(svrHndl, _
                                           SessionId, _
                                           sessionParam, _
                                           bufPtr, _
                                           bufLen) Then
                If bufPtr <> 0 Then
                    'retrive User Name
                    headptr = bufPtr
                    StrPart = GetStringValue(headptr)
                    retStruct = StrPart
                    'retrieve Working directory
                    headptr = headptr + 4
                    StrPart = GetStringValue(headptr)
                    retStruct = retStruct & StrPart
                    'retrieve Application Name
                    headptr = headptr + 4
                    StrPart = GetStringValue(headptr)
                    retStruct = retStruct & StrPart
                    CTXQuerySessionInformation = True
                End If
                retString = retStruct
                Call WFFreeMemory(bufPtr)
            End If
            'new one starts from here
            Case WFClientInfo
            
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                If bufPtr <> 0 Then
                    'retrieve name
                    headptr = bufPtr
                    StrPart = GetStringValue(headptr)
                    retStruct = StrPart
                    'retrieve directory
                    headptr = headptr + 4
                    StrPart = GetStringValue(headptr)
                    retStruct = retStruct & StrPart
                    'Build number
                    headptr = headptr + 4
                    Call RtlMoveMemory(tmpLongValue, headptr, 4)
                    retStruct = retStruct & tmpLongValue & ","
                    ' ProductId
                    headptr = headptr + 4
                    Call RtlMoveMemory(tmpLongValue, headptr, 4)
                    retStruct = retStruct & tmpLongValue & ","
                    ' HardWareId
                    headptr = headptr + 4
                    Call RtlMoveMemory(tmpLongValue, headptr, 4)
                    retStruct = retStruct & tmpLongValue & ","
                    ' Client Address
                    headptr = headptr + 4
                    Call RtlMoveMemory(tmpVal, headptr, 4)
                    'If tmpVal = AF_INET Then
                        ReDim tmpName(20)
                        Call RtlMoveMemory(tmpName(0), headptr + 4, 20)
                        For X = 0 To 20 - 1
                            tmpStr = tmpStr & Chr$(tmpName(X))
                        Next X
                        'Erase tmpName
                    If tmpVal = AF_INET Then
                        StrPart = Asc(Mid$(tmpStr, 3, 1)) & "." & _
                                  Asc(Mid$(tmpStr, 4, 1)) & "." & _
                                  Asc(Mid$(tmpStr, 5, 1)) & "." & _
                                 Asc(Mid$(tmpStr, 6, 1))
                    Else
                        If tmpVal = AF_IPX Then
                            For X = 0 To 3
                                tmpIPX = tmpIPX & Hex(tmpName(X))
                            Next X
                                tmpIPX = tmpIPX & ":"
                            For X = 4 To 9
                                tmpIPX = tmpIPX & Hex(tmpName(X))
                            Next X
                            StrPart = tmpIPX
                        Else
                            StrPart = Left(tmpStr, 12)
                        End If
                    End If
                    Erase tmpName
                    retStruct = retStruct & StrPart & ","
                    retString = retStruct
                CTXQuerySessionInformation = True
                Call WFFreeMemory(bufPtr)
            End If
            End If
                                  
            Case WFClientCache
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(cltCache, bufPtr, bufLen)
                        retString = "CacheTiny - " & _
                                    cltCache.CacheTiny & _
                                    ", CacheLowMem - " & _
                                    cltCache.CacheLowMem & _
                                    ", CacheXMS - " & _
                                    cltCache.CacheXms & _
                                    ", CacheDisk - " & _
                                    cltCache.CacheDisk & _
                                    ", CacheSize - " & _
                                    cltCache.DimCacheSize & _
                                    ", CacheMinBitmap - " & _
                                    cltCache.DimBitmapMin & _
                                    ", CacheSignatureLevel - " & _
                                    cltCache.DimSignatureLevel & _
                                    ", CacheFilesysOverhead - " & _
                                    cltCache.DimFilesysOverhead
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFClientLatency
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(cltLatency, bufPtr, bufLen)
                        retString = "Average  - " & _
                                    cltLatency.Average & _
                                    ", Last - " & _
                                    cltLatency.Last & _
                                    ", Deviation - " & _
                                    cltLatency.Deviation
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFClientDrives
                If WFQuerySessionInformationA(svrHndl, _
                                              SessionId, _
                                              sessionParam, _
                                              bufPtr, _
                                              bufLen) Then
                    If bufLen > 0 Then
                        Call RtlMoveMemory(cltDrives.fAutoClientDrives, _
                                           bufPtr, _
                                           4)
                        Call RtlMoveMemory(cltDrives.Count, bufPtr + 4, 4)
                        retString = ""
                        For idx = 0 To cltDrives.Count - 1
                            Call RtlMoveMemory(cltDrive, _
                                               (bufPtr + 8 + _
                                                (idx * Len(cltDrive))), _
                                               Len(cltDrive))
                            retString = retString & _
                                        Chr$(cltDrive.DriveLetter) & " - "
                            
                            If cltDrive.Flags And WF_DRIVE_REMOVEABLE Then _
                                retString = retString & "Removeable "
                            If cltDrive.Flags And WF_DRIVE_FIXED Then _
                                retString = retString & "Fixed "
                            If cltDrive.Flags And WF_DRIVE_REMOTE Then _
                                retString = retString & "Remote "
                            If cltDrive.Flags And WF_DRIVE_CDROM Then _
                                retString = retString & "CDRom "
                            
                            retString = retString & ", "
                        Next idx
                        retString = Mid$(retString, 1, Len(retString) - 2)
                        CTXQuerySessionInformation = True
                    End If
                    Call WFFreeMemory(bufPtr)
                End If
            Case WFClientHardwareId, WFLicenseEnabler
                retString = ""
                CTXQuerySessionInformation = True
            Case Else
                CTXQuerySessionInformation = False
        End Select
        
        Call WFCloseServer(svrHndl)
    Else
        MsgBox "Could not open server - " & svrName
    End If
    Exit Function
    
ERR_CTXQuerySessionInformation:
    MsgBox "reached here error", vbExclamation, "WFAPIX"
    Exit Function
End Function

'
' CTXEnumerateProcesses:
'
' Wrapper for WFEnumerateProcessesA.
'

Public Function CTXEnumerateProcesses( _
                    svrName As String, _
                    processStruct() As WF_PROCESS_INFO) _
                    As Boolean

    Dim svrHndl         As Long
    Dim processCnt      As Long
    Dim ptrProcess      As Long
    Dim ptrProcessName  As Long
    Dim processNameLen  As Long
    Dim tmpName()       As Byte
    Dim idx             As Integer
    Dim X               As Integer
    Dim pProcInfo       As Long

On Error GoTo ERR_CTXEnumerateProcesses

    CTXEnumerateProcesses = False
    
    If CTXOpenServer(svrHndl, svrName) Then
        If WFEnumerateProcessesA(svrHndl, 0, 1, ptrProcess, processCnt) Then
            If processCnt > 0 Then
                ReDim processStruct(processCnt)

                pProcInfo = ptrProcess
                For idx = 0 To processCnt - 1
                    'retrieve session ID
                    Call RtlMoveMemory(processStruct(idx).SessionId, _
                                       pProcInfo, _
                                       Len(processStruct(idx).SessionId))
                    
                    'retrieve process ID
                    Call RtlMoveMemory(processStruct(idx).ProcessId, _
                                       pProcInfo + 4, _
                                       Len(processStruct(idx).ProcessId))
                    
                    'retrieve process name
                    Call RtlMoveMemory(ptrProcessName, _
                                       pProcInfo + 8, _
                                       4)

                    If ptrProcessName <> 0 Then 'Not NULL then
                        processNameLen = StrLenByRef(ptrProcessName)
                        ReDim tmpName(processNameLen)
                        
                        Call RtlMoveMemory(tmpName(0), _
                                           ptrProcessName, _
                                           processNameLen)
                        
                        For X = 0 To processNameLen - 1
                            processStruct(idx).pProcessName = _
                                processStruct(idx).pProcessName & _
                                Chr$(tmpName(X))
                        Next X
                        
                        Erase tmpName
                    End If
                    
                    'retrieve user SID
                    Call RtlMoveMemory(processStruct(idx).pUserSid, _
                                       pProcInfo + 12, _
                                       Len(processStruct(idx).pUserSid))
                    pProcInfo = pProcInfo + WF_PROCESS_INFO_SIZE
                Next idx
                
                CTXEnumerateProcesses = True
            End If
            Call WFFreeMemory(ptrProcess)
        End If
        Call WFCloseServer(svrHndl)
    Else
        MsgBox "Could not open server - " & svrName
    End If
    Exit Function

ERR_CTXEnumerateProcesses:
    Exit Function
End Function

'
' CTXTerminateProcess:
'
' Terminate a process.
'

Public Function CTXTerminateProcess(svrName As String, pid As Long) As Boolean
    Dim svrHndl  As Long
    Dim exitCode As Long
    
On Error GoTo ERR_CTXTerminateProcess

    CTXTerminateProcess = False
    If CTXOpenServer(svrHndl, svrName) Then
        If WFTerminateProcess(svrHndl, pid, exitCode) Then _
            CTXTerminateProcess = True
        Call WFCloseServer(svrHndl)
    End If
    Exit Function

ERR_CTXTerminateProcess:
    Exit Function
End Function

'
' CTXSendMessage:
'
' Send a message to another session.
'

Public Function CTXSendMessage(svrName As String, _
                               SessionId As Long, _
                               msgTitle As String, _
                               msgText As String, _
                               msgStyle As Long, _
                               msgTimeout As Long, _
                               msgResponse As Long, _
                               ByVal msgWait As Boolean) As Boolean
    Dim svrHndl As Long

On Error GoTo ERR_CTXSendMessage

    CTXSendMessage = False
    If CTXOpenServer(svrHndl, svrName) Then
        If WFSendMessageA(svrHndl, SessionId, msgTitle, Len(msgTitle), _
                          msgText, Len(msgText), msgStyle, msgTimeout, _
                          msgResponse, msgWait And (msgTimeout > 0)) Then
            CTXSendMessage = True

        End If
        Call WFCloseServer(svrHndl)
    End If
    Exit Function
    
ERR_CTXSendMessage:
    Exit Function
End Function

'
' CTXShutdownSystem:
'
' Wrapper for WFShutdownSystem.
'

Public Function CTXShutdownSystem(svrName As String, _
                                  shutdownMode As Long) As Boolean
    Dim svrHndl  As Long
    
On Error GoTo ERR_CTXShutdownSystem

    CTXShutdownSystem = False
    If CTXOpenServer(svrHndl, svrName) Then
        If WFShutdownSystem(svrHndl, shutdownMode) Then
            CTXShutdownSystem = True
        End If
        Call WFCloseServer(svrHndl)
    Else
        MsgBox "Can't open server " & svrName, vbOKOnly, "Error"
    End If
    Exit Function
    
ERR_CTXShutdownSystem:
    Exit Function
End Function

'
' CTXWaitSystemEvent:
'
' Wrapper for
Public Function CTXWaitSystemEvent(svrName As String, _
                                   EventMask As Long, _
                                   eventFlags As Long) As Boolean
    Dim svrHndl  As Long
    
On Error GoTo ERR_CTXWaitSystemEvent

    CTXWaitSystemEvent = False
    If CTXOpenServer(svrHndl, svrName) Then
        If WFWaitSystemEvent(svrHndl, EventMask, eventFlags) Then
            CTXWaitSystemEvent = True
        End If
        Call WFCloseServer(svrHndl)
    End If
    Exit Function

ERR_CTXWaitSystemEvent:
    Exit Function
End Function

'
' CTXLogoffSession:
'
' Wrapper for WFLogoffSession.
'

Public Function CTXLogoffSession(svrName As String, _
                                 SessionId As Long, _
                                 waitForLogoff As Boolean) As Boolean
    Dim svrHndl  As Long
    
On Error GoTo ERR_CTXLogoffSession

    CTXLogoffSession = False
    If CTXOpenServer(svrHndl, svrName) Then
        If WFLogoffSession(svrHndl, SessionId, waitForLogoff) Then
            CTXLogoffSession = True
        End If
        Call WFCloseServer(svrHndl)
    End If
    Exit Function

ERR_CTXLogoffSession:
    Exit Function
End Function

'
' CTXDisconnectSession:
'
' Wrapper for WFDisconnectSession
'

Public Function CTXDisconnectSession(svrName As String, _
                                     SessionId As Long, _
                                     waitForDisconnect As Boolean) As Boolean
    Dim svrHndl  As Long
    
On Error GoTo ERR_CTXDisconnectSession

    CTXDisconnectSession = False
    If CTXOpenServer(svrHndl, svrName) Then
        If WFDisconnectSession(svrHndl, SessionId, waitForDisconnect) Then
            CTXDisconnectSession = True
        End If
        Call WFCloseServer(svrHndl)
    End If
    Exit Function

ERR_CTXDisconnectSession:
    Exit Function
End Function

'
' CTXQueryUserConfig:
'
' Wrapper for WFQueryUserConfigA.
'

Public Function CTXQueryUserConfig(svrName As String, _
                                   userName As String, _
                                   configType As WF_CONFIG_CLASS, _
                                   retString As String) As Boolean
    Dim bufPtr      As Long
    Dim bufLen      As Long
    Dim tmpStr()    As Byte
    Dim idx         As Integer
    Dim cSvrName    As String
    Dim cUsrName    As String

On Error GoTo ERR_CTXQueryUserConfig

    CTXQueryUserConfig = False
    cSvrName = svrName & vbNullChar
    cUsrName = userName & vbNullChar
    If WFQueryUserConfigA(cSvrName, cUsrName, configType, bufPtr, bufLen) Then
        If bufLen > 0 Then
            ReDim tmpStr(bufLen)
            
            Call RtlMoveMemory(tmpStr(0), bufPtr, bufLen)
            For idx = 0 To bufLen - 1
                retString = retString & Chr$(tmpStr(idx))
            Next idx
            Erase tmpStr
        End If
        Call WFFreeMemory(bufPtr)
        CTXQueryUserConfig = True
    End If
    Exit Function
    
ERR_CTXQueryUserConfig:
    Exit Function
End Function

'
' CTXSetUserConfig:
'
' Wrapper for WFSetUserConfigA.
'

Public Function CTXSetUserConfig(svrName As String, _
                                 userName As String, _
                                 configType As WF_CONFIG_CLASS, _
                                 configParam As String) As Boolean
    Dim cSvrName    As String
    Dim cUsrName    As String

On Error GoTo ERR_CTXSetUserConfig

    CTXSetUserConfig = False
    
    cSvrName = svrName & vbNullChar
    cUsrName = userName & vbNullChar
    
    CTXSetUserConfig = WFSetUserConfigA(cSvrName, _
                                        cUsrName, _
                                        configType, _
                                        configParam, _
                                        Len(configParam))
    
    Exit Function
    
ERR_CTXSetUserConfig:
    Exit Function
End Function

'
' StrLenByRef:
'
' A helper function to return length of a string.  A pointer to the string is
' passed in.
'
' Count each character one by one.  The way to do it is to examine each byte
' and count the byte until it is 0.
'
Public Function StrLenByRef(ByRef pStr As Long) As Integer
    Dim I As Integer
    Dim C As Byte
    
On Error GoTo ERR_StrLenByRef

    StrLenByRef = 0
    
    I = 0
    Call RtlMoveMemory(C, pStr, 1)
    Do Until C = 0
        I = I + 1
        Call RtlMoveMemory(C, pStr + I, 1)
    Loop
    StrLenByRef = I
    Exit Function
    
ERR_StrLenByRef:
    Exit Function
End Function

'
'GetStringValue:
'
'   A helper function to return the string value from inside a structure
'   Pointer to the structure (Or a pointer to the string pointer is passed in
'   A "," is added after each value as a delimiter.This helps to seperate
'   the values from the retString
'   Returns the string value
'

Public Function GetStringValue(pHead As Long) As String
    Dim ptrSrc As Long
    Dim retString As String
    Dim tmpBuf() As Byte
    Dim SrcLen As Long
    Dim Index As Long
    Dim tmplong As Long
    
    'This will get the pointer to the string in ptrSrc
    Call RtlMoveMemory(ptrSrc, pHead, 4)
            If ptrSrc <> 0 Then
                SrcLen = StrLenByRef(ptrSrc)
                If SrcLen <> 0 Then
                    ReDim tmpBuf(SrcLen)
                    Call RtlMoveMemory(tmpBuf(0), ptrSrc, SrcLen)
                    retString = ""
                    For Index = 0 To SrcLen - 1
                        retString = retString & Chr$(tmpBuf(Index))
                    Next Index
                    Erase tmpBuf
                    GetStringValue = retString & ","
                Else
                    GetStringValue = " ,"
                End If
            Else
                GetStringValue = "Error in obtaining value.,"
            End If

End Function

