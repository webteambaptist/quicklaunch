﻿using com.citrix.wing.webpn;
using global::QuickLaunch.Shared;
using QuickLaunch.Domain.Models;
using QuickLaunch.Models;
//using Shell32; //"Microsoft Shell Control And Automation"
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Threading;
using System.Timers;
using System.Windows.Forms;
using WFICALib;
using OneSignAuthInterop;
using System.Runtime.InteropServices;

namespace QuickLaunch
{
    /// <summary>
    /// The UI for Quick Launch
    /// </summary>
    public partial class QuickLaunch : Form
    {


        #region Properties

        private System.Windows.Forms.Timer appTimer = new System.Windows.Forms.Timer();
        private ICAClientClass ica;
        private BackgroundWorker backgroundWorker;
        private BackgroundWorker icaWorker;
        private BackgroundWorker bwTSDisconnect;
        public delegate void AsyncMethodCaller();
        public delegate void RoamingDelegate();
        private OneSignEventProviderClass oneSign = new OneSignEventProviderClass();

        // private static System.Timers.Timer CheckStatusTimer = new System.Timers.Timer();
        // private static readonly int DefaultTimerInterval = 1000 * 60 * 3; //3 minutes

        public ImprivataEvent CurrentState { get; set; }
       // public DateTime SetCurrentUserStartTime { get; set; }
      //  System.Windows.Forms.Timer timerNoConnection = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer CitrixDowntimeTimer = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer timerRoam = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer timerLaunch = new System.Windows.Forms.Timer();
        Stopwatch timerRoamSearch = new Stopwatch();
        public Stopwatch timerLogin = new Stopwatch();

        UserInfo globalUser;

        /// <summary>
        /// Gets or sets the user context.
        /// </summary>
        public UserContext userContext { get; set; }

        /// <summary>
        /// Current Citrix application being launched
        /// </summary>
        public static Models.MenuApplication CurrentLaunchApplication { get; set; }

        /// <summary>
        /// Gets a value indicating whether this instance is physician.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is physician; otherwise, <c>false</c>.
        /// </value>
        public bool IsPhysician { get { return CurrentUser == null ? false : CurrentUser.UserType == "Physician" ? true : false; } }

        /// <summary>
        /// Gets or sets the current user.
        /// </summary>
        private UserInfo _CurrentUser;
        public UserInfo CurrentUser { get { return _CurrentUser; } set { _CurrentUser = value; CurrentUserId = value.UserName.ToUpper(); } }
        public static string CurrentUserId { get; set; }

        System.Windows.Forms.Timer icaFailTimer = new System.Windows.Forms.Timer();

        NotifyIcon niSystemAlert = new NotifyIcon(); //only show once    

        NotifyIcon niPwdExpire = new NotifyIcon(); //only show once       

        private List<FishMenuItem> _MenuItems;

        private static NetworkInterface[] adaptersStart = NetworkInterface.GetAllNetworkInterfaces();

        private string logLevel = "1";

        private static Stopwatch swRoam = new Stopwatch();

        private static Stopwatch swLaunch = new Stopwatch();

        private bool isAuto = false;

        private bool isLogon = false;

      //  private TimeSpan tsLoginTime = new TimeSpan();

        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="QuickLaunch"/> class. See QuickLaunchShared.cs for the rest of the code.
        /// </summary>

        public QuickLaunch()
        {         
            try
            {
                ica = ApplicationLauncher.GetICAClient();
                InitImprivata();
                // StartSVC();
                LoadTopIconTray();

            }
            catch (Exception ee) 
            {
                Program.LogException(ee, false); 
            } //sometimes on agent startup were not ready for this

            try
            {
                ConnectICAEvents(); //running this method async is of questionable value
                //IAsyncResult resultWireICAEvents = new AsyncMethodCaller(ConnectICAEvents).BeginInvoke(null, null); // Initiate the async call.
                //var rEventDisconnectCitrix = new ManualResetEvent(false);
                //ThreadPool.QueueUserWorkItem(q => { DisconnectCitrixSessionsIfNecessary(); rEventDisconnectCitrix.Set(); });
                InitializeComponent();

                SetTimerEvents();

                //NEW 
                //setup Imprivata Events

                //END NEW
                pnlFooter.DragDrop += new DragEventHandler(bookmark_DragDrop);
                pnlFooter.DragEnter += new DragEventHandler(bookmark_DragEnter);
                pnlFooter.DragLeave += new EventHandler(bookmark_DragLeave);

                foreach (Control c in pnlFooter.Controls)
                {
                    c.DragDrop += new DragEventHandler(bookmark_DragDrop);
                    c.DragEnter += new DragEventHandler(bookmark_DragEnter);
                    c.DragLeave += new EventHandler(bookmark_DragLeave); 
                }

                //NetworkChange.NetworkAddressChanged += new NetworkAddressChangedEventHandler(AddressChangedCallback);
                //in case need to handle network errors

                //get logLevel once for all methods
                try
                {
                    logLevel = Utility.getSettingFromCache("LogLevel");
                }
                catch (Exception ee)
                {
                    logLevel = "1";
                }

                UIHelper.SetupQLForm(this);
              //  rEventDisconnectCitrix.WaitOne();
            }
            catch(Exception ee)
            {
                Program.LogException(ee, false);
            }
        }

        #region onesignhandlers
        private void InitImprivata()
        {

            try
            {

                OneSignAuthSrvClass oneSignAuthSrv = new OneSignAuthSrvClass();
                OneSignAuthClass oneSignAuth = new OneSignAuthClass();
                IOneSignAuth12 oneSignAuth12 = (IOneSignAuth12)oneSignAuth;
                

                IOneSignAuth13 oneSignAuth13 = (IOneSignAuth13)oneSignAuth;

                
                IComUserIdentity userdent = (IComUserIdentity)oneSignAuth.CurrentOneSignUser;
                IComWindowsIdentity windent = (IComWindowsIdentity)oneSignAuth.CurrentWindowsUser;
                //TODO USER AS LSID CHECK

               // Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "userdent" + userdent.DisplayName);

                //set Event Handlers
                oneSign.OnUserLogin += OneSign_OnUserLogin;

                oneSign.OnLocked += OneSign_OnLocked;

               // oneSign.OnUserLogoff += oneSign_OnUserLogoff;

                oneSign.OnNonSSOUserAuthenticated += OneSign_OnNonSSOUserAuthenticated;

                //     oneSign.OnSessionConnected += OneSign_OnSessionConnected; //these don't do anything

                //     oneSign.OnSessionDisconnected += OneSign_OnSessionDisconnected; //these don't do anything

                oneSign.OnUnlocked += OneSign_OnUnlocked;

              //  oneSign.OnUserAuthenticated += OneSign_OnUserAuthenticated;

              //  oneSign.OnUserAuthenticationCanceled += OneSign_OnUserAuthenticationCanceled;

                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "Init IMP");

            }
            catch(COMException ex)
            {
                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "ERRORIMP: " + ex.Message);
            }
        }

        

        private void OneSign_OnUserLogin()
        {
            this.Hide();
            Application.Exit(); //closes QL, notifier respawns

           // Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "UserLogin");
        }

        private void OneSign_OnUserAuthenticationCanceled()
        {
           // Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "UserAuthenticationCanceled");
        }

        private void OneSign_OnUserAuthenticated()
        {
           // Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "OnUserAuthenticated");
        }

        private void OneSign_OnUnlocked()
        {
            try
            {
                if (CurrentUser != null)
                {
                    if (String.IsNullOrEmpty(CurrentUser.UserName))
                    {
                        SetCurrentUser(CurrentUser, ImprivataEvent.WorkstationUnlock);
                    }
                }

                Truncus.LogEvents(logLevel, "", CurrentUser.UserName, 0, "ImprivataEvent.WorkstationUnlock");
            }
            catch (Exception ex)
            { }
        }

        private void OneSign_OnSessionDisconnected()
        {
            Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "SessionDisconnected");
        }

        private void OneSign_OnSessionConnected()
        {
            Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "SessionConnected");
        }

        private void OneSign_OnNonSSOUserAuthenticated()
        {
            try
            {
                if (CurrentUser != null)
                {
                    if (String.IsNullOrEmpty(CurrentUser.UserName))
                    {
                        SetCurrentUser(CurrentUser, ImprivataEvent.NonOneSignLogon);
                    }
                }

                Truncus.LogEvents(logLevel, "", CurrentUser.UserName, 0, "ImprivataEvent.NonOneSignLogon");
            }
            catch (Exception ex)
            { }
        }

        private void OneSign_OnLocked()
        {
            try
            {
                if (CurrentUser != null)
                {
                    if (String.IsNullOrEmpty(CurrentUser.UserName))
                    {
                        SetCurrentUser(CurrentUser, ImprivataEvent.WorkstationLock);
                    }
                }

                Truncus.LogEvents(logLevel, "", CurrentUser.UserName, 0, "ImprivataEvent.WorkstationLock");
            }
            catch (Exception ex)
            { }
        }


        private void oneSign_OnUserLogoff()
        {
            this.Hide();

          //  Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "UserLogoff");
        }

        private void GetUser()
        {
            try
            {
                string user = "";
                string pass = "";
                string domain = "";

                IOneSignAuth _auth = new OneSignAuthClass();
                IComUserIdentity userIdent = (IComUserIdentity)_auth.CurrentOneSignUser;

                using (Loginifier logForm = new Loginifier())
                {
                    logForm.ShowDialog();
                    if (!string.IsNullOrEmpty(logForm.secret))
                    {
                        pass = logForm.secret;
                        user = userIdent.Username;
                    }
                }

                UserInfo uInfo = new UserInfo();
                uInfo.UserName = user;
                uInfo.Password = pass;
                uInfo.Domain = domain;
                CurrentUser = uInfo;
            }
            catch (Exception ex)
            { }
        }

        #endregion
        private void CheckStatusTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //CheckEmail(globalUser);
        }

        /// <summary>
        /// set event handlers for QL timers
        /// </summary>
        private void SetTimerEvents()
        {
          //  timerNoConnection.Interval = 10000;
          //  timerNoConnection.Tick += new EventHandler(timerNoConnection_Tick);

            CitrixDowntimeTimer.Interval = 8000;
            CitrixDowntimeTimer.Tick += new EventHandler(CitrixDowntimeTimer_Tick);

            int launchTimeout = 300000;

            try
            {
                string lTimeout = Utility.getSettingFromCache("LAUNCHTIMEOUT");
                launchTimeout = int.Parse(lTimeout);
            }
            catch (Exception ee)
            {
                launchTimeout = 300000;
            }

            timerLaunch.Interval = launchTimeout;
            timerLaunch.Tick += new EventHandler(timerLaunch_Tick);


            //new process to timeout background worker if it hangs
            int roamTimeout = 150000;

            try
            {
                string rTimeout = Utility.getSettingFromCache("ROAM-TIMEOUT");
                roamTimeout = int.Parse(rTimeout);
            }
            catch (Exception ee)
            {
                roamTimeout = 150000; //default setting if cache not avail
            }

            timerRoam.Interval = roamTimeout;
            timerRoam.Tick += new EventHandler(timerRoam_Tick);
        }

        private void timerRoam_Tick(object sender, EventArgs e)
        {
            timerRoam.Stop();
            if (backgroundWorker.IsBusy)
            {
               // backgroundWorker.CancelAsync();
                backgroundWorker.ReportProgress(100, "Timeout");
            }
        }

        private void timerLaunch_Tick(object sender, EventArgs e)
        {
            timerLaunch.Stop();
            if (backgroundWorker.IsBusy)
            {
                backgroundWorker.ReportProgress(100, "Timeout");
            }
           // backgroundWorker.CancelAsync();
        }
        
        #region User Events
        /// <summary>
        /// Sets the current user.
        /// </summary>
        /// <param name="user">The user.</param>
        public void SetCurrentUser(UserInfo user, ImprivataEvent imprivataEvent)
        {

            try
            {

                globalUser = user;  //set for reset function
                CurrentState = imprivataEvent;
                Models.AppQueue queue = new Models.AppQueue();

 
                switch (imprivataEvent)
                {

                    case ImprivataEvent.UserLogon:

                        isLogon = true;        
                        PrepareDesktop();
                        KillListedProcesses();
                        progressBar.Value = 20;  
                        QLUtility.KillIECookies(user.UserName);
                        progressBar.Value = 30;                        
                        CloseServiceForm();
                        DoLogin(user);

                        //removed printer cache clear, April 2015

                        break;
                    case ImprivataEvent.WorkstationLock:

                        isLogon = false;
                        QLUtility.LSIDCheck(CurrentUser);
                        QLUtility.UpdateAllCaches(CurrentUser); //for now just kill and machine caches

                        if (QLUtility.SetandCheckCache())
                        {
                            QLUtility.deleteApps(true); //purge all
                            Truncus.LogEvents(logLevel, "", CurrentUser.UserName, 0, "Central Purge");
                        }

                        //clear local log
                        QLUtility.ShrinkLogs();
                        this.Hide();

                        break;
                    case ImprivataEvent.WorkstationUnlock:

                        isLogon = false;

                        if (CurrentUser == null)
                        {
                            LoadTopIconTray();
                            KillListedProcesses();
                            QLUtility.KillIECookies(user.UserName);
                            DoLogin(user);
                        }
                        else
                        {
                            this.Show();
                            LoadTopIconTray();
                            showme();

                            isAuto = false; //reset for the next one
                            isLaunch = false;
                            swRoam.Reset();
                            swLaunch.Reset();

                            BuildUserContext();

                            if (!IsCitrixOpen())
                            {
                                StartRoamingProcess(); //old
                            }
                        }
                        break;
                    case ImprivataEvent.NonOneSignLogon:
                        isAuto = false; //reset for the next one
                        isLaunch = false;
                        swRoam.Reset();
                        swLaunch.Reset();
                        isLogon = false;
                        CloseServiceForm();
                        this.Hide();
                      //  ThreadPool.QueueUserWorkItem(q => QLUtility.checkQueue(user, imprivataEvent));
                        IAsyncResult rDisconnect = new AsyncMethodCaller(DisconnectCitrixSessionsIfNecessary).BeginInvoke(null, null); // Initiate the async call.                                                           
                        KillListedProcesses();
                        QLUtility.KillIECookies(CurrentUser.UserName);
                       // ToggleBHTHINDesktopIcon(true); //give them back icon
                        break;
                    case ImprivataEvent.Install:                        
                        UIHelper.DisplayOSBubble(niSystemAlert, "Quick Launch Auto Install", "An Update to Quick Launch has taken place. You may need to re-tap in, if Quick Launch doesn't show on its own.");                         
                       // DisconnectCitrixSessionsIfNecessary();
                        isAuto = false; //reset for the next one
                        isLaunch = false;
                        swRoam.Reset();
                        swLaunch.Reset();
                        isLogon = false;
                        CloseServiceForm();
                        Thread.Sleep(750);
                        break;
                    case ImprivataEvent.InstallComplete:
                        isLogon = false;
                        //UIHelper.DisplayOSBubble(niSystemAlert, "Quick Launch Updated", "Quick Launch has been updated to the latest version. Thank you for your patience"); //throws an error
                        CloseServiceForm();
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex) 
            {
                Program.LogException(ex, true);
                AutoClosingMessageBox.Show("Error. Quick Launch will close, please Tap Out and Tap In.", "Quick Launch Closing", 9000);
                try
                {

                    if (backgroundWorker.IsBusy)
                    {
                        backgroundWorker.CancelAsync();
                    }
                }
                catch (Exception ee)
                {
                    Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "ERROR: " + ee.Message);
                }

                Application.Exit();
                
            }
            finally { }   
        }

        private static void CloseServiceForm()
        {
            //close service form
            try
            {
                foreach (Form f in Application.OpenForms)
                {
                    if (f.GetType() == typeof(ServiceForm))
                    {
                        f.Close();
                    }
                }
            }
            catch (Exception ee)
            { }

            //close any dialog forms
            try
            {
                foreach (Form f in Application.OpenForms)
                {
                    if (f.GetType() == typeof(DialogForm))
                    {
                        f.Close();
                    }
                }
            }
            catch (Exception ee)
            { }
        }

        /// <summary>
        /// Resets QL for current user
        /// </summary>
        public void ResetConnection()
        {
            try
            {
                AsyncMethodCaller disconnectCaller = new AsyncMethodCaller(DisconnectCitrixSessionsIfNecessary);
                IAsyncResult resultDisconnect = disconnectCaller.BeginInvoke(null, null); // Initiate the async call. 
                QLUtility.deleteApps(true); //purge all

                try
                {
                    if (backgroundWorker.IsBusy)
                    {
                        backgroundWorker.CancelAsync();
                    }
                }
                catch (Exception ee)
                { }

                PrepareDesktop();
                KillListedProcesses();
                progressBar.Value = 20;

                progressBar.Value = 30;
                disconnectCaller.EndInvoke(resultDisconnect); //wait on disconnectCaller before we continue to login

                if (CurrentUser != null)
                {
                    QLUtility.KillIECookies(CurrentUser.UserName);
                    bool isLaunched = new Models.AppQueue().ClearLaunches(CurrentUser.UserName);
                }

                //QLUtility.ClearPrinters();
                DoLogin(globalUser, true); //need sessions disconnected before starting this. 
                pnlNotifier.Visible = false;

                isAuto = false; //reset for the next one
                isLaunch = false;
                swRoam.Reset();
                swLaunch.Reset();
            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
            }
            
        }

        /// <summary>
        /// Does the login.
        /// </summary>
        /// <param name="user">The user.</param>
        private void DoLogin(UserInfo user, bool isReset = false)
        {
            try
            {
                bool isNewLogin = true; //check current user and see if already logged on
                swLaunch.Reset();
                swRoam.Reset(); //just in case these are in a "runaway" state, reset them
                
                try
                {
                    string LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper();

                    if (LSID.Contains("BH\\"))
                    {
                        LSID = LSID.Substring(3);
                    }

                    if (user.UserName.ToUpper()== LSID)
                    {
                        //verify that it's TYPE2
                        EndpointInfoProviderClass endInfo = new EndpointInfoProviderClass();
                        int agentType = endInfo.GetAgentType;
                        if(agentType == 2) //2 is shared workstation, LSID
                        {
                            Truncus.insEventLog("USER AS LSID", user.UserName, System.Environment.MachineName, "", 0, "");
                        }
                    }
                }
                catch(Exception ee)
                {

                }

                try
                {
                    if(CurrentUser != null)
                    {
                        if(CurrentUser.UserName == user.UserName)
                        {
                            //check to see if cache has been cleared
                            isNewLogin = false; //same user, already logged in
                        }
                        else
                        {
                            isNewLogin = true;
                        }

                        if(CurrentUser.Password == user.Password)
                        {
                            isNewLogin = false; //same password, no change
                        }
                        else
                        {
                            isNewLogin = true; //new password, reload
                        }


                    }
                    else
                    {
                        isNewLogin = true;
                    }
                }
                catch(Exception ee)
                { }

                if ((isNewLogin) || (isReset))  //if new user or reset button is clicked
                {
                    //this.SwitchingUsers = true;

                    //timerNoConnection.Stop(); //just in case we are switching users    
                    isLaunch = false;

                    //if (!IsNetworkAvailable())
                    //{
                    //    bool isDown = showNetworkError();
                    //    if (CurrentUser != null)
                    //    {
                    //        CurrentUser = null; //set to null so on tap back in, DoLogin will occur
                    //    }

                    //    try
                    //    {
                    //        NetworkChange.NetworkAddressChanged += AddressChangedCallback;
                    //    }
                    //    catch (Exception ee) { }
                    //}
                    //else
                    //{
                        Stopwatch swatch = new Stopwatch();

                        swatch.Start();

                        EnumerateADGroups(ref user);

                        checkTime(swatch); //check elapsed time for excessive login

                        CurrentUser = user; //ensures we get groups, title, and dept

                        var rEventUserContext = new ManualResetEvent(false);
                        ThreadPool.QueueUserWorkItem(h => { BuildUserContext(); rEventUserContext.Set(); });

                        var rEventBookmarks = new ManualResetEvent(false);
                        ThreadPool.QueueUserWorkItem(h => { LoadBookmarks(); rEventBookmarks.Set(); });

                        checkTime(swatch); //check elapsed time for excessive login
                        progressBar.Value = 50;

                        if (ADHelper.IsInGroup(user.ADgroups, "SSO Physician Sharepoint"))
                        {
                            CurrentUser.UserType = "Physician";
                        }
                        else if (ADHelper.IsInGroup(user.ADgroups, "SSO Clinician Sharepoint"))
                        {
                            CurrentUser.UserType = "Clinician";
                        }
                        else if (ADHelper.IsInGroup(user.ADgroups, "SSO BHTHIN Sharepoint"))
                        {
                            CurrentUser.UserType = "BHTHIN";
                        }

                        HandlePortalOnLogin(); //runs faster on UI thread
                        rEventUserContext.WaitOne();

                        checkTime(swatch); //check elapsed time for excessive login
                        progressBar.Value = 60;

                        SetupMenu(); //has to be on this thread and after app access is determined   

                        checkTime(swatch); //check elapsed time for excessive login
                        progressBar.Value = 70;

                        BuildAppMenu(); //requires menu to be built first

                        checkTime(swatch); //check elapsed time for excessive login
                        progressBar.Value = 90;

                        rEventBookmarks.WaitOne();

                        timerLogin.Stop();

                        try
                        {
                            NetworkChange.NetworkAddressChanged += AddressChangedCallback;
                        }
                        catch (Exception ee) { }

                        //new
                        RoamingDelegate rd = StartRoamingProcess;

                        TSDisconnectParam param = new TSDisconnectParam { Seconds = 3, IsLogOff = false, ErrorMsg = "Disconnect Error Occurred", SuccessMsg = "Disconnect Complete. Attempting Roam...", ClearMsgCallback = rd };
                        StartTSDisconnect(param);
                        this.Refresh();
                   // }
                    showme();
                }
                else
                {
                    showme(); //already logged in, just show the bar
                }

                Models.Machine.insMachine(new Models.Machine
                {
                    PCName = System.Environment.MachineName,
                    LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper(),
                    QLVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    ICAVersion = Models.Machine.GetIcaVersion(),
                    IEVersion = Models.Machine.GetIEVersion(),
                    //ComputerInfo = sbComputer.ToString(),
                    //OSInfo = sbOS.ToString(),
                    Processes = Shared.SysHelper.GetSystemProcesses().ToString(),
                    //Memory = ram,
                    IpAddress = SysHelper.GetIpAddress(),
                    AutoInstallVersion = Models.Machine.GetAutoInstallVersion(),
                    LastLogin = DateTime.Now
                });
            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
            }

        }

        /// <summary>
        /// checks to see if login process has exceedeed 300000 milliseconds
        /// </summary>
        /// <param name="swatch"></param>
        private void checkTime(Stopwatch swatch)
        {
            try
            {
                if (swatch.ElapsedMilliseconds > 300000)
                {
                    swatch.Stop();
                    Truncus.LogEvents("1", "", CurrentUser.UserName, swatch.ElapsedMilliseconds, "LOGIN-TIMEOUT");
                    AutoClosingMessageBox.Show("Login timed out. Tap out and tap in again.", "Quick Launch Closing", 7000);

                    try
                    {

                        if (backgroundWorker.IsBusy)
                        {
                            backgroundWorker.CancelAsync();
                        }
                    }
                    catch (Exception ee)
                    {
                        Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "ERROR: " + ee.Message);
                    }

                    Application.Exit();
                }
            }
            catch(Exception ee)
            {
                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "ERROR: " + ee.Message);
            }
        }
             
        [Obsolete]   
        public void timerNoConnection_Tick(object sender, EventArgs e)
        {
            try
            { 
                UserInfo u = CurrentUser;
                if ((this.CurrentState == ImprivataEvent.UserLogon || this.CurrentState == ImprivataEvent.WorkstationUnlock) && ADHelper.GetGroups(ref u))
                    this.DoLogin((UserInfo)this.CurrentUser);
                else
                    LoadTopIconTray();
            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
            }
        }

        /// <summary>
        /// Shows QuickLaunch while it is preparing itself to keep load times low and responsive. 
        /// Espcially useful when previous user had a EMR session open.
        /// </summary>
        /// <returns></returns>
        private void PrepareDesktop()
        {

            try
            {
                toolStripUser.Enabled = false;
                toolStripUser.Text = "Loading...";

                this.lblNotifier.Text = "Preparing Desktop. Please Wait.";
                pnlNotifier.Visible = true;
                this.Refresh();
                this.Show();

                IntPtr lHwnd = UnsafeNativeMethods.FindWindow("Shell_TrayWnd", null); //minimizes all windows
                UnsafeNativeMethods.SendMessage(lHwnd, UnsafeNativeMethods.WM_COMMAND, (IntPtr)UnsafeNativeMethods.MIN_ALL, IntPtr.Zero);

                foreach (Process p in GetWFICAProcess()) //hides wfica windows while they are closing gracefully
                    UnsafeNativeMethods.ShowWindow(p.MainWindowHandle, 0);

                progressBar.Value = 5;
                LoadTopIconTray();
                progressBar.Value = 10;
                this.Refresh();
            }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
            }

        }
        
        /// <summary>
        /// Handles portal functions such as display, logging, and login. 
        /// </summary>
        /// <returns></returns>
        private void HandlePortalOnLogin()
        {
            try
            { 
                UpdateLastLogin();
                if (DisplayPortal())
                {
                    ShowPortal();
                    //CheckUserPwdExpiration(); //AD is not currently expiring passwords, so no need for this yet   
                }             
               // ThreadPool.QueueUserWorkItem(q => UpdateMachineInventory());
                }
            catch (Exception ex)
            {
                Program.LogException(ex, false);
            }
        }

        /// <summary>
        /// Checks AD to determine if a users' password is expired or is about to expire
        /// </summary>
        //private void CheckUserPwdExpiration()
        //{
        //    try
        //    {
        //        niPwdExpire.BalloonTipClicked += delegate { System.Diagnostics.Process.Start("https://pwreset/"); niPwdExpire.Visible = false; };
        //        int DaysTillExpire = ADHelper.DaysTillExpiration(CurrentUser);
        //        if (DaysTillExpire < 0)
        //            UIHelper.DisplayOSBubble(niPwdExpire, "Password Expired", "Your network password has expired, and will need to be reset. \n\nClick to reset it now...", 25000);
        //        else if (DaysTillExpire < 5)
        //            UIHelper.DisplayOSBubble(niPwdExpire, "Password Expiration Warning", string.Format("Your network password is going to expire in {0} day(s), unless you reset it. \n\nClick to reset it now...", DaysTillExpire.ToString()), 15000);
        //    }
        //    catch (Exception ex)
        //    {
        //        Program.LogException(ex, false);
        //    }
        //}

        /// <summary>
        /// Displays the portal.
        /// </summary>
        /// <returns></returns>
        private bool DisplayPortal()
        {
            try
            {
                if (CurrentUser.CurrentProfile != null)
                    if (CurrentUser.CurrentProfile.lastPortalLaunch < DateTime.Now.AddHours(-12))
                        return true;
            }
            catch (Exception ex) { Program.LogException(ex, false); }
            return false;
        }

        /// <summary>
        /// Shows the portal.
        /// </summary>
        private void ShowPortal()
        {
            try
            {
                CurrentUser.CurrentProfile.lastPortalLaunch = DateTime.Now;
                Models.UserProfile.updUserProfileLastPortalLaunch(CurrentUser.CurrentProfile);
            }
            catch (Exception ex) { Program.LogException(ex, false); }
            LaunchPortal_Click(null, null);
        }

        /// <summary>
        /// Updates the last login in the user profile only.
        /// </summary>
        private void UpdateLastLogin()
        {
            try
            {
              // TimeSpan tsLoginTime = DateTime.Now.Subtract(SetCurrentUserStartTime.ToString().Contains("0001") ? DateTime.Now : SetCurrentUserStartTime); //find out how long it took to login -- 0001 means the start time wasn't set

               try //to do move out out login sequence
               {
                   Stopwatch watch = new Stopwatch();
                   watch.Start();

                   CurrentUser.CurrentProfile = Models.UserProfile.selUserProfileById(CurrentUser.UserName);
                   //tsLoginTime.TotalMilliseconds
                   if (CurrentUser.CurrentProfile == null)
                   {
                       CurrentUser.CurrentProfile = new Models.UserProfile { 
                           ntID = CurrentUser.UserName, 
                           userType = CurrentUser.UserType, 
                           userTitle = CurrentUser.UserAdTitle, 
                           userDept = CurrentUser.UserAdDept, 
                           lastLoginLength = timerLogin.ElapsedMilliseconds, 
                           lastLogin = DateTime.Now, 
                           lastLoginMachine = System.Environment.MachineName 
                       };
                       Models.UserProfile.insUserProfile(CurrentUser.CurrentProfile);                
                   }
                   else
                   {
                       CurrentUser.CurrentProfile.userType = CurrentUser.UserType;
                       CurrentUser.CurrentProfile.userTitle = CurrentUser.UserAdTitle;
                       CurrentUser.CurrentProfile.userDept = CurrentUser.UserAdDept;
                    //   CurrentUser.CurrentProfile.lastLoginLength = tsLoginTime.TotalMilliseconds;
                       CurrentUser.CurrentProfile.lastLoginLength = timerLogin.ElapsedMilliseconds;
                       CurrentUser.CurrentProfile.lastLogin = DateTime.Now;
                       CurrentUser.CurrentProfile.lastLoginMachine = System.Environment.MachineName;
                       Models.UserProfile.updUserProfile(CurrentUser.CurrentProfile); //to do move out of login sequence      
                     //  CurrentUser.CurrentProfile.Roles = Models.UserProfile.selUserProfileRoles(CurrentUser.CurrentProfile.id);
                   }

                   watch.Stop();

                   if ((logLevel == "2") || (logLevel) == "3") //log userprofile timings
                   {
                        Truncus.LogEvents("1", "", CurrentUserId, watch.ElapsedMilliseconds, "Current User Profiling");
                   }

               }
               catch(Exception e)
               { }

            }
            catch (Exception ex) { Program.LogException(ex, false); }
            
        }

        /// <summary>
        /// Event that fires when a user updates their profile
        /// </summary>
        public void UpdateProfile_Changed()
        {
            try
            {
                CurrentUser.CurrentProfile = Models.UserProfile.selUserProfileById(CurrentUser.UserName);//get latest profile
                UIHelper.SetupQLForm(this, (CurrentUser.CurrentProfile.formVerticalStartup ?? "TOP"), (CurrentUser.CurrentProfile.formHorizontalStartup ?? "MID")); //implement new setting
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        #endregion

        #region Show/Hide/Close/Kill Functions

        /// <summary>
        /// Shows this form.
        /// </summary>
        private void showme()
        {
            try
            {
                toolStripUser.Text = CurrentUser.UserName;
                toolStripUser.Enabled = true;
                this.lblVersion.Text = "V" + Assembly.GetExecutingAssembly().GetName().Version;
                this.Show();
                this.Refresh();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Allows user profile to move form around the page without saving settings, or pulling new settings.
        /// </summary>
        public void PreviewMoveQLForm(string vertical, string horizontal)
        {
            try
            {
                UIHelper.SetupQLForm(this, vertical, horizontal); //implement new setting
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }


        /// <summary>
        /// Sends CTRL+Q.
        /// </summary>
        public void SendCTRLQ()
        {
            try
            {
                System.Type objBLType = System.Type.GetTypeFromProgID("WScript.Shell");
                object objBL = System.Activator.CreateInstance(objBLType);
                objBLType.InvokeMember("SendKeys", System.Reflection.BindingFlags.InvokeMethod, null, objBL, new object[] { "^q" });
            }
            catch (Exception) { }

         //   QLUtility.checkQueue(CurrentUser.UserName, "WorkstationLock");
        }


        /// <summary>
        /// Clears any messages on the notify panel after the specified time async
        /// </summary>
        /// <param name="seconds">Time to wait before hiding message</param>
        /// <param name="callback">Method to call after hiding message</param>
        private void ClearNotifyMessage(int seconds = 5, Delegate callback = null){
            try
            {
                var clearMessage = new BackgroundWorker();
                // clearMessage.WorkerSupportsCancellation = true;
                clearMessage.DoWork += new DoWorkEventHandler(clearMessage_DoWork);
                clearMessage.RunWorkerCompleted += new RunWorkerCompletedEventHandler(clearMessage_RunWorkerCompleted);
                ClearParam cp = new ClearParam { Seconds = seconds, Callback = callback };
                clearMessage.RunWorkerAsync(cp);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
 
        /// <summary>
        /// Waits for specified # of seconds async before issuing clear command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">Seconds to wait</param>
        void clearMessage_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                ClearParam cp = e.Argument as ClearParam;
                //int seconds = (int)e.Argument;            
                Thread.Sleep(cp.Seconds * 1000);
                if (cp.Callback != null)
                    this.Invoke(cp.Callback);
                e.Result = string.Format("Message cleared after {0} seconds", 0);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        /// <summary>
        /// Performs clear of notify panel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void clearMessage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                pnlNotifier.Visible = false;
                lblNotifier.Text = "";
                this.progressBar.Value = 0;
                this.Refresh();
            }
            catch(Exception ee)
            { 
                Program.LogException(new Exception("Issue clearing message in clearMessage_DoWork"), false); 
            }
        }

        /// <summary>
        /// Focus on a window, and bring it to the front
        /// </summary>
        private void FocusWindow(Process proc)
        {
            try
            {
                if (proc != null)
                {
                    UnsafeNativeMethods.ShowWindow(proc.MainWindowHandle, 9); //SW_RESTORE
                    UnsafeNativeMethods.SetForegroundWindow(proc.MainWindowHandle);
                }
            }
            catch (DllNotFoundException dlnf) { }
        }

        /// <summary>
        /// Prevents this form from displaying when it is initially run, allowing us to start Quick Launch without showing a UI.
        /// </summary>
        /// <param name="value">true to make the control visible; otherwise, false.</param>
        protected override void SetVisibleCore(bool value)
        {
            try
            {
                if (!this.IsHandleCreated)
                {
                    this.CreateHandle();
                    value = false;
                }
                base.SetVisibleCore(value);
            }
            catch (Exception ex) { Program.LogException(ex, false); SendCTRLQ(); Process.GetCurrentProcess().Kill(); } //restart ql. this happens when too many handles have been created. Often when QL hasn't been restarted in a while
        }

        /// <summary>
        /// Gets the create params. This is overriden to hide the close button and prevent ALT+F4 from closing the application.
        /// </summary>
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams param = base.CreateParams;
                param.ClassStyle = param.ClassStyle | 0x200;
                return param;
            }
        }

        /// <summary>
        /// Fired when the form closes. meant to hide OS bubble.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void QuickLaunch_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
              //  niSystemAlert.Dispose(); //so the icon doesnt stay in the tray
                niPwdExpire.Dispose();

                try
                {

                    if (backgroundWorker.IsBusy)
                    {
                        backgroundWorker.CancelAsync();
                    }
                }
                catch (Exception ee)
                {
                    Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "ERROR: " + ee.Message);
                }

                Application.Exit();
                //e.Cancel = true;      
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Form loses focus with a mouseout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void QuickLaunch_LostFocus(object sender, System.EventArgs e)
        {
            try
            {
                toolStripUser.DropDown.Close();
                if (this.WindowState == FormWindowState.Minimized)
                    this.WindowState = FormWindowState.Normal;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        #endregion              
                     
        #region Top Icon Tray

        private void LoadTopIconTray(){
            try
            {
                int imgSize = 15; 
                string AssemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                System.Windows.Forms.Padding margin = new System.Windows.Forms.Padding(1, 4, 5, 1);

                try
                {
                    ToolTip tHelp = UIHelper.SetupToolTip(new ToolTip());
                    tHelp.SetToolTip(pbIcon, "[v" + AssemblyVersion + "]");
                }
                catch(Exception ee)
                { }

              //  UIHelper.DisposeControls(pnlIconNotifications.Controls); //helps stop memory leaks
                try
                {
                    if (pnlIconNotifications.Controls != null)
                    {
                        pnlIconNotifications.Controls.Clear();
                    }
                }
                catch(Exception ee)
                {

                }


                //if (!DBManager.HasDBConnection())
                //{
                //    try
                //    {
                //        ToolTip tNoConn = UIHelper.SetupToolTip(new ToolTip());
                //        PictureBox pbNoConn = new PictureBox();
                //        pbNoConn.Image = global::QuickLaunch.Properties.Resources.alertRED;
                //        pbNoConn.Margin = margin;
                //        pbNoConn.Width = imgSize;
                //        pbNoConn.Height = imgSize;
                //        pbNoConn.SizeMode = PictureBoxSizeMode.Zoom;
                //        pbNoConn.Click += new EventHandler(delegate { MessageBox.Show("No Network Connection Available. Tap out and tap in.", "No Network Connection", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); });
                //        tNoConn.SetToolTip(pbNoConn, "No Network Connection Available");
                //        pnlIconNotifications.Controls.Add(pbNoConn);
                //    }
                //    catch (Exception ee)
                //    { }
                //}
                //else
                //{
                    margin = checkVersions(imgSize, AssemblyVersion, margin);
                // }

                try
                {
                    string debug = ConfigurationManager.AppSettings["IsDebug"];

                    if (!string.IsNullOrEmpty(debug))
                    {
                        if (debug == "1")
                        {
                            pbIcon.Image = this.pbIcon.Image = global::QuickLaunch.Properties.Resources.Squirrel_sm;
                        }
                    }
                }
                catch(Exception ee)
                {

                }
            }
            catch (Exception ex) { Program.LogException(ex, false); }
        }

        private System.Windows.Forms.Padding checkVersions(int imgSize, string AssemblyVersion, System.Windows.Forms.Padding margin)
        {
            try
            {

                //nobody uses it so turning off Help button for now

                //PictureBox pbHelp = new PictureBox();
                //pbHelp.Image = global::QuickLaunch.Properties.Resources.HelpPNG;
                //pbHelp.Margin = margin;
                //pbHelp.Width = imgSize;
                //pbHelp.Height = imgSize;
                //pbHelp.Cursor = Cursors.Hand;
                //pbHelp.SizeMode = PictureBoxSizeMode.Zoom;
                //pbHelp.Click += new EventHandler(LaunchHelp);

                //pnlIconNotifications.Controls.Add(pbHelp);

                bool HasICAInstalled = false;
                List<Models.Version_QL> versions = Models.Version_QL.selVersions(); 
                Models.Version_QL vIca = versions.Find(v => v.Code == "ICA");
                Version vIcaStd = new Version(vIca.LatestVersion);
                Version vIcaAccept1 = new Version(string.IsNullOrEmpty(vIca.AcceptableVersion1) ? "0.1.0" : vIca.AcceptableVersion1);
                Version vIcaAccept2 = new Version(string.IsNullOrEmpty(vIca.AcceptableVersion2) ? "0.1.0" : vIca.AcceptableVersion2);
                Version vIcaAccept3 = new Version(string.IsNullOrEmpty(vIca.AcceptableVersion3) ? "0.1.0" : vIca.AcceptableVersion3);

                string ver = Models.Machine.GetIcaVersion();
                if (ver.Contains("N/A"))
                {
                    ver = "0.0.0";
                }
                Version vIcaInstalled = new Version(ver);

                if (vIcaStd.CompareTo(vIcaInstalled) == 0) HasICAInstalled = true;
                if (vIcaAccept1.CompareTo(vIcaInstalled) == 0) HasICAInstalled = true;
                if (vIcaAccept2.CompareTo(vIcaInstalled) == 0) HasICAInstalled = true;
                if (vIcaAccept3.CompareTo(vIcaInstalled) == 0) HasICAInstalled = true;
                if (!HasICAInstalled)
                {
                    ToolTip tt = UIHelper.SetupToolTip(new ToolTip());
                    PictureBox pbIca = new PictureBox();
                    pbIca.Image = global::QuickLaunch.Properties.Resources.alertIYellow;
                    pbIca.Margin = margin;
                    pbIca.Width = imgSize;
                    pbIca.Height = imgSize;
                    pbIca.SizeMode = PictureBoxSizeMode.Zoom;
                    if (vIcaInstalled.ToString() == "0")
                        pbIca.Click += new EventHandler(delegate { MessageBox.Show("Unable to find an installed ICA client. Please contact the Service Desk and ask for the " + vIca.LatestVersion + " ICA client to be installed.", "Missing ICA Client", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); });
                    else
                        pbIca.Click += new EventHandler(delegate { MessageBox.Show(vIca.Message.Replace("{InstalledVersion}", vIcaInstalled.ToString()).Replace("{Version}", vIca.LatestVersion), "Invalid ICA Client", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); });
                    tt.SetToolTip(pbIca, "Invalid ICA Client");
                    pnlIconNotifications.Controls.Add(pbIca);
                }

                if (!SysHelper.IsVirtualMachine()) //virtual machine images are not subject to versioning controls
                {
                    bool HasQLInstalled = false;
                    Models.Version_QL vQL = versions.Find(v => v.Code == "QL"); 
                    Version vQLStd = new Version(vQL.LatestVersion);
                    Version vQLAccept1 = new Version(string.IsNullOrEmpty(vQL.AcceptableVersion1) ? "0.1.0" : vQL.AcceptableVersion1);
                    Version vQLAccept2 = new Version(string.IsNullOrEmpty(vQL.AcceptableVersion2) ? "0.1.0" : vQL.AcceptableVersion2);
                    Version vQLAccept3 = new Version(string.IsNullOrEmpty(vQL.AcceptableVersion3) ? "0.1.0" : vQL.AcceptableVersion3);
                    Version vQLInstalled = new Version(AssemblyVersion);
                    if (vQLStd.CompareTo(vQLInstalled) <= 0) HasQLInstalled = true;
                    if (vQLAccept1.CompareTo(vQLInstalled) == 0) HasQLInstalled = true;
                    if (vQLAccept2.CompareTo(vQLInstalled) == 0) HasQLInstalled = true;
                    if (vQLAccept3.CompareTo(vQLInstalled) == 0) HasQLInstalled = true;
                    if (!HasQLInstalled)
                    {
                        ToolTip tt = UIHelper.SetupToolTip(new ToolTip());
                        PictureBox pbQL = new PictureBox();
                        pbQL.Image = global::QuickLaunch.Properties.Resources.alertOrange;
                        pbQL.Margin = margin;
                        pbQL.Width = imgSize;
                        pbQL.Height = imgSize;
                        pbQL.SizeMode = PictureBoxSizeMode.Zoom;
                        pbQL.Click += new EventHandler(delegate
                        {
                            if (MessageBox.Show(vQL.Message.Replace("{InstalledVersion}", AssemblyVersion).Replace("{Version}", vQL.LatestVersion), "Out of Date Quick Launch", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == System.Windows.Forms.DialogResult.Yes)
                            {
                                try
                                {
                                    var svcController = new System.ServiceProcess.ServiceController("QLInstall", Environment.MachineName);
                                    if (svcController.Status == System.ServiceProcess.ServiceControllerStatus.Running)
                                    {
                                        SendCTRLQ();
                                        svcController.ExecuteCommand(131); //forces service to install
                                    }
                                }
                                catch (Exception ex) { Program.LogException(ex, false); }
                            }
                        });
                        tt.SetToolTip(pbQL, "Out of Date Quick Launch");
                        pnlIconNotifications.Controls.Add(pbQL);
                    }

                }

            }
            catch (Exception ee)
            { }
            return margin;
        }
        #endregion

        #region Main Menu
        /// <summary>
        /// Sets up the data driven menu
        /// </summary>
        private void SetupMenu()
        {
            try
            {
                AsyncMethodCaller callerLoadCitrixApps = new AsyncMethodCaller(Models.App.LoadCitrixApplications);

                IAsyncResult resultLoadCitrixApps = callerLoadCitrixApps.BeginInvoke(null, null); // Initiate the async call. 

                AsyncMethodCaller callerLoadMenuApps = new AsyncMethodCaller(Models.MenuApplication.LoadMenuApplications);
                IAsyncResult resultLoadMenuApps = callerLoadMenuApps.BeginInvoke(null, null); // Initiate the async call. 

                List<Models.MenuLink> ml;
                try
                {
                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    ml = Models.MenuLink.selMenuLinks();
                    sw.Stop();
                    try
                    {

                        if (string.IsNullOrEmpty(logLevel))
                        {
                            logLevel = "1";
                        }

                        if ((logLevel == "2") || (logLevel) == "3") //only log if more thorough diagnostics turned on
                        {
                            XMLEntry xmlEntry = new XMLEntry();
                            xmlEntry.timeStamp = DateTime.Now;
                            xmlEntry.eventType = "Select Menu Links";
                            xmlEntry.ntID = "";
                            xmlEntry.machine = System.Environment.MachineName;
                            xmlEntry.app = "";
                            xmlEntry.timeElapsed = sw.ElapsedMilliseconds.ToString();
                            Truncus.writeXMLEntry(xmlEntry);
                        }
                    }
                    catch (Exception ee) { }
                }
                catch (Exception ex) { ml = null; Program.LogException(ex, false); }
                if (ml == null)
                    ml = Models.MenuLink.DefaultMenu;

                ToolTip toolTip1 = UIHelper.SetupToolTip(new ToolTip());
                int index = 0;
                _MenuItems = new List<FishMenuItem>();

               // bool IsCitrixEnabled = true; //Models.SystemAlert.c(CurrentUser.UserName).Find(s => !s.IsQLCitrixEnabled) == null;
                callerLoadMenuApps.EndInvoke(resultLoadMenuApps); //wait on global list of apps for menu items
                this.Visible = false;

                UIHelper.DisposeControls(pnlFlowContainer.Controls); //helps stop memory leaks

                GC.Collect();

                pnlFlowContainer.Controls.Clear();

                foreach (Models.MenuLink m in ml)
                {
                    if (m.audience == "PHY" && !IsPhysician) continue; //skip link for non-physicians
                    if (m.audience == "CLI" && IsPhysician) continue; //skip link for physicians
                    if (!string.IsNullOrEmpty(m.adGroupFilter))
                    { //is there ad filter
                        if (m.adGroupFilter.Contains(","))
                        {
                            if (!ADHelper.IsInGroup(CurrentUser.ADgroups, m.adGroupFilter.Split(','))) //apply multi group ad filter
                                continue; //user is not in any of the groups specified
                        }
                        else if (!ADHelper.IsInGroup(CurrentUser.ADgroups, m.adGroupFilter)) //apply ad filter
                            continue; //user is not in specified group
                    }
                    Label lbl = new Label();
                    FishMenuItem pb = new FishMenuItem(m.id.ToString(), m.orderId, m.img);
                    pb.MouseMove += new MouseEventHandler(this.Picture_MouseMove);
                    pb.MouseLeave += new EventHandler(this.Picture_MouseLeave);
                    lbl.MouseMove += new MouseEventHandler(this.MenuLabel_MouseMove);
                    lbl.MouseLeave += new EventHandler(this.MenuLabel_MouseLeave);
                    string link = m.clickEvent.ToUpper();
                    switch (link)
                    {
                        case "EMR": //versions 2.1.4674.28767 and below need this in the database...JE                      
                        case "FN": //versions 2.1.4674.28767 and below need this in the database...JE                    
                        case "SN": //versions 2.1.4674.28767 and below need this in the database...JE
                        case "AS": //new for 2015  
                        case "AN": //new for 2016       
                                
                        case "CITRIX":
                            pb.apps.AddRange(Models.MenuApplication.MenuApplications.FindAll(ma => ma.MenuLink_Id == m.id));

                            foreach (Models.MenuApplication ma in pb.apps)
                            {
                                
                                try
                                {
                                    if (CurrentUser.ApplicationInfo.ContainsKey(ma.Value))
                                    {
                                        //if (!ma.Value.ToString().ToLower().Contains("obtv")) //do usual logic //SAVE FOR Q2
                                        //{
                                            pb.CurrentApplication = ma; //if left empty that means they have no access
                                            break; //give priority to first application found
                                        //}
                                        //else //check location
                                        //{
                                        //    string locName = string.Empty;

                                        //    //get location
                                        //    try
                                        //    {
                                        //        //check for VDI
                                        //        if (System.Environment.MachineName.ToLower().StartsWith("vclin"))
                                        //        {
                                        //            string IP = SysHelper.GetClientIP(System.Environment.MachineName);
                                        //            locName = Machine.FindLocation(IP, "");
                                        //        }
                                        //        else
                                        //        {
                                        //            string IP = SysHelper.GetIpAddress();
                                        //            locName = Machine.FindLocation(IP, "");
                                        //        }
                                               
                                        //        string debug = ConfigurationManager.AppSettings["IsDebug"];

                                        //        if (!string.IsNullOrEmpty(debug))
                                        //        {
                                        //            if (debug == "1")
                                        //            {
                                        //                locName = "Beaches";
                                        //            }
                                        //        }

                                        //        if (ma.Value.Contains(locName))
                                        //        {

                                        //            pb.CurrentApplication = ma; //if left empty that means they have no access
                                        //            break; //give priority to first application found
                                        //        }
                                        //    }
                                        //    catch(Exception ex)
                                        //    { }

                                        //}
                                    }
                                }
                                catch(Exception ee)
                                { }
                            }
                            pb.Visible = pb.CurrentApplication != null;
                            pb.Click += new System.EventHandler(this.Citrix_Click);
                            lbl.Click += new System.EventHandler(this.Citrix_Click);
                            CitrixDowntimeTimer.Stop();
                            break;
                        case "EMAIL":
                            pb.Click += new System.EventHandler(this.HttpLink_Click);
                            lbl.Click += new System.EventHandler(this.HttpLink_Click);
                            link = "http://webmail.bmcjax.com/";
                            break;
                        case "PORTAL":
                            pb.Click += new System.EventHandler(this.LaunchPortal_Click);
                            lbl.Click += new System.EventHandler(this.LaunchPortal_Click);
                            break;
                        case "INTRANET":
                            pb.Click += new System.EventHandler(this.HttpLink_Click);
                            lbl.Click += new System.EventHandler(this.HttpLink_Click);
                            link = "http://intranet";
                            break;
                        default: // GENERIC WEB LINK                        
                            pb.Click += new System.EventHandler(this.HttpLink_Click);
                            lbl.Click += new System.EventHandler(this.HttpLink_Click);
                            link = m.clickEvent;
                            break;
                    }

                    if (pb.Visible)
                    {
                        pb.link = link; //has updated link
                        _MenuItems.Add(pb);
                        toolTip1.SetToolTip(pb, m.toolTip);
                        Panel pnl = new Panel();
                        pnl.Width = 60;
                        pnl.Height = 60;
                        pnl.Tag = m.id.ToString();
                        
                        if (this.InvokeRequired)
                        {
                            this.BeginInvoke((MethodInvoker)delegate()
                            {
                                pnlFlowContainer.Controls.Add(pnl);
                                pnl.Controls.Add(FishMenuItem.SetupItemLabelOverlay(lbl, m.name, new Point(-5, 46)));
                                pnl.Controls.Add(pb);
                                index++;
                            });
                        }
                        else
                        {
                            pnlFlowContainer.Controls.Add(pnl);
                            pnl.Controls.Add(FishMenuItem.SetupItemLabelOverlay(lbl, m.name, new Point(-5, 46)));
                            pnl.Controls.Add(pb);
                            index++;
                        }
                    }
                }
                try
                {
                    UIHelper.SetupQLForm(this, (CurrentUser.CurrentProfile.formVerticalStartup ?? "TOP"), (CurrentUser.CurrentProfile.formHorizontalStartup ?? "MID"));
                }
                catch { } //sometimes CurrentProfile is null, do try catch just in case anything else fails
                if (_MenuItems.Count < 5)
                {
                    lblTitle.Text = "BH QLaunch";
                   // pnlIconNotifications.Location = new Point(58, 2);
                }
                else if (_MenuItems.Count < 7)
                {
                    lblTitle.Text = "BH Quick Launch";
                 //   pnlIconNotifications.Location = new Point(115, 2);
                }
                else
                {
                    lblTitle.Text = "Baptist Health Quick Launch";
                 //   pnlIconNotifications.Location = new Point(179, 2);
                }
                this.Visible = true;
                callerLoadCitrixApps.EndInvoke(resultLoadCitrixApps);
                //linkLabel1.Left = (this.ClientSize.Width - linkLabel1.Width) / 2;
                pnlIconNotifications.Left = (this.ClientSize.Width - pnlIconNotifications.Width);

                this.Visible = true;
            }
            catch (Exception ee)
            {
                Program.LogException(ee, false); 
            }
        }

        void CitrixDowntimeTimer_Tick(object sender, EventArgs e)
        {
            //if (Models.SystemAlert.selActiveSystemAlerts(CurrentUser.UserName).Find(s => !s.IsQLCitrixEnabled) == null)
                SetupMenu();
        }

        //DateTime dtLaunchEMRTime;
        /// <summary>
        /// Handles the Click event of a Citrix menu item
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Citrix_Click(object sender, EventArgs e)
        {
            //FishMenuItem i = sender as FishMenuItem;
            Control c = sender as Control;
            FishMenuItem i = _MenuItems.Find(fmi => fmi.Name == c.Parent.Tag.ToString());
            if (i.CurrentApplication != null)
            {
                CurrentLaunchApplication = i.CurrentApplication;
                Process procCitrix = FindWFICAProcessByWindowTitle(i.CurrentApplication.WindowTitle);
                if (procCitrix == null)
                {
                   // Debug.WriteLine(i.CurrentApplication.Value + " : Citrix_Click");
                   // dtLaunchEMRTime = DateTime.Now;
                   // startLaunchStopwatch();
                    if(!swLaunch.IsRunning)
                    {
                        swLaunch.Reset();
                        swLaunch.Start();
                    }

                    bool isLaunched = new Models.AppQueue().LogQueueEvent(CurrentUser, "LAUNCH-START", i.CurrentApplication.Value); //check for other instances within 8 min window, record start of launch event
                    if (!isLaunched)
                    {
                        LaunchApplication(i.CurrentApplication);
                        try
                        {
                            //to do modify table to allow AllScripts statistics
                            Models.UserProfile.updUserProfileRoamingMetrics(CurrentUserId, PowerChart: (i.link == "EMR" || i.link == "PC"), FirstNet: (i.link == "FN"), SurgiNet: (i.link == "SN"));                        
                        }
                        catch { } //Database not available
                    }
                    else
                    {
                        swLaunch.Reset();
                        EnforceMsg("Application is busy, please wait...", "Quick Launch", 12000);
                       // AutoClosingMessageBox.Show("Application is loading, please wait...", "Quick Launch", 9000);
                       // QLUtility.SuppressIE(9000);
                    }
                }
                else if (procCitrix != null)
                {
                    swLaunch.Reset();
                    FocusWindow(procCitrix);
                }
            }
            else { MessageBox.Show("You are not authorized to access this application", "Not Authorized", MessageBoxButtons.OK, MessageBoxIcon.Exclamation); }        
        }

        private void EnforceMsg(string msg, string title, long timer)
        {

            AutoClosingForm.Show(msg, title, 9000);
        }

        /// <summary>
        /// Handles the Click event of the pnlLinkTop control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void LaunchPortal_Click(object sender, EventArgs e)
        {
            if (IsPhysician)
            {
                System.Diagnostics.Process.Start("http://spphysicians2010.e-baptisthealth.com/_layouts/tito/tito.aspx");
            }
            else
            {
                System.Diagnostics.Process.Start("http://authhome.e-baptisthealth.com/pcs/Pages/PFCSLogin.aspx");
            }
        }

        /// <summary>
        /// Handles the Click event of Http Links
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void HttpLink_Click(object sender, EventArgs e)
        {
            //FishMenuItem i = sender as FishMenuItem;
            Control c = sender as Control;
            FishMenuItem i = _MenuItems.Find(fmi => fmi.Name == c.Parent.Tag.ToString());

            if (!string.IsNullOrEmpty(i.link))
            {
                if (i.link == "http://webmail.bmcjax.com/") //reset
                {
                    ResetEmail();
                }

                System.Diagnostics.Process.Start(i.link);
            }
            else
            {
                MessageBox.Show("Unable to open link");
            }
        }

        private void ResetEmail()
        {
            foreach (var pnl in this.pnlFlowContainer.Controls.OfType<Panel>())
            {
                foreach (var lbl in pnl.Controls.OfType<Label>())
                {
                    if (lbl.Text.ToLower().Contains("email"))
                    {
                        lbl.Text = "Email";
                        lbl.Font = new System.Drawing.Font(lbl.Font.FontFamily, lbl.Font.Size, System.Drawing.FontStyle.Regular);
                        break;
                    }

                }

                foreach (var fish in pnl.Controls.OfType<FishMenuItem>())
                {
                    if (fish.link != null)
                    {
                        if (fish.link == "http://webmail.bmcjax.com/")
                        {
                            System.Drawing.Bitmap bitMap = global::QuickLaunch.Properties.Resources.email;
                            fish.Image = bitMap;
                            break;

                        }
                    }
                }
            }
        }

        /// <summary>
        /// Handles the Picture_MouseLeave event of the Menu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Picture_MouseLeave(object sender, EventArgs e)
        {
            FishMenuItem fmi = sender as FishMenuItem;            
            //fmi.Width = 60;
            //fmi.Height = 60;
            fmi.Width = 38;
            fmi.Height = 38;
            fmi.Location = new Point(11, 5); //location;  
            fmi.Parent.Controls[0].ForeColor = Color.SteelBlue;            
        }  

        /// <summary>
        /// Handles the Picture_MouseHover event of the Menu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Picture_MouseMove(object sender, MouseEventArgs e)
        {
            FishMenuItem fmi = sender as FishMenuItem;
            //fmi.Width = 73;
            //fmi.Height = 73;
            fmi.Width = 48;
            fmi.Height = 48;
            fmi.Location = new Point(6, 0);
            fmi.Parent.Controls[0].ForeColor = Color.Red;            
        }

        /// <summary>
        /// Handles the MenuLabel_MouseLeave event of the Menu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MenuLabel_MouseLeave(object sender, EventArgs e)
        {
            Control c = sender as Control;
            FishMenuItem i = _MenuItems.Find(fmi => fmi.Name == c.Parent.Tag.ToString());
            //i.Width = 60;
            //i.Height = 60;
            //i.Location = new Point(0, 0);
            i.Width = 38;
            i.Height = 38;
            i.Location = new Point(11, 5); 
            
            c.ForeColor = Color.SteelBlue;
        }

        /// <summary>
        /// Handles the MenuLabel_MouseHover event of the Menu control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void MenuLabel_MouseMove(object sender, MouseEventArgs e)
        {
            Control c = sender as Control;
            FishMenuItem i = _MenuItems.Find(fmi => fmi.Name == c.Parent.Tag.ToString());
            //i.Width = 73;
            //i.Height = 73;
            //i.Location = new Point(-5, -5);  
            i.Width = 48;
            i.Height = 48;
            i.Location = new Point(6, 0);   
            c.ForeColor = Color.Red;
        }
            

        #endregion

        #region URL Bookmarks
        void myFavsMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                foreach (Form f in this.OwnedForms)
                    if (f != null)
                        if (!f.IsDisposed)
                            if (f.GetType() == typeof(Bookmarks))
                            {
                                f.Activate(); //show currently open form
                                return; //no need to continue
                            }

                Bookmarks b = new Bookmarks();
                b.Owner = this;
                b.Show();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        public void LoadBookmarks()
        {
            Stopwatch watch = new Stopwatch();
            watch.Start();

            try
            {
                foreach (ToolStripMenuItem x in myFavsMenuItem.DropDownItems)
                    x.Dispose();
            }
            catch { }

            try
            {
                myFavsMenuItem.DropDownItems.Clear();
                List<Models.Bookmark> favs = Models.Bookmark.selBookmarkByUserId(CurrentUserId);

                foreach (Models.Bookmark f in favs)
                {
                    ToolStripMenuItem tmi = new ToolStripMenuItem(f.Title);
                    tmi = UIHelper.SetupToolStripMenuItem(tmi);
                    tmi.Tag = f.Url;
                    tmi.ToolTipText = f.Url;
                    tmi.Click += new EventHandler(tmiBookmark_Click);
                    if (DateTime.Now.Subtract(f.add_dt) <= TimeSpan.FromMinutes(3)) tmi.BackColor = Color.Beige; //highlight new items
                    myFavsMenuItem.DropDownItems.Add(tmi);
                }

                try
                {
                    foreach (ToolStripMenuItem tmi in myFavsMenuItem.DropDownItems)
                    {
                        if (tmi != null)
                        {
                            int idx = myFavsMenuItem.DropDownItems.IndexOf(tmi);

                            if (idx % 2 != 0)
                            {
                                tmi.BackColor = Color.WhiteSmoke;
                            }
                            else
                            {
                                tmi.BackColor = Color.Gainsboro;
                            }
                        }
                    }
                }
                catch (Exception ee)
                { }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            watch.Stop();

            try
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, watch.ElapsedMilliseconds, "Load Bookmarks"); //just write it local
            }
            catch(Exception ee){}
        }

        public void tmiBookmark_Click(object sender, EventArgs e)
        {
            ToolStripDropDownItem dd = sender as ToolStripDropDownItem;
            try
            {
                System.Diagnostics.Process.Start(dd.Tag.ToString());
            }
            catch
            {
                MessageBox.Show("Unable to open link");
            }
        }

        private bool IsOverBookmarkLimit()
        {

                return myFavsMenuItem.DropDownItems.Count >= 20;

        }

        void bookmark_DragEnter(object sender, DragEventArgs e)
        {
            try
            {
                if (IsOverBookmarkLimit())
                { pnlFooter.BackColor = Color.Salmon; pnlNotifier.Visible = true; lblNotifier.Text = string.Format("You may only have up to {0} bookmarks", Bookmarks.BookmarkLimit); }
                else
                {
                    e.Effect = DragDropEffects.All;
                    pnlFooter.BackColor = Color.Green;
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        void bookmark_DragLeave(object sender, EventArgs e)
        {
            try
            {
                pnlFooter.BackColor = Color.Transparent;
                if (pnlNotifier.Visible)
                    ClearNotifyMessage(4);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        void bookmark_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                pnlNotifier.Visible = true;
                if (IsOverBookmarkLimit()) { pnlFooter.BackColor = Color.Salmon; lblNotifier.Text = string.Format("You may only have up to {0} bookmarks", Bookmarks.BookmarkLimit); }
                lblNotifier.Text = "Adding Bookmark...Please wait.";
                this.Refresh();
                try
                {
                    string url = Shared.SysHelper.GetDraggedUrl(e);

                    if (!string.IsNullOrEmpty(url))
                    {
                        Models.Bookmark.insBookmark(new Models.Bookmark { Title = Shared.SysHelper.GetUrlTitle(url), Url = url, NtId = CurrentUserId, OrderId = myFavsMenuItem.DropDownItems.Count + 1, IsArchived = false });
                        LoadBookmarks();
                        lblNotifier.Text = "Bookmark has been added!";
                        progressBar.Value = 100;
                    }
                    else
                        lblNotifier.Text = "You have dropped an unsupported file type. Please try dragging url text, or the icon from the browser url.";
                    ClearNotifyMessage(3);
                }
                catch (Exception ex) { pnlNotifier.Visible = false; MessageBox.Show("There was an error adding your bookmark. Please try again later."); Program.LogException(ex, false); }
                pnlFooter.BackColor = Color.Transparent;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        #endregion

        #region User Menu Apps
        private void BuildAppMenu()
        {
            try
            {
                tsMyApps.DropDownItems.Clear();
                foreach (KeyValuePair<String, String> entry in CurrentUser.ApplicationInfo)
                {
                    if (_MenuItems.Find(m => m.apps.Find(a => a.Value == entry.Key) != null) == null)
                    {
                        ToolStripMenuItem tmi = new ToolStripMenuItem(entry.Key);
                        tmi.ToolTipText = entry.Value;
                        tmi.Click += new EventHandler(tmiApp_Click);
                        tmi.Tag = Models.App.CitrixApplications.Find(c => c.Value == entry.Key);
                        tmi.Image = FishMenuItem.GetImage(tmi.Tag != null ? (tmi.Tag as Models.App).DefaultImg : entry.Key, "");
                        tsMyApps.DropDownItems.Add(UIHelper.SetupToolStripMenuItem(tmi));
                    }
                }

                try
                {
                    foreach(ToolStripMenuItem tmi in tsMyApps.DropDownItems)
                    {
                        if(tmi != null)
                        {
                            int idx = tsMyApps.DropDownItems.IndexOf(tmi);

                            if(idx % 2 != 0)
                            {
                                tmi.BackColor = Color.WhiteSmoke;
                            }
                            else
                            {
                                tmi.BackColor = Color.Gainsboro;
                            }
                        }
                    }
                }
                catch (Exception ee)
                { }
            }
            catch (Exception ex) { Program.LogException(ex, false); }
            tsMyApps.Visible = tsMyApps.DropDownItems.Count > 0;
        }

        /// <summary>
        /// click event of application list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void tmiApp_Click(object sender, EventArgs e)
        {
            ToolStripDropDownItem dd = sender as ToolStripDropDownItem;
            try
            {
                Models.App app = dd.Tag as Models.App;
                if (app == null)
                    app = new Models.App { Title = dd.Text, Value = dd.Text, WindowTitle = dd.Text, id = 0, AllowRoaming = false, DefaultImg = "citrix" };
                Process procCitrix = FindWFICAProcessByWindowTitle(app.WindowTitle);
                if (procCitrix == null)
                {
                   // dtLaunchEMRTime = DateTime.Now;

                    if(CurrentLaunchApplication == null)
                    {
                        CurrentLaunchApplication = new MenuApplication();
                        CurrentLaunchApplication.Value = app.Value;
                        CurrentLaunchApplication.MenuLink_Id = app.id;
                        CurrentLaunchApplication.WindowTitle = app.WindowTitle;
                    }
                    else
                    {
                        CurrentLaunchApplication.Value = app.Value;
                        CurrentLaunchApplication.MenuLink_Id = app.id;
                        CurrentLaunchApplication.WindowTitle = app.WindowTitle;
                    }

                   // startLaunchStopwatch();
                    if (!swLaunch.IsRunning)
                    {
                        swLaunch.Reset();
                        swLaunch.Start();
                    }
                    bool isLaunched = new Models.AppQueue().LogQueueEvent(CurrentUser, "LAUNCH-START", app.Value); //check for other instances within 8 min window, record start of launch event
                    if (!isLaunched)
                    {
                        LaunchApplication(app);
                    }
                    else
                    {
                        swLaunch.Reset();
                        EnforceMsg("Application is busy, please wait...", "Quick Launch", 12000);
                       // AutoClosingMessageBox.Show("Application is loading, please wait...", "Quick Launch", 9000);
                    }
                }
                else
                {                   
                    FocusWindow(procCitrix);
                }
            }
            catch
            {
                MessageBox.Show("Unable to open Citrix app");
            }
        }
        #endregion
       
        #region Link Label Events

        /// <summary>
        /// Handles the Click event of the lblTapOut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void lblTapOut_Click(object sender, EventArgs e)
        {
            try
            {
                KeyboardHelper.Ctrl_Q();
                Thread.Sleep(200);
                KeyboardHelper.Ctrl_Q();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Handles the MouseLeave event of the lblTapOut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void lblTapOut_MouseLeave(object sender, EventArgs e)
        {
            try
            {
                lblTapOut.ForeColor = Color.White;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Handles the MouseEnter event of the lblTapOut control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        void lblTapOut_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                lblTapOut.ForeColor = Color.YellowGreen;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Handles the click event of the menu username my profile which displays the profile page
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        private void myProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                UserProfile uProfile = new UserProfile(_MenuItems.FindAll(p => (p.link == "PC" || p.link == "EMR") && p.Visible).Count > 0
                                                     , _MenuItems.FindAll(p => (p.link == "FN") && p.Visible).Count > 0
                                                     , _MenuItems.FindAll(p => (p.link == "SN") && p.Visible).Count > 0
                                                    , _MenuItems.FindAll(p => (p.link == "CITRIX") && (p.apps[0].Title.ToLower().Contains("touchworks")) && p.Visible).Count > 0
                                                    , _MenuItems.FindAll(p => (p.link == "CITRIX") && (p.apps[0].Title.ToLower().Contains("anesthesia")) && p.Visible).Count > 0
                                                    );
                uProfile.Owner = this;
                if (!uProfile.IsDisposed)
                    uProfile.Show();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void menuStripUser_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            try
            {
                toolStripUser.ShowDropDown();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void myProfileToolStripMenuItem_MouseEnter(object sender, EventArgs e)
        {
            try
            {
                toolStripUser.ShowDropDown();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }        
        }
        
        /// <summary>
        /// Handles the click event of the help icon and launches the help form
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        //Help help;
        //private void LaunchHelp(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        OpenHelp();
        //    }
        //    catch (Exception ee)
        //    {
        //        Truncus.LogException(ee, true, "LaunchHelp");
        //    }  
        //}

        //public void OpenHelp(string NavigateTo = "0")
        //{
        //    try
        //    {
        //        Help help = new Help(NavigateTo);
        //        help.Owner = this;
        //        if (!help.IsDisposed)
        //            help.Show();
        //    }
        //    catch (Exception ee)
        //    {
        //        Truncus.LogException(ee, true, "OpenHelp");
        //    }  
        //}
        private void lblVersion_Click(object sender, EventArgs e)
        {
            try
            {
                QLDebug debug = new QLDebug();
                debug.Owner = this;
                if (!debug.IsDisposed)
                    debug.Show();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }  
        }
        #endregion

        /// <summary>
        /// Find Window Title by Process
        /// </summary>
        private string FindWindowTitleByProcess(Process proc)
        {
            try
            {
                if (proc != null)
                {
                    return proc.MainWindowTitle;
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            return string.Empty;
        }

        /// <summary>
        /// Find Process By Window Title
        /// </summary>
        private Process FindWFICAProcessByWindowTitle(string title)
        {
            try
            {
                Process[] procs = GetWFICAProcess();
                foreach (Process p in procs)
                {
                    if (FindWindowTitleByProcess(p).Contains(title))
                    {
                        return p;
                    }

                    if (title.ToLower().Contains("powerchart")) //also check HNAM
                    {
                        if (FindWindowTitleByProcess(p).Contains("HNAM"))
                        {
                            return p;
                        }
                    }
                }
              
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            return null;
        }

        /// <summary>
        /// Find Process By Window Title, return bool
        /// </summary>
        private bool FindByWindowTitle(string title)
        {
            try
            {
                Process[] procs = GetWFICAProcess();
                foreach (Process p in procs)
                {
                    if (FindWindowTitleByProcess(p).Contains(title))
                    {
                        return true;
                    }

                    if (title.ToLower().Contains("powerchart")) //also check HNAM
                    {
                        if (FindWindowTitleByProcess(p).Contains("HNAM"))
                        {
                            return true;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            return false;
        }
        
        private void resetConnectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ResetConnection();
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            KillListedProcesses();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ServiceForm helpForm = new ServiceForm(CurrentUser.UserName);
            helpForm.ShowDialog();
        }

        private void pbIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            QLUtility.deleteApps(true); //purge all
            Models.UserFarm.clearUserList(CurrentUser.UserName);
            

            try
            {

                bool isClear = new Models.AppQueue().ClearLaunches(CurrentUser.UserName); //reset queue table

                if (backgroundWorker.IsBusy)
                {
                    backgroundWorker.CancelAsync();
                }
            }
            catch(Exception ee)
            {
                Truncus.LogEvents("1", System.Reflection.MethodInfo.GetCurrentMethod().Name, "", 0, "ERROR: " + ee.Message);
            }

            QLUtility.dragonCleaner();

            //Application.Exit();
            Truncus.insEventLog("CACHE-CLEAR", CurrentUserId, Environment.MachineName, "", 0, "");
            Environment.Exit(0); //straight termination
            
        }

        void AddressChangedCallback(object sender, EventArgs e)
        {
            try
            {
                //bool isNetworkup = true;

                //NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                //for (int i = 0; i < adapters.Length - 1; i++)
                //{
                //    if (adapters[i].OperationalStatus != adaptersStart[i].OperationalStatus)
                //    {
                //        isNetworkup = false;
                //        //break;
                //    }

                //}

                Thread.Sleep(5000);

                if (!IsNetworkAvailable()) //using this instead of NetworkInterface.GetIsNetworkAvailable() so I don't have to check all NICs
                {
                    showNetworkError();
                }
                else
                {
                    clearNetworkErrors();  //don't do anything just sit there
                }

            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void clearNetworkErrors()
        {
            try
            {
                if (CurrentUser == null)
                {
                    string msg = "Please tap out and tap in again.";
                    if (this.InvokeRequired)
                    {
                        this.BeginInvoke((MethodInvoker)delegate()
                        {
                            pnlIconNotifications.Controls.Clear();
                            lblNotifier.Text = msg;
                            progressBar.Visible = true;
                            progressBar.Value = 0;
                            pnlNotifier.Visible = true;
                            lblNotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11);
                            this.Refresh();
                            //pnlNotifier.Visible = false;
                            //   progressBar.Visible = true;
                            //   pnlNotifier.Visible = false;
                        });
                    }
                    else
                    {
                        pnlIconNotifications.Controls.Clear();
                        lblNotifier.Text = msg;
                        progressBar.Visible = true;
                        progressBar.Value = 0;
                        pnlNotifier.Visible = true;
                        lblNotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11);
                        this.Refresh();
                      //  pnlNotifier.Visible = false;
                        //   progressBar.Visible = true;
                        //   pnlNotifier.Visible = false;
                    }
                }
                else
                {
                    if (this.InvokeRequired)
                    {
                        this.BeginInvoke((MethodInvoker)delegate()
                        {
                            pnlIconNotifications.Controls.Clear();
                            //lblNotifier.Text = "Network reconnected. QL will close and lock PC.";
                            progressBar.Visible = true;
                            progressBar.Value = 0;
                            pnlNotifier.Visible = true;
                            lblNotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11);
                            this.Refresh();
                            pnlNotifier.Visible = false;
                            //   progressBar.Visible = true;
                            //   pnlNotifier.Visible = false;
                        });
                    }
                    else
                    {
                        //lblNotifier.Text = "Network reconnected. QL will close and lock PC.";
                        pnlIconNotifications.Controls.Clear();
                        //lblNotifier.Text = "Network reconnected. QL will close and lock PC.";
                        progressBar.Visible = true;
                        progressBar.Value = 0;
                        pnlNotifier.Visible = true;
                        lblNotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11);
                        this.Refresh();
                        pnlNotifier.Visible = false;
                        //   progressBar.Visible = true;
                        //   pnlNotifier.Visible = false;
                    }
                }

                Truncus.LogEvents("2", "", "", 0, "NETWORK RECONNECTED");
                
                //lock machine
                //KeyboardHelper.Ctrl_Q();
                //Thread.Sleep(200);
                //KeyboardHelper.Ctrl_Q();

                //auto reconnect fn, don't delete
               // LoadNotifier();
            }
            catch (Exception ee)
            {

            }
        }

        /// <summary>
        /// callback to Notifier to kill QL and reset on ICA errors
        /// </summary>
        private void LoadNotifier()
        {

                string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "Notifier.exe");
                if (File.Exists(path))
                {
                    Process p = Process.Start(path, "UserLogon " + CurrentUser.UserName + " " + CurrentUser.Password + " bh");
                    p.WaitForInputIdle();
                }
                else if (File.Exists(@"..\..\Notifier\bin\Notifier.exe"))
                {
                    Process p = Process.Start(@"..\..\Notifier\bin\Notifier.exe", "UserLogon " + CurrentUser.UserName + " " + CurrentUser.Password + " bh");
                    p.WaitForInputIdle();
                }
                else
                {
                    AutoClosingMessageBox.Show("Path to Notifier not found. Quick Launch will close, please Tap Out and Tap In.", "Quick Launch Closing", 9000);
                    // throw new ApplicationException("Path to Notifier not found '" + path + "'");
                }

        }

        private bool showNetworkError()
        {
            string msg = "Connection lost. If not reconnected in 2 minutes, call Service Desk.";

            if (CurrentUser == null)
            {
                msg = "No connection. Tap out and tap back in. ";
            }
            else
            {
                msg = "Connection lost. If not reconnected in 2 minutes, call Service Desk.";
            }

            try
            {
                if (this.InvokeRequired)
                {
                    this.BeginInvoke((MethodInvoker)delegate()
                    {
                        int imgSize = 15;
                        System.Windows.Forms.Padding margin = new System.Windows.Forms.Padding(1, 4, 5, 1);
                        PictureBox pbNoConn = new PictureBox();
                        pbNoConn.Image = global::QuickLaunch.Properties.Resources.alertRED;
                        pbNoConn.Margin = margin;
                        pbNoConn.Width = imgSize;
                        pbNoConn.Height = imgSize;
                        pbNoConn.SizeMode = PictureBoxSizeMode.Zoom;
                        pbNoConn.Visible = true;
                        pnlIconNotifications.Controls.Add(pbNoConn);
                        pnlIconNotifications.Visible = true;
                        pnlNotifier.Visible = true;
                        lblNotifier.Text = msg;
                        lblNotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11);
                        progressBar.Visible = false;
                        progressBar.Value = 0;
                        pnlNotifier.Visible = true;
                        //  timerNoConnection.Start();
                        Truncus.LogEvents("1", "", "", 0, "NO NETWORK CONNECTION");
                        this.Refresh();
                    });
                }
                else
                {
                    int imgSize = 15;
                    System.Windows.Forms.Padding margin = new System.Windows.Forms.Padding(1, 4, 5, 1);
                    PictureBox pbNoConn = new PictureBox();
                    pbNoConn.Image = global::QuickLaunch.Properties.Resources.alertRED;
                    pbNoConn.Margin = margin;
                    pbNoConn.Width = imgSize;
                    pbNoConn.Height = imgSize;
                    pbNoConn.SizeMode = PictureBoxSizeMode.Zoom;
                    pbNoConn.Visible = true;
                    pnlIconNotifications.Controls.Add(pbNoConn);
                    pnlIconNotifications.Visible = true;
                    pnlNotifier.Visible = true;
                    lblNotifier.Text = msg;
                    lblNotifier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11);
                    progressBar.Visible = false;
                    progressBar.Value = 0;
                    pnlNotifier.Visible = true;
                    //timerNoConnection.Start();
                    Truncus.LogEvents("1", "", "", 0, "NO NETWORK CONNECTION");
                    this.Refresh();
                }
 
            }
            catch (Exception ee) 
            {
                return true;
            }

            return true;
        }

        /// <summary>
        /// Indicates whether any network connection is available
        /// Filter connections below a specified speed, as well as virtual network cards.
        /// </summary>
        /// <returns>
        ///     <c>true</c> if a network connection is available; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNetworkAvailable()
        {
            return IsNetworkAvailable(0);
        }

        /// <summary>
        /// Indicates whether any network connection is available.
        /// Filter connections below a specified speed, as well as virtual network cards.
        /// </summary>
        /// <param name="minimumSpeed">The minimum speed required. Passing 0 will not filter connection using speed.</param>
        /// <returns>
        ///     <c>true</c> if a network connection is available; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsNetworkAvailable(long minimumSpeed)
        {
            if (!NetworkInterface.GetIsNetworkAvailable())
                return false;

            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                // discard because of standard reasons
                if ((ni.OperationalStatus != OperationalStatus.Up) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Loopback) ||
                    (ni.NetworkInterfaceType == NetworkInterfaceType.Tunnel))
                    continue;

                // this allow to filter modems, serial, etc.
                // I use 10000000 as a minimum speed for most cases
                if (ni.Speed < minimumSpeed)
                    continue;

                // discard virtual cards (virtual box, virtual pc, etc.)
                if ((ni.Description.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0) ||
                    (ni.Name.IndexOf("virtual", StringComparison.OrdinalIgnoreCase) >= 0))
                    continue;

                // discard "Microsoft Loopback Adapter", it will not show as NetworkInterfaceType.Loopback but as Ethernet Card.
                if (ni.Description.Equals("Microsoft Loopback Adapter", StringComparison.OrdinalIgnoreCase))
                    continue;

                return true;
            }
            return false;
        }

        private void menuStripUser_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                toolStripUser.ShowDropDown();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void menuStripUser_MouseHover(object sender, EventArgs e)
        {
            try
            {
                toolStripUser.ShowDropDown();
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
    }
}
