﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MetroFramework.Forms;
namespace QuickLaunch
{
    public partial class Help : Form
    {
        
        private string CssStyle = @"<style type='text/css'> 
                                        body{font-family:font-family: Segoe UI; font-size:11pt;margin:5px;padding:0; line-height:135%;}
                                    </style>";
        private string StartHtml;
        private string EndHtml = "</body></html>";
        public Help(string NavigateTo = "0")
        {
            InitializeComponent();
            StartHtml = string.Format("<html><head>{0}</head><body>", CssStyle);
            try {
                LoadHelp();
                if (NavigateTo != "0")
                { //pre-select node
                    try
                    {
                        wbBody.Document.Write(string.Empty); //initializes browser
                        LoadTopic(Convert.ToInt32(NavigateTo));
                        SelectNode(tvHelp.Nodes[0], NavigateTo);
                        tvHelp.SelectedNode.Parent.Expand();
                        tvHelp.SelectedNode.Expand();
                    } catch {}
                }
            } catch (Exception ex) {
                //if (!DBManager.HasDBConnection())
                //    MessageBox.Show("Unable to connect to the server.\n\n Please check that you have a network connection, and try again later.");
                //else
                //{
                //    MessageBox.Show("There was an error retrieving the help topics. Please try again later.");
                //    Program.LogException(ex, false); 
                //}
                this.Close();
            }
        }

        private void LoadHelp()
        {
            try
            {
                wbBody.DocumentText = string.Format("{0}Welcome to Quick Launch Help Topics. Please select a Help Topic from the left pane.<p>For immediate assistance, contact the Service Desk 202.7565</p>{1}", StartHtml, EndHtml);
                List<Models.Help> HelpList = Models.Help.selHelps();
                AddHelpChildNodes(tvHelp.Nodes[0], HelpList);
                tvHelp.Nodes[0].Expand();
            }
            catch (Exception ee)
            {
                Shared.Domain.Concrete.Utility.LogException(ee, true, "LoadHelp");
            }  
        }

        private void AddHelpChildNodes(TreeNode parent, List<Models.Help> HelpList)
        {
            try
            {
                List<Models.Help> children = HelpList.FindAll(f => f.ParentId == Convert.ToInt32(parent.Tag.ToString()));
                if (children != null)
                {
                    foreach (Models.Help h in children)
                    {
                        TreeNode tn = new TreeNode(h.Title);
                        tn.Tag = h.id.ToString();
                        if (h.IsOpenOnload) tn.Expand();
                        parent.Nodes.Add(tn);
                        AddHelpChildNodes(tn, HelpList);
                    }
                }
            }
            catch (Exception ee)
            {
                Shared.Domain.Concrete.Utility.LogException(ee, true, "AddHelpChildNodes");
            }  
        }

        private void tvHelp_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {
                if (e.Node.Tag.ToString() == "0") return;
                if (!LoadTopic(Convert.ToInt32(e.Node.Tag.ToString())))
                    e.Node.Expand();
            }
            catch (Exception ee)
            {
                Shared.Domain.Concrete.Utility.LogException(ee, true, "tvHelp_AfterSelect");
            } 
        }

        private bool LoadTopic(int id)
        {
            try
            {
                Models.Help h = Models.Help.selHelpById(id);
                lbTitle.Text = h.Title;
                if (h.IsSelectable)
                {                    
                    wbBody.DocumentText = StartHtml + h.Body + EndHtml;
                }
                else
                {
                    StringBuilder sbHelp = new StringBuilder();
                    List<Models.Help> hs = Models.Help.selHelpByParentId(id);
                    foreach (Models.Help hp in hs)
                        sbHelp.Append(string.Format("<p><a href='id={0}'>{1}</a></p>", hp.id.ToString(), hp.Title));
                    wbBody.DocumentText = sbHelp.ToString();
                }

            }
            catch (Exception ex) { MessageBox.Show("There was an error retrieving that particular help topic. Please try again later."); Program.LogException(ex, false); }
            return true;    
        }


        private void sbBody_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            try
            {
                string id = e.Url.ToString().ToLower();
                if (id.StartsWith("about:id="))
                {
                    e.Cancel = true;
                    wbBody.Stop();
                    id = id.Replace("about:id=", string.Empty);
                    LoadTopic(Convert.ToInt32(id));
                    SelectNode(tvHelp.Nodes[0], id);
                    tvHelp.SelectedNode.Parent.Expand();
                }
            }
            catch (Exception ee)
            {
                Shared.Domain.Concrete.Utility.LogException(ee, true, "sbBody_Navigating");
            }
        }

        private void SelectNode(TreeNode ParentNode, string value)
        {
            try
            {
                foreach (TreeNode tn in ParentNode.Nodes)
                    if (tn.Tag.ToString() == value)
                    {
                        tvHelp.SelectedNode = tn;
                        break;
                    }
                    else
                        SelectNode(tn, value);
            }
            catch (Exception ee)
            {
                Shared.Domain.Concrete.Utility.LogException(ee, true, "SelectNode");
            }
        }
    }
}
