﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Security;
using QuickLaunch.Shared;

namespace QuickLaunch
{
    public partial class QLDebug : Form
    {
        public QLDebug()
        {
            InitializeComponent();
            gvProcs.ShowCellErrors = false;
            gvProcs.ShowRowErrors = false;
            gvProcs.DataError += new DataGridViewDataErrorEventHandler(gv_DataError);

            gvEvents.ShowCellErrors = false;
            gvEvents.ShowRowErrors = false;
            gvEvents.DataError += new DataGridViewDataErrorEventHandler(gv_DataError);
            
            try {
                LoadDebug();              
            } catch (Exception ex) {
                //if (!DBManager.HasDBConnection())
                //    MessageBox.Show("Unable to connect to the server.\n\n Please check that you have a network connection, and try again later.");
                //else
                //{
                //    MessageBox.Show("There was an error retrieving the debug stuff. Please try again later.");
                //    Program.LogException(ex, false); 
                //}
                this.Close();
            }
        }

        void gv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        { //hides not authorized errors on the procs binding
            //throw new NotImplementedException();
        }

        private void LoadDebug()
        {
            //Models.Machine m = Models.Machine.selMachineById(System.Environment.MachineName);            
            gvProcs.DataSource = Process.GetProcesses();
            gvEvents.DataSource = Shared.SysHelper.GetErrorLog(3);
            StringBuilder sbComputer = new StringBuilder();
            try
            {
                System.Collections.ArrayList al = Shared.SysHelper.GetWMIAttributes("Win32_ComputerSystem", string.Empty);
                foreach (string s in al.ToArray(typeof(string)))
                {
                    sbComputer.AppendLine(s);
                }
            }
            catch { }
            lblSysInfo.Text = sbComputer.ToString();

            StringBuilder sbOS = new StringBuilder();
            try
            {
                System.Collections.ArrayList al2 = Shared.SysHelper.GetWMIAttributes("Win32_OperatingSystem", "where Primary='true'");
                foreach (string s in al2.ToArray(typeof(string)))
                    sbOS.AppendLine(s);
            }
            catch { }
            lblOS.Text = sbOS.ToString();
                        
        }
               

        private void refreshToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LoadDebug();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void commandPromptToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt();
        }

        private void LaunchCommandPrompt(string _params = ""){
            QuickLaunch ql = this.Owner as QuickLaunch;
            Process p = Process.Start("cmd", string.IsNullOrEmpty(_params) ? string.Empty : "/c " + _params, ql.CurrentUser.UserName, Utility.ConvertToSecureString(ql.CurrentUser.Password), ql.CurrentUser.Domain);
            p.WaitForExit(750);            
        }

        private void registryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt("regedit");
        }

        private void servicesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt("services.msc");
        }

        private void networkConnectionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt("ncpa.cpl");

        }

        private void eventViewerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt("eventvwr.msc");            
        }

        private void addRemoveProgramsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt("appwiz.cpl");
        }

        private void taskManagerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt("taskmgr");
        }

        private void browserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LaunchCommandPrompt("start iexplore \"http://bhwebapp.bmcjax.com/TITO_Admin\"");
        }
        
    }
}
