﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickLaunch
{
    /// <summary>
    /// Form dialog for temporary messages
    /// </summary>
    public partial class DialogForm : Form
    {
        private const int WS_SYSMENU = 0x80000;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.Style &= ~WS_SYSMENU;
                return cp;
            }
        }
        public DialogForm()
        {
            InitializeComponent();
        }

        public DialogForm(string text, string caption)
        {
            InitializeComponent();
            this.CenterToScreen();
            this.Text = caption;
            this.content.Text = text;
        }
    }
}
