﻿using Microsoft.Exchange.WebServices.Data;
using QuickLaunch.Domain.Models;
using QuickLaunch.Models;
using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using System.Windows.Forms;
using WFICALib;

namespace QuickLaunch
{
    /// <summary>
    /// Code not directly related to the form, but too entangled to move into a separate class for now
    /// </summary>
    public partial class QuickLaunch : Form
    {
        private string PreviousState = "";
        private bool isLaunch = false;

        private static bool EnumerateADGroups(ref UserInfo user)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();

            var NetworkIsAvailable = ADHelper.GetGroups(ref user);

            sw.Stop();

            Truncus.LogEvents("1", "", user.UserName, sw.ElapsedMilliseconds, "AD Get Groups Baseline");

            return NetworkIsAvailable;
        }

        /// <summary>
        /// Records timing of the login event
        /// </summary>
        private void recordLogin()
        {

            double totalTime = timerRoamSearch.ElapsedMilliseconds + timerLogin.ElapsedMilliseconds;

            string logBase = Utility.getSettingFromCache("LoginBaseline");
            
            try
            {
                if (totalTime > double.Parse(logBase))
                {
                   // Models.EventLog_QL.LogEvent("LOGIN BASELINE", CurrentUserId, string.Empty, LengthOfTime: totalTime);
                    Truncus.insEventLog("LOGIN-BASELINE", CurrentUserId, Environment.MachineName, "", 0, totalTime.ToString());
                    Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(totalTime), "LOGIN-BASELINE");
                }
                else
                {
                    Truncus.insEventLog("LOGIN", CurrentUserId, Environment.MachineName, "", 0, totalTime.ToString());
                    Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(totalTime), "LOGIN");
                }
            }
            catch (Exception ex) //something failed just write it to the db
            {
                Truncus.insEventLog("LOGIN", CurrentUserId, Environment.MachineName, "", 0, totalTime.ToString());
                Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(totalTime), "LOGIN");
            }

            timerLogin.Reset();
            //SetCurrentUserStartTime = DateTime.Now; //reset only in logging code
            timerRoamSearch.Reset();
            isLogon = false; //so additional roams don't trigger another logon event


        }

        /// <summary>
        /// Kills the listed processes.
        /// </summary>
        public void KillListedProcesses()
        {
            //look for cache and if it doesn't exist, do another db pull
           // QLUtility.LogEvents(logLevel, "KillListedProcesses", "", 0, "START");
            List<string> killList = Models.KillList.getCache();

            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                if (killList.Count < 1)
                {
                    killList = new List<string>() { "iexplore", "firefox", "chrome", "notepad", "cmd", "natspeak" };
                }
            }
            catch (Exception ex)
            {
                killList = new List<string>() { "iexplore", "firefox", "chrome", "notepad", "cmd" };
            }

            foreach (string p in killList)
            {
                Process[] pname = Process.GetProcessesByName(p);
                foreach (var proc in pname)
                { //NOTE: Added try w/ empty catch because the kill can fail and that is OK.
                    try
                    {

                        if (p.Trim().ToLower().Contains("natspeak"))
                        {
                            //QLUtility.LogEvents("1", "", "", 0, "Natspeak running");

                            try
                            {
                              //  QLUtility.LogEvents("1", "", "", 0, "Natspeak running attempting graceful shutdown");

                                string path = "";

                                if (Directory.Exists(@"C:\Program Files (x86)\Nuance\NaturallySpeaking12\"))  //is OS 64 bit?
                                {
                                    path = @"C:\Program Files (x86)\Nuance\NaturallySpeaking12\Program\natspeak.exe";
                                }
                                else
                                {
                                    path = @"C:\Program Files\Nuance\NaturallySpeaking12\Program\natspeak.exe";
                                }

                                Process process = Process.Start(path, " /saveandshutdown");
                            }
                            catch (Exception ex)
                            {
                                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);

                                try
                                {
                                    proc.Kill();
                                }
                                catch (Exception ee)
                                {
                                    Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
                                }
                            }
                        }
                        else
                        {
                            proc.Kill();
                            // QLUtility.LogEvents(logLevel, "KillListedProcesses", "", 0, "Killing " + proc.ToString());
                        }
                    }
                    catch (Exception ex)
                    {
                        Models.ErrorLog.AddErrorToLog(ex, "", System.Reflection.Assembly.GetExecutingAssembly().ToString(), false);
                    }
                }
            }

            FormKill(sw);

            try
            {
                Truncus.LogEvents(logLevel, "", "", sw.ElapsedMilliseconds, "Kill Listed Processes");
            }
            catch (Exception ex)
            { }

           // QLUtility.LogEvents(logLevel, "KillListedProcesses", "", 0, "END");
        }

        private void FormKill(Stopwatch sw)
        {
            try
            {
                niSystemAlert.Visible = false; //KILLS ANY OS NOTIFICATIONS
                niPwdExpire.Visible = false;
                foreach (Form f in this.OwnedForms)
                {
                    if (f != null)
                        if (!f.IsDisposed)
                            f.Close();
                }

                sw.Stop();
            }
            catch (Exception ex) { }
        }

        private void startRoamStopwatch()
        {
            if (!swRoam.IsRunning)
            {
                swRoam.Reset();
                swRoam.Start();
            }
            else
            {
                swRoam.Stop();
                swRoam.Reset();
                swRoam.Start();
            }
        }

        /// <summary>
        /// Handles the ReportProgress event of the LogonCompleted control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        void LaunchCompleted_ReportProgress(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (e.ProgressPercentage >= 100)
                {
                   LogLaunchandRoam();
                  // LogLaunch();
                }
                else if (progressBar.Value <= 90)
                {
                    progressBar.Value += 10;
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// moved to QL autoinstaller
        /// </summary>
        [Obsolete]
        private void StartSVC()
        {
            try
            {
                NetTcpBinding binding = new NetTcpBinding();
                //  string address = "net.tcp://" + System.Environment.MachineName + ".BH.LOCAL:8000/wcfserver";
                string address = "net.tcp://" + SysHelper.GetIpAddress() + ":8000/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());
                ServiceHost serviceHost = new ServiceHost(typeof(WCFServer), baseAddress);
                serviceHost.AddServiceEndpoint(typeof(IWCFServer), binding, baseAddress);
                serviceHost.Open();
                //  MessageBox.Show("started");
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }

        }

        #region Terminal Services Functions
        /// <summary>
        /// Handles the click event of the menu item disconnect which disconnects all user sessions
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        private void tsDisconnect_Click(object sender, EventArgs e)
        {
            try
            {
                lblNotifier.Text = "Disconnecting Sessions on Network";
                pnlNotifier.Visible = true;
                RoamingDelegate rd = StartRoamingProcess;
                TSDisconnectParam param = new TSDisconnectParam { Seconds = 3, IsLogOff = false, ErrorMsg = "Disconnect Error Occurred", SuccessMsg = "Disconnect Complete. Attempting Roam...", ClearMsgCallback = rd };
                StartTSDisconnect(param);
                this.Refresh();
            }
            catch (Exception ex) { Program.LogException(ex, false); MessageBox.Show("An error occurred while disconnecting your EMR sessions."); }

        }

        /// <summary>
        /// Handles the click event of the menu item LOGOFF which DISCONNECTS AND LOGOFFS all user sessions
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>   
        private void tsLogoff_Click(object sender, EventArgs e)
        {
            try
            {
                lblNotifier.Text = "Killing Session...Please Wait";
                pnlNotifier.Visible = true;
                TSDisconnectParam param = new TSDisconnectParam { Seconds = 70, IsLogOff = true, ErrorMsg = "Logoff/Reset Error Occurred", SuccessMsg = "Logoff/Reset Complete", ClearMsgCallback = null };
                StartTSDisconnect(param);
                this.Refresh();
            }
            catch (Exception ex) { Program.LogException(ex, false); MessageBox.Show("An error occurred while logging off your EMR sessions."); }


        }

        /// <summary>
        /// Kicks off background async disconnect task
        /// </summary>
        private void StartTSDisconnect(TSDisconnectParam param)
        {
            try
            {
                progressBar.Value = 0;
                Debug.WriteLine(string.Format("bwTSDisconnect Progress Bar based on {0} seconds", param.Seconds));
                bwTSDisconnect = new BackgroundWorker();
                // bwTSDisconnect.WorkerSupportsCancellation = true;
                bwTSDisconnect.WorkerReportsProgress = true;
                bwTSDisconnect.ProgressChanged += new ProgressChangedEventHandler(bwTSDisconnect_ReportProgress);
                bwTSDisconnect.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwTSDisconnect_RunWorkerCompleted);
                bwTSDisconnect.DoWork += new DoWorkEventHandler(bwTSDisconnect_DoWork);
                bwTSDisconnect.RunWorkerAsync(param);
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Executes after async disconnect completes. It displays results to user.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bwTSDisconnect_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                TSDisconnectParam param = e.Result as TSDisconnectParam;
                Exception exResult = param.WorkCompleteResult as Exception;
                if (exResult != null)
                {
                    if (CurrentState == ImprivataEvent.Install)
                    {
                        DisconnectCitrixSessionsIfNecessary();
                        pnlNotifier.Visible = false; 
                        return;
                    }
                    lblNotifier.Text = param.ErrorMsg;
                    Program.LogException(exResult, false);
                }
                else
                    lblNotifier.Text = param.SuccessMsg;

                progressBar.Value = 100;
                this.Refresh();
                BuildUserContext();
                ClearNotifyMessage(3, param.ClearMsgCallback);
            }
            catch (Exception ex) { Program.LogException(ex, false); }
        }

        /// <summary>
        /// Handles bwTSDisconnect report progress
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        void bwTSDisconnect_ReportProgress(object sender, ProgressChangedEventArgs e)
        {
          //  Debug.WriteLine(string.Format("Starting doWork bwTSDisconnect report progress of {0}", e.ProgressPercentage.ToString()));
            progressBar.Value = e.ProgressPercentage;
        }

        /// <summary>
        /// Handles the DoWork event of the bwTSDisconnect
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        void bwTSDisconnect_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker bw = sender as BackgroundWorker;
                TSDisconnectParam param = e.Argument as TSDisconnectParam;

                var rEventTSDicon = new ManualResetEvent(false);
                ThreadPool.QueueUserWorkItem(h =>
                {
                    param.WorkCompleteResult = ApplicationLauncher.TerminalServicesDisconnect(ica, CurrentUser, IsLogOff: param.IsLogOff);
                    e.Result = param;
                    rEventTSDicon.Set();
                });

                double x = 0;
                double xIncrement = Convert.ToDouble(100) / Convert.ToDouble(param.Seconds);
               // Debug.WriteLine(string.Format("Starting doWork bwTSDisconnect increment of {0}", xIncrement));
                while (x < 100)
                {
                    try
                    {
                        x += xIncrement;
                        if (rEventTSDicon.WaitOne(1000)) x = 100; //wait on thread to finish
                        bw.ReportProgress(Convert.ToInt32(x));
                      //  Debug.WriteLine(string.Format("Starting doWork bwTSDisconnect while value of {0}", x.ToString()));
                    }
                    catch { }
                }
                rEventTSDicon.WaitOne(); //wait on thread to finish
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        #endregion

        #region Multiple ICA Session Monitoring
        [System.Security.SuppressUnmanagedCodeSecurity]
        internal static class UnsafeNativeMethods
        {
            [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true, CharSet = System.Runtime.InteropServices.CharSet.Auto)]
            internal static extern int GetWindowText(IntPtr hWnd, [System.Runtime.InteropServices.Out] System.Text.StringBuilder lpString, int nMaxCount);
            [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
            internal static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
            [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
            internal static extern bool SetForegroundWindow(IntPtr hWnd);
            [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
            internal static extern bool ShowWindow(IntPtr hWnd, int nCmdShow); //2 minimizes

            [System.Runtime.InteropServices.DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true)]
            internal static extern IntPtr SendMessage(IntPtr hWnd, Int32 Msg, IntPtr wParam, IntPtr lParam);

            internal const int WM_COMMAND = 0x111;
            internal const int MIN_ALL = 419;
            internal const int MIN_ALL_UNDO = 416;


        }

        #endregion

        #region ICA SDK Events

        /// <summary>
        /// Connects the ICA events. Most of these events are unused, but I've left them in place
        /// because they won't cause any problems and may be used in the future. Moved unused and log only to Citrix Methods.
        /// </summary>
        private void ConnectICAEvents()
        {
            try
            {
                ica.OnACRReconnected += new _IICAClientEvents_OnACRReconnectedEventHandler(CitrixMethods.ica_OnACRReconnected);
                ica.OnACRReconnectFailed += new _IICAClientEvents_OnACRReconnectFailedEventHandler(CitrixMethods.ica_OnACRReconnectFailed);
                ica.OnCGPDisconnect += new _IICAClientEvents_OnCGPDisconnectEventHandler(CitrixMethods.ica_OnCGPDisconnect);
                ica.OnCGPReconnect += new _IICAClientEvents_OnCGPReconnectEventHandler(CitrixMethods.ica_OnCGPReconnect);
                ica.OnCGPUnwarn += new _IICAClientEvents_OnCGPUnwarnEventHandler(CitrixMethods.ica_OnCGPUnwarn);
                ica.OnCGPWarn += new _IICAClientEvents_OnCGPWarnEventHandler(CitrixMethods.ica_OnCGPWarn);
                ica.OnChannelDataReceived += new _IICAClientEvents_OnChannelDataReceivedEventHandler(CitrixMethods.ica_OnChannelDataReceived);
                ica.OnConnect += new _IICAClientEvents_OnConnectEventHandler(ica_OnConnect);
                ica.OnConnectFailed += new _IICAClientEvents_OnConnectFailedEventHandler(ica_OnConnectFailed);
                ica.OnConnecting += new _IICAClientEvents_OnConnectingEventHandler(ica_OnConnecting);
                ica.OnDisconnect += new _IICAClientEvents_OnDisconnectEventHandler(ica_OnDisconnect);
                ica.OnDisconnectFailed += new _IICAClientEvents_OnDisconnectFailedEventHandler(CitrixMethods.ica_OnDisconnectFailed);
                ica.OnDisconnectSessions += new _IICAClientEvents_OnDisconnectSessionsEventHandler(CitrixMethods.ica_OnDisconnectSessions);
                ica.OnDisconnectSessionsFailed += new _IICAClientEvents_OnDisconnectSessionsFailedEventHandler(CitrixMethods.ica_OnDisconnectSessionsFailed);
                ica.OnICAFile += new _IICAClientEvents_OnICAFileEventHandler(ica_OnICAFile);
                ica.OnICAFileFailed += new _IICAClientEvents_OnICAFileFailedEventHandler(ica_OnICAFileFailed);
                ica.OnInitializing += new _IICAClientEvents_OnInitializingEventHandler(ica_OnInitializing);
                ica.OnInitialProp += new _IICAClientEvents_OnInitialPropEventHandler(CitrixMethods.ica_OnInitialProp);
                ica.OnLogoffFailed += new _IICAClientEvents_OnLogoffFailedEventHandler(CitrixMethods.ica_OnLogoffFailed);
                ica.OnLogoffSessions += new _IICAClientEvents_OnLogoffSessionsEventHandler(CitrixMethods.ica_OnLogoffSessions);
                ica.OnLogoffSessionsFailed += new _IICAClientEvents_OnLogoffSessionsFailedEventHandler(CitrixMethods.ica_OnLogoffSessionsFailed);
                ica.OnLogon += new _IICAClientEvents_OnLogonEventHandler(ica_OnLogon);
                ica.OnLogonFailed += new _IICAClientEvents_OnLogonFailedEventHandler(ica_OnLogonFailed);
                ica.OnPublishedApp += new _IICAClientEvents_OnPublishedAppEventHandler(ica_OnPublishedApp);
                ica.OnPublishedAppFailed += new _IICAClientEvents_OnPublishedAppFailedEventHandler(CitrixMethods.ica_OnPublishedAppFailed);
                ica.OnReadyStateChange += new _IICAClientEvents_OnReadyStateChangeEventHandler(CitrixMethods.ica_OnReadyStateChange);
                ica.OnSessionAttach += new _IICAClientEvents_OnSessionAttachEventHandler(CitrixMethods.ica_OnSessionAttach);
                ica.OnSessionDetach += new _IICAClientEvents_OnSessionDetachEventHandler(CitrixMethods.ica_OnSessionDetach);
                ica.OnSessionEventPending += new _IICAClientEvents_OnSessionEventPendingEventHandler(ica_OnSessionEventPending);
                ica.OnSessionSwitch += new _IICAClientEvents_OnSessionSwitchEventHandler(ica_OnSessionSwitch);
                ica.OnWindowCloseRequest += new _IICAClientEvents_OnWindowCloseRequestEventHandler(CitrixMethods.ica_OnWindowCloseRequest);
                ica.OnWindowCreated += new _IICAClientEvents_OnWindowCreatedEventHandler(CitrixMethods.ica_OnWindowCreated);
                ica.OnWindowDestroyed += new _IICAClientEvents_OnWindowDestroyedEventHandler(ica_OnWindowDestroyed);
                ica.OnWindowDisplayed += new _IICAClientEvents_OnWindowDisplayedEventHandler(CitrixMethods.ica_OnWindowDisplayed);
                ica.OnWindowDocked += new _IICAClientEvents_OnWindowDockedEventHandler(CitrixMethods.ica_OnWindowDocked);
                ica.OnWindowFullscreened += new _IICAClientEvents_OnWindowFullscreenedEventHandler(CitrixMethods.ica_OnWindowFullscreened);
                ica.OnWindowHidden += new _IICAClientEvents_OnWindowHiddenEventHandler(CitrixMethods.ica_OnWindowHidden);
                ica.OnWindowMaximized += new _IICAClientEvents_OnWindowMaximizedEventHandler(CitrixMethods.ica_OnWindowMaximized);
                ica.OnWindowMinimized += new _IICAClientEvents_OnWindowMinimizedEventHandler(CitrixMethods.ica_OnWindowMinimized);
                ica.OnWindowMoved += new _IICAClientEvents_OnWindowMovedEventHandler(CitrixMethods.ica_OnWindowMoved);
                ica.OnWindowRestored += new _IICAClientEvents_OnWindowRestoredEventHandler(CitrixMethods.ica_OnWindowRestored);
                ica.OnWindowSized += new _IICAClientEvents_OnWindowSizedEventHandler(CitrixMethods.ica_OnWindowSized);
                ica.OnWindowUndocked += new _IICAClientEvents_OnWindowUndockedEventHandler(CitrixMethods.ica_OnWindowUndocked);
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void ica_OnSessionEventPending(int hSession, int EventNum)
        {
            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnSessionEventPending");
            }
        }

        void ica_OnSessionSwitch(int hOldSession, int hNewSession)
        {
            //Debug.WriteLine(string.Format("ica_OnSessionSwitch: ({0}), ({1})", hOldSession, hNewSession));
            //lblNotifier.Text += ".";

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnSessionSwitch");
            }

            if (progressBar.Value <= 90)
                progressBar.Value += 10;
        }

        void ica_OnLogonFailed()
        {
            //Debug.WriteLine("ica_OnLogonFailed");
           // dtRoamStart = new DateTime(1900, 1, 1);
           // dtLaunchEMRTime = new DateTime(1900, 1, 1);

            pnlNotifier.Visible = false;
            swLaunch.Reset();
            swRoam.Reset();
            timerRoam.Stop();
            timerLaunch.Stop();
            isLaunch = false;

            if (CurrentUser != null && CurrentLaunchApplication != null)
            {
                bool isCleared = new AppQueue().ClearLaunch(CurrentUser.UserName, CurrentLaunchApplication.Value);
            }

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnLogonFailed");
            }
        }

        void ica_OnInitializing()
        {
            // Debug.WriteLine("ica_OnInitializing");
            //lblNotifier.Text += ".";
            if (progressBar.Value <= 90)
                progressBar.Value += 10;

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnInitializing");
            }
        }

        void ica_OnICAFileFailed()
        {
            MessageBox.Show("ICA File creation has failed, which sometimes indicates a corrupt ICA client install. Quicklaunch will now restart. If this issue persists, please restart your machine before calling the Service Desk.");

            pnlNotifier.Visible = false;
          //  dtRoamStart = new DateTime(1900, 1, 1);
           // dtLaunchEMRTime = new DateTime(1900, 1, 1);
            swLaunch.Reset();
            swRoam.Reset();
            timerRoam.Stop();
            timerLaunch.Stop();
            isLaunch = false;
            if (CurrentUser != null && CurrentLaunchApplication != null)
            {
                bool isCleared = new AppQueue().ClearLaunch(CurrentUser.UserName, CurrentLaunchApplication.Value);
            }

            //Debug.WriteLine("ica_OnICAFileFailed");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnICAFileFailed");
            }

            Program.LogException(new Exception("ica_OnICAFileFailed : Usually a corrupt ICA client is at fault"), false);

            LoadNotifier();        
        }

        void ica_OnICAFile()
        {
            Debug.WriteLine("ica_OnICAFile");
            //lblNotifier.Text += ".";
            if (progressBar.Value <= 90)
                progressBar.Value += 10;

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnICAFile");
            }
        }

        void ica_OnWindowDestroyed(int WndType)
        {
            // Debug.WriteLine("ica_OnWindowDestroyed");
            DisconnectCitrixSessionsIfNecessary();

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnWindowDestroyed");
            }
        }

        void ica_OnPublishedApp()
        {
            // Debug.WriteLine("ica_OnPublishedApp");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", "", 0, "ica_OnPublishedApp");
            }

            if(pnlNotifier.Visible)
            {
                LogLaunchandRoam();
            }
        }

        /// <summary>
        /// Triggered after the ICA client completes login. Used to provide a delay in showing the buttons again.
        /// </summary>
        void ica_OnLogon()
        {
            try
            {

                lblNotifier.Text = "Loading";
                progressBar.Value = 0;
                //  Debug.WriteLine("ica_OnLogon");

                if ((logLevel == "2") || (logLevel == "3"))
                {
                    Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnLogon");
                }

                icaWorker = new BackgroundWorker();
                icaWorker.WorkerSupportsCancellation = true;
                icaWorker.WorkerReportsProgress = true;
                icaWorker.ProgressChanged += new ProgressChangedEventHandler(LaunchCompleted_ReportProgress);
                icaWorker.DoWork += new DoWorkEventHandler(LaunchCompleted_DoWork);
               // icaWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(LaunchCompleted_Finalize);
                icaWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }


        /// <summary>
        /// method for logging launches
        /// </summary>
        private void LogLaunch()
        {
            try
            {
                timerLaunch.Stop();

                if (progressBar.Value > 100) //fake out progress 
                {
                    progressBar.Value = 100;
                    Thread.Sleep(2000);
                }

                if (isLaunch)
                {
                    try
                    {
                        double launchLength = swLaunch.ElapsedMilliseconds;

                        Models.UserProfile.updUserProfileRoamingMetrics(CurrentUserId, LastEMRLaunchLength: launchLength);

                        string launchApp = CurrentLaunchApplication.Value;

                        if ((!string.IsNullOrEmpty(ApplicationLauncher.lastapp)) && (!string.IsNullOrEmpty(ApplicationLauncher.lastserver)))
                        {
                            launchApp = ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver;
                        }

                        if (isAuto)
                        {
                            //second write to DB
                            if (launchLength > 60000)
                            {
                                Truncus.insEventLog("LAUNCH-AUTO-DELAY", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                                Truncus.LogEvents("1", launchApp, CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-AUTO-DELAY");
                            }
                            else
                            {
                                Truncus.insEventLog("LAUNCH-AUTO", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                                Truncus.LogEvents("1", launchApp, CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-AUTO");
                            }
                        }
                        else if (launchLength > 0)
                        {
                            //second write to DB
                            if (launchLength > 60000)
                            {
                                Truncus.insEventLog("LAUNCH-DELAY", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                                Truncus.LogEvents("1", launchApp, CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-DELAY");
                            }
                            else if (launchLength < 1500)
                            {
                                Truncus.insEventLog("LAUNCH-RELAUNCH", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                                Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-RELAUNCH");
                            }
                            else if ((launchLength > 1500) && (launchLength < 60000))
                            {
                                Truncus.insEventLog("LAUNCH", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                                Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH");
                            }
                        }

                        Process[] procs = GetWFICAProcess();
                        foreach (Process p in procs)
                        {
                            FocusWindow(p);
                        }

                    }
                    catch (Exception ex)
                    {
                        Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
                    }
                    finally
                    {
                        CurrentLaunchApplication = null; //clear
                        isAuto = false; //reset for the next one
                        isLaunch = false;
                        swLaunch.Reset();
                        pnlNotifier.Visible = false;
                        this.Refresh();
                    }
                }
                else
                {
                    if(swLaunch.ElapsedMilliseconds > 0)
                    {
                        LogRelaunch(swLaunch.ElapsedMilliseconds);
                    }
                }
    
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }

        }

        private void LogRelaunch(long timer)
        {
            try
            {
                double rLength = timer;

                if (progressBar.Value > 100) //fake out progress 
                {
                    progressBar.Value = 100;
                    Thread.Sleep(2000);
                }

                if ((rLength < 1500) && (rLength > 0))
                {
                    Models.UserProfile.updUserProfileRoamingMetrics(CurrentUserId, Roaming: true, LastRoamLength: rLength);
                    if ((!string.IsNullOrEmpty(ApplicationLauncher.lastapp)) && (!string.IsNullOrEmpty(ApplicationLauncher.lastserver)))
                    {
                        Truncus.insEventLog("LAUNCH-RELAUNCH", CurrentUserId, Environment.MachineName, ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, 0, rLength.ToString());
                        Truncus.LogEvents("1", ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, CurrentUserId, Convert.ToInt64(rLength), "LAUNCH-RELAUNCH");
                    }
                    else
                    {
                        Truncus.insEventLog("LAUNCH-RELAUNCH", CurrentUserId, Environment.MachineName, "", 0, rLength.ToString());
                        Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(rLength), "LAUNCH-RELAUNCH");
                    }
                }
                else if (rLength > 1500)
                {
                    Truncus.insEventLog("LAUNCH", CurrentUserId, Environment.MachineName, ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, 0, rLength.ToString());
                    Truncus.LogEvents("1", ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, CurrentUserId, Convert.ToInt64(rLength), "LAUNCH");
                }

                Process[] procs = GetWFICAProcess();
                foreach (Process p in procs)
                {
                    FocusWindow(p);
                }

            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            finally
            {
                //swRoam.Reset();
                swLaunch.Reset();
                pnlNotifier.Visible = false;
                this.Refresh();
            }
        }

        /// <summary>
        /// method for logging roams
        /// </summary>
        private void LogRoam()
        {
            try
            {

                timerRoam.Stop();

                if (progressBar.Value > 100) //fake out progress 
                {
                    progressBar.Value = 100;
                    Thread.Sleep(2000);
                }

                double rlength = swRoam.ElapsedMilliseconds;

                if (rlength > 0)
                {
                    Models.UserProfile.updUserProfileRoamingMetrics(CurrentUserId, Roaming: true, LastRoamLength: rlength);
                    if ((!string.IsNullOrEmpty(ApplicationLauncher.lastapp)) && (!string.IsNullOrEmpty(ApplicationLauncher.lastserver)))
                    {
                        Truncus.insEventLog("ROAM", CurrentUserId, Environment.MachineName, ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, 0, rlength.ToString());
                        Truncus.LogEvents("1", ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, CurrentUserId, Convert.ToInt64(rlength), "ROAM");
                    }
                    else
                    {
                        Truncus.insEventLog("ROAM", CurrentUserId, Environment.MachineName, "", 0, rlength.ToString());
                        Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(rlength), "ROAM");
                    }
                }

                Process[] procs = GetWFICAProcess();
                foreach (Process p in procs)
                {
                    FocusWindow(p);
                }

            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            finally
            {
                swRoam.Reset();
                pnlNotifier.Visible = false;
                this.Refresh();
            }
        }

        /// <summary>
        /// method for logging launches and roams
        /// </summary>
        private void LogLaunchandRoam()
        {
            try
            {
                          
                timerLaunch.Stop();
                timerRoam.Stop();

                if (progressBar.Value > 100) //fake out progress 
                {
                    progressBar.Value = 100;
                    Thread.Sleep(2000);
                }

                if (isLaunch)
                {
                    //double launchLength = DateTime.Now.Subtract(dtLaunchEMRTime).TotalMilliseconds;
                    double launchLength = swLaunch.ElapsedMilliseconds;
                    //first write to DB
                    Models.UserProfile.updUserProfileRoamingMetrics(CurrentUserId, LastEMRLaunchLength: launchLength);

                    string launchApp = CurrentLaunchApplication.Value;

                    if ((!string.IsNullOrEmpty(ApplicationLauncher.lastapp)) && (!string.IsNullOrEmpty(ApplicationLauncher.lastserver)))
                    {
                        launchApp = ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver;
                    }

                    if (isAuto)
                    {
                        //second write to DB
                        if (launchLength > 60000)
                        {
                            //Models.EventLog_QL.LogEvent("LAUNCH-DELAY-AUTO", CurrentUserId, launchApp, MenuItemId: CurrentLaunchApplication.MenuLink_Id, LengthOfTime: launchLength);

                            Truncus.insEventLog("LAUNCH-AUTO-DELAY", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                            Truncus.LogEvents("1", launchApp, CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-AUTO-DELAY");
                        }
                        else if (launchLength > 0)
                        {
                            //Models.EventLog_QL.LogEvent("LAUNCH-AUTO", CurrentUserId, launchApp, MenuItemId: CurrentLaunchApplication.MenuLink_Id, LengthOfTime: launchLength);

                            Truncus.insEventLog("LAUNCH-AUTO", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                            Truncus.LogEvents("1", launchApp, CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-AUTO");
                        }
                    }
                    else if (launchLength > 0)
                    {
                        if (launchLength > 60000)
                        {
                            Truncus.insEventLog("LAUNCH-DELAY", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                            Truncus.LogEvents("1", launchApp, CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-DELAY");
                        }
                        else if (launchLength < 1500)
                        {
                            Truncus.insEventLog("LAUNCH-RELAUNCH", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                            Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH-RELAUNCH");
                        }
                        else if ((launchLength > 1500) && (launchLength < 60000))
                        {
                            Truncus.insEventLog("LAUNCH", CurrentUserId, Environment.MachineName, launchApp, CurrentLaunchApplication.MenuLink_Id, launchLength.ToString());
                            Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(launchLength), "LAUNCH");
                        }
                    }
                }
                else if (swLaunch.ElapsedMilliseconds > 0)
                {
                    LogRelaunch(swLaunch.ElapsedMilliseconds);                    
                }
                else
                {
                    //double roamLength = DateTime.Now.Subtract(dtRoamStart).TotalMilliseconds;
                    double roamLength = swRoam.ElapsedMilliseconds;
                    if (roamLength > 0)
                    {
                        Models.UserProfile.updUserProfileRoamingMetrics(CurrentUserId, Roaming: true, LastRoamLength: roamLength);
                        if ((!string.IsNullOrEmpty(ApplicationLauncher.lastapp)) && (!string.IsNullOrEmpty(ApplicationLauncher.lastserver)))
                        {
                            //Models.EventLog_QL.LogEvent("ROAM", CurrentUserId, ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, LengthOfTime: roamLength);
                            Truncus.insEventLog("ROAM", CurrentUserId, Environment.MachineName, ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, 0, roamLength.ToString());
                            Truncus.LogEvents("1", ApplicationLauncher.lastapp + ": " + ApplicationLauncher.lastserver, CurrentUserId, Convert.ToInt64(roamLength), "ROAM");
                        }
                        else
                        {
                            //  Models.EventLog_QL.LogEvent("ROAM", CurrentUserId, string.Empty, LengthOfTime: roamLength);
                            Truncus.insEventLog("ROAM", CurrentUserId, Environment.MachineName, "", 0, roamLength.ToString());
                            Truncus.LogEvents("1", "", CurrentUserId, Convert.ToInt64(roamLength), "ROAM");
                        }
                    }
                }

                Process[] procs = GetWFICAProcess();
                foreach (Process p in procs)
                {
                    FocusWindow(p);
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
            finally
            {
                CurrentLaunchApplication = null; //clear
                isAuto = false; //reset for the next one
                isLaunch = false;
                swRoam.Reset();
                swLaunch.Reset();
                pnlNotifier.Visible = false;
                this.Refresh();
            }
        }

        /// <summary>
        /// Handles the DoWork event of the LogonCompleted control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        void LaunchCompleted_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                int x = 0;
                int sleepSeconds = 10;
                int xIncrement = 100 / sleepSeconds;
                string rTitle = "";
                bool isShowing = false;

                    if ((CurrentLaunchApplication == null) || ((ApplicationLauncher.lastapp != null) &&(!string.IsNullOrEmpty(rTitle))))
                    {
                        List<MenuApplication> menuApps = MenuApplication.selMenuApplications();
                        var title = (from m in menuApps where ApplicationLauncher.lastapp.ToLower().Contains(m.Value.ToLower()) select m.Title);
                        rTitle = title.ToString();
                

                        while (((!isShowing) || swLaunch.ElapsedMilliseconds > 60000) && (x <100))
                        //while (x < 100)
                        {
                            Thread.Sleep(1000);
                            x += xIncrement;
                            icaWorker.ReportProgress(x);
                            //string ProcessTitle = string.Empty;
                            //Process[] procs = GetWFICAProcess();
                            //foreach (Process p in procs)
                            //{
                            //    ProcessTitle += FindWindowTitleByProcess(p);
                            //}

                            try
                            {
                                if (CurrentLaunchApplication != null)
                                {
                                    //if (ProcessTitle.Contains(CurrentLaunchApplication.WindowTitle))
                                    isShowing = FindByWindowTitle(CurrentLaunchApplication.WindowTitle);
                                    if(isShowing)
                                    {
                                        icaWorker.ReportProgress(100); //window is done loading                        
                                        break;
                                    }
                                }
                                else if((ApplicationLauncher.lastapp != null) &&(!string.IsNullOrEmpty(rTitle)))
                                {

                                    isShowing = FindByWindowTitle(rTitle);

                                    if(isShowing)
                                    {
                                        icaWorker.ReportProgress(100); //window is done loading                        
                                        break;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                else //can't find app
                                {
                                    break;
                                }
                            }
                            catch (Exception ex)
                            { }
                        }
                }
                else
                {
                    while (x < 120)
                    {
                        Thread.Sleep(1000);
                        x += xIncrement;
                        icaWorker.ReportProgress(x);
                    }
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        void ica_OnDisconnect()
        {
            // Debug.WriteLine("ica_OnDisconnect");

            //get list of remaining Citrix windows
            List<string> procsList = new List<string>();

            try
            {
                Process[] procs = GetWFICAProcess();
                foreach (Process p in procs)
                {
                    procsList.Add(FindWindowTitleByProcess(p));
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            bool isLaunched = new AppQueue().ResetLaunch(CurrentUser, procsList);

            //NEW
            isAuto = false; //reset for the next one
            isLaunch = false;
            swRoam.Reset();
            swLaunch.Reset();
            //END NEW

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnDisconnect");
            }

        }

        void ica_OnConnecting()
        {
            // Debug.WriteLine("ica_OnConnecting");
            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnConnecting");
            }

            if (progressBar.Value <= 90)
                progressBar.Value += 10;
        }

        void ica_OnConnectFailed()
        {

            try
            {
                string error = "ICA CONNECT FAILED : \n";
                int e = ica.GetLastError();
                int ce = ica.GetLastClientError();
                string msg1 = e > 0 ? ica.GetErrorMessage(e) : string.Empty;
                string msg2 = ce > 0 ? ica.GetClientErrorMessage(ce) : string.Empty;
                if (msg1 != string.Empty)
                {
                    Debug.WriteLine(string.Format("{0}: ({1})", msg1, e));
                    error += string.Format("{0}: ({1})\n", msg1, e);
                }
                if (msg2 != string.Empty)
                {
                    Debug.WriteLine(string.Format("{0}: ({1})", msg2, ce));
                    error += string.Format("{0}: ({1})\n", msg2, ce);
                }
                Program.LogException(new Exception(error), false);
                MessageBox.Show("Unable to connect, which sometimes indicates a corrupt ICA client install. Quick Launch will now reset its Citrix settings in an attempt to fix the issue now. Please restart your machine and try again before calling the Service Desk. Also, please verify that we are not currently in a downtime. \n\n" + error);

                swLaunch.Reset();
                swRoam.Reset();
                //timerRoam.Stop();
                //timerLaunch.Stop();
                //isLaunch = false;

                if ((logLevel == "2") || (logLevel == "3"))
                {
                    Truncus.LogEvents("2", "", CurrentUser.UserName, 0, "ica_OnConnectFailed");
                }
                else
                {
                    Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnConnectFailed");
                }

                //callback to Notifier and kill current QL process
                LoadNotifier();
              //  ResetConnection();

            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        void ica_OnConnect()
        {
            // Debug.WriteLine("ica_OnConnect");

            if ((logLevel == "2") || (logLevel == "3"))
            {
                Truncus.LogEvents("1", "", CurrentUser.UserName, 0, "ica_OnConnect");
            }

            if (progressBar.Value <= 90)
                progressBar.Value += 10;
        }

        private void BuildUserContext()
        {
            try
            {
                userContext = ApplicationLauncher.GetUserContext(CurrentUser);
                CurrentUser.ApplicationInfo = ApplicationLauncher.GetApplicationInfo(userContext, CurrentUser);
            }
            catch (TypeInitializationException ex) 
            { 
                Program.LogException(ex, true); 
                MessageBox.Show("J# Runtime could not be found"); 
                this.Visible = false; 
                lblNotifier.Text = "J# Runtime could not be found"; 
                progressBar.Value = 0; return; 
            }
            catch (Exception ex) 
            { 
                Program.LogException(ex, true); 
            }
        }
        #endregion

        #region ICA User Launch/Roaming Events

        /// <summary>
        /// Launches an application.
        /// </summary>
        /// <param name="application">The application.</param>
        public void LaunchApplication(Models.App citrixApp)
        {
            try
            {
                //ApplicationLaunching = true;
                backgroundWorker = new BackgroundWorker();
                backgroundWorker.WorkerSupportsCancellation = true;
                backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_ApplicationLauncher);
                backgroundWorker.WorkerReportsProgress = true;
                backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ApplicationLaunchProgressChanged);
                //backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_ApplicationLaunchComplete);
                timerLaunch.Start();
                backgroundWorker.RunWorkerAsync(citrixApp);
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        [Obsolete]
        private void backgroundWorker_ApplicationLaunchComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                if (icaWorker != null)
                {
                    while (icaWorker.IsBusy)
                    {
                        Thread.Sleep(1000);
                    }
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {
               LogLaunchandRoam();
                //LogLaunch();
            }
        }

        /// <summary>
        /// Handles the ApplicationLauncher event of the backgroundWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        void backgroundWorker_ApplicationLauncher(object sender, DoWorkEventArgs e)
        {
            try
            {
                var worker = (BackgroundWorker)sender;

                if (worker.CancellationPending)
                {
                    backgroundWorker.ReportProgress(100, "Timeout");

                }
                else
                {

                    backgroundWorker.ReportProgress(0, "Started");
                    backgroundWorker.ReportProgress(0, "BuildingICAFile");


                  //  ApplicationLauncher.RoamUsersApplications(backgroundWorker, ica, userContext, CurrentUser, ref timerRoamSearch //check for roam first???

                    if (ApplicationLauncher.LaunchApplication(backgroundWorker, ica, (Models.App)e.Argument, userContext, CurrentUser))
                    {

                        backgroundWorker.ReportProgress(0, "ICALaunched");

                        if(!ica.Launch)
                        {
                            isLaunch = false;
                        }
                        else
                        {
                            isLaunch = true;
                        }


                    }
                }

            }
            catch (Exception ex)
            {
                isLaunch = false;
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Handles the ApplicationLaunchProgressChanged event of the backgroundWorker control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        void backgroundWorker_ApplicationLaunchProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                switch (e.UserState.ToString())
                {
                    case "Started":
                        pnlNotifier.Visible = true;
                        break;
                    case "BuildingICAFile":
                        lblNotifier.Text = "Building ICA file";
                        break;
                    case "ICAFileBuilt":
                        lblNotifier.Text = "ICA File built";
                        break;
                    case "ICALaunched":
                        pnlNotifier.Visible = true;
                        lblNotifier.Text = "Starting application";
                        break;
                    case "Timeout":
                        //show message screen
                        if (swLaunch.IsRunning || swLaunch.ElapsedMilliseconds > 0)
                        {
                            lblNotifier.Text = "Launch Timeout, clearing sessions";
                            pnlNotifier.Visible = true;
                            TSDisconnectParam param = new TSDisconnectParam { Seconds = 70, IsLogOff = true, ErrorMsg = "Logoff/Reset Error Occurred", SuccessMsg = "Logoff/Reset Complete", ClearMsgCallback = null };
                            StartTSDisconnect(param);
                            this.Refresh();
                            Truncus.LogEvents("1", "", CurrentUser.UserName, swLaunch.ElapsedMilliseconds, "LAUNCH-TIMEOUT");
                            swLaunch.Stop();
                            swLaunch.Reset();
                            //dtLaunchEMRTime = new DateTime(1900, 1, 1);
                            CurrentLaunchApplication = null; //clear
                            pnlNotifier.Visible = false;
                        }
                        break;
                    default:
                        lblNotifier.Text = e.UserState.ToString();
                        break;
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Starts the roaming process.
        /// </summary>
        /// <param name="ica">The ica.</param>
        /// <param name="user">The user.</param>
        private void StartRoamingProcess() //ICAClientClass ica, UserInfo user
        {
            try
            {

                startRoamStopwatch(); 
                backgroundWorker = new BackgroundWorker();
                backgroundWorker.WorkerSupportsCancellation = true;
                backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
                backgroundWorker.WorkerReportsProgress = true;
                backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
                backgroundWorker.RunWorkerCompleted += backgroundWorker_RunWorkerCompleted;
                timerRoam.Start();
                backgroundWorker.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

 
        void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            lblNotifier.Text = "";

            if (timerRoamSearch.IsRunning)
            {
                timerRoamSearch.Stop();
            }

            if(isLogon)
            {
                recordLogin();
            }

            LogRoam(); //for roams only
            //LogLaunchandRoam();
            //login is complete, can now do notifications
            if (CurrentUser != null)
            {
                StartNotifications(CurrentUser); //email
            }

        }

        /// <summary>
        /// Handles the ProgressChanged event of the backgroundWorker control, Roaming only
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.ProgressChangedEventArgs"/> instance containing the event data.</param>
        void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                if (e.UserState == null) return; //switching quickly between users while roaming can cause a null

                if (e.UserState.ToString() != PreviousState)
                {
                    progressBar.Value = 0; //some events get called multiple times
                }

                switch (e.UserState.ToString())
                {
                    case "Started":
                        pnlNotifier.Visible = true;
                        lblNotifier.Text = "Looking for session";
                        break;
                    case "ApplicationsFound":
                        pnlNotifier.Visible = true;
                        lblNotifier.Text = "Session found, roaming";
                        break;
                    case "ICAStarted":
                        break;
                    case "NoApplicationsFound":
                        lblNotifier.Text = "No session found";
                        progressBar.Value = 100;
                        swRoam.Reset();
                        Thread.Sleep(3000);
                        pnlNotifier.Visible = false;
                        this.Refresh();

                        break;
                    case "Done":
                        progressBar.Value = 100;
                        break;
                    case "Timeout":
                        //show message screen

                        if (swRoam.IsRunning || swRoam.ElapsedMilliseconds > 0)
                        {
                            lblNotifier.Text = "Roaming Timeout, clearing sessions";
                            pnlNotifier.Visible = true;
                            TSDisconnectParam param = new TSDisconnectParam { Seconds = 70, IsLogOff = true, ErrorMsg = "Logoff/Reset Error Occurred", SuccessMsg = "Logoff/Reset Complete", ClearMsgCallback = null };
                            StartTSDisconnect(param);
                            this.Refresh();
                            // dtRoamStart = new DateTime(1900, 1, 1);   
                            Truncus.LogEvents("1", "", CurrentUser.UserName, swRoam.ElapsedMilliseconds, "ROAM-TIMEOUT");
                            swRoam.Reset();
                            swLaunch.Reset();
                        }
                        break;
                    case "Reconnecting":
                        lblNotifier.Text = "Session, reconnecting...";
                        break;
                    case "Unable to start application":
                        
                        if(swRoam.IsRunning)
                        {
                            swRoam.Reset();
                        }

                        if(swLaunch.IsRunning)
                        {
                            swLaunch.Reset();
                        }

                        Thread.Sleep(3000);
                        pnlNotifier.Visible = false;
                        this.Refresh();

                        break;
                    default:
                        lblNotifier.Text = e.UserState.ToString();
                        break;
                }
                PreviousState = e.UserState.ToString();
                this.Refresh();
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }


        /// <summary>
        /// Handles the DoWork event of the backgroundWorker control, roam and autolaunch only
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.ComponentModel.DoWorkEventArgs"/> instance containing the event data.</param>
        void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {

            try
            {
                var worker = (BackgroundWorker)sender;

                if (worker.CancellationPending)
                {
                    if(timerRoamSearch.IsRunning)
                    {
                        timerRoamSearch.Stop();
                    }

                    //return;
                    backgroundWorker.ReportProgress(100, "Timeout");
                }
                else
                {
                    timerRoamSearch.Reset();
                    timerRoamSearch.Start(); //stopped in application launcher

                    backgroundWorker.ReportProgress(0, "Started");
                    if (ApplicationLauncher.RoamUsersApplications(backgroundWorker, ica, userContext, CurrentUser, ref timerRoamSearch)) //ROAM
                    {
                        backgroundWorker.ReportProgress(10, "ICAStarted");
                    }
                    else
                    { 
                        swRoam.Reset(); //not a roam, so reset the timer

                        try
                        {
                          //  backgroundWorker.ReportProgress(0, "Started"); //LAUNCH
                            autoLaunchLogic(); //start autolaunch now
                            Thread.Sleep(5000);
                            //backgroundWorker.ReportProgress(0, "Done");
                        }
                        catch (InvalidOperationException ioe) { } //happens sometimes when autolaunch is enabled...because worker could be complete                
                    }
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Logic for autolaunch, TO DO, make this dynamic, eventually...
        /// </summary>
        private void autoLaunchLogic()
        {
            try
            {
                if (!string.IsNullOrEmpty(CurrentUser.CurrentProfile.CitrixAutoLaunchApp) && !IsCitrixOpen())
                {
                    switch (CurrentUser.CurrentProfile.CitrixAutoLaunchApp)
                    {
                        case "PC":
                            Citrix_Click(_MenuItems.Find(c => c.link == "PC" || c.link == "EMR"), null);
                            break;
                        case "FN":
                            Citrix_Click(_MenuItems.Find(c => c.link == "FN"), null);
                            break;
                        case "SN":
                            Citrix_Click(_MenuItems.Find(c => c.link == "SN"), null);
                            break;
                        case "AS": 
                            Citrix_Click(_MenuItems.Find(c => c.link == "CITRIX" && c.apps[0].Title.ToLower().Contains("touchworks")), null); //AS
                            break;
                        case "AN":
                            Citrix_Click(_MenuItems.Find(c => c.link == "CITRIX" && c.apps[0].Title.ToLower().Contains("anesthesia")), null); //AN
                            break;
                        default:
                            return;
                    }
                    progressBar.Value = 0;
                    lblNotifier.Text = "No session found, auto-launching EMR";
                    UIHelper.DisplayOSBubble(niSystemAlert, "EMR Auto Launch", "If you would like to change the auto-launch settings, click your user id at the top right of the Quick Launch bar.");
                    isAuto = true;
                }
            }
            catch (NullReferenceException ex) { } //normal to get a null here
            catch (Exception ex) 
            { 
                Program.LogException(ex, false); 
            }
        }

        /// <summary>
        /// Disconnects Citrix Sessions
        /// </summary>
        private void DisconnectCitrixSessionsIfNecessary()
        {
            try
            {
                Process[] pname; //MessageBox.Show("ica Session Ct: " + ica.GetSessionCount().ToString() + "\n " + userContext.getClientDevice().getClientName());            
                if (IsCitrixOpen())
                {
                    try
                    {
                        if (CurrentUser != null)
                            ApplicationLauncher.TerminalServicesDisconnect(ica, CurrentUser, LogEntry: false);
                    }
                    catch { }
                    try
                    {
                        userContext.disconnectClientSessions();
                    }
                    catch { }

                    pname = GetWFICAProcess();
                    foreach (var proc in pname)
                    {
                        proc.WaitForExit(5000); //wait 5 sec                   
                        proc.CloseMainWindow();
                    }

                    Thread.Sleep(150);
                    pname = GetWFICAProcess();  //confirm citrix closed. If sub windows are open the request to close the main window doesnt always work
                    foreach (var proc in pname)
                        proc.Kill();
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        /// <summary>
        /// Determines if a Citrix session is open
        /// </summary>
        private bool IsCitrixOpen()
        {
            Process[] pname = GetWFICAProcess();
            if (pname.Length > 0)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Gets all processes associates with the ICA client
        /// </summary>
        private Process[] GetWFICAProcess()
        {
            return Process.GetProcessesByName("wfica32");
        }
        #endregion

        #region EWS Notifier

        /// <summary>
        /// Method to start "new" EWS notifications 
        /// </summary>
        /// <param name="currentUser"></param>
        internal void StartNotifications(UserInfo currentUser)
        {
            //TO DO add profile check if checkemail selected...
            try
            {
                ExchangeService exService = QLUtility.CreateEWS(currentUser);
                StreamingSubscription subscription = exService.SubscribeToStreamingNotifications(new FolderId[] { WellKnownFolderName.Inbox }, EventType.NewMail);

                StreamingSubscriptionConnection connection = new StreamingSubscriptionConnection(exService, 30);
                connection.AddSubscription(subscription);
                connection.OnNotificationEvent +=
                    new StreamingSubscriptionConnection.NotificationEventDelegate(OnEvent);
                connection.OnSubscriptionError +=
                    new StreamingSubscriptionConnection.SubscriptionErrorDelegate(OnError);
                connection.OnDisconnect +=
                    new StreamingSubscriptionConnection.SubscriptionErrorDelegate(OnDisconnect);
                connection.Open();
            }
            catch(Exception ex)
            {
               // Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }

        }

        private void OnDisconnect(object sender, SubscriptionErrorEventArgs args)
        {
            //refresh connection
            try
            {
                StreamingSubscriptionConnection connection = (StreamingSubscriptionConnection)sender;

                if (!connection.IsOpen)
                {
                    connection.Open();
                }
            }
            catch (Exception ex)
            {
                //Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void OnError(object sender, SubscriptionErrorEventArgs args)
        {
            try
            {
                Exception e = args.Exception;
                Truncus.LogEvents("1", "EWS Subscription Error", globalUser.UserName, 0, "ERROR: " + e.Message);
            }
            catch(Exception ex)
            { }
        }

        private void OnEvent(object sender, NotificationEventArgs args)
        {
            try
            {
                StreamingSubscription subscription = args.Subscription;

            // Loop through all item-related events. 
            foreach (NotificationEvent notification in args.Events)
            {

                switch (notification.EventType)
                {
                    case EventType.NewMail:
                        //Console.WriteLine("\n-------------Mail created:-------------");
                        UpdateToolbar();
                        break;
                    //case EventType.Created:
                    //    Console.WriteLine("\n-------------Item or folder created:-------------");
                    //    break;
                    //case EventType.Deleted:
                    //    Console.WriteLine("\n-------------Item or folder deleted:-------------");
                    //    break;
                }
                // Display the notification identifier. 
                //if (notification is ItemEvent)
                //{
                //    // The NotificationEvent for an email message is an ItemEvent. 
                //    ItemEvent itemEvent = (ItemEvent)notification;
                //    Console.WriteLine("\nItemId: " + itemEvent.ItemId.UniqueId);
                //}
                //else
                //{
                //    // The NotificationEvent for a folder is a FolderEvent. 
                //    FolderEvent folderEvent = (FolderEvent)notification;
                //    Console.WriteLine("\nFolderId: " + folderEvent.FolderId.UniqueId);
                //}
            }
            }
            catch (Exception ex)
            {
               // Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void UpdateToolbar()
        {
            try
            {
                foreach (var pnl in this.pnlFlowContainer.Controls.OfType<Panel>())
                {
                    foreach (var lbl in pnl.Controls.OfType<Label>())
                    {
                        if (lbl.Text.ToLower().Contains("email"))
                        {
                            // if (!string.IsNullOrEmpty(count.ToString()))
                            // {
                            //lbl.Text = "Email: " + count.ToString();
                            lbl.Font = new System.Drawing.Font(lbl.Font.FontFamily, lbl.Font.Size, System.Drawing.FontStyle.Bold);
                            // lbl.ForeColor = System.Drawing.Color.Green;

                            // }
                            // else
                            // {
                            //    lbl.Text = "Email";
                            //    lbl.Font = new System.Drawing.Font(lbl.Font.FontFamily, lbl.Font.Size, System.Drawing.FontStyle.Regular);
                            // }

                            break;
                        }
                    }

                    foreach (var fish in pnl.Controls.OfType<FishMenuItem>())
                    {
                        if (fish.link != null)
                        {
                            if (fish.link == "http://webmail.bmcjax.com/")
                            {
                                //  if (count > 0)
                                //  {
                                System.Drawing.Bitmap bitMap = global::QuickLaunch.Properties.Resources.citrix_email_new;
                                fish.Image = bitMap;
                                break;
                                //  }
                                //  else
                                //  {
                                //     System.Drawing.Bitmap bitMap = global::QuickLaunch.Properties.Resources.email;
                                //     fish.Image = bitMap;
                                //     break;
                                // }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }


        /// <summary>
        /// Updates QL toolbar with count of current unread emails
        /// </summary>
        /// <param name="currentUser"></param>
        private void CheckEmailCount(UserInfo currentUser)
        {
            int count = QLUtility.GetEmailCount(currentUser);
            
            foreach (var pnl in this.pnlFlowContainer.Controls.OfType<Panel>())
            {
                foreach (var lbl in pnl.Controls.OfType<Label>())
                {
                    if (lbl.Text.ToLower().Contains("email"))
                    {
                        if (!string.IsNullOrEmpty(count.ToString()))
                        {
                            lbl.Text = "Email: " + count.ToString();
                            lbl.Font = new System.Drawing.Font(lbl.Font.FontFamily, lbl.Font.Size, System.Drawing.FontStyle.Bold);
                        }
                        else
                        {
                            lbl.Text = "Email";
                            lbl.Font = new System.Drawing.Font(lbl.Font.FontFamily, lbl.Font.Size, System.Drawing.FontStyle.Regular);
                        }

                        break;
                    }
                }

                foreach (var fish in pnl.Controls.OfType<FishMenuItem>())
                {
                    if (fish.link != null)
                    {
                        if (fish.link == "http://webmail.bmcjax.com/")
                        {
                            if (count > 0)
                            {
                                System.Drawing.Bitmap bitMap = global::QuickLaunch.Properties.Resources.citrix_email_new;
                                fish.Image = bitMap;
                                break;
                            }
                            else
                            {
                                System.Drawing.Bitmap bitMap = global::QuickLaunch.Properties.Resources.email;
                                fish.Image = bitMap;
                                break;
                            }
                        }
                    }
                }
            }
        }

        #endregion
    }

    public class AutoClosingForm
    {
        System.Threading.Timer _timeoutTimer;
        string _caption;

        AutoClosingForm(string text, string caption, int timeout)
        {
            try
            {
                _caption = caption;
                _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                    null, timeout, System.Threading.Timeout.Infinite);
                DialogForm dForm = new DialogForm(text, caption);
                dForm.Show();
            }
            catch (Exception ex) { }
        }

        public static void Show(string text, string caption, int timeout)
        {
            try
            {
                new AutoClosingForm(text, caption, timeout);
            }
            catch (Exception ex) { }
        }

        void OnTimerElapsed(object state)
        {
            try
            {
                foreach (Form f in Application.OpenForms)
                {
                    if (f.GetType() == typeof(DialogForm))
                    {
                        f.Close();
                    }
                }
            }
            catch (Exception ex) { }
        }

        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }
}

