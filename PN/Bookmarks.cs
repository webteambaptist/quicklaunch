﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using QuickLaunch.Shared;
using System.Reflection;

namespace QuickLaunch
{
    public partial class Bookmarks : Form
    {
        #region Properties
        public string UserId { get; set; }
        List<Models.Bookmark> Items { get; set; }
        public static readonly int BookmarkLimit = 20;
        #endregion

        public Bookmarks()
        {
            try
            {
                InitializeComponent();
              //  this.Region = UIHelper.RoundEdges(this.Width, this.Height, 5);

                this.tvBookmarks.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.treeView_ItemDrag);
                this.tvBookmarks.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView_DragEnter);
                this.tvBookmarks.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeView_DragDrop);
                this.tvBookmarks.BeforeSelect += new TreeViewCancelEventHandler(treeView_BeforeSelect);
                this.tvBookmarks.AfterLabelEdit += new NodeLabelEditEventHandler(treeView_AfterLabelEdit);
                this.tvBookmarks.KeyDown += new KeyEventHandler(tvBookmarks_KeyDown);
                this.tvBookmarks.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(tvBookmarks_NodeMouseDoubleClick);
                this.FormClosing += new FormClosingEventHandler(Bookmarks_FormClosing);
                this.KeyDown += new KeyEventHandler(Bookmarks_KeyDown);
                this.lblDropFiles.DragEnter += new DragEventHandler(lblDropFiles_DragEnter);
                this.lblDropFiles.DragLeave += new EventHandler(lblDropFiles_DragLeave);
                this.lblDropFiles.DragDrop += new DragEventHandler(lblDropFiles_DragDrop);

                UserId = QuickLaunch.CurrentUserId;
                lblTitle.Text = "Manage Bookmarks - " + UserId;

                PrepareForm();
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }            

        #region Methods
        public void PrepareForm()
        {
            try
            {
                Items = Models.Bookmark.selBookmarkByUserId(UserId);                
                foreach (Models.Bookmark b in Items)
                {
                    AddTreeNode(b);
                }
            }
            catch (Exception ex) { MessageBox.Show("There was an error loading your bookmarks. This is likely due to lack of network availability. Please try again later."); Program.LogException(ex, false); this.Close(); }            
        }
        private void AddTreeNode(Models.Bookmark f)
        {
            try
            {
                ContextMenu cm = new ContextMenu(new MenuItem[] { new MenuItem("Modify", miModify_Click), new MenuItem("Open", miView_Click), new MenuItem("Delete", miDelete_Click) });
                cm.Tag = f.id;
                TreeNode tn = new TreeNode(f.Title);
                tn.Tag = f.id;
                tn.ToolTipText = f.Url;
                tn.ContextMenu = cm;
                tn.Name = f.id.ToString();
                tvBookmarks.Nodes[0].Nodes.Add(tn);
                tvBookmarks.Nodes[0].Expand();
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }           
        }
        private void ModifyNode(object tag)
        {
            try
            {
                pnlModify.Tag = tag.ToString();
                Models.Bookmark b = Items.Find(i => i.id == Convert.ToInt32(tag.ToString()));
                if (b != null)
                {
                    tTitle.Text = b.Title;
                    tUrl.Text = b.Url;
                    pnlModify.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private bool IsOverBookmarkLimit()
        {
            try
            {
                return Items.FindAll(i => i.IsArchived != true).Count >= BookmarkLimit;
            }
            catch { return false; }
        }
        #endregion

        #region Form Events
        void Bookmarks_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                foreach (TreeNode tn in tvBookmarks.Nodes[0].Nodes)
                {
                    try
                    {
                        Items.Find(i => i.id == Convert.ToInt32(tn.Tag.ToString())).OrderId = tn.Index + 1;
                    }
                    catch { }
                }
                Models.Bookmark.delBookmark(Items.FindAll(i => i.IsArchived == true)); //deletes
                Models.Bookmark.insBookmark(Items.FindAll(i => i.id < 0)); //adds
                Models.Bookmark.updBookmark(Items.FindAll(i => i.id > 0)); //modifys
            }
            catch (Exception ex) { MessageBox.Show("There was an error saving your bookmarks. Please try again later."); Program.LogException(ex, false); }
            finally
            {
                ((QuickLaunch)this.Owner).LoadBookmarks();
                this.Dispose();
            }
        }
        void Bookmarks_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void bSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(UserId))
                {
                    if (pnlModify.Tag.ToString() == "-1")
                    {//Add item   
                        int id = -1 * DateTime.Now.Millisecond; //random negative #
                        Models.Bookmark b = new Models.Bookmark { id = id, Title = tTitle.Text, Url = tUrl.Text, NtId = UserId, OrderId = 0, IsArchived = false };
                        Items.Add(b);
                        AddTreeNode(b);
                    }
                    else
                    {
                        Models.Bookmark b = Items.Find(i => i.id == Convert.ToInt32(pnlModify.Tag.ToString()));
                        b.Title = tTitle.Text;
                        b.Url = tUrl.Text;

                        TreeNode[] tnc = tvBookmarks.Nodes[0].Nodes.Find(pnlModify.Tag.ToString(), false);
                        if (tnc.Length > 0)
                            tnc[0].Text = b.Title;
                    }
                    pnlModify.Visible = false;
                }
            }
            catch (Exception ex) { MessageBox.Show("There was an error saving your bookmark. Please try again later."); Program.LogException(ex, false); }
            finally { }
        }

        private void bCancel_Click(object sender, EventArgs e)
        {
            try
            {
                pnlModify.Visible = false;
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void lAddbookmark_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsOverBookmarkLimit()) { MessageBox.Show(string.Format("Sorry, you may only have up to {0} bookmarks", BookmarkLimit)); return; }
                pnlModify.Tag = "-1";
                tTitle.Text = string.Empty;
                tUrl.Text = "http://";
                pnlModify.Visible = true;
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void pbClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void pbBookmarkHelp_Click(object sender, EventArgs e)
        {
            try
            {
                QuickLaunch ql = this.Owner as QuickLaunch;
              //  ql.OpenHelp("160");
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        #endregion        

        #region Right Click Menu Events
        private void miModify_Click(object sender, EventArgs e)
        {
            try
            {
                MenuItem mi = sender as MenuItem;
                ModifyNode(mi.Parent.Tag.ToString());
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void miView_Click(object sender, EventArgs e)
        {
            try
            {
                MenuItem mi = sender as MenuItem;
                try
                {
                    System.Diagnostics.Process.Start(Items.Find(i => i.id == Convert.ToInt32(mi.Parent.Tag.ToString())).Url);
                }
                catch
                {
                    MessageBox.Show("Unable to open link");
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void miDelete_Click(object sender, EventArgs e)
        {
            try
            {
                MenuItem mi = sender as MenuItem;
                Items.Find(i => i.id == Convert.ToInt32(mi.Parent.Tag.ToString())).IsArchived = true;
                TreeNode[] tnc = tvBookmarks.Nodes[0].Nodes.Find(mi.Parent.Tag.ToString(), false);
                if (tnc.Length > 0)
                    tnc[0].Remove();                
            }
            catch (Exception ex) { Program.LogException(ex, false); }
        }
        #endregion

        #region Treeview Events
        private void treeView_ItemDrag(object sender, System.Windows.Forms.ItemDragEventArgs e)
        {
            try
            {
                DoDragDrop(e.Item, DragDropEffects.Move);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void treeView_DragEnter(object sender, System.Windows.Forms.DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent(DataFormats.UnicodeText))
                    e.Effect = DragDropEffects.All;
                else
                    e.Effect = DragDropEffects.Move;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void treeView1_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                Point targetPoint = tvBookmarks.PointToClient(new Point(e.X, e.Y));
                tvBookmarks.SelectedNode = tvBookmarks.GetNodeAt(targetPoint);
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void treeView_DragDrop(object sender, System.Windows.Forms.DragEventArgs e)
        {
            try
            {
                if (e.Data.GetDataPresent("System.Windows.Forms.TreeNode", false)) //make sure its a treenode
                {
                    Point pt = ((TreeView)sender).PointToClient(new Point(e.X, e.Y));
                    TreeNode DestinationNode = ((TreeView)sender).GetNodeAt(pt);
                    if (DestinationNode == null) return; //bad drag
                    TreeNode DraggedNode = (TreeNode)e.Data.GetData("System.Windows.Forms.TreeNode");
                    if (DestinationNode.Tag.ToString() != "0")
                    {
                        int index = DestinationNode.Index > 0 ? DestinationNode.Index + 1 : 0;
                        DestinationNode.Parent.Nodes.Insert(index, (TreeNode)DraggedNode.Clone());
                        DraggedNode.Remove();
                    }
                }
                else
                    lblDropFiles_DragDrop(sender, e); //see if it is a url being dragged
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void treeView_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            try
            {
                if (e.Node.Tag.ToString() == "0")
                    e.Cancel = true;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        void treeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            try
            {
                Models.Bookmark b = Items.Find(i => i.id == Convert.ToInt32(e.Node.Tag.ToString()));
                b.Title = e.Label;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        void tvBookmarks_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (tvBookmarks.SelectedNode == null) return;
                if (e.KeyCode == Keys.Delete)
                {
                    Items.Find(i => i.id == Convert.ToInt32(tvBookmarks.SelectedNode.Tag.ToString())).IsArchived = true;
                    tvBookmarks.Nodes.Remove(tvBookmarks.SelectedNode);                    
                }
                else if (e.KeyCode == Keys.Enter)
                {
                    pnlModify.Tag = tvBookmarks.SelectedNode.Tag.ToString();
                    Models.Bookmark b = Items.Find(i => i.id == Convert.ToInt32(tvBookmarks.SelectedNode.Tag.ToString()));
                    tTitle.Text = b.Title;
                    tUrl.Text = b.Url;
                    pnlModify.Visible = true;
                }
            }
            catch (Exception ex) { Program.LogException(ex, false); }
        }
        void tvBookmarks_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            try
            {
                ModifyNode(e.Node.Tag.ToString());
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        #endregion

        #region Drop File Events
        void lblDropFiles_DragEnter(object sender, DragEventArgs e)
        {
            try
            {
                if (IsOverBookmarkLimit())
                { lblDropFiles.BackColor = Color.Salmon; lblDropFiles.Text = string.Format("You may only have up to {0} bookmarks", BookmarkLimit); }
                else
                {
                    e.Effect = DragDropEffects.All;
                    lblDropFiles.BackColor = Color.Green;
                    lblDropFiles.ForeColor = Color.White;
                    lblDropFiles.Text = "Drop Text or Urls Here";
                }
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        void lblDropFiles_DragLeave(object sender, EventArgs e)
        {
            try
            {
                lblDropFiles.BackColor = Color.Honeydew;
                lblDropFiles.ForeColor = Color.Black;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        void lblDropFiles_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (IsOverBookmarkLimit()) { lblDropFiles.BackColor = Color.Salmon; lblDropFiles.Text = "Maximum bookmarks reached"; MessageBox.Show(string.Format("Sorry, you may only have up to {0} bookmarks", BookmarkLimit)); return; }
                lblDropFiles.Text = "Adding Bookmark...Please wait.";
                try
                {
                    string url = Shared.SysHelper.GetDraggedUrl(e);

                    if (!string.IsNullOrEmpty(url))
                    {
                        int id = -1 * DateTime.Now.Millisecond; //random negative #                    
                        Models.Bookmark b = new Models.Bookmark { id = id, Title = Shared.SysHelper.GetUrlTitle(url), Url = url, NtId = UserId, OrderId = 0, IsArchived = false };
                        Items.Add(b);
                        AddTreeNode(b);
                    }
                    else
                        MessageBox.Show("You have dropped an unsupported file type. Please try dragging url text, or the icon from the browser url.");
                }
                catch (Exception ex) { MessageBox.Show("There was an error adding your bookmark. Please try again later."); Program.LogException(ex, false); }
                lblDropFiles.Text = "Drop Text or Urls Here";
                lblDropFiles.BackColor = Color.Honeydew;
                lblDropFiles.ForeColor = Color.Black;
                pnlModify.Visible = false;
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        #endregion

    }
}
