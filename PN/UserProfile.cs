﻿using QuickLaunch.Shared;
using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace QuickLaunch
{
    public partial class UserProfile : Form
    {

        public string UserId { get; set; }
        public string CurrentHorizontalLaunchSetting { get; set; }
        public string CurrentVerticalLaunchSetting { get; set; }
        private readonly Color SelectedColor = Color.FromArgb(25, 80, 106);
        private readonly Color HoverColor = Color.FromArgb(61, 117, 142);
        private readonly Color DefaultColor = Color.FromArgb(191, 220, 184);
        
        
        public UserProfile(bool PowerChartEnabled, bool FirstNetEnabled, bool SurgiNetEnabled, bool AllScriptsEnabled, bool AnesEnabled)
        {
            try
            {
                InitializeComponent();
                this.KeyDown += new KeyEventHandler(UserProfile_KeyDown);
                //this.Region = UIHelper.RoundEdges(this.Width, this.Height, 5);
                UserId = QuickLaunch.CurrentUserId;
                lblTitle.Text = "My Profile - " + UserId;
                rbLaunchPC.Enabled = PowerChartEnabled;
                rbLaunchFN.Enabled = FirstNetEnabled;
                rbLaunchSN.Enabled = SurgiNetEnabled;
                rbLaunchAS.Enabled = AllScriptsEnabled;
                rbLaunchAN.Enabled = AnesEnabled;
                PrepareForm();
            }
            catch(Exception ex) 
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        void UserProfile_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.KeyCode == Keys.Escape)
                {
                    pbClose_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        public void PrepareForm()
        {
            try
            {
                Models.UserProfile up = Models.UserProfile.selUserProfileById(UserId);
                rbLaunchPC.Checked = rbLaunchPC.Enabled ? (up.CitrixAutoLaunchApp == rbLaunchPC.Tag.ToString() ? true : false) : false;
                rbLaunchFN.Checked = rbLaunchFN.Enabled ? (up.CitrixAutoLaunchApp == rbLaunchFN.Tag.ToString() ? true : false) : false;
                rbLaunchSN.Checked = rbLaunchSN.Enabled ? (up.CitrixAutoLaunchApp == rbLaunchSN.Tag.ToString() ? true : false) : false;
                rbLaunchAS.Checked = rbLaunchAS.Enabled ? (up.CitrixAutoLaunchApp == rbLaunchAS.Tag.ToString() ? true : false) : false;
                rbLaunchAN.Checked = rbLaunchAN.Enabled ? (up.CitrixAutoLaunchApp == rbLaunchAN.Tag.ToString() ? true : false) : false;
                rbNoLaunch.Checked = !rbLaunchPC.Checked && !rbLaunchFN.Checked && !rbLaunchSN.Checked && !rbLaunchAS.Checked && !rbLaunchAN.Checked;

                CurrentVerticalLaunchSetting = up.formVerticalStartup ?? "TOP";
                CurrentHorizontalLaunchSetting = up.formHorizontalStartup ?? "MID";
                SelectPanel(CurrentVerticalLaunchSetting + "_" + CurrentHorizontalLaunchSetting);

            }
            catch (Exception ex) { MessageBox.Show("There was an error loading your profile. This is likely due to lack of network availability. Please try again later."); Program.LogException(ex, false); this.Close(); }            
        }
        private void pbClose_Click(object sender, EventArgs e)
        {
            try
            {
                ((QuickLaunch)this.Owner).UpdateProfile_Changed();
                this.Close();
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void pnlLocationSetting_Click(object sender, EventArgs e)
        {
            try
            {
                Panel pnl = sender as Panel;
                string[] location = pnl.Tag.ToString().Split('_');
                CurrentVerticalLaunchSetting = location[0];
                CurrentHorizontalLaunchSetting = location[1];
                SelectPanel(pnl.Tag.ToString());
                ((QuickLaunch)this.Owner).PreviewMoveQLForm(CurrentVerticalLaunchSetting, CurrentHorizontalLaunchSetting);
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void pnlLocationSetting_OnMouseOver(object sender, EventArgs e)
        {
            try
            {
                Panel pnl = sender as Panel;
                if (!string.IsNullOrEmpty(CurrentVerticalLaunchSetting) && !string.IsNullOrEmpty(CurrentHorizontalLaunchSetting))
                    if (pnl.Tag.ToString() != CurrentVerticalLaunchSetting + "_" + CurrentHorizontalLaunchSetting)
                        pnl.BackColor = HoverColor;
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void pnlLocationSetting_OnMouseOut(object sender, EventArgs e)
        {
            try
            {
                Panel pnl = sender as Panel;
                if (!string.IsNullOrEmpty(CurrentVerticalLaunchSetting) && !string.IsNullOrEmpty(CurrentHorizontalLaunchSetting))
                {
                    if (pnl.Tag.ToString() == CurrentVerticalLaunchSetting + "_" + CurrentHorizontalLaunchSetting)
                    {
                        pnl.BackColor = SelectedColor;
                        return;
                    }
                }
                pnl.BackColor = DefaultColor;
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }
        private void SelectPanel(string value)
        {
            try
            {
                //selected color
                pnlTopLeft.BackColor = pnlTopLeft.Tag.ToString() == value ? SelectedColor : DefaultColor;
                pnlTopCenter.BackColor = pnlTopCenter.Tag.ToString() == value ? SelectedColor : DefaultColor;
                pnlTopRight.BackColor = pnlTopRight.Tag.ToString() == value ? SelectedColor : DefaultColor;
                pnlBottomLeft.BackColor = pnlBottomLeft.Tag.ToString() == value ? SelectedColor : DefaultColor;
                pnlBottomCenter.BackColor = pnlBottomCenter.Tag.ToString() == value ? SelectedColor : DefaultColor;
                pnlBottomRight.BackColor = pnlBottomRight.Tag.ToString() == value ? SelectedColor : DefaultColor;

                pnlTopLeft.BackgroundImage = pnlTopLeft.Tag.ToString() == value ? global::QuickLaunch.Properties.Resources.QLPreview : null;
                pnlTopCenter.BackgroundImage = pnlTopCenter.Tag.ToString() == value ? global::QuickLaunch.Properties.Resources.QLPreview : null;
                pnlTopRight.BackgroundImage = pnlTopRight.Tag.ToString() == value ? global::QuickLaunch.Properties.Resources.QLPreview : null;
                pnlBottomLeft.BackgroundImage = pnlBottomLeft.Tag.ToString() == value ? global::QuickLaunch.Properties.Resources.QLPreview : null;
                pnlBottomCenter.BackgroundImage = pnlBottomCenter.Tag.ToString() == value ? global::QuickLaunch.Properties.Resources.QLPreview : null;
                pnlBottomRight.BackgroundImage = pnlBottomRight.Tag.ToString() == value ? global::QuickLaunch.Properties.Resources.QLPreview : null;
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void pbHelp_Click(object sender, EventArgs e)
        {
            try
            {
                QuickLaunch ql = this.Owner as QuickLaunch;
               // ql.OpenHelp("148");
            }
            catch (Exception ee)
            {
                Truncus.LogErrorEvents(ee.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        private void bOk_Click(object sender, EventArgs e)
        {
            try
            {
                string CitrixAutoLaunchApp = null;

                if (rbLaunchPC.Checked)
                {
                    CitrixAutoLaunchApp = rbLaunchPC.Tag.ToString();
                }

                if (rbLaunchFN.Checked)
                {
                    CitrixAutoLaunchApp = rbLaunchFN.Tag.ToString();
                }

                if (rbLaunchSN.Checked)
                {
                    CitrixAutoLaunchApp = rbLaunchSN.Tag.ToString();
                }

                if (rbLaunchAS.Checked)
                {
                    CitrixAutoLaunchApp = rbLaunchAS.Tag.ToString();
                }

                if (rbLaunchAN.Checked)
                {
                    CitrixAutoLaunchApp = rbLaunchAN.Tag.ToString();
                }

                if (rbNoLaunch.Checked)
                {
                    CitrixAutoLaunchApp = "NONE";
                }

                if ((!string.IsNullOrEmpty(UserId)) && (!string.IsNullOrEmpty(CitrixAutoLaunchApp)))
                {
                    Models.UserProfile.updUserProfileSettings(new Models.UserProfile { ntID = UserId, CitrixAutoLaunchApp = CitrixAutoLaunchApp, formHorizontalStartup = CurrentHorizontalLaunchSetting, formVerticalStartup = CurrentVerticalLaunchSetting });
                    ((QuickLaunch)this.Owner).UpdateProfile_Changed();
                }
            }
            catch (Exception ex) { MessageBox.Show("There was an error saving your profile. This is likely due to lack of network availability. Please try again later."); Program.LogException(ex, false); }
            finally { this.Close(); } 
        }

    }
}
