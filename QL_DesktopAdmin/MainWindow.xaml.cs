﻿using QuickLaunch.Models;
using QuickLaunch.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace QL_DesktopAdmin
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static readonly string PHash = "9F579CB7-2EB3-4D2A-BA12-69E90ACB6AAF";
        static readonly string SaltKey = "03690DC9-C7DB-45FE-ABCC-F58F13FCACE5";
        static readonly string VIKey = "TI$2pIjHR$1pIa14";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnCache_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtMachine.Text))
            {
                string names = txtMachine.Text;
                string[] values = names.Split(',').Select(sValue => sValue.Trim()).ToArray();
                lblResult.Content = "";
                txtResult.Text = "";
                foreach (string s in values)
                {
                    machine m = new machine();
                    m.pcname = s;
                    txtResult.Text = txtResult.Text + ", " + FindandClearCache(m);
                }
            }
        }

        private string FindandClearCache(machine m)
        {
            string result = "";

            try
            {

                var addresslist = Dns.GetHostAddresses(m.pcname + ".bh.local");
                // var addresslist = Dns.GetHostAddresses("5vmmng1.bh.local");
                NetTcpBinding binding = new NetTcpBinding();
                // string address = "net.tcp://" + System.Environment.MachineName + ".BH.LOCAL:8000/wcfserver";
                IPAddress ipa = (from i in addresslist where i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork select i).First();
                string address = "net.tcp://" + ipa.ToString() + ":8001/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());

                //client

                EndpointAddress endAddress = new EndpointAddress(baseAddress.ToString().ToLower());
                ChannelFactory<IWCFServer> channelFactory =
                    new ChannelFactory<IWCFServer>(binding, endAddress);
                IWCFServer server = channelFactory.CreateChannel();

                server.ClearCache();
                result = "OK: " + m.pcname + " ";
            }
            catch (Exception ee)
            {
                //result = ee.Message;
                result = "ERR: " + m.pcname + " ";
                try
                {
                    Task.Factory.StartNew(() => deleteCache(m));
                }
                catch(Exception e)
                {
                    XMLEntry xe = new XMLEntry();
                    xe.machine = m.pcname;
                    writeXMLEntry(xe);
                }
               
                
            }

            return result;
        }

        /// <summary>
        /// the nuclear option, clears all cache on all machines that are listening
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCache_Copy_Click(object sender, RoutedEventArgs e)
        {
            txtMachine.Text ="Starting...";
            List<machine> machines = getMachines();
            WipeCache(machines);
        }

        private void WipeCache(List<machine> machines)
        {
            int counter = 0;

            Parallel.ForEach<machine>(machines, Machine => FindandClearCache(Machine));
           

            int cout = counter;
        }

        private static void deleteCache(machine m)
        {
            //\\1HKSA37077\c$\Documents and Settings\WXPL0305\Local Settings\Temp
            string win7cache = "\\\\" + m.pcname + "\\c$\\Users\\" + m.lsid + "\\AppData\\Local\\Temp";
            string winXPcache = "\\\\" + m.pcname + "\\c$\\Documents and Settings\\" + m.lsid + "\\Local Settings\\Temp";
            try
            {
                if (Directory.Exists(winXPcache))
                {
                    foreach (string f in Directory.GetFiles(winXPcache, "QL_*.xml"))
                    {
                        File.Delete(f);
                    }
                }
                else if (Directory.Exists(win7cache))
                {
                    foreach (string f in Directory.GetFiles(win7cache, "QL_*.xml"))
                    {
                        File.Delete(f);
                    }
                }

            }
            catch (IOException ie)
            {
                XMLEntry xe = new XMLEntry();
                xe.machine = m.pcname;
                writeXMLEntry(xe);
            }
        }

        private static List<machine> getMachines()
        {
            List<machine> machines = new List<machine>();
            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("select pcname, lsid from tito.dbo.sso_machineinventory where qlversion like \'2.8.%\' order by pcname desc", sqlConnection);
                    //SqlCommand sqlCommand = new SqlCommand("SELECT pcname, lsid FROM [TITO].[dbo].[SSO_ErrorLog] where exception like (\'%ICA file could not be downloaded%\') and lup_dt > \'2015-03-24 10:25\'", sqlConnection);
                    sqlCommand.CommandType = CommandType.Text;
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        machine m = new machine();
                        m.pcname = sqlDataReader[0].ToString();
                        string lsid = sqlDataReader[1].ToString();
                        if (lsid.StartsWith("bh", true, null))
                        {
                            try
                            {
                                m.lsid = lsid.Substring(3);
                            }
                            catch (Exception ee)
                            { }
                        }
                        machines.Add(m);
                    }
                    sqlDataReader.Close();
                    //machine n = new machine();
                    //n.pcname = "5VMMNG1";
                    //n.lsid = "CXER0104";
                    //machines.Insert(0, n);
                }
                catch (SqlException sqlException)
                {
                }
                catch (Exception exception)
                {
                }

            }
                return machines;
            }

        public class machine
        {
            public string pcname { get; set; }
            public string lsid { get; set; }
        }

        private static void writeXMLEntry(XMLEntry xmlEntry)
        {
            try
            {
                string logFileName = "c:\\temp\\fullLog" + DateTime.Now.ToString("yyyy-MM-dd") + ".xml";

                if (File.Exists(logFileName))
                {
                        XDocument xmlDoc = XDocument.Load(logFileName);
                        xmlDoc.Element("Entries").Add(
                            new XElement("Entry",
                                    new XElement("Machine", xmlEntry.machine)
                                    )
                                );
                        xmlDoc.Save(logFileName);
                    
                }
                else
                {
                    XDocument xmlDoc = new XDocument(
                        new XDeclaration("1.0", "utf-16", "true"),
                        new XComment("Quick Launch Log"),
                        new XElement("Entries",
                            new XElement("Entry",
                                    new XElement("Machine", xmlEntry.machine))
                                    )
                            );
                    xmlDoc.Save(logFileName);
                }
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee.Message);
                // EventLog.WriteEntry("Quick Launch", "Log Write Failed" + ee.Message, EventLogEntryType.Information);

            }
        }

        public sealed class XMLEntry : IDataInfo
        {
            public XMLEntry() { }
            public string machine { get; set; }
            public string LSID { get; set; }
        }

        public interface IDataInfo
        { }

        private void btnDecrypt_Click(object sender, RoutedEventArgs e)
        {
            txtResult.Text = "";
            txtResult.Text = Decrypt(txtCrypto.Text.Trim());
        }

        private void btnEncrypt_Click(object sender, RoutedEventArgs e)
        {
            txtResult.Text = "";
            txtResult.Text = Encrypt(txtCrypto.Text.Trim());
        }

        public static string Encrypt(string pTxt)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(pTxt);

            byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static string Decrypt(string encTxt)
        {
            try
            {
                byte[] cipherTextBytes = Convert.FromBase64String(encTxt);
                byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
                var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

                var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
                var memoryStream = new MemoryStream(cipherTextBytes);
                var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
                byte[] plainTextBytes = new byte[cipherTextBytes.Length];

                int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
                memoryStream.Close();
                cryptoStream.Close();
                return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
            }
            catch (Exception ex)
            {
                return ("You probably left the double quotes on and broke it, dummy, try it again WITHOUT quotes");
            }
        }

        private void btnAlert_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                var addresslist = Dns.GetHostAddresses("HK5YMX1-WIN7" + ".bh.local");
                // var addresslist = Dns.GetHostAddresses("5vmmng1.bh.local");
                NetTcpBinding binding = new NetTcpBinding();
                // string address = "net.tcp://" + System.Environment.MachineName + ".BH.LOCAL:8000/wcfserver";
                IPAddress ipa = (from i in addresslist where (i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) && (!i.ToString().Contains("192"))  select i).First();
                string address = "net.tcp://" + ipa.ToString() + ":8989/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());

                //client

                EndpointAddress endAddress = new EndpointAddress(baseAddress.ToString().ToLower());
                ChannelFactory<IWCFService> channelFactory =
                    new ChannelFactory<IWCFService>(binding, endAddress);
                IWCFService server = channelFactory.CreateChannel();

                SystemAlert sa = new SystemAlert();
                server.ReceiveAlert(sa);
              
            }
            catch (Exception ee)
            {
                //result = ee.Message;
               
                try
                {
                    //Task.Factory.StartNew(() => deleteCache(m));
                }
                catch (Exception eee)
                {
                    //XMLEntry xe = new XMLEntry();
                    //xe.machine = m.pcname;
                    //writeXMLEntry(xe);
                }


            }
        }

        private void btnLog_Click(object sender, RoutedEventArgs e)
        {
            string pcName = txtMachine.Text;

            try
            {

                var addresslist = Dns.GetHostAddresses(pcName + ".bh.local");
                // var addresslist = Dns.GetHostAddresses("5vmmng1.bh.local");
                NetTcpBinding binding = new NetTcpBinding();
                // string address = "net.tcp://" + System.Environment.MachineName + ".BH.LOCAL:8000/wcfserver";
                IPAddress ipa = (from i in addresslist where i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork select i).First();
                string address = "net.tcp://" + ipa.ToString() + ":8001/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());

                //client

                EndpointAddress endAddress = new EndpointAddress(baseAddress.ToString().ToLower());
                ChannelFactory<IWCFServer> channelFactory =
                    new ChannelFactory<IWCFServer>(binding, endAddress);
                IWCFServer server = channelFactory.CreateChannel();

                server.ClearLog(1);
                txtResult.Text = txtResult.Text + ",OK: " + pcName + " ";
            }
            catch (Exception ee)
            {

                txtResult.Text = txtResult.Text + ",ERR: " + pcName + " ";
            }
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            txtCrypto.Text = "";
            txtResult.Text = "";
        }

        private void btnSendMsg_Click(object sender, RoutedEventArgs e)
        {
            string pcName = txtMachine.Text;

            try
            {

                var addresslist = Dns.GetHostAddresses(pcName + ".bh.local");
                // var addresslist = Dns.GetHostAddresses("5vmmng1.bh.local");
                NetTcpBinding binding = new NetTcpBinding();
                // string address = "net.tcp://" + System.Environment.MachineName + ".BH.LOCAL:8000/wcfserver";
                IPAddress ipa = (from i in addresslist where i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork select i).First();
                string address = "net.tcp://" + ipa.ToString() + ":8001/wcfserver";
                Uri baseAddress = new Uri(address.ToLower());

                //client

                EndpointAddress endAddress = new EndpointAddress(baseAddress.ToString().ToLower());
                ChannelFactory<IWCFServer> channelFactory =
                    new ChannelFactory<IWCFServer>(binding, endAddress);
                IWCFServer server = channelFactory.CreateChannel();

                server.SendMessage(txtMsg.Text, "TEST", 5000);
                txtResult.Text = txtResult.Text + ",OK: " + pcName + " ";
            }
            catch (Exception ee)
            {

                txtResult.Text = txtResult.Text + ",ERR: " + pcName + " ";
            }
        }

        private void btnUpload_Click(object sender, RoutedEventArgs e)
        {

            try {
                FileStream stream = new FileStream(txtFileName.Text.Trim() + txtName.Text.Trim(), FileMode.Open, FileAccess.Read);
                BinaryReader bReader = new BinaryReader(stream);
                byte[] imageArray = bReader.ReadBytes((int)stream.Length);

                bReader.Close();
                stream.Close();

                bool success = saveFile(imageArray);

                if (success)
                {
                   // txtFileName.Text = "";
                    txtName.Text = "";
                }
                else
                {
                    txtFileName.Text = "ERROR";
                    txtName.Text = "";
                }
            }
            catch(Exception ee)
            {
                txtFileName.Text = "ERROR";
            }

        }

        private bool saveFile(byte[] imageArray)
        {
            bool success = true;

            using (SqlConnection sqlConnection = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand("dbo.insMenuImage", sqlConnection);
                    sqlCommand.CommandType = CommandType.StoredProcedure;
                    sqlCommand.Parameters.AddWithValue("ImgFile", imageArray);
                    sqlCommand.Parameters.AddWithValue("ImgName", txtName.Text.Trim());
                    sqlCommand.Parameters.AddWithValue("UpdateDT", DateTime.Now.ToString());
                    sqlCommand.Parameters.AddWithValue("UpdateUser", "Admin");
                    sqlCommand.ExecuteNonQuery();

                }
                catch (SqlException sqlException)
                {
                    success = false;
                }
                catch (Exception exception)
                {
                    success = false;
                }

            }

            return success;
        }

        private void btnAsyncEvt_Click(object sender, RoutedEventArgs e)
        {
           // Truncus.ProcessEventLogs();
        }

        private void btnAsyncErr_Click(object sender, RoutedEventArgs e)
        {
            //Truncus.ProcessErrorLogs();
        }

        private void btnDownload_Copy_Click(object sender, RoutedEventArgs e)
        {
            Bitmap _bitmap;

            //check resource folder
            try
            {
                string path = @"C:\Program Files (x86)\Baptist Health\Quick Launch\images\";

                if (Directory.Exists(@"C:\Program Files (x86)\Baptist Health\Quick Launch\"))  //is OS 64 bit?
                {
                    if (!Directory.Exists(@"C:\Program Files (x86)\Baptist Health\Quick Launch\images"))
                    {
                        Directory.CreateDirectory(@"C:\Program Files (x86)\Baptist Health\Quick Launch\images");
                    }

                }
                else
                {
                    path = @"C:\Program Files\Baptist Health\Quick Launch\images\";

                    if (!Directory.Exists(@"C:\Program Files\Baptist Health\Quick Launch\images"))
                    {
                        Directory.CreateDirectory(@"C:\Program Files\Baptist Health\Quick Launch\images");
                    }
                }

                string[] fileList = Directory.GetFiles(path, txtFileSave.Text, SearchOption.AllDirectories);

                if (fileList.Length > 0)
                {
                    System.Drawing.Image _image = System.Drawing.Image.FromFile(fileList[0]);
                    _bitmap = (Bitmap)_image;
                }
                else  //if not in resource folder, check DB
                {
                    MenuImage menuImage = new MenuImage();
                    menuImage = MenuImage.getFile(txtFileSave.Text);

                    if (menuImage.ImgFile != null)
                    {
                        File.WriteAllBytes(path + menuImage.ImgName, menuImage.ImgFile);
                    }
                }


            }
            catch (Exception ee)
            {

            }
        }

        private void btnICA_Click(object sender, RoutedEventArgs e)
        {
            txtICA.Text = Machine.GetIcaVersion();
        }

        private void btnHelp_Click(object sender, RoutedEventArgs e)
        {

        }

        private void btnSvc_Click(object sender, RoutedEventArgs e)
        {
            //QuickLaunch.ServiceForm sForm = new QuickLaunch.ServiceForm();
          //  lblresult.Content = sForm.createTicketHarness(txtName1.Text, txtSeri.Text, txtPhon.Text, txtNTID.Text, txtLSID.Text, txtMsg1.Text, txtEmal.Text);
        }

        private void btnGCIP_Click(object sender, RoutedEventArgs e)
        {
            txtGCIP.Text = SysHelper.GetClientIP("");
        }

        private void btnGCN_Click(object sender, RoutedEventArgs e)
        {
            txtGCN.Text = SysHelper.GetClientName("");
        }

        private void btnUMI_Click(object sender, RoutedEventArgs e)
        {
            QuickLaunch.Models.Machine.insMachine(new QuickLaunch.Models.Machine
            {
                PCName = "VCLIN04-076",
                LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper(),
                QLVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                ICAVersion = QuickLaunch.Models.Machine.GetIcaVersion(),
                IEVersion = QuickLaunch.Models.Machine.GetIEVersion(),
                ComputerInfo = "",
                OSInfo = "",
                Processes = "",
                Memory = "",
                IpAddress = SysHelper.GetIpAddress(),
                AutoInstallVersion = "",
                LastLogin = DateTime.Now
            });

        }

        private void btnUpload_Copy_Click(object sender, RoutedEventArgs e)
        {
            txtGCIP.Text = Utility.getPath();
        }

        private void btnUpload_Copy1_Click(object sender, RoutedEventArgs e)
        {
            UserInfo uInfo = new UserInfo();
            uInfo.UserName = "sbart007";
           // QLUtility.LSIDCheck(uInfo);
        }

        private void btnharne_Click(object sender, RoutedEventArgs e)
        {
            WHarness harn = new WHarness();
            harn.Show();
        }
    }
    public class SystemAlert
    {


        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }


        /// <summary>
        /// Gets/Sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public string Code { get; set; }

        
        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public string CodeDescription { get; set; }

        
        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public int CodeId { get; set; }

        
        /// <summary>
        /// Gets/Sets the msg
        /// </summary>
        public string Message { get; set; }

        
        /// <summary>
        /// Gets/Sets the Application
        /// </summary>
        public string Application { get; set; }

        
        /// <summary>
        /// Gets/Sets the Attachment Link
        /// </summary>
        public string AttachmentLink { get; set; }

        
        /// <summary>
        /// Gets/Sets the Icon
        /// </summary>
        public string Icon { get; set; }

        
        /// <summary>
        /// Gets/Sets the Font color
        /// </summary>
        public string FontColor { get; set; }

        
        /// <summary>
        /// Gets/Sets the font color
        /// </summary>
        public string FontColorTicker { get; set; }

        
        /// <summary>
        /// Gets/Sets the font color
        /// </summary>
        public string FontColorToast { get; set; }

        
        /// <summary>
        /// Gets/Sets the font color
        /// </summary>
        public string FontColorDialog { get; set; }

        
        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColor { get; set; }

        
        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColorTicker { get; set; }

        
        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColorToast { get; set; }

        
        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColorDialog { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is to be used by the net scaler
        /// </summary>
        public bool IsNetScalerVisible { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is to be used by the web sharepoint physician portal
        /// </summary>
        public bool IsPhysicianPortalVisible { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is to be used by the web sharepoint pfcs portal
        /// </summary>
        public bool IsPFCSVisible { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is to be used by the bhthin portal
        /// </summary>
        public bool IsBHTHINVisible { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is to be used by the CETech
        /// </summary>
        public bool IsCETechVisible { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is to be used by the intranet
        /// </summary>
        public bool IsIntranetVisible { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is to be used by the Novell enabled PC machines
        /// </summary>
        public bool IsPCVisible { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is shown as toast from task bar
        /// </summary>
        public bool IsToast { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is shown as Operating system bubble
        /// </summary>
        public bool IsOSBubble { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is shown as dialog
        /// </summary>
        public bool IsDialog { get; set; }

        
        /// <summary>
        /// Gets/Sets whether the alert is shown in the ticker marquee
        /// </summary>
        public bool IsTicker { get; set; }

        
        /// <summary>
        /// Gets/Sets the # of times an alert is shown
        /// </summary>
        public string Frequency { get; set; }


        
        /// <summary>
        /// Gets/Sets the start date time
        /// </summary>
        public DateTime StartDt { get; set; }


        
        /// <summary>
        /// Gets/Sets the start of the downtime date time
        /// </summary>
        public DateTime DowntimeDt { get; set; }


        
        /// <summary>
        /// Gets/Sets the end date time
        /// </summary>
        public DateTime EndDt { get; set; }


        
        /// <summary>
        /// Gets/Sets the disable key of type guid
        /// </summary>
        public Guid DisableKey { get; set; }


        
        /// <summary>
        /// Gets/Sets the enabled Quick Launch Citrix menu status
        /// </summary>
        public bool IsQLCitrixEnabled { get; set; }


        
        /// <summary>
        /// Gets/Sets the everywhere status
        /// </summary>
        public bool IsEverywhere { get; set; }


        
        /// <summary>
        /// Gets/Sets the enabled status
        /// </summary>
        public bool IsEnabled { get; set; }


        
        /// <summary>
        /// Gets/Sets the last update user
        /// </summary>
        public string lup_user { get; set; }


        
        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }


        
        /* Start User Fields */
        /// <summary>
        /// Gets/Sets the response existence 
        /// </summary>
        public bool ResponseExists { get; set; }

        
        /// <summary>
        /// Gets/Sets the response dialog confirmation 
        /// </summary>
        public bool ResponseToastConfirmation { get; set; }

        
        /// <summary>
        /// Gets/Sets the response dialog confirmation 
        /// </summary>
        public bool ResponseOSBubbleConfirmation { get; set; }

        
        /// <summary>
        /// Gets/Sets the response dialog confirmation 
        /// </summary>
        public bool ResponseDialogConfirmation { get; set; }

        
        /// <summary>
        /// Gets/Sets the response dialog confirmation 
        /// </summary>
        public DateTime ResponseLastUpdate { get; set; }

        
        /* Start User Fields */
        /// <summary>
        /// Gets/Sets the response count 
        /// </summary>
        public int ResponseCt { get; set; }


        public SystemAlert() { }

        public string createTicketHarness(string name, string serial, string phone, string userID, string LSID, string txtMsg, string email)
        {
            string result = "";

            try
            {
                string user = ConfigurationManager.AppSettings["esmUser"].ToString();
                string pass = ConfigurationManager.AppSettings["esmPass"].ToString();
                string acct = ConfigurationManager.AppSettings["esmAccount"].ToString();
                string guid = ConfigurationManager.AppSettings["esmGUID"].ToString();

                string userout = SysHelper.Decrypt(user);
                string passout = SysHelper.Decrypt(pass);
                bhpiiseva01v.WebService ws = new bhpiiseva01v.WebService();

                //to do encrypt user/pass, stick in config

                string ezvTask = ws.EZV_CreateRequest(acct, userout, passout, guid, "", "", "", serial, "3", "41", "", phone, "", email, name, "", "", "", "", "", "", email, name, "12", txtMsg, "", "", "", "", "");

                if (!ezvTask.ToUpper().Trim().StartsWith("I"))
                {
                    result = "Ticket #: EMAIL";
                }
                else
                {
                    result = "Ticket #: " + ezvTask;
                }

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return result;
        }

    }
}
