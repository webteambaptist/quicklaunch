﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace QL_DesktopAdmin
{

    [ServiceContract]
    interface IWCFServer
    {
        //[OperationContract]
        //void SendMessage(string user, string message);

        [OperationContract]
        void LockMachine(string user);

        [OperationContract]
        void ClearCache();

        [OperationContract]
        void ClearLog(double days);


        [OperationContract]
        void SendMessage(string message, string caption, int timeout);

    }

    [ServiceContract]
    interface IWCFService
    {

        [OperationContract]
        void ReceiveAlert(SystemAlert systemAlert); //passes alert for serialization

        [OperationContract]
        void DeleteAlert(string ID); //deletes alert with specified ID

        [OperationContract]
        void DeleteAllAlerts();

        [OperationContract]
        void StopAlert(string ID);

        [OperationContract]
        void StopAllAlerts();

        [OperationContract]
        void UpdateInterval(int interval);


    }
}
