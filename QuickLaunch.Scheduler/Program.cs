﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Reflection;

namespace QuickLaunch.Scheduler
{
    class Program
    {
        #region Properties
        public static readonly string ApplicationName = "SCHEDULER";
        #endregion

        static void Main(string[] args)
        {
            Console.WriteLine("Scheduler Started.");
            try
            {
                if (args.Count() > 0)
                {
                    switch (args[0].ToUpper())
                    {
                        case "SENDREPORTS":
                            SendScheduledReports();
                            break;                        
                        case "UPDATEWTSLOCATIONS":
                            UpdateWTSLocations();
                            break;
                        case "CLEANUSERS":
                            UserCleanUp();
                            break;
                        case "CLEANERRORS":
                            ErrorCleanUp();
                            break;
                        case "CLEANREPORTHISTORY":
                            ReportHistoryCleanUp();
                            break;
                        case "CLEANEVENTS":
                            EventCleanUp();
                            break;
                        case "CLEANMACHINES":
                            MachineCleanUp();
                            break;
                        default:
                            throw new Exception("Invalid Command!");
                    }
                }
                else
                { //do all    
                    SendScheduledReports();
                    ReportHistoryCleanUp();
                    UserCleanUp();
                    ErrorCleanUp();                    
                    MachineCleanUp();
                    EventCleanUp(); //want most up to date user and machine ct, so do after their cleanup
                    UpdateWTSLocations();
                }
            }
            catch (Exception ex) { Console.WriteLine("An error occurred."); LogException(ex, true); }
            Console.WriteLine("Scheduler Finished.");            
        }

        #region Operations
        private static void SendScheduledReports()
        {
            Console.WriteLine("Starting Reporting.");           
            Reporting reporting = new Reporting();
            reporting.FindReports();
            if (reporting.Reports.Count() > 0)
            {
                Console.WriteLine(" - Checking Schedule.");           
                reporting.CheckSchedule();
                Console.WriteLine(" - Sending Reports.");           
                reporting.SendReports();
            }
            Console.WriteLine(string.Format("{0} Scheduled Reports Sent.", reporting.Reports.Count().ToString()));
            reporting = null;
        }

        private static void MachineCleanUp()
        {
            Models.Machine.delExtinctMachines();
            Console.WriteLine("Extinct Machines Removed.");
        }

        private static void EventCleanUp()
        {
            List<Models.EventLog.DataWarehouse> lExtincting = Models.EventLog.selExtinctingEventLogs(); //aggregate soon to be extinct data
            if (lExtincting != null)
            {
                foreach (Models.EventLog.DataWarehouse dw in lExtincting)
                    Models.EventLog.insDataWarehouse(dw); //save aggregated data

                Models.EventLog.delExtinctEventLogs(); //remove event log
                Console.WriteLine("Extinct Events Removed.");
            }
        }

        private static void ReportHistoryCleanUp()
        {
            Models.Report.delExtinctReportHistory();
            Console.WriteLine("Extinct Report History Removed.");
        }

        private static void ErrorCleanUp()
        {
            Models.ErrorLog.delExtinctErrors();
            Console.WriteLine("Extinct Errors Removed.");
        }

        private static void UserCleanUp()
        {
            Models.UserProfile.delExtinctUserProfiles();
            Console.WriteLine("Extinct Users Removed.");
        }

        private static void UpdateWTSLocations()
        {
            Console.WriteLine("WTS Locations Update Started.");
            List<Models.Location.WTSLocation> wts = Models.Location.selMasterWTSLocations();
            foreach (Models.Location.WTSLocation loc in wts)            
                Models.Location.insSSOWTSLocation(loc);
            Console.WriteLine("WTS Locations Update Finished.");
        }
        #endregion

        /// <summary>
        /// Logs the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void LogException(Exception ex, bool IsFatal)
        {
            try {
            EventLog myLog = new EventLog();
            myLog.Source = "Quick Launch Scheduler v2";
            myLog.WriteEntry("Version " + Assembly.GetExecutingAssembly().GetName().Version + "\n\n" + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace, EventLogEntryType.Error);
            } catch(Exception exLog) {}
            
            if (DBManager.HasDBConnection())
                Models.ErrorLog.AddErrorToLog(ex, "N/A", ApplicationName, IsFatal);
                
        }
    }
}
