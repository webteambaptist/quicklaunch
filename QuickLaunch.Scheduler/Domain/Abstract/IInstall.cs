﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickLaunch.Scheduler.Abstract
{
    interface IInstall
    {
        void LoadComputersFromAD();
        void LoadComputersFromDB();        
        void SaveComputerInventory();
        void SendQuickLaunchMSI();
        //void GetQuickLaunchVersion();
        //void GetMSIInstall();
    }
}
