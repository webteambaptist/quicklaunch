﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickLaunch.Scheduler.Abstract
{
    interface IReporting
    {
        void FindReports();
        void SendReports();        
    }
}
