﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickLaunch.Scheduler
{
    class util
    {


        #region Inner Classes
        public class ThreadRange
        {
            public int StartIndex { get; set; }
            public int Count { get; set; }
            public int OriginalComputerCount { get; set; }
            public ThreadRange() { }
        }
        #endregion
        
        #region Old Program Startup
        //methods from program...saving for future use...maybe
        //private static void InstallQuickLaunch()
        //{
        //    Abstract.IInstall install = new Install();
        //    install.LoadComputersFromAD();
        //    install.SaveComputerInventory();
        //}

        //private static void UpdateInventory()
        //{
        //    Console.WriteLine("Starting...");
        //    Abstract.IInstall install = new Install();
        //    install.LoadComputersFromAD();
        //    Console.WriteLine("Computers Loaded from AD");
        //    int originalComputerCt = Install.Computers.Count;
        //    int interval = originalComputerCt / 10;
        //    Thread[] threads = new Thread[10];
        //    for (int t = 0; t < threads.Length; t++)
        //    {
        //        ParameterizedThreadStart pStart = new ParameterizedThreadStart(PThreadRemoveNonImprivataComputers);
        //        threads[t] = new Thread(pStart);
        //        threads[t].Start(new Install.ThreadRange { StartIndex = t * interval, Count = interval });
        //    }

        //    for (int i = 0; i < threads.Length; i++)
        //        threads[i].Join();


        //    Console.WriteLine("Non-Imprivata Machines Removed");
        //    install.SaveComputerInventory();
        //    Console.WriteLine("Inventory Updated!");
        //}

        //private static void PThreadRemoveNonImprivataComputers(object RangeAssignment)
        //{
        //    Install.ThreadRange tr = RangeAssignment as Install.ThreadRange;
        //    try
        //    {
        //        if (tr.StartIndex + tr.Count > tr.OriginalComputerCount)
        //            Install.RemoveNonImprivataComputers(Install.Computers.GetRange(tr.StartIndex, tr.Count));
        //        else
        //            Install.RemoveNonImprivataComputers(Install.Computers.GetRange(tr.StartIndex, tr.StartIndex - tr.OriginalComputerCount)); //last one will have less to do
        //    }
        //    catch { }
        //}

        #endregion

    }
}
