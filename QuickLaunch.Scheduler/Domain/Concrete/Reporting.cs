﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Collections;
using System.Data.SqlClient;
using QuickLaunch.Models;
namespace QuickLaunch.Scheduler
{
    public class Reporting : Abstract.IReporting
    {
        #region Properties
        public List<Models.Report> Reports { get; set; }        
        #endregion

        public void FindReports()
        {
            Reports = Models.Report.selScheduledReports();
            if (Reports == null) //ensures no nulls
                Reports = new List<Report>();
        }
                
        public void SendReports()
        {            
            for (int i = 0;i < Reports.Count();i++)
            {
                Report CurrentReport = Reports[i];
                try {
                    Scheduler.Reporting.GenerateReport(ref CurrentReport);        
                    Report.ReportHistory rptHistory = new Report.ReportHistory(CurrentReport);
                    rptHistory.lup_user = Program.ApplicationName;                
                    rptHistory.IsSent = Report.GenerateEmail(CurrentReport);
                    Report.insReportHistory(rptHistory);  
                } catch (Exception ex) { CurrentReport.Exception = ex; }

                if(CurrentReport.Exception != null)
                    Program.LogException(CurrentReport.Exception, false);
            }
        }

        public void CheckSchedule()
        {
            List<Report> _reports = new List<Report>();
            _reports.AddRange(Reports.GetRange(0,Reports.Count()));
            for (int i = 0; i < _reports.Count(); i++)
            {
            try {
                Report r = _reports[i];
                if (r.StartDt >= DateTime.Now) //Check if report has started yet
                    Reports.Remove(r);
                if (r.LastSentDt != null)
                { //Check if report has ever been sent...if not send it
                    switch (r.Frequency)
                    {
                        case "One Time": 
                            if (r.StartDt < r.LastSentDt) { Reports.Remove(r); continue; } //if start dt is less than last sent dt then it has already been sent once since starting
                            break;
                        case "Daily":
                            if (r.LastSentDt.AddHours(23) > DateTime.Now) { Reports.Remove(r); continue; }                            
                            break;
                        case "Weekly":
                            if (r.LastSentDt.AddDays(7).AddHours(-1) > DateTime.Now) { Reports.Remove(r); continue; }
                            break;                            
                        case "Bi-Weekly":
                            if (r.LastSentDt.AddDays(14).AddHours(-1) > DateTime.Now) { Reports.Remove(r); continue; }                            
                            break;
                        case "Monthly":
                            if (r.LastSentDt.AddMonths(1).AddHours(-1) > DateTime.Now) { Reports.Remove(r); continue; }                            
                            break;
                        case "Quarterly":
                            if (r.LastSentDt.AddMonths(3).AddHours(-1) > DateTime.Now) { Reports.Remove(r); continue; }                            
                            break;
                        case "Bi-Yearly":
                            if (r.LastSentDt.AddMonths(6).AddHours(-1) > DateTime.Now) { Reports.Remove(r); continue; }                            
                            break;
                        case "Yearly":
                            if (r.LastSentDt.AddYears(1).AddHours(-1) > DateTime.Now) { Reports.Remove(r); continue; }
                            break;
                        default: //acts like ad-hoc
                            Reports.Remove(r); continue;                            
                    }
                }
                if (r.IsRunSun || r.IsRunMon || r.IsRunTues || r.IsRunWed || r.IsRunThur || r.IsRunFri || r.IsRunSat)
                {
                    switch (DateTime.Now.DayOfWeek)
                    {
                        case DayOfWeek.Sunday:
                            if (!r.IsRunSun) { Reports.Remove(r); continue; }
                            break;
                        case DayOfWeek.Monday:
                            if (!r.IsRunMon) { Reports.Remove(r); continue; }
                            break;
                        case DayOfWeek.Tuesday:
                            if (!r.IsRunTues) { Reports.Remove(r); continue; }
                            break;
                        case DayOfWeek.Wednesday:
                            if (!r.IsRunWed) { Reports.Remove(r); continue; }
                            break;
                        case DayOfWeek.Thursday:
                            if (!r.IsRunThur) { Reports.Remove(r); continue; }
                            break;
                        case DayOfWeek.Friday:
                            if (!r.IsRunFri) { Reports.Remove(r); continue; }
                            break;
                        case DayOfWeek.Saturday:
                            if (!r.IsRunSat) { Reports.Remove(r); continue; }
                            break;
                    }
                }
            } catch {}
            }            
        }

        #region Shared
        //requires 4.x framework
        public static void GenerateReport(ref Models.Report CurrentReport)
        {
            XDocument doc = XDocument.Parse(CurrentReport.Definition); //requires 3.5 and above, so it cannot go in the shared project
            try
            {
                CurrentReport.Parameters = doc.Descendants("parameters").Descendants("param").Any() ? doc.Descendants("parameters").Select(x => new Models.Report.Parameter
                {
                    Name = x.Element("param").Attribute("name").Value,
                    Value = x.Element("param").Attribute("value").Value,
                }).ToList() : null;
            }
            catch (Exception ex) { CurrentReport.Exception = ex; }

            try
            {
                CurrentReport.Columns = doc.Descendants("column").Select(x => new Models.Report.Column
                {
                    Source = x.Attribute("src").Value, //required field
                    Text = x.Attribute("text") != null ? x.Attribute("text").Value : x.Attribute("src").Value,
                    Format = x.Attribute("format") != null ? x.Attribute("format").Value : string.Empty,
                    Alignment = x.Attribute("align") != null ? x.Attribute("align").Value : string.Empty,
                    Style = x.Attribute("style") != null ? x.Attribute("style").Value : string.Empty,
                    ToolTip = x.Attribute("tooltip") != null ? x.Attribute("tooltip").Value : string.Empty,
                    CompareToOperator = x.Attribute("comparetooperator") != null ? x.Attribute("comparetooperator").Value : string.Empty,
                    CompareToValue = x.Attribute("comparetovalue") != null ? x.Attribute("comparetovalue").Value : string.Empty,
                    CompareToStyle = x.Attribute("comparetostyle") != null ? x.Attribute("comparetostyle").Value : "font-weight:bold;"
                }).ToList();
            }
            catch (Exception ex) { CurrentReport.Exception = ex; }
            try
            {
                switch (CurrentReport.Type)
                {
                    case "SlowMachines":
                        CurrentReport.DataSource = new ArrayList(Models.Machine.selSlowestMachines(Convert.ToInt32(Models.Report.GetParameter(CurrentReport, "LoginLengthLower", "3"))
                                                        , Convert.ToInt32(Models.Report.GetParameter(CurrentReport, "RoamLengthLower", "20"))
                                                        , Convert.ToInt32(Models.Report.GetParameter(CurrentReport, "LaunchLengthLower", "40"))).ToArray());
                        break;
                    case "EventLogMetrics":
                        CurrentReport.DataSource = new ArrayList((new List<Models.UserProfile.UserMetrics> { Models.UserProfile.selEventLogMetrics() }).ToArray());
                        break;
                    case "InvalidICA":
                        CurrentReport.DataSource = new ArrayList(Models.Machine.selInvalidICAMachines().ToArray());
                        break;
                    default:
                        if (string.IsNullOrEmpty(CurrentReport.Sql)) { CurrentReport.Exception = new Exception("Custom report contains no SQL"); return; } //no sql
                        SqlParameter[] @params = new SqlParameter[CurrentReport.Parameters.Count];
                        for (int i = 0; i < @params.Length; i++)
                            @params[i] = new SqlParameter(string.Format("@{0}", CurrentReport.Parameters[i].Name), CurrentReport.Parameters[i].Value);
                        CurrentReport.DataSource = new ArrayList(DBManager.ExecCommandDS(CurrentReport.Sql, @params).Tables[0].AsEnumerable().ToArray());
                        break;
                }
                CurrentReport.ResultCt = CurrentReport.DataSource != null ? CurrentReport.DataSource.Count : 0;
            }
            catch (Exception ex) { CurrentReport.Exception = ex; }            
        }

        #endregion

        public Reporting()
        {

        }

    }
}
