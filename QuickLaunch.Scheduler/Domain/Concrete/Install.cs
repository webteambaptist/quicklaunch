﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace QuickLaunch.Scheduler
{
    class Install : Abstract.IInstall
    {
        #region Properties
        public static List<Models.Machine> Computers { get; set; }
        Version QuickLaunchVersion { get; set; }
        #endregion

        public void LoadComputersFromAD()
        {
            Computers = Shared.ADHelper.GetComputers();
        }

        public void LoadComputersFromDB()
        {
            Computers = Models.Machine.selMachines();
        }

        public static void RemoveNonImprivataComputers(List<Models.Machine> ComputerRange)
        {
            List<Models.Machine> _Computers = new List<Models.Machine>();
            _Computers.AddRange(ComputerRange);
            foreach (Models.Machine m in _Computers)
            {
                if(Shared.SysHelper.PingComputer(m.PCName)){ //check to see if it's awake
                    if (!File.Exists(string.Format(@"\\{0}\c$\Progra~1\Impriv~1\OneSig~1\ISXAgent.exe", m.PCName))){ //does it have imprivata installed?
                        Computers.Remove(m); //no imprivata
                    }
                } else { //machine not responding
                    Computers.Remove(m);
                }
            }
        }

        public void SaveComputerInventory()
        {
            foreach (Models.Machine m in Computers)            
                Models.Machine.insMachineFromAD(m);
        }

        public void SendQuickLaunchMSI() { }

        private void GetQuickLaunchVersion() { }

        private void GetMSIInstall() { }


        public Install()
        {

        }

    #region Inner Classes
    #endregion

    }
}
