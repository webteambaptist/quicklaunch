﻿namespace QuickLaunch.Shared
{
    using System;

    [Serializable]
    public enum ImprivataEvent
    {
        UserLogon,

        WorkstationLock,

        WorkstationUnlock,

        NonOneSignLogon,

        Install,

        InstallComplete
    }
}