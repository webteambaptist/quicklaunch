﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class SystemAlertResponse : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the SystemAlertId
        /// </summary>
        public int SystemAlertId { get; set; }

        /// <summary>
        /// Gets/Sets the Network ID
        /// </summary>
        public string NtId { get; set; }

        /// <summary>
        /// Gets/Sets whether the Dialog has been Shown 
        /// </summary>
        public bool DialogShown { get; set; }

        /// <summary>
        /// Gets/Sets whether the Bubble has been Shown
        /// </summary>
        public bool BubbleShown { get; set; }

        /// <summary>
        /// Gets/Sets whether the Toast has been Shown
        /// </summary>
        public bool ToastShown { get; set; }

        /// <summary>
        /// Gets/Sets whether the Dialog Acceptance has occurred
        /// </summary>
        public bool DialogAccept { get; set; }
        
        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }


        /* Display Fields */

        /// <summary>
        /// Gets/Sets the Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets/Sets the Alert Title
        /// </summary>
        public string AlertTitle { get; set; }

        /* End Display Fields */

        #endregion

        public SystemAlertResponse() { }
        
        #region "SELECTS"
        public static SystemAlertResponse selSystemAlertResponseById(int id)
        {
            string sql = "";
            sql = "SELECT * FROM SSO_SystemAlertResponse WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<SystemAlertResponse>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<SystemAlertResponse> selSystemAlertResponses(DateTime StartDt, DateTime EndDt, int AlertId = 0, string NtId = "", int CodeId = 0)
        {
            string sql = "SELECT sar.*, sa.Name as AlertTitle, sac.Code FROM SSO_SystemAlertResponse sar " + 
                " LEFT OUTER JOIN SSO_SystemAlert sa ON sa.id = sar.SystemAlertId " +
                " LEFT OUTER JOIN SSO_SystemAlertCode sac ON sac.id = sa.CodeId " +  
                " WHERE sar.[lup_dt] >= @StartDt AND sar.[lup_dt] <= @EndDt " +
                (AlertId > 0 ? " AND sar.[SystemAlertId] = @SystemAlertId " : string.Empty) +
                (CodeId > 0 ? " AND sa.[CodeId] = @CodeId " : string.Empty) +
                (!string.IsNullOrEmpty(NtId) ? " AND sar.[NtId] = @NtId " : string.Empty) +                
                " order by sar.lup_dt DESC";

            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@StartDt", StartDt);
            @params[1] = new SqlParameter("@EndDt", EndDt);
            @params[2] = new SqlParameter("@SystemAlertId", AlertId);
            @params[3] = new SqlParameter("@NtId", NtId);
            @params[4] = new SqlParameter("@CodeId", CodeId);

            return BOHelper.FillCollection<SystemAlertResponse>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static void insSystemAlertResponse(SystemAlertResponse sar)
        {
            string sql = @"IF NOT EXISTS (SELECT * FROM SSO_SystemAlertResponse WHERE SystemAlertId = @SystemAlertId AND NtId = @NtId) 
                                 INSERT INTO SSO_SystemAlertResponse ([SystemAlertId], [NtId], [DialogShown], [BubbleShown], [ToastShown], [DialogAccept]) VALUES (@SystemAlertId, @NtId, @DialogShown, @BubbleShown, @ToastShown, @DialogAccept);
                           ELSE
                                 UPDATE SSO_SystemAlertResponse SET [DialogShown] = @DialogShown, [BubbleShown] = @BubbleShown, [ToastShown] = @ToastShown, [DialogAccept] = @DialogAccept, [lup_dt] = @lup_dt WHERE [SystemAlertId] = @SystemAlertId AND [NtId] LIKE @NtId
                          ";
            SqlParameter[] @params = new SqlParameter[8];

            @params[0] = new SqlParameter("@SystemAlertId", sar.SystemAlertId);
            @params[1] = new SqlParameter("@NtId", sar.NtId);
            @params[2] = new SqlParameter("@DialogShown", sar.DialogShown);
            @params[3] = new SqlParameter("@BubbleShown", sar.BubbleShown);
            @params[4] = new SqlParameter("@ToastShown", sar.ToastShown);
            @params[5] = new SqlParameter("@DialogAccept", sar.DialogAccept);
            @params[6] = new SqlParameter("@lup_dt", sar.lup_dt);

            DBManager.ExecCommandDR(sql, @params);
        }
        #endregion

        #region "UPDATES"
        public static void updSystemAlertAcceptResponse(SystemAlertResponse SystemAlertResponse)
        {
            string sql = @"UPDATE SSO_SystemAlertResponse SET [DialogAccept] = @DialogAccept, [lup_dt] = @lup_dt WHERE [SystemAlertId] = @SystemAlertId AND [NtId] LIKE @NtId";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@NtId", SystemAlertResponse.NtId);
            @params[1] = new SqlParameter("@SystemAlertId", SystemAlertResponse.SystemAlertId);            
            @params[2] = new SqlParameter("@DialogAccept", SystemAlertResponse.DialogAccept);
            @params[3] = new SqlParameter("@lup_dt", SystemAlertResponse.lup_dt);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlertResponseById(SystemAlertResponse SystemAlertResponse)
        {
            throw new NotImplementedException();
            string sql = @"DELETE FROM SSO_SystemAlertResponse WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", SystemAlertResponse.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlertResponseByAlertId(SystemAlertResponse SystemAlertResponse)
        {
            throw new NotImplementedException();
            string sql = @"DELETE FROM SSO_SystemAlertResponse WHERE [SystemAlertId] = @SystemAlertId";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@SystemAlertId", SystemAlertResponse.SystemAlertId);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlertResponses(List<SystemAlertResponse> sars)
        {            
            string sql = @"";
            foreach(SystemAlertResponse sar in sars)
                sql += string.Format("DELETE FROM SSO_SystemAlertResponse WHERE [id] = {0}; ", sar.id.ToString());

            SqlParameter[] @params = new SqlParameter[3];            
            @params[0] = new SqlParameter("@lup_dt", DateTime.Now);
            if(!string.IsNullOrEmpty(sql))
                DBManager.ExecCommandDR(sql, @params, true);
        }

        #endregion
    }
}
