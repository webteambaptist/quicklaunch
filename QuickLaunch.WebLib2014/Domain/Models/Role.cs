﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class Role : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the role title.
        /// </summary>
        public string role { get; set; }

        /// <summary>
        /// Gets/Sets the lup user
        /// </summary>
        public string lup_user { get; set; }

         /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion

        public Role() { }
        
        #region "SELECTS"
        public static Role selRoleById(int id)
        {
            string sql = "";
            sql = "SELECT * FROM SSO_Role WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<Role>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<Role> selRoles()
        {
            string sql = "SELECT * FROM SSO_Role order by Role";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Role>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insRole(Role Role)
        {
            string sql = @"INSERT INTO SSO_Role ([Role], [lup_user]) VALUES (@Role, @lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];

            @params[0] = new SqlParameter("@Role", Role.role);
            @params[1] = new SqlParameter("@lup_user", Role.lup_user);            

            return BOHelper.FillObject<Role>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updRole(Role Role)
        {
            string sql = @"UPDATE SSO_Role SET [Role] = @Role, [lup_user] = @lup_user, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@Role", Role.role);
            @params[1] = new SqlParameter("@lup_user", Role.lup_user);            
            @params[2] = new SqlParameter("@lup_dt", Role.lup_dt);
            @params[3] = new SqlParameter("@id", Role.id);
            
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delRole(Role Role)
        {            
            string sql = @"DELETE FROM SSO_UserProfileRole WHERE RoleId = @id; DELETE FROM SSO_Role WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", Role.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
