﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Text;
namespace QuickLaunch.Models
{
    public class EventLog : BaseModel, Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the EventCode.
        /// </summary>
        public string EventCode { get; set; }

        /// <summary>
        /// Gets/Sets the NtId
        /// </summary>
        public string NtId { get; set; }

        /// <summary>
        /// Gets/Sets the Machine
        /// </summary>
        public string Machine { get; set; }

        /// <summary>
        /// Gets/Sets the Application
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// Gets/Sets the Menu Item Id.
        /// </summary>
        public int MenuItemId { get; set; }

        /// <summary>
        /// Gets/Sets the time length in event.
        /// </summary>
        public double LengthOfTime { get; set; }

        /* Display Fields */
        public int GroupCount { get; set; }
        public long JSTimeStamp { get; set; }
        /* End Display Fields */

        #endregion

        public EventLog() { }
        
        #region "SELECTS"
        public static EventLog selEventLogById(int id)
        {
            string sql = "";
            sql = @"SELECT el.* FROM SSO_EventLog el
                        LEFT OUTER JOIN SSO_Event e ON e.Code like el.EventCode
                    WHERE el.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<EventLog>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<EventLog> selEventLogs(int MaxCt = 100)
        {
            string sql = @"SELECT TOP " + MaxCt + " * FROM SSO_EventLog order by lup_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<EventLog>(DBManager.ExecCommandDR(sql, @params));
        }

        /// <summary>
        /// Event Log grouped by datetime
        /// </summary>
        /// <param name="GroupBy">week,day,hour</param>
        /// <param name="userId">NTID</param>
        /// <param name="machine">S/N</param>
        /// <returns></returns>
        public static List<EventLog> selEventLogsGroupedBy(string GroupBy, DateTime DataYoungerThan, string userId = "", string machine = "")
        {
            string sql = string.Format(@"SELECT DATEPART({0}, lup_dt) as id, EventCode, count(*) AS GroupCount, (avg(lengthoftime) / 1000) as LengthOfTime
                                FROM [SSO_EventLog]
                                WHERE lup_dt >= @DataYoungerThan {1} {2}
                                GROUP BY DATEPART({0}, lup_dt), EventCode
                                ORDER BY id asc", GroupBy, string.IsNullOrEmpty(userId) ? string.Empty : " AND ntID LIKE @NtId", string.IsNullOrEmpty(machine) ? string.Empty : " AND machine LIKE @Machine");
            SqlParameter[] @params = new SqlParameter[4];            
            @params[0] = new SqlParameter("@NtId", userId);
            @params[1] = new SqlParameter("@Machine", machine);
            @params[2] = new SqlParameter("@DataYoungerThan", DataYoungerThan);

            return BOHelper.FillCollection<EventLog>(DBManager.ExecCommandDR(sql, @params));
        }
        
        public static List<EventLog> selEventsByMachine(string machine, int MaxCt = 10)
        {
            string sql = @"SELECT TOP " + MaxCt + " * FROM SSO_EventLog WHERE [Machine] LIKE @Machine order by lup_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@Machine", machine);  
            return BOHelper.FillCollection<EventLog>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<EventLog> selEventsByUser(string userId, int MaxCt = 10)
        {
            string sql = @"SELECT TOP " + MaxCt + " * FROM SSO_EventLog WHERE [NtId] LIKE @NtId order by lup_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@NtId", userId);
            return BOHelper.FillCollection<EventLog>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<EventLog.DataWarehouse> selExtinctingEventLogs(int days = 90)
        {
            string sql = @"SELECT convert(datetime, convert(int, EL.lup_dt)) AS ActivityDt
	                              , MAX(u.USERCT) as UserCt
	                              , MAX(m.MCHCT) as MachineCt
	                              , MAX(LG.Ct) as LoginCt	   
	                              , MAX(LG.AvgLength) as LoginAvg
	                              , MAX(RM.Ct) as RoamCt	   
	                              , MAX(RM.AvgLength) as RoamAvg
	                              , MAX(LC.Ct) as LaunchCt	   
	                              , MAX(LC.AvgLength) as LaunchAvg
	                              , MAX(DC.Ct) as DisconnectCt	   
	                              , MAX(DC.AvgLength) as DisconnectAvg
	                              , MAX(LO.Ct) as LogoffCt	   
	                              , MAX(LO.AvgLength) as LogoffAvg
                            FROM SSO_EventLog EL
	                            LEFT OUTER JOIN (SELECT count(*) AS USERCT FROM SSO_UserProfile WHERE lastLoginLength > 0) AS U ON 1=1
	                            LEFT OUTER JOIN (SELECT count(*) AS MCHCT FROM SSO_MachineInventory WHERE len(QLVersion) > 0) AS M ON 1=1
                                LEFT OUTER JOIN (SELECT convert(datetime, convert(int, lup_dt)) AS ActivityDt, COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM SSO_EventLog WHERE EventCode LIKE 'LOGIN' GROUP BY convert(datetime, convert(int, lup_dt))) LG ON convert(datetime, convert(int, el.lup_dt)) = LG.ActivityDt
                                LEFT OUTER JOIN (SELECT convert(datetime, convert(int, lup_dt)) AS ActivityDt, COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM SSO_EventLog WHERE EventCode LIKE 'ROAM' GROUP BY convert(datetime, convert(int, lup_dt))) RM ON convert(datetime, convert(int, el.lup_dt)) = RM.ActivityDt
                                LEFT OUTER JOIN (SELECT convert(datetime, convert(int, lup_dt)) AS ActivityDt, COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM SSO_EventLog WHERE EventCode LIKE 'LAUNCH' GROUP BY convert(datetime, convert(int, lup_dt))) LC ON convert(datetime, convert(int, el.lup_dt)) = LC.ActivityDt
                                LEFT OUTER JOIN (SELECT convert(datetime, convert(int, lup_dt)) AS ActivityDt, COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM SSO_EventLog WHERE EventCode LIKE 'DISCONNECT' GROUP BY convert(datetime, convert(int, lup_dt))) DC ON convert(datetime, convert(int, el.lup_dt)) = DC.ActivityDt
                                LEFT OUTER JOIN (SELECT convert(datetime, convert(int, lup_dt)) AS ActivityDt, COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM SSO_EventLog WHERE EventCode LIKE 'LOGOFF' GROUP BY convert(datetime, convert(int, lup_dt))) LO ON convert(datetime, convert(int, el.lup_dt)) = LO.ActivityDt   
                            WHERE EL.[lup_dt] < dateadd(day, @Days, getdate())
                            GROUP BY convert(datetime, convert(int, EL.lup_dt))
                            ORDER BY ActivityDt desc";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@Days", -days);

            return BOHelper.FillCollection<EventLog.DataWarehouse>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"
        public static string insEventLog(EventLog EventLog)
        {
            string sql = @"INSERT INTO SSO_EventLog ([EventCode],[NtId],[Machine],[Application],[MenuItemId],[LengthOfTime]) 
                                             VALUES (@EventCode,@NtId,@Machine,@Application,@MenuItemId,@LengthOfTime); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[10];

            @params[0] = new SqlParameter("@EventCode", EventLog.EventCode);
            @params[1] = new SqlParameter("@NtId", EventLog.NtId);
            @params[2] = new SqlParameter("@Machine", EventLog.Machine);
            @params[3] = new SqlParameter("@Application", EventLog.Application);
            @params[4] = new SqlParameter("@MenuItemId", EventLog.MenuItemId);
            @params[5] = new SqlParameter("@LengthOfTime", EventLog.LengthOfTime);
            
            return BOHelper.FillObject<EventLog>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }

        /// <summary>
        /// UserCt and MachineCt are snapshot #'s.
        /// The rest of the counts and avgs are what's being removed. So in the case where there's a record because of timing or missed run day, we need to append that data to the existing row.
        /// </summary>
        /// <param name="dw">Record to be inserted or updated</param>
        public static void insDataWarehouse(EventLog.DataWarehouse dw)
        {
            string sql = @"IF NOT EXISTS (SELECT * FROM SSO_EventDataWarehouse WHERE [ActivityDt] = @ActivityDt) 
                                 INSERT INTO SSO_EventDataWarehouse ([ActivityDt],[UserCt],[LoginCt],[LaunchCt],[RoamCt],[DisconnectCt],[LogoffCt],[MachineCt],[LoginAvg],[LaunchAvg],[RoamAvg],[DisconnectAvg],[LogoffAvg]) 
                                                            VALUES (@ActivityDt,@UserCt,@LoginCt,@LaunchCt,@RoamCt,@DisconnectCt,@LogoffCt,@MachineCt,@LoginAvg,@LaunchAvg,@RoamAvg,@DisconnectAvg,@LogoffAvg);
                           ELSE
                                 UPDATE SSO_EventDataWarehouse SET [UserCt] = @UserCt,[MachineCt] = @MachineCt,[LoginCt] = ([LoginCt]+@LoginCt),[LaunchCt] = ([LaunchCt]+@LaunchCt),[RoamCt] = ([RoamCt]+@RoamCt),[DisconnectCt] = ([DisconnectCt]+@DisconnectCt),[LogoffCt] = ([LogoffCt]+@LogoffCt),[LoginAvg] = ([LoginAvg]+@LoginAvg),[LaunchAvg] = ([LaunchAvg]+@LaunchAvg),[RoamAvg] = ([RoamAvg]+@RoamAvg),[DisconnectAvg] = ([DisconnectAvg]+@DisconnectAvg),[LogoffAvg] = ([LogoffAvg]+@LogoffAvg),[lup_dt] = @lup_dt 
                                            WHERE [ActivityDt] = @ActivityDt
                          ";
            SqlParameter[] @params = new SqlParameter[15];
            @params[0] = new SqlParameter("@ActivityDt", dw.ActivityDt);
            @params[1] = new SqlParameter("@UserCt", dw.UserCt);
            @params[2] = new SqlParameter("@MachineCt", dw.MachineCt);
            @params[3] = new SqlParameter("@LoginCt", dw.LoginCt);
            @params[4] = new SqlParameter("@LoginAvg", dw.LoginAvg);
            @params[5] = new SqlParameter("@RoamCt", dw.RoamCt);
            @params[6] = new SqlParameter("@RoamAvg", dw.RoamAvg);
            @params[7] = new SqlParameter("@LaunchCt", dw.LaunchCt);
            @params[8] = new SqlParameter("@LaunchAvg", dw.LaunchAvg);
            @params[9] = new SqlParameter("@DisconnectCt", dw.DisconnectCt);
            @params[10] = new SqlParameter("@DisconnectAvg", dw.DisconnectAvg);
            @params[11] = new SqlParameter("@LogoffCt", dw.LogoffCt);
            @params[12] = new SqlParameter("@LogoffAvg", dw.LogoffAvg);
            @params[13] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }

        #endregion

        #region "UPDATES"

        public static void updClearEventLogMetrics(IEnumerable<UserProfile> profilesToClear, bool Roams = false, bool Logins = false, bool Launches = false, bool Disconnects = false)
        {
            if (!Roams && !Logins && !Launches && !Disconnects) { throw new Exception("No event passed for clearing [EventLog.updClearEventLogMetrics]"); }
            
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.Append(@"DELETE SSO_EventLog SET WHERE 1=1 AND [NTID] IN ('1'");
            foreach (UserProfile up in profilesToClear)
                sbSQL.Append(string.Format(", '{0}'", Shared.SysHelper.CleanSql(up.ntID)));

            sbSQL.Append(") " + (Logins ? " AND EventCode LIKE 'LOGIN' " : string.Empty)
                           + (Roams ? " AND EventCode LIKE 'ROAM' " : string.Empty)
                           + (Launches ? " AND EventCode LIKE 'LAUNCH' " : string.Empty)
                           + (Disconnects ? " AND EventCode LIKE 'DISCONNECT' " : string.Empty)
                           + " ; ");

            SqlParameter[] @params = new SqlParameter[2];
            @params[0] = new SqlParameter("@lastUpdate", DateTime.Now);

            if (sbSQL.Length > 0)
                DBManager.ExecCommandDR(sbSQL.ToString(), @params, true);
        }
        public static void delEventLog(EventLog EventLog)
        {            
            string sql = @"DELETE FROM SSO_EventLog WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", EventLog.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delExtinctEventLogs(int days = 90)
        {
            string sql = @"DELETE FROM SSO_EventLog where [lup_dt] < dateadd(day, @Days, getdate())";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@Days", -days);

            DBManager.ExecCommandDR(sql, @params, true);
        }

        #endregion


        public static void LogEvent(string Code, string UserId, string App, int MenuItemId = 0, double LengthOfTime = 0)
        {
            try
            {
                insEventLog(new EventLog
                {
                     EventCode = Code
                    ,NtId = UserId.ToUpper()
                    ,Machine = System.Environment.MachineName
                    ,LengthOfTime = LengthOfTime
                    ,MenuItemId = MenuItemId
                    ,Application = App
                });
            } catch {} //do nothing if network is unavailable
        }


        #region Collections
        public class EventChart
        {
            public Chart logins { get; set; }
            public Chart roams { get; set; }
            public Chart launches { get; set; }
            public Chart disconnects { get; set; }
            public Chart logoffs { get; set; }
            public EventChart() {
                logins = new Chart();
                roams = new Chart();
                launches = new Chart();
                disconnects = new Chart();
                logoffs = new Chart();

                logins.label = "Logins";
                roams.label = "Roams";
                logoffs.label = "Logoffs";
                launches.label = "Launches";
                disconnects.label = "Disconnects";
            }
        }
        public class Chart
        {
            public string label { get; set; }
            public long[][] data { get; set; }
            public Chart() { }
        }
        #endregion

        #region Inner Classes
        public class DataWarehouse : BaseModel, Abstract.IDataInfo
        {            
            public DateTime ActivityDt { get; set; }
            public int UserCt { get; set; }
            public int LoginCt { get; set; }
            public int LaunchCt { get; set; }
            public int RoamCt { get; set; }
            public int DisconnectCt { get; set; }
            public int LogoffCt { get; set; }
            public int MachineCt { get; set; }
            public double LoginAvg { get; set; }
            public double LaunchAvg { get; set; }
            public double RoamAvg { get; set; }
            public double DisconnectAvg { get; set; }
            public double LogoffAvg { get; set; }            
        }

        #endregion

    }
}
