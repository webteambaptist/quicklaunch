﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class Version : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets/Sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets/Sets the latest version
        /// </summary>
        public string LatestVersion { get; set; }

        /// <summary>
        /// Gets/Sets the acceptable version 1
        /// </summary>
        public string AcceptableVersion1 { get; set; }

        /// <summary>
        /// Gets/Sets the acceptable version 2
        /// </summary>
        public string AcceptableVersion2 { get; set; }

        /// <summary>
        /// Gets/Sets the acceptable version 3
        /// </summary>
        public string AcceptableVersion3 { get; set; }

        /// <summary>
        /// Gets/Sets the Check Frequency
        /// </summary>
        public int CheckFrequency { get; set; }

        /// <summary>
        /// Gets/Sets the lup user
        /// </summary>
        public string lup_user { get; set; }

         /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /* Display Fields */
        public int MachineCt { get; set; }
        /* End Display Fields */

        #endregion

        public Version() { }
        
        #region "SELECTS"
        public static Version selVersionById(int id)
        {
            string sql = "";
            sql = "SELECT * FROM SSO_Version WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<Version>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<Version> selVersions()
        {
            string sql = "SELECT v.*, (SELECT COUNT(*) FROM SSO_MachineInventory mi WHERE mi.QLVersion like v.LatestVersion OR mi.ICAVersion like v.LatestVersion OR mi.AutoInstallVersion like v.LatestVersion) as MachineCt FROM SSO_Version v order by v.id";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Version>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insVersion(Version Version)
        {
            string sql = @"INSERT INTO SSO_Version ([Code], [Title], [LatestVersion], [AcceptableVersion1], [AcceptableVersion2], [AcceptableVersion3], [Message], [CheckFrequency], [lup_user]) VALUES (@Code, @Title, @LatestVersion, @AcceptableVersion1, @AcceptableVersion2, @AcceptableVersion3, @Message, @CheckFrequency, @lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@Code", Version.Code);
            @params[1] = new SqlParameter("@Title", Version.Title);
            @params[2] = new SqlParameter("@LatestVersion", Version.LatestVersion);
            @params[3] = new SqlParameter("@AcceptableVersion1", Version.AcceptableVersion1);
            @params[4] = new SqlParameter("@AcceptableVersion2", Version.AcceptableVersion2);
            @params[5] = new SqlParameter("@AcceptableVersion3", Version.AcceptableVersion3);            
            @params[6] = new SqlParameter("@Message", Version.Message);
            @params[7] = new SqlParameter("@CheckFrequency", Version.CheckFrequency);
            @params[8] = new SqlParameter("@lup_user", Version.lup_user);            

            return BOHelper.FillObject<Version>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updVersion(Version Version)
        {
            string sql = @"UPDATE SSO_Version SET [Title] = @Title, [LatestVersion] = @LatestVersion, [AcceptableVersion1] = @AcceptableVersion1, [AcceptableVersion2] = @AcceptableVersion2, [AcceptableVersion3] = @AcceptableVersion3, [Message] = @Message, [CheckFrequency] = @CheckFrequency, [lup_user] = @lup_user, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[12];
            @params[0] = new SqlParameter("@Code", Version.Code);
            @params[1] = new SqlParameter("@Title", Version.Title);
            @params[2] = new SqlParameter("@LatestVersion", Version.LatestVersion);
            @params[3] = new SqlParameter("@AcceptableVersion1", Version.AcceptableVersion1);
            @params[4] = new SqlParameter("@AcceptableVersion2", Version.AcceptableVersion2);
            @params[5] = new SqlParameter("@AcceptableVersion3", Version.AcceptableVersion3);    
            @params[6] = new SqlParameter("@Message", Version.Message);
            @params[7] = new SqlParameter("@CheckFrequency", Version.CheckFrequency);
            @params[8] = new SqlParameter("@lup_user", Version.lup_user);            
            @params[9] = new SqlParameter("@lup_dt", Version.lup_dt);
            @params[10] = new SqlParameter("@id", Version.id);
            
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delVersion(Version Version)
        {
            throw new NotImplementedException();
            string sql = @"DELETE FROM SSO_Version WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", Version.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
