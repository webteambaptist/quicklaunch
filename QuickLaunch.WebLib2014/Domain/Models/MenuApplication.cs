﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    [Serializable]
    public class MenuApplication : Application, Abstract.IDataInfo
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the menu link ID.
        /// </summary>
        public int MenuLink_Id { get; set; }

        /// <summary>
        /// Gets/Sets the application ID.
        /// </summary>
        public int Application_Id { get; set; }

        /// <summary>
        /// Gets/Sets the Order ID.
        /// </summary>
        public int Order_Id { get; set; }

 
        /* Display Fields */

        
        /* End Display Fields */

        /// <summary>
        /// Gets/Sets relationship between Citrix Applications and menu.
        /// Default list for offline mode : gets overriden on QL menu load
        /// </summary>        
        public static List<MenuApplication> MenuApplications = new List<MenuApplication> {  
                    new MenuApplication { MenuLink_Id = 100, Application_Id = 100, Order_Id = 1},
                    new MenuApplication { MenuLink_Id = 100, Application_Id = 101, Order_Id = 2},
                    new MenuApplication { MenuLink_Id = 100, Application_Id = 102, Order_Id = 3},
                    new MenuApplication { MenuLink_Id = 101, Application_Id = 103, Order_Id = 1},
                    new MenuApplication { MenuLink_Id = 101, Application_Id = 104, Order_Id = 2},
                    new MenuApplication { MenuLink_Id = 102, Application_Id = 105, Order_Id = 1},
                    new MenuApplication { MenuLink_Id = 102, Application_Id = 106, Order_Id = 2}
        };
        public static DateTime LastMenuApplicationsUpdate { get; set; }
        #endregion

        public MenuApplication() { }

        public static void LoadMenuApplications(){
            if (LastMenuApplicationsUpdate < DateTime.Now.AddHours(-1)) //only check once an hour
            {
                List<MenuApplication> DbApps = selMenuApplications();
                if (DbApps.Count > 0)
                {
                    MenuApplications.Clear();
                    MenuApplications.AddRange(DbApps);
                    LastMenuApplicationsUpdate = DateTime.Now;
                }
            }
        }
        

        #region "SELECTS"

        public static MenuApplication selMenuApplicationById(int id)
        {
            string sql = "";
            sql = @"SELECT ma.*, app.Title, app.Value, app.WindowTitle  FROM SSO_MenuApplications ma
                           INNER JOIN SSO_Application app ON app.id = ma.Application_Id
                    WHERE ma.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<MenuApplication>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<MenuApplication> selApplicationsByMenuId(int MenuId)
        {
            string sql = "";
            sql = @"SELECT ma.*, app.Title, app.Value, app.WindowTitle, (app.Title + ' (' + app.Value + ')') as TitleValueDisplay  FROM SSO_MenuApplications ma
                           INNER JOIN SSO_Application app ON app.id = ma.Application_Id
                        WHERE ma.[MenuLink_Id] = @MenuId
                        ORDER BY Order_Id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@MenuId", MenuId);

            return BOHelper.FillCollection<MenuApplication>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<MenuApplication> selMenuApplications()
        {
            string sql = @"SELECT ma.*, app.Title, app.Value, app.WindowTitle  FROM SSO_MenuApplications ma
                           INNER JOIN SSO_Application app ON app.id = ma.Application_Id
                           order by MenuLink_Id, Order_Id";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<MenuApplication>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"
        public static string insMenuApplication(MenuApplication Application)
        {
            string sql = @"INSERT INTO SSO_MenuApplications ([MenuLink_Id],[Application_Id],[Order_Id]) VALUES (@MenuLink_Id,@Application_Id,@Order_Id); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@MenuLink_Id", Application.MenuLink_Id);
            @params[1] = new SqlParameter("@Application_Id", Application.Application_Id);
            @params[2] = new SqlParameter("@Order_Id", Application.Order_Id);            

            return BOHelper.FillObject<MenuApplication>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updMenuApplication(MenuApplication Application)
        {
            string sql = @"UPDATE SSO_MenuApplications SET [Order_Id] = @Order_Id WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@Order_Id", Application.Order_Id);        
            @params[1] = new SqlParameter("@id", Application.id);

            DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void updReorderAppsForMenuItem(List<MenuApplication> ma)
        {
            string sql = "";
            for (int i = 0; i < ma.Count; i++)
                sql += @"UPDATE SSO_MenuApplications SET [Order_Id] = " + (i + 1).ToString() + " WHERE [id] = " + ma[i].id + ";";

            SqlParameter[] @params = new SqlParameter[2];
            if (sql.Length > 0) DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void delMenuApplication(MenuApplication Application)
        {
            string sql = @"DELETE FROM SSO_MenuApplications WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", Application.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
