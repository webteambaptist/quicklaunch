﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Management;
using System.Diagnostics;
using System.Text;
using System.Configuration;
namespace QuickLaunch.Models
{
    [Serializable]
    public class Machine : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets whether the machine is excluded or not.
        /// </summary>
        public Boolean isExcluded { get; set; }

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the pc.
        /// </summary>
        public string PCName { get; set; }

        /// <summary>
        /// Gets/Sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the LSID.
        /// </summary>
        public string LSID { get; set; }

        /// <summary>
        /// Gets/Sets the location id.
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        /// Gets/Sets the QLVersion.
        /// </summary>
        public string QLVersion { get; set; }

        /// <summary>
        /// Gets/Sets the ICAVersion.
        /// </summary>
        public string ICAVersion { get; set; }

        /// <summary>
        /// Gets/Sets the IEVersion.
        /// </summary>
        public string IEVersion { get; set; }

        /// <summary>
        /// Gets/Sets the AutoInstall Version on the computer.
        /// </summary>
        public string AutoInstallVersion { get; set; }

        /// <summary>
        /// Gets/Sets the Computer Info.
        /// </summary>
        public string ComputerInfo { get; set; }

        /// <summary>
        /// Gets/Sets the Operating System Info.
        /// </summary>
        public string OSInfo { get; set; }

        /// <summary>
        /// Gets/Sets the Processes running on the computer.
        /// </summary>
        public string Processes { get; set; }

        /// <summary>
        /// Gets/Sets the Memory on the computer.
        /// </summary>
        public string Memory { get; set; }

        /// <summary>
        /// Gets/Sets the IpAddress of the computer.
        /// </summary>
        public string IpAddress { get; set; }

        /// <summary>
        /// Gets/Sets the Last login.
        /// </summary>
        public DateTime LastLogin { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

 


        /* Display Fields */

        /// <summary>
        /// Gets/Sets the Location Title.
        /// </summary>
        public string LocationTitle { get; set; }

        /// <summary>
        /// Gets/Sets the WTS Printer Location.
        /// </summary>
        public string WTSLocation { get; set; }

        public double AvgLoginLength { get; set; }
        public double AvgEMRLaunchLength { get; set; }
        public double AvgRoamLength { get; set; }
        /* End Display Fields */
        #endregion

        public Machine() { }
        
        #region "SELECTS"
        public static Machine selMachineById(string pcName)
        {
            Machine mac = new Machine();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selMachineById", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("pcName", pcName);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        mac.AutoInstallVersion = sdr["AutoInstallVersion"].ToString();
                        mac.ComputerInfo = sdr["ComputerInfo"].ToString();
                        mac.Description = sdr["Description"].ToString();
                        mac.ICAVersion = sdr["ICAVersion"].ToString();
                        mac.id = int.Parse(sdr["id"].ToString());
                        mac.IEVersion = sdr["IEVersion"].ToString();
                        mac.IpAddress = sdr["IpAddress"].ToString();
                        
                        mac.LastLogin = DateTime.Parse(sdr["LastLogin"].ToString());
                        mac.LocationId = int.Parse(sdr["LocationId"].ToString());
                        mac.LocationTitle = sdr["LocationTitle"].ToString();
                        mac.LSID = sdr["LSID"].ToString();
                        mac.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        mac.Memory = sdr["Memory"].ToString();
                        mac.OSInfo = sdr["OSInfo"].ToString();
                        mac.PCName = sdr["PCName"].ToString();
                        mac.Processes = sdr["Processes"].ToString();
                        mac.QLVersion = sdr["QLVersion"].ToString();
                        mac.WTSLocation = sdr["WTSLocation"].ToString();

                        try
                        {
                            if (sdr["IsExcluded"].ToString() == "" || sdr["IsExcluded"] == null)
                            {
                                mac.isExcluded = false;
                            }
                            else
                            {
                                mac.isExcluded = Boolean.Parse(sdr["IsExcluded"].ToString());
                            }
                        }
                        catch(NullReferenceException nre){
                            mac.isExcluded = false;
                        }

                        //m.Add(mac);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List
         
            return mac;
                       
        }//end selMachineById()

//        public static Machine selMachineById(int id)
//        {
//            string sql = "";
//            sql = @"SELECT mi.*, ml.Title as LocationTitle, wts.Device_Location as WTSLocation FROM SSO_MachineInventory mi
//                        LEFT OUTER JOIN SSO_MachineLocation ml ON mi.LocationId = ml.id
//                        LEFT OUTER JOIN SSO_WTSLocations wts ON wts.ClientName like mi.PCName
//                    WHERE mi.[id] = @id";
//            SqlParameter[] @params = new SqlParameter[1];
//            @params[0] = new SqlParameter("@id", id);

//            return BOHelper.FillObject<Machine>(DBManager.ExecCommandDR(sql, @params));
//        }

        public static List<Machine> selMachines()
        {
            List<Machine> m = new List<Machine>();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selMachines", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("pcName", pcName);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Machine mac = new Machine();

                        mac.AutoInstallVersion = sdr["AutoInstallVersion"].ToString();
                        //mac.AvgEMRLaunchLength = int.Parse(sdr["MRN"].ToString());
                        //mac.AvgLoginLength = int.Parse(sdr["AvgLoginLength"].ToString());
                        //mac.AvgRoamLength = int.Parse(sdr["AvgRoamLength"].ToString());
                        mac.ComputerInfo = sdr["ComputerInfo"].ToString();
                        mac.Description = sdr["Description"].ToString();
                        mac.ICAVersion = sdr["ICAVersion"].ToString();
                        mac.id = int.Parse(sdr["id"].ToString());
                        mac.IEVersion = sdr["IEVersion"].ToString();
                        mac.IpAddress = sdr["IpAddress"].ToString();

                        mac.LastLogin = DateTime.Parse(sdr["LastLogin"].ToString());
                        mac.LocationId = int.Parse(sdr["LocationId"].ToString());
                        mac.LocationTitle = sdr["LocationTitle"].ToString();
                        mac.LSID = sdr["LSID"].ToString();
                        mac.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        mac.Memory = sdr["Memory"].ToString();
                        mac.OSInfo = sdr["OSInfo"].ToString();
                        mac.PCName = sdr["PCName"].ToString();
                        mac.Processes = sdr["Processes"].ToString();
                        mac.QLVersion = sdr["QLVersion"].ToString();
                        mac.WTSLocation = sdr["WTSLocation"].ToString();

                        try
                        {
                            if (sdr["IsExcluded"].ToString() == "" || sdr["IsExcluded"] == null)
                            {
                                mac.isExcluded = false;
                            }
                            else
                            {
                                mac.isExcluded = Boolean.Parse(sdr["IsExcluded"].ToString());
                            }
                        }
                        catch (NullReferenceException nre)
                        {
                            //mac.isExcluded = false;
                        }

                        m.Add(mac);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return m;
        }

//        public static DataSet selSlowMachines()
//        {
//            string sql = @"SELECT AVG(up.lastLoginLength) / 1000 AS AVGlastLoginLength, AVG(up.lastEMRLaunchLength) / 1000 AS AVGlastEMRLaunchLength, AVG(up.lastRoamLength) / 1000 AS AVGlastRoamLength, COUNT(up.ID) AS USERCT, mi.id, mi.PCName, mi.LSID, mi.[Description], mi.[Memory], l.Device_Location as WTSLocation
//                              FROM [SSO_MachineInventory] mi
//                                  INNER JOIN [SSO_UserProfile] up ON up.lastLoginMachine LIKE mi.PCName 
//                                  LEFT OUTER JOIN [SSO_WTSLocations] l ON mi.PCName LIKE l.ClientName
//                              WHERE up.lastLoginLength > 5000
//                              GROUP BY mi.id, mi.PCName, mi.LSID, mi.[Description], mi.[Memory], l.Device_Location
//                              ORDER BY AVGLASTLOGINLENGTH DESC";
//            SqlParameter[] @params = new SqlParameter[1];

//            return DBManager.ExecCommandDS(sql, @params); //BOHelper.FillCollection<Machine>(DBManager.ExecCommandDR(sql, @params));
//        }

        public static List<Machine> selSlowestMachines(int LoginLengthLower = 3, int RoamLengthLower = 20, int LaunchLengthLower = 40)
        {
            List<Machine> m = new List<Machine>();

            DateTime todaysDate = DateTime.Now;
            DateTime lastMonth = todaysDate.AddMonths(-1);

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSlowestMachines", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("LoginLengthLower", LoginLengthLower);
                    sqlcmd.Parameters.AddWithValue("RoamLengthLower", RoamLengthLower);
                    sqlcmd.Parameters.AddWithValue("LaunchLengthLower", LaunchLengthLower);
                    sqlcmd.Parameters.AddWithValue("now", todaysDate);
                    sqlcmd.Parameters.AddWithValue("lastmonth", lastMonth);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Machine mac = new Machine();

                        mac.AvgEMRLaunchLength = double.Parse(sdr["AvgEMRLaunchLength"].ToString());
                        mac.AvgLoginLength = double.Parse(sdr["AvgLoginLength"].ToString());
                        mac.AvgRoamLength = double.Parse(sdr["AvgRoamLength"].ToString());
                        mac.Description = sdr["Description"].ToString();                        
                        mac.id = int.Parse(sdr["id"].ToString());                        
                        mac.LSID = sdr["LSID"].ToString();                       
                        mac.Memory = sdr["Memory"].ToString();                        
                        mac.PCName = sdr["PCName"].ToString();
                        mac.WTSLocation = sdr["WTSLocation"].ToString();

                        m.Add(mac);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List


            return m;
        }//end selSlowestMachines()

        public static List<Machine> selInvalidICAMachines()
        {
            List<Machine> m = new List<Machine>();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selInvalidICAMachines", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("pcName", pcName);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Machine mac = new Machine();
                                               
                        mac.Description = sdr["Description"].ToString();
                        mac.ICAVersion = sdr["ICAVersion"].ToString();
                        mac.id = int.Parse(sdr["id"].ToString());                       
                        mac.LastLogin = DateTime.Parse(sdr["LastLogin"].ToString());
                        mac.LSID = sdr["LSID"].ToString();
                        mac.PCName = sdr["PCName"].ToString();
                        mac.QLVersion = sdr["QLVersion"].ToString();
                        mac.WTSLocation = sdr["WTSLocation"].ToString();

                        m.Add(mac);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return m;
        }//end selInvalidICAMachines()

        #endregion

        #region "INSERTS"
        public static void insMachine(Machine Machine)
        {
            bool isSaved = true;

            SqlCommand sqlcmd = new SqlCommand();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insMachines";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("PCName", Machine.PCName);
                    sqlcmd.Parameters.AddWithValue("Description", Machine.Description ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("LSID", Machine.LSID ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("QLVersion", Machine.QLVersion ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("ICAVersion", Machine.ICAVersion ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("IEVersion", Machine.IEVersion ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("ComputerInfo", Machine.ComputerInfo ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("OSInfo", Machine.OSInfo ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("Processes", Machine.Processes ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("Memory", Machine.Memory ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("IpAddress", Machine.IpAddress ?? string.Empty);
                    sqlcmd.Parameters.AddWithValue("AutoInstallVersion", Machine.AutoInstallVersion);
                    sqlcmd.Parameters.AddWithValue("LastLogin", Machine.LastLogin);
                    sqlcmd.Parameters.AddWithValue("lup_dt", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("excluded", Machine.isExcluded);
                    
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    isSaved = false;
                }
            }//end sql
        }//end insMachine()

        public static void insMachineFromAD(Machine Machine)
        {
            bool isSaved = true;

            SqlCommand sqlcmd = new SqlCommand();

            if (Machine == null) return;
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insMachineFromAD";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("PCName", Machine.PCName);
                    sqlcmd.Parameters.AddWithValue("Description", Machine.Description ?? string.Empty);                   
                    sqlcmd.Parameters.AddWithValue("LastLogin", Machine.LastLogin);
                    sqlcmd.Parameters.AddWithValue("lup_dt", DateTime.Now);
                    
                    sqlcmd.Connection.Close();

                }
                catch (Exception e)
                {
                    isSaved = false;
                }
            }//end sql  
        }//end insMachinesFromAD()
        #endregion

        #region "UPDATES"
        public static void updAdminMachine(Machine Machine)
        {
            bool isSaved = true;

            SqlCommand sqlcmd = new SqlCommand();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updAdminMachine";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", Machine.id);
                    sqlcmd.Parameters.AddWithValue("LocationId", Machine.LocationId);
                    sqlcmd.Parameters.AddWithValue("lup_dt", DateTime.Now);
                    sqlcmd.Parameters.AddWithValue("excluded", Machine.isExcluded);
                    sqlcmd.Parameters.AddWithValue("ip", Machine.IpAddress);


                    sqlcmd.Connection.Close();

                }
                catch (Exception e)
                {
                    isSaved = false;
                }
            }//end sql                   
        }//end updAdminMachine()

        public static void updMachineLocationInBulk(string machines, int locationId)
        {
            bool isSaved = true;

            SqlCommand sqlcmd = new SqlCommand();
            
            char[] mac = {','};
            string[] name = machines.Split(mac);
            for (int i = 0; i < name.Length - 1; i++)
            {
                using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
                {
                    try
                    {
                        sqlcmd.Connection = sqlConn;
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.CommandText = "dbo.updMachineLocationInBulk";
                        sqlcmd.Connection.Open();

                        sqlcmd.Parameters.AddWithValue("@name", name[i]);
                        sqlcmd.Parameters.AddWithValue("@LocationId", locationId);
                        sqlcmd.Parameters.AddWithValue("@lup_dt", DateTime.Now);

                        sqlcmd.Connection.Close();

                    }
                    catch (Exception e)
                    {
                        isSaved = false;
                    }
                }//end sql                   
            }//end for
        }//end updMachineLocationInBulk()

        public static void delMachine(Machine Machine)
        {
            bool isSaved = true;

            SqlCommand sqlcmd = new SqlCommand();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delMachine";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("id", Machine.id);

                    sqlcmd.Connection.Close();

                }
                catch (Exception e)
                {
                    isSaved = false;
                }
            }//end sql 
        }//end delMachine()

        public static void delExtinctMachines(int days = 365)
        {
            bool isSaved = true;

            SqlCommand sqlcmd = new SqlCommand();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delExtinctMachines";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("days", days);

                    sqlcmd.Connection.Close();

                }
                catch (Exception e)
                {
                    isSaved = false;
                }
            }//end sql                   
        }//end delExtinctMachines()
        #endregion

        #region SystemInfo
        public static string GetIcaVersion()
        {
            try
            {
                string CitrixFileName = @"\Citrix\ICA Client\wfica32.exe";
                string x32Bit = string.Format(@"{0}{1}", @"C:\Program Files (x86)", CitrixFileName);
                string x64Bit = string.Format(@"{0}{1}", @"C:\Program Files", CitrixFileName);
                if (System.IO.File.Exists(x32Bit))
                    CitrixFileName = x32Bit;
                else if (System.IO.File.Exists(x64Bit))
                    CitrixFileName = x64Bit;

                return System.Diagnostics.FileVersionInfo.GetVersionInfo(CitrixFileName).ProductVersion;
            }
            catch { return "N/A"; }
        }
        public static string GetIEVersion()
        {
            try
            {
                return Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Internet Explorer").GetValue("Version").ToString();
            }
            catch { return "N/A"; }
        }
        public static string GetAutoInstallVersion()
        {
            try
            {
                string FileName = @"\Baptist Health\QL Auto Install\QuickLaunch.Install.exe";
                string x32Bit = string.Format(@"{0}{1}", @"C:\Program Files (x86)", FileName);
                string x64Bit = string.Format(@"{0}{1}", @"C:\Program Files", FileName);
                if (System.IO.File.Exists(x32Bit))
                    FileName = x32Bit;
                else if (System.IO.File.Exists(x64Bit))
                    FileName = x64Bit;

                return System.Diagnostics.FileVersionInfo.GetVersionInfo(FileName).ProductVersion;
            }
            catch { return "N/A"; }
        }
        public static string GetDefaultLSID()
        {
            try
            {
                return Microsoft.Win32.Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\Windows NT\CurrentVersion\Winlogon").GetValue("DefaultUserName").ToString().ToUpper();
            }
            catch { return "N/A"; }
        }
        #endregion
    }
}
