﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    [Serializable]
    public class Deployment : Abstract.IDataInfo
    {        
        #region Properties
        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the Description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the Install Message.
        /// </summary>
        public string InstallMessage { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build.
        /// </summary>
        public byte[] MSIBuild { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build file content type.
        /// </summary>
        public string MSIBuildContentType { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build file size.
        /// </summary>
        public string MSIBuildFileSize { get; set; }

        /// <summary>
        /// Gets/Sets the MSI Build version.
        /// </summary>
        public string MSIBuildVersion { get; set; }

        /// <summary>
        /// Gets/Sets the Locations
        /// </summary>
        public List<string> Locations { get; set; }
        
        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /// <summary>
        /// Gets/Sets the datetime the deployment was added
        /// </summary>
        public DateTime add_dt { get; set; }

        /// <summary>
        /// Gets/Sets the datetime the isPublished flag was set
        /// </summary>
        public DateTime publish_dt { get; set; }

        /// <summary>
        /// Gets/Sets the IsEverywhere indicator
        /// </summary>
        public bool IsEverywhere { get; set; }

        /// <summary>
        /// Gets/Sets the IsPublished indicator
        /// </summary>
        public bool IsPublished { get; set; }

        /// <summary>
        /// Gets/Sets the IsArchived indicator
        /// </summary>
        public bool IsArchived { get; set; }

 
        /* Display Fields */

        public int MachineCt { get; set; }
        /* End Display Fields */


        #endregion

        public Deployment() { }        

        #region "SELECTS"

        public static Deployment selDeploymentById(int id)
        {
            string sql = "";
            sql = @"SELECT * FROM SSO_Deployment dep                        
                    WHERE dep.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<Deployment>(DBManager.ExecCommandDR(sql, @params));
        }
        public static Deployment selPublishedDeployment(string Machine)
        { //NOTE IF THERE ARE NO LOCATION ASSOCIATED, IT IS ASSUMMED TO BE ALL LOCATIONS
            string sql = @"SELECT TOP 1 dep.id, dep.Title, dep.[Description], dep.MSIBuildVersion, dep.publish_dt, dep.IsPublished, dep.IsEverywhere, dep.IsArchived
                           FROM SSO_Deployment dep 
	                           LEFT OUTER JOIN (SELECT dl.DeploymentId, dl.LocationId FROM SSO_DeploymentLocation dl, SSO_MachineInventory mi WHERE mi.LocationId = dl.LocationId AND mi.PCName LIKE @Machine) loc ON dep.id = loc.DeploymentId
                           WHERE dep.IsArchived = 0 AND dep.IsPublished = 1 AND ((loc.LocationId IS NOT NULL AND dep.IsEverywhere = 0) OR dep.IsEverywhere = 1)
                           ORDER BY MSIBuildVersion DESC, publish_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@Machine", Machine);
            return BOHelper.FillObject<Deployment>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Deployment> selActiveDeployments()
        {
            string sql = @"SELECT dep.*, (SELECT COUNT(*) FROM SSO_MachineInventory mi WHERE mi.QLVersion like dep.MSIBuildVersion) as MachineCt FROM SSO_Deployment dep                                 
                            WHERE dep.IsArchived = 0 Order by MSIBuildVersion DESC, publish_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Deployment>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"
        public static string insDeployment(Deployment Deployment)
        {
            string sql = @"INSERT INTO SSO_Deployment ([Title]
                                                       ,[Description]
                                                       ,[InstallMessage]
                                                       ,[MSIBuild]
                                                       ,[MSIBuildContentType]
                                                       ,[MSIBuildFileSize]
                                                       ,[MSIBuildVersion]
                                                       ,[lup_user]
                                                       ,[publish_dt]
                                                       ,[IsEverywhere]
                                                       ,[IsPublished]
                                               )VALUES( @Title
                                                       ,@Description
                                                       ,@InstallMessage
                                                       ,@MSIBuild
                                                       ,@MSIBuildContentType
                                                       ,@MSIBuildFileSize
                                                       ,@MSIBuildVersion
                                                       ,@lup_user
                                                       ,@publish_dt
                                                       ,@IsEverywhere
                                                       ,@IsPublished); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[12];
            @params[0] = new SqlParameter("@Title", Deployment.Title);
            @params[1] = new SqlParameter("@Description", Deployment.Description);
            @params[2] = new SqlParameter("@InstallMessage", Deployment.InstallMessage);
            @params[3] = new SqlParameter("@MSIBuild", Deployment.MSIBuild);
            @params[4] = new SqlParameter("@MSIBuildContentType", Deployment.MSIBuildContentType);
            @params[5] = new SqlParameter("@MSIBuildFileSize", Deployment.MSIBuildFileSize);
            @params[6] = new SqlParameter("@MSIBuildVersion", Deployment.MSIBuildVersion);
            @params[7] = new SqlParameter("@lup_user", Deployment.lup_user);
            @params[8] = new SqlParameter("@publish_dt", Deployment.publish_dt);
            @params[9] = new SqlParameter("@IsPublished", Deployment.IsPublished);
            @params[10] = new SqlParameter("@IsEverywhere", Deployment.IsEverywhere);       

            return BOHelper.FillObject<Deployment>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updDeployment(Deployment Deployment)
        {
            string sql = @"UPDATE SSO_Deployment SET   [Title] = @Title
                                                      ,[Description] = @Description
                                                      ,[InstallMessage] = @InstallMessage
                                                      ,[MSIBuildVersion] = @MSIBuildVersion
                                                      ,[publish_dt] = @publish_dt
                                                      ,[IsPublished] = @IsPublished
                                                      ,[IsEverywhere] = @IsEverywhere
                                                      ,[lup_user] = @lup_user
                                                      ,[lup_dt] = @lup_dt 
                                                WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@id", Deployment.id);
            @params[1] = new SqlParameter("@Title", Deployment.Title);
            @params[2] = new SqlParameter("@Description", Deployment.Description);
            @params[3] = new SqlParameter("@InstallMessage", Deployment.InstallMessage);
            @params[4] = new SqlParameter("@MSIBuildVersion", Deployment.MSIBuildVersion);
            @params[5] = new SqlParameter("@lup_user", Deployment.lup_user);
            @params[6] = new SqlParameter("@lup_dt", DateTime.Now);
            @params[7] = new SqlParameter("@publish_dt", Deployment.publish_dt);
            @params[8] = new SqlParameter("@IsPublished", Deployment.IsPublished);
            @params[9] = new SqlParameter("@IsEverywhere", Deployment.IsEverywhere);       
            

            DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void delDeployment(Deployment Deployment)
        {
            string sql = @"UPDATE SSO_Deployment SET [IsArchived] = 1, [IsPublished] = 0, [lup_user] = @lup_user, [lup_dt] = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[4];
            @params[0] = new SqlParameter("@id", Deployment.id);
            @params[1] = new SqlParameter("@lup_user", Deployment.lup_user);
            @params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
