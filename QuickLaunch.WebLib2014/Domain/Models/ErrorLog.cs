﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class ErrorLog : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Exception.
        /// </summary>
        public string Exception { get; set; }

        /// <summary>
        /// Gets/Sets the QLVersion
        /// </summary>
        public string QLVersion { get; set; }

        /// <summary>
        /// Gets/Sets the serial number
        /// </summary>
        public string Serial { get; set; }

        /// <summary>
        /// Gets/Sets the LSID
        /// </summary>
        public string LSID { get; set; }

        /// <summary>
        /// Gets/Sets the Current UserId
        /// </summary>
        public string CurrentUserId { get; set; }

        /// <summary>
        /// Gets/Sets whether the error is fatal
        /// </summary>
        public bool IsFatal { get; set; }

        /// <summary>
        /// Gets/Sets the ICAVersion
        /// </summary>
        public string ICAVersion { get; set; }

        /// <summary>
        /// Gets/Sets the IEVersion
        /// </summary>
        public string IEVersion { get; set; }

        /// <summary>
        /// Gets/Sets the Application
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /* Display Fields */

        /// <summary>
        /// Gets/Sets the WTS Printer Location.
        /// </summary>
        public string WTSLocation { get; set; }

        /* End Display Fields */

        #endregion

        public ErrorLog() { }
        
        #region "SELECTS"
        public static ErrorLog selErrorLogById(int id)
        {
            string sql = "";
            sql = @"SELECT el.*, wts.Device_Location as WTSLocation FROM SSO_ErrorLog el
                        LEFT OUTER JOIN SSO_WTSLocations wts ON wts.ClientName like el.Serial
                    WHERE el.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<ErrorLog>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<ErrorLog> selErrorLogs(int MaxCt = 100)
        {
            string sql = @"SELECT TOP " + MaxCt + " * FROM SSO_ErrorLog order by lup_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<ErrorLog>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<ErrorLog> selErrorsByMachine(string machine, int MaxCt = 10)
        {
            string sql = @"SELECT TOP " + MaxCt + " * FROM SSO_ErrorLog WHERE [Serial] LIKE @Serial order by lup_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@Serial", machine);  
            return BOHelper.FillCollection<ErrorLog>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<ErrorLog> selErrorsByUser(string userId, int MaxCt = 10)
        {
            string sql = @"SELECT TOP " + MaxCt + " * FROM SSO_ErrorLog WHERE [CurrentUserId] LIKE @CurrentUserId order by lup_dt DESC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@CurrentUserId", userId);
            return BOHelper.FillCollection<ErrorLog>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insErrorLog(ErrorLog ErrorLog)
        {
            string sql = @"INSERT INTO SSO_ErrorLog ([Exception],[QLVersion],[Serial],[LSID],[CurrentUserId],[IsFatal],[ICAVersion],[IEVersion], [Application]) 
                                             VALUES (@Exception,@QLVersion,@Serial,@LSID,@CurrentUserId,@IsFatal,@ICAVersion,@IEVersion, @Application); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[10];

            @params[0] = new SqlParameter("@Exception", ErrorLog.Exception);
            @params[1] = new SqlParameter("@QLVersion", ErrorLog.QLVersion);
            @params[2] = new SqlParameter("@Serial", ErrorLog.Serial);
            @params[3] = new SqlParameter("@LSID", ErrorLog.LSID);
            @params[4] = new SqlParameter("@CurrentUserId", ErrorLog.CurrentUserId);
            @params[5] = new SqlParameter("@IsFatal", ErrorLog.IsFatal);
            @params[6] = new SqlParameter("@ICAVersion", ErrorLog.ICAVersion);
            @params[7] = new SqlParameter("@IEVersion", ErrorLog.IEVersion);
            @params[8] = new SqlParameter("@Application", ErrorLog.Application); 

            return BOHelper.FillObject<ErrorLog>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void delErrorLog(ErrorLog ErrorLog)
        {            
            string sql = @"DELETE FROM SSO_ErrorLog WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", ErrorLog.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delExtinctErrors(int days = 180)
        {
            string sql = @"DELETE FROM SSO_ErrorLog where [lup_dt] < dateadd(day, @Days, getdate())";
            SqlParameter[] @params = new SqlParameter[2];
            @params[0] = new SqlParameter("@Days", -days);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion


        public static void AddErrorToLog(Exception ex, string UserId, string App, bool isFatal)
        {
            try
            {
                insErrorLog(new ErrorLog
                {
                     Exception = ex.ToString()
                    ,CurrentUserId = UserId.ToUpper()
                    ,ICAVersion = Machine.GetIcaVersion()
                    ,IEVersion = Machine.GetIEVersion()
                    ,Serial = System.Environment.MachineName
                    ,QLVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()
                    ,IsFatal = isFatal
                    ,LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper()
                    ,Application = App
                });
            } catch {} //do nothing if network is unavailable
        }

    }
}
