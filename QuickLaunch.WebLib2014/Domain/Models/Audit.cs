﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{    
    public class Audit : Abstract.IDataInfo
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the TYPE.
        /// </summary>
        public string TYPE { get; set; }

        /// <summary>
        /// Gets/Sets the TableName.
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// Gets/Sets the [FieldName]
        /// </summary>
        public string FieldName { get; set; }
        
        /// <summary>
        /// Gets/Sets the [OldValue]
        /// </summary>
        public string OldValue { get; set; }

        /// <summary>
        /// Gets/Sets the [NewValue]
        /// </summary>
        public string NewValue { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime UpdateDate { get; set; }

        /// <summary>
        /// Gets/Sets the [UserName]
        /// </summary>
        public string UserName { get; set; }

        #endregion

        public Audit() { }
                        

        #region "SELECTS"

        public static Audit selAuditById(int id)
        {
            string sql = "";
            sql = @"SELECT * FROM SSO_Audit aud                        
                    WHERE aud.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<Audit>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Audit> selAuditByPKId(string tableId, string PK)
        {
            string sql = "";
            sql = @"SELECT * FROM SSO_Audit aud                        
                    WHERE aud.[TableName] LIKE @tableId AND aud.[PK] LIKE @PK
                    ORDER BY UpdateDate DESC";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@tableId", tableId);
            @params[1] = new SqlParameter("@PK", PK);

            return BOHelper.FillCollection<Audit>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<Audit> selAudit(string tableId)
        {
            string sql = @"SELECT aud.* FROM SSO_Audit aud                                 
                            WHERE aud.[TableName] LIKE @tableId AND aud.[PK] LIKE @PK
                            ORDER BY UpdateDate DESC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@tableId", tableId);
            return BOHelper.FillCollection<Audit>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"


        #endregion

        #region "UPDATES"
        public static void delExtinctAudit(int days = 365)
        {
            string sql = @"DELETE FROM SSO_Audit where [UpdateDate] < dateadd(day, @Days, getdate())";
            SqlParameter[] @params = new SqlParameter[2];
            @params[0] = new SqlParameter("@Days", -days);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
