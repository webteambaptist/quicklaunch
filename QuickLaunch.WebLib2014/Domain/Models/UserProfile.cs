﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    [Serializable]
    public class UserProfile : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the ntID.
        /// </summary>
        public string ntID { get; set; }

        /// <summary>
        /// Gets/Sets the user type. ie. Physician, Clinician
        /// </summary>
        public string userType { get; set; }

        /// <summary>
        /// Gets/Sets the user title
        /// </summary>
        public string userTitle { get; set; }

        /// <summary>
        /// Gets/Sets the user dept
        /// </summary>
        public string userDept { get; set; }

        /// <summary>
        /// Gets/Sets the Last login.
        /// </summary>
        public DateTime lastLogin { get; set; }

        /// <summary>
        /// Gets/Sets the last time the portal was launched.
        /// </summary>
        public DateTime lastPortalLaunch { get; set; }

        /// <summary>
        /// Gets/Sets the first time the portal was launched.
        /// </summary>
        public DateTime firstLogin { get; set; }
                
        /// <summary>
        /// Gets/Sets the auto launch app for the EMR.
        /// </summary>
        public string CitrixAutoLaunchApp { get; set; }

        /// <summary>
        /// Gets/Sets the forms Horizontal position Startup location.
        /// </summary>
        public string formHorizontalStartup { get; set; }

        /// <summary>
        /// Gets/Sets the forms Vertical position Startup location.
        /// </summary>
        public string formVerticalStartup { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lastUpdate { get; set; }

        /// <summary>
        /// Gets/Sets the users last login time length.
        /// </summary>
        public double lastLoginLength { get; set; }

        /// <summary>
        /// Gets/Sets the users last roam time length.
        /// </summary>
        public double lastRoamLength { get; set; }

        /// <summary>
        /// Gets/Sets the users last EMR launch time length (not including roams).
        /// </summary>
        public double lastEMRLaunchLength { get; set; }

        /// <summary>
        /// Gets/Sets the users last login machine name
        /// </summary>
        public string lastLoginMachine { get; set; }     

        /// <summary>
        /// Gets/Sets the user roaming count.
        /// </summary>
        public int roamingCt { get; set; }

        /// <summary>
        /// Gets/Sets the PowerChart Launch Ct.
        /// </summary>
        public int pcLaunchCt { get; set; }

        /// <summary>
        /// Gets/Sets the FirstNet Launch Ct.
        /// </summary>
        public int fnLaunchCt { get; set; }

        /// <summary>
        /// Gets/Sets the SurgiNet Launch Ct.
        /// </summary>
        public int snLaunchCt { get; set; }

        /// <summary>
        /// Gets/Sets the last Metric Count Clear
        /// </summary>
        public DateTime lastMetricCtClear { get; set; }


        /* Display Fields */
        /// <summary>
        /// Gets/Sets the Role count
        /// </summary>
        public int RoleCt { get; set; }
                
        /// <summary>
        /// Gets/Sets the Roles
        /// </summary>
        public List<Models.Role> Roles { get; set; }

        /// <summary>
        /// Gets/Sets the Role count
        /// </summary>
        public double Launch_Roam_Avg { get; set; }

        /// <summary>
        /// Gets/Sets the WTS Printer Location.
        /// </summary>
        public string WTSLocation { get; set; }

        public class UserMetrics : Abstract.IDataInfo
        {
            public double AvgLoginLength { get; set; }
            public double SumLoginLength { get; set; }
            public double AvgEMRLaunchLength { get; set; }
            public double SumEMRLaunchLength { get; set; }
            public double AvgRoamLength { get; set; }
            public double SumRoamLength { get; set; }            
            public double AvgRoamLaunchSavings { get; set; }
            public double SumRoamLaunchSavings { get; set; }
            public int SumRoamCt { get; set; }
            public int SumPCLaunchCt { get; set; }
            public int SumFNLaunchCt { get; set; }
            public int SumSNLaunchCt { get; set; }
            public int SumAutoLaunchCt { get; set; }
            public int SumQLUserCt { get; set; }
            public int SumNewUsersCt { get; set; }
            public int SumMachineCt { get; set; }
            public DateTime LastMetricClear { get; set; }     
        }
        /* End Display Fields */
        #endregion

        public UserProfile() { }
        
        #region "SELECTS"
        public static UserProfile selUserProfileById(string ntID)
        {
            string sql = "";
            sql = "SELECT * FROM SSO_UserProfile WHERE [NTID] LIKE @UserName";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@UserName", ntID + "%");                        
                       
            return BOHelper.FillObject<UserProfile>(DBManager.ExecCommandDR(sql, @params));
        }
        public static UserProfile selUserProfileById(int id)
        {
            string sql = "";
            sql = @"SELECT up.*, wts.Device_Location as WTSLocation FROM SSO_UserProfile up
                    LEFT OUTER JOIN SSO_WTSLocations wts ON wts.ClientName like up.lastLoginMachine
                    WHERE up.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<UserProfile>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<UserProfile> selUserProfiles()
        {
            string sql = "SELECT up.*, (CASE WHEN up.lastEMRLaunchLength > 0 AND up.lastRoamLength > 0 THEN up.lastEMRLaunchLength - up.lastRoamLength ELSE 0 END) as Launch_Roam_Avg, (SELECT COUNT(*) FROM SSO_UserProfileRole upr WHERE upr.ProfileId = up.id) as RoleCt FROM SSO_UserProfile up order by up.ntID";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<UserProfile>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<UserProfile> selUserProfilesByMachine(string machine)
        {
            string sql = @"SELECT up.*, (CASE WHEN up.lastEMRLaunchLength > 0 AND up.lastRoamLength > 0 THEN up.lastEMRLaunchLength - up.lastRoamLength ELSE 0 END) as Launch_Roam_Avg, (SELECT COUNT(*) FROM SSO_UserProfileRole upr WHERE upr.ProfileId = up.id) as RoleCt 
                            FROM SSO_UserProfile up 
                            WHERE [lastLoginMachine] LIKE @machine
                            order by lastLogin DESC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@machine", machine);
            return BOHelper.FillCollection<UserProfile>(DBManager.ExecCommandDR(sql, @params));
        }

        public static UserProfile.UserMetrics selUserMetrics()
        {
            string sql = @"SELECT count(*) as SumQLUserCt
	                            , SUM(lastLoginLength) / 1000 as SumLoginLength
	                            , AVG(lastLoginLength) / 1000 as AvgLoginLength
	                            , MAX(EL.SumEMRLaunchLength) / 1000 as SumEMRLaunchLength
	                            , MAX(EL.AvgEMRLaunchLength) / 1000 as AvgEMRLaunchLength
	                            , MAX(RL.SumRoamLength) / 1000 as SumRoamLength
	                            , MAX(RL.AvgRoamLength) / 1000 as AvgRoamLength
	                            , (MAX(SumEMRLaunchLength) - MAX(SumRoamLength)) / 1000 AS SumRoamLaunchSavings
	                            , (MAX(AvgEMRLaunchLength) - MAX(AvgRoamLength)) / 1000 AS AvgRoamLaunchSavings
	                            , SUM(roamingCt) as SumRoamCt
	                            , SUM(pcLaunchCt) as SumPCLaunchCt
	                            , SUM(fnLaunchCt) as SumFNLaunchCt
	                            , SUM(snLaunchCt) as SumSNLaunchCt
	                            , MAX(AL.SumAutoLaunchCt) as SumAutoLaunchCt
	                            , MAX(NU.SumNewUsersCt) as SumNewUsersCt
	                            , MAX(MC.SumMachineCt) as SumMachineCt
                                , MIN(lastMetricCtClear) as LastMetricClear
                            FROM [dbo].[SSO_UserProfile] UP
                              LEFT OUTER JOIN (SELECT SUM(lastEMRLaunchLength) as SumEMRLaunchLength, AVG(lastEMRLaunchLength) as AvgEMRLaunchLength FROM dbo.SSO_UserProfile WHERE lastEMRLaunchLength > 0) EL ON 1 = 1
                              LEFT OUTER JOIN (SELECT SUM(lastRoamLength) as SumRoamLength, AVG(lastRoamLength) as AvgRoamLength FROM dbo.SSO_UserProfile WHERE lastRoamLength > 0) RL ON 1 = 1
                              LEFT OUTER JOIN (SELECT COUNT(*) as SumAutoLaunchCt FROM dbo.SSO_UserProfile WHERE CitrixAutoLaunchApp IN ('PC','FN','SN')) AL ON 1 = 1
                              LEFT OUTER JOIN (SELECT COUNT(*) as SumNewUsersCt FROM dbo.SSO_UserProfile WHERE firstLogin >= dateadd(day, -30, getdate())) NU ON 1 = 1
                              LEFT OUTER JOIN (SELECT COUNT(*) as SumMachineCt FROM dbo.SSO_MachineInventory WHERE QLVersion IS NOT NULL) MC ON 1 = 1
                            WHERE lastLoginLength > 0;";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillObject<UserProfile.UserMetrics>(DBManager.ExecCommandDR(sql, @params));
        }

        public static UserProfile.UserMetrics selEventLogMetrics()
        {
            string sql = @"SELECT count(*) as EventCt
		                           , MAX(LG.Ct) as LoginCt
		                           , MAX(LG.SumLength) as SumLoginLength
		                           , MAX(LG.AvgLength) as AvgLoginLength
		                           , MAX(RM.Ct) as SumRoamCt
		                           , MAX(RM.SumLength) as SumRoamLength
		                           , MAX(RM.AvgLength) as AvgRoamLength
		                           , MAX(LC.Ct) as LaunchCt
		                           , MAX(LC.SumLength) as SumEMRLaunchLength
		                           , MAX(LC.AvgLength) as AvgEMRLaunchLength
		                           , (MAX(LC.SumLength) - MAX(RM.SumLength)) AS SumRoamLaunchSavings
		                           , (MAX(LC.AvgLength) - MAX(RM.AvgLength)) AS AvgRoamLaunchSavings
		                           , MAX(lcCount.SumPCLaunchCt) AS SumPCLaunchCt
		                           , MAX(lcCount.SumFNLaunchCt) AS SumFNLaunchCt
		                           , MAX(lcCount.SumSNLaunchCt) AS SumSNLaunchCt
		                           , MAX(AL.SumAutoLaunchCt) as SumAutoLaunchCt
		                           , MAX(NU.SumNewUsersCt) as SumNewUsersCt
		                           , MAX(MC.SumMachineCt) as SumMachineCt			                        
                                FROM [dbo].[SSO_EventLog] EL
                                  LEFT OUTER JOIN (SELECT COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM dbo.SSO_EventLog WHERE EventCode LIKE 'LOGIN') LG ON 1 = 1
                                  LEFT OUTER JOIN (SELECT COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM dbo.SSO_EventLog WHERE EventCode LIKE 'ROAM') RM ON 1 = 1
                                  LEFT OUTER JOIN (SELECT COUNT(LengthOfTime) as Ct, SUM(LengthOfTime) / 1000 as SumLength, AVG(LengthOfTime) / 1000 as AvgLength FROM dbo.SSO_EventLog WHERE EventCode LIKE 'LAUNCH') LC ON 1 = 1
                                  LEFT OUTER JOIN (SELECT SUM(CASE WHEN MenuItemId = 100 THEN 1 ELSE 0 END) AS SumPCLaunchCt, SUM(CASE WHEN MenuItemId = 101 THEN 1 ELSE 0 END) AS SumFNLaunchCt, SUM(CASE WHEN MenuItemId = 102 THEN 1 ELSE 0 END) AS SumSNLaunchCt FROM dbo.SSO_EventLog WHERE EventCode LIKE 'LAUNCH') lcCount ON 1 = 1
                                  LEFT OUTER JOIN (SELECT COUNT(*) as SumAutoLaunchCt FROM dbo.SSO_UserProfile WHERE CitrixAutoLaunchApp IN ('PC','FN','SN')) AL ON 1 = 1
                                  LEFT OUTER JOIN (SELECT COUNT(*) as SumNewUsersCt FROM dbo.SSO_UserProfile WHERE firstLogin >= dateadd(day, -30, getdate())) NU ON 1 = 1
                                  LEFT OUTER JOIN (SELECT COUNT(*) as SumMachineCt FROM dbo.SSO_MachineInventory WHERE QLVersion IS NOT NULL) MC ON 1 = 1
                        ;";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillObject<UserProfile.UserMetrics>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<UserProfile> selUserProfilesByRoleId(int RoleId)
        {
            string sql = "SELECT up.[id], up.[ntID] FROM SSO_UserProfile up, SSO_UserProfileRole upr WHERE upr.[ProfileId] = up.[id] AND upr.[RoleId] = @RoleId";
            SqlParameter[] @params = new SqlParameter[2];
            @params[0] = new SqlParameter("@RoleId", RoleId);

            return BOHelper.FillCollection<UserProfile>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Role> selUserProfileRoles(int ProfileId)
        {
            string sql = "SELECT [RoleId] as id FROM SSO_UserProfileRole WHERE [ProfileId] = @ProfileId";
            SqlParameter[] @params = new SqlParameter[2];
            @params[0] = new SqlParameter("@ProfileId", ProfileId);

            return BOHelper.FillCollection<Role>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<UserProfile> selUserProfilesInRole(int roleId)
        {
            string sql = @"SELECT up.*, (CASE WHEN up.lastEMRLaunchLength > 0 AND up.lastRoamLength > 0 THEN up.lastEMRLaunchLength - up.lastRoamLength ELSE 0 END) as Launch_Roam_Avg, (SELECT COUNT(*) FROM SSO_UserProfileRole upr WHERE upr.ProfileId = up.id) as RoleCt 
                            FROM SSO_UserProfile up 
                           INNER JOIN SSO_UserProfileRole upr ON upr.ProfileId = up.id AND upr.RoleId = @RoleId 
                           order by up.ntID";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@RoleId", roleId);
            return BOHelper.FillCollection<UserProfile>(DBManager.ExecCommandDR(sql, @params));
        }

        public static void delUserRoleId(int ProfileId, int RoleId)
        {
            string sql = @"DELETE FROM SSO_UserProfileRole WHERE ProfileId = @ProfileId AND RoleId = @RoleId";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@ProfileId", ProfileId);
            @params[1] = new SqlParameter("@RoleId", RoleId);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion

        #region "INSERTS"
        public static string insUserProfile(UserProfile UserProfile)
        {
            string sql = @"IF NOT EXISTS (SELECT * FROM SSO_UserProfile WHERE [NtId] = @NtID)
                                          INSERT INTO SSO_UserProfile ([NTID], [userType], [userTitle], [userDept], [lastLoginLength], [LASTLOGIN], [lastLoginMachine]) VALUES (@NtID, @UserType, @UserTitle, @UserDept, @LastLoginLength, @LastLogin, @lastLoginMachine); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@NtID", UserProfile.ntID);
            @params[1] = new SqlParameter("@UserType", UserProfile.userType);
            @params[2] = new SqlParameter("@UserTitle", UserProfile.userTitle);
            @params[3] = new SqlParameter("@UserDept", UserProfile.userDept);
            @params[4] = new SqlParameter("@LastLoginLength", UserProfile.lastLoginLength);
            @params[5] = new SqlParameter("@LastLogin", UserProfile.lastLogin);
            @params[6] = new SqlParameter("@lastLoginMachine", UserProfile.lastLoginMachine);

            return BOHelper.FillObject<UserProfile>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static string insUserProfileRole(int ProfileId, int RoleId, string Grantor)
        {
            string sql = @"INSERT INTO SSO_UserProfileRole ([RoleId], [ProfileId], [lup_user]) VALUES (@RoleId, @ProfileId, @lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@RoleId", RoleId);
            @params[1] = new SqlParameter("@ProfileId", ProfileId);
            @params[2] = new SqlParameter("@lup_user", Grantor);

            return BOHelper.FillObject<UserProfile>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        
        #endregion

        #region "UPDATES"
        public static void updUserProfile(UserProfile UserProfile)
        {
            string sql = @"UPDATE SSO_UserProfile SET [LASTLOGIN] = @LastLogin, [userType] = @UserType, [userTitle] = @UserTitle, [userDept] = @UserDept, [LastLoginLength] = @LastLoginLength, [lastLoginMachine] = @lastLoginMachine, lastUpdate = @lastUpdate WHERE [NTID] LIKE @NtID";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@NtId", UserProfile.ntID);
            @params[1] = new SqlParameter("@UserType", UserProfile.userType);
            @params[2] = new SqlParameter("@UserTitle", UserProfile.userTitle);
            @params[3] = new SqlParameter("@UserDept", UserProfile.userDept);
            @params[4] = new SqlParameter("@LastLogin", UserProfile.lastLogin);
            @params[5] = new SqlParameter("@LastLoginLength", UserProfile.lastLoginLength);
            @params[6] = new SqlParameter("@lastLoginMachine", UserProfile.lastLoginMachine);
            @params[7] = new SqlParameter("@lastUpdate", DateTime.Now);
            
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void updUserProfileLastPortalLaunch(UserProfile UserProfile)
        {
            string sql = @"UPDATE SSO_UserProfile SET [lastPortalLaunch] = @lastPortalLaunch WHERE [NTID] LIKE @NtId";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@NtId", UserProfile.ntID);
            @params[1] = new SqlParameter("@lastPortalLaunch", UserProfile.lastPortalLaunch);            

            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void updUserProfileSettings(UserProfile UserProfile)
        {
            string sql = @"UPDATE SSO_UserProfile SET [CitrixAutoLaunchApp] = @CitrixAutoLaunchApp, [formHorizontalStartup] = @formHorizontalStartup, [formVerticalStartup] = @formVerticalStartup, [lastUpdate] = @lastUpdate WHERE [NTID] LIKE @NtId";
            SqlParameter[] @params = new SqlParameter[7];
            @params[0] = new SqlParameter("@NtId", UserProfile.ntID);
            @params[1] = new SqlParameter("@CitrixAutoLaunchApp", UserProfile.CitrixAutoLaunchApp);
            @params[2] = new SqlParameter("@formHorizontalStartup", UserProfile.formHorizontalStartup);
            @params[3] = new SqlParameter("@formVerticalStartup", UserProfile.formVerticalStartup);
            @params[4] = new SqlParameter("@lastUpdate", DateTime.Now);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void updUserProfileRoamingMetrics(string UserId, bool Roaming = false, bool PowerChart = false, bool FirstNet = false, bool SurgiNet = false, double LastRoamLength = 0, double LastEMRLaunchLength = 0)
        {
            if (!Roaming && !PowerChart && !FirstNet && !SurgiNet && LastRoamLength == 0 && LastEMRLaunchLength == 0) { throw new Exception("No Profile Metrics passed [UserProfile.updUserProfileRoamingMetrics]"); }
            string sql = @"UPDATE SSO_UserProfile SET "
                           + (Roaming ? " [roamingCt] = [roamingCt] + 1 " : string.Empty)
                           + (PowerChart ? " [pcLaunchCt] = [pcLaunchCt] + 1 " : string.Empty)
                           + (FirstNet ? " [fnLaunchCt] = [fnLaunchCt] + 1 " : string.Empty)
                           + (SurgiNet ? " [snLaunchCt] = [snLaunchCt] + 1 " : string.Empty)
                           + (LastRoamLength > 0 ? " [lastRoamLength] = @LastRoamLength " : string.Empty)
                           + (LastEMRLaunchLength > 0 ? " [lastEMRLaunchLength] = @LastEMRLaunchLength " : string.Empty)
                           + " WHERE [NTID] LIKE @NtId";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@NtId", UserId);
            @params[1] = new SqlParameter("@LastRoamLength", LastRoamLength);
            @params[2] = new SqlParameter("@LastEMRLaunchLength", LastEMRLaunchLength);
            @params[3] = new SqlParameter("@lastUpdate", DateTime.Now);

            DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void updClearUserProfileRoamingMetrics(IEnumerable<UserProfile> profilesToClear, bool Roaming = false, bool PowerChart = false, bool FirstNet = false, bool SurgiNet = false, bool LastLoginLength = false, bool LastRoamLength = false, bool LastEMRLaunchLength = false)
        {
            if (!Roaming && !PowerChart && !FirstNet && !SurgiNet && !LastLoginLength && !LastRoamLength && !LastEMRLaunchLength) { throw new Exception("No Profile Metrics passed for clearing [UserProfile.updClearUserProfileRoamingMetrics]"); }
            string sql = string.Empty;
            foreach(UserProfile up in profilesToClear)
                sql += @"UPDATE SSO_UserProfile SET [lastMetricCtClear] = @lastUpdate "
                           + (LastLoginLength ? ", [lastLoginLength] = 0 " : string.Empty)
                           + (Roaming ? ", [roamingCt] = 0 " : string.Empty)
                           + (PowerChart ? ", [pcLaunchCt] = 0 " : string.Empty)
                           + (FirstNet ? ", [fnLaunchCt] = 0 " : string.Empty)
                           + (SurgiNet ? ", [snLaunchCt] = 0 " : string.Empty)
                           + (LastRoamLength ? ", [lastRoamLength] = 0 " : string.Empty)
                           + (LastEMRLaunchLength ? ", [lastEMRLaunchLength] = 0 " : string.Empty)
                           + " WHERE [NTID] LIKE '" + up.ntID + "'; ";
            SqlParameter[] @params = new SqlParameter[3];                                    
            @params[0] = new SqlParameter("@lastUpdate", DateTime.Now);

            if(!string.IsNullOrEmpty(sql))
                DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delUserProfile(UserProfile UserProfile)
        {
            throw new NotImplementedException();
            string sql = @"";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", UserProfile.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delUserProfileRolesByUser(int ProfileId)
        {
            string sql = @"DELETE FROM SSO_UserProfileRole WHERE ProfileId = @ProfileId";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@ProfileId", ProfileId);
            
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delExtinctUserProfiles(int days = 180)
        {
            string sql = @"DELETE FROM SSO_UserProfile where [lastlogin] < dateadd(day, @Days, getdate())";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@Days", -days);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion

        //public static void UpdateUserTypes()
        //{
        //    List<Models.UserProfile> up = Models.UserProfile.selUserProfiles();
        //    foreach (Models.UserProfile u in up)
        //    {

        //        var NetworkIsAvailable = ADHelper.GetGroups(new UserInfo { UserName = u.ntID, Domain = "BH" }, out ADgroups);
        //        if (ADHelper.IsInGroup(ADgroups, "SSO Physician Sharepoint"))
        //        {
        //            u.userType = "Physician";
        //        }
        //        else if (ADHelper.IsInGroup(ADgroups, "SSO Clinician Sharepoint"))
        //        {
        //            u.userType = "Clinician";
        //        }
        //        else if (ADHelper.IsInGroup(ADgroups, "SSO BHTHIN Sharepoint"))
        //        {
        //            u.userType = "BHTHIN";
        //        }

        //        Models.UserProfile.updUserProfile(u);
        //    }

        //}
    }
}
