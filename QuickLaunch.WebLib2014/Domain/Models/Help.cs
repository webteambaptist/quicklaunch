﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace QuickLaunch.Models
{
    public class Help : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the parent node ID.
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// Gets/Sets the title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Gets/Sets the Open Onload indicator
        /// </summary>
        public bool IsOpenOnload { get; set; }

        /// <summary>
        /// Gets/Sets the node selectable indicator
        /// </summary>
        public bool IsSelectable { get; set; }

        /// <summary>
        /// Gets/Sets the order ID.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets/Sets the lup user
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion

        public Help() { }

        #region "SELECTS"

        public static Help selHelpSubjects(int id)
        {
            Help h = new Help();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selHelpSubjects", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("pId", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        h.id = id;
                        h.ParentId = int.Parse(sdr["parentId"].ToString());
                        h.Body = sdr["Body"].ToString();
                        h.Title = sdr["Title"].ToString();
                        h.OrderId = int.Parse(sdr["OrderId"].ToString());
                        h.lup_user = sdr["lup_user"].ToString();
                        h.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        h.IsOpenOnload = Boolean.Parse(sdr["IsOpenOnload"].ToString());
                        h.IsSelectable = Boolean.Parse(sdr["IsSelectable"].ToString());


                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return h;
        }

        public static List<Help> selHelpTopic()
        {
            List<Help> h = new List<Help>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selHelpTopic", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Help ht = new Help();

                        ht.id = int.Parse(sdr["id"].ToString());
                        ht.ParentId = int.Parse(sdr["ParentId"].ToString());
                        ht.Body = sdr["Body"].ToString();
                        ht.Title = sdr["Title"].ToString();
                        ht.OrderId = int.Parse(sdr["OrderId"].ToString());
                        ht.lup_user = sdr["lup_user"].ToString();
                        ht.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        ht.IsOpenOnload = Boolean.Parse(sdr["IsOpenOnload"].ToString());
                        ht.IsSelectable = Boolean.Parse(sdr["IsSelectable"].ToString());


                        h.Add(ht);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return h;
        }

        public static List<Help> selParentId()
        {
            List<Help> h = new List<Help>();

            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selParentID", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Help ht = new Help();

                        ht.ParentId = int.Parse(sdr["ParentId"].ToString());

                        h.Add(ht);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return h;
        }

        public static Help selHelpById(int id)
        {
            string sql = "";
            sql = "SELECT * FROM SSO_Help WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<Help>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<Help> selHelps()
        {
            string sql = "SELECT * FROM SSO_Help order by ParentId, OrderId";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Help>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Help> selHelpByParentId(int ParentId)
        {
            string sql = "SELECT * FROM SSO_Help WHERE [ParentId] = @ParentId order by ParentId, OrderId";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@ParentId", ParentId);
            return BOHelper.FillCollection<Help>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insHelp(Help Help)
        {
            string sql = @"INSERT INTO SSO_Help ([ParentId],[Title],[Body],[IsOpenOnload],[IsSelectable],[OrderId],[lup_user]) VALUES (@ParentId, @Title, @Body, @IsOpenOnload, @IsSelectable, @OrderId, @lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[8];
            @params[0] = new SqlParameter("@ParentId", Help.ParentId);
            @params[1] = new SqlParameter("@Title", Help.Title);
            @params[2] = new SqlParameter("@Body", Help.Body);
            @params[3] = new SqlParameter("@IsOpenOnload", Help.IsOpenOnload);
            @params[4] = new SqlParameter("@IsSelectable", Help.IsSelectable);
            @params[5] = new SqlParameter("@OrderId", Help.OrderId);
            @params[6] = new SqlParameter("@lup_user", Help.lup_user);

            return BOHelper.FillObject<Help>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updHelp(Help Help)
        {
            string sql = @"UPDATE SSO_Help SET [ParentId] = @ParentId,[Title] = @Title,[Body] = @Body,[IsOpenOnload] = @IsOpenOnload,[IsSelectable] = @IsSelectable,[OrderId] = @OrderId, [lup_user] = @lup_user, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@Title", Help.Title);
            @params[1] = new SqlParameter("@Body", Help.Body);
            @params[2] = new SqlParameter("@IsOpenOnload", Help.IsOpenOnload);
            @params[3] = new SqlParameter("@IsSelectable", Help.IsSelectable);
            @params[4] = new SqlParameter("@OrderId", Help.OrderId);
            @params[5] = new SqlParameter("@lup_user", Help.lup_user);
            @params[6] = new SqlParameter("@lup_dt", Help.lup_dt);
            @params[7] = new SqlParameter("@ParentId", Help.ParentId);
            @params[8] = new SqlParameter("@id", Help.id);

            DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void updReorderHelpByParent(List<Help> help)
        {
            string sql = "";
            for (int i = 0; i < help.Count; i++)
                sql += @"UPDATE SSO_Help SET [OrderId] = " + (i + 1).ToString() + " WHERE [id] = " + help[i].id + ";";

            SqlParameter[] @params = new SqlParameter[2];
            if (sql.Length > 0) DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delHelp(Help Help)
        {
            string sql = @"DELETE FROM SSO_Help WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", Help.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
