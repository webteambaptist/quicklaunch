﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class SystemAlert : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public string CodeDescription { get; set; }

        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public int CodeId { get; set; }

        /// <summary>
        /// Gets/Sets the msg
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets/Sets the Application
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// Gets/Sets the Attachment Link
        /// </summary>
        public string AttachmentLink { get; set; }

        /// <summary>
        /// Gets/Sets the Icon
        /// </summary>
        public string Icon { get; set; }

        /// <summary>
        /// Gets/Sets the Font color
        /// </summary>
        public string FontColor { get; set; }

        /// <summary>
        /// Gets/Sets the font color
        /// </summary>
        public string FontColorTicker { get; set; }

        /// <summary>
        /// Gets/Sets the font color
        /// </summary>
        public string FontColorToast { get; set; }

        /// <summary>
        /// Gets/Sets the font color
        /// </summary>
        public string FontColorDialog { get; set; }

        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColor { get; set; }

        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColorTicker { get; set; }

        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColorToast { get; set; }

        /// <summary>
        /// Gets/Sets the back color for web list
        /// </summary>
        public string BackColorDialog { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is to be used by the net scaler
        /// </summary>
        public bool IsNetScalerVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is to be used by the web sharepoint physician portal
        /// </summary>
        public bool IsPhysicianPortalVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is to be used by the web sharepoint pfcs portal
        /// </summary>
        public bool IsPFCSVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is to be used by the bhthin portal
        /// </summary>
        public bool IsBHTHINVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is to be used by the CETech
        /// </summary>
        public bool IsCETechVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is to be used by the intranet
        /// </summary>
        public bool IsIntranetVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is to be used by the Novell enabled PC machines
        /// </summary>
        public bool IsPCVisible { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is shown as toast from task bar
        /// </summary>
        public bool IsToast { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is shown as Operating system bubble
        /// </summary>
        public bool IsOSBubble { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is shown as dialog
        /// </summary>
        public bool IsDialog { get; set; }

        /// <summary>
        /// Gets/Sets whether the alert is shown in the ticker marquee
        /// </summary>
        public bool IsTicker { get; set; }

        /// <summary>
        /// Gets/Sets the # of times an alert is shown
        /// </summary>
        public string Frequency { get; set; }

        /// <summary>
        /// Gets/Sets the start date time
        /// </summary>
        public DateTime StartDt { get; set; }

        /// <summary>
        /// Gets/Sets the start of the downtime date time
        /// </summary>
        public DateTime DowntimeDt { get; set; }

        /// <summary>
        /// Gets/Sets the end date time
        /// </summary>
        public DateTime EndDt { get; set; }

        /// <summary>
        /// Gets/Sets the disable key of type guid
        /// </summary>
        public Guid DisableKey { get; set; }

        /// <summary>
        /// Gets/Sets the enabled Quick Launch Citrix menu status
        /// </summary>
        public bool IsQLCitrixEnabled { get; set; }

        /// <summary>
        /// Gets/Sets the everywhere status
        /// </summary>
        public bool IsEverywhere { get; set; }

        /// <summary>
        /// Gets/Sets the enabled status
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Gets/Sets the last update user
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }


        /* Start User Fields */
        /// <summary>
        /// Gets/Sets the response existence 
        /// </summary>
        public bool ResponseExists { get; set; }

        /// <summary>
        /// Gets/Sets the response dialog confirmation 
        /// </summary>
        public bool ResponseDialogConfirmation { get; set; }

        /// <summary>
        /// Gets/Sets the response dialog confirmation 
        /// </summary>
        public DateTime ResponseLastUpdate { get; set; }

        /* Start User Fields */
        /// <summary>
        /// Gets/Sets the response count 
        /// </summary>
        public int ResponseCt { get; set; }
        /* End User Fields */

        #endregion

        public SystemAlert() { }

        #region "SELECTS"

        public static SystemAlert selSystemAlertById(int id)
        {
            string sql = "";
            sql = @"SELECT sa.*, cd.Code, cd.Description as CodeDescription FROM SSO_SystemAlert sa
                    LEFT OUTER JOIN SSO_SystemAlertCode cd ON cd.id = sa.CodeId
                    WHERE sa.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<SystemAlert>(DBManager.ExecCommandDR(sql, @params));
        }
        public static SystemAlert selSystemAlertByDisableKey(Guid guid)
        {
            string sql = "";
            sql = @"SELECT sa.*, cd.Code, cd.Description as CodeDescription FROM SSO_SystemAlert sa
                    LEFT OUTER JOIN SSO_SystemAlertCode cd ON cd.id = sa.CodeId
                    WHERE sa.[DisableKey] LIKE @guid";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@guid", guid);

            return BOHelper.FillObject<SystemAlert>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<SystemAlert> selSystemAlerts()
        {
            string sql = @"SELECT sa.*, cd.Code, cd.Description as CodeDescription, (SELECT COUNT(*) FROM SSO_SystemAlertResponse sar WHERE sar.SystemAlertId = sa.id) as ResponseCt FROM SSO_SystemAlert sa
                           LEFT OUTER JOIN SSO_SystemAlertCode cd ON cd.id = sa.CodeId
                           order by IsEnabled DESC, sa.endDt ASC";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<SystemAlert>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<SystemAlert> selActiveSystemAlerts(string NtId)
        {
            string sql = @"SELECT sa.*, cd.Code, cd.Description as CodeDescription, (CASE WHEN res.id IS NOT NULL THEN 'True' ELSE 'False' END) as ResponseExists, (CASE WHEN res.DialogAccept IS NOT NULL THEN res.DialogAccept ELSE 'False' END) as ResponseDialogConfirmation, (CASE WHEN res.DialogAccept IS NOT NULL THEN res.lup_dt ELSE NULL END) as ResponseLastUpdate 
                           FROM SSO_SystemAlert sa
                           LEFT OUTER JOIN SSO_SystemAlertCode cd ON cd.id = sa.CodeId
                           LEFT OUTER JOIN SSO_SystemAlertResponse res ON res.SystemAlertId = sa.id AND res.[NtId] = @NtId
                           WHERE sa.IsEnabled = 1 AND sa.StartDt <= getdate() AND sa.EndDt >= getdate() order by sa.endDt ASC";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@NtId", NtId);

            return BOHelper.FillCollection<SystemAlert>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insSystemAlert(SystemAlert SystemAlert)
        {
            string sql = @"INSERT INTO SSO_SystemAlert ([Name],[CodeId],[Message],[Application],[AttachmentLink],[Icon],[FontColor],[FontColorTicker],[FontColorToast],[FontColorDialog],[BackColor],[BackColorTicker],[BackColorToast],[BackColorDialog],[IsNetScalerVisible],[IsPhysicianPortalVisible],[IsPFCSVisible],[IsBHTHINVisible],[IsCETechVisible],[IsIntranetVisible],[IsPCVisible],[IsToast],[IsOSBubble],[IsDialog],[IsTicker],[Frequency],[StartDt],[DowntimeDt],[EndDt],[IsQLCitrixEnabled],[IsEverywhere],[IsEnabled],[lup_user]) 
                                                VALUES (@Name,@CodeId,@Message,@Application,@AttachmentLink,@Icon,@FontColor,@FontColorTicker,@FontColorToast,@FontColorDialog,@BackColor,@BackColorTicker,@BackColorToast,@BackColorDialog,@IsNetScalerVisible,@IsPhysicianPortalVisible,@IsPFCSVisible,@IsBHTHINVisible,@IsCETechVisible,@IsIntranetVisible,@IsPCVisible,@IsToast,@IsOSBubble,@IsDialog,@IsTicker,@Frequency,@StartDt,@DowntimeDt,@EndDt,@IsQLCitrixEnabled,@IsEverywhere,@IsEnabled,@lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[34];
            @params[0] = new SqlParameter("@Name", SystemAlert.Name);
            @params[1] = new SqlParameter("@CodeId", SystemAlert.CodeId);
            @params[2] = new SqlParameter("@Message", SystemAlert.Message);
            @params[3] = new SqlParameter("@Application", SystemAlert.Application);
            @params[4] = new SqlParameter("@AttachmentLink", SystemAlert.AttachmentLink);
            @params[5] = new SqlParameter("@Icon", SystemAlert.Icon);
            @params[6] = new SqlParameter("@FontColor", SystemAlert.FontColor);
            @params[7] = new SqlParameter("@FontColorTicker", SystemAlert.FontColorTicker);
            @params[8] = new SqlParameter("@FontColorToast", SystemAlert.FontColorToast);
            @params[9] = new SqlParameter("@FontColorDialog", SystemAlert.FontColorDialog);
            @params[10] = new SqlParameter("@BackColor", SystemAlert.BackColor);
            @params[11] = new SqlParameter("@BackColorTicker", SystemAlert.BackColorTicker);
            @params[12] = new SqlParameter("@BackColorToast", SystemAlert.BackColorToast);
            @params[13] = new SqlParameter("@BackColorDialog", SystemAlert.BackColorDialog);
            @params[14] = new SqlParameter("@IsNetScalerVisible", SystemAlert.IsNetScalerVisible);
            @params[15] = new SqlParameter("@IsPhysicianPortalVisible", SystemAlert.IsPhysicianPortalVisible);
            @params[16] = new SqlParameter("@IsPFCSVisible", SystemAlert.IsPFCSVisible);
            @params[17] = new SqlParameter("@IsBHTHINVisible", SystemAlert.IsBHTHINVisible);
            @params[18] = new SqlParameter("@IsCETechVisible", SystemAlert.IsCETechVisible);
            @params[19] = new SqlParameter("@IsIntranetVisible", SystemAlert.IsIntranetVisible);
            @params[20] = new SqlParameter("@IsPCVisible", SystemAlert.IsPCVisible);
            @params[21] = new SqlParameter("@IsToast", SystemAlert.IsToast);
            @params[22] = new SqlParameter("@IsOSBubble", SystemAlert.IsOSBubble);
            @params[23] = new SqlParameter("@IsDialog", SystemAlert.IsDialog);
            @params[24] = new SqlParameter("@IsTicker", SystemAlert.IsTicker);
            @params[25] = new SqlParameter("@Frequency", SystemAlert.Frequency);
            @params[26] = new SqlParameter("@StartDt", SystemAlert.StartDt);
            @params[27] = new SqlParameter("@DowntimeDt", SystemAlert.DowntimeDt);
            @params[28] = new SqlParameter("@EndDt", SystemAlert.EndDt);
            @params[29] = new SqlParameter("@IsEnabled", SystemAlert.IsEnabled);
            @params[30] = new SqlParameter("@IsEverywhere", SystemAlert.IsEverywhere);
            @params[31] = new SqlParameter("@IsQLCitrixEnabled", SystemAlert.IsQLCitrixEnabled);
            @params[32] = new SqlParameter("@lup_user", SystemAlert.lup_user);
            

            return BOHelper.FillObject<SystemAlert>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updSystemAlert(SystemAlert SystemAlert)
        {
            string sql = @"UPDATE SSO_SystemAlert SET [Name] = @Name,[CodeId] = @CodeId,[Message] = @Message,[Application] = @Application,[AttachmentLink] = @AttachmentLink,[Icon] = @Icon,[FontColor] = @FontColor,[FontColorTicker] = @FontColorTicker,[FontColorToast] = @FontColorToast,[FontColorDialog] = @FontColorDialog,[BackColor] = @BackColor,[BackColorTicker] = @BackColorTicker,[BackColorToast] = @BackColorToast,[BackColorDialog] = @BackColorDialog,[IsNetScalerVisible] = @IsNetScalerVisible,[IsPhysicianPortalVisible] = @IsPhysicianPortalVisible,[IsPFCSVisible] = @IsPFCSVisible,[IsBHTHINVisible] = @IsBHTHINVisible,[IsCETechVisible] = @IsCETechVisible,[IsIntranetVisible] = @IsIntranetVisible,[IsPCVisible] = @IsPCVisible,[IsToast] = @IsToast,[IsOSBubble] = @IsOSBubble,[IsDialog] = @IsDialog, [IsTicker] = @IsTicker,[Frequency] = @Frequency,[StartDt] = @StartDt,[DowntimeDt] = @DowntimeDt,[EndDt] = @EndDt,[IsQLCitrixEnabled] = @IsQLCitrixEnabled,[IsEverywhere] = @IsEverywhere,[IsEnabled] = @IsEnabled,[lup_user] = @lup_user, lup_dt = @lup_dt 
                            WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[36];
            @params[0] = new SqlParameter("@Name", SystemAlert.Name);
            @params[1] = new SqlParameter("@CodeId", SystemAlert.CodeId);
            @params[2] = new SqlParameter("@Message", SystemAlert.Message);
            @params[3] = new SqlParameter("@Application", SystemAlert.Application);
            @params[4] = new SqlParameter("@AttachmentLink", SystemAlert.AttachmentLink);
            @params[5] = new SqlParameter("@Icon", SystemAlert.Icon);
            @params[6] = new SqlParameter("@FontColor", SystemAlert.FontColor);
            @params[7] = new SqlParameter("@FontColorTicker", SystemAlert.FontColorTicker);
            @params[8] = new SqlParameter("@FontColorToast", SystemAlert.FontColorToast);
            @params[9] = new SqlParameter("@FontColorDialog", SystemAlert.FontColorDialog);
            @params[10] = new SqlParameter("@BackColor", SystemAlert.BackColor);
            @params[11] = new SqlParameter("@BackColorTicker", SystemAlert.BackColorTicker);
            @params[12] = new SqlParameter("@BackColorToast", SystemAlert.BackColorToast);
            @params[13] = new SqlParameter("@BackColorDialog", SystemAlert.BackColorDialog);
            @params[14] = new SqlParameter("@IsNetScalerVisible", SystemAlert.IsNetScalerVisible);
            @params[15] = new SqlParameter("@IsPhysicianPortalVisible", SystemAlert.IsPhysicianPortalVisible);
            @params[16] = new SqlParameter("@IsPFCSVisible", SystemAlert.IsPFCSVisible);
            @params[17] = new SqlParameter("@IsBHTHINVisible", SystemAlert.IsBHTHINVisible);
            @params[18] = new SqlParameter("@IsCETechVisible", SystemAlert.IsCETechVisible);
            @params[19] = new SqlParameter("@IsIntranetVisible", SystemAlert.IsIntranetVisible);
            @params[20] = new SqlParameter("@IsPCVisible", SystemAlert.IsPCVisible);
            @params[21] = new SqlParameter("@IsToast", SystemAlert.IsToast);
            @params[22] = new SqlParameter("@IsOSBubble", SystemAlert.IsOSBubble);
            @params[23] = new SqlParameter("@IsDialog", SystemAlert.IsDialog);
            @params[24] = new SqlParameter("@IsTicker", SystemAlert.IsTicker);
            @params[25] = new SqlParameter("@Frequency", SystemAlert.Frequency);
            @params[26] = new SqlParameter("@StartDt", SystemAlert.StartDt);
            @params[27] = new SqlParameter("@DowntimeDt", SystemAlert.DowntimeDt);
            @params[28] = new SqlParameter("@EndDt", SystemAlert.EndDt);
            @params[29] = new SqlParameter("@IsQLCitrixEnabled", SystemAlert.IsQLCitrixEnabled);
            @params[30] = new SqlParameter("@IsEverywhere", SystemAlert.IsEverywhere);
            @params[31] = new SqlParameter("@IsEnabled", SystemAlert.IsEnabled);
            @params[32] = new SqlParameter("@lup_user", SystemAlert.lup_user);
            @params[33] = new SqlParameter("@lup_dt", SystemAlert.lup_dt);
            @params[34] = new SqlParameter("@id", SystemAlert.id);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void updNewDisableKey(SystemAlert SystemAlert)
        {
            string sql = @"UPDATE SSO_SystemAlert SET [DisableKey] = @DisableKey,[lup_user] = @lup_user, lup_dt = @lup_dt 
                            WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@DisableKey", SystemAlert.DisableKey);
            @params[1] = new SqlParameter("@lup_user", SystemAlert.lup_user);
            @params[2] = new SqlParameter("@lup_dt", SystemAlert.lup_dt);
            @params[3] = new SqlParameter("@id", SystemAlert.id);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void updToggleIsEnabled(SystemAlert SystemAlert)
        {
            string sql = @"UPDATE SSO_SystemAlert SET [IsEnabled] = @IsEnabled,[lup_user] = @lup_user, lup_dt = @lup_dt 
                            WHERE [DisableKey] LIKE @DisableKey";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@DisableKey", SystemAlert.DisableKey);
            @params[1] = new SqlParameter("@lup_user", SystemAlert.lup_user);
            @params[2] = new SqlParameter("@lup_dt", SystemAlert.lup_dt);
            @params[3] = new SqlParameter("@IsEnabled", SystemAlert.IsEnabled);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlert(SystemAlert SystemAlert)
        {
            string sql = @"DELETE FROM SSO_SystemAlertResponse WHERE [SystemAlertId] = @id; DELETE FROM SSO_SystemAlert WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", SystemAlert.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion

        #region Collections
        public class EventCalendar
        {
            public int id { get; set; }
            public string title { get; set; }
            public bool allDay { get; set; }
            public string start { get; set; }
            public string end { get; set; }
            public string url { get; set; }
            public string textColor { get; set; }
            public string backgroundColor { get; set; }

            public EventCalendar() { }
        }
        #endregion
    }
}
