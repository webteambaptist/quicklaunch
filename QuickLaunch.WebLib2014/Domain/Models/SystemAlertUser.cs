﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class SystemAlertUser : BaseModel, Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the codeId
        /// </summary>
        public int CodeId { get; set; }

        /// <summary>
        /// Gets/Sets the userId
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets/Sets the userId
        /// </summary>
        public int LocationId { get; set; }

        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the userId
        /// </summary>
        public string Title { get; set; }

        #endregion

        public SystemAlertUser() { }
        
        #region "SELECTS"
        public static SystemAlertCode selSystemAlertUserCodeById(int id)
        {
            string sql = "";
            sql = @"SELECT sa.* FROM SSO_SystemAlertUserCodes sa                    
                    WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<SystemAlertCode> selSystemAlertUserCode()
        {
            string sql = @"SELECT sa.* FROM SSO_SystemAlertUserCodes sa 
                           order by Id";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insSystemAlertUserCode(SystemAlertUser sa)
        {
            string sql = @"INSERT INTO SSO_SystemAlertUserCodes ([CodeId],[UserId]) 
                                                VALUES (@CodeId,@UserId); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@CodeId", sa.CodeId);
            @params[1] = new SqlParameter("@UserId", sa.UserId);
            @params[2] = new SqlParameter("@lup_user", sa.lup_user);

            return BOHelper.FillObject<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static string insSystemAlertUserLocation(SystemAlertUser sa)
        {
            string sql = @"INSERT INTO SSO_SystemAlertUserLocations ([LocationId],[UserId]) 
                                                VALUES (@LocationId,@UserId); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@UserId", sa.UserId);
            @params[1] = new SqlParameter("@LocationId", sa.LocationId);            
            @params[2] = new SqlParameter("@lup_user", sa.lup_user);

            return BOHelper.FillObject<SystemAlertUser>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void delSystemAlertUserCodeByUser(SystemAlertUser usercode)
        {
            string sql = @"DELETE FROM SSO_SystemAlertUserCodes WHERE [UserId] = @UserId";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@UserId", usercode.UserId);
            @params[1] = new SqlParameter("@lup_user", usercode.lup_user);
            @params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlertUserLocationByUser(SystemAlertUser userloc)
        {
            string sql = @"DELETE FROM SSO_SystemAlertUserLocations WHERE [UserId] = @UserId";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@UserId", userloc.UserId);
            @params[1] = new SqlParameter("@lup_user", userloc.lup_user);
            @params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }   
        public static void delSystemAlertUserCode(SystemAlertUser usercode)
        {
            string sql = @"DELETE FROM SSO_SystemAlertUserCodes WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", usercode.id);
            @params[1] = new SqlParameter("@lup_user", usercode.lup_user);
            @params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlertUserLocation(SystemAlertUser userloc)
        {
            string sql = @"DELETE FROM SSO_SystemAlertUserLocations WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", userloc.id);
            @params[1] = new SqlParameter("@lup_user", userloc.lup_user);
            @params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }        
        #endregion


    }
}
