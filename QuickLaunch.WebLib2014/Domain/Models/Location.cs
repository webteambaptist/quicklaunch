﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class Location : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the Desc.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }


        //Start Display fields
        public int MachineCt { get; set; }
        //End Display fields

        public class DeploymentLocation : Location
        {
            public int DeploymentId { get; set; }
            public int LocationId { get; set; }
        }
        public class AlertLocation : Location
        {
            public int AlertId { get; set; }
            public int LocationId { get; set; }
        }
        public class LocationIp : Location
        {
            public int LocationId { get; set; }
            public string Ip { get; set; }
            public bool IsExcluded { get; set; }
        }
        public class WTSLocation : Abstract.IDataInfo
        {
            public string ClientName { get; set; }
            public string Device_Location { get; set; }
            public string Default_Printer { get; set; }
            public DateTime lup_dt { get; set; }
        }
        #endregion

        public Location() { }


        #region "SELECTS"

        public static Location selLocationById(int id)
        {
            string sql = "";
            sql = @"SELECT *, (SELECT COUNT(*) FROM SSO_MachineInventory mi WHERE mi.LocationId = loc.id) as MachineCt FROM SSO_MachineLocation loc                        
                    WHERE loc.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<Location>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Location> selLocations()
        {
            string sql = @"SELECT loc.*, (SELECT COUNT(*) FROM SSO_MachineInventory mi WHERE mi.LocationId = loc.id) as MachineCt FROM SSO_MachineLocation loc                                 
                           order by title";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Location>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<LocationIp> selIpByLocation(int LocationId)
        {
            string sql = @"SELECT loc.* FROM SSO_MachineLocationIp loc                                 
                           WHERE LocationId = @LocationId
                           order by Ip";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@LocationId", LocationId);

            return BOHelper.FillCollection<LocationIp>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<DeploymentLocation> selDeployLocations(int DeployId)
        {
            string sql = @"SELECT dl.id, loc.Title, loc.Description, loc.lup_dt FROM SSO_MachineLocation loc                                 
                                INNER JOIN SSO_DeploymentLocation dl ON loc.id = dl.LocationId
                           WHERE dl.DeploymentId = @DeployId
                           order by title";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@DeployId", DeployId);

            return BOHelper.FillCollection<DeploymentLocation>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Location> selSystemAlertUserLocations(int UserId)
        {
            string sql = @"SELECT ul.id, loc.Title, loc.Description, loc.lup_dt 
                           FROM SSO_MachineLocation loc                                 
                                INNER JOIN SSO_SystemAlertUserLocations ul ON loc.id = ul.LocationId
                           WHERE ul.UserId = @UserId
                           order by title";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@UserId", UserId);

            return BOHelper.FillCollection<Location>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Location> selSystemAlertUserLocationsWithLocId(int UserId)
        {
            string sql = @"SELECT loc.id, loc.Title, loc.Description, loc.lup_dt 
                           FROM SSO_MachineLocation loc                                 
                                INNER JOIN SSO_SystemAlertUserLocations ul ON loc.id = ul.LocationId
                           WHERE ul.UserId = @UserId
                           order by title";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@UserId", UserId);

            return BOHelper.FillCollection<Location>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Location> selAlertLocations(int AlertId)
        {
            string sql = @"SELECT loc.id, loc.Title, loc.Description, loc.lup_dt FROM SSO_MachineLocation loc                                 
                                INNER JOIN SSO_SystemAlertLocation dl ON loc.id = dl.LocationId
                           WHERE dl.AlertId = @AlertId
                           order by title";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@AlertId", AlertId);

            return BOHelper.FillCollection<Location>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Location.WTSLocation> selWTSLocations()
        {
            string sql = @"SELECT loc.ClientName, loc.Device_Location, loc.Default_Printer, loc.lup_dt FROM SSO_WTSLocations loc                                 
                           order by clientname";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Location.WTSLocation>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Location.WTSLocation> selMasterWTSLocations()
        {
            string sql = @"SELECT loc.ClientName, loc.Device_Location, loc.Default_Printer FROM Locations loc                                 
                           order by clientname";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Location.WTSLocation>(DBManagerWTS.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insLocation(Location Location)
        {
            string sql = @"INSERT INTO SSO_MachineLocation ([Title],[Description],[lup_user]) VALUES (@Title,@Description,@lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@Title", Location.Title);
            @params[1] = new SqlParameter("@Description", Location.Description);
            @params[2] = new SqlParameter("@lup_user", Location.lup_user);

            return BOHelper.FillObject<Location>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static string insLocationIp(LocationIp LocationIp)
        {
            string sql = @"INSERT INTO SSO_MachineLocationIp ([LocationId],[Ip],[IsExcluded]) VALUES (@LocationId,@Ip,@IsExcluded); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[4];
            @params[0] = new SqlParameter("@LocationId", LocationIp.LocationId);
            @params[1] = new SqlParameter("@Ip", LocationIp.Ip);
            @params[2] = new SqlParameter("@IsExcluded", LocationIp.IsExcluded);

            return BOHelper.FillObject<LocationIp>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static string insDeploymentLocation(DeploymentLocation dl)
        {
            string sql = @"INSERT INTO SSO_DeploymentLocation ([DeploymentId], [LocationId]) VALUES (@DeploymentId,@LocationId); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@DeploymentId", dl.DeploymentId);
            @params[1] = new SqlParameter("@LocationId", dl.LocationId);

            return BOHelper.FillObject<Location>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static string insAlertLocation(AlertLocation dl)
        {
            string sql = @"INSERT INTO SSO_SystemAlertLocation ([AlertId], [LocationId]) VALUES (@AlertId,@LocationId); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@AlertId", dl.AlertId);
            @params[1] = new SqlParameter("@LocationId", dl.LocationId);

            return BOHelper.FillObject<Location>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static void insSSOWTSLocation(Location.WTSLocation Location)
        {
            string sql = @"IF NOT EXISTS (SELECT * FROM SSO_WTSLocations WHERE [ClientName] = @ClientName)
                                          INSERT INTO SSO_WTSLocations ([ClientName], [Device_Location], [Default_Printer]) VALUES (@ClientName, @Device_Location, @Default_Printer)
                           ELSE IF EXISTS (SELECT 1 FROM SSO_WTSLocations WHERE [ClientName] = @ClientName AND ([Device_Location] NOT LIKE @Device_Location OR [Default_Printer] NOT LIKE @Default_Printer))
                                          UPDATE SSO_WTSLocations SET [Device_Location] = @Device_Location                                                                          
                                                                     ,[Default_Printer] = @Default_Printer
                                                                     ,[lup_dt] = @lup_dt
                                                    WHERE [ClientName] LIKE @ClientName;";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@ClientName", Location.ClientName);
            @params[1] = new SqlParameter("@Device_Location", Location.Device_Location);
            @params[2] = new SqlParameter("@Default_Printer", Location.Default_Printer);
            @params[3] = new SqlParameter("@lup_dt", DateTime.Now);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion

        #region "UPDATES"
        public static void updLocation(Location Location)
        {
            string sql = @"UPDATE SSO_MachineLocation SET [Title] = @Title,[Description] = @Description,[lup_user] = @lup_user, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@Title", Location.Title);
            @params[1] = new SqlParameter("@Description", Location.Description);
            @params[2] = new SqlParameter("@lup_user", Location.lup_user);
            @params[3] = new SqlParameter("@lup_dt", Location.lup_dt);
            @params[4] = new SqlParameter("@id", Location.id);

            DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void delLocation(Location Location)
        {
            string sql = @"DELETE FROM SSO_DeploymentLocation WHERE [LocationId] = @id; DELETE FROM SSO_MachineLocationIp WHERE [LocationId] = @id; DELETE FROM SSO_MachineLocation WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", Location.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delLocationIp(LocationIp ip)
        {
            string sql = @"DELETE FROM SSO_MachineLocationIp WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", ip.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delDeploymentLocation(DeploymentLocation dl)
        {
            string sql = @"DELETE FROM SSO_DeploymentLocation WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", dl.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delAlertLocation(AlertLocation dl)
        {
            string sql = @"DELETE FROM SSO_SystemAlertLocation WHERE [LocationId] = @LocationId AND [AlertId] = @AlertId";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@LocationId", dl.LocationId);
            @params[1] = new SqlParameter("@AlertId", dl.AlertId);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
