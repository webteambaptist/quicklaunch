﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;
using System.Configuration;
namespace QuickLaunch.Models
{
    [Serializable]
    public class Report : Abstract.IDataInfo
    {        
        #region Properties
        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the sub Title.
        /// </summary>
        public string SubTitle { get; set; }

        /// <summary>
        /// Gets/Sets the Type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets/Sets the Definition
        /// </summary>
        public string Definition { get; set; }

        /// <summary>
        /// Gets/Sets the Sql.
        /// </summary>
        public string Sql { get; set; }

        /// <summary>
        /// Gets/Sets the Email Recipients
        /// </summary>
        public string EmailRecipients { get; set; }

        /// <summary>
        /// Gets/Sets the Email recipients CC'd
        /// </summary>
        public string EmailCC { get; set; }

        /// <summary>
        /// Gets/Sets the Email Msg
        /// </summary>
        public string EmailMsg { get; set; }

        /// <summary>
        /// Gets/Sets the Email Subject
        /// </summary>
        public string EmailSubject { get; set; }

        /// <summary>
        /// Gets/Sets the Is Excel Attachment
        /// </summary>
        public bool IsExcelAttachment { get; set; }

        /// <summary>
        /// Gets/Sets the Is Transposed
        /// </summary>
        public bool IsTransposed { get; set; }

        /// <summary>
        /// Gets/Sets the IsRunSun
        /// </summary>
        public bool IsRunSun { get; set; }

        /// <summary>
        /// Gets/Sets the IsRunMon
        /// </summary>
        public bool IsRunMon { get; set; }

        /// <summary>
        /// Gets/Sets the IsRunTues
        /// </summary>
        public bool IsRunTues { get; set; }

        /// <summary>
        /// Gets/Sets the IsRunWed
        /// </summary>
        public bool IsRunWed { get; set; }

        /// <summary>
        /// Gets/Sets the IsRunThur
        /// </summary>
        public bool IsRunThur { get; set; }

        /// <summary>
        /// Gets/Sets the IsRunFri
        /// </summary>
        public bool IsRunFri { get; set; }

        /// <summary>
        /// Gets/Sets the IsRunSat
        /// </summary>
        public bool IsRunSat { get; set; }

        /// <summary>
        /// Gets/Sets the Frequency
        /// </summary>
        public string Frequency { get; set; }

        /// <summary>
        /// Gets/Sets the StartDt
        /// </summary>
        public DateTime StartDt { get; set; }

        /// <summary>
        /// Gets/Sets the LastRunDt
        /// </summary>
        public DateTime LastRunDt { get; set; }

        /// <summary>
        /// Gets/Sets the LastSentDt
        /// </summary>
        public DateTime LastSentDt { get; set; }

        /// <summary>
        /// Gets/Sets the IsEnabled
        /// </summary>
        public bool IsEnabled { get; set; }

        /// <summary>
        /// Gets/Sets the notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /// <summary>
        /// Gets/Sets the datetime the Report was added
        /// </summary>
        public DateTime add_dt { get; set; }

        /// <summary>
        /// Gets/Sets the IsArchived indicator
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Gets/Sets the IsArchived indicator
        /// </summary>
        public string ReportURL { get; set; }

        /* Internal Properties */
        public string FileName { get { return Title.Replace(" ", "-") + "_" + DateTime.Now.ToString().Replace(" ", "-"); } }
        public ArrayList DataSource { get; set; }
        public Exception Exception { get; set; }
        public List<Report.Parameter> Parameters { get; set; }
        public List<Report.Column> Columns { get; set; }
        public int ResultCt { get; set; } //History column as well
        /* End Internal Properties */
 
        /* Display Fields */
        
        /* End Display Fields */

        #endregion

        public Report() { }        

        #region "SELECTS"

        public static Report selReportById(int id)
        {
            Report rep = new Report();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selReportById", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        //Report r = new Report();

                        rep.add_dt = DateTime.Parse(sdr["add_dt"].ToString());
                        rep.Title = sdr["Title"].ToString();
                        rep.SubTitle = sdr["SubTitle"].ToString();
                        rep.Type = sdr["Type"].ToString();
                        rep.ReportURL = sdr["ReportURL"].ToString();
                        rep.EmailRecipients = sdr["EmailRecipients"].ToString();
                        rep.EmailCC = sdr["EmailCC"].ToString();
                        rep.EmailMsg = sdr["EmailMsg"].ToString();
                        rep.EmailSubject = sdr["EmailSubject"].ToString();
                        rep.IsExcelAttachment = Boolean.Parse(sdr["IsExcelAttachment"].ToString());
                        rep.IsTransposed = Boolean.Parse(sdr["IsTransposed"].ToString());
                        rep.IsRunSun = Boolean.Parse(sdr["IsRunSun"].ToString());
                        rep.IsRunMon = Boolean.Parse(sdr["IsRunMon"].ToString());
                        rep.IsRunTues = Boolean.Parse(sdr["IsRunTues"].ToString());
                        rep.IsRunWed = Boolean.Parse(sdr["IsRunWed"].ToString());
                        rep.IsRunThur = Boolean.Parse(sdr["IsRunThur"].ToString());
                        rep.IsRunFri = Boolean.Parse(sdr["IsRunFri"].ToString());
                        rep.IsRunSat = Boolean.Parse(sdr["IsRunSat"].ToString());
                        rep.Frequency = sdr["Frequency"].ToString();
                        rep.StartDt = DateTime.Parse(sdr["StartDt"].ToString());
                        rep.LastRunDt = DateTime.Parse(sdr["LastRunDt"].ToString());
                        try
                        {
                            rep.LastSentDt = DateTime.Parse(sdr["LastSentDt"].ToString());
                        } catch{}

                        rep.IsEnabled = Boolean.Parse(sdr["IsEnabled"].ToString());
                        rep.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        rep.lup_user = sdr["lup_dt"].ToString();
                        rep.Notes = sdr["Notes"].ToString();
                        rep.Sql = sdr["Sql"].ToString();
                        rep.Definition = sdr["Definition"].ToString();

                        //rep.Add(r);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return rep;
        }
        public static List<Report> selReports()
        {
            List<Report> rep = new List<Report>();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selReports", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Report r = new Report();

                        r.id = int.Parse(sdr["id"].ToString());
                        r.add_dt = DateTime.Parse(sdr["add_dt"].ToString());
                        r.Title = sdr["Title"].ToString();
                        r.SubTitle = sdr["SubTitle"].ToString();
                        r.Type = sdr["Type"].ToString();
                        r.ReportURL = sdr["ReportURL"].ToString();
                        r.EmailRecipients = sdr["EmailRecipients"].ToString();
                        r.EmailCC = sdr["EmailCC"].ToString();
                        r.EmailMsg = sdr["EmailMsg"].ToString();
                        r.EmailSubject = sdr["EmailSubject"].ToString();
                        r.IsExcelAttachment = Boolean.Parse(sdr["IsExcelAttachment"].ToString());
                        r.IsTransposed = Boolean.Parse(sdr["IsTransposed"].ToString());
                        r.IsRunSun = Boolean.Parse(sdr["IsRunSun"].ToString());
                        r.IsRunMon = Boolean.Parse(sdr["IsRunMon"].ToString());
                        r.IsRunTues = Boolean.Parse(sdr["IsRunTues"].ToString());
                        r.IsRunWed = Boolean.Parse(sdr["IsRunWed"].ToString());
                        r.IsRunThur = Boolean.Parse(sdr["IsRunThur"].ToString());
                        r.IsRunFri = Boolean.Parse(sdr["IsRunFri"].ToString());
                        r.IsRunSat = Boolean.Parse(sdr["IsRunSat"].ToString());
                        r.Frequency = sdr["Frequency"].ToString();
                        r.StartDt = DateTime.Parse(sdr["StartDt"].ToString());
                        r.LastRunDt = DateTime.Parse(sdr["LastRunDt"].ToString());

                        try
                        {
                            r.LastSentDt = DateTime.Parse(sdr["LastSentDt"].ToString());
                        }
                        catch { }
                        
                        r.IsEnabled = Boolean.Parse(sdr["IsEnabled"].ToString());
                        r.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        r.lup_user = sdr["lup_user"].ToString();
                        r.Notes = sdr["Notes"].ToString();
                        r.Sql = sdr["Sql"].ToString();
                        r.Definition = sdr["Definition"].ToString();

                        rep.Add(r);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return rep;
        }
        public static List<Report> selScheduledReports()
        {
            List<Report> rep = new List<Report>();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selScheduledReports", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    //sqlcmd.Parameters.AddWithValue("id", id);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Report r = new Report();

                        r.add_dt = DateTime.Parse(sdr["add_dt"].ToString());
                        r.Title = sdr["Title"].ToString();
                        r.SubTitle = sdr["SubTitle"].ToString();
                        r.Type = sdr["Type"].ToString();
                        r.ReportURL = sdr["ReportURL"].ToString();
                        r.EmailRecipients = sdr["EmailRecipients"].ToString();
                        r.EmailCC = sdr["EmailCC"].ToString();
                        r.EmailMsg = sdr["EmailMsg"].ToString();
                        r.EmailSubject = sdr["EmailSubject"].ToString();
                        r.IsExcelAttachment = Boolean.Parse(sdr["IsExcelAttachment"].ToString());
                        r.IsTransposed = Boolean.Parse(sdr["IsTransposed"].ToString());
                        r.IsRunSun = Boolean.Parse(sdr["IsRunSun"].ToString());
                        r.IsRunMon = Boolean.Parse(sdr["IsRunMon"].ToString());
                        r.IsRunTues = Boolean.Parse(sdr["IsRunTues"].ToString());
                        r.IsRunWed = Boolean.Parse(sdr["IsRunWed"].ToString());
                        r.IsRunThur = Boolean.Parse(sdr["IsRunThur"].ToString());
                        r.IsRunFri = Boolean.Parse(sdr["IsRunFri"].ToString());
                        r.IsRunSat = Boolean.Parse(sdr["IsRunSat"].ToString());
                        r.Frequency = sdr["Frequency"].ToString();
                        r.StartDt = DateTime.Parse(sdr["StartDt"].ToString());
                        r.LastRunDt = DateTime.Parse(sdr["LastRunDt"].ToString());

                        try
                        {
                            r.LastSentDt = DateTime.Parse(sdr["LastSentDt"].ToString());
                        }
                        catch { }

                        r.IsEnabled = Boolean.Parse(sdr["IsEnabled"].ToString());
                        r.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        r.lup_user = sdr["lup_user"].ToString();
                        r.Notes = sdr["Notes"].ToString();
                        r.Sql = sdr["Sql"].ToString();
                        r.Definition = sdr["Definition"].ToString();

                        rep.Add(r);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return rep;


//            string sql = @"SELECT * FROM SSO_Report rep
//                           WHERE Frequency NOT LIKE 'Ad-Hoc' AND LEN(EmailRecipients) > 0                                 
//                            Order by LastRunDt DESC";
//            SqlParameter[] @params = new SqlParameter[1];

//            return BOHelper.FillCollection<Report>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Report.ReportHistory> selReportHistory(int ReportId, int MaxCt = 200)
        {
            List<Report.ReportHistory> rep = new List<Report.ReportHistory>();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selReportHistory", sqlConn);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("ReportId", ReportId);
                    sqlcmd.Parameters.AddWithValue("MaxCt", MaxCt);
                    SqlDataReader sdr = sqlcmd.ExecuteReader();

                    while (sdr.Read())
                    {
                        Report.ReportHistory r = new Report.ReportHistory();

                        r.add_dt = DateTime.Parse(sdr["add_dt"].ToString());
                        r.Title = sdr["Title"].ToString();
                        r.SubTitle = sdr["SubTitle"].ToString();
                        r.Type = sdr["Type"].ToString();
                        r.ReportURL = sdr["ReportURL"].ToString();
                        r.EmailRecipients = sdr["EmailRecipients"].ToString();
                        r.EmailCC = sdr["EmailCC"].ToString();
                        r.EmailMsg = sdr["EmailMsg"].ToString();
                        r.EmailSubject = sdr["EmailSubject"].ToString();
                        r.IsExcelAttachment = Boolean.Parse(sdr["IsExcelAttachment"].ToString());
                        r.IsTransposed = Boolean.Parse(sdr["IsTransposed"].ToString());
                        r.IsRunSun = Boolean.Parse(sdr["IsRunSun"].ToString());
                        r.IsRunMon = Boolean.Parse(sdr["IsRunMon"].ToString());
                        r.IsRunTues = Boolean.Parse(sdr["IsRunTues"].ToString());
                        r.IsRunWed = Boolean.Parse(sdr["IsRunWed"].ToString());
                        r.IsRunThur = Boolean.Parse(sdr["IsRunThur"].ToString());
                        r.IsRunFri = Boolean.Parse(sdr["IsRunFri"].ToString());
                        r.IsRunSat = Boolean.Parse(sdr["IsRunSat"].ToString());
                        r.Frequency = sdr["Frequency"].ToString();
                        r.StartDt = DateTime.Parse(sdr["StartDt"].ToString());
                        r.LastRunDt = DateTime.Parse(sdr["LastRunDt"].ToString());
                        

                        try
                        {
                            r.LastSentDt = DateTime.Parse(sdr["LastSentDt"].ToString());
                        }
                        catch { }

                        r.IsEnabled = Boolean.Parse(sdr["IsEnabled"].ToString());
                        r.lup_dt = DateTime.Parse(sdr["lup_dt"].ToString());
                        r.lup_user = sdr["lup_user"].ToString();
                        r.Notes = sdr["Notes"].ToString();
                        r.Sql = sdr["Sql"].ToString();
                        r.Definition = sdr["Definition"].ToString();

                        rep.Add(r);
                    }//end while

                    sdr.Close();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end List

            return rep;
//            string sql = string.Format(@"SELECT top {0} * FROM SSO_ReportHistory rep     
//                            Where rep.[ReportId] = @ReportId                            
//                            Order by lup_dt DESC", MaxCt.ToString());
//            SqlParameter[] @params = new SqlParameter[1];
//            @params[0] = new SqlParameter("@ReportId", ReportId);
//            return BOHelper.FillCollection<Report.ReportHistory>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"
        public static string insReport(Report Report)
        {
            
            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insReport";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("@Title", Report.Title);
                    sqlcmd.Parameters.AddWithValue("@SubTitle", Report.SubTitle);
                    sqlcmd.Parameters.AddWithValue("@Type", Report.Type);
                    sqlcmd.Parameters.AddWithValue("@Definition", Report.Definition);
                    sqlcmd.Parameters.AddWithValue("@Sql", Report.Sql);
                    sqlcmd.Parameters.AddWithValue("@EmailRecipients", Report.EmailRecipients);
                    sqlcmd.Parameters.AddWithValue("@EmailCC", Report.EmailCC);
                    sqlcmd.Parameters.AddWithValue("@EmailMsg", Report.EmailMsg);
                    sqlcmd.Parameters.AddWithValue("@EmailSubject", Report.EmailSubject);
                    sqlcmd.Parameters.AddWithValue("@IsExcelAttachment", Report.IsExcelAttachment);
                    sqlcmd.Parameters.AddWithValue("@IsTransposed", Report.IsTransposed);
                    sqlcmd.Parameters.AddWithValue("@IsRunSun", Report.IsRunSun);
                    sqlcmd.Parameters.AddWithValue("@IsRunMon", Report.IsRunMon);
                    sqlcmd.Parameters.AddWithValue("@IsRunTues", Report.IsRunTues);
                    sqlcmd.Parameters.AddWithValue("@IsRunWed", Report.IsRunWed);
                    sqlcmd.Parameters.AddWithValue("@IsRunThur", Report.IsRunThur);
                    sqlcmd.Parameters.AddWithValue("@IsRunFri", Report.IsRunFri);
                    sqlcmd.Parameters.AddWithValue("@IsRunSat", Report.IsRunSat);
                    sqlcmd.Parameters.AddWithValue("@Frequency", Report.Frequency);
                    sqlcmd.Parameters.AddWithValue("@StartDt", Report.StartDt);
                    sqlcmd.Parameters.AddWithValue("@LastRunDt", Report.LastRunDt);
                    sqlcmd.Parameters.AddWithValue("@LastSentDt", Report.LastSentDt);
                    sqlcmd.Parameters.AddWithValue("@Notes", Report.Notes);
                    sqlcmd.Parameters.AddWithValue("@lup_user", Report.lup_user);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e) { }
            }

            return id;
//            string sql = @"INSERT INTO SSO_Report ([Title]
//                                                       ,[SubTitle]
//                                                       ,[Type]
//                                                       ,[Definition]
//                                                       ,[Sql]
//                                                       ,[EmailRecipients]
//                                                       ,[EmailCC]
//                                                       ,[EmailMsg]
//                                                       ,[EmailSubject]
//                                                       ,[IsExcelAttachment]
//                                                       ,[IsTransposed]
//                                                       ,[IsRunSun]
//                                                       ,[IsRunMon]
//                                                       ,[IsRunTues]
//                                                       ,[IsRunWed]
//                                                       ,[IsRunThur]
//                                                       ,[IsRunFri]
//                                                       ,[IsRunSat]
//                                                       ,[Frequency]
//                                                       ,[StartDt]
//                                                       ,[Notes]
//                                                       ,[lup_user]
//                                               )VALUES( @Title
//                                                       ,@SubTitle
//                                                       ,@Type
//                                                       ,@Definition
//                                                       ,@Sql
//                                                       ,@EmailRecipients
//                                                       ,@EmailCC
//                                                       ,@EmailMsg
//                                                       ,@EmailSubject
//                                                       ,@IsExcelAttachment
//                                                       ,@IsTransposed
//                                                       ,@IsRunSun
//                                                       ,@IsRunMon
//                                                       ,@IsRunTues
//                                                       ,@IsRunWed
//                                                       ,@IsRunThur
//                                                       ,@IsRunFri
//                                                       ,@IsRunSat
//                                                       ,@Frequency
//                                                       ,@StartDt
//                                                       ,@Notes
//                                                       ,@lup_user); SELECT @@IDENTITY as id";
//            SqlParameter[] @params = new SqlParameter[25];
//            @params[0] = new SqlParameter("@Title", Report.Title);
//            @params[1] = new SqlParameter("@SubTitle", Report.SubTitle);
//            @params[2] = new SqlParameter("@Type", Report.Type);
//            @params[3] = new SqlParameter("@Definition", Report.Definition);
//            @params[4] = new SqlParameter("@Sql", Report.Sql);
//            @params[5] = new SqlParameter("@EmailRecipients", Report.EmailRecipients);
//            @params[6] = new SqlParameter("@EmailCC", Report.EmailCC);
//            @params[7] = new SqlParameter("@EmailMsg", Report.EmailMsg);
//            @params[8] = new SqlParameter("@EmailSubject", Report.EmailSubject);
//            @params[9] = new SqlParameter("@IsExcelAttachment", Report.IsExcelAttachment);
//            @params[10] = new SqlParameter("@IsTransposed", Report.IsTransposed);
//            @params[11] = new SqlParameter("@IsRunSun", Report.IsRunSun);
//            @params[12] = new SqlParameter("@IsRunMon", Report.IsRunMon);
//            @params[13] = new SqlParameter("@IsRunTues", Report.IsRunTues);
//            @params[14] = new SqlParameter("@IsRunWed", Report.IsRunWed);
//            @params[15] = new SqlParameter("@IsRunThur", Report.IsRunThur);
//            @params[16] = new SqlParameter("@IsRunFri", Report.IsRunFri);
//            @params[17] = new SqlParameter("@IsRunSat", Report.IsRunSat);
//            @params[18] = new SqlParameter("@Frequency", Report.Frequency);
//            @params[19] = new SqlParameter("@StartDt", Report.StartDt);
//            @params[20] = new SqlParameter("@LastRunDt", Report.LastRunDt);
//            @params[21] = new SqlParameter("@LastSentDt", Report.LastSentDt);
//            @params[22] = new SqlParameter("@Notes", Report.Notes);
//            @params[23] = new SqlParameter("@lup_user", Report.lup_user);

//            return BOHelper.FillObject<Report>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }

        public static string insReportHistory(Report.ReportHistory Report)
        {
            updRunDate(new Models.Report { id = Report.ReportId, LastRunDt = DateTime.Now });
            if (Report.IsSent) updSentDate(new Models.Report { id = Report.ReportId, LastSentDt = DateTime.Now });

            string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insReportHistory";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("@ReportId", Report.ReportId);
                    sqlcmd.Parameters.AddWithValue("@Type", Report.Type);
                    sqlcmd.Parameters.AddWithValue("@Definition", Report.Definition);
                    sqlcmd.Parameters.AddWithValue("@Sql", Report.Sql);
                    sqlcmd.Parameters.AddWithValue("@EmailRecipients", Report.EmailRecipients);
                    sqlcmd.Parameters.AddWithValue("@EmailCC", Report.EmailCC);
                    sqlcmd.Parameters.AddWithValue("@IsSent", Report.IsSent);       
                    //sqlcmd.Parameters.AddWithValue("@EmailSubject", Report.EmailSubject);
                    //sqlcmd.Parameters.AddWithValue("@IsExcelAttachment", Report.IsExcelAttachment);
                    //sqlcmd.Parameters.AddWithValue("@IsTransposed", Report.IsTransposed);
                    //sqlcmd.Parameters.AddWithValue("@IsRunSun", Report.IsRunSun);
                    //sqlcmd.Parameters.AddWithValue("@IsRunMon", Report.IsRunMon);
                    //sqlcmd.Parameters.AddWithValue("@IsRunTues", Report.IsRunTues);
                    //sqlcmd.Parameters.AddWithValue("@IsRunWed", Report.IsRunWed);
                    //sqlcmd.Parameters.AddWithValue("@IsRunThur", Report.IsRunThur);
                    //sqlcmd.Parameters.AddWithValue("@IsRunFri", Report.IsRunFri);
                    //sqlcmd.Parameters.AddWithValue("@IsRunSat", Report.IsRunSat);
                    //sqlcmd.Parameters.AddWithValue("@Frequency", Report.Frequency);
                    sqlcmd.Parameters.AddWithValue("@StartDt", Report.StartDt);
                    sqlcmd.Parameters.AddWithValue("@IsSent", Report.IsSent);
                    sqlcmd.Parameters.AddWithValue("@ResultCt", Report.ResultCt);
                    sqlcmd.Parameters.AddWithValue("@lup_user", Report.lup_user);

                    id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e) { }
            }

            return id;
//            string sql = @"INSERT INTO SSO_ReportHistory ([ReportId]                                                     
//                                                       ,[Type]
//                                                       ,[Definition]
//                                                       ,[Sql]
//                                                       ,[EmailRecipients]
//                                                       ,[EmailCC]                                                       
//                                                       ,[StartDt]    
//                                                       ,[IsSent]                                                   
//                                                       ,[ResultCt]                                                   
//                                                       ,[lup_user]
//                                               )VALUES(@ReportId                                                   
//                                                       ,@Type
//                                                       ,@Definition
//                                                       ,@Sql
//                                                       ,@EmailRecipients
//                                                       ,@EmailCC
//                                                       ,@StartDt
//                                                       ,@IsSent
//                                                       ,@ResultCt
//                                                       ,@lup_user); SELECT @@IDENTITY as id";
//            SqlParameter[] @params = new SqlParameter[15];            
//            @params[0] = new SqlParameter("@ReportId", Report.ReportId);
//            @params[1] = new SqlParameter("@Type", Report.Type);
//            @params[2] = new SqlParameter("@Definition", Report.Definition);
//            @params[3] = new SqlParameter("@Sql", Report.Sql);
//            @params[4] = new SqlParameter("@EmailRecipients", Report.EmailRecipients);
//            @params[5] = new SqlParameter("@EmailCC", Report.EmailCC);
//            @params[6] = new SqlParameter("@StartDt", Report.StartDt);
//            @params[7] = new SqlParameter("@IsSent", Report.IsSent);            
//            @params[8] = new SqlParameter("@ResultCt", Report.ResultCt);
//            @params[9] = new SqlParameter("@lup_user", Report.lup_user);

//            return BOHelper.FillObject<Report>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }

        #endregion

        #region "UPDATES"
        public static void updReport(Report Report)
        {
            //string id = "";

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updReport";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("@Title", Report.Title);
                    sqlcmd.Parameters.AddWithValue("@SubTitle", Report.SubTitle);
                    sqlcmd.Parameters.AddWithValue("@Type", Report.Type);
                    sqlcmd.Parameters.AddWithValue("@Definition", Report.Definition);
                    sqlcmd.Parameters.AddWithValue("@Sql", Report.Sql);
                    sqlcmd.Parameters.AddWithValue("@EmailRecipients", Report.EmailRecipients);
                    sqlcmd.Parameters.AddWithValue("@EmailCC", Report.EmailCC);
                    sqlcmd.Parameters.AddWithValue("@EmailMsg", Report.EmailMsg);
                    sqlcmd.Parameters.AddWithValue("@EmailSubject", Report.EmailSubject);
                    sqlcmd.Parameters.AddWithValue("@IsExcelAttachment", Report.IsExcelAttachment);
                    sqlcmd.Parameters.AddWithValue("@IsTransposed", Report.IsTransposed);
                    sqlcmd.Parameters.AddWithValue("@IsRunSun", Report.IsRunSun);
                    sqlcmd.Parameters.AddWithValue("@IsRunMon", Report.IsRunMon);
                    sqlcmd.Parameters.AddWithValue("@IsRunTues", Report.IsRunTues);
                    sqlcmd.Parameters.AddWithValue("@IsRunWed", Report.IsRunWed);
                    sqlcmd.Parameters.AddWithValue("@IsRunThur", Report.IsRunThur);
                    sqlcmd.Parameters.AddWithValue("@IsRunFri", Report.IsRunFri);
                    sqlcmd.Parameters.AddWithValue("@IsRunSat", Report.IsRunSat);
                    sqlcmd.Parameters.AddWithValue("@Frequency", Report.Frequency);
                    sqlcmd.Parameters.AddWithValue("@StartDt", Report.StartDt);
                    sqlcmd.Parameters.AddWithValue("@id", Report.id);
                    sqlcmd.Parameters.AddWithValue("@lup_dt", Report.lup_dt);
                    sqlcmd.Parameters.AddWithValue("@Notes", Report.Notes);
                    sqlcmd.Parameters.AddWithValue("@lup_user", Report.lup_user);

                    //id = sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e) { }
            }

            //return id;



//            string sql = @"UPDATE SSO_Report SET [Title] = @Title
//                                                      ,[SubTitle] = @SubTitle
//                                                      ,[Type] = @Type
//                                                      ,[Definition] = @Definition
//                                                      ,[Sql] = @Sql
//                                                      ,[EmailRecipients] = @EmailRecipients
//                                                      ,[EmailCC] = @EmailCC
//                                                      ,[EmailMsg] = @EmailMsg
//                                                      ,[EmailSubject] = @EmailSubject
//                                                      ,[IsExcelAttachment] = @IsExcelAttachment
//                                                      ,[IsTransposed] = @IsTransposed
//                                                      ,[IsRunSun] = @IsRunSun
//                                                      ,[IsRunMon] = @IsRunMon
//                                                      ,[IsRunTues] = @IsRunTues
//                                                      ,[IsRunWed] = @IsRunWed
//                                                      ,[IsRunThur] = @IsRunThur
//                                                      ,[IsRunFri] = @IsRunFri
//                                                      ,[IsRunSat] = @IsRunSat
//                                                      ,[Frequency] = @Frequency
//                                                      ,[StartDt] = @StartDt                                                      
//                                                      ,[Notes] = @Notes
//                                                      ,[lup_user] = @lup_user
//                                                      ,[lup_dt] = @lup_dt 
//                                                WHERE [id] = @id";
//            SqlParameter[] @params = new SqlParameter[27];
//            @params[0] = new SqlParameter("@Title", Report.Title);
//            @params[1] = new SqlParameter("@SubTitle", Report.SubTitle);
//            @params[2] = new SqlParameter("@Type", Report.Type);
//            @params[3] = new SqlParameter("@Definition", Report.Definition);
//            @params[4] = new SqlParameter("@Sql", Report.Sql);
//            @params[5] = new SqlParameter("@EmailRecipients", Report.EmailRecipients);
//            @params[6] = new SqlParameter("@EmailCC", Report.EmailCC);
//            @params[7] = new SqlParameter("@EmailMsg", Report.EmailMsg);
//            @params[8] = new SqlParameter("@EmailSubject", Report.EmailSubject);
//            @params[9] = new SqlParameter("@IsExcelAttachment", Report.IsExcelAttachment);
//            @params[10] = new SqlParameter("@IsTransposed", Report.IsTransposed);
//            @params[11] = new SqlParameter("@IsRunSun", Report.IsRunSun);
//            @params[12] = new SqlParameter("@IsRunMon", Report.IsRunMon);
//            @params[13] = new SqlParameter("@IsRunTues", Report.IsRunTues);
//            @params[14] = new SqlParameter("@IsRunWed", Report.IsRunWed);
//            @params[15] = new SqlParameter("@IsRunThur", Report.IsRunThur);
//            @params[16] = new SqlParameter("@IsRunFri", Report.IsRunFri);
//            @params[17] = new SqlParameter("@IsRunSat", Report.IsRunSat);
//            @params[18] = new SqlParameter("@Frequency", Report.Frequency);
//            @params[19] = new SqlParameter("@StartDt", Report.StartDt);
//            @params[20] = new SqlParameter("@LastRunDt", Report.LastRunDt);
//            @params[21] = new SqlParameter("@LastSentDt", Report.LastSentDt);
//            @params[22] = new SqlParameter("@Notes", Report.Notes);
//            @params[23] = new SqlParameter("@lup_user", Report.lup_user);
//            @params[24] = new SqlParameter("@lup_dt", Report.lup_dt);
//            @params[25] = new SqlParameter("@id", Report.id);
            
//            DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void updRunDate(Report Report)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updRunDate";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("@LastRunDt", Report.LastRunDt);
                    sqlcmd.Parameters.AddWithValue("@id", Report.id);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e) { }
            }
        }
        public static void updSentDate(Report Report)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.updSentDate";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("@LastSentDt", Report.LastSentDt);
                    sqlcmd.Parameters.AddWithValue("@id", Report.id);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e) { }
            }

            //string sql = @"UPDATE SSO_Report SET [LastSentDt] = @LastSentDt  WHERE [id] = @id";
            //SqlParameter[] @params = new SqlParameter[3];
            //@params[0] = new SqlParameter("@LastSentDt", Report.LastSentDt);
            //@params[1] = new SqlParameter("@id", Report.id);

            //DBManager.ExecCommandDR(sql, @params, true);
        }

        public static void delReport(Report Report)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delReport";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("@id", Report.id);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e) { }
            }
            //string sql = @"DELETE SSO_Report WHERE [id] = @id";
            //SqlParameter[] @params = new SqlParameter[4];
            //@params[0] = new SqlParameter("@id", Report.id);
            //@params[1] = new SqlParameter("@lup_user", Report.lup_user);
            //@params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            //DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delExtinctReportHistory(int days = 730)
        {
            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.delExtinctReportHistory";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("@Days", days);
                    //sqlcmd.Parameters.AddWithValue("@id", Report.id);

                    sqlcmd.Connection.Close();
                }
                catch (Exception e) { }
            }

            //string sql = @"DELETE FROM SSO_ReportHistory where [lup_dt] < dateadd(day, @Days, getdate())";
            //SqlParameter[] @params = new SqlParameter[2];
            //@params[0] = new SqlParameter("@Days", -days);
            //DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion


        #region Helpers

        public static bool GenerateEmail(Report rpt, string baseUrl = "http://bhwebapp.bmcjax.com/TITO_Admin")
        {
            if(rpt != null) {
                StringBuilder sbReport = new StringBuilder("<br />");
                StringBuilder sbCSV = new StringBuilder();
                
                if (rpt.IsTransposed) //flips cols for rows, and vice versa
                {
                    sbReport.Append("<table style='font-size:.90em;text-align:center;' cellpadding='2'>");
                    for (int c = 0; c < rpt.Columns.Count; c++)
                    {
                        sbReport.Append(string.Format("<tr style='" + (c % 2 == 0 ? "background-color:rgb(245, 245, 245);" : "background-color:rgb(230, 230, 230);") + "'><td><b>{0}</b></td>", rpt.Columns[c].Text));
                        sbCSV.Append(string.Format("\"{0}\",", rpt.Columns[c].Text));
                        for (int r = 0; r < rpt.DataSource.Count; r++)
                        {
                            object propVal = Shared.SysHelper.GetPropValue(rpt.DataSource[r], rpt.Columns[c]);
                            sbReport.Append(string.Format("<td{0}>{1}</td>", rpt.Columns[c].GetHtmlAttributes(propVal), propVal));
                            sbCSV.Append(string.Format("\"{0}\",", propVal));
                        }
                        sbReport.Append("</tr>");
                        sbCSV.Remove(sbCSV.Length - 1, 1);
                        sbCSV.Append(Environment.NewLine);
                    }
                }
                else //std grid layout
                {
                    sbReport.Append("<table style='width:100%;font-size:.90em;text-align:center;'><tr style='background-color:#E1E1E1;padding:3px 3px 3px 3px;'>");
                    for (int c = 0; c < rpt.Columns.Count; c++)
                    {
                        sbReport.Append(string.Format("<th>{0}</th>", rpt.Columns[c].Text));
                        sbCSV.Append(string.Format("\"{0}\",", rpt.Columns[c].Text));
                    }
                    if (sbCSV.Length > 0) { sbCSV.Remove(sbCSV.Length - 1, 1); sbCSV.Append(Environment.NewLine); }
                    sbReport.Append("</tr>");

                    for (int r = 0; r < rpt.DataSource.Count; r++)
                    {
                        sbReport.Append("<tr style='" + (r % 2 == 0 ? "background-color:rgb(245, 245, 245);" : "background-color:rgb(230, 230, 230);") + "'>");
                        for (int c = 0; c < rpt.Columns.Count; c++)
                        {
                            try
                            {
                                object propVal = Shared.SysHelper.GetPropValue(rpt.DataSource[r], rpt.Columns[c]);
                                sbReport.Append(string.Format("<td{0}>{1}</td>", rpt.Columns[c].GetHtmlAttributes(propVal), propVal));
                                sbCSV.Append(string.Format("\"{0}\",", propVal));
                            } catch { }
                        }
                        sbReport.Append("</tr>");
                        sbCSV.Remove(sbCSV.Length - 1, 1);
                        sbCSV.Append(Environment.NewLine);
                    }
                }
                sbReport.Append("</table><br />");

                sbReport.Append(string.Format("<div style='color:dimgray;'><center><span>{1} row(s) returned</span></center><span style='float:right;'>Auto Generated {0}</span><br /><br /><span style='float:left'>Baptist Health, All Rights Reserved</span></div>", DateTime.Now.ToString(), rpt.DataSource.Count.ToString()));
                                
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                System.IO.StreamWriter sw = new System.IO.StreamWriter(ms);
                if (rpt.IsExcelAttachment)
                {
                    sw.Write(sbCSV.ToString());
                    sw.Flush();
                    ms.Position = 0;
                }
                
                if(!string.IsNullOrEmpty(rpt.EmailRecipients))
                    Shared.mail.sendMail(new Shared.mail { sendto = rpt.EmailRecipients, cc = rpt.EmailCC, from = "TITO_Reporting-noreply@bmcjax.com", subject = rpt.EmailSubject, body = rpt.EmailMsg + (!rpt.IsExcelAttachment ? "<br />" + sbReport.ToString() : string.Empty) + string.Format("<p><a href='{1}/Report.aspx?id={0}'>View in Browser</a></p>", rpt.id.ToString(), baseUrl), attach = rpt.IsExcelAttachment ? new System.Net.Mail.Attachment(ms, rpt.FileName + ".csv", "text/csv") : null });
                                
                sw.Dispose();
                ms.Close();
                return !string.IsNullOrEmpty(rpt.EmailRecipients);
            }

            return false;
        }


        public static string GetParameter(Report rpt, string param, string defaultValue = "")
        {
            var v = rpt.Parameters.Find(p => p.Name == param);
            if (v != null)
                return v.Value;
            else
                return defaultValue;
        }

        public static ArrayList ConvertDT(DataTable dt)
        {
            ArrayList converted = new ArrayList(dt.Rows.Count);
            foreach (DataRow row in dt.Rows)
                converted.Add(row);

            return converted;
        }

        #endregion

        #region Inner Classes
        public class Parameter
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }
        public class Column
        {
            public string Source { get; set; }
            public string Text { get; set; }
            public string Format { get; set; }
            public string Alignment { get; set; }
            public string ToolTip { get; set; }
            public string Style { get; set; }
            public string CompareToOperator { get; set; }
            public string CompareToValue { get; set; }
            public string CompareToStyle { get; set; }            

            //pass a value then check for comparisons, and return back html attributes
            public string GetHtmlAttributes(object value)
            {
                string CellStyle = Style ?? string.Empty; //inherit base style
                if (!string.IsNullOrEmpty(CompareToOperator))
                {
                    try {
                        Type t = value.GetType(); bool comparison = false;
                        if(t == typeof(int) || t == typeof(double))
                            comparison = Shared.SysHelper.Compare<double>(CompareToOperator, Convert.ToDouble(value), Convert.ToDouble(CompareToValue));
                        else
                            comparison = Shared.SysHelper.Compare<string>(CompareToOperator, value.ToString(), CompareToValue);
                        if(comparison)
                            CellStyle = CellStyle + (!CellStyle.EndsWith(";") && CellStyle.Length > 0 ? ";" : string.Empty) + CompareToStyle;
                    } catch { } //dont care if it fails
                }

                string attr = string.Empty;
                attr += !string.IsNullOrEmpty(Alignment) ? " align='" + Alignment + "'" : string.Empty;
                attr += !string.IsNullOrEmpty(CellStyle) ? " style='" + CellStyle + "'" : string.Empty;
                attr += !string.IsNullOrEmpty(ToolTip) ? " title=\"" + ToolTip + "\"" : string.Empty;
                return attr;
            }

        }
        public class ReportHistory : Report
        {
            public int ReportId { get; set; } //History column            
            public bool IsSent { get; set; } //History column
            public ReportHistory(Report r)
            {
                ReportId = r.id;
                Type = r.Type;
                Definition = r.Definition;
                Sql = r.Sql;
                EmailRecipients = r.EmailRecipients;
                EmailCC = r.EmailCC;
                StartDt = r.StartDt;
                IsSent = false;
                ResultCt = r.ResultCt;
                LastRunDt = r.LastRunDt;
                LastSentDt = r.LastSentDt;
                lup_user = string.Empty;
            }
            public ReportHistory(){ }
        }
        #endregion
    }
}
