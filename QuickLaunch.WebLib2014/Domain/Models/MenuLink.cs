﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    public class MenuLink : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the name.
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// Gets/Sets the tool tip of the menu item
        /// </summary>
        public string toolTip { get; set; }

        /// <summary>
        /// Gets/Sets the click event. http denotes web url
        /// </summary>
        public string clickEvent { get; set; }

        /// <summary>
        /// Gets/Sets the image. Image should be stored as resource in app.
        /// </summary>
        public string img { get; set; }

        /// <summary>
        /// Gets/Sets the audience that sees this link.
        /// </summary>
        public string audience { get; set; }

        /// <summary>
        /// Gets/Sets the active directory filter for the menu item.
        /// when not empty, the menu item will only show if the user is in the specified AD group
        /// </summary>
        public string adGroupFilter { get; set; }

        /// <summary>
        /// Gets/Sets the order of the item
        /// </summary>
        public int orderId { get; set; }

        /// <summary>
        /// Gets/Sets the visibility
        /// </summary>
        public bool isVisible { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion

        public MenuLink() { }
        
        #region "SELECTS"
        public static MenuLink selMenuLinkById(int id)
        {
            string sql = "";
            sql = "SELECT * FROM SSO_MenuLinks WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<MenuLink>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<MenuLink> selMenuLinks(bool ShowAll = false)
        {
            string sql = "SELECT * FROM SSO_MenuLinks WHERE " + (ShowAll ? "1=1" : "IsVisible = 1") + " order by orderId";
            SqlParameter[] @params = new SqlParameter[1];
            
            return BOHelper.FillCollection<MenuLink>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insMenuLink(MenuLink MenuLink)
        {
            string sql = @"INSERT INTO SSO_MenuLinks ([name], [toolTip], [clickEvent], [img], [audience], [adGroupFilter], [isVisible], [orderId]) VALUES (@name, @toolTip, @clickEvent, @img, @audience, @adGroupFilter, @isVisible, @orderId); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[10];

            @params[0] = new SqlParameter("@name", MenuLink.name);
            @params[1] = new SqlParameter("@toolTip", MenuLink.toolTip);
            @params[2] = new SqlParameter("@clickEvent", MenuLink.clickEvent);
            @params[3] = new SqlParameter("@img", MenuLink.img);
            @params[4] = new SqlParameter("@audience", MenuLink.audience);
            @params[5] = new SqlParameter("@adGroupFilter", MenuLink.adGroupFilter);
            @params[6] = new SqlParameter("@isVisible", MenuLink.isVisible);
            @params[7] = new SqlParameter("@orderId", MenuLink.orderId);

            return BOHelper.FillObject<MenuLink>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updMenuLink(MenuLink MenuLink)
        {
            string sql = @"UPDATE SSO_MenuLinks SET [name] = @name, [toolTip] = @toolTip, [clickEvent] = @clickEvent, [img] = @img, [audience] = @audience, [adGroupFilter] = @adGroupFilter, [isVisible] = @isVisible, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@name", MenuLink.name);
            @params[1] = new SqlParameter("@toolTip", MenuLink.toolTip);
            @params[2] = new SqlParameter("@clickEvent", MenuLink.clickEvent);
            @params[3] = new SqlParameter("@img", MenuLink.img);
            @params[4] = new SqlParameter("@audience", MenuLink.audience);
            @params[5] = new SqlParameter("@adGroupFilter", MenuLink.adGroupFilter);
            @params[6] = new SqlParameter("@isVisible", MenuLink.isVisible);    
            @params[7] = new SqlParameter("@lup_dt", MenuLink.lup_dt);
            @params[8] = new SqlParameter("@id", MenuLink.id);
            
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void updMenuLinkOrder(List<MenuLink> links)
        {
            string sql = string.Empty;
            for(int i = 0; i < links.Count;i++)
                sql += "UPDATE SSO_MenuLinks SET [OrderId] = " + (i + 1).ToString() + " WHERE [id] = " + links[i].id.ToString() + "; ";
            SqlParameter[] @params = new SqlParameter[1];
            if(!string.IsNullOrEmpty(sql))
                DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delMenuLink(MenuLink MenuLink)
        {
            string sql = @"DELETE FROM SSO_MenuLinks WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", MenuLink.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion

        public static List<MenuLink> DefaultMenu = new List<MenuLink>{
            new MenuLink { id = 100, name = "Power Chart", toolTip = "Open Power Chart", clickEvent = "EMR", img = "shield", audience = "ALL", adGroupFilter = "", orderId = 1, isVisible = true  },
            new MenuLink { id = 101, name = "FirstNet", toolTip = "Open FirstNet", clickEvent = "FN", img = "ambulance", audience = "ALL", adGroupFilter = "", orderId = 2, isVisible = true  },
            new MenuLink { id = 102, name = "SurgiNet", toolTip = "Open SurgiNet", clickEvent = "SN", img = "drhead", audience = "ALL", adGroupFilter = "", orderId = 3, isVisible = true  },
            new MenuLink { id = 103, name = "Email", toolTip = "Open Email", clickEvent = "EMAIL", img = "email", audience = "ALL", adGroupFilter = "", orderId = 4, isVisible = true  },
            new MenuLink { id = 104, name = "User Portal", toolTip = "Open My Portal", clickEvent = "PORTAL", img = "myportal", audience = "ALL", adGroupFilter = "", orderId = 5, isVisible = true  },
            new MenuLink { id = 105, name = "Intranet", toolTip = "Open Intranet", clickEvent = "INTRANET", img = "intranet", audience = "ALL", adGroupFilter = "", orderId = 6, isVisible = true  }
        };
        
    }
}
