﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{
    [Serializable]
    public class Application : Abstract.IDataInfo
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        public int LaunchCt { get; set; }

        public DateTime LastLaunch { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the Value.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Gets/Sets the Window Title.
        /// </summary>
        public string WindowTitle { get; set; }

        /// <summary>
        /// Gets/Sets the Allowing Roaming
        /// </summary>
        public bool AllowRoaming { get; set; }

        /// <summary>
        /// Gets/Sets the default img
        /// </summary>
        public string DefaultImg { get; set; }
        
        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

 
        /* Display Fields */

        /// <summary>
        /// Gets/Sets the display value for the title/value
        /// </summary>
        public string TitleValueDisplay { get; set; }

        /* End Display Fields */

        /// <summary>
        /// Gets/Sets Citrix Applications.
        /// Default list for offline mode : gets overriden on QL initialize
        /// </summary>        
        public static List<Application> CitrixApplications = new List<Application> {  
                    new Application { id = 100, Title = "Power Chart", Value = "EMR 1024x768 - SSOI", WindowTitle = "PowerChart", AllowRoaming = true, DefaultImg = "powerchart"},
                    new Application { id = 101, Title = "Power Chart", Value = "EMR 1024x768 - SP", WindowTitle = "PowerChart", AllowRoaming = true, DefaultImg = "powerchart"},
                    new Application { id = 102, Title = "Power Chart", Value = "EMR Imprivata 4-6 SSO Test", WindowTitle = "PowerChart", AllowRoaming = true, DefaultImg = "powerchart"},
                    new Application { id = 103, Title = "FirstNet", Value = "Firstnet - SSOI", WindowTitle = "FirstNet", AllowRoaming = true, DefaultImg = "firstnet"},
                    new Application { id = 104, Title = "FirstNet", Value = "Firstnet - SP", WindowTitle = "FirstNet", AllowRoaming = true, DefaultImg = "firstnet"},
                    new Application { id = 105, Title = "SurgiNet", Value = "Surginet - SSOI", WindowTitle = "SNSurgiNet", AllowRoaming = true, DefaultImg = "surginet"},
                    new Application { id = 106, Title = "SurgiNet", Value = "Surginet - SP", WindowTitle = "SNSurgiNet", AllowRoaming = true, DefaultImg = "surginet"}
        };
        public static DateTime LastCitrixApplicationsUpdate { get; set; }
        #endregion

        public Application() { }

        public static void LoadCitrixApplications(){
            if (LastCitrixApplicationsUpdate < DateTime.Now.AddHours(-1)) //only check once an hour
            {
                List<Application> DbApps = selApplications();
                if (DbApps.Count > 0)
                {
                    CitrixApplications.Clear();
                    CitrixApplications.AddRange(DbApps);
                    LastCitrixApplicationsUpdate = DateTime.Now;
                }
            }
        }
        

        #region "SELECTS"

        public static Application selApplicationById(int id)
        {
            string sql = "";
            sql = @"SELECT * FROM SSO_Application app                        
                    WHERE app.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<Application>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Application> selApplications()
        {
            string sql = @"SELECT app.*, (app.Title + ' (' + app.Value + ')') as TitleValueDisplay FROM SSO_Application app                                 
                           order by title";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Application>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"
        public static string insApplication(Application Application)
        {
            string sql = @"INSERT INTO SSO_Application ([Title],[Value],[WindowTitle],[AllowRoaming],[DefaultImg],[lup_user]) VALUES (@Title,@Value,@WindowTitle,@AllowRoaming,@DefaultImg,@lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[8];
            @params[0] = new SqlParameter("@Title", Application.Title);
            @params[1] = new SqlParameter("@Value", Application.Value);
            @params[2] = new SqlParameter("@WindowTitle", Application.WindowTitle);
            @params[3] = new SqlParameter("@AllowRoaming", Application.AllowRoaming);
            @params[4] = new SqlParameter("@DefaultImg", Application.DefaultImg);
            @params[5] = new SqlParameter("@lup_user", Application.lup_user);

            return BOHelper.FillObject<Application>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updApplication(Application Application)
        {
            string sql = @"UPDATE SSO_Application SET [Title] = @Title,[Value] = @Value,[WindowTitle] = @WindowTitle,[AllowRoaming] = @AllowRoaming,[DefaultImg] = @DefaultImg,[lup_user] = @lup_user, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@Title", Application.Title);
            @params[1] = new SqlParameter("@Value", Application.Value);
            @params[2] = new SqlParameter("@WindowTitle", Application.WindowTitle);
            @params[3] = new SqlParameter("@AllowRoaming", Application.AllowRoaming);
            @params[4] = new SqlParameter("@DefaultImg", Application.DefaultImg);
            @params[5] = new SqlParameter("@lup_user", Application.lup_user);
            @params[6] = new SqlParameter("@lup_dt", Application.lup_dt);            
            @params[7] = new SqlParameter("@id", Application.id);

            DBManager.ExecCommandDR(sql, @params, true);
        }

        //public static void updReorderHelpByParent(List<Help> help)
        //{
        //    string sql = "";
        //    for (int i = 0; i < help.Count; i++)
        //        sql += @"UPDATE SSO_ SET [OrderId] = " + (i + 1).ToString() + " WHERE [id] = " + help[i].id + ";";

        //    SqlParameter[] @params = new SqlParameter[2];
        //    if (sql.Length > 0) DBManager.ExecCommandDR(sql, @params, true);
        //}

        public static void delApplication(Application Application)
        {
            string sql = @"DELETE FROM SSO_Application WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", Application.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        #endregion
    }
}
