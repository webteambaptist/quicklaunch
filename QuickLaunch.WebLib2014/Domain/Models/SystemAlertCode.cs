﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
namespace QuickLaunch.Models
{
    public class SystemAlertCode : Abstract.IDataInfo
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets/Sets the description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets/Sets the IconColor
        /// </summary>
        public string IconColor { get; set; }

        /// <summary>
        /// Gets/Sets the ForeColor
        /// </summary>
        public string ForeColor { get; set; }

        /// <summary>
        /// Gets/Sets the BackColor
        /// </summary>
        public string BackColor { get; set; }

        /// <summary>
        /// Gets/Sets the UseTicker
        /// </summary>
        public bool UseTicker { get; set; }

        /// <summary>
        /// Gets/Sets the UseDialog
        /// </summary>
        public bool UseDialog { get; set; }

        /// <summary>
        /// Gets/Sets the UseOSBubble
        /// </summary>
        public bool UseOSBubble { get; set; }

        /// <summary>
        /// Gets/Sets the UseTrayToast
        /// </summary>
        public bool UseTrayToast { get; set; }

        /// <summary>
        /// Gets/Sets the archive indicator
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Gets/Sets the last update user
        /// </summary>
        public string lup_user { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        #endregion

        public SystemAlertCode() { }
        
        #region "SELECTS"
        public static SystemAlertCode selSystemAlertCodeById(int id)
        {
            string sql = "";
            sql = @"SELECT sa.* FROM SSO_SystemAlertCode sa                    
                    WHERE [id] = @id AND IsArchived = 0";
            SqlParameter[] @params = new SqlParameter[1];            
            @params[0] = new SqlParameter("@id", id);                        
                       
            return BOHelper.FillObject<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params));
        }

        public static List<SystemAlertCode> selSystemAlertCodes()
        {
            string sql = @"SELECT sa.* FROM SSO_SystemAlertCode sa WHERE IsArchived = 0                          
                           order by Code";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<SystemAlertCode> selSystemAlertCodesByUser(int UserId)
        {
            string sql = @"SELECT sa.id, sa.Code, sa.Description, sa.lup_user, sa.lup_dt FROM SSO_SystemAlertCode sa 
                            INNER JOIN SSO_SystemAlertUserCodes sauc ON sauc.CodeId = sa.id 
                            WHERE IsArchived = 0 AND sauc.UserId = @UserId
                            order by Code";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@UserId", UserId);
            return BOHelper.FillCollection<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<SystemAlertCode> selSystemAlertUserCodes(int UserId) //diff than ByUser because it takes sauc.id
        {
            string sql = @"SELECT sauc.id, sa.Code, sa.Description, sa.lup_user, sa.lup_dt FROM SSO_SystemAlertCode sa 
                            INNER JOIN SSO_SystemAlertUserCodes sauc ON sauc.CodeId = sa.id 
                            WHERE IsArchived = 0 AND sauc.UserId = @UserId
                            order by Code";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@UserId", UserId);
            return BOHelper.FillCollection<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params));
        }
        public static SystemAlertTemplate selSystemAlertTemplateById(int id)
        {
            string sql = @"SELECT sa.* FROM SSO_SystemAlertTemplate sa WHERE Id = @Id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@Id", id);

            return BOHelper.FillObject<SystemAlertTemplate>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<SystemAlertTemplate> selSystemAlertTemplates(int codeId)
        {
            string sql = @"SELECT sa.* FROM SSO_SystemAlertTemplate sa WHERE CodeId = @CodeId                          
                           order by Text";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@CodeId", codeId);

            return BOHelper.FillCollection<SystemAlertTemplate>(DBManager.ExecCommandDR(sql, @params));
        }
        #endregion

        #region "INSERTS"
        public static string insSystemAlertCode(SystemAlertCode SystemAlertCode)
        {
            string sql = @"INSERT INTO SSO_SystemAlertCode ([Code],[Description],[IconColor],[ForeColor],[BackColor],[UseTicker],[UseDialog],[UseOSBubble],[UseTrayToast],[lup_user]) 
                                                VALUES (@Code,@Description,@IconColor,@ForeColor,@BackColor,@UseTicker,@UseDialog,@UseOSBubble,@UseTrayToast,@lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[11];
            @params[0] = new SqlParameter("@Code", SystemAlertCode.Code);
            @params[1] = new SqlParameter("@Description", SystemAlertCode.Description);
            @params[2] = new SqlParameter("@IconColor", SystemAlertCode.IconColor);
            @params[3] = new SqlParameter("@ForeColor", SystemAlertCode.ForeColor);
            @params[4] = new SqlParameter("@BackColor", SystemAlertCode.BackColor);
            @params[5] = new SqlParameter("@UseTicker", SystemAlertCode.UseTicker);
            @params[6] = new SqlParameter("@UseDialog", SystemAlertCode.UseDialog);
            @params[7] = new SqlParameter("@UseOSBubble", SystemAlertCode.UseOSBubble);
            @params[8] = new SqlParameter("@UseTrayToast", SystemAlertCode.UseTrayToast);            
            @params[9] = new SqlParameter("@IsArchived", SystemAlertCode.IsArchived);
            @params[10] = new SqlParameter("@lup_user", SystemAlertCode.lup_user);

            return BOHelper.FillObject<SystemAlertCode>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static string insSystemAlertTemplate(SystemAlertTemplate SystemAlertTemplate)
        {
            string sql = @"INSERT INTO SSO_SystemAlertTemplate ([CodeId],[Text],[lup_user]) 
                                                VALUES (@CodeId,@Text,@lup_user); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@CodeId", SystemAlertTemplate.CodeId);
            @params[1] = new SqlParameter("@Text", SystemAlertTemplate.Text);            
            @params[2] = new SqlParameter("@lup_user", SystemAlertTemplate.lup_user);

            return BOHelper.FillObject<SystemAlertTemplate>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        #endregion

        #region "UPDATES"
        public static void updSystemAlertCode(SystemAlertCode SystemAlertCode)
        {
            string sql = @"UPDATE SSO_SystemAlertCode SET[Code] = @Code,[Description] = @Description,[IconColor] = @IconColor,[ForeColor] = @ForeColor,[BackColor] = @BackColor,[UseTicker] = @UseTicker,[UseDialog] = @UseDialog,[UseOSBubble] = @UseOSBubble,[UseTrayToast] = @UseTrayToast,[IsArchived] = @IsArchived,[lup_user] = @lup_user, lup_dt = @lup_dt 
                            WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[14];
            @params[0] = new SqlParameter("@Code", SystemAlertCode.Code);
            @params[1] = new SqlParameter("@Description", SystemAlertCode.Description);
            @params[2] = new SqlParameter("@IconColor", SystemAlertCode.IconColor);
            @params[3] = new SqlParameter("@ForeColor", SystemAlertCode.ForeColor);
            @params[4] = new SqlParameter("@BackColor", SystemAlertCode.BackColor);
            @params[5] = new SqlParameter("@UseTicker", SystemAlertCode.UseTicker);
            @params[6] = new SqlParameter("@UseDialog", SystemAlertCode.UseDialog);
            @params[7] = new SqlParameter("@UseOSBubble", SystemAlertCode.UseOSBubble);
            @params[8] = new SqlParameter("@UseTrayToast", SystemAlertCode.UseTrayToast);          
            @params[9] = new SqlParameter("@IsArchived", SystemAlertCode.IsArchived);
            @params[10] = new SqlParameter("@lup_user", SystemAlertCode.lup_user);
            @params[11] = new SqlParameter("@lup_dt", SystemAlertCode.lup_dt);
            @params[12] = new SqlParameter("@id", SystemAlertCode.id);
            
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlertCode(SystemAlertCode SystemAlertCode)
        {            
            string sql = @"UPDATE SSO_SystemAlertCode SET [IsArchived] = 1, [lup_user] = @lup_user, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", SystemAlertCode.id);
            @params[1] = new SqlParameter("@lup_user", SystemAlertCode.lup_user);
            @params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delSystemAlertTemplate(SystemAlertCode SystemAlertCode)
        {
            string sql = @"DELETE FROM SSO_SystemAlertTemplate WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@id", SystemAlertCode.id);
            @params[1] = new SqlParameter("@lup_user", SystemAlertCode.lup_user);
            @params[2] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        
        #endregion

        public class SystemAlertTemplate : SystemAlertCode, Abstract.IDataInfo
        {

            #region Properties

            /// <summary>
            /// Gets/Sets the code id
            /// </summary>
            public int CodeId { get; set; }

            /// <summary>
            /// Gets/Sets the template msg
            /// </summary>
            public string Text { get; set; }
            
            #endregion

            public SystemAlertTemplate() { }

            public static List<SystemAlertTemplate> selSysAlertTemplate(int cID)
            {
                List<SystemAlertTemplate> sc = new List<SystemAlertTemplate>();

                using (SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["TITOConnString"].ConnectionString))
                {
                    try
                    {
                        sqlConn.Open();
                        SqlCommand sqlcmd = new SqlCommand("dbo.selSysAlertTemplate", sqlConn);
                        sqlcmd.CommandType = CommandType.StoredProcedure;
                        sqlcmd.Parameters.AddWithValue("codeID", cID);
                        SqlDataReader sdr = sqlcmd.ExecuteReader();

                        while (sdr.Read())
                        {
                            SystemAlertTemplate t = new SystemAlertTemplate();

                            t.CodeId = cID;
                            t.Text = sdr["Text"].ToString();
                           
                            sc.Add(t);
                        }//end while

                        sdr.Close();
                    }//end try

                    catch (SqlException se)
                    { }//end catch
                    catch (Exception ee)
                    { }//end catch

                }//end sql

                return sc;
            }

        }
    }
}
