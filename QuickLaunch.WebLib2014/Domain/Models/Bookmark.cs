﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
namespace QuickLaunch.Models
{    
    public class Bookmark : Abstract.IDataInfo
    {
        
        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the network id.
        /// </summary>
        public string NtId { get; set; }

        /// <summary>
        /// Gets/Sets the Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets/Sets the url.
        /// </summary>
        public string Url { get; set; }
       
        /// <summary>
        /// Gets/Sets the last update user.
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Gets/Sets the last udpate datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /// <summary>
        /// Gets/Sets the add datetime
        /// </summary>
        public DateTime add_dt { get; set; }

        public bool IsArchived { get; set; }

        #endregion

        public Bookmark() { }
                        

        #region "SELECTS"

        public static Bookmark selBookmarkById(int id)
        {
            string sql = "";
            sql = @"SELECT * FROM SSO_Bookmark fav                        
                    WHERE fav.[id] = @id";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@id", id);

            return BOHelper.FillObject<Bookmark>(DBManager.ExecCommandDR(sql, @params));
        }
        public static List<Bookmark> selBookmarkByUserId(string userId)
        {
            string sql = "";
            sql = @"SELECT * FROM SSO_Bookmark fav                        
                    WHERE fav.[NtId] LIKE @userId
                    ORDER BY OrderId";
            SqlParameter[] @params = new SqlParameter[1];
            @params[0] = new SqlParameter("@userId", userId);

            return BOHelper.FillCollection<Bookmark>(DBManager.ExecCommandDR(sql, @params));
        }
        
        public static List<Bookmark> selBookmark()
        {
            string sql = @"SELECT fav.* FROM SSO_Bookmark fav                                 
                           order by OrderId";
            SqlParameter[] @params = new SqlParameter[1];

            return BOHelper.FillCollection<Bookmark>(DBManager.ExecCommandDR(sql, @params));
        }

        #endregion

        #region "INSERTS"
        public static string insBookmark(Bookmark fav)
        {
            string sql = @"INSERT INTO SSO_Bookmark ([NtId],[Title],[Url],[OrderId]) VALUES (@NtId,@Title,@Url,@OrderId); SELECT @@IDENTITY as id";
            SqlParameter[] @params = new SqlParameter[5];
            @params[0] = new SqlParameter("@NtId", fav.NtId);
            @params[1] = new SqlParameter("@Title", fav.Title);
            @params[2] = new SqlParameter("@Url", fav.Url);
            @params[3] = new SqlParameter("@OrderId", fav.OrderId);

            return BOHelper.FillObject<Bookmark>(DBManager.ExecCommandDR(sql, @params)).id.ToString();
        }
        public static void insBookmark(List<Bookmark> fav)
        {
            string sql = @"";
            foreach(Bookmark b in fav)
                sql += string.Format(@"INSERT INTO SSO_Bookmark ([NtId],[Title],[Url],[OrderId]) VALUES ('{0}','{1}','{2}',{3}); ", b.NtId, Shared.SysHelper.CleanSql(b.Title), Shared.SysHelper.CleanSql(b.Url), b.OrderId);           
            
            SqlParameter[] @params = new SqlParameter[1];
            if(!string.IsNullOrEmpty(sql))
                DBManager.ExecCommandDR(sql, @params);
        }

        #endregion

        #region "UPDATES"
        public static void updBookmark(Bookmark fav)
        {
            string sql = @"UPDATE SSO_Bookmark SET [Title] = @Title,[Url] = @Url,[OrderId] = @OrderId, lup_dt = @lup_dt WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[10];
            @params[0] = new SqlParameter("@NtId", fav.NtId);
            @params[1] = new SqlParameter("@Title", fav.Title);
            @params[2] = new SqlParameter("@Url", fav.Url);
            @params[3] = new SqlParameter("@OrderId", fav.OrderId);         
            @params[4] = new SqlParameter("@id", fav.id);

            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void updBookmark(List<Bookmark> fav)
        {
            string sql = @"";
            foreach (Bookmark b in fav)
                sql += string.Format(@"UPDATE SSO_Bookmark SET [Title] = '{1}',[Url] = '{2}',[OrderId] = {3}, lup_dt = getdate() WHERE [id] = {0}; ", b.id, Shared.SysHelper.CleanSql(b.Title), Shared.SysHelper.CleanSql(b.Url), b.OrderId);

            SqlParameter[] @params = new SqlParameter[1];
            if (!string.IsNullOrEmpty(sql))
                DBManager.ExecCommandDR(sql, @params);
        }
        public static void updBookmarkOrder(List<Bookmark> favs)
        {
            string sql = string.Empty;
            for (int i = 0; i < favs.Count; i++)
                sql += "UPDATE SSO_Bookmark SET [OrderId] = " + (i + 1).ToString() + " WHERE [id] = " + favs[i].id.ToString() + "; ";
            SqlParameter[] @params = new SqlParameter[1];
            if (!string.IsNullOrEmpty(sql))
                DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delBookmark(Bookmark Bookmark)
        {
            string sql = @"DELETE FROM SSO_Bookmark WHERE [id] = @id";
            SqlParameter[] @params = new SqlParameter[3];
            @params[0] = new SqlParameter("@id", Bookmark.id);
            @params[1] = new SqlParameter("@lup_dt", DateTime.Now);
            DBManager.ExecCommandDR(sql, @params, true);
        }
        public static void delBookmark(List<Bookmark> fav)
        {
            string sql = @"";
            foreach (Bookmark b in fav)
                sql += string.Format(@"DELETE FROM SSO_Bookmark WHERE [id] = {0}; ", b.id);

            SqlParameter[] @params = new SqlParameter[1];
            if (!string.IsNullOrEmpty(sql))
                DBManager.ExecCommandDR(sql, @params);
        }
        #endregion
    }
}
