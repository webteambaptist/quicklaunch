﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Reflection;

namespace QuickLaunch.Shared.Domain.Concrete
{
    public class Utility
    {
        /// <summary>
        /// Logs the exception.
        /// </summary>
        /// <param name="ex">The ex.</param>
        public static void LogException(Exception ex, bool IsFatal, string currentUserID)
        {
            EventLog myLog = new EventLog();
            myLog.Source = "Quick Launch Notifier";
            myLog.WriteEntry("Version " + Assembly.GetExecutingAssembly().GetName().Version + "\n\n" + ex.Message + "\n\nStack Trace:\n" + ex.StackTrace, EventLogEntryType.Error);
            QuickLaunch.Models.ErrorLog.AddErrorToLog(ex, currentUserID ?? "N/A", "NOTIFIER", IsFatal);
            if (ex.InnerException != null)
            {
                LogException(ex.InnerException, IsFatal, "");
            }
        }
    }
}
