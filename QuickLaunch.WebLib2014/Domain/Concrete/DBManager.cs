﻿using QuickLaunch.Shared;
using System;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Runtime.CompilerServices;

public class DBManager
{
	private static string ConnectionString
	{
		get;
		set;
	}

	public DBManager()
	{
	}

    #region Properties
    //private const string ConnHash = "j6O2hjHCq3bNONcO+1YbJlBMmbHcrmdORqWM7e4NzMM7Pw+2gtBwIGMMp+UnNrZBn/cx4wcRwgYNnGW1YTdcTIXyOS2aapqGy7mXFzlFLLoudHwPv1MD72R9ESiOd508/DwxZyvCtvPy9Ec6FidrTg=="; //TEST
    private const string ConnHash = "j0qGJE/FB0pS85AXz5rH+OgR7gDCvHVdcBgTnyQMzLNeo6wcEUqCPOnnZ1o7dYPNUwazRcr98cR9yWtRB31UMdHkyx8RhCMWrLoMuUZ9AhzNm98CA66auXyxLXBSQMIbiUzoJUPice+XIrEY1ySaRQ=="; //PROD

    #endregion

	private static SqlConnection ConnectionOleDb()
	{
		SqlConnection sqlConnection;
		try
		{

			if (string.IsNullOrEmpty(DBManager.ConnectionString))
			{
                DBManager.ConnectionString = SysHelper.Decrypt(ConnHash);
			}
			sqlConnection = new SqlConnection()
			{
				ConnectionString = (ConfigurationManager.ConnectionStrings["TITOConnString"] != null ? ConfigurationManager.ConnectionStrings["TITOConnString"].ToString() : DBManager.ConnectionString)
			};
		}
		catch (Exception exception)
		{
			sqlConnection = null;
		}
		return sqlConnection;
	}

	public static IDataReader ExecCommandDR(string sQuery, SqlParameter[] @params, bool CloseConnImmediately = false, bool IsRetry = false)
	{
		IDataReader dataReader;
		bool flag;
		SqlConnection sqlConnection = DBManager.ConnectionOleDb();
		SqlCommand sqlCommand = new SqlCommand(sQuery, sqlConnection);
		for (int i = 0; i <= (int)@params.Length - 1; i++)
		{
			try
			{
				if (@params[i] != null)
				{
					if (@params[i].SqlDbType != SqlDbType.DateTime)
					{
						flag = true;
					}
					else
					{
						flag = (@params[i].Value.ToString().Contains("0001") ? false : !@params[i].Value.ToString().Contains("1900"));
					}
					if (!flag)
					{
						@params[i].Value = DBNull.Value;
					}
					SqlParameterCollection parameters = sqlCommand.Parameters;
					string parameterName = @params[i].ParameterName;
					object value = @params[i].Value;
					if (value == null)
					{
						value = string.Empty;
					}
					parameters.AddWithValue(parameterName, value);
				}
			}
			catch
			{
			}
		}
		try
		{
			try
			{
				sqlConnection.Open();
				sqlCommand.CommandTimeout = 120;
				IDataReader dataReader1 = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
				if (!CloseConnImmediately)
				{
					dataReader = dataReader1;
				}
				else
				{
					dataReader1.Close();
					dataReader = null;
				}
			}
			catch (SqlException sqlException1)
			{
				SqlException sqlException = sqlException1;
				if (IsRetry)
				{
					throw sqlException;
				}
				dataReader = DBManager.ExecCommandDR(sQuery, @params, CloseConnImmediately, true);
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}
		finally
		{
		}
		return dataReader;
	}

	public static DataSet ExecCommandDS(string strQuery, SqlParameter[] @params)
	{
		DataSet dataSet;
		DataSet dataSet1 = new DataSet();
		SqlConnection sqlConnection = DBManager.ConnectionOleDb();
		SqlCommand sqlCommand = new SqlCommand(strQuery, sqlConnection);
		for (int i = 0; i <= (int)@params.Length - 1; i++)
		{
			try
			{
				if (@params[i] != null)
				{
					sqlCommand.Parameters.AddWithValue(@params[i].ParameterName, @params[i].Value);
				}
			}
			catch
			{
			}
		}
		try
		{
			try
			{
				SqlDataAdapter sqlDataAdapter = new SqlDataAdapter()
				{
					SelectCommand = sqlCommand
				};
				sqlDataAdapter.SelectCommand.Connection = sqlConnection;
				sqlDataAdapter.SelectCommand.CommandTimeout = 120;
				sqlDataAdapter.Fill(dataSet1);
				dataSet = dataSet1;
			}
			catch (Exception exception)
			{
				throw exception;
			}
		}
		finally
		{
			sqlConnection.Close();
		}
		return dataSet;
	}

	public static IDataReader ExecQueryDR(string strQuery)
	{
		IDataReader dataReader;
		SqlConnection sqlConnection = DBManager.ConnectionOleDb();
		try
		{
			try
			{
				DataSet dataSet = new DataSet();
				SqlCommand sqlCommand = new SqlCommand(strQuery)
				{
					CommandType = CommandType.Text
				};
				dataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
			}
			catch (Exception exception1)
			{
				Exception exception = exception1;
				sqlConnection.Close();
				throw exception;
			}
		}
		finally
		{
		}
		return dataReader;
	}

	public static bool HasDBConnection()
	{
		bool flag;
		try
		{
			SqlConnection sqlConnection = DBManager.ConnectionOleDb();
			sqlConnection.Open();
			sqlConnection.Close();
			flag = true;
		}
		catch (Exception exception)
		{
			flag = false;
		}
		return flag;
	}
}
