﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.DirectoryServices;
//using ActiveDs;

namespace QuickLaunch.Shared
{
    public class ADHelper
    {        
        public ADHelper()
        {
        }

        /// <summary>
        /// Determines whether [is in group] [the specified user].
        /// </summary>
        /// <param name="groups">Available AD groups.</param>
        /// <param name="group">The group.</param>
        /// <returns>
        ///   <c>true</c> if [is in group] [the specified user]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsInGroup(List<string> groups, string group)
        {
            return groups.Contains(group);
        }
        public static bool IsInGroup(List<string> groups, string[] group)
        {
            foreach (string s in group)
                if (groups.Contains(s))
                    return true;
            return false;
        }

        /// <summary>
        /// Gets the groups.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public static bool GetGroups(ref Shared.UserInfo user)
        {            
            bool result = true;
            DirectorySearcher search = new DirectorySearcher(new DirectoryEntry("LDAP://BH.LOCAL"));  //DirectoryEntry de = new DirectoryEntry(string.Format("WinNT://{0}/{1},user", user.Domain, user.UserName)); 
            search.Filter = string.Format("(SAMAccountName={0})", user.UserName);
            search.PropertiesToLoad.Add("department");
            search.PropertiesToLoad.Add("title");
            SearchResult r = search.FindOne();            
            try
            {                
                var gs = (IEnumerable)r.GetDirectoryEntry().Invoke("Groups");
                foreach (var g in gs)
                {
                    var gn = new DirectoryEntry(g);
                    user.ADgroups.Add(gn.Name.Replace("CN=",string.Empty));
                }

                try
                {                    
                    user.UserAdTitle = r.Properties["title"][0].ToString();
                    user.UserAdDept = r.Properties["department"][0].ToString();
                }
                catch { user.UserAdTitle = string.Empty; user.UserAdDept = string.Empty; } //not all users have these attributes in AD, and properties.contains is not doing a proper check
            }
            catch (Exception ex)
            {
                result = false;
                //Program.LogException(ex, false);
            }
            search.Dispose(); 
            return result;
        }

        public static int DaysTillExpiration(Shared.UserInfo user)
        {            
            DirectoryEntry de = new DirectoryEntry("LDAP://BH.LOCAL");
            DirectorySearcher search = new DirectorySearcher(de, "(objectClass=*)",null, SearchScope.Base);            
            SearchResult result = search.FindOne();
            
            long MaxPwdDaysTillExpire = 90;            
            Int64 int64Val = (Int64)result.Properties["maxPwdAge"][0];
            int hLi = (int)(int64Val >> 32);
            int lLi = (int)(int64Val & 0xFFFFFFFF);
            MaxPwdDaysTillExpire = (long)((hLi * Math.Pow(2, 32)) + lLi) / -864000000000;

            if (MaxPwdDaysTillExpire < 0)
                MaxPwdDaysTillExpire *= -1;
            else if (MaxPwdDaysTillExpire > 180)
                MaxPwdDaysTillExpire = 180;

            search = new DirectorySearcher(de);
            search.Filter = string.Format(@"(SAMAccountName={0})", "jehle001"); //user.UserName);           //user.UserName
            result = search.FindOne();
            de = result.GetDirectoryEntry();
            
            //LargeInteger liAcctPwdChange = de.Properties["pwdLastSet"].Value as LargeInteger;
            Int64 liAcctPwdChange = (Int64)result.Properties["pwdLastSet"][0];

            int hLiPChg = (int)(liAcctPwdChange >> 32);
            int lLiPChg = (int)(liAcctPwdChange & 0xFFFFFFFF);
            long AcctPwdChange = (long)((hLiPChg * Math.Pow(2, 32)) + lLiPChg);
            if (AcctPwdChange < 0)
                AcctPwdChange *= -1;
            //AcctPwdChange = System.Math.Abs(AcctPwdChange);
            //long AcctPwdChange = (((long)(liAcctPwdChange.HighPart) << 32) + (long)liAcctPwdChange.LowPart); // Convert the highorder/loworder parts of the property pulled to a long.
                        
            DateTime dtNow = DateTime.Now;
            DateTime dtAcctPwdChange = DateTime.FromFileTime(AcctPwdChange).AddDays(MaxPwdDaysTillExpire);
            string strAcctPwdChange = DateTime.FromFileTime(AcctPwdChange).ToShortDateString();
            string strAcctPwdExpires = DateTime.FromFileTime(AcctPwdChange).AddDays(MaxPwdDaysTillExpire).ToShortDateString();



            TimeSpan time;
            time = dtAcctPwdChange - dtNow;
            Console.WriteLine(strAcctPwdChange);
            Console.WriteLine(strAcctPwdExpires);
            Console.WriteLine(time.Days.ToString() + " day(s) till expired");
            return time.Days;
        }

        /// <summary>
        /// Gets the groups.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <returns></returns>
        public static List<Models.Machine> GetComputers()
        {
            List<Models.Machine> machines = new List<Models.Machine>();
            DirectoryEntry de = new DirectoryEntry(string.Format(@"LDAP://{0}", "bh"));
            DirectorySearcher ds = new DirectorySearcher(de, "(&(objectCategory=computer))", new string[] { "name", "description", "location" }, SearchScope.Subtree);            
            SearchResultCollection src = ds.FindAll();
            try
            {                
                foreach (SearchResult g in src)
                {
                    DirectoryEntry subDE = g.GetDirectoryEntry();
                    machines.Add(new Models.Machine {
                        PCName = (subDE.Properties["name"].Value ?? string.Empty).ToString(),
                        Description = (subDE.Properties["description"].Value ?? string.Empty).ToString(),
                        LastLogin = new DateTime(1900, 1, 1)
                    });
                }
            }
            catch (Exception ex)
            {                
                //Program.LogException(ex, false);
            }
            return machines;
        }
    
    }
}
