﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Diagnostics;
using System.Management;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.ServiceProcess;
using System.Text.RegularExpressions;
using System.IO;
using System.Security;
using System.Security.Cryptography;

namespace QuickLaunch.Shared
{
    public class SysHelper
    {
        #region Properties
        static readonly string PHash = "9F579CB7-2EB3-4D2A-BA12-69E90ACB6AAF";
        static readonly string SaltKey = "03690DC9-C7DB-45FE-ABCC-F58F13FCACE5";
        static readonly string VIKey = "TI$2pIjHR$1pIa14";
        #endregion

        public static string GetRegistrySetting(string key, string valueName)
        {
            return (string)Registry.GetValue(@key, valueName, string.Empty);
        }
        public static bool SetRegistrySetting(string key, string valueName, string value)
        {
            try
            {
                Registry.SetValue(@key, valueName, value);
                return true;
            }catch{ return false; }
        }        
        public static bool IsVirtualMachine()
        {
            try
            {
                if (SysHelper.GetRegistrySetting(@"HKLM\Software\VMware, Inc.\VMware Tools", "LastBoot") == "virtual")
                    return true;
            } catch {}
            return false;
        }
        public static string CleanUserId(string ntID)
        {
            return ntID.ToUpper().Substring(ntID.IndexOf(@"\") + 1);
        }
        public static string CleanSql(string value)
        {
            return value != null ? value.Replace("'", "''") : string.Empty;
        }
        public static string GetIpAddress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)            
                if (ip.AddressFamily == AddressFamily.InterNetwork)                
                    localIP = ip.ToString();                
            
            return localIP;
        }
        public static string GetHospitalFromIp(string ip)
        {
            try {
                string identifier = ip.Split('.')[1];
                if(string.IsNullOrEmpty(identifier)) return "N/A";
                string aBeaches = "4,18,220",
                       aDowntown = "1,2,16,101,102,103,104,105,106,107,108,109,200,250",
                       aISCenter = "15",
                       aMetro = "11,30,226",
                       aNassau = "5,222",
                       aPriCare = "3,8,10,12",
                       aSouth = "6,14,17,224";

                if (aDowntown.Contains(identifier))
                    return "Downtown/WCH";
                else if (aBeaches.Contains(identifier))
                    return "Beaches";
                else if (aISCenter.Contains(identifier))
                    return "IS Center";
                else if (aMetro.Contains(identifier))
                    return "Metro";
                else if (aNassau.Contains(identifier))
                    return "Nassau";
                else if (aPriCare.Contains(identifier))
                    return "Primary Care";
                else if (aSouth.Contains(identifier))
                    return "South";
                else
                    return "Unknown";
            }
            catch { return "N/A"; }
        }
        public static bool IsServiceRunning(string service)
        {
            try
            {
                ServiceController sc = new ServiceController(service);
                if (sc.Status == ServiceControllerStatus.Running)
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }
        public static object GetPropValue(object src, Models.Report.Column column)
        {
            try
            {
                System.Reflection.PropertyInfo pi = src.GetType().GetProperty(column.Source);                
                object item = pi == null ? ((System.Data.DataRow)src)[column.Source] : pi.GetValue(src, null);
                if(!string.IsNullOrEmpty(column.Format)){ //formating
                    if (item.GetType() == typeof(decimal))
                        return ((decimal)item).ToString(column.Format);
                    else if (item.GetType() == typeof(double))
                        return ((double)item).ToString(column.Format);
                    else if (item.GetType() == typeof(DateTime))
                        return ((DateTime)item).ToString(column.Format);
                }
                return item;
            }
            catch { return null; }
        }
        public static bool Compare<T>(string op, T x, T y) where T : IComparable
        {
            switch (op)
            {
                case "==": return x.CompareTo(y) == 0;
                case "!=": return x.CompareTo(y) != 0;
                case ">": return x.CompareTo(y) > 0;
                case ">=": return x.CompareTo(y) >= 0;
                case "<": return x.CompareTo(y) < 0;
                case "<=": return x.CompareTo(y) <= 0;
            }
            return false;
        }
        public static string GetUrlTitle(string url)
        {
            string title = string.Empty;
            try
            {
                WebClient x = new WebClient();
                x.Credentials = CredentialCache.DefaultNetworkCredentials;
                x.Proxy = null;                
                string source = x.DownloadString(url);
                title = Regex.Match(source, @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>", RegexOptions.IgnoreCase).Groups["Title"].Value.Replace(Environment.NewLine, string.Empty);
                if (title.Contains("Access to this site is blocked"))
                    title = string.Empty;
            } catch { title = string.Empty; }
            if (string.IsNullOrEmpty(title)) title = url.Length < 70 ? url : url.Substring(0, 69);
            return title;
        }
        //public static string GetDraggedUrl(System.Windows.Forms.DragEventArgs e)
        //{
        //    string url = string.Empty;
        //    object data = e.Data.GetData("UniformResourceLocator");
        //    if (data != null)
        //    {
        //        MemoryStream ms = data as MemoryStream;
        //        byte[] bytes = ms.ToArray();
        //        Encoding encod = Encoding.ASCII;
        //        url = encod.GetString(bytes);
        //        url = url.Substring(0, url.IndexOf('\0'));
        //    }
        //    else if (e.Data.GetData(System.Windows.Forms.DataFormats.Text) != null)
        //        url = e.Data.GetData(System.Windows.Forms.DataFormats.Text).ToString();
        //    return url;
        //}
 
        public static bool PingComputer(string sn)
        {
            try
            {
                int timeout = 3;
                System.Net.NetworkInformation.Ping pingSender = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingOptions options = new System.Net.NetworkInformation.PingOptions();
                options.DontFragment = true;
                string ipAddressOrHostName = sn;
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = System.Text.Encoding.ASCII.GetBytes(data);
                System.Net.NetworkInformation.PingReply reply = pingSender.Send(ipAddressOrHostName, timeout, buffer, options);
                return (reply.Status == System.Net.NetworkInformation.IPStatus.Success);
            }
            catch { }
            return false;
        }

        //public static ArrayList GetWMIAttributes(string qo, string whereClause)
        //{
        //    ManagementObjectSearcher searcher;
        //    int i = 0;
        //    ArrayList hd = new ArrayList();
        //    try
        //    {
        //        searcher = new ManagementObjectSearcher(string.Format("SELECT * FROM {0} {1}", qo, whereClause));
        //        foreach (ManagementObject wmi_HD in searcher.Get())
        //        {
        //            i++;
        //            PropertyDataCollection searcherProperties = wmi_HD.Properties;
        //            foreach (PropertyData sp in searcherProperties)
        //            {
        //                hd.Add(sp.Name + " = " + sp.Value);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //    return hd;
        //}

        public static List<EventLogEntry> GetErrorLog(int days)
        {
            List<EventLogEntry> entries = new List<System.Diagnostics.EventLogEntry>();
            EventLog log = new
            EventLog("Application", ".", "");
            DateTime MinTimeWritten = DateTime.Now.AddDays(-days);
            foreach (EventLogEntry entry in log.Entries)
            {
                if (entry.EntryType == EventLogEntryType.Error && entry.TimeWritten >= MinTimeWritten)
                    entries.Add(entry);
            }
            return entries;
        }

        public static StringBuilder GetSystemProcesses()
        {
            StringBuilder sbProcs = new StringBuilder("<Processes>");
            try
            {
                Process[] processes = Process.GetProcesses();
                foreach (Process p in processes)
                    sbProcs.AppendLine(String.Format("<Process><Name>{0}</Name><WindowTitle>{1}</WindowTitle><ThreadCt>{2}</ThreadCt><MemorySize>{3}</MemorySize><IsResponding>{4}</IsResponding></Process>", p.ProcessName, p.MainWindowTitle, p.Threads.Count.ToString(), (p.PrivateMemorySize64 / 1024).ToString("N0") + "K", p.Responding.ToString()));
            }
            catch { }
            sbProcs.Append("</Processes>");
            return sbProcs;
        }
        public static double ConvertToGB(object value)
        {
            double dbl;
            double.TryParse(value.ToString(), out dbl);
            return dbl / 1024 / 1024 / 1024;
        }
        public static double ConvertToMB(object value)
        {
            double dbl;
            double.TryParse(value.ToString(), out dbl);
            return dbl / 1024 / 1024;
        }
        public static SecureString ToSecurePassword(string pwd)
        {
            SecureString password = new SecureString();
            foreach (char c in pwd)
                password.AppendChar(c);
            return password;
        }
        public static string Encrypt(string pTxt)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(pTxt);

            byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }
        public static string Decrypt(string encTxt)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encTxt);
            byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }
    }
}
