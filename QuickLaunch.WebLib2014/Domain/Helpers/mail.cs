﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Mail;
namespace QuickLaunch.Shared
{
    public class mail
    {
        #region Properties
        public string from { get; set; }
        public string sendto { get; set; }
        public string cc { get; set; }
        public string bcc { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public Attachment attach { get; set; }
        public int priority { get; set; }
        #endregion

        public mail() { }

        public static bool sendMail(mail m)
        {
            SmtpClient smtpClient = new SmtpClient();
            MailMessage message = new MailMessage();

            try
            {
                smtpClient.Host = "mail.bmcjax.com";
                smtpClient.Port = 25;

                MailAddress fromAddress = new MailAddress(m.from);
                message.From = fromAddress;

                if (m.sendto == null) return false; //must have a sender
                string[] sTO = m.sendto.Split(Convert.ToChar(","));
                for (int i = 0; i < sTO.Length; i++)
                    message.To.Add(sTO[i]);

                if (m.cc != null)
                {
                    string[] sCC = m.cc.Split(Convert.ToChar(","));
                    for (int i = 0; i < sCC.Length; i++)
                        message.CC.Add(sCC[i]);
                }

                if (m.bcc != null)
                {
                    string[] sBCC = m.bcc.Split(Convert.ToChar(","));
                    for (int i = 0; i < sBCC.Length; i++)
                        message.Bcc.Add(sBCC[i]);
                }

                message.Subject = m.subject;
                message.IsBodyHtml = true;
                message.Body = m.body;

                if (m.attach != null)
                    message.Attachments.Add(m.attach);

                smtpClient.Send(message);

                return true;

            }
            catch (Exception err)
            {
                return false;
            }
        }
    }
}
